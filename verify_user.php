<?php define('WP_USE_THEMES', false);
require_once($_SERVER['DOCUMENT_ROOT'] . '/khatoco/wp-load.php');

if( isset($_GET['otp']) && $_GET['otp'] != '' && isset($_GET['email']) && $_GET['email'] != '' ){
	$otp = sanitize_text_field( $_GET['otp'] );
	$email = sanitize_email( $_GET['email'] );

	// Page 2503 : Trang thong tin ca nhan

	if( $otp == $_SESSION['verify_user_otp'] ){
		$user_id = email_exists( $email );

		if( $user_id <= 0 || !$user_id ){
			wp_redirect( add_query_arg( array('verify' => 'email_unavailable'), get_page_link(2503) ) );
		}
		else{
			unset($_SESSION['verify_user_otp']);
			update_user_meta( $user_id, 'actived', 'yes' );
			wp_redirect( add_query_arg( array('verify' => 'success'), get_page_link(2503) ) );
		}
	}
	else{
		wp_redirect( add_query_arg( array('verify' => 'failed'), get_page_link(2503) ) );
	}

	exit();
}

/*wp_redirect( home_url('/') );
exit();*/
