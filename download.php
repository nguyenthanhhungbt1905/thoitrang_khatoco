<?php define('WP_USE_THEMES', false);
require_once($_SERVER['DOCUMENT_ROOT'] . '/khatoco/wp-load.php');

define('KHATOCO_DOWNLOAD_SALT', 'Hg6032r5xpNW');

if(isset($_GET['collection'])){
	$collection = intval($_GET['collection']);
	$gallery = get_post_meta($collection, 'collection_galery', true);
	$files_id = array();

	if($gallery){
		foreach($gallery as $gkey => $p) {
			foreach($p as $img => $pid) {
				array_push($files_id, $img);
			}
		}
	}

	$path = $_SERVER['DOCUMENT_ROOT'] . '/khatoco/wp-content/uploads/';
	$filename = '[Khatoco]'. get_the_title($collection) .'.zip';
	$zipname = $path . $filename;
	if(file_exists($zipname)) unlink($zipname);
	
	$zip = new ZipArchive();
	$zip->open($zipname, ZipArchive::CREATE);
	
	if($files_id){
		foreach($files_id as $f){
			$file_info = pathinfo(get_attached_file($f));
			chdir($file_info['dirname']);
			$zip->addFile($file_info['basename']);
		}
	}
	$zip->close();

	chdir($path);
	if (headers_sent()) {
		echo 'HTTP header already sent';
		return;
	} else {
		if (!is_file($filename)) {
			header($_SERVER['SERVER_PROTOCOL'].' 404 Not Found');
			echo 'File not found';
		} else if (!is_readable($filename)) {
			header($_SERVER['SERVER_PROTOCOL'].' 403 Forbidden');
			echo 'File not readable';
		} else {
			header($_SERVER['SERVER_PROTOCOL'].' 200 OK');
			header("Content-Type: application/zip");
			header("Content-Transfer-Encoding: Binary");
			header("Content-Length: ". filesize($filename));
			header("Content-Disposition: attachment; filename=\"". $filename ."\"");
			readfile($filename);
			unlink($zipname);

			$count = (int) get_post_meta($collection, 'download_count', true);
			if($count == '') $count = 0;
			update_post_meta($collection, 'download_count', $count+1);

			exit;
		}
	}
}
elseif(isset($_GET['item'])) {
	$item = $_GET['item'];
	$file = get_attached_file($item);

	header('Content-Description: File Transfer');
	header("Content-Type: application/octet-stream");
	header('Content-Disposition: attachment; filename='. basename($file));
	header('Content-Length: '. filesize($file));
	readfile($file);
}
else {
	wp_redirect(home_url('/'));
	exit();
}