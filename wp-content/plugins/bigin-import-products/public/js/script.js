jQuery(document).ready(function($) {

    jQuery('#file-7').on('change', function(event) {
        event.preventDefault();
        jQuery('#uploadValue').html(this.value);
    });



    jQuery("#import-button").on('click', function(event) {
        event.preventDefault();

        var el = jQuery(this);
        el.css('display', 'none');
        el.siblings('.button-wait').css('display', 'inline-block');

        jQuery('.upload_message').html('');

        var data = new FormData();
        data.append("action", "import_sp");
        jQuery.each(jQuery("#file-7"), function(i, tag) {
            jQuery.each(jQuery(tag)[0].files, function(i, file) {
                data.append('product_file', file);
            });
        });

        jQuery.ajax({
            url: admin_ajax,
            type: 'POST',
            data: data,
            cache: false,
            dataType: 'json',
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
            success: function(data, textStatus, jqXHR) {
                console.log(data);

                if(data.response == 'ERROR'){
                    jQuery('.upload_message').html('<div class="alert alert-danger" role="alert">'+data.error+'</div>');
                }

                if(data.response == 'SUCCESS'){
                    jQuery('.upload_message').html('<div class="alert alert-success" role="alert">Import thành công</div>');
                    setTimeout(function(){
                        location.reload();
                    }, 1000);
                }

                jQuery("div.alert, p.alert").delay(5000).slideUp();

                el.siblings('.button-wait').css('display', 'none');
                el.css('display', 'inline-block');

            }
        });

        return false;
    });
});