<?php

if(!class_exists('WP_List_Table')){
	require_once ABSPATH . 'wp-admin/includes/class-wp-list-table.php';
}

class Cm_Ip_Importproduct_Model extends WP_List_Table{

	private $_per_page = 20;
	private $_sql;

	public function __construct(){
		parent::__construct(array(
			'plural'   => 'importproduct',
			'singular' => 'id',
			'ajax'     => false,
			'screen'   => null
		) );
	}

	public function prepare_items(){
		$columns 	= $this->get_columns();
		$hidden 	= $this->get_hidden_columns();
		$sortable 	= $this->get_sortable_columns();
		$this->_column_headers 	= array($columns,$hidden,$sortable);
		$this->items 			= $this->table_data();
		$total_items 	= $this->total_items();
		$per_page 		= $this->_per_page;
		$total_pages 	= ceil($total_items/$per_page);
		$this->set_pagination_args(array(
			'total_items' => $total_items,
			'per_page'    => $per_page,
			'total_pages' => $total_pages
		));
	}

	public function get_bulk_actions(){
		$actions = array(
			'delete' => 'Xóa'
		);
		return $actions;
	}

	public function column_file($item){
		global $cmController;
		$page      = $cmController->getParams('page');
		$name      = 'security_code';
		$lnkDelete =  add_query_arg(array('action'=>'delete','id'=>$item['id']));
		$action    = 'delete_id_' . $item['id'];
		$lnkDelete = wp_nonce_url($lnkDelete, $action, $name);

    	$upload_path = home_url() . '/wp-content/uploads/product_imports/';

		$actions   = array(
			'download' => '<a href="' . $upload_path . $item['file'] . '">Download</a>',
			'delete' => '<a href="' . $lnkDelete . '">Xóa</a>',
		);
		$html = '<strong>' . $item['file'] .'</strong>'
				. $this->row_actions($actions);
		return $html;
	}

	public function column_author($item){
		$user = get_user_by( 'id', $item['author'] );
		$html = $user->first_name . ' ' . $user->last_name;;

		return $html;
	}

	public function column_cb($item){
		$singular = $this->_args['singular'];
		$html = '<input type="checkbox" name="' . $singular .'[]" value="' . $item['id'] .'" />';
		return $html;
	}

	public function column_default($item, $column_name){
		return $item[$column_name];
	}

	private function total_items(){
		global $wpdb;
		return $wpdb->query($this->_sql);
	}

	private function table_data(){
		$data = array();
		global $wpdb,$cmController;
		$orderby 	= ($cmController->getParams('orderby') == '')? 'id' : $_GET['orderby'];
		$order		= ($cmController->getParams('order') == '')? 'DESC' : $_GET['order'];
		$tblArea = $wpdb->prefix . 'bigin_import_logs';
		$sql = 'SELECT m.*
				FROM ' . $tblArea . ' AS m ';
		$whereArr = array();

		if($cmController->getParams('s') != ''){
			$s = esc_sql($cmController->getParams('s'));
			$whereArr[] = " (m.file LIKE '%$s%') ";
		}

		if(count($whereArr)>0){
			$sql .= " WHERE " . join(" AND ", $whereArr);
		}

		$sql .= ' ORDER BY m.' . esc_sql($orderby) . ' ' . esc_sql($order);
		$this->_sql  = $sql;
		$paged 		= max(1,@$_REQUEST['paged']);
		$offset 	= ($paged - 1) * $this->_per_page;
		$sql .= ' LIMIT ' . $this->_per_page . ' OFFSET ' . $offset;
		$data = $wpdb->get_results($sql,ARRAY_A);
		return $data;
	}

	public function get_sortable_columns(){
		return array(
			'file' => array('file',true),
			'author' => array('author',true),
			'date' => array('date',true)
		);
	}

	public function get_columns(){
		$arr = array(
			'cb'         => '<input type="checkbox" />',
			'file'       => 'File Import',
			// 'message' => 'Kết quả',
			'author'     => 'Người thực hiện',
			'date'       => 'Ngày thực hiện'
		);
		return $arr;
	}

	public function get_hidden_columns(){
		return array();
	}

	public function deleteItem($arrData = array(), $options = array()){
		global $wpdb;
		$table 	= $wpdb->prefix . 'bigin_import_logs';

		$upload_dir = wp_upload_dir();
    	$upload_path = $upload_dir["basedir"]."/product_imports/";

		if(!is_array($arrData['id'])){
			$row = $this->getItem(array('id'=>absint($arrData['id'])));
			$file = $upload_path . $row['file'];
			$where 	= array('id' => absint($arrData['id']));
			$wpdb->delete($table, $where);
			if(file_exists($file)){
				unlink($file);
			}
		}else{
			$arrData['id'] = array_map('absint', $arrData['id']);
			$ids = join(',', $arrData['id']);
			$sql = "DELETE FROM $table WHERE id IN ($ids)";
			$wpdb->query($sql);
		}
	}

	public function getItem($arrData = array(), $options = array()){
		global $wpdb;
		$id = absint($arrData['id']);
		$table = $wpdb->prefix . 'bigin_import_logs';
		$sql = "SELECT * FROM $table WHERE id = $id";
		$row = $wpdb->get_row($sql, ARRAY_A);
		return $row;
	}

	public function save_item($data = array(), $options = array()){
		global $cmController, $wpdb;
		$table = $wpdb->prefix . 'bigin_import_logs';
		$wpdb->insert($table, $data);
	}

}