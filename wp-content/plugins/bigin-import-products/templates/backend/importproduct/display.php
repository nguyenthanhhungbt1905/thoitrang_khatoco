<?php 
    global $cmController;
    $tblRule = $cmController->getModel('Importproduct');
    $tblRule->prepare_items();	
    $lbl = 'Lịch Sử Import';
    $page = $cmController->getParams('page');
?>
<div class="wrap">
    <div class="import-container">
        <div class="import-header">
            <h3>Import Products</h3>
            <p>
                <a href="<?php echo CM_IP_FILE_URL; ?>/product_import_construct.xlsx">Click Here</a> to Download Template File
            </p>
        </div>
        <div class="import-wrap">
            <p style="color: red">** Please upload all images in your file BEFORE import.</p>
            <form method="post" action="" id="importForm" enctype="multipart/form-data">
                <div class="upload_message"></div>
                <div id="uploadValue"></div>
                <label class="fileContainer">
                    <span class="dashicons dashicons-upload"></span> Select File
                    <input type="file" name="file-7" id="file-7" class="inputfile inputfile-6" />
                </label>
                <input type="submit" id="import-button" class="button import-button" value="Import">
                <input type="button" class="button import-button button-wait" value="Importing ...">
            </form>
        </div>
    </div>
    
    <h2><?php echo esc_html__($lbl);?></h2>
    <?php
        if($cmController->getParams('msg') == 1){
            echo '<div class="updated"><p>Xóa thành công</p></div>';
        }
    ?>
    <form action="" method="post" name="<?php echo $page;?>" id="<?php echo $page;?>">
	<?php $tblRule->search_box('search', 'search_id');?>
	<?php $tblRule->display();?>
    </form>
</div>
<link rel="stylesheet" href="<?php echo CM_IP_CSS_URL; ?>/style.css">
<script>
    var admin_ajax = "<?php echo admin_url(); ?>admin-ajax.php";
</script>
<script src="<?php echo CM_IP_JS_URL; ?>/script.js"></script>