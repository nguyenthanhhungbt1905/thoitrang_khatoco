<?php
class Cm_Ip_ImportProduct_Controller{
	public function __construct(){
        $this->dispatch_function();
	}

	public function dispatch_function(){
            
		global $cmController;

		$action = $cmController->getParams('action');
		
		switch ($action){
			case 'delete'	: $this->delete(); break;
			default			: $this->display(); break;
		}

	}

	public function display(){
		global $cmController;
		if($cmController->getParams('action') == -1){
			$url = $this->createUrl();
			wp_redirect($url);
		}
		
		$cmController->getView('/importproduct/display.php','/backend');
	}

	public function createUrl(){
		global $cmController;
		
		$url = 'admin.php?page=' . $cmController->getParams('page');
		
		//filter_status
		if($cmController->getParams('filter_status') != '0'){
			$url .= '&filter_status=' . $cmController->getParams('filter_status');
		}
		
		if(mb_strlen($cmController->getParams('s'))){
			$url .= '&s=' . $cmController->getParams('s');
		}
		
		return $url;
	}

	public function delete(){
		
		global $cmController;
		$arrParam = $cmController->getParams();
		
		if(!is_array($arrParam['id'])){
			$action 	= 'delete_id_' . $arrParam['id'];
			check_admin_referer($action,'security_code');
		}else{
			wp_verify_nonce('_wpnonce');
		}
		
		$model = $cmController->getModel('Importproduct');
		$model->deleteItem($arrParam);
		
		$paged = max(1,$arrParam['paged']);
		$url = 'admin.php?page=' . $_REQUEST['page']. '&msg=1';
		wp_redirect($url);
	}

}