<?php
class Cm_Controller{
	public function __construct($options = array()){

	}

	public function isPost(){
		$flag = ($_SERVER['REQUEST_METHOD']=='POST') ? true : false;
		return $flag;
	}

	public function getParams($name = null){
		if($name == null || empty($name)){
			return $_REQUEST;
		}else{
			$val = (isset($_REQUEST[$name])) ? $_REQUEST[$name] : '';
			return $val;
		}
	}

	public function getController($filename = '', $dir = ''){
		
		$obj = new stdClass();
		
		$file =  CM_IP_CONTROLLER_PATH . $dir . DS . $filename . '.php';
		
		if(file_exists($file)){
			require_once $file;
			$controllerName = CM_IP_PREFIX . $filename . '_Controller';
			$obj = new $controllerName ();
		}
		return $obj;
	}
	
	public function getModel($filename = '', $dir = ''){

		$obj = new stdClass();
		
		$file =  CM_IP_MODELS_PATH . $dir . DS . $filename . '.php';
		
		if(file_exists($file)){
			require_once $file;
			$modelName = CM_IP_PREFIX . $filename . '_Model';
			$obj = new $modelName ();
		}
		return $obj;
	}
	
	public function getView($filename = '', $dir = ''){
		
		$file =  CM_IP_TEMPLATE_PATH . $dir . DS . $filename;
		
		if(file_exists($file)){
			require_once $file;		
		}
	}
	
	public function getValidate($filename = '', $dir = ''){

		$obj = new stdClass();
		
		$file =  CM_IP_VALIDATE_PATH . $dir . DS . $filename . '.php';
		
		if(file_exists($file)){
			require_once $file;
			$validateName = CM_IP_PREFIX . $filename . '_Validate';
			$obj = new $validateName ();
		}
		return $obj;
	}
	
	public function getCssUrl($filename = '', $dir = ''){
		
		$url = CM_IP_CSS_URL . $dir . '/' . $filename;
		
		$headers = @get_headers($url);
		$flag = stripos($headers[0], "200 OK")?true:false;
		
		if($flag == true){
			return $url;
		}
		
		return false;
	}
	
	public function getImageUrl($filename = '', $dir = ''){

		$url = CM_IP_IMAGES_URL . $dir . '/' . $filename;
		
		$headers = @get_headers($url);
		$flag = stripos($headers[0], "200 OK")?true:false;
		
		if($flag == true){
			return $url;
		}
		
		return false;
	}
	
	public function getJsUrl($filename = '', $dir = ''){

		$url = CM_IP_JS_URL . $dir . '/' . $filename;
		
		$headers = @get_headers($url);
		$flag = stripos($headers[0], "200 OK")?true:false;
		
		if($flag == true){
			return $url;
		}
		
		return false;
	}

	public function getData($table, $select='*', $where='', $order='', $limit=''){
        global $wpdb;
        $sql = 'select '.$select.' from '.$table;
        if($where != '') $sql .= ' where '.$where;
        if($order != '') $sql .= ' order by '.$order;
        if($limit != '') $sql .= ' limit '.$limit;

        $results = $wpdb->get_results($sql) or die(mysql_error());
        return $results;
    }

}