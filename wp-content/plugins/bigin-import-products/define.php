<?php

//====================URL==========================
define('CM_IP_PLUGIN_URL', plugins_url() . '/bigin-import-products');
define('CM_IP_CSS_URL', CM_IP_PLUGIN_URL . '/public/css');
define('CM_IP_JS_URL', CM_IP_PLUGIN_URL . '/public/js');
define('CM_IP_FILE_URL', CM_IP_PLUGIN_URL . '/public/files');

//====================PATH==========================
define('DS', DIRECTORY_SEPARATOR);
define('CM_IP_PLUGIN_PATH', plugin_dir_path( __FILE__ ));
define('CM_IP_CONTROLLER_PATH', CM_IP_PLUGIN_PATH . 'controllers');
define('CM_IP_MODELS_PATH', CM_IP_PLUGIN_PATH . 'models');
define('CM_IP_TEMPLATE_PATH', CM_IP_PLUGIN_PATH . 'templates');

define('CM_IP_PREFIX', 'Cm_Ip_');

