<?php
/**
 * Plugin Name: Bigin Import Products
 * Plugin URI: http://bigin.vn/
 * Description: Import Product Lists by Excel
 * Version: 1.0
 * Author: Bigin
 * Author URI: http://bigin.vn/
 */

register_activation_hook( __FILE__, 'bigin_import_install' );

function bigin_import_install() {
    global $wpdb;
    $table_name = $wpdb->prefix . 'bigin_import_logs';
    $charset_collate = $wpdb->get_charset_collate();

    $sql = "CREATE TABLE $table_name (
        id mediumint(9) NOT NULL AUTO_INCREMENT,
        date datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
        file varchar(255) NOT NULL,
        message text,
        author int(11) NOT NULL,
        PRIMARY KEY  (id)
    ) $charset_collate;";
    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );
}


require_once 'define.php';
require_once 'controllers/cmController.php';
global $cmController;
$cmController = new Cm_Controller();
require_once 'BiginClass.php';
new Bigin_Class();

function import_sp() {

    $fileErrors = array(
        0 => "There is no error, the file uploaded with success",
        1 => "The uploaded file exceeds the upload_max_files in server settings",
        2 => "The uploaded file exceeds the MAX_FILE_SIZE from html form",
        3 => "The uploaded file uploaded only partially",
        4 => "No file was uploaded",
        6 => "Missing a temporary folder",
        7 => "Failed to write file to disk",
        8 => "A PHP extension stoped file to upload"
    );
    $posted_data =  isset( $_POST ) ? $_POST : array();
    $file_data = isset( $_FILES ) ? $_FILES : array();
    $data = array_merge( $posted_data, $file_data );
    $response = array();

    $upload_dir = wp_upload_dir();
    $upload_path = $upload_dir["basedir"]."/product_imports/";
    $upload_url = $upload_dir["baseurl"]."/product_imports/";
    if(!file_exists($upload_path)){
        mkdir($upload_path);
    }
    $fileName = $data["product_file"]["name"];
    $fileNameChanged = str_replace(" ", "_", $fileName);

    list($first, $end) = explode(".", $fileNameChanged);
    $fileNameChanged = $first . '_' . round(microtime(true)) . '.' . $end;

    $temp_name = $data["product_file"]["tmp_name"];
    $file_size = $data["product_file"]["size"];
    $fileError = $data["product_file"]["error"];
    $mb = 2 * 1024 * 1024;
    $targetPath = $upload_path;
    $response["filename"] = $fileName;
    $response["file_size"] = $file_size;

    if($fileError > 0){
        $response["response"] = "ERROR";
        $response["error"] = $fileErrors[ $fileError ];
    } else {
        if(file_exists($targetPath . "/" . $fileNameChanged)){
            $response["response"] = "ERROR";
            $response["error"] = "Vui lòng chọn file cần import.";
        } else {
            $file = pathinfo( $targetPath . "/" . $fileNameChanged );
            if( $file && isset( $file["extension"] ) ){
                $type = $file["extension"];
                if($type == 'xlsx' || $type == 'xls'){
                    if($file_size <= $mb){
                        if( move_uploaded_file( $temp_name, $targetPath . "/" . $fileNameChanged ) ){
                            $response["response"] = "SUCCESS";
                            $response["url"] =  $upload_url . "/" . $fileNameChanged;
                            $response["type"] = $type;

                            //begin import
                            import($fileNameChanged);
                        } else {
                            $response["response"] = "ERROR";
                            $response["error"]= "Có lỗi xảy ra. Vui lòng thực hiện lại.";
                        }
                    } else {
                        $response["response"] = "ERROR";
                        $response["error"]= "File quá lớn. Dung lượng file cho phép tối đa là 2MB.";
                    }
                }else{
                    $response["response"] = "ERROR";
                    $response["error"]= "Vui lòng upload file excel (.xls, .xlsx)";
                }
            }
        }
    }
    echo json_encode( $response );
    die();
}
add_action( 'wp_ajax_import_sp', 'import_sp' );
add_action( 'wp_ajax_nopriv_import_sp', 'import_sp' );

function import($file){
    require_once get_template_directory() . '/includes/phpexcel/PHPExcel/IOFactory.php';
    $upload_dir = wp_upload_dir();
    $filename = $upload_dir["basedir"]."/product_imports/".$file;


    $inputFileType = PHPExcel_IOFactory::identify($filename);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objReader->setReadDataOnly(true);

    $objPHPExcel = $objReader->load("$filename");
    $total_sheets=$objPHPExcel->getSheetCount();

    $allSheetName = $objPHPExcel->getSheetNames();
    $objWorksheet  = $objPHPExcel->setActiveSheetIndex(0);
    $highestRow    = $objWorksheet->getHighestRow();
    $highestColumn = $objWorksheet->getHighestColumn();
    $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
    $arraydata = array();
    $dd = array();
    $post_error = array();
    for ($row = 2; $row <= $highestRow;++$row)
    {
       $no = $row-1;
       // for ($col = 0; $col < $highestColumnIndex; ++$col)
       // {
       //     $value = $objWorksheet->getCellByColumnAndRow($col, $row)->getValue();
       //     $arraydata[$row-2][$col] = $value . '--';
       // }
       $fields[] = array(
           'ten_san_pham'          => $objWorksheet->getCellByColumnAndRow(0, $row)->getValue(),
           'permalink'             => $objWorksheet->getCellByColumnAndRow(1, $row)->getValue(),
           'post_sku'              => $objWorksheet->getCellByColumnAndRow(2, $row)->getValue(),
           'post_price'            => $objWorksheet->getCellByColumnAndRow(3, $row)->getValue(),
           'sale_off'              => $objWorksheet->getCellByColumnAndRow(4, $row)->getValue(),
           'post_weight'           => $objWorksheet->getCellByColumnAndRow(5, $row)->getValue(),
           'loai_san_pham'         => $objWorksheet->getCellByColumnAndRow(6, $row)->getValue(),
           'san_pham_moi'          => $objWorksheet->getCellByColumnAndRow(9, $row)->getValue(),
           'kich_thuoc'            => $objWorksheet->getCellByColumnAndRow(7, $row)->getValue(),
           'post_is_hot'           => $objWorksheet->getCellByColumnAndRow(8, $row)->getValue(),
           'ma_mau'                => $objWorksheet->getCellByColumnAndRow(10, $row)->getValue(),
           'ten_mau'               => $objWorksheet->getCellByColumnAndRow(11, $row)->getValue(),
           'ma_code_mau'           => $objWorksheet->getCellByColumnAndRow(12, $row)->getValue(),
           'anh_san_pham'          => $objWorksheet->getCellByColumnAndRow(13, $row)->getValue(),
           'post_info'             => $objWorksheet->getCellByColumnAndRow(14, $row)->getValue(),
           'post_delivery'         => $objWorksheet->getCellByColumnAndRow(15, $row)->getValue(),
           'post_preservation'     => $objWorksheet->getCellByColumnAndRow(16, $row)->getValue(),
           '_yoast_wpseo_metadesc' => $objWorksheet->getCellByColumnAndRow(17, $row)->getValue(),
           '_yoast_wpseo_focuskw'  => $objWorksheet->getCellByColumnAndRow(18, $row)->getValue(),
       );
       
        $post_title = $objWorksheet->getCellByColumnAndRow(0, $row)->getValue();
        $permalink  = $objWorksheet->getCellByColumnAndRow(1, $row)->getValue();
        $post_sku   = $objWorksheet->getCellByColumnAndRow(2, $row)->getValue();
        $ma_mau     = $objWorksheet->getCellByColumnAndRow(10, $row)->getValue();
        if(!empty($post_title) && !empty($post_sku) && !empty($ma_mau)){
            $check_post_exist = cm_check_post_exist($post_sku, $ma_mau);
            $kich_thuoc = $objWorksheet->getCellByColumnAndRow(7, $row)->getValue();
            $post_sizes_arr = get_post_sizes($kich_thuoc);
            if($check_post_exist){
                //update tồn kho
                $pId = $check_post_exist;
                update_post_meta($pId, 'post_sizes', $post_sizes_arr['sizes']);
                update_post_meta($pId, 'post_onhand', $post_sizes_arr['post_onhand']);
                update_post_meta($pId, 'post_onhand_all', array_sum($post_sizes_arr['post_onhand']));
            }else{
                $post_name = (!empty($permalink)) ? sanitize_title($permalink) : sanitize_title($post_title);
                $my_post = array(
                   'post_title'  => $post_title,
                   'post_author' => get_current_user_id(),
                   'post_status' => 'pending',
                   'post_type'   => 'post',
                   'post_name'   => $post_name
                );
                $post_id = wp_insert_post( $my_post );
                if($post_id){
                   //Sku
                   update_post_meta($post_id, 'post_sku', $post_sku);
                   // Store ID
                   update_post_meta($post_id, 'store_id', 'KHBSMVD');
                   //unit
                   update_post_meta($post_id, 'post_unit', 'CAI');
                   //Price
                   $post_price = $objWorksheet->getCellByColumnAndRow(3, $row)->getValue();
                   update_post_meta($post_id, 'post_price', $post_price);
                   //Sale off
                   $sale_off = $objWorksheet->getCellByColumnAndRow(4, $row)->getValue();
                   if(empty($sale_off)){
                       update_post_meta($post_id, 'sale_off', '0');
                   }else{
                       update_post_meta($post_id, 'sale_off', $sale_off);
                   }
                   //Weight
                   $post_weight = $objWorksheet->getCellByColumnAndRow(5, $row)->getValue();
                   update_post_meta($post_id, 'post_weight', $post_weight);
                   //Category Ids
                   $loai_san_pham = $objWorksheet->getCellByColumnAndRow(6, $row)->getValue();
                   $san_pham_moi = $objWorksheet->getCellByColumnAndRow(9, $row)->getValue();
                   if (!empty($loai_san_pham)) {
                       $categories = explode(',', $loai_san_pham);
                       // safeoff or not
                       if(!empty($sale_off) && $sale_off != '0')
                           array_push($categories, 111);
                       // new products?
                       if(!empty($san_pham_moi) && $san_pham_moi != '0'){
                           array_push($categories, 112);
                           update_post_meta($post_id, 'post_is_new', '1');
                       } else {
                           update_post_meta($post_id, 'post_is_new', '0');
                       }
                       // set category
                       wp_set_post_categories($post_id, $categories, false);
                       update_post_meta($post_id, 'post_category_ids', explode(',', $loai_san_pham));
                   }
                    //Sizes && tồn kho
                    if (!empty($kich_thuoc)) {
                        update_post_meta($post_id, 'post_sizes', $post_sizes_arr['sizes']);
                        update_post_meta($post_id, 'post_onhand', $post_sizes_arr['post_onhand']);
                        update_post_meta($post_id, 'post_onhand_all', array_sum($post_sizes_arr['post_onhand']));
                    }
                   //is Hot
                   $post_is_hot = $objWorksheet->getCellByColumnAndRow(8, $row)->getValue();
                   update_post_meta($post_id, 'post_is_hot', $post_is_hot);
                   //color
                   
                   $ten_mau = $objWorksheet->getCellByColumnAndRow(11, $row)->getValue();
                   $ma_code_mau = $objWorksheet->getCellByColumnAndRow(12, $row)->getValue();
                   $ma_code_mau = ($ma_code_mau == '') ? '#C61D20' : $ma_code_mau;
                   $anh_san_pham = $objWorksheet->getCellByColumnAndRow(13, $row)->getValue();
                   $array_id = cm_get_image_id($anh_san_pham);
                   $ids = ($array_id) ? implode(',', array_map('array_pop', $array_id)) : '';
                   
                   $image_arr = explode(',', $anh_san_pham);
                   $first_img = home_url() . '/wp-content/uploads/' . trim(reset($image_arr));
                   $colors = array(
                       array(
                           'color_id'   => $ma_mau,
                           'name'       => $ten_mau,
                           'code'       => $ma_code_mau,
                           'images_ids' => $ids,
                           'thumb'      => $first_img
                       )
                   );
                   update_post_meta($post_id, 'post_colors', $colors);
                   update_post_meta($post_id, 'post_color_id', $ma_mau);
                   // More info
                   $post_info = $objWorksheet->getCellByColumnAndRow(14, $row)->getValue();
                   $post_delivery = $objWorksheet->getCellByColumnAndRow(15, $row)->getValue();
                   $post_preservation = $objWorksheet->getCellByColumnAndRow(16, $row)->getValue();
                   update_post_meta($post_id, 'post_info', $post_info);
                   update_post_meta($post_id, 'post_delivery', $post_delivery);
                   update_post_meta($post_id, 'post_preservation', $post_preservation);
                   //Yoast
                   $_yoast_wpseo_metadesc = $objWorksheet->getCellByColumnAndRow(17, $row)->getValue();
                   $_yoast_wpseo_focuskw = $objWorksheet->getCellByColumnAndRow(18, $row)->getValue();
                   update_post_meta($post_id, '_yoast_wpseo_metadesc', $_yoast_wpseo_metadesc);
                   update_post_meta($post_id, '_yoast_wpseo_focuskw', $_yoast_wpseo_focuskw);

                }else{
                   $post_error[] = $no;
                }
            }
        }else{
           $post_error[] = $no;
        }
    }

    global $cmController;
    $import_products = $cmController->getModel('Importproduct');
    $log_data = array(
        'date'   => date('Y-m-d H:i:s'),
        'file'   => $file,
        'author' => get_current_user_id()
    );
    $import_products->save_item($log_data);
}

function cm_get_image_id($image_url) {
    global $wpdb;
    $urls_arr = explode(',', $image_url);
    $new_arr = array();
    foreach ($urls_arr as $url){
        $new_arr[] = '"'.home_url() . '/wp-content/uploads/' . trim($url).'"';
    }
    $str = '(' . implode(',', $new_arr) . ')';
 
    $sql = "SELECT ID FROM $wpdb->posts WHERE guid in $str";
    $results = $wpdb->get_results($sql, ARRAY_N);

    if(empty($results)){
        return false;
    }
    return $results;
}

function get_post_sizes($string_sizes){
    $result = [];
    $sizes = explode(',', $string_sizes);

    if(!empty($sizes)){
        foreach ($sizes as $item) {
            $item_arr = explode(':', $item);
            $size = trim($item_arr[0]);

            $result['sizes'][] = $size;
            $result['post_onhand'][$size] = trim(intval($item_arr[1]));
        }
    }
    return $result;
}

function cm_check_post_exist($post_sku, $ma_mau)
{
    global $wpdb;
    $sql = '
        SELECT p.ID FROM '.$wpdb->prefix.'posts p
        LEFT JOIN '.$wpdb->prefix.'postmeta pm ON p.ID = pm.post_id
        LEFT JOIN '.$wpdb->prefix.'postmeta pm2 ON p.ID = pm2.post_id
        WHERE p.post_type = "post" AND p.post_status IN ("pending", "publish", "draft", "private") 
            AND (
                (pm.meta_key = "post_sku" and pm.meta_value = "'.$post_sku.'") 
                AND (pm2.meta_key = "post_color_id" and pm2.meta_value = "'.$ma_mau.'")
            ) 
        GROUP BY p.ID
    ';
    $post = $wpdb->get_row($sql);
    if($post) return $post->ID;
    return false;
}