<?php
/**
 * Plugin Name: Khatoco Plugin
 * Plugin URI: #
 * Description: All about API
 * Version: 4.7.2
 * Author: ABC
 * Author URI: #
 * License: GPL2+
 * Text Domain: khatoco-plugin
 */

if ( ! function_exists( 'order_update' ) ) :
function order_update( $meta_id, $object_id, $meta_key, $meta_value ) {
	$post = get_post( );
    if (empty($post->post_type)) return;
	if ($post->post_type == 'orders' && $meta_key == 'user_update_stack') {
        $data = get_post_meta($object_id);
        $info = $_POST;

        $data_save['order_user_id'] = get_current_user_id();
        if(isset($post->ID))
            $data_save['order_id']    = $post->ID;
        if(isset($data['order_code'][0]))
            $data_save['order_code'] = trim($data['order_code'][0]);            
        if(isset($info['verify'])) {
        	$data_save['order_status'] = 'verify';
        } else {
        	if(isset($info['order_status']))
            	$data_save['order_status'] = trim($info['order_status']);
        }
        if(isset($info['order_pos']))
            $data_save['order_pos']   = trim($info['order_pos']);
        if(isset($info['transport_code']))
            $data_save['transport_code']  = trim($info['transport_code']);
        if(isset($info['transport_name']))
            $data_save['transport_name']   = trim($info['transport_name']);

        $data_save['created_date'] = date("Y-m-d H:i:s", time());

        global $wpdb;
        $table = $wpdb->prefix . 'order_status';

        $wpdb->insert( $table, $data_save );
	}
}
endif;


//die;
add_action( 'updated_post_meta', 'order_update', 10, 4 );