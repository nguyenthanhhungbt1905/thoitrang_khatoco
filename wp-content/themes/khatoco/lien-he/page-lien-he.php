<?php if(!isset($_SESSION)) session_start();
/*
Template Name: Liên hệ
*/

if(isset($_POST['contact_submit'])) {
	$data = array(
		'post_title'   => sanitize_text_field($_POST['contact_subject']),
		'post_content' => sanitize_text_field($_POST['contact_content']),
		'post_type'    => 'contact',
		'post_status'  => 'pending'
	);
	$pid = wp_insert_post($data);
	if(!is_wp_error($pid)) {
		update_post_meta($pid, 'contact_name', sanitize_text_field($_POST['contact_name']));
		update_post_meta($pid, 'contact_email', sanitize_email($_POST['contact_email']));
		update_post_meta($pid, 'contact_phone', wp_strip_all_tags($_POST['contact_phone']));

		$_SESSION['contacted'] = 1;
		
		$email = 'cskh@thoitrangkhatoco.vn';
		$subject = '[Khatoco] '. $_POST['contact_email'] .' gửi tin nhắn đến hệ thống';
		// $content = sanitize_email($_POST['contact_email']) .' '. (isset($_POST['contact_name']) ? '('. sanitize_text_field($_POST['contact_name']) .')' : '') .' đã gửi tin nhắn với nội dung:'. "\n" . sanitize_text_field($_POST['contact_content']);
		$content = sanitize_email($_POST['contact_email']) .' '. (isset($_POST['contact_name']) ? '('. sanitize_text_field($_POST['contact_name']) .')' : '').' đã gửi tin nhắn với nội dung:'."\n".'Số điện thoại: '.(isset($_POST['contact_phone']) ? sanitize_text_field($_POST['contact_phone']) : '')."\n".'Tiêu đề: '.(isset($_POST['contact_subject']) ? sanitize_text_field($_POST['contact_subject']) : '')."\n".'Nội dung: '. sanitize_text_field($_POST['contact_content']);
		wp_mail( $email, $subject, $content );

		wp_redirect(get_permalink());
		exit();
	}
}

get_header(); ?>
<!-- <script type="text/javascript" src="//maps.googleapis.com/maps/api/js?v=3&amp;callback=googleLoadMapCallback&amp;libraries=visualization,drawing,geometry,places&amp;key=AIzaSyA56R0C2n_rs_oJajhK1s_iGffr3zPjjo8&amp;language=vn"></script> -->

<script type="text/javascript" src="//maps.googleapis.com/maps/api/js?v=3&amp;callback=googleLoadMapCallback&amp;libraries=visualization,drawing,geometry,places&amp;key=AIzaSyAVh6TYSf6kJ5s7EKXTL6ecbZmctgzDeFU&amp;language=vn"></script>
 

<div class="block-outer showroom">
	<div class="container fluid clearfix">
		<div class="block-inner">
			<div class="block-title-outer"><h2 class="block-title block-title-inner condensed">Liên Hệ</h2></div>
			<div class="block-content clearfix">
				<div class="col-md-6 col-xs-12">
					<div class="summary-info-inner">
						<h2>Công ty TNHH Thương Mại Khatoco</h2>
						<p class="addr">07 Võ Thị Sáu, phường Vĩnh Nguyên,<br>Thành phố Nha Trang,<br> Khánh Hòa, Việt Nam</p>
						<p class="phone">CSKH 1800 585860</p>
						<p class="phone">Phòng kinh doanh 058.3882230</p>
						<p class="fax">058.3886422</p>
						<p class="email">cskh@thoitrangkhatoco.vn</p>
					</div>
					<div id="map"></div>
				</div>
				<div class="col-md-6 col-xs-12">
						<h2>Thông tin liên hệ</h2>
						<p>Hãy liên hệ với chúng tôi, chúng tôi sẽ trả lời nhanh nhất những gì bạn quan tâm</p>
						<form action="" method="POST" class="clearfix">
							<p><input type="text" name="contact_name" placeholder="Họ và Tên (*)" required=""></p>
							<p><input type="email" name="contact_email" placeholder="Email (*)" required=""></p>
							<p><input type="phone" name="contact_phone" placeholder="Điện thoại (*)" required=""></p>
							<p><input type="text" name="contact_subject" placeholder="Tiêu đề (*)" required=""></p>
							<p><textarea name="contact_content" rows="7" placeholder="Nội dung cần liên hệ (*)" required=""></textarea></p>
							<p><input class="fr" type="submit" name="contact_submit" value="Gửi"></p>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
<div id="popup" class="popup-outer contact-popup <?php if(!$_SESSION['contacted']) echo 'hidden'; ?>">
	<!-- <div class="popup-inner">
		<div class="message-wrap row clearfix">
			<p>Cám ơn bạn đã gửi thư.</p><p>Chúng tôi sẽ kiểm tra và trả lời bạn sớm nhất có thể!</p>
		</div>
		<div class="button-close row clearfix">
			<p><a href="javascript:;" class="close">Đóng lại</a></p>
		</div>
	</div>
 	-->
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        	<h4 class="modal-title">KHATOCO THÔNG BÁO</h4>
	      	</div>
	      	<div class="modal-body">
	        	<p>Cám ơn bạn đã gửi thư.</p><p>Chúng tôi sẽ kiểm tra và trả lời bạn sớm nhất có thể!</p>
	      	</div>
	      	<div class="modal-footer">
	        	<button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
	      	</div>
	    </div>
	  </div>
</div>

<?php get_footer();
$_SESSION['contacted'] = 0; ?>