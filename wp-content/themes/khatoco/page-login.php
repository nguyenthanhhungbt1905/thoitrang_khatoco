<?php
$user_info = wp_get_current_user();
if($user_info->ID > 0){
	wp_redirect(home_url('/'));
	exit;
}

/*
Template Name: Page Login
*/
get_header(); ?>

<div class="block-outer block-account">
	<div class="block-inner">
		<div class="container fluid clearfix">
			<div class="block-title-outer"><h2 class="block-title block-title-inner condensed">Đăng nhập</h2></div>
			<form id="form_login" class="page-account-form account-form" method="POST">
				<p class="clearfix">
					<label for="login_email">Tài khoản</label>
					<input class="field" type="text" id="login_email" name="email" placeholder="Số điện thoại" value="<?php if(isset($_GET['email'])) echo $_GET['email']; ?>" required />
				</p>
				<p class="clearfix">
					<label for="login_password">Mật khẩu</label>
					<input class="field" name="password" id="login_password" placeholder="Mật khẩu" type="password" required />
				</p>	
				<p class="clearfix">
					<button class="button" type="submit">Đăng nhập</button>
					<input type="hidden" name="redirect" value="<?php echo isset($_GET['next_url']) ? rawurldecode($_GET['next_url']) : home_url('/'); ?>" /> 
					<input type="hidden" name="action" value="form_login_ajax" /> 
					<input type="hidden" id="login_nonce_field" name="login_nonce_field" value="<?php echo wp_create_nonce('login_nonce_action'); ?>" />
					<input type="hidden" name="_wp_http_referer" value="/" />
				</p>
				<p id="login_status" class="form-status"></p>
				<p class="page-account-switch">
					<a class="link-switch" href="<?php echo get_page_link(171); ?>">Đăng kí tài khoản mới</a>
				</p>
			</form>

		</div>
	</div>
</div>

<?php get_footer(); ?>