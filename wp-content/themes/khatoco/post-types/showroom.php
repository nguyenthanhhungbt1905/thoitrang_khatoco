<?php 
/* Register Showrooms */
add_action('init', 'posttype_showroom');
function posttype_showroom() {
	$labels = array(
		'name' => 'Đại lý/Showroom',
		'singular_name' => 'Đại lý/Showroom',
		'all_items' => 'Đại lý/Showroom',
		'add_new' => 'Thêm mới',
		'add_new_item' => 'Thêm mới',
		'edit_item' => 'Sửa',
		'new_item' => 'Đại lý/Showroom mới',
		'view_item' => 'Xem chi tiết',
		'search_items' => 'Tìm kiếm',
		'not_found' =>  'No Showroom was found with that criteria',
		'not_found_in_trash' => 'No Showroom found in the Trash with that criteria',
		'view' =>  'Xem'
	);

	$args = array(
		'labels' => $labels,
		'description' => 'This is the holding location for all ecas',
		'public' => false,
		'publicly_queryable' => true,
		'exclude_from_search' => true,
		'show_ui' => true,
        'show_in_nav_menus' => false,
		'rewrite' => true,
		'menu_position' => 15,
		'_builtin' => false,
		'menu_icon' => 'dashicons-location',
		'supports' => array('title', 'thumbnail'),
	);
	register_post_type('showroom', $args);

	register_taxonomy("city", "showroom", array(
        "hierarchical" => true,
        "label" => "Thành phố",
        "singular_label" => "Thành phố",
        "rewrite" => true,
        "show_admin_column" => true,
        'show_in_nav_menus' => false,
    ));
    // flush_rewrite_rules();
}


add_action('restrict_manage_posts', 'admin_showroom_filter_restrict_manage_posts');
function admin_showroom_filter_restrict_manage_posts() {
    $post_type = 'post';
    if(isset($_GET['post_type']))
        $post_type = $_GET['post_type'];

    if($post_type == 'showroom') {
        $terms = array(
            '1' => 'Nhà phân phối',
            '2' => 'Cửa hàng',
        ); ?>
        <select name="s_type">
            <option value="">Cửa hàng/Phân phối</option><?php $current_s = (isset($_GET['s_type'])) ? $_GET['s_type']:'';
            foreach ($terms as $tkey => $term) {
            echo '<option value="'. $tkey .'" '. (($current_s == $tkey) ? 'selected=""':'') .'>'. $term .'</option>'. "\n";
        } ?>
        </select>
    <?php
    }
}

add_filter('parse_query', 'custom_showroom_filter');
function custom_showroom_filter($query) {
    global $pagenow;
    $post_type = 'post';
    if(isset($_GET['post_type']))
        $post_type = $_GET['post_type'];

    if ('showroom' == $post_type && is_admin() && $pagenow == 'edit.php' && isset($_GET['s_type']) && $_GET['s_type'] != '') {
        $query->query_vars['meta_key'] = 'agency_type';
        $query->query_vars['meta_value'] = $_GET['s_type'];
    }
}

add_action('manage_edit-showroom_columns', 'add_column_showroom_type');
function add_column_showroom_type($defaults) {
    global $post;
    $post_type = get_post_type( $post );
    if($post_type = 'showroom') {
        $defaults = array_slice($defaults, 0, 2, true) + array('sh_type' => 'Loại đại lý') + array_slice($defaults, 2, count($defaults)-2, true);
    }
    return $defaults;
}

add_action('manage_posts_custom_column' , 'showroom_type_custom_column', 10, 2);
function showroom_type_custom_column($column, $post_id){
    if(get_post_type( $post_id ) == 'showroom') {
        switch ($column) {
            case 'sh_type':
                $sh_type = get_post_meta($post_id, 'agency_type', true);
                if($sh_type == '1') echo 'Showroom';
                elseif($sh_type == '2') echo 'Điểm bán siêu thị';
                elseif($sh_type == '3') echo 'Đại lý';
                elseif($sh_type == '4') echo 'Gian hàng online';
                else echo '---';
                break;
        }
    } ?>
    <style type="text/css">
    th#like-count{width:5%}
    th#price{width:8%}
    </style>
<?php }

// METABOX
add_filter('rwmb_meta_boxes', 'add_meta_boxes');
function add_meta_boxes() {
    $meta_boxes[] = array(
        'id' => 'agency_id',
        'title' => 'Đại lý',
        'pages' => array('showroom'),
        'context' => 'normal',
        'priority' => 'high',
        'fields' => array(
            array(
                'name' => 'Kiểu đại lý',
                'id' => 'agency_type',
                'type' => 'select',
                'placeholder' => '-- Chọn --',
                'options' => array(
                    '1' => 'Showroom',
                    '2' => 'Điểm bán siêu thị',
                    '3' => 'Đại lý',
                    '4' => 'Gian hàng online',
                )
            ),
            array(
                'name' => 'Địa chỉ cụ thể',
                'desc' => 'Địa chỉ cụ thể',
                'id' => 'agency_address',
                'type' => 'text',
                'std' => ''
            ),
            array(
                'name' => 'Số điện thoại',
                'desc' => 'Số điện thoại liên hệ',
                'id' => 'agency_phone',
                'type' => 'text'
            ),
            array(
                'name' => 'Bản đồ',
                'id' => 'agency_latlng',
                'type' => 'map',
                'address_field' => 'agency_address',
                'style' => 'width: 100%; height: 350px',
                'std' => '12.2458187, 109.1947299, 15',
                'desc' => 'Mặc định: Trần Phú, Lộc Thọ, tp. Nha Trang, Khánh Hòa, Vietnam'
            ),
        )
    );
    return $meta_boxes;
}
?>