<?php 
add_action('init', 'posttype_slide');
function posttype_slide() {
	$labels = array(
		'name'               => 'Banners',
		'singular_name'      => 'Banners',
		'menu_name'          => 'Banners',
		'name_admin_bar'     => 'Banners',
		'add_new'            => 'Thêm ảnh',
		'add_new_item'       => 'Thêm ảnh',
		'new_item'           => 'Thêm ảnh mới',
		'edit_item'          => 'Chỉnh sửa',
		'view_item'          => 'Xem',
		'all_items'          => 'Tất cả ảnh',
		'search_items'       => 'Tìm kiếm',
		'not_found'          => 'No slides found.',
		'not_found_in_trash' => 'No slides found in Trash.'
	);

	$args = array(
		'labels'             => $labels,
		'public'             => false,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'show_in_nav_menus'  => false,
		'query_var'          => false,
		'rewrite'            => true,
		'hierarchical'       => false,
		'menu_position'      => 63,
		'menu_icon'          => 'dashicons-images-alt',
		'supports'           => array('title', 'thumbnail')
	);

	register_post_type('banner', $args);

	register_taxonomy('positions', 'banner', array(
		'label' => 'Vị trí',
		'singular_label' => 'Vị trí',
		'show_in_nav_menus' => false,
		'show_admin_column' => true,
		'hierarchical' => true
	));
}

add_action('init', 'remove_thumbnail_support_for_post');
function remove_thumbnail_support_for_post() {
	remove_post_type_support( 'post', 'thumbnail' );
}

add_action( 'do_meta_boxes', 'slides_change_meta_boxes' );
function slides_change_meta_boxes() {
	remove_meta_box( 'postimagediv', 'banner', 'side' );
	add_meta_box( 'postimagediv', 'Thêm ảnh', 'post_thumbnail_meta_box', 'banner', 'normal', 'high' );
}

add_filter('manage_edit-banner_columns', 'post_banner_columns_head');
function post_banner_columns_head($defaults) {
	$defaults = array_slice($defaults, 0, 2, true) + array('thumbnail' => 'Ảnh') + array_slice($defaults, 2, count($defaults)-2, true);
	return $defaults;
}

add_action('manage_banner_posts_custom_column' , 'post_banner_custom_column', 10, 2);
function post_banner_custom_column($column, $post_id){
	switch ($column) {
		case 'thumbnail':
			echo get_the_post_thumbnail( $post_id, array(300, 100) );
			break;
	}
}

add_action('admin_init', 'add_banner_metabox');
function add_banner_metabox($post){
	add_meta_box(
        'metabox_banner_general',
        'Thông tin chung',
        'display_metabox_banner_general',
        'banner',
        'normal',
        'low'
    );

    function display_metabox_banner_general($post) {
		$post_id = $post->ID;
		$kind = get_post_meta($post_id, 'kind', true);
		$link = get_post_meta($post_id, 'link', true);
		$img_title = get_post_meta($post_id, 'img_title', true);
		$text = get_post_meta($post_id, 'text', true); ?>
		<div class="wrap banner-wrap">
			<p><label for="kind"><strong>Loại</strong></label><select name="kind" id="kind">
				<option value="-1">--Chọn--</option>
				<option value="banner" <?php selected($kind, 'banner'); ?>>Banner</option>
				<option value="advert" <?php selected($kind, 'advert'); ?>>Quảng cáo</option>
			</select></p>
			<p><label for="link"><strong>Link (optional)</strong></label><input type="text" class="regular-text" name="link" id="link" value="<?php echo $link; ?>"></p>
			<p><label for="img_title"><strong>Image title (optional)</strong></label><input type="text" class="regular-text" name="img_title" id="img_title" value="<?php echo $img_title; ?>"></p>
			<p>
				<label for="text"><strong>Text (optional)</strong></label>
			</p>
			<?php wp_editor($text, 'text', array('media_buttons' => false, 'textarea_rows' => 5)); ?>
		</div>
		<div class="clear"></div>
		<style>
		.banner-wrap label{width:200px;float:left}
		#titlediv div.inside{display:none}
		</style>
<?php }
}

add_action('save_post', 'save_banner_metabox');
function save_banner_metabox($post_id) {
	if(get_post_type() == 'banner'){
        if(isset($_POST['kind'])){
            update_post_meta($post_id, 'kind', $_POST['kind']);
        }

        if(isset($_POST['link'])){
            update_post_meta($post_id, 'link', $_POST['link']);
        }

        if(isset($_POST['img_title'])){
            update_post_meta($post_id, 'img_title', $_POST['img_title']);
        }

        if(isset($_POST['text'])){
            update_post_meta($post_id, 'text', $_POST['text']);
        }
	}
}

/*list banner images*/
function get_banner_at($bannerPosition){
	$sliderImages = get_posts(array(
		'post_type' => 'banner',
		'posts_per_page' => -1,
		'positions' => $bannerPosition,
		'meta_key' => 'kind',
		'meta_value' => 'banner'
	));

	$htmlBanner = '<ul class="slides clearfix">';
	foreach ($sliderImages as $sliderImage) {
		$slide_id = $sliderImage->ID;
		$image = wp_get_attachment_image_src( get_post_thumbnail_id( $slide_id ), 'full' );
		$link = get_post_meta($slide_id, 'link', true);
		$img_title = get_post_meta($slide_id, 'img_title', true);
		$text = get_post_meta($slide_id, 'text', true);

		$htmlBanner .= '<li>';
		if($link) {
			$htmlBanner .= '<a href="'. $link .'"><img nopin="nopin" src="'.$image[0].'" title="'. $img_title .'"></a>';
		}
		else {
			$htmlBanner .= '<img nopin="nopin" src="'.$image[0].'" title="'. $img_title .'">';
		}
		if($text) {
			$htmlBanner .= '<div class="intro clearfix">'. apply_filters('the_content', $text) .'</div>';
		}
		$htmlBanner .= '</li>';
	}
	$htmlBanner .= '</ul>';

	echo $htmlBanner;
}

function get_advertisement_at($advertPosition) {
	$advertImages = get_posts(array(
		'post_type' => 'banner',
		'posts_per_page' => -1,
		'positions' => $advertPosition,
		'meta_key' => 'kind',
		'meta_value' => 'advert'
	));

	$htmlAdvert = '';
	foreach ($advertImages as $advertImage) {
		$slide_id = $advertImage->ID;
		$image = wp_get_attachment_image_src( get_post_thumbnail_id( $slide_id ), 'full' );
		$link = get_post_meta($slide_id, 'link', true);
		$img_title = get_post_meta($slide_id, 'img_title', true);

		if($link) {
			$htmlAdvert .= '<a href="'. $link .'"><img nopin="nopin" src="'.$image[0].'" title="'. $img_title .'" rel="index,follow"></a>';
		}
		else {
			$htmlAdvert .= '<img nopin="nopin" src="'.$image[0].'" title="'. $img_title .'" rel="index,follow">';
		}
	}

	echo $htmlAdvert;
}