<?php 
// Register new post type: Style Guide
add_action('init', 'posttype_style_guide');
function posttype_style_guide() {
	$labels = array(
		'name' => 'Style Guide',
		'singular_name' => 'Style Guide',
		'all_items' => 'Tất cả',
		'add_new' => 'Thêm mới',
		'add_new_item' => 'Thêm mới',
		'edit_item' => 'Chỉnh sửa',
		'new_item' => 'Style guide mới',
		'view_item' => 'Xem chi tiết',
		'search_items' => 'Tìm kiếm',
		'not_found' =>  'Không tìm thấy bài đăng nào',
		'not_found_in_trash' => 'Không tìm thấy bài đăng nào trong thùng rác',
		'view' =>  'Xem'
	);

	$args = array(
		'labels' => $labels,
		'description' => '',
		'public' => true,
		'exclude_from_search' => false,
		'publicly_queryable' => true,
		'show_in_nav_menus' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'rewrite' => true,
		'has_archive' => false,
		'menu_position' => 5,
		'menu_icon' => 'dashicons-star-filled',
		'supports' => array('title', 'thumbnail', 'editor'),
	);
	register_post_type('style-guide', $args);

	register_taxonomy('style-guide-category', 'style-guide', array(
		'hierarchical' => true,
		'label' => 'Danh mục Style Guide',
		'singular_label' => 'Danh mục Style Guide',
		'rewrite' => array('slug' => 'thoi-trang', 'hierarchical' => true),
		// 'show_ui' => false,
		'show_admin_column' => true,
	));
	// flush_rewrite_rules();
}

add_action('restrict_manage_posts', 'admin_style_guide_filter_restrict_manage_posts');
function admin_style_guide_filter_restrict_manage_posts() {
	$post_type = 'post';
	if(isset($_GET['post_type']))
		$post_type = $_GET['post_type'];

	if($post_type == 'style-guide') {
		$terms = get_terms('style-guide-category', array('hide_empty' => 0)); ?>
		<select name="filter_by_series">
			<option value="">Danh mục</option><?php $current_s = (isset($_GET['filter_by_series'])) ? $_GET['filter_by_series']:'';
			foreach ($terms as $tkey => $term) {
			echo '<option value="'. $term->slug .'" '. (($current_s == $term->slug) ? 'selected=""':'') .'>'. $term->name .'</option>'. "\n";
		} ?>
		</select>
	<?php
	}
}

add_filter('parse_query', 'custom_style_guide_filter');
function custom_style_guide_filter($query) {
	global $pagenow;
	$post_type = 'post';
	if(isset($_GET['post_type']))
		$post_type = $_GET['post_type'];

	if ('style-guide' == $post_type && is_admin() && $pagenow == 'edit.php' && isset($_GET['filter_by_series']) && $_GET['filter_by_series'] != '') {
		$query->query_vars['style-guide-category'] = $_GET['filter_by_series'];
	}
}

// Remove style-guide-categorydiv meta box in style-guide post edit
add_action('admin_menu', 'remove_style_guide_category_div', 999);
function remove_style_guide_category_div() {
	remove_meta_box('style-guide-categorydiv', 'style-guide', 'advanced');
}

// Add meta boxes for page template
add_action('add_meta_boxes', 'khatoco_style_guide_metabox');
function khatoco_style_guide_metabox() {
	add_meta_box('khatoco_style_guide_metabox', 'Chi tiết style guide (Enable Javascript Required)', 'khatoco_style_guide_callback', 'style-guide', 'normal', 'high');
}

add_action('add_meta_boxes', 'khatoco_style_guide_homepage_display');
function khatoco_style_guide_homepage_display() {
    add_meta_box('khatoco_style_guide_short_description', 'Short Description', 'khatoco_style_guide_short_description_callback', 'style-guide', 'normal', 'high');
    add_meta_box('khatoco_style_guide_homepage_display', 'Hiển thị ở trang chủ', 'khatoco_style_guide_homepage_display_callback', 'style-guide', 'normal', 'high');
}
function khatoco_style_guide_short_description_callback($post){
    $style_guide_template = get_post_meta($post->ID);
    $content = null;
    if(isset($style_guide_template['style_guide_short_description']))
        $content = $style_guide_template['style_guide_short_description'][0];
    ?>
<!--    <label style="margin-left: 20px;margin-top: 10px;">Short description: </label>-->


    <textarea name="style_guide_short_description" style="width:100% !important; min-height:80px !important;"  ><?php echo $content ?></textarea>
    <?php

}
function khatoco_style_guide_homepage_display_callback($post){
    $taxonomy = 'style-guide-category';
    $style_guide_template = get_post_meta($post->ID);
    $selected = 2;
    if(isset($style_guide_template['style_guide_homepage_display'])){
        $selected = $style_guide_template['style_guide_homepage_display'][0];
    }
    ?>
<!--    <label style="margin-left: 20px;margin-top: 10px;">Homepage display: </label>-->
    <select name="style-guide-homepage-display">
        <option <?php if($selected == 1){ echo "selected "; } ?> value="1">Có</option>
        <option <?php if($selected == 2){ echo "selected "; } ?> value="2">Không</option>
    </select>
    <?php

}

add_action( 'save_post_style-guide', 'save_homepage_display' );
add_action( 'save_post_style-guide', 'save_short_description' );
function save_short_description($post_id){
    if ( ! isset( $_POST['style_guide_short_description'] ) ) {
        return;
    }
    $my_data = $_POST['style_guide_short_description'];

    // Update the meta field in the database.

    update_post_meta( $post_id, 'style_guide_short_description', $my_data );

}
function save_homepage_display($post_id){
    if ( ! isset( $_POST['style-guide-homepage-display'] ) ) {
        return;
    }
    $my_data = $_POST['style-guide-homepage-display'];

    // Update the meta field in the database.

    update_post_meta( $post_id, 'style_guide_homepage_display', $my_data );

}

function khatoco_style_guide_callback($post) {
	$tax = 'style-guide-category';
	$selected = '';

	wp_nonce_field('khatoco_style_guide_metabox', 'khatoco_style_guide_metabox_nonce');
	$target_saved = get_post_meta($post->ID, 'style_guide_save', true);

	if(is_null($target_saved) || empty($target_saved)) $target_saved = array(); ?>
	<input type="hidden" name="do" value="style_guide_savepost">
	<input type="hidden" name="target_save" value="" id="target_save"><?php $terms = get_terms($tax, array('orderby' => 'id', 'order' => 'ASC', 'hide_empty' => false, 'parent' => 0));
	$current_term = get_the_terms($post->ID, $tax);
	$current_term = $current_term[0];
	
	echo '<p><label style="margin-top: 10px;">Danh mục: </label><select name="tax_select" id="tax_select"><option value="-1" data-tax-ref="" data-tax-slug="">Chọn một danh mục để tiếp tục</option>';
	foreach($terms as $term) {
		if($term->term_id == $current_term->term_id)
			$selected = 'selected';
		else $selected = '';
		echo '<option value="'. $term->term_id .'" data-tax-ref="'. $term->slug .'_content" data-tax-slug="#'. $term->slug .'" '. $selected .'>'. $term->name .'</option>';
	}
	echo '</select>'; ?>
	
	<?php
		$style_guide_template = get_post_meta($post->ID, 'style_guide_template', true);
	?>
	<label style="margin-left: 20px;margin-top: 10px;">Style guide template: </label>
	<select name="style-guide-template" id="style-guide-template">
		<option data-tax-ref="xu-huong-thoi-trang_content" data-tax-slug="#xu-huong-thoi-trang" value="xu-huong-thoi-trang" <?= $style_guide_template =='xu-huong-thoi-trang' ? 'selected="selected"':'' ?> >Xu hướng thời trang</option>
		<option data-tax-ref="meo-hay-cho-phong-cach_content" data-tax-slug="#meo-hay-cho-phong-cach" value="meo-hay-cho-phong-cach" <?= $style_guide_template =='meo-hay-cho-phong-cach' ? 'selected="selected"':'' ?>>Mẹo hay phong cách</option>
		<option data-tax-ref="hinh-tuong-thoi-trang_content" data-tax-slug="#hinh-tuong-thoi-trang" value="hinh-tuong-thoi-trang" <?= $style_guide_template =='hinh-tuong-thoi-trang' ? 'selected="selected"':'' ?>>Hình tượng thời trang</option>
		<option data-tax-ref="cham-soc-trang-phuc_content" data-tax-slug="#cham-soc-trang-phuc" value="cham-soc-trang-phuc" <?= $style_guide_template =='cham-soc-trang-phuc' ? 'selected="selected"':'' ?>>Chăm sóc trang phục</option>
	</select></p>


	<div id="popup-image">
		<img src="#" alt="">
	</div>

	<div id="xu-huong-thoi-trang" class="tax_select_inner hidden">
		<?php wp_editor($post->post_content, 'xu_huong_thoi_trang_content', array('media_buttons' => true, 'textarea_row' => 50)); ?>
		<h3><strong>Chọn những sản phẩm phù hợp</strong></h3>
		<p><label for="disable-preview-1"><input type="checkbox" name="" id="disable-preview-1" class="disable-preview"><strong>Tắt chức năng xem ảnh sản phẩm</strong></label></p>
		<hr>
		<ul class="clearfix" style="max-height: 500px;overflow-y: auto;"><?php $all_products = get_posts(array('posts_per_page' => -1));
		foreach($all_products as $p) {
			$pid = $p->ID;
			$checked = (in_array($pid, $target_saved)) ? 'checked' : '';
			$thumb = get_post_thumbnail_id($pid);
			$thumb = wp_get_attachment_image_src($thumb, 'thumbnail');
			$thumb = (isset($thumb[0])) ? $thumb[0]:'';

			if($thumb == '') {
				$product_colors = get_post_meta($pid, 'post_colors', true);
				$thumb = isset($product_colors[0]['thumb']) ? $product_colors[0]['thumb']:'';
			}

			echo '<li id="post-'. $pid .'" class="product-item" data-thumb="'. $thumb .'">
				<label for="in-post-'. $pid .'" class="selectit"><input type="checkbox" name="xu_huong_thoi_trang[posts][]" id="in-post-'. $pid .'" value="'. $pid .'" '. $checked .'>'. $p->post_title .'</label> - 
				<a href="'. admin_url('/') .'post.php?post='. $pid .'&action=edit' .'" title="Chi tiết" target="_blank">Chi tiết sản phẩm</a> - 
				<a href="'. get_permalink($pid) .'" title="Xem" target="_blank">Xem sản phẩm</a>
			</li>';
		} ?>
		</ul>
	</div>

	<div id="meo-hay-cho-phong-cach" class="tax_select_inner hidden">
		<h3><strong>Nguyên tắc, lý giải, giới thiệu về mẹo</strong></h3>
		<?php wp_editor($post->post_content, 'meo_hay_cho_phong_cach_content', array('media_buttons' => true, 'textarea_row' => 50)); ?>
		<h3><strong>Chọn những sản phẩm phù hợp</strong></h3>
		<p><label for="disable-preview-2"><input type="checkbox" name="" id="disable-preview-2" class="disable-preview">Tắt chức năng xem ảnh sản phẩm</label></p>
		<hr>
		<ul class="clearfix" style="max-height: 500px;overflow-y: auto;"><?php $all_products = get_posts(array('posts_per_page' => -1));
		foreach($all_products as $p) {
			$pid = $p->ID;
			$checked = (in_array($pid, $target_saved)) ? 'checked' : '';
			$thumb = get_post_thumbnail_id($pid);
			$thumb = wp_get_attachment_image_src($thumb, 'thumbnail');
			$thumb = (isset($thumb[0])) ? $thumb[0]:'';

			echo '<li id="post-'. $pid .'" class="product-item" data-thumb="'. $thumb .'">
				<label for="at-post-'. $pid .'" class="selectit"><input type="checkbox" name="meo_hay_cho_phong_cach[posts][]" id="at-post-'. $pid .'" value="'. $pid .'" '. $checked .'>'. $p->post_title .'</label> - 
				<a href="'. admin_url('/') .'post.php?post='. $pid .'&action=edit' .'" title="Chi tiết" target="_blank">Chi tiết sản phẩm</a> - 
				<a href="'. get_permalink($pid) .'" title="Xem" target="_blank">Xem sản phẩm</a>
			</li>';
		} ?>
		</ul>
	</div>

	<div id="hinh-tuong-thoi-trang" class="tax_select_inner hidden">
		<h3><strong>Giới thiệu về hình tượng</strong></h3>
		<?php wp_editor($post->post_content, 'hinh_tuong_thoi_trang_content', array('media_buttons' => true, 'textarea_row' => 50)); ?>
	</div>

	<div id="cham-soc-trang-phuc" class="tax_select_inner hidden">
		<?php wp_editor($post->post_content, 'cham_soc_trang_phuc_content', array('media_buttons' => true, 'textarea_row' => 50)); ?>
	</div>

	<style type="text/css">
	.tax_select_inner h3{ padding: 10px 0 !important; border: 0 !important; cursor: default !important }
	.hidden{ display: none !important }
	#popup-image{position:fixed;top:100px;left:170px;z-index:10000;display:none;border:7px solid rgba(0,0,0,0.7);border-radius:5px;max-width: 250px}
	#popup-image img{width: 100%}
    #postdivrich{
        display: none !important;
    }
	</style>

	<script type="text/javascript">
	jQuery(document).ready(function($) {
		var tab_show = jQuery('#style-guide-template').find('option:selected').attr('data-tax-slug');
		var tabRef = jQuery('#style-guide-template').find('option:selected').attr('data-tax-ref').split('-').join('_');
		if(tab_show != '' || typeof tab_show != 'undefined') {
			jQuery(tab_show).removeClass('hidden');
			jQuery('#target_save').val(tab_show.substr(1));
		}
		else {
			jQuery('tax_select_inner').addClass('hidden');
			jQuery('#target_save').val('');
		}

		jQuery('#style-guide-template').on('change', function(event) {
			var tax_id = jQuery(this).val();
			var tax_slug = jQuery(this).find('option:selected').attr('data-tax-slug');
            tabRef = jQuery('#style-guide-template').find('option:selected').attr('data-tax-ref').split('-').join('_');

            //handle editor for yoast seo
            setTimeout(function () {
                var fromEditor = tinyMCE.get(tabRef)
                var contentEditor = tinyMCE.get('content')
                contentEditor.setContent(fromEditor.getContent())
                console.log(contentEditor.getContent())
                fromEditor.onChange.add(function (ed, e) {
                    var newContent = ed.getContent()
                    contentEditor.setContent(newContent);
                })
            }, 500)


			if(tax_id != '-1') {
				jQuery('#target_save').val(tax_slug.substr(1));
				jQuery('.tax_select_inner').addClass('hidden');
				jQuery(tax_slug).removeClass('hidden');
			}
			else {
				alert('Vui lòng chọn một danh mục để tiếp tục!');
				return false;
			}
		});

		jQuery('.product-item').hover(function() {
			if(jQuery(this).parents('.tax_select_inner').find('.disable-preview').is(':checked'))
				return false;
			var src = jQuery(this).attr('data-thumb');
			jQuery('#popup-image img').attr('src', src);
			jQuery('#popup-image').show();
		}, function() {
			jQuery('#popup-image img').attr('src', '#');
			jQuery('#popup-image').hide();
		});



		//handle editor
        setTimeout(function () {
            var fromEditor = tinyMCE.get(tabRef)
            var contentEditor = tinyMCE.get('content');
            fromEditor.onChange.add(function (ed, e) {
                var newContent = ed.getContent()
                contentEditor.setContent(newContent);
            })
        }, 500)
	});
	</script>
<?php 
}


//add action save post
add_action( 'save_post_style-guide', 'wpse63478_save' );
function wpse63478_save($pid){

	$style_guide_template = get_post_meta($pid, 'style_guide_template', true);
	if($style_guide_template){
		update_post_meta($pid, 'style_guide_template', $_POST['style-guide-template']);
	}else{
		add_post_meta($pid, 'style_guide_template', $_POST['style-guide-template']);//save template type
	}

	if($_POST['style-guide-template'] == 'cham-soc-trang-phuc' || $_POST['style-guide-template'] == 'hinh-tuong-thoi-trang'){//remove all relatest product
		update_post_meta($pid, 'style_guide_save', null);
	}


}


add_action( 'wp_ajax_ajax_load_more_style_guide', 'ajax_load_more_style_guide_func' );
add_action( 'wp_ajax_nopriv_ajax_load_more_style_guide', 'ajax_load_more_style_guide_func' );



function ajax_load_more_style_guide_func(){
    $paged = isset($_POST['ajax_paged'])?intval($_POST['ajax_paged']):'';
    if($paged <= 0 || !$paged || !is_numeric($paged)) wp_send_json_error('Paged?');

    $tax = $_POST['tax'];
    $args = array('posts_per_page' => 6, 'post_type' => 'style-guide', $tax => $_POST['term_slug'], 'paged' => $_POST['ajax_paged'], 'post_status' => 'publish', 'orderby' => 'date',
        'order' => 'DESC');
    $styleGuides = new WP_Query($args);
    if($styleGuides->have_posts()) {
        ob_start();
        while($styleGuides->have_posts()):
            $styleGuides->the_post();
            $excerpt = wp_trim_words(apply_filters('the_content', get_the_content()), 15); ?>

            <div class="item col-lg-4 col-sm-6 col-xs-12 mobile-nopadding"> <!-- item-animate -->
                <div class="item-outer clearfix">
                    <div class="item-inner">
                        <a class="item-thumb" href="<?php echo the_permalink(); ?>">
                            <?php if(has_post_thumbnail()) the_post_thumbnail('full'); ?>
                        </a>
                        <div class="item-info">
                            <h2 class="item-title"><a href="<?php echo the_permalink(); ?>"><?php the_title(); ?></a></h2>
                            <p class="item-excerpt"><?php echo $excerpt; ?></p>
                        </div>
                    </div>
                </div>
            </div>
<?php
        endwhile;
        wp_reset_query();
        $content = ob_get_clean();
        wp_send_json_success($content);
        wp_die();
    }else{
        wp_send_json_error('no_post');
        wp_die();
    }

}


function getAllStyleGuide(){
    global $wpdb;
    $wp_ = $wpdb->prefix;

    $query = "SELECT {$wp_}posts.ID as 'id', {$wp_}posts.post_title as 'title', short_desc.meta_value as 'short_description' FROM {$wp_}posts 
LEFT JOIN {$wp_}postmeta `display_home` ON {$wp_}posts.ID = display_home.post_id AND display_home.meta_key = 'style_guide_homepage_display'
LEFT JOIN {$wp_}postmeta `short_desc` ON {$wp_}posts.ID = short_desc.post_id AND short_desc.meta_key = 'style_guide_short_description'
WHERE {$wp_}posts.post_type = 'style-guide' AND {$wp_}posts.post_status ='publish' AND display_home.meta_value = 1
" ;
//    var_dump($query);die;
    $result              = $wpdb->get_results($query, ARRAY_A);
    if($result){
        foreach ($result as $key => $postItem) {
            $url = wp_get_attachment_url( get_post_thumbnail_id($postItem['id']), 'thumbnail' );
            $link = get_permalink($postItem['id']);
            $result[$key]['feature_image'] = $url;
            $result[$key]['link'] = $link;
        }
    }
    return $result;
}

function get_style_guide_category()
{
    global $wpdb;
    $wp_ = $wpdb->prefix;
    $query = "SELECT {$wp_}terms.term_id as 'id',{$wp_}terms.name as 'title',{$wp_}terms.slug as 'slug',term_taxonomy.description as 'description' FROM {$wp_}terms
    LEFT JOIN {$wp_}term_taxonomy `term_taxonomy` ON term_taxonomy.term_id = {$wp_}terms.term_id
    WHERE term_taxonomy.taxonomy = 'style-guide-category' AND term_taxonomy.parent = '0' ORDER BY {$wp_}terms.term_id ASC
    ";
    $results = $wpdb->get_results($query,ARRAY_A);
    foreach ($results as $key => $singleItem) {
        $term_url = get_term_link($singleItem['slug'], 'style-guide-category');
        $banner_image = wp_get_attachment_image_src(get_single_taxonomy_meta('cat-description', $singleItem['id'], 'cat-banner-img'), array(562, 310));
        $img_src = wp_get_attachment_image_src(get_single_taxonomy_meta('cat-description', $singleItem['id'], 'cat-bg-img'), array(562, 310));
        $results[$key]['image_preview'] = $img_src[0];
        $results[$key]['link'] = $term_url;
        $results[$key]['banner_image'] = $banner_image[0];
    }
    return $results;
}
function get_limit_with_category($category, $limit){
    global $wpdb;
    $wp_ = $wpdb->prefix;
    $query = "SELECT {$wp_}posts.ID as 'id',{$wp_}posts.post_title as 'title', post_meta.meta_value as 'short_description'
    FROM {$wp_}posts
    LEFT JOIN {$wp_}term_relationships `term_rela` ON term_rela.object_id = {$wp_}posts.ID
    LEFT JOIN {$wp_}term_taxonomy `term_taxonomy` ON term_rela.term_taxonomy_id = term_taxonomy.term_taxonomy_id
    LEFT JOIN {$wp_}terms `terms` ON terms.term_id = term_taxonomy.term_id
    LEFT JOIN {$wp_}postmeta `post_meta` ON post_meta.post_id = {$wp_}posts.ID AND post_meta.meta_key = 'style_guide_short_description'
    WHERE {$wp_}posts.post_type = 'style-guide' and {$wp_}posts.post_status = 'publish' AND terms.term_id = '{$category}'  ORDER BY {$wp_}posts.ID DESC LIMIT {$limit}
    ";
    $result = $wpdb->get_results($query,ARRAY_A);
    foreach ($result as $key => $postItem) {
        $url = wp_get_attachment_url( get_post_thumbnail_id($postItem['id']), 'thumbnail' );
        $link = get_permalink($postItem['id']);
        $result[$key]['feature_image'] = $url;
        $result[$key]['link'] = $link;
    }
    return $result;
}

function getSingleStyleGuide(){
    global $wpdb;
    global $post;
    $postId = $post->ID;
    $wp_ = $wpdb->prefix;
    $query = "SELECT {$wp_}posts.ID as 'id',{$wp_}posts.post_content as 'content', {$wp_}posts.post_title as 'title',terms.name as 'category_name',terms.slug as 'slug' FROM {$wp_}posts
    LEFT JOIN {$wp_}term_relationships `term_relation` ON {$wp_}posts.ID = term_relation.object_id
    LEFT JOIN {$wp_}term_taxonomy `term_taxonomy` ON term_relation.term_taxonomy_id = term_taxonomy.term_taxonomy_id
    LEFT JOIN {$wp_}terms `terms` ON terms.term_id = term_taxonomy.term_id
    WHERE {$wp_}posts.ID = '{$postId}' AND {$wp_}posts.post_status = 'publish' AND {$wp_}posts.post_type = 'style-guide'and term_taxonomy.taxonomy = 'style-guide-category'
";
    $result = $wpdb->get_results($query,ARRAY_A);
    return $result;
}

function get_related_style_guide($termSlug)
{
    global $wpdb;
    $wp_    = $wpdb->prefix;
    $query  = "SELECT {$wp_}posts.ID as 'id', {$wp_}posts.post_title as 'title', short_desc.meta_value as 'short_description' FROM {$wp_}posts 
LEFT JOIN {$wp_}postmeta `short_desc` ON {$wp_}posts.ID = short_desc.post_id AND short_desc.meta_key = 'news_short_description'
LEFT JOIN {$wp_}term_relationships `term_rela` ON {$wp_}posts.ID = term_rela.object_id
LEFT JOIN {$wp_}term_taxonomy `term_taxonomy` ON term_rela.term_taxonomy_id = term_taxonomy.term_taxonomy_id
LEFT JOIN {$wp_}terms `terms` ON terms.term_id = term_taxonomy.term_id
WHERE {$wp_}posts.post_type = 'style-guide' AND {$wp_}posts.post_status ='publish' AND terms.slug = '{$termSlug}' ORDER BY {$wp_}posts.ID DESC LIMIT 6 OFFSET 0
";
    $result = $wpdb->get_results($query, ARRAY_A);
    foreach ($result as $key => $postItem) {
        $url                           = wp_get_attachment_url(get_post_thumbnail_id($postItem['id']), 'thumbnail');
        $link                          = get_permalink($postItem['id']);
        $result[$key]['feature_image'] = $url;
        $result[$key]['link']          = $link;
    }

    return $result;
}