<?php

/* Register Coupon post-type */
add_action('init', 'posttype_coupon');
function posttype_coupon() {
	$labels = array(
		'name' => 'Mã giảm giá',
		'singular_name' => 'Mã giảm giá',
		'all_items' => 'Tất cả các mã',
		'add_new' => 'Thêm mã',
		'add_new_item' => 'Thêm mã',
		'edit_item' => 'Sửa mã giảm giá',
		'new_item' => 'Mã giảm giá mới',
		'view_item' => 'Xem chi tiết',
		'search_items' => 'Tìm kiếm',
		'not_found' =>  'No Result was found with that criteria',
		'not_found_in_trash' => 'No Result found in the Trash with that criteria',
		'view' =>  ''
	);

	$args = array(
		'labels' => $labels,
		'description' => 'This is the holding location for all ecas',
		'public' => false,
		'publicly_queryable' => true,
		'exclude_from_search' => true,
		'show_ui' => true,
		'show_in_nav_menus' => false,
		'show_in_admin_bar' => false,
		'rewrite' => false,
		'has_archive' => false,
		'menu_position' => 60,
		'menu_icon' => 'dashicons-tickets-alt',
		'supports' => array('title'),
	);

	register_post_type('coupon', $args);
}

add_action('admin_init', 'add_metabox_coupon');
function add_metabox_coupon() {
	function display_metabox_coupon_general($post) {
		$post_id = $post->ID;
		
		$coupon_code = get_post_meta($post_id, 'coupon_code', true); // Ma coupon
		$coupon_quota = intval( get_post_meta($post_id, 'coupon_quota', true) ); // Số lần sử dụng của coupon
		$coupon_used_time = get_post_meta($post_id, 'coupon_used_time', true);

		if( $coupon_used_time == '' )
			$coupon_used_time = 0;
		else
			$coupon_used_time = intval( $coupon_used_time );

		$coupon_time_use = get_post_meta($post_id, 'coupon_time_use', true); // So lan duoc phep su dung coupon cua 1 user
		$coupon_users_only = get_post_meta($post_id, 'coupon_users_only', true); // Pick users only
		$coupon_login_required = get_post_meta($post_id, 'coupon_login_required', true);
		if($coupon_login_required == '') $coupon_login_required = 0;

		$coupon_kind = get_post_meta($post_id, 'coupon_kind', true); // Kieu giam gia cua coupon (% hay -)
		if(!$coupon_kind) $coupon_kind = array(0, 0);

		$coupon_users_filter = get_post_meta($post_id, 'coupon_users_filter', true);
		$coupon_users_filter_key = isset($coupon_users_filter['keys']) ? $coupon_users_filter['keys'] : '';
		$coupon_users_filter_value = isset($coupon_users_filter['values']) ? $coupon_users_filter['values'] : '';
        
        echo '$coupon_users_filter_key : <br>';
        print_r($coupon_users_filter_key);
        
		$coupon_time_select = get_post_meta($post_id, 'coupon_time_select', true); // Moc thoi gian hay khoang thoi gian
		$fromto = $range = array('', '');
		$$coupon_time_select = get_post_meta($post_id, $coupon_time_select, true); // Gia tri thoi gian cu the
		if(!$$coupon_time_select) $$coupon_time_select = array('', ''); ?>

		<div class="wrap clearfix">
			<table class="form-table coupon-admin-table">
				<tbody>
					<tr>
						<th><label for="code">Mã giảm giá</label></th>
						<td>
							<input type="text" id="code" name="coupon_code" value="<?php echo $coupon_code; ?>" required="">
							&nbsp;hoặc&nbsp;
							<button class="button generate-coupon">Tự động tạo mã giảm giá</button>
							<input type="number" id="coupon_length" min="6" value="6" placeholder="Độ dài" style="width:50px">
							<span class="generate-coupon-load spinner"></span>
						</td>
					</tr>
					<tr>
						<th><label for="kind">Kiểu giảm giá</label></th>
						<td>
							<select name="kind_select" id="kind_select">
								<option value="0" <?php selected($coupon_kind[0], 0); ?>>Giảm giá theo phần trăm đơn hàng</option>
								<option value="1" <?php selected($coupon_kind[0], 1); ?>>Giảm giá trực tiếp đơn hàng</option>
							</select>
							<input type="number" id="kind" min="0" name="coupon_kind" value="<?php echo $coupon_kind[1]; ?>">
							<span class="unit"><?php echo ($coupon_kind[0] == 0) ? '%':'VND'; ?></span>
						</td>
					</tr>
							<script>
							var kindInit = '<?php echo $coupon_kind[0]; ?>';
							</script>
					<tr class="limit-decrease-row">
						<th><label for="kind">Mức giảm trần</label></th>
						<td>
                            <?php
                                $limit_decrease = get_post_meta($post_id, 'limit_decrease',true);
                                $limit_decrease = empty($limit_decrease) ?0:intval($limit_decrease);
                            ?>
							<input type="number" id="limit_decrease" min="0" name="limit_decrease" value="<?php echo $limit_decrease; ?>">
							<span class="unit">VNĐ</span>
						</td>
					</tr>

					<tr>
						<th><label for="login_required">KH đăng nhập</label></th>
						<td>
							<input type="checkbox" name="coupon_login_required" id="login_required" value="1" <?php checked($coupon_login_required, 1); ?>>
						</td>
					</tr>
					<tr>
						<th><label for="quota">Tổng số lần sử dụng</label></th>
						<td>
							<input type="number" id="quota" name="coupon_quota" value="<?php echo $coupon_quota; ?>">
							<?php if($coupon_used_time > 0 && $coupon_used_time < $coupon_quota ){
								echo '<input type="hidden" id="used_time" name="coupon_used_time" value="'. $coupon_used_time .'" readonly="">';
								echo ' <span>Còn '. ($coupon_quota - $coupon_used_time) .' lần sử dụng!</span>';
							}
							elseif( $coupon_used_time >= $coupon_quota )
								echo ' <span>Coupon đã được sử dụng hết!</span>'; ?>
						</td>
					</tr>
					<?php  ?>
					<tr>
						<th><label for="time_use">Số lần sử dụng/KH</label></th>
						<td>
							<input type="number" id="time_use" name="coupon_time_use" min="1" value="<?php echo $coupon_time_use; ?>">
						</td>
					</tr>
					<?php  ?>
					<tr>
						<th><label for="time_select_event">Thời gian sử dụng</label></th>
						<td>
							<select name="time_select_event" id="time_select_event">
								<option value="fromto" <?php selected($coupon_time_select, 'fromto'); ?>>Mốc thời gian (từ ... đến ...)</option>
								<option value="range" <?php selected($coupon_time_select, 'range'); ?>>Khoảng thời gian (... ngày/tuần/tháng)</option>
							</select>

							<div class="clear"></div>

							<div id="time_select_event_fromto" class="time_select_event">
								<label for="date_start">Từ thời điểm </label>
								<input type="text" class="datetime_picker" name="fromto[0]" id="date_start" value="<?php echo $fromto[0]; ?>">
								<label for="date_end"> đến </label>
								<input type="text" class="datetime_picker" name="fromto[1]" id="date_end" value="<?php echo $fromto[1]; ?>">
							</div>

							<div id="time_select_event_range" class="time_select_event">
								<label for="date_start">Khoảng thời gian </label>
								<input type="number" id="time_amount" min="1" name="range[0]" value="<?php echo $range[0]; ?>">
								<?php $time_select = array(
									1 => 'Ngày',
									7 => 'Tuần',
									30 => 'Tháng',
									120 => 'Quý',
								); ?>
								<select name="range[1]" id="time_unit"><?php foreach ($time_select as $tkey => $time) {
									echo '<option value="'. $tkey .'" '. selected($range[1], $tkey, false) .'>'. $time .'</option>';
								} ?>
								</select>
								&nbsp;từ thời điểm&nbsp;Đăng ký
							</div>
						</td>
					</tr>
					<tr>
						<th><label for="login_required">Đối tượng đăng nhập</label><br><small>không chọn nếu áp dụng với tất cả khách hàng</small></th>
						<td>
							<select name="coupon_users_filter" id="coupon_users_filter" style="display:block;margin-bottom:10px">
                                <?php $_mark_selected_filter = false; ?>
                                
								<option value="" <?php if (empty($coupon_users_filter_key)) { 
                                    echo 'selected';
                                    $_mark_selected_filter = true;
                                } ?>>Chọn đối tượng</option>
                                
								<option value="card" 
                                    <?php if (!$_mark_selected_filter)  selected( $coupon_users_filter_key, 'card' ); ?>>
                                    Hạng thẻ
                                </option>
                                
								<option value="birth" 
                                        <?php if (!$_mark_selected_filter) selected( $coupon_users_filter_key, 'birth' ); ?>>
                                    Ngày sinh
                                </option>
                                
								<option value="gender" 
                                        <?php if (!$_mark_selected_filter) selected( $coupon_users_filter_key, 'gender' ); ?>>
                                    Giới tính
                                </option>
							</select>
							<?php $selected_card = array(
								'0' => 'Không thẻ',
								'1' => 'Thẻ Bạc',
								'2' => 'Thẻ Vàng',
								'3' => 'Thẻ Kim cương',
							); ?>
							<select name="users_filter_card[]" id="filter_by_card" class="filter-by-des" multiple=""><?php foreach($selected_card as $ckey => $cvalue) {
								$_select = '';
								if( $coupon_users_filter_key == 'card' && !empty( $coupon_users_filter_value ) ){
									$_select = in_array($ckey, $coupon_users_filter_value) ? 'selected=""':'';
								}
								echo '<option value="'. $ckey .'" '. $_select .'>'. $cvalue .'</option>';
							} ?></select>

							<?php $selected_birth = array(
                                '0'  => 'Không sử dụng',
								'01' => 'Tháng 01',
								'02' => 'Tháng 02',
								'03' => 'Tháng 03',
								'04' => 'Tháng 04',
								'05' => 'Tháng 05',
								'06' => 'Tháng 06',
								'07' => 'Tháng 07',
								'08' => 'Tháng 08',
								'09' => 'Tháng 09',
								'10' => 'Tháng 10',
								'11' => 'Tháng 11',
								'12' => 'Tháng 12',
							); ?>
							<select name="users_filter_birth[]" id="filter_by_birth" class="filter-by-des" multiple=""><?php
                            foreach($selected_birth as $bkey => $bvalue) {
								$_select = '';
								if( $coupon_users_filter_key == 'birth' && !empty( $coupon_users_filter_value ) ){
									$_select = in_array($bkey, $coupon_users_filter_value) ? 'selected=""':'';
								}
								echo '<option value="'. $bkey .'" '. $_select .'>'. $bvalue .'</option>';
							} ?></select>

							<?php $selected_gender = array(
								'0' => 'Nữ',
								'1' => 'Nam',
                                '2' => 'Không sử dụng'
							); ?>
							<select name="users_filter_gender[]" id="filter_by_gender" class="filter-by-des" multiple=""><?php
                            foreach($selected_gender as $gkey => $gvalue) {
								$_select = '';
								if( $coupon_users_filter_key == 'gender' && !empty( $coupon_users_filter_value ) ){
									$_select = in_array($gkey, $coupon_users_filter_value) ? 'selected=""':'';
								}
								echo '<option value="'. $gkey .'" '. $_select .'>'. $gvalue .'</option>';
							} ?></select>
							<div class="clear"></div>
                            
							<select name="coupon_users_only[]" id="coupon_users_only" multiple="multiple" 
                                    class="<?php if (!empty($coupon_users_filter_key)) echo 'no-users-select';?>">
							<?php $users = get_users(array('role' => 'subscriber'));
							foreach ($users as $ukey => $user) {
								$select = (in_array(strval($user->ID), $coupon_users_only)) ? ' selected':'';
								$crm_code = get_user_meta( $user->ID, 'code_crm', true ) ? get_user_meta( $user->ID, 'code_crm', true ):'WEB'. str_pad($user->ID, 9, '0', STR_PAD_LEFT);
								$phone = (get_user_meta( $user->ID, 'user_phone', true ) != '') ? ' - '. get_user_meta( $user->ID, 'user_phone', true ):'';
								$user_info = $crm_code . $phone;
							 	echo '<option value="'. $user->ID .'"'. $select .'>'. $user_info .'</option>';
							} ?>
							</select>
                            <div class='error-coupon' id='error-coupon' style="display:none"></div>
						</td>
					</tr>
					<!-- <div class="btn btn-primary click-import" style="display: none;">Click import</div> -->
				</tbody>
			</table>
		</div>

		<style>
		#metabox_coupon_general p{margin: 13px 0}
		#metabox_coupon_general label{font-weight: bold; display: inline-block; margin-right: 5px;}
		#metabox_coupon_general p label{min-width: 100px}
		#metabox_coupon_general .time_select_event label{vertical-align: top; margin-top: 5px}
		#metabox_coupon_general input{vertical-align: bottom}
		#metabox_coupon_general .generate-coupon-load, #metabox_coupon_general .filter-by-des{display: none}
		#metabox_coupon_general .filter-by-des{width: 120px;}
		.select2-selection, .select2-dropdown{min-width: 150px;}
		.form-table th{vertical-align: middle;}
        .no-users-select + span { display: none; }
		#message, #edit-slug-box, #post-preview{display: none}
		</style>
	<?php 
    script_coupon_backend();
    }

	add_meta_box(
		'metabox_coupon_general',
		'Thông tin mã giảm giá',
		'display_metabox_coupon_general',
		'coupon',
		'normal',
		'high'
	);
}

function script_coupon_backend(){ ?>
    <script src="<?php echo get_template_directory_uri();?>/assets/admin/js/coupon.js"></script>
<?php
}


add_action('save_post', 'custom_coupon_save');
// $arr = ["72AE5C99","17F702AA"];

function custom_coupon_save( $post_id ) {

	if ( wp_is_post_revision( $post_id ) )
		return;

	// Coupon code
	if ( isset( $_POST['coupon_code'] ) ) {
		if(check_coupon_available($_POST['coupon_code']) && $_POST['original_publish'] == 'Publish'){
			wp_die( 'Mã coupon này đã tồn tại!', 'Lỗi coupon' );
			wp_delete_post( $post_id, true );
			exit();
		}
		
		update_post_meta( $post_id, 'coupon_code', sanitize_text_field( $_POST['coupon_code'] ) );
		// for($i = 0;$i<count($arr);$i++){
		// 	update_post_meta( $post_id, 'coupon_code', sanitize_text_field( $arr[$i] ) );
		// }
	}

	if ( isset( $_POST['coupon_login_required'] ) ) {
		update_post_meta( $post_id, 'coupon_login_required', 1 );
	}
	else{
		update_post_meta( $post_id, 'coupon_login_required', 0 );
	}

	// coupon_quota
	if ( isset( $_POST['coupon_quota'] ) ) {
		update_post_meta( $post_id, 'coupon_quota', $_POST['coupon_quota'] );
		if( !isset( $_POST['coupon_used_time'] ) )
			update_post_meta( $post_id, 'coupon_used_time', intval(0) );
	}

	// Users picked only
	if ( isset( $_POST['coupon_users_only'] ) ) {
		update_post_meta( $post_id, 'coupon_users_only', $_POST['coupon_users_only'] );
	}
	else
        update_post_meta( $post_id, 'coupon_users_only', array() );
    
	if(isset($_POST['coupon_users_filter'])){
        $by = $_POST['coupon_users_filter'];
        
        // If admin select 'members' list - we will set user_filter = null ~ user_filter = coupon_users_only
        if (empty($by)) :
            update_post_meta( $post_id, 'coupon_users_filter', null);
        else :
            if( isset($_POST['users_filter_'. $by]) ){
                $temp = array(
                    'keys' => $by,
                    'values' => $_POST['users_filter_'. $by],
                );
                update_post_meta( $post_id, 'coupon_users_filter', $temp );
                unset( $temp );
            }
        endif;
    } 
    else 
        update_post_meta( $post_id, 'coupon_users_filter', null);
	

	// Kind
	if ( isset( $_POST['kind_select'] ) && isset( $_POST['coupon_kind'] ) ) {
		update_post_meta( $post_id, 'coupon_kind', array($_POST['kind_select'], $_POST['coupon_kind']) );
	}

	// Date
	// var_dump($_POST);exit();
	if ( isset( $_POST['time_select_event'] ) ) {
		$pre = $_POST['time_select_event'];
		update_post_meta( $post_id, 'coupon_time_select', $pre );

		if ( isset( $_POST[$pre] ) ) {
			update_post_meta( $post_id, $pre, $_POST[$pre] );
		}
	}

	if( isset( $_POST['coupon_time_use'] ) ) {
        update_post_meta( $post_id, 'coupon_time_use', $_POST['coupon_time_use'] );
    }

    if( isset( $_POST['limit_decrease'] ) ) {
        update_post_meta( $post_id, 'limit_decrease', $_POST['limit_decrease'] );
    }
}
add_action('wp_ajax_insertCoupon','insertCoupon');
add_action('wp_ajax_nopriv_insertCoupon', 'insertCoupon');
function insertCoupon(){
	$coupons = [
		
	];
	foreach ($coupons as $key => $coupon) {
		$arr = array(
			'post_title' => $coupon,
			'post_author' => get_current_user_id(),
			'post_type' => 'coupon',
			'post_status' => 'publish'
		);

		$post_id = wp_insert_post($arr);

		update_post_meta( $post_id, 'coupon_code',$coupon );
		update_post_meta( $post_id, 'coupon_login_required', '1' );
		update_post_meta( $post_id, 'coupon_time_use','1' );
		update_post_meta( $post_id, 'coupon_quota', '1');
		update_post_meta( $post_id, 'limit_decrease', '500000' );
		update_post_meta( $post_id, 'coupon_time_select', 'fromto' );
		update_post_meta( $post_id, 'fromto', array('2017/10/01','2017/11/30' ));
		update_post_meta( $post_id, 'coupon_kind', array('0','20') );
		echo the_ID() . '<br/>';
	}
	die;
}

// insertCoupon($coupons);

?>

