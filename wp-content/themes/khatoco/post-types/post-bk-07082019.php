<?php
require_once(get_template_directory() . '/includes/product/ProductUtils.php');
require_once(__DIR__).'/product/export.php';
function get_default_thumb_link($postID)
{
    $product_colors = get_post_meta($postID, 'post_colors', true);
    $images_ids = explode(',', $product_colors[0]['images_ids']);
    $images_ids = $images_ids[0];
    $images_ids = wp_get_attachment_image_src($images_ids, array(350, 1000));
    return $images_ids[0];
}


/* Register Taxonomy */
add_action('init', 'reg_post_taxonomy');

function reg_post_taxonomy()
{

    register_taxonomy("store", "post", array(
        "hierarchical" => true,
        'public' => false,
        "label" => "Cửa hàng",
        "singular_label" => "Cửa hàng",
        "rewrite" => array('slug' => 'cua-hang'),
        'show_in_nav_menus' => false,
        "show_admin_column" => false
    ));
    // flush_rewrite_rules();
}

// Manage post filter
add_action('restrict_manage_posts', 'admin_post_filter_restrict_manage_posts');

function admin_post_filter_restrict_manage_posts()
{
    $post_type = 'post';
    if (isset($_GET['post_type']))
        $post_type = $_GET['post_type'];

    if ($post_type == 'post') {
        ?>
        <input type="text" class="" placeholder="Mã SKU" name="code_sku"
               value="<?php echo isset($_GET['code_sku']) ? $_GET['code_sku'] : ''; ?>">
        <style>
            .wp-list-table th#code,
            .wp-list-table th#color_code {
                width: 8%;
            }

            .wp-list-table th#tags,
            .wp-list-table th#date,
            .wp-list-table th#price {
                width: 7%;
            }

            .wp-list-table th#onhand {
                width: 5%;
            }

            .wp-list-table th#categories {
                width: 10%;
            }
        </style>
        <?php
    }
}

add_action('parse_query', 'custom_post_filter_by_sku');

function custom_post_filter_by_sku($wp_query)
{
    global $pagenow;
    $post_type = 'post';
    if (isset($_GET['post_type']))
        $post_type = $_GET['post_type'];

    if (!is_admin())
        return;

    if ('post' == $post_type && $pagenow == 'edit.php' && isset($_GET['code_sku']) && $_GET['code_sku'] != '') {
        $wp_query->query_vars['meta_key'] = 'post_sku';
        $wp_query->query_vars['meta_value'] = $_GET['code_sku'];
        $wp_query->query_vars['meta_compare'] = 'LIKE';
    }

    if ('post' == $post_type && $pagenow == 'edit.php' && isset($_GET['orderby']) && $_GET['orderby'] == 'post_onhand_all') {
        $wp_query->query_vars['orderby'] = 'meta_value_num';
        $wp_query->query_vars['meta_key'] = $_GET['orderby'];
        $wp_query->query_vars['order'] = strtoupper($_GET['order']);
    }
}

// Manage post column
add_filter('manage_pages_columns', 'remove_comment_count_columns');
add_action('manage_edit-post_columns', 'remove_comment_count_columns');

function remove_comment_count_columns($defaults)
{
    global $post;
    $post_type = get_post_type($post);
    if ($post_type == 'post') {
        $defaults = array_slice($defaults, 0, 3, true) + array('code' => 'SKU', 'color_code' => 'Mã màu', 'price' => 'Giá <br> (Khuyến mãi)', 'onhand' => 'SL Tồn') + array_slice($defaults, 3, count($defaults) - 3, true);
        $defaults = array_slice($defaults, 0, 10, true) + array('like-count' => 'Like') + array_slice($defaults, 10, count($defaults) - 10, true);
        unset($defaults['author']);
    }
    unset($defaults['comments']);
    return $defaults;
}

add_action('manage_posts_custom_column', 'post_like_count_custom_column', 10, 2);

function post_like_count_custom_column($column, $post_id)
{
    if (get_post_type($post_id) == 'post') {
        switch ($column) {
            case 'code':
                $sku = get_post_meta($post_id, 'post_sku', true);
                echo '<p><strong style="color:#655">' . $sku . '</strong></p>';
                break;
            case 'color_code':
                $colors = get_post_meta($post_id, 'post_colors', true);
                echo '<p><strong style="color:#655">' . $colors[0]['color_id'] . '</strong></p>';
                break;
            case 'onhand':
//                $onhands = get_post_meta($post_id, 'post_onhand_all', true);
                $totalOnhands = array_sum(get_post_meta($post_id, 'post_onhand',true));
                echo $totalOnhands;
                break;
            case 'price':
                $sale = get_post_meta($post_id, 'sale_off', true);
                $price = get_post_meta($post_id, 'post_price', true);
                if ($price == '')
                    $price = 0;
                echo '<p><span style="color:#655">' . number_format($price, 0, ',', '.') . 'đ</span><br>';
                if ($sale != '' && $sale != '0')
                    echo '<strong>(' . number_format($price * (100 - $sale) / 100, 0, ',', '.') . 'đ)</strong></p>';
                break;
            case 'like-count':
                $like_count = get_post_meta($post_id, 'like_count', true);
                echo ($like_count == '') ? '0' : $like_count;
                break;
        }
    }
    ?>
    <?php
}

add_filter("manage_edit-post_sortable_columns", "post_sortable_columns");

function post_sortable_columns()
{
    return array(
        'price' => 'price',
        'onhand' => 'post_onhand_all',
        // 'like-count' => 'like_count',
    );
}

add_action('save_post', 'save_metabox_of_post');

function save_metabox_of_post($post_id)
{
    if (wp_is_post_revision($post_id))
        return;

    if (get_post_type() == 'post' && isset($_POST['do']) && $_POST['do'] == 'post_metabox') {
        //Sku
        if (isset($_POST['post_sku'])) {
            update_post_meta($post_id, 'post_sku', $_POST['post_sku']);
        }

        // Store ID
        if (isset($_POST['store_id'])) {
            update_post_meta($post_id, 'store_id', $_POST['store_id']);
        }

        //unit
        if (isset($_POST['unit'])) {
            update_post_meta($post_id, 'post_unit', $_POST['unit']);
        }

        //Price
        if (isset($_POST['post_price'])) {
            update_post_meta($post_id, 'post_price', $_POST['post_price']);
        }

        //Sale off
        if (isset($_POST['post_sale']) && isset($_POST['post_sale_off'])) {
            if ($_POST['post_sale_off'])
                update_post_meta($post_id, 'sale_off', $_POST['post_sale']);
            else
                update_post_meta($post_id, 'sale_off', '0');
        } else {
            update_post_meta($post_id, 'sale_off', '0');
        }

        //Weight
        if (isset($_POST['post_weight'])) {
            update_post_meta($post_id, 'post_weight', $_POST['post_weight']);
        }

        //is Hot
        if (isset($_POST['post_is_hot'])) {
            update_post_meta($post_id, 'post_is_hot', 1);
        } else {
            update_post_meta($post_id, 'post_is_hot', 0);
        }

        //Sizes
        if (isset($_POST['post_sizes'])) {
            update_post_meta($post_id, 'post_sizes', $_POST['post_sizes']);
        }

        if (isset($_POST['post_onhand'])) {
            update_post_meta($post_id, 'post_onhand', $_POST['post_onhand']);
            update_post_meta($post_id, 'post_onhand_all', array_sum($_POST['post_onhand']));
        }

        if (isset($_POST['stores_with_onhand'])) {
            update_post_meta($post_id, 'stores_with_onhand', $_POST['stores_with_onhand']);
        }

        //Category Ids
        if (isset($_POST['post_category_ids']) && !empty($_POST['post_category_ids'])) {
            $all_cats = $_POST['post_category_ids'];
            // safeoff or not
            if (isset($_POST['post_sale_off']) && !empty($_POST['post_sale_off']))
                array_push($all_cats, 111);
            // new products?
            if (isset($_POST['post_is_new'])) {
                array_push($all_cats, 112);
                update_post_meta($post_id, 'post_is_new', 1);
            } else {
                update_post_meta($post_id, 'post_is_new', 0);
            }

            wp_set_post_categories($post_id, $all_cats, false);
            update_post_meta($post_id, 'post_category_ids', $_POST['post_category_ids']);
        }

        //Colors
        if (isset($_POST['color'])) {
            $colors = array();
            foreach ($_POST['color'] as $post_color) {
                if ($post_color['color_id']) {
                    if (!$post_color['name']) {
                        $post_color['name'] = $post_color['color_id'];
                    }
                    if (!$post_color['code']) {
                        $post_color['code'] = '#C61D20'; // default color
                    }
                    if (!$post_color['images_ids']) {
                        $post_color['images_ids'] = '';
                    }

                    array_push($colors, $post_color);
                    update_post_meta($post_id, 'post_color_id', $post_color['color_id']);
                    break; // save first colors
                }
            }
            update_post_meta($post_id, 'post_colors', $colors);
        }

        // More info
        if (isset($_POST['post_info'])) {
            update_post_meta($post_id, 'post_info', $_POST['post_info']);
        }
        if (isset($_POST['post_delivery'])) {
            update_post_meta($post_id, 'post_delivery', $_POST['post_delivery']);
        }
        if (isset($_POST['post_preservation'])) {
            update_post_meta($post_id, 'post_preservation', $_POST['post_preservation']);
        }
        if (isset($_POST['post_wrap'])) {
            update_post_meta($post_id, 'post_wrap', $_POST['post_wrap']);
        }

        // Accessories
        if (isset($_POST['acs']) && !empty($_POST['acs'])) {
            $sa = array();
            foreach ($_POST['acs'] as $aid) {
                array_push($sa, $aid);
            }
            update_post_meta($post_id, 'post_accessories', $sa);
        }
    }
}

add_action('wp_ajax_backend__sync_product_once', 'backend__sync_product_once_cb');

function backend__sync_product_once_cb()
{
    $id = $_POST['id'];
    $results = array(
        'result' => 0,
        'message' => 'Đồng bộ thất bại. Vui lòng thử lại trong ít phút',
    );

    $result = sync_update_products($id);

    if ($result) {
        $results = array(
            'result' => 1,
            'message' => 'Đồng bộ thành công!',
        );
    }

    echo json_encode($results);
    exit;
}

/* Meta Boxes */
add_action('admin_init', 'add_metabox_post');

function add_metabox_post()
{

    function display_metabox_sync($post)
    {
        ?>
        <div id="sync_popup"><span class="spinner"></span></div>
        <p>
            <button type="button" id="sync" class="button">Sync this product</button>
        </p>
        <p id="sync_message"></p>

        <script type="text/javascript" charset="utf-8">
            jQuery(document).ready(function ($) {
                $('#sync').live('click' , function (event) {
                    event.preventDefault();

                    var container = $('#sync_message');
                    container.html('<span class="spinner" style="visibility: visible; float: none;"></span>');

                    $.ajax({
                        url: '<?php echo admin_url("admin-ajax.php"); ?>',
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            action: 'backend__sync_product_once',
                            id: <?php echo $post->ID; ?>
                        }
                    })
                        .done(function (response) {
                            $('#sync_message').text(response.message);
                            if (response.result) {
                                location.reload();
                            }
                        });

                });
            });
        </script>
        <?php
    }

    add_meta_box(
        'metabox_sync', 'Sync', 'display_metabox_sync', 'post', 'side', 'high'
    );

    /* Metabox - General */

    function display_metabox_post_general($post)
    {

        function aj_wp_dropdown_categories($parentId, $currentCategoryId, $taxonomy = 'category', $selectId = '')
        {
            $args = array(
                'show_option_all' => '--Lựa chọn--',
                'show_option_none' => '',
                'orderby' => 'name',
                'order' => 'ASC',
                'show_count' => 0,
                'hide_empty' => 0,
                'child_of' => $parentId,
                'exclude' => '111,112',
                'echo' => 1,
                'selected' => $currentCategoryId,
                'hierarchical' => 1,
                'name' => 'taxonomy[' . $taxonomy . '][' . $parentId . ']',
                'id' => $selectId,
                'class' => 'custom-select',
                'depth' => 0,
                'tab_index' => 0,
                'taxonomy' => $taxonomy,
                'hide_if_empty' => false,
                'walker' => ''
            );
            wp_dropdown_categories($args);
        }

        $post_id = $post->ID;
        $price = (int)get_post_meta($post_id, 'post_price', true);
        if (!$price)
            $price = '';
        $sku = get_post_meta($post_id, 'post_sku', true);

        $sale_off = get_post_meta($post_id, 'sale_off', true);
        $campaign = check_product_is_saleoff_by_campaign($post_id);
        if ($campaign && $campaign['type'] == 'product') {
            $sale_off = (int)$campaign['sale']['number'];
            $sale_off_note = 'Sản phẩm nằm trong chiến dịch khuyến mãi <strong>' . $campaign['name'] . '</strong>';
        }

        $weight = get_post_meta($post_id, 'post_weight', true);
        $is_hot = get_post_meta($post_id, 'post_is_hot', true);
        $is_new = get_post_meta($post_id, 'post_is_new', true);
        $sizes = get_post_meta($post_id, 'post_sizes', true);
        $onhands = get_post_meta($post_id, 'post_onhand', true);

        $stores_with_onhand = get_post_meta($post_id, 'stores_with_onhand', true);

        $unit = get_post_meta($post_id, 'post_unit', true);

        //$categories = wp_get_post_categories($post_id, array('fields' => 'ids'));
        $categories = get_post_meta($post_id, 'post_category_ids', true);
        ?>
        <input type="hidden" name="do" value="post_metabox"/>
        <input type="hidden" name="unit" id="unit" value="<?php echo $unit; ?>"/>
        <table class="form-table">
            <tbody>
            <tr>
                <th scope="row"><label for="post_sku">Mã sản phẩm (SKU)</label></th>
                <td><input name="post_sku" type="text" id="post_sku" value="<?php if (isset($sku)) echo $sku; ?>"
                           class="regular-text"/></td>
            </tr>
            <tr>
                <th scope="row"><label for="post_price">Giá tiền</label></th>
                <td><input name="post_price" type="number" id="post_price"
                           value="<?php if (isset($price)) echo $price; ?>" class="regular-text"/> (VNĐ)
                </td>
            </tr>
            <tr>
                <th scope="row"><label for="post_sale">Khuyến mại</label></th>
                <td>
                    <p style="margin: 3px 0 13px"><input type="checkbox" name="post_sale_off"
                                                         id="post_sale_off" <?php if ($sale_off != '' && $sale_off != '0') echo 'checked=""'; ?>>
                    </p>
                    <div id="has-sale"
                         class="hidden" <?php if ($sale_off != '' && $sale_off != '0') echo 'style="display:block"'; ?>>
                        <input name="post_sale" type="number" id="post_sale"
                               value="<?php if ($sale_off != '' && $sale_off != '0') echo $sale_off;
                               else echo '0'; ?>" min="0" max="100" class="regular-text"/> (%)
                        <p>Giá trị sau khuyến mãi: <span id="after_saleoff"
                                                         style="font-weight: bold"><?php echo $price * ((100 - $sale_off) / 100) . ' VNĐ'; ?></span>
                        </p>
                        <p><?php echo isset($sale_off_note) ? $sale_off_note : ''; ?></p>
                        <p><i style="font-size: 12px">0 nếu sản phẩm không khuyến mãi! Dùng phím mũi tên để xem sự thay
                                đổi giá!</i></p>
                    </div>
                </td>
            </tr>
            <tr>
                <th scope="row"><label for="post_price">Khối lượng</label></th>
                <td><input name="post_weight" type="number" id="post_weight"
                           value="<?php if (isset($weight)) echo $weight; ?>" class="regular-text"/> (Grams)
                </td>
            </tr>
            <tr>
                <th scope="row"><label for="category[category][0]">Loại sản phẩm</label></th>
                <td id="select_categories"></td>
            </tr>
            <tr id="product_sizes">
                <th scope="row"><label for="size">Kích thước</label></th>
                <td>
                    <div><?php
                        $sizes = $sizes != '' ? $sizes : array();

                        $all_shirt_sizes = aj_get_all_shirt_sizes();
                        echo '<div id="shirt_sizes" class="all-sizes" style="width: 45%; float: left;">';
                        foreach ($all_shirt_sizes as $sk => $sv) {
                            $checked = in_array($sk, $sizes) ? 'checked' : '';
                            echo '<p>';
                            echo '<label><input type="checkbox" class="size-class" id="size-' . $sk . '" name="post_sizes[' . $sk . ']" value="' . $sk . '" ' . $checked . '> ' . $sv . ( (isset($onhands[$sk]) ) ? ' <br>Còn : <input type="number" style="width:50px" value="' . $onhands[$sk] . '" name="post_size_available" data-size-available="'.$sk.'"> sản phẩm' : '') . '</label>';
                            echo '<input type="hidden" class="onhand-item" name="post_onhand[' . $sk . ']" value="' . (isset($onhands[$sk]) ? $onhands[$sk] : 0) . '">';
                            echo '<input type="hidden" class="stores" name="stores_with_onhand[' . $sk . ']" value="' . (isset($stores_with_onhand[$sk]) ? $stores_with_onhand[$sk] : '') . '">';
                            echo '</p>';
                        }
                        echo '</div>';

                        $all_pants_sizes = aj_get_all_pants_sizes();
                        echo '<div id="pants_sizes" class="all-sizes" style="width: 45%; float: left;">';
                        foreach ($all_pants_sizes as $sk => $sv) {
                            $checked = in_array($sk, $sizes) ? 'checked' : '';
                            echo '<p>';
                            echo '<label><input type="checkbox" class="size-class" id="size-' . $sk . '" name="post_sizes[' . $sk . ']" value="' . $sk . '" ' . $checked . '> ' . $sv . ((isset($onhands[$sk]) ) ? ' <br>Còn : <input type="number" style="width:50px" value="' . $onhands[$sk] . '" name="post_size_available" data-size-available="'.$sk.'"> sản phẩm' : '') . '</label>';
                            echo '<input type="hidden" class="onhand-item" name="post_onhand[' . $sk . ']" value="' . (isset($onhands[$sk]) ? $onhands[$sk] : 0) . '">';
                            echo '<input type="hidden" class="stores" name="stores_with_onhand[' . $sk . ']" value="' . (isset($stores_with_onhand[$sk]) ? $stores_with_onhand[$sk] : '') . '">';
                            // echo '<input type="hidden" name="post_buid['. $sk .']" value="'. (isset( $buid[$sk] ) ? $buid[$sk] : 0) .'">';
                            echo '</p>';
                        }
                        echo '</div>';
                        ?>
                    </div>
                </td>
            </tr>
            <tr>
                <th scope="row"><label for="post_is_hot">Sản phẩm nổi bật</label></th>
                <td><input name="post_is_hot" type="checkbox" id="post_is_hot"
                           value="1" <?php if (isset($is_hot) && $is_hot) echo 'checked'; ?> /></td>
            </tr>
            <tr>
                <th scope="row"><label for="post_is_new">Sản phẩm mới</label></th>
                <td><input name="post_is_new" type="checkbox" id="post_is_new"
                           value="1" <?php if (isset($is_new) && $is_new) echo 'checked'; ?> /></td>
            </tr>
            </tbody>
        </table>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                // Change post_on_hand - update to hidden field
                jQuery('input[name="post_size_available"]').on('change', function(){
                    var is = jQuery(this);
                    var size = is.data('size-available');
                    var onhands = is.val();

                    if (onhands === 'undefined' || onhands=='' || onhands < 0){
                        onhands = 0;
                    }

                    jQuery('input[name="post_onhand['+size+']"]').val(onhands);
                });

                // Saleoff toogle
                jQuery('#post_sale_off').change(function (event) {
                    jQuery('#has-sale').toggle(this.checked);
                });
                // Sale
                jQuery('#post_sale').on('change', function (event) {
                    var price = parseInt(jQuery('#post_price').val());
                    if (typeof price == 'undefined')
                        alert('Nhập giá sản phẩm trước!');
                    var _this_val = parseInt(jQuery(this).val());
                    if (typeof _this_val == 'undefined')
                        _this_val = 0;
                    var after = price * ((100 - _this_val) / 100);
                    jQuery('#after_saleoff').text(after + ' VNĐ');
                });
                <?php
                if (!empty($categories) && $categories[0] > 0) {
                    $level = 0;

                    echo "createCategorySelect(0, {$categories[0]}, {$level});";
                    foreach ($categories as $key => $c) {
                        $level++;
                        if (isset($categories[$key + 1]) && $categories[$key + 1] != '') {
                            echo "createCategorySelect({$c}, {$categories[$key + 1]}, {$level});";
                        } else {
                            if ($categories[$key] != '')
                                echo "createCategorySelect({$categories[$key]}, 0, {$level});";
                        }
                    }
                } else {
                    echo "createCategorySelect(0, 0, 0);";
                }
                ?>
            });

            function createCategorySelect(parent_id, selected_id, level) {

                if (level > 0) {
                    jQuery('.select-level-' + level).remove();
                    child_class = 'select-is-child';
                    level_class = '';
                    for (var i = 0; i <= level; i++) {
                        level_class += ' select-level-' + i;
                    }
                    ;
                } else {
                    child_class = 'select-is-root';
                    level_class = 'select-level-0';
                }

                jQuery.ajax({
                    url: "<?php echo admin_url('admin-ajax.php'); ?>",
                    dataType: "json",
                    async: false,
                    type: "POST",
                    cache: true,
                    data: {
                        action: 'get_category_by_parent_ajax',
                        parent_id: parent_id,
                        selected_id: selected_id,
                        level: level
                    },
                    success: function (data) {
                        if (data.success == 'true') {
                            select = jQuery('<select name="post_category_ids[' + level + ']" class="select-extend ' + child_class + ' ' + level_class + '"></select>');
                            select.html(data.html);
                            select.on("change", function (evt) {
                                var category_id = jQuery(this).val();
                                if (category_id > 0) {
                                    createCategorySelect(category_id, 0, level + 1, level_class + ' select-level-' + (level + 1));
                                } else {
                                    jQuery('.select-level-' + (level + 1)).remove();
                                }
                            });
                            select.appendTo(jQuery('#select_categories'));
                        } else {

                        }
                    }
                });
            }
        </script>
        <?php
    }

    add_meta_box(
        'metabox_post_general', 'Thông tin chung', 'display_metabox_post_general', 'post', 'normal', 'low'
    );

    // Product colors
    function display_metabox_post_colors($post)
    {
        $post_id = $post->ID;

        $colors = get_post_meta($post_id, 'post_colors', true);

        if (!empty($colors)) {
            foreach ($colors as $kc => $c) {
                $colorsJSON[$kc] = $c;
                $colors_images_ids = explode(',', $c['images_ids']);
                foreach ($colors_images_ids as $ki => $img_id) {
                    $colorsJSON[$kc]['images'][$ki]['id'] = $img_id;
                    $colorsJSON[$kc]['images'][$ki]['url'] = aj_get_image_src($img_id);
                }
            }
        } else {
            $colorsJSON = array();
        }
        ?>
        <table id="color_list" class="form-table">
            <tbody>
            <tr class="color-more" id="color-more">
                <th scope="row">
                    <button type="button" class="button color-button-more">Thêm ảnh</button>
                </th>
                <td>&nbsp;</td>
            </tr>
            </tbody>
        </table>

        <script type="text/javascript">
            jQuery(document).ready(function () {

                //Load Files
                var colorJSON = '<?php echo json_encode($colorsJSON); ?>';
                colorJSON = JSON.parse(colorJSON);
                //console.log(colorJSON);

                if (colorJSON) {
                    var count = 0;
                    jQuery.each(colorJSON, function (i, item) {
                        createColorRow(i + 1, item.name, item.color_id, item.code, item.images, item.images_ids);
                        count++;
                        if (count > 0) {
                            jQuery('#color_list .color-button-more').hide();
                            return false;
                        }
                    });
                }

                //Upload
                jQuery('#color_list').delegate('.color-button-upload', 'click', function (event) {
                    var _this = jQuery(this);
                    var spinner = _this.parents('tr.color-row').find('.spinner');
                    var _row = _this.parents('tr.color-row').attr('data-row');
                    _row = parseInt(_row);

                    spinner.show();

                    var file_frame = wp.media.frames.file_frame = wp.media({
                        title: 'Select image',
                        library: {},
                        button: {text: 'Select'},
                        //frame: 'post',
                        multiple: true
                    });

                    file_frame.on('select', function () {
                        var attachment_ids = [];
                        attachment = file_frame.state().get('selection').toJSON();
                        imgs_html = '';
                        jQuery.each(attachment, function (index, item) {
                            attachment_ids.push(item.id);

                            var item_url = item.url;
                            jQuery.ajax({
                                url: '<?php echo admin_url("admin-ajax.php"); ?>',
                                async: false,
                                cache: false,
                                type: 'POST',
                                dataType: 'json',
                                data: {action: 'get_thumb_src', attid: item.id},
                            })
                                .done(function (data) {
                                    item_url = data.src;
                                });

                            if (index == 0) {

                                imgs_html += '<input type="hidden" name="color[' + _row + '][thumb]" value="' + item_url + '"/>';
                            }

                            imgs_html += '<img data-img-id="' + item.id + '" src="' + item_url + '" />';
                        });

                        var _this_parent = _this.parents('td');
                        _this_parent.find('.color-images-ids').val(attachment_ids.join(','));
                        _this_parent.find('.color-images div').html(imgs_html);
                        spinner.hide();
                    });

                    file_frame.open();
                });

                //Delete
                jQuery('#color_list').delegate('.color-button-dismiss', 'click', function (event) {
                    var _this = jQuery(this);
                    var _this_row = _this.parents('.color-row');
                    _this_row.fadeOut(600);
                    setTimeout(function () {
                        _this_row.remove();
                        resetRowNumber();
                    }, 650);
                });

                //Add
                jQuery('#color_list').find('.color-button-more').click(function (evt) {
                    var totalRow = jQuery('#color_list').find('.color-row').size();
                    jQuery(this).hide();
                    createColorRow(totalRow + 1, '', '', '', null, '');
                });
            });

            //Create each row
            function createColorRow(number, name, color_id, code, images, images_ids) {
                console.log(number, name, color_id, code, images, images_ids);
                var fileRowHtml = '';

                var thisRow = jQuery('<tr class="color-row" data-row="' + number + '"></tr>');

                fileRowHtml += '<th scope="row">';
                fileRowHtml += '<label class="color-label" for="post_sku">Màu #' + number + ':</label>&nbsp&nbsp';
                //fileRowHtml +=         '<input name="color['+number+'][name]" type="text" value="'+name+'" placeholder="Tên màu" class="regular-text color-name" />';
                fileRowHtml += '</th>';
                fileRowHtml += '<td>';
                fileRowHtml += '<div class="color-controls">';
                fileRowHtml += '<input name="color[' + number + '][color_id]" type="text" value="' + color_id + '" />';
                fileRowHtml += '<input name="color[' + number + '][code]" type="text" value="' + code + '" class="khatoco-color" />';
                fileRowHtml += '<button type="button" class="button color-button-upload" >Chọn ảnh</button>';
                fileRowHtml += '<span class="dashicons dashicons-dismiss color-button-dismiss" title="Xoá"></span>';
                fileRowHtml += '<span class="spinner"></span>';
                fileRowHtml += '</div>';
                fileRowHtml += '<div class="color-images">';
                fileRowHtml += '<input class="color-images-ids" type="hidden" name="color[' + number + '][images_ids]" value="' + images_ids + '"/>';
                fileRowHtml += '<div>';
                if (images != null) {
                    jQuery.each(images, function (index, item) {
                        if (index == 0) {
                            fileRowHtml += '<input type="hidden" name="color[' + number + '][thumb]" value="' + item.url + '"/>';
                        }

                        var item_url = '';
                        jQuery.ajax({
                            url: '<?php echo admin_url("admin-ajax.php"); ?>',
                            async: false,
                            cache: false,
                            type: 'POST',
                            dataType: 'json',
                            data: {action: 'get_thumb_src', attid: item.id},
                            success: function (res) {
                                fileRowHtml += '<img data-img-id="' + item.id + '" src="' + res['src'] + '" />';
                            }
                        });
                    });
                }
                fileRowHtml += '</div>';
                fileRowHtml += '</div>';
                fileRowHtml += '</td>';

                thisRow.html(fileRowHtml);

                jQuery('#color_list tbody').append(thisRow);

                thisRow.find('.khatoco-color').wpColorPicker({
                    defaultColor: false,
                    change: function (event, ui) {
                    },
                    clear: function () {
                    },
                    hide: true,
                    palettes: true
                });

                thisRow.find('.wp-picker-container').prepend('<input name="color[' + number + '][name]" type="text" value="' + name + '" placeholder="Tên màu" class="regular-text color-name" />');

                thisRow.find('.color-images div').sortable({
                    //placeholder: "ui-state-highlight",
                    start: function (e, ui) {

                    },
                    update: function (e, ui) {
                        var new_attachment_ids = [];
                        jQuery.each(thisRow.find('.color-images img'), function (index, item) {
                            var this_img_id = jQuery(this).attr("data-img-id");
                            new_attachment_ids.push(this_img_id);
                        });
                        thisRow.find('.color-images-ids').val(new_attachment_ids.join(','));
                    }
                });
                thisRow.find('.color-images div').disableSelection();

                jQuery('#color-more').remove();
            }

            //Reset row number
            function resetRowNumber() {
                jQuery('#color_list').find('.color-row').each(function (index, el) {
                    var _this = jQuery(this);
                    var number = index + 1;
                    _this.find('.color-label').html('Màu #' + number);
                });
            }
        </script>

        <style type="text/css">
            .wp-color-result {
                height: 26px;
            }

            .wp-color-result:after {
                padding-top: 2px;
                padding-bottom: 2px;
            }

            .wp-picker-container input[type=text].wp-color-picker {
                line-height: 20px;
            }

            .wp-core-ui .button.button-small {
                height: 28px;
            }

            .color-name {
                width: 120px !important;
                display: inline-block;
                margin: 0 6px 6px 0;
            }

            .color-images {
                margin-top: 10px;
            }

            .color-images img {
                width: 80px;
                margin-right: 10px;
            }

            .color-row .color-button-dismiss {
                float: right;
                margin-top: 3px;
                cursor: pointer;
                opacity: 0.5;
            }

            .color-row:hover .color-button-dismiss {
                opacity: 1;
            }
        </style>

        <?php
    }

    add_meta_box(
        'metabox_post_colors', 'Ảnh sản phẩm', 'display_metabox_post_colors', 'post', 'normal', 'low'
    );

    // More info
    function display_metabox_post_more_info($post)
    {
        $post_id = $post->ID;
        $post_info = get_post_meta($post_id, 'post_info', true);
        $post_delivery = get_post_meta($post_id, 'post_delivery', true);
        $post_preservation = get_post_meta($post_id, 'post_preservation', true);
        // $post_wrap = get_post_meta($post_id, 'post_wrap', true); 
        ?>
        <table class="form-table">
            <tr>
                <th scope="row"><label for="post_info">Thông tin sản phẩm</label></th>
                <td><textarea name="post_info" id="post_info" style="width: 100%"
                              rows="5"><?php echo isset($post_info) ? $post_info : ''; ?></textarea></td>
            </tr>
            <tr>
                <th scope="row"><label for="post_delivery">Giao hàng và đổi trả</label></th>
                <td><textarea name="post_delivery" id="post_delivery" style="width: 100%"
                              rows="5"><?php echo ($post_delivery != '') ? $post_delivery : 'N/A'; ?></textarea></td>
            </tr>
            <tr>
                <th scope="row"><label for="post_preservation">Hướng dẫn bảo quản</label></th>
                <td><textarea name="post_preservation" id="post_preservation" style="width: 100%"
                              rows="5"><?php echo ($post_preservation != '') ? $post_preservation : 'N/A'; ?></textarea>
                </td>
            </tr>
            <?php /* ?>
          <tr>
          <th scope="row"><label for="post_wrap">Dịch vụ gói quà và gửi quà tặng</label></th>
          <td><textarea name="post_wrap" id="post_wrap" style="width: 100%" rows="5"><?php echo ($post_wrap != '') ? $post_wrap:'N/A'; ?></textarea></td>
          </tr>
          <?php */ ?>
        </table>
        <?php
    }

    add_meta_box(
        'metabox_post_more_info', 'Thông tin thêm', 'display_metabox_post_more_info', 'post', 'normal', 'low'
    );

    // Accessories
    function display_metabox_post_accessories($post)
    {
        $post_id = $post->ID;
        $sa = get_post_meta($post_id, 'post_accessories', true);
        if (!$sa || $sa == '')
            $sa = array();
        $args = array('posts_per_page' => -1, 'category__in' => array(86, 87, 88, 89, 90));
        $acs = new WP_Query($args);
        if ($acs->have_posts()):
            echo '<ul>';
            while ($acs->have_posts()) {
                $acs->the_post();
                $pid = get_the_id();
                $checked = (in_array($pid, $sa)) ? 'checked' : '';
                echo '<li class="post-' . $pid . '">
                    <label for="post-acs-' . $pid . '" class="selectit"><input type="checkbox" name="acs[]" id="post-acs-' . $pid . '" value="' . $pid . '" ' . $checked . '>' . get_the_title() . '</label> - 
                    <a href="' . admin_url('/') . 'post.php?post=' . $pid . '&action=edit' . '" title="Chi tiết" target="_blank">Chi tiết sản phẩm</a> - 
                    <a href="' . get_permalink($pid) . '" title="Xem" target="_blank">Xem sản phẩm</a>
                </li>';
            }
            echo '</ul>';
        else:
            echo '<p>Hiện chưa có sản phẩm phụ kiện nào! <a href="' . admin_url('/') . '/post-new.php" title="">Thêm sản phẩm</a></p>';
        endif;
    }

    add_meta_box(
        'metabox_post_accessories', 'Phụ kiện đi kèm', 'display_metabox_post_accessories', 'post', 'normal', 'low'
    );

    /* Metabox - CRM Pick Product */

    function display_metabox_post_crm_picker($post)
    {
        $post_id = $post->ID;
        $store_id = get_post_meta($post_id, 'store_id', true);
        ?>
        <p>Nhập mã sản phẩm bên dưới textbox rồi Tìm kiếm, hệ thống trả về danh sách những sản phẩm bên dưới. Chọn để
            nhập các dữ liệu có sẵn tự động (nếu có)!</p>
        <p>
            Mã sản phẩm:
            <input type="text" id="product_crm_sku" name="product_sku" value="" placeholder="Mã SKU">
            <?php /* ?><input type="text" id="product_store_id" name="store_id" value="<?php echo $store_id; ?>" placeholder="Mã kho"><?php */ ?>
            Mã kho:
            <select id="product_store_id" name="store_id" multiple="">
                <?php
                $store_ids = get_option('storehouses', array());
                foreach ($store_ids as $sid => $sdetail) {
                    echo '<option value="' . $sid . '" ' . ($sid == $store_id ? 'selected=""' : '') . '>' . $sid . '</option>';
                }
                ?>
            </select>
            <?php $sync_paged = 0; ?>
            <input type="hidden" id="product_store_id_paged" value="<?php echo $sync_paged; ?>">
            <button type="button" class="button" id="crm_product_find_by_store">Tìm</button>
        </p>

        <div id="show_container"></div>

        <style>
            #table-filter-crm th, #table-filter-crm td {
                border: 1px solid #777;
                padding: 5px;
            }

            #table-filter-crm tr:nth-of-type(even) td {
                background-color: #f0f0f0;
            }
        </style>
        <script>
            jQuery(document).ready(function ($) {
                $('#crm_product_find_by_store').click(function (event) {
                    event.preventDefault();

                    var container = $('#show_container');
                    container.html('<span class="spinner" style="visibility: visible; float: none;"></span>');

                    var store_id = $('#product_store_id').val();
                    var item_id = $('#product_crm_sku').val();
                    var paged = $('#product_store_id_paged').val();

                    if (item_id == '') {
                        container.html('Vui lòng nhập Mã sản phẩm');
                        return false;
                    }

                    if (store_id) {
                        store_id = store_id.join(',');
                    }

                    $.ajax({
                        url: '<?php echo admin_url("admin-ajax.php"); ?>',
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            action: 'sync_get_product_item_by_store_crm',
                            store_id: store_id,
                            item_id: item_id,
                            paged: paged
                        }
                    })
                        .done(function (response) {
                            console.log(response)
                            var html = '<p>Có lỗi xảy ra hoặc không tìm thấy sản phẩm tương ứng trên CRM. Ấn thử nút Tìm phía trên để thử lại!</p>';
                            if (!$.isEmptyObject(response)) {
                                html = '<table id="table-filter-crm" class="form-table widefat">';
                                html += '<thead>';
                                html += '<tr><th width="40%">Mã sản phẩm/Mã màu</th><th width="20%">Mã size</th><th width="10%">Còn hàng</th><th width="25%">Kho</th><th width="5%">Tác vụ</th></tr>';
                                html += '</thead>';
                                html += '<tbody>';

                                $.each(response, function (index, el) {
                                    var data_size = '';
                                    var size_count = 0;
                                    var data = 'data-item-id="' + el['ItemID'] + '" data-color-id="' + el['ColorID'] + '"';

                                    $.each(el['Sizes'], function (size_id, size) {
                                        size_count++;
                                        var stores_with_onhand = '';
                                        $.each(size, function (store, infos) {
                                            if (store != 'total')
                                                stores_with_onhand += store + '|' + infos['OnHand'] + ',';
                                        });
                                        stores_with_onhand = stores_with_onhand.substring(0, stores_with_onhand.lastIndexOf(','));
                                        data_size += '<span style="display:none;" data-size-id="' + size_id + '" data-onhand="' + size['total'] + '" data-stores="' + stores_with_onhand + '"></span>';
                                    });

                                    var old_item_id = '';
                                    var old_color_id = '';
                                        $.each(el['Sizes'], function (size_id, sdetails) {
                                            if (size_id != 'total') {
                                                html += '<tr>';
                                                new_row = false;
                                                if (el['ItemID'] != old_item_id || el['ColorID'] != old_color_id) {
                                                    html += '<td rowspan="' + size_count + '"><strong>' + el['ItemID'] + '</strong> / ' + el['ColorID'] + '</td>';
                                                    el['ItemID'] = old_item_id;
                                                    el['ColorID'] = old_color_id;
                                                    new_row = true;
                                                }

                                                html += '<td>' + size_id + '</td>';
                                                html += '<td>' + sdetails['total'] + '</td>';

                                                var stores_with_onhand = '';
                                                var index = 1;
                                                $.each(sdetails, function (store, infos) {
                                                    if (store != 'total')
                                                        stores_with_onhand += store + ' (' + infos['OnHand'] + '), ';
                                                });
                                                html += '<td>' + stores_with_onhand + '</td>';

                                                if (new_row) {
                                                    html += '<td rowspan="' + size_count + '"><a ' + data + ' href="javascript:;" class="product-pick">Chọn</a>' + data_size + '</td>';
                                                }
                                                html += '</tr>';
                                            }
                                        });
                                    

                                });

                                // html += '<p><a href="" class="sync-prev-page">Quay lại trang trước</a> <a href="#" class="sync-next-page">Tìm tiếp</a></p>';
                            }
                            container.html(html);
                        });
                });

                $('#show_container').delegate('.sync-next-page', 'click', function (event) {
                    $('#product_store_id_paged').val(function (i, oldval) {
                        $('#crm_page').val(oldval + 1);
                        return ++oldval;
                    });

                    $('#crm_product_find_by_store').click();
                    return false;
                });

                $('#show_container').delegate('.sync-prev-page', 'click', function (event) {
                    $('#product_store_id_paged').val(function (i, oldval) {
                        $('#crm_page').val(oldval - 1);
                        if (oldval > 0)
                            return --oldval;
                        else
                            return 0;
                    });

                    $('#crm_product_find_by_store').click();
                    return false;
                });


                $('#show_container').delegate('.product-pick', 'click', function (event) {
                    event.preventDefault();

                    var t = $(this);
                    var mtbx = t.parents('#metabox_post_crm_picker');

                    var item_id = t.data('itemId');
                    var color_id = t.data('colorId');
                    var store = t.data('stores');

                    $.ajax({
                        url: '<?php echo admin_url("admin-ajax.php"); ?>',
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            action: 'checking_product_available',
                            item_id: item_id,
                            color_id: color_id
                        }
                    })
                        .done(function (response) {
                            if (response.result == 'false') {
                                var sizes = [];
                                var size_objs = t.siblings('span');
                                if (size_objs.length > 0) {
                                    // reset
                                    $('#product_sizes').find('input.size-class').prop('checked', false).siblings('span').remove();

                                    var onhand_item;
                                    size_objs.each(function (i, size) {
                                        onhand_item = $(size).data('onhand');
                                        stores_with_onhand = $(size).data('stores');

                                        var obj = $('#product_sizes').find('input#size-' + $(size).data('sizeId'));
                                        var text = ' <span>(còn: ' + onhand_item + ' sp)</span>';
                                        obj.prop('checked', true);
                                        obj.parents('label').append(text);
                                        obj.parents('label').siblings('input.onhand-item').val(onhand_item);
                                        obj.parents('label').siblings('input.stores').val(stores_with_onhand);
                                    });
                                }

                                $.ajax({
                                    url: '<?php echo admin_url("admin-ajax.php"); ?>',
                                    type: 'POST',
                                    dataType: 'json',
                                    data: {
                                        action: 'sync_get_item_infomation_crm',
                                        item_id: item_id,
                                        color_id: color_id
                                    }
                                })
                                    .done(function (response) {
                                        $('#post_sku').val(item_id);
                                        $('#title').val(response['title']).focus();
                                        $('#unit').val(response['unit']);
                                        $('#colors_crm').val(color_id);

                                        if ($('#color_list').find('.color-button-dismiss')) {
                                            $('#color_list .color-button-dismiss').click();
                                        }

                                        createColorRow(0, '', color_id, '', null, '');
                                        resetRowNumber();
                                    });

                                mtbx.find('h3.hndle').click(); // close metabox
                                alert('Đã cập nhật!');
                            } else {
                                alert('Sản phẩm này đã tồn tại!');
                            }
                            return false;
                        });
                });
            });
        </script>
        <?php
    }

    add_meta_box(
        'metabox_post_crm_picker', 'CRM', 'display_metabox_post_crm_picker', 'post', 'normal', 'high'
    );
}

/* Ajax - Get category by parent id */
add_action('wp_ajax_sync_get_product_item_by_store_crm', 'sync_get_product_item_by_store_crm');
add_action('wp_ajax_nopriv_sync_get_product_item_by_store_crm', 'sync_get_product_item_by_store_crm');

function sync_get_product_item_by_store_crm()
{
    $item_id = $_POST['item_id'];
    $store_id = $_POST['store_id'];
    $paged = $_POST['paged'];

    $results = sync_getProductOnHand($item_id, $store_id, $paged);
    $returns = array();
    if (!empty($results)) {
        foreach ($results as $rkey => $r) {
            $item_id = $r['ItemID'];
            $color_id = $r['ColorID'];
            $buid = $r['BUID'];
            $on_hand = $r['OnHand'];
            $size_id = $r['SizeID'];
            $store_id = $r['StoreHouseID'];
            $sku = $item_id . '-' . $color_id;

            if (!isset($returns[$sku])) {
                $returns[$sku] = array(
                    'ItemID' => $item_id,
                    'ColorID' => $color_id,
                    'Sizes' => array(),
                );
            }

            if (intval($on_hand) > 0) {
                if (!isset($returns[$sku]['Sizes'][$size_id])) {
                    $returns[$sku]['Sizes'][$size_id] = array();
                    $returns[$sku]['Sizes'][$size_id]['total'] = 0;
                }

                if (!isset($returns[$sku]['Sizes'][$size_id][$store_id]))
                    $returns[$sku]['Sizes'][$size_id][$store_id] = array();

                $returns[$sku]['Sizes'][$size_id]['total'] += $on_hand;
                $returns[$sku]['Sizes'][$size_id][$store_id] = array(
                    'BUID' => $buid,
                    'OnHand' => $on_hand,
                );
            }
        }
    }

    echo json_encode($returns);
    exit;
}

/* Ajax - Get category by parent id */
add_action('wp_ajax_checking_product_available', 'checking_product_available');
add_action('wp_ajax_nopriv_checking_product_available', 'checking_product_available');

function checking_product_available()
{
    $item_id = $_POST['item_id'];
    $color_id = $_POST['color_id'];

    $args = array(
        'post_type' => 'post',
        'posts_per_page' => -1,
        'meta_query' => array(
            'relation' => 'AND',
            array(
                'key' => 'post_sku',
                'compare' => '=',
                'value' => $item_id,
            ),
            array(
                'key' => 'post_color_id',
                'compare' => '=',
                'value' => $color_id,
            ),
        ),
    );

    $products = get_posts($args);

    if (count($products) <= 0) {
        echo json_encode(array('result' => 'false')); // Un-available
    } else {
        echo json_encode(array('result' => 'true')); // Availabled
    }
    exit;
}

add_action('wp_ajax_sync_get_item_infomation_crm', 'sync_get_item_infomation_crm');
add_action('wp_ajax_nopriv_sync_get_item_infomation_crm', 'sync_get_item_infomation_crm');

function sync_get_item_infomation_crm()
{
    $returns = array();
    $item_id = $_POST['item_id'];
    $color_id = $_POST['color_id'];
    $sku = $item_id;
    $item = sync_getProductItem($item_id);
    $color = sync_getProductColors($color_id);
    $returns = array(
        'title' => '',
        'sku' => $sku,
        'color_id' => $color_id,
        'unit' => 'CAI',
    );

    if ($item) {
        if (!is_null($item['ItemFullName']) && !is_empty($item['ItemFullName']))
            $returns['title'] = $item['ItemFullName'];
        elseif (!is_null($item['ItemName2'])&& !is_empty($item['ItemName2']))
            $returns['title'] = $item['ItemName2'];
        else
            $returns['title'] = $item['ItemName'];

        $returns['unit'] = $item['SellMearsure'];
    }


    echo json_encode($returns);
    exit;
}

/* Ajax - Get category by parent id */
add_action('wp_ajax_get_category_by_parent_ajax', 'get_category_by_parent_ajax');
add_action('wp_ajax_nopriv_get_category_by_parent_ajax', 'get_category_by_parent_ajax');

function get_category_by_parent_ajax()
{

    $result = array(
        'success' => 'false'
    );

    $selected_id = (int)$_POST['selected_id'];
    $parent_id = (int)$_POST['parent_id'];
    $level = (int)$_POST['level'];

    $terms = aj_get_f1_terms_by_parent($parent_id);

    if (!empty($terms)) {

        $html = '<option value="">--Tất cả--</option>';

        foreach ($terms as $t) {
            if ($t->term_id != 111 && $t->term_id != 112) { // exclude 'Khuyen mai' + 'Hang moi' category
                $selected = $selected_id == $t->term_id ? "selected" : "";
                $html .= '<option ' . $selected . ' value="' . $t->term_id . '">' . $t->name . '</option>';
            }
        }

        $result = array(
            'success' => 'true',
            'html' => $html
        );
    }

    echo json_encode($result);
    exit;
}

add_action('wp_ajax_get_posts_by_categories', 'get_posts_by_categories');

function get_posts_by_categories()
{
    $categories = $_POST['categories'];
    $result = array(
        'success' => 'false'
    );

    if (!empty($categories)) {
        $posts = get_posts(array(
            'post_type' => 'post',
            'category__and' => $categories,
            'posts_per_page' => -1,
        ));

        $html = '';
        if ($posts)
            foreach ($posts as $pkey => $p) {
                $sku = get_post_meta($p->ID, 'post_sku', true);
                $product_colors = get_post_meta($p->ID, 'post_colors', true);
                foreach ($product_colors as $c_key => $c) {
                    // $images_ids = $images_ids[0];
                    $sku .= '-' . $c['color_id'];
                    break;
                }

                $html .= '<option value="' . $p->ID . '">' . $sku . '</option>';
            }

        $result = array(
            'success' => 'true',
            'html' => $html
        );
    }

    echo json_encode($result);
    exit;
}

add_action('wp_ajax_get_thumb_src', 'get_thumb_src');
add_action('wp_ajax_nopriv_get_thumb_src', 'get_thumb_src');

function get_thumb_src()
{
    $attid = $_POST['attid'];
    $src = wp_get_attachment_image_src($attid, 'medium');
    if ($src) {
        $src = $src[0];
        echo json_encode(array('src' => $src));
        exit();
    }
}

add_action('wp_ajax_manage_cart_ajax', 'manage_cart_ajax');
add_action('wp_ajax_nopriv_manage_cart_ajax', 'manage_cart_ajax');

function manage_cart_ajax()
{

    $result = array(
        'success' => 'false',
        'msg' => ''
    );

    //add_to_cart
    if (isset($_POST['do_ajax']) && $_POST['do_ajax'] == 'add_to_cart') {

        $post_id = intval($_POST['id']);
        $color_number = $_POST['color_number'];
        $color_id = $_POST['color_id'];
        $size = $_POST['size'];
        $quantity = (int)$_POST['quantity'];
        $cart_id = implode('__', array($post_id, $color_number, $size));

        $colors = get_post_meta($post_id, 'post_colors', true);
        $sizes = get_post_meta($post_id, 'post_sizes', true);
        $weight = get_post_meta($post_id, 'post_weight', true);
        $onhands = get_post_meta($post_id, 'post_onhand', true);

        if (!isset($colors[$color_number])) {
            echo json_encode(array(
                'success' => 'false',
                'msg' => 'Không tìm thấy màu tương ứng cho sản phẩm này, vui lòng F5 để thử lại.'
            ));
            exit;
        }

        if ($quantity <= 0) {
            echo json_encode(array(
                'success' => 'false',
                'msg' => 'Số lượng sản phẩm không thể nhỏ hơn 1.'
            ));
            exit;
        }

        if (isset($_SESSION['cart']['products'][$cart_id])) {
            $quantity += intval($_SESSION['cart']['products'][$cart_id]['quantity']);
        }

        if ($quantity > $onhands[$size]) {
            echo json_encode(array(
                'success' => 'false',
                'msg' => 'Số lượng sản phẩm vượt quá số sản phẩm có trong kho, vui lòng F5 để thử lại.'
            ));
            exit;
        }

        if (!isset($_SESSION['cart'])) {
            $_SESSION['cart'] = array();
        }

        $_SESSION['cart']['products'][$cart_id] = array(
            'id' => $post_id,
            'quantity' => $quantity,
            'size' => $size,
            'weight' => $weight,
            'color_number' => $color_number,
            'color_id' => $color_id,
            'color_name' => $colors[$color_number]['name'],
            'color_thumb' => $colors[$color_number]['thumb'],
            'color_code' => $colors[$color_number]['color_id'],
        );

        $_SESSION['cart_total'] = count($_SESSION['cart']['products']);

        $result = array(
            'success' => 'true',
            'msg' => ''
        );
        echo json_encode($result);
        exit;
    }

    //load_top_cart
    if (isset($_POST['do_ajax']) && $_POST['do_ajax'] == 'load_top_cart') {

        $total = 0;
        $count = 0;
        $html = '';
        if (isset($_SESSION['cart']) && isset($_SESSION['cart']['products']) && $_SESSION['cart_total'] > 0) {
            foreach ($_SESSION['cart']['products'] as $product) {

                $price = get_post_meta($product['id'], 'post_price', true);
                $sale_off = get_post_meta($product['id'], 'sale_off', true);
                $price = (empty($sale_off) || intval($sale_off)==0) ? $price : $price * (1 - $sale_off / 100);
                $quantity = intval($product['quantity']);
                $count += $quantity;
                $total += $price * $quantity;

            }
        }
        $html .=
            '<div class="price-preview row clearfix">
                <div class="cart-left fl">Sản phẩm:</div>
                <div class="cart-right fr"><span class="fr">' . aj_format_number($count) . '</span></div>
            </div>';

        $html .=
            '<div class="price-preview row clearfix">
                <div class="cart-left fl">Tổng tiền:</div>
                <div class="cart-right fr"><span class="fr">' . aj_format_number($total) . ' đ</span></div>
            </div>';

        $result = array(
            'success' => 'true',
            'html' => $html,
        	'cartCount' => aj_format_number($count)
        );

        echo json_encode($result);
        exit;
    }

    //remove_from_cart
    if (isset($_POST['do_ajax']) && $_POST['do_ajax'] == 'remove_from_cart') {

        if (isset($_SESSION['cart']) && isset($_SESSION['cart']['products']) && $_SESSION['cart_total'] > 0) {

            unset($_SESSION['cart']['products'][$_POST['cart_id']]);

            $_SESSION['cart_total'] = count($_SESSION['cart']['products']);

            $result = array(
                'success' => 'true'
            );
        }
    }

}

function get_all_stores()
{

    $args = array(
        'orderby' => 'name',
        'order' => 'DESC',
        'hide_empty' => false,
        'include' => false,
        //'number'        => $limit,
        'fields' => 'all',
        //'hierarchical'  => true
    );

    $stores = get_terms('store', $args);

    if (!empty($stores)) {

        $results = array();

        foreach ($stores as $s) {
            $results[$s->term_id] = array(
                'id' => $s->term_id,
                'name' => $s->name
            );
        }

        return $results;
    }

    return false;
}

function aj_get_home_posts($categoryId = 1, $limit = 12, $filter = 'newproduct', $price = '')
{
    $args = array(
        'post_type' => 'post',
        'order' => 'DESC',
        'orderby' => 'ID',
        'posts_per_page' => $limit,
        'paged' => 1,
        'post_status' => 'publish'
    );

    if ($filter == 'hot') {
        $args['meta_key'] = 'post_is_hot';
        $args['meta_value'] = '1';
    }

    if ($categoryId > 0) {
        $args['cat'] = $categoryId;
    }

    if ($price) {
        $prices = explode('-', $price);
        if (isset($prices[0]) && isset($prices[1])) {
            $args['meta_query'] = array();
            $prices[0] = $prices[0] * 1000;
            $prices[1] = $prices[1] * 1000;
            if ($prices[0] > 0) {
                $args['meta_query'][] = array(
                    'key' => 'post_price',
                    'value' => (int)$prices[0],
                    'compare' => '>=',
                    'type' => 'NUMERIC'
                );
            }
            if ($prices[1] > 0) {
                $args['meta_query'][] = array(
                    'key' => 'post_price',
                    'value' => (int)$prices[1],
                    'compare' => '<=',
                    'type' => 'NUMERIC'
                );
            }
        }
    }

    $posts = new WP_Query($args);
    return $posts;
}

function get_product_sizes_by($post_id)
{
    $sizes = get_post_meta($post_id, 'post_sizes', true);
}

// function product_is_hot($post_id)
// {
//     $is_hot = get_post_meta($post_id, 'post_is_hot', true);
//     if (isset($is_hot) && $is_hot)
//         echo '<span class="sticky hot"></span>';
// }

// function product_is_new($post_id)
// {
//     $is_new = get_post_meta($post_id, 'post_is_new', true);
//     if (isset($is_new) && $is_new)
//         echo '<span class="sticky new"></span>';
// }

function product_is_hot($post_id)
{
    $is_hot = get_post_meta($post_id, 'post_is_hot', true);
    if (isset($is_hot) && $is_hot)
        // echo '<span class="sticky hot"></span>';
        echo '<span class="sticky badge badge-primary">Hot</span>';
}

function product_is_new($post_id)
{
    $is_new = get_post_meta($post_id, 'post_is_new', true);
    if (isset($is_new) && $is_new)
        // echo '<span class="sticky new"></span>';
        echo '<span class="sticky badge badge-success">New</span>';
}

function product_is_sale($post_id)
{
    $is_sale = get_saleoff_info($post_id);
    $saleOff_camp = ProductUtils::getProductSaleOffPrice($post_id);
    if ($is_sale) {
        echo '<span class="sticky-center sale">' . $is_sale['sale_txt'] . '</span>';
    } elseif (!empty($saleOff_camp['percent'])){
        echo '<span class="sticky-center sale">' . aj_format_number($saleOff_camp['percent']) . '%</span>';
    }
}

function aj_display_each_post_home($index)
{
    global $post;
    $post_id = get_the_ID();
    $permalink = get_permalink($post_id);
    $price = (int)get_post_meta($post_id, 'post_price', true);
    if ($index & 1) {    //check odd number
        echo '<div class="item clearfix">';
    }
    ?>
    <div class="item-outer item-for-cart col-md-12 col-sm-12 col-xs-6 mobile-nopadding">
        <div class="item-inner">
            <div class="item-thumb">
                <a href="<?php echo $permalink; ?>"><?php
                    $product_colors = get_post_meta($post->ID, 'post_colors', true);
                    $images_ids = explode(',', $product_colors[0]['images_ids']);
                    $images_ids = $images_ids[0];
                    $images_ids = wp_get_attachment_image_src($images_ids, array(350, 1000));
                    echo '<img src="' . $images_ids[0] . '" alt="">';
                    ?></a>
                </div>
                <h2 class="item-title condensed"><a href="<?php echo $permalink; ?>"><?php echo get_the_title(); ?>
                 </a>

                </h2>
                <span class="item-price condensed">
    <?php
    /*$saleoff_info = get_saleoff_info($post->ID);*/
    $saleoff_info = ProductUtils::getProductSaleOffPrice($post->ID);
    if ($saleoff_info['percent']>0) {
        echo number_format($saleoff_info['value'],0,',','.') . ' đ<br>';
        echo '<span class="blink-text">' . aj_format_number($price) . ' Đ</span>';
    } else {
        echo aj_format_number($price) . ' đ';
    }
    ?>

                </span>
                <?php product_is_new($post_id);
                product_is_hot($post_id); ?>
            <ul>
            	<?php if(is_user_logged_in()):?>
                <li><span class="add-to-wishlist"
                          data-product-id="<?php echo $post_id; ?>"><i class="fa <?php echo is_favorite_product($post_id) ? 'fa-heart' : 'fa-heart-o'; ?>"></i></span></li>
                <?php endif;?>
                <li><span class="add-to-cart"><a rel="nofollow" href="<?php echo $permalink; ?>"><i
                                class="fa fa-shopping-cart"></i></a></span></li>
            </ul>

        </div>
    </div>
    <?php
    if (!($index & 1)) {
        echo '</div>';
    }
}

function display_product_more_info($post_id)
{
    $shops = get_the_terms($post_id, 'store');
    if ($shops) {
        $shops_html = '';
        foreach ($shops as $term) {
            $shops_html .= '<p>' . $term->name . '</p>';
        }
    }
    $post_more_info = array(
        array(
            'title' => 'Thông tin sản phẩm',
            'content' => apply_filters('the_content', get_post_meta($post_id, 'post_info', true))
        ),
        /* array(
          'title' => 'Giao hàng và đổi trả',
          'content' => apply_filters('the_content', get_post_meta($post_id, 'post_delivery', true))
          ), */
        array(
            'title' => 'Có tại cửa hàng?',
            'content' => isset($shops_html) ? $shops_html : ''
        ),
        array(
            'title' => 'Hướng dẫn bảo quản',
            'content' => apply_filters('the_content', get_post_meta($post_id, 'post_preservation', true))
        ),
        /* array(
          'title' => 'Dịch vụ gói quà và gửi quà tặng',
          'content' => apply_filters('the_content', get_post_meta($post_id, 'post_wrap', true))
          ) */
    );

    foreach ($post_more_info as $vals):
        if ($vals['content'] != ''):
            ?>
            <h4><?php echo $vals['title']; ?></h4>
            <div class="the-content">
                <?php echo $vals['content']; ?>
            </div>
            <?php
        endif;
    endforeach;
    return false;
}

// Accessories front-end func
function display_product_accessories($post_id)
{
    $sa = get_post_meta($post_id, 'post_accessories', true);
    if (is_null($sa) || empty($sa) || $sa == '') {
        echo '<p style="padding: 30px 0 150px; text-align: center">Sản phẩm không có phụ kiện đi kèm hoặc chưa cập nhật!</p>';
    } else {
        echo '<ul class="list-style-none clearfix">';
        foreach ($sa as $a_key => $a) {
            $a_colors = get_post_meta($a, 'post_colors', true);
            $img_src = wp_get_attachment_image_src($a_colors[0]['images_ids'], 'medium');
            echo '<li id="product-accessory-' . $a . '" class="fl grid item512" title="' . get_the_title($a) . '">
            <a href="' . get_permalink($a) . '"><img src="' . $img_src[0] . '" alt=""></a>
            <h3><a href="' . get_permalink($a) . '">' . get_the_title($a) . '</a></h3>
            </li>' . "\n";
        }
        echo '</ul>';
    }
}

// Related products
function display_related_products($post_id)
{
    $cats = wp_get_post_categories($post_id);
    $cats_a = array();

    $args = array('post_status' =>'publish' , 'posts_per_page' => 5, 'category__and' => $cats, 'orderby' => 'rand', 'post__not_in' => array($post_id));
    $rp = new WP_Query($args);
    if ($rp->have_posts()) {
        echo '<ul class="list-style-none clearfix">';
        while ($rp->have_posts()) {
            $rp->the_post();
            $p_colors = get_post_meta(get_the_id(), 'post_colors', true);

            foreach ($p_colors as $c_key => $c) {
                $images_ids = explode(',', $c['images_ids']);
                $images_ids = $images_ids[0];
                $images_ids = wp_get_attachment_image_src($images_ids, array(350, 1000));
                break;
            }

            /* $img_a = $img_a[0];
              $img_src = isset($p_colors[0]['thumb']) ? $p_colors[0]['thumb']:$img_a; */

            $saleoff_info = ProductUtils::getProductSaleOffPrice(get_the_id());

            echo '<li id="product-accessory-' . get_the_id() . '" class="fl grid item512" title="' . get_the_title() . '">
                <a href="' . get_permalink() . '"><img src="' . $images_ids[0] . '" alt=""></a>
                <div class="cat"><a href="' . get_category_link(get_the_category(get_the_id())[0]->term_id) . '">' . get_the_category(get_the_id())[0]->name . '</a></div>
                <h3><a href="' . get_permalink() . '">' . get_the_title() . '</a></h3>';

            if ($saleoff_info['percent']>0) {
                echo '<div class=price>'.number_format($saleoff_info['value'],0,',','.') . '</div>';
                echo '<div class="blink-text price">' . aj_format_number(get_post_meta(get_the_id(), 'post_price', true)) . '</div>';
            } else {
                echo '<div class="price">' . aj_format_number(get_post_meta(get_the_id(), 'post_price', true)) . '</div>';
            }

            echo '</li>' . "\n";
        }
        echo '</ul>';
        wp_reset_postdata();
    }
}

// Products just seen
function get_products_just_seen()
{
    if (!isset($_SESSION)) {
        session_start();
    }
    if (isset($_SESSION['just_seen']) && is_array($_SESSION['just_seen'])) {
        $count = 1;
        $html = '<ul class="list-style-none clearfix">';
        foreach ($_SESSION['just_seen'] as $pjs) {
            $count++;
            if ($count > 6)
                break;
            $p_colors = get_post_meta($pjs, 'post_colors', true);
            if (isset($p_colors[0])) {
                $p_images = explode(',', $p_colors[0]['images_ids']);
                $img_a = wp_get_attachment_image_src($p_images[0], 'medium');
                $img_a = $img_a[0];
                $img_src = $img_a;
                $html .= '<li id="product-justseen-' . $pjs . '" class="fl grid item512" title="' . get_the_title($pjs) . '">
                    <a href="' . get_permalink($pjs) . '"><img alt="" src="' . $img_src . '"></a>
                    <div class="cat"><a href="' . get_category_link(get_the_category($pjs)[0]->term_id) . '">' . get_the_category($pjs)[0]->name . '</a></div>
                    <h3><a href="' . get_permalink($pjs) . '">' . get_the_title($pjs) . '</a></h3>';

                $saleoff_info = ProductUtils::getProductSaleOffPrice($pjs);
                if ($saleoff_info['percent']>0) {
                    $html.='<div class=price>'.number_format($saleoff_info['value'],0,',','.') . '</div>';
                    $html.='<div class="blink-text price">' . aj_format_number(get_post_meta($pjs, 'post_price', true)) . '</div>';
                } else {
                    $html.='<div class="price">' . aj_format_number(get_post_meta($pjs, 'post_price', true)) . '</div>';
                }
                $html.='</li>';
            }
        }
        $html .= '</ul>';
    } else {
        $html = 'Bạn chưa xem sản phẩm nào!';
    }
    return $html;
}

add_action('wp_ajax_filter__get_sub_categories', 'filter__get_sub_categories');
add_action('wp_ajax_nopriv_filter__get_sub_categories', 'filter__get_sub_categories');

function filter__get_sub_categories()
{
    $args = array('hide_empty' => 0, 'parent' => $_POST['cat_p'], 'orderby' => 'id');
    $cats = get_categories($args);
    $res = array();
    foreach ($cats as $key => $cat) {
        $res[] = array('id' => $cat->term_id, 'slug' => $cat->slug, 'name' => $cat->name);
    }
    echo json_encode($res);
    exit();
}

// Generate filter records if isset $_GET
function generate_filter_record($slug, $key, $isset = true)
{
    $args = array('hide_empty' => 0, 'orderby' => 'id');
    if ($isset) {
        $cat_parent = get_category_by_slug($slug);
        $args['parent'] = $cat_parent->category_parent;
    } else {
        $cat_parent = get_category_by_slug($slug);
        $args['parent'] = $cat_parent->term_id;
    }
    $cats = get_categories($args);
    if (is_null($cats) || empty($cats))
        exit();
    echo '<select class="fl form-record" name="form[]" id="form-' . $key . '" data-order="' . ($key + 1) . '"><option value="">--Lựa chọn--</option>';
    foreach ($cats as $cat) {
        $select = ($isset) ? selected($cat->slug, $slug, false) : '';
        echo '<option value="' . $cat->slug . '" data-id="' . $cat->term_id . '" ' . $select . '>' . $cat->name . '</option>';
    }
    echo '</select>';
}

function is_favorite_product($pid)
{
    $uid = get_current_user_id();
    if ($uid == 0)
        return false;
    $user_wishlist = get_user_meta($uid, 'wishlist', true);
    if (is_null($user_wishlist) || empty($user_wishlist) || $user_wishlist == '')
        $user_wishlist = array();
    if (!in_array($pid, $user_wishlist))
        return false;
    return true;
}

function get_all_product_min()
{
    $list_prods = get_posts(array('posts_per_page' => -1, 'post_type' => 'post'));
    $list = array();
    foreach ($list_prods as $p):
        $t = new stdClass();
        $t->ID = $p->ID;
        $t->post_title = $p->post_title;
        $list [] = $t;
    endforeach;
    return $list;
}

function get_all_product_in_array()
{
    $post = get_posts(array('posts_per_page' => -1, 'post_type' => 'post'));
    $result = array();
    foreach ($post as $p) :
        $temp_array = array('ID' => $p->ID, 'post_title' => $p->post_title);
        array_push($result, $temp_array);
    endforeach;
    return $result;
}

function get_all_cat_product_in_array()
{
    $cats = get_categories();
    $result = array();
    foreach ($cats as $c) :
        $temp_array = array('term_id' => $c->term_id, 'name' => $c->name);
        array_push($result, $temp_array);
    endforeach;
    return $result;
}

function get_all_cat_min()
{
    $cats = get_categories(array('parent' => 0));
    $result = array();
    foreach ($cats as $c) :
        $temp_array = new stdClass();
        $temp_array->term_id = $c->term_id;
        $temp_array->name = $c->name;
        $result [] = $temp_array;
    endforeach;
    return $result;
}

function get_all_card()
{
    $result = array(
        '0' => 'Không có thẻ',
        '1' => 'Thẻ bạc',
        '2' => 'Thẻ vàng',
        '3' => 'Thẻ bạch kim'
    );
    return $result;
}

function get_card($key)
{
    return get_all_card()[$key];
}

// Check saleoff
function get_saleoff_info($product_id)
{
    $price = get_post_meta($product_id, 'post_price', true);
    $sale_off = (int)get_post_meta($product_id, 'sale_off', true);

    $campaign = check_product_is_saleoff_by_campaign($product_id);
    if (!$campaign || $campaign['type'] != 'product') {
        if ($sale_off == 0)
            return false;
        $new_price = $price * ((100 - $sale_off) / 100);

        return array(
            'sale' => array(
                'number' => $sale_off,
                'unit' => '%'
            ),
            'sale_txt' => $sale_off . '%',
            'new_price' => $new_price,
            'new_price_formated' => aj_format_number($new_price),
        );
    }

    return $campaign;
}

// Get product object if exists
function get_product_by_sku($sku)
{
    if ($sku == '')
        return false;

    $args = array(
        'post_type' => 'post',
        'post_status' => 'publish',
        'posts_per_page' => -1,
        'meta_query' => array(
            array(
                'key' => 'post_sku',
                'value' => $sku
            )
        )
    );

    $products = get_posts($args);
    if (isset($products[0])) {
        return $products[0];
    } else {
        return false;
    }
}

if (is_admin() && current_user_can('manage_options') && isset($_GET['kajsdkjaskdjasd'])) {
    $posts = get_posts(array('posts_per_page' => -1, 'post_type' => 'post'));

    foreach ($posts as $key => $p) {
        $product_colors = get_post_meta($p->ID, 'post_colors', true);

        foreach ($product_colors as $c_key => $c) {
            // if( strpos($c['thumb'], 'beta') !== false ){
            $c['thumb'] = str_replace('/beta', '', $c['thumb']);
            // }
        }

        print_r($product_colors);
        echo '<br>';
    }
    exit;
}

