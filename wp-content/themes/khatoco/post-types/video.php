<?php
/* Register video Type */
add_action('init', 'reg_post_type_video');
function reg_post_type_video() {
    $labels = array(
        'name' => 'Video',
        'singular_name' => 'Video',
        'menu_name' => 'Video',
        'all_items' => 'Tất cả video',
        'add_new' => 'Thêm mới',
        'add_new_item' => 'Thêm mới video',
        'edit_item' => 'Sửa video',
        'new_item' => 'Video mới',
        'view_item' => 'Xem chi tiết',
        'search_items' => 'Tìm kiếm',
        'not_found' => 'Không tìm thấy bài video nào',
        'not_found_in_trash' => 'Không có bài video nào trong thùng rác',
        'view' => 'Xem video'
    );

    $args = array(
        'labels' => $labels,
        'description' => 'Video, khacoto',
        'public' => false,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'show_in_nav_menus' => false,
        'show_ui' => true,
        'rewrite' => array('slug' => 'video'),
        'menu_position' => 5,
        'menu_icon' => 'dashicons-format-video',
        'supports' => array('title', 'editor', 'thumbnail'),
        'has_archive' => 'video'
    );

    register_post_type('video', $args);
}

/* Meta Boxes */
add_action( 'admin_init', 'add_metabox_video' );
function add_metabox_video() {
    function display_metabox_video_general($post){
        $post_id = $post->ID;
        $youtube_code = get_post_meta($post_id, 'video_youtube_code', true);
        ?>
        <input type="hidden" name="do" value="update_video">
        <table id="color_list" class="form-table">
        <tbody>
            <tr>
                <td>
                    <input id="video_url" type="text" class="text" name="youtube_code" value="<?php echo $youtube_code; ?>" placeholder="" />
                    <input id="video_preview" type="button" name="publish" id="publish" class="button button-primary" value="Xem trước">
                </td>
            </tr>
            <tr>
                <td id="video_frame_outer"></td>
            </tr>
        </tbody>
        </table>
        <script type="text/javascript">
            jQuery(document).ready(function($){
                jQuery('#video_preview').click(function(){
                    var url = jQuery('#video_url').val();
                    var tmp1 = url.split("?");
                    var code = '';

                    if(tmp1.length > 1){
                        var tmp2 = tmp1[1].split("=");
                        code = tmp2[1];
                    }else{
                        code = url;
                    }

                    jQuery('#video_url').val(code);
                    jQuery('#video_frame_outer').html('<iframe width="560" height="315" src="//www.youtube.com/embed/'+code+'" frameborder="0" allowfullscreen></iframe>');
                    
                    return false;
                });

                var code = '<?php echo $youtube_code; ?>';
                if(code){
                    jQuery('#video_preview').click();
                }
            });
        </script>

        <?php
    }
    add_meta_box(
        'metabox_video_general',
        'Đường dẫn hoặc ID Youtube. VD: <a target="_blank" href="https://www.youtube.com/watch?v=Jukx0qDSQZs">https://www.youtube.com/watch?v=Jukx0qDSQZs</a> hoặc <a target="_blank" href="https://www.youtube.com/watch?v=Jukx0qDSQZs">Jukx0qDSQZs</a>',
        'display_metabox_video_general',
        'video',
        'normal',
        'high'
    );
}

add_action( 'save_post', 'save_metabox_video' );
function save_metabox_video($post_id) {
    if(get_post_type() == 'video' && isset($_POST['do']) && $_POST['do'] == 'update_video'){
        update_post_meta( $post_id, 'video_youtube_code', $_POST['youtube_code']);
    }
}