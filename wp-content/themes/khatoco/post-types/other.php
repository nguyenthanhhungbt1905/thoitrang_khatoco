<?php

/* Remove meta box Product_category */
add_action('admin_menu', 'remove_meta_box_post');
function remove_meta_box_post() {
    remove_meta_box('categorydiv', 'post', 'normal');
}

/* Custom Admin Menu */
add_action( 'admin_menu', 'aj_custom_admin_menu', 999 );
function aj_custom_admin_menu() {
    global $menu;
    global $submenu;

    if(current_user_can('manage_categories')) {
        $menu[5][0] = 'Sản phẩm';

        $submenu['edit.php'][5][0] = 'Tất cả sản phẩm';
        $submenu['edit.php'][10][0] = 'Thêm mới';
        // unset($submenu['edit.php'][10]);
        $submenu['edit.php'][15][0] = 'Loại sản phẩm';
        $submenu['edit.php'][16][0] = 'Từ khóa';
    }

    /* Remove 'Comments' menu page */
    remove_menu_page( 'edit-comments.php' );
    // remove_submenu_page('edit.php?post_type=rate', 'post-new.php?post_type=rate');
}

add_action( 'admin_menu', 'change_post_object' );
function change_post_object() {
    global $wp_post_types;

    $labels = &$wp_post_types['post']->labels;
    $labels->name = 'Sản phẩm';
    $labels->singular_name = 'Sản phẩm';
    $labels->add_new = 'Thêm mới';
    $labels->add_new_item = 'Thêm mới';
    $labels->edit_item = 'Sửa sản phẩm';
    $labels->new_item = 'Sản phẩm';
    $labels->view_item = 'Xem sản phẩm';
    $labels->search_items = 'Tìm kiếm';
    $labels->not_found = 'Không tìm thấy sản phẩm tương ứng';
    $labels->not_found_in_trash = 'Không tìm thấy sản phẩm tương ứng trong thùng rác';
    $labels->all_items = 'Tất cả sản phẩm';
    $labels->menu_name = 'Sản phẩm';
    $labels->name_admin_bar = 'Sản phẩm';

    // $wp_post_types['post']->cap->create_posts = false;
}

add_action('init', 'remove_editor_in_post_edit_page');
function remove_editor_in_post_edit_page() {
    remove_post_type_support('post', 'editor');
}

