<?php

//set heartbeat timeout 120s
function wp_heartbeat_settings_3242( $settings ) {
    $settings['interval'] = 600; //Anything between 15-120
    return $settings;
}
add_filter( 'heartbeat_settings', 'wp_heartbeat_settings_3242' );





//required php excel
require_once (__DIR__).'../../includes/phpexcel/PHPExcel.php';

// required import active cards
require_once(__DIR__).'/cards/active.php';

// required import active cards
require_once(__DIR__).'/cards/import.php';

// required import active cards
require_once(__DIR__).'/cards/export.php';

// required topup
require_once(__DIR__).'/cards/topup.php';

//Register custom post scratch card

add_action('init', 'posttype_scratch_card');
function posttype_scratch_card(){

    //add new table import tmp if not exists
    global $wpdb;
    $charset_collate = $wpdb->get_charset_collate();

    $sql = "CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}cards_temp`(
          id int not null AUTO_INCREMENT,
          code varchar(255) not null,
          pin varchar(255) not null,
          row varchar(255) not null,
          created_at datetime NOT NULL default CURRENT_TIMESTAMP,
          PRIMARY KEY  (id)
        )$charset_collate;";
    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    dbDelta($sql);
    // print_r($sql );
    //end add new table

    // add new table active tmp if not exist
    $sqlActiveTmp = "CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}cards_active_temp`(
            id int not null AUTO_INCREMENT,
            serial varchar(255) not null,
            product_id varchar(255) not null,
            date_scan varchar(255) not null,
            warehouse varchar(255) not null,
            market varchar(255) not null,
            agency varchar(255) not null,
            row varchar(255) not null,
            created_at datetime NOT NULL default CURRENT_TIMESTAMP,
            PRIMARY KEY  (id)
        )$charset_collate;";
    dbDelta($sqlActiveTmp);
    //end add new table active tmp


    //create table customer register
    $sqlCustomerRegister = "CREATE TABLE IF NOT EXISTS`{$wpdb->prefix}customer_register` (
      id int(11) NOT NULL AUTO_INCREMENT,
      user_id int(11) DEFAULT NULL,
      name varchar(255) NOT NULL,
      phone varchar(255) NOT NULL,
      address varchar(500) NOT NULL,
      date_of_birth varchar(255) NOT NULL,
      gender varchar(20) NOT NULL,
      questions longtext,
      created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
      PRIMARY KEY  (id)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
";
    dbDelta($sqlCustomerRegister);

    //create table customer register cards
    $sqlCustomerRegisterCards = "CREATE TABLE `{$wpdb->prefix}customer_register_cards` (
      id int(11) NOT NULL AUTO_INCREMENT,
      post_id int(11) NOT NULL,
      customer_register_id int(11) NOT NULL,
      top_up_value varchar(255),
      created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
      PRIMARY KEY  (id)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

    dbDelta($sqlCustomerRegisterCards);




    //create table topup
    $sqlValueCards = "CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}value_cards`(
      id int(11) NOT NULL AUTO_INCREMENT,
      card_value varchar(255) NOT NULL,
      card_quantity varchar(255) NOT NULL DEFAULT 0,
      created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
      PRIMARY KEY  (id)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
    dbDelta($sqlValueCards);


    //add default data
    $valueCardData = $wpdb->get_results("select * from `{$wpdb->prefix}value_cards`");
    if(!$valueCardData){

        $wpdb->insert($wpdb->prefix."value_cards", ['card_value'=>10000, 'card_quantity' => 10]);
        $wpdb->insert($wpdb->prefix."value_cards", ['card_value'=>20000, 'card_quantity' => 20]);
        $wpdb->insert($wpdb->prefix."value_cards", ['card_value'=>50000, 'card_quantity' => 50]);
        $wpdb->insert($wpdb->prefix."value_cards", ['card_value'=>100000, 'card_quantity' => 100]);
    }

    //end add default data


    $labels = array(
        'name' => 'Thẻ cào',
        'singular_name' => 'Thẻ cào',
        'all_items' => 'Tất cả thẻ cào',
        'add_new' => 'Import',
        'add_new_item' => 'Thêm mới thẻ cào',
        'edit_item' => 'Chỉnh sửa',
        'new_item' => 'Thẻ cào mới',
        'view_item' => 'Xem chi tiết',
        'search_items' => 'Tìm kiếm',
        'not_found' =>  'Không tìm thấy thẻ cào nào',
        'not_found_in_trash' => 'Không tìm thấy thẻ cào nào trong thùng rác',
        'view' =>  'Xem chi tiết'
    );

    $args = array(
        'labels' => $labels,
        'description' => '',
        'public' => false,
        'exclude_from_search' => true,
        'publicly_queryable' => true,
        'show_in_nav_menus' => false,
        'show_ui' => true,
        'show_in_menu' => true,
        'rewrite' => false,
        'has_archive' => false,
        'menu_position' => 6,
        'menu_icon' => 'dashicons-id',
        //'supports' => array('title'),
        'supports'      => false,
        'capabilities' => array(
            'create_posts' => 'do_not_allow',
        ),
        'map_meta_cap' => true,
        'capability_type' => 'post',
    );
    register_post_type('cards', $args);

}

//add filter admin
add_action('restrict_manage_posts', 'add_filter_restrict_manage_cards');
function add_filter_restrict_manage_cards(){
    global $typenow;
    if($typenow == 'cards'){
        $values = array(
            'Active' => 'active',
            'Used'  => 'used',
            'Blank' => 'blank',
            'Deleted' => 'deleted',
        );
        ?>
            <select name="ADMIN_FILTER_STATUS">
                <option value=""><?php _e('Filter By Status', ''); ?></option>
                <?php
                $current_v = isset($_GET['ADMIN_FILTER_STATUS'])? $_GET['ADMIN_FILTER_STATUS']:'';
                foreach ($values as $label => $value) {
                    printf
                    (
                        '<option value="%s"%s>%s</option>',
                        $value,
                        $value == $current_v? ' selected="selected"':'',
                        $label
                    );
                }
                ?>
            </select>
        <?php

    }
}

add_filter( 'parse_query', 'wpse45436_cards_filter' );
function wpse45436_cards_filter($query){
    global $pagenow, $typenow;
    if($typenow === 'cards' && is_admin() && $pagenow == 'edit.php' && isset($_GET['ADMIN_FILTER_STATUS']) && $_GET['ADMIN_FILTER_STATUS']!=''){
        $query->query_vars['meta_key']  = 'status';
        $query->query_vars['meta_value'] = $_GET['ADMIN_FILTER_STATUS'];
    }
}

//end add filter admin


//add filter search phone and pin
add_filter('posts_join', 'cards_search_join');
function cards_search_join($join){
    global $pagenow, $wpdb, $typenow;
    $table_user_register_card = $wpdb->prefix . 'customer_register_cards';
    $table_user_register = $wpdb->prefix . 'customer_register';
    if(is_admin() && $pagenow == 'edit.php' && $typenow ==='cards' && isset($_GET['s']) && $_GET['s']!=''){
        $join .= 'LEFT JOIN ' . $wpdb->postmeta . ' ON ' . $wpdb->posts . '.ID = ' . $wpdb->postmeta . '.post_id ';
        $join .= 'LEFT JOIN '.$table_user_register_card.' ON '.$wpdb->posts.'.ID = '. $table_user_register_card.'.post_id ';
        $join .= 'LEFT JOIN '.$table_user_register.' ON '.$table_user_register_card.'.customer_register_id = '.$table_user_register.'.id';
    }
    return $join;
}

add_filter('posts_where', 'cards_search_where');
function cards_search_where($where){
    global $pagenow, $wpdb, $typenow;
    $table_user_register = $wpdb->prefix . 'customer_register';
    if(is_admin() && $pagenow == 'edit.php' && $typenow ==='cards' && isset($_GET['s']) && $_GET['s']!=''){
        $where = preg_replace(
            "/\(\s*" . $wpdb->posts . ".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",
            "(" . $wpdb->posts . ".post_title LIKE $1) OR (" . $wpdb->postmeta . ".meta_value LIKE $1) OR(".$table_user_register.".phone LIKE $1)", $where );
    }
    return $where;
}



function cards_search_distinct( $where ){
    global $pagenow, $wpdb, $typenow;

    if(is_admin() && $pagenow == 'edit.php' && $typenow ==='cards' && isset($_GET['s']) && $_GET['s']!=''){
        return "DISTINCT";
    }
    return $where;
}
add_filter( 'posts_distinct', 'cards_search_distinct' );
//end add filter search phone and pin


function cards_columns_head($defaults) {
    $defaults['pin'] = 'PIN';
    $defaults['userid'] = 'Admin import';
    $defaults['createdAt'] = 'Ngày tạo';
    $defaults['userUpdate'] = 'Admin active';
    $defaults['dateActive'] = 'Ngày active';
    $defaults['adminDeleted'] = 'Admin xóa';
    $defaults['deletedAt'] = 'Ngày xóa';
    $defaults['status'] = 'Trạng thái';
    $defaults['phone'] = 'Phone';
    return $defaults;
}
function cards_columns_content($column, $post_ID) {
    switch ($column) {
        case 'pin':
            echo get_post_meta($post_ID, 'pin', true);
            break;
        case 'status':
            echo get_post_meta($post_ID,'status',true);
            break;
        case 'userid':
            echo get_post_meta($post_ID, 'userId', true);
            break;
        case 'createdAt':
            echo get_post_meta($post_ID, 'createdAt', true);
            break;
        case 'userUpdate':
            echo get_post_meta($post_ID, 'userUpdate', true);
            break;
        case 'dateActive':
            echo get_post_meta($post_ID, 'dateActive', true);
            break;
        case 'adminDeleted':
            echo get_post_meta($post_ID, 'admin_deleted', true);
            break;
        case 'deletedAt':
            echo get_post_meta($post_ID, 'deleted_at', true);
            break;
        case 'phone':
            global $wpdb;
            $table_name = $wpdb->prefix . 'customer_register_cards';
            $table_user_register = $wpdb->prefix . 'customer_register';
            $dataTmp = $wpdb->get_results('select * from ' . $table_name.' left join '.$table_user_register.' on '.$table_name.'.customer_register_id = '.$table_user_register.'.id'.' where post_id='.$post_ID);
            echo $dataTmp ? $dataTmp[0]->phone : '';
            break;
        default:
            # code...
            break;
    }
}

add_filter('manage_cards_posts_columns', 'cards_columns_head');
add_filter('manage_cards_posts_custom_column', 'cards_columns_content', 10, 2);





/* Remove meta box cards */
add_action('admin_init', 'remove_meta_box_cards');
function remove_meta_box_cards() {
    if(post_type_exists('cards')) remove_meta_box('submitdiv', 'cards', 'normal');
}


// add custom metabox import excel template
function posttype_register_meta_boxes() {
    add_meta_box( 'card-template', __( 'Thông tin thẻ cào' ), 'posttype_display_import_callback', 'cards' );
}
add_action( 'add_meta_boxes', 'posttype_register_meta_boxes' );


function calculatorTopup($topup){
    switch ($topup) {
        case 10000:
            return 9.091;

        case 20000:
            return 18.182;

        case 50000:
            return 45.455;

        case 100000:
            return 90.909;
        default:
            return null;
    }
}
function posttype_display_import_callback($post){
    global $wpdb;
    $table_name = $wpdb->prefix . 'customer_register_cards';
    $table_user_register = $wpdb->prefix . 'customer_register';
    $serial = get_post_meta($post->ID, 'serial', true);
    $pin = get_post_meta($post->ID, 'pin', true);
    $adminImport = get_post_meta($post->ID, 'userId', true);
    $adminActive = get_post_meta($post->ID, 'userUpdate', true);
    $dateActive = get_post_meta($post->ID, 'dateActive', true);
    $status = get_post_meta($post->ID, 'status', true);
    $createdAt = get_post_meta($post->ID, 'createdAt', true);
    $dataTmp = $wpdb->get_results('select * from ' . $table_name.' left join '.$table_user_register.' on '.$table_name.'.customer_register_id = '.$table_user_register.'.id'.' where post_id='.$post->ID);

    $productId = get_post_meta($post->ID, 'product_id', true);
    $scanedAt = date_format(date_create(get_post_meta($post->ID,'date_scan',true)),'d-m-y H:i');
    $wareHouse = get_post_meta($post->ID,'ware_house',true);

    $marketPlace = get_post_meta($post->ID,'market_place',true);
    $agency = get_post_meta($post->ID,'agency',true);

    ?>
    <style type="text/css">
    .empty-container{
        border: none!important;
    }
    table input{
        width: 50%;
    }
</style>
    <table class="form-table">
        <tbody>
        <tr>
            <th>
                <label for="serial">Số serial</label>
            </th>
            <td>
                <input type="text" id="serial" name="serial" value="<?= $serial ?>" disabled>
            </td>
        </tr>
        <tr>
            <th>
                <label for="pin">Mã PIN</label>
            </th>
            <td>
                <input type="text" id="pin" name="pin" value="<?= $pin ?>" disabled>
            </td>
        </tr>
        <tr>
            <th>
                <label for="admport">Admin import</label>
            </th>
            <td>
                <input type="text" id="admport" name="admport" value="<?= $adminImport ?>" disabled>
            </td>
        </tr>

    <?php if($adminActive): ?>
        <tr>
            <th>
                <label for="adactive">Admin active</label>
            </th>
            <td>
                <input type="text" id="adactive" name="adactive" value="<?= $adminActive ?>" disabled>
            </td>
        </tr>
        <tr>
            <th>
                <label for="dateActive">Ngày active</label>
            </th>
            <td>
                <input type="text" id="dateActive" name="dateActive" value="<?= $dateActive ?>" disabled>
            </td>
        </tr>
    <?php endif; ?>


        <tr>
            <th>
                <label for="status">Trạng thái</label>
            </th>
            <td>
                <input type="text" id="status" name="status" value="<?= $status ?>" disabled>
            </td>
        </tr>

        <tr>
            <th>
                <label for="created_at">Ngày khởi tạo</label>
            </th>
            <td>
                <input type="text" id="created_at" name="created_at" value="<?= $createdAt ?>" disabled>
            </td>
        </tr>

        <?php if($dataTmp): ?>
            <tr>
                <th>
                    <label for="phone">Phone</label>
                </th>
                <td>
                    <p><?= $dataTmp[0]->phone ?></p>
                </td>
            </tr>
            <tr>
                <th>
                    <label for="product_id">ID Sản Phẩm</label>
                </th>
                <td>
                    <p><?= $productId ? $productId : '' ?></p>
                </td>
            </tr>
            <tr>
                <th>
                    <label for="scanned_at">Scaned At</label>
                </th>
                <td>
                    <p><?= $scanedAt ? $scanedAt : '' ?></p>
                </td>
            </tr>
            <tr>
                <th>
                    <label for="ware_house">Ware House</label>
                </th>
                <td>
                    <p><?= $wareHouse ? $wareHouse : '' ?></p>
                </td>
            </tr>
            <tr>
                <th>
                    <label for="market_place">Market Place</label>
                </th>
                <td>
                    <p><?= $marketPlace ? $marketPlace : '' ?></p>
                </td>
            </tr>

            <tr>
                <th>
                    <label for="agency">Agency</label>
                </th>
                <td>
                    <p><?= $agency ? $agency : '' ?></p>
                </td>
            </tr>

            <tr>
                <th>
                    <label for="client_name">Name</label>
                </th>
                <td>
                    <p><?= $dataTmp[0]->name ?></p>
                </td>
            </tr>

            <tr>
                <th>
                    <label for="client_phone">Phone</label>
                </th>
                <td>
                    <p><?= $dataTmp[0]->phone ?></p>
                </td>
            </tr>

            <tr>
                <th>
                    <label for="client_birth_day">Ngày Sinh</label>
                </th>
                <td>
                    <p><?= $dataTmp[0]->date_of_birth ?></p>
                </td>
            </tr>

            <tr>
                <th>
                    <label for="client_address">Địa Chỉ</label>
                </th>
                <td>
                    <p><?= $dataTmp[0]->address ?></p>
                </td>
            </tr>

            <tr>
                <th>
                    <label for="top_up_value">Topup</label>
                </th>
                <td>
                    <p><?= !empty($dataTmp) ? calculatorTopup(floatval($dataTmp[0]->top_up_value)) : null ?></p>
                </td>
            </tr>

            <tr>
                <th>
                    <label for="answ1">Câu hỏi 1: </label>
                </th>
                <td>
                    <p><?= join(unserialize($dataTmp[0]->questions)['answer1'], ', '); ?></p>
                </td>
            </tr>

            <tr>
                <th>
                    <label for="answ2">Câu hỏi 2: </label>
                </th>
                <td>
                    <p><?= join(unserialize($dataTmp[0]->questions)['answer2'], ', '); ?></p>
                </td>
            </tr>


        <?php endif; ?>

        <?php if($status != 'deleted'): ?>
            <tr>
                <th></th>
                <td>
                    <button type="submit" name="submit">Move to trash</button>
                </td>
            </tr>
            <input type="hidden" name="updateCard" value="true" />
        <?php endif; ?>


        </tbody>
    </table>
    <?php
}
//end custom metabox import excel template

//disable yoast seo field
function remove_yoast_metabox_reservations(){
    remove_meta_box('wpseo_meta', 'cards', 'normal');
}
add_action( 'add_meta_boxes', 'remove_yoast_metabox_reservations',11 );
//end disable yoast seo metabox

// remove publish metabox
//    add_action( 'admin_menu', function () {
//        remove_meta_box( 'metabox_id', 'cards', 'default_position' );
//        remove_meta_box( 'submitdiv', 'cards', 'side' );
//    } );
//end remove publish metabox






//edit card status
add_action( 'save_post_cards', 'editCardAction', 10, 2 );
function editCardAction(){
    global $post;
    if(!isset($_POST['updateCard']) && !isset($_POST['serial']) && !isset($_POST['pin'])){// only accept update action
        return false;
    }
    $current_user = wp_get_current_user();
    update_post_meta($post->ID, 'status', 'deleted');
    update_post_meta($post->ID, 'admin_deleted',$current_user->user_login );
    update_post_meta($post->ID, 'deleted_at', date("d-m-Y H:i:s"));
    //validate pin code
}
//end edit cards




function clearOldData($table){
    global $wpdb;
    $table_name = $table;
    $wpdb->query('TRUNCATE TABLE '.$table_name);
}




//validate PIN step 3
add_action('wp_ajax_checkPINCode', 'checkPINCodeInit');
add_action('wp_ajax_nopriv_checkPINCode', 'checkPINCodeInit');

function checkPINCodeInit(){
    $errors = [];
    $serialPin = $_POST['cards'];
    validatePINCODE($serialPin, $errors);

    if(!$errors){
        $errors['status'] = 'success';
        $errors['message'] = 'PIN Code ok';
    }
    echo json_encode($errors);
    wp_die();
}


function validatePINCODE($serialPin, &$errors){
    global $wpdb;

    $table_customer_register = $wpdb->prefix . 'customer_register';
    $table_customer_register_cards = $wpdb->prefix.'customer_register_cards';

    if(!$serialPin){
        $errors['code'] = 'global';
        $errors['message'] = 'Mã số không tồn tại trên hê thống, vui lòng nhập lại mã số khác';
        $errors['status'] = 'errors';
    }

    foreach($serialPin as $srKey => $code){

        //check serial - pin exists and active
        $postSerial = new WP_Query([ 'post_type' =>'cards', 'posts_per_page' => -1, 'meta_key' => 'pin', 'meta_value' => $code, 'post_status' => 'publish' ]);
        if(!$postSerial->post){
            $errors['code'] = $code;
            $errors['message'] = 'Mã dự thưởng không tồn tại trên hệ thống';
            $errors['status'] = 'errors';
            break;
        }


        //check if duplicate serial pin
        $duplicate = 0;
        array_map(function ($item) use( $code, $errors, &$duplicate ){
            if($item == $code)
                $duplicate +=1;
        },$serialPin);
        if($duplicate>1){
            $errors['code'] = $code;
            $errors['message'] = 'Mã số này đã được sử dụng để đăng ký tham gia chương trình';
            $errors['status'] = 'errors';
            break;
        }


        //check serial - pin exists and active
        $postSerial = new WP_Query([ 'post_type' =>'cards', 'posts_per_page' => -1, 'meta_key' => 'pin', 'meta_value' => $code, 'post_status' => 'publish' ]);
        if(!$postSerial->post){
            $errors['code'] = $code;
            $errors['message'] = 'Mã số này không tồn tại trên hệ thống';
            $errors['status'] = 'errors';
            break;
        }
        $serialStatus = get_post_meta($postSerial->post->ID, 'status', true);
        $serialCode = get_post_meta($postSerial->post->ID, 'serial', true);
        $serialPinCode = get_post_meta($postSerial->post->ID, 'pin', true);



        if($serialStatus!='active'){
            $errors['code'] = $code;
            $errors['message'] = 'Mã dự thưởng không tồn tại trên hệ thống';
            $errors['status'] = 'errors';
            break;
        }

        //check if serial exist before
        $serialExist = $wpdb->get_results("select * from ".$table_customer_register_cards." where post_id=".$postSerial->post->ID);
        if($serialExist){
            $errors['code'] = $code;
            $errors['message'] = 'Mã số này đã được sử dụng để đăng ký tham gia chương trình';
            $errors['status'] = 'errors';
            break;
        }
    }

}






//check phone exists
add_action('wp_ajax_checkPhonePromotion', 'checkPhonePromotionInit');
add_action('wp_ajax_nopriv_checkPhonePromotion', 'checkPhonePromotionInit');
function checkPhonePromotionInit(){
    global $wpdb;
    $errors = [];
    $table_customer_register = $wpdb->prefix . 'customer_register';
    $phones = $wpdb->get_results("select * from ".$table_customer_register." where phone='".$_POST['phone']."'");
    if($phones){
        $errors['code'] = 'global';
        $errors['message'] = ' Số điện thoại này đã được sử dụng để đăng ký tham gia chương trình rồi';
        $errors['status'] = 'errors';
        echo json_encode($errors);
        wp_die();
    }
    $errors['code'] = 'global';
    $errors['message'] = 'Valid phone';
    $errors['status'] = 'success';
    echo json_encode($errors);
    wp_die();
}
//end ccheck phone exists


add_action('wp_ajax_registerPromotion', 'registerPromotionInit');
add_action('wp_ajax_nopriv_registerPromotion', 'registerPromotionInit');

function registerPromotionInit(){

    global $wpdb;
    $errors = [];

    $table_customer_register = $wpdb->prefix . 'customer_register';
    $table_customer_register_cards = $wpdb->prefix.'customer_register_cards';
    //check phone
    $phones = $wpdb->get_results("select * from ".$table_customer_register." where phone='".$_POST['phone']."'");
    if($phones){
        $errors['code'] = 'global';
        $errors['message'] = ' Số điện thoại này đã được sử dụng để đăng ký tham gia chương trình rồi';
        $errors['status'] = 'errors';
        echo json_encode($errors);
        wp_die();
    }

    //check serial code

    $serialPin = $_POST['cards'];

    if(!$serialPin){
        $errors['code'] = 'global';
        $errors['message'] = 'Mã dự thưởng không tồn tại trên hệ thống';
        $errors['status'] = 'errors';
    }

    //insert user register data
    $dataUserRegister = $wpdb->insert($table_customer_register ,['name' => $_POST['fullName'], 'phone' => $_POST['phone'], 'address' => $_POST['address'], 'date_of_birth'=> $_POST['dateOfBirth'], 'gender'=>$_POST['gender'], 'questions' => serialize($_POST['questions'])]);
    if(!$dataUserRegister){
        $errors['code'] = 'global';
        $errors['message'] = 'Đã có lỗi xảy ra từ hệ thống, vui lòng liên hệ nhà phát triển';
        $errors['status'] = 'errors';
    }
    $dataUserRegisterId = $wpdb->insert_id;

    //check topup value exists
    $dataTopupValue = $wpdb->get_results("select * from `{$wpdb->prefix}value_cards` where card_quantity>0");
    if(!$dataTopupValue){
        $errors['code'] = 'global';
        $errors['message'] = 'Không có giá trị mệnh giá thẻ cào, liên hệ nhà phát triển';
        $errors['status'] = 'errors';
    }
    //end

    validatePINCODE($serialPin,$errors);
    
    
    //add data if validate ok
    if(!isset($errors['status']) || $errors['status'] != 'errors'){

        foreach($serialPin as $srDataKey => $dataCode){

            $dataPostSerial = new WP_Query([ 'post_type' =>'cards', 'posts_per_page' => -1, 'meta_key' => 'pin', 'meta_value' => $dataCode, 'post_status' => 'publish' ]);
            if(!$dataPostSerial->post){
                $errors['message'] = 'Mã số này đã được sử dụng để đăng ký tham gia chương trình';
                $errors['status'] = 'errors';
                break;
            }


            //add data
            $dataUserRegisterCards = $wpdb->insert($table_customer_register_cards, ['post_id' => $dataPostSerial->post->ID, 'customer_register_id' => $dataUserRegisterId]);
            if(!$dataUserRegisterCards){
                $errors['message'] = 'Đã có lỗi xảy ra từ hệ thống, vui lòng liên hệ nhà phát triển';
                $errors['status'] = 'errors';
                break;
            }

            //update status serial pin
            update_post_meta($dataPostSerial->post->ID, 'status', 'used');
        }
    }



    //remove user register if error
    if(isset($errors['status']) && $errors['status'] =='errors'){
        $wpdb->delete($table_customer_register, ['id' => $dataUserRegisterId]);
    }else{


        $topupValue = $wpdb->get_results("select * from `{$wpdb->prefix}value_cards` where card_quantity>0");
        $topupValueKey = array_rand($topupValue, 1);
        $amount = $topupValue[$topupValueKey]->card_value;

        //update quantity ò card value
        $cardQuantity = $topupValue[$topupValueKey]->card_quantity;
        $cardId = $topupValue[$topupValueKey]->id;
        $wpdb->update($wpdb->prefix."value_cards", ['card_quantity' =>  intval($cardQuantity-1) > 0 ? intval($cardQuantity-1) : 0 ], ['id' => $cardId]);

        //get all PIN user input
        $allPinCode = join("\n", $serialPin);


        $phone = $_POST['phone'];
        $user_top_up = 'khatoco_topup';
        $password_top_up = 'j9j5r';
        $tid = date('Y-m-d');
        $url = 'http://sms.vietguys.biz/api/topup/?u='.$user_top_up.'&pwd='.$password_top_up.'&phone='.$phone.'&amount='.$amount.'&tid='.$tid;
        $data = array("u" => $user_top_up,"pwd" => $password_top_up, "phone" => $phone, "tid" => $tid);
        $data_string = json_encode($data);
       $ch=curl_init($url);
       curl_setopt($ch, CURLOPT_POST, true);
       curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
       curl_setopt($ch, CURLOPT_HEADER, 0);
       curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
       curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
       curl_setopt($ch, CURLOPT_USERPWD, $user_top_up . ":" . $password_top_up);
       curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8','Content-length: '.strlen($data_string)));
       $response = curl_exec( $ch );
       curl_close($ch);
         //$response = 00;
        // print_r($response);
        if($response == 00 || $response === '00'){
            $wpdb->update( $table_customer_register_cards, array( 'top_up_value' => $amount),array('customer_register_id' => $dataUserRegisterId));
            # Plivo AUTH ID
            $u = 'M.KHATOCO';
            # Plivo AUTH TOKEN
            $pwd = 'e5ymb';
            # SMS sender ID.
            $from = 'M.KHATOCO';

            # SMS text
            $sms = "Ban da dang ky tham gia CT Mua he Bali thanh cong."."\n"."Dien thoai cua Ban se nhan duoc ".number_format($amount, 0, ",", ".")." d trong giay lat.\nBan co them co hoi trung chuyen du lich Bali tri gia 20 trieu dong va nhieu giai thuong gia tri khac se duoc cong bo ngay 19/5/2019.\nMa so du thuong cua ban la: \n".$allPinCode."\nCSKH 1800585860-mien phi-gio HC\nVUI LONG GIU LAI TIN NHAN DE DOI CHIEU TRONG TRUONG HOP TRUNG THUONG.";

            $url = 'http://sms.vietguys.biz/api/?u='.$u.'&pwd='.$pwd.'&from='.$from.'&phone='.$phone.'&sms='. urlencode($sms);

            $data = array("from" => $from, "phone" => $phone, "sms" => $sms);
            $string = json_encode($data);

            $ch=curl_init($url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $string);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_USERPWD, $u . ":" . $pwd);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8','Content-length: '.strlen($string)));
            $result = curl_exec( $ch );
            curl_close($ch);
        }
    }

    if(!isset($errors['status']) || $errors['status'] != 'errors'){
        $errors['status'] = 'success';
        $errors['message'] = '<div class="text-center">Điện thoại của bạn sẽ nhận được '.floatval($amount/1000).'k trong giây lát. <br/><br/> Bạn còn cơ hội nhận thêm giải thưởng là chuyến du lịch Mùa Hè cặp đôi Bali sẽ được công bố ngày <span style="color: red">19/05/2019</span> <br/><br/> Cảm ơn bạn đã đăng ký tham gia chương trình.</div>';
    }

    echo json_encode($errors);
    wp_die();
}


function remove_date_column( $columns ) {
  unset($columns['date']);
  return $columns;
}

function remove_date_column_init() {
  add_filter( 'manage_posts_columns' , 'remove_date_column' );
}
add_action( 'admin_init' , 'remove_date_column_init' );

add_filter( 'post_row_actions', 'remove_row_actions', 10, 1 );
function remove_row_actions( $actions )
{
    if( get_post_type() === 'cards' ){
       unset( $actions['trash'] );
       unset( $actions['inline hide-if-no-js'] ); 
    }
    return $actions;
}



?>

