<?php
include_once(get_template_directory() . '/post-types/order-more.php');
require_once get_template_directory() . '/includes/cart/CheckOutUtil.php';

/* Register Rate Type */
add_action('init', 'reg_post_type_orders');
function reg_post_type_orders()
{
    $labels = array(
        'name' => 'Đơn hàng',
        'singular_name' => 'Đơn hàng',
        'menu_name' => 'Đơn hàng',
        'all_items' => 'Tất cả đơn hàng',
        'add_new' => 'Thêm mới',
        'add_new_item' => 'Thêm mới đơn hàng',
        'edit_item' => 'Xem đơn hàng',
        'new_item' => 'Đơn hàng mới',
        'view_item' => 'Xem chi tiết',
        'search_items' => 'Tìm kiếm',
        'not_found' => 'Không tìm thấy đơn hàng nào',
        'not_found_in_trash' => 'Không có đơn hàng nào trong thùng rác',
        'view' => 'Xem đơn hàng'
    );

    $args = array(
        'labels' => $labels,
        'description' => 'Đơn hàng của khách hàng về sản phẩm',
        'public' => false,
        'publicly_queryable' => true,
        'exclude_from_search' => true,
        'show_in_nav_menus' => false,
        'show_ui' => true,
        'rewrite' => array(
            'slug' => 'don-hang',
            'with_front' => true
        ),
        'menu_position' => 5,
        'menu_icon' => 'dashicons-clipboard',
        'supports' => array('title'),
        'capability_type' => 'post',
        'capabilities' => array(
            'create_posts' => false, // Removes support for the "Add New" function
            

        ),
        'map_meta_cap' => true,
    );
    register_post_type('orders', $args);
    // flush_rewrite_rules();
}

add_action('admin_init', 'add_metabox_order');
function add_metabox_order()
{
    function display_metabox_order_general($post)
    {
        $post_id = $post->ID;
        $order_info = get_post_meta($post_id, 'order_info', true);

        if (!isset($order_info['products']) || empty($order_info['products']) || empty($order_info)) {
            echo 'Đơn hàng bị huỷ do bị xoá hết sản phẩm!';
            return false;
        }

        $order_code = get_post_meta($post_id, 'order_code', true);
        $order_status = get_post_meta($post_id, 'order_status', true);
        $order_payment_status = get_post_meta($post_id, 'order_payment_status', true);
        $order_payment_status_text = ($order_payment_status == true) ? ' <b>(Đã thanh toán)</b>' : ' <b>(Chưa thanh toán)</b>';
        $order_payment_method = get_post_meta($post_id, 'order_payment_method', true);
        $order_user_id = get_post_meta($post_id, 'order_user_id', true);
        $csCode = get_user_meta($order_user_id, 'code_crm', true);
        $customer = array();
        if (($order_user_id > 0 || $order_user_id != '0') && ($order_user_id != 443 || $order_user_id != '443')) {
            $order_user_data = get_userdata($order_user_id);
          
            $customer = array(
                'fullname' => isset($order_info['fullname']) ? $order_info['fullname'] : '',
                'email' => $order_info['email'],
                'phone' => $order_info['phone'],
                'address' => $order_info['address'],  
            );

          


            $fullname = $customer['fullname'];
            if (empty($fullname)){
                $fullname = $order_user_data->display_name;
                $name = get_user_meta($order_user_id, 'name', true);
                if ($name) $fullname = $name;
                $name = get_user_meta($order_user_id, 'fullname', true);
                if ($name) $fullname = $name;
            }
            $customer['fullname'] = $fullname;
        } else {
            $customer = $order_info['customer'];
        }
        if (!isset($customer['fullname'])) {
            $customer['fullname'] = 'N/A';
        }

        $receiver = get_post_meta($post_id, 'receiver', true);

        $campaign_influence = get_post_meta($post_id, 'campaign_influence', true);
        if ($campaign_influence == '' || $campaign_influence == '0')
            $campaign_influence = false;
        else
            $campaign_influence = true;
        $campaignInfo = get_post_meta($post_id, 'campaignInfo', true);

        $hasCoupon = get_post_meta($post_id, 'hasCoupon', true);
        $couponName = get_post_meta($post_id, 'couponName', true);
        $couponSaleoffValue = get_post_meta($post_id, 'couponSaleoffValue', true);
        $isVNECODE = get_post_meta($post_id,'couponIsVneCode',true);
        $updatedToEgift = get_post_meta($post_id,'updated',true);

        $isVip = get_post_meta($post_id, 'isVip', true);
        $vipAfter = get_post_meta($post_id, 'vipAfter', true);
        ?>
        <style type="text/css">
            .form_inner_table {
                border-collapse: collapse;
            }

            .form_inner_table, .form_inner_table td, .form_inner_table th {
                border: 1px solid rgb(165, 165, 165);
                padding: 20px 10px;
                width: auto;
            }

            #color_list th {
                width: 120px
            }
            .btn_submit_status_order_to_vne{
                position: absolute;
                left: 81%;
                top: 1%;
            }
        </style>
        <div class="modal fade" id="annoucement" tabindex="-1" role="dialog" aria-labelledby="exampleModal3Label" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModal3Label">THÔNG BÁO LỖI</h5>
                <button type="button" class="close" data-dismiss="modal" style="margin-top: -30px!important;" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <p id="content-annoucement"></p>
              </div>
             
            </div>
          </div>
        </div>

        <input type="hidden" name="changed" id="changed" value="0">

        <table id="color_list" class="form-table">
            <tbody>
            <tr>
                <th scope="row">Mã đơn hàng: <?php echo $couponName; ?></th>
                <td><input type="text" name="order_code" id="order_code" value="<?php echo $order_code; ?>" disabled="">
                </td>
              
                <?php if ($hasCoupon): ?>
                    <?php if ($couponName == 'ktcvne' || $couponName == 'KTCVNE' ): ?>
                        <?php if(  $updatedToEgift == '' || $updatedToEgift == '0' || $updatedToEgift == 0): ?>
                        <td id="btn-update-field" value="<?php echo get_the_ID() ?>"> 

                            <button type="button" id="submit_status_order_to_vne" class="button button-primary btn_submit_status_order_to_vne" value="<?php echo $isVNECODE ?>"> Cập Nhật Trên EGIFT</button>
                            
                        </td>
                        <?php else: ?>
                        <td id="updatec_field">
                            <button type="button" id="updated_status_order_to_vne" disabled="disabled" class="btn btn-success btn_submit_status_order_to_vne "> Đã gửi lên EGIFT</button>
                        </td>
                        <?php endif ?>
                    <?php endif; ?>
                <?php endif; ?>
            </tr>
            <tr>
                <th scope="row">Trạng thái:</th>
                <td><?php
                    switch ($order_status) {
                        case 'neworder':
                            echo 'Mới đặt hàng' . $order_payment_status_text;
                            break;
                        case 'pending':
                            echo 'Đang xử lý tại kho' . $order_payment_status_text;
                            break;
                        case 'shipping':
                            echo 'Đang giao hàng' . $order_payment_status_text;
                            break;
                        case 'not_available':
                            echo 'Hết hàng' . $order_payment_status_text;
                            break;
                        case 'not_receiving':
                            echo 'Khách hàng không nhận' . $order_payment_status_text;
                            break;
                        case 'return':
                            echo 'Hàng trả về kho' . $order_payment_status_text;
                            break;
                        case 'cancel':
                            echo 'Hủy đơn hàng' . $order_payment_status_text;
                            break;
                        case 'done':
                            echo 'Đã hoàn thành' . $order_payment_status_text;
                            break;
                    }
                    ?></td>
            </tr>
            <?php
            global $wpdb;
            $query = "SELECT * FROM `khatoco_order_status` WHERE `order_id` = $post_id GROUP BY `order_status`";
            $result = $wpdb->get_results($query . " ORDER BY `created_date` DESC");
            if ($result) {
                ?>
                <tr>
                    <th>Chi tiết trạng thái:</th>
                    <td>
                        <table class="form_inner_table" id="products_table">
                            <tr>
                                <th style="width: 200px">Ngày - Giờ</th>
                                <th>Mô Tả</th>
                                <th>Thực Hiện Bởi</th>
                            </tr>
                            <?php
                            foreach ($result as $key => $value) {
                                ?>
                                <tr>
                                    <td><?php echo get_date_from_gmt( date( 'Y-m-d H:i:s', strtotime($value->created_date) ), 'd-m-Y - H:i:s' ); /*date("d-m-Y - H:i:s", strtotime($value->created_date))*/ ?></td>
                                    <td><?php
                                        switch ($value->order_status) {
                                            case 'neworder':
                                                echo 'Mới đặt hàng';
                                                break;
                                            case 'verify':
                                                echo 'Đã xác nhận';
                                                break;
                                            case 'pending':
                                                echo 'Đang xử lý tại kho';
                                                break;
                                            case 'shipping':
                                                echo 'Đang giao hàng';
                                                break;
                                            case 'not_available':
                                                echo 'Hết hàng';
                                                break;
                                            case 'not_receiving':
                                                echo 'Khách hàng không nhận';
                                                break;
                                            case 'return':
                                                echo 'Hàng trả về kho';
                                                break;
                                            case 'cancel':
                                                echo 'Hủy đơn hàng';
                                                break;
                                            case 'done':
                                                echo 'Đã hoàn thành';
                                                break;
                                            case 'verify':
                                                echo 'Đã xác nhận';
                                                break;
                                        }
                                        ?>
                                    </td>
                                    <td><?php   $user = get_userdata($value->order_user_id);
                                                echo $user->display_name.' ('. $user->user_nicename .')';           
                                    ?></td>
                                </tr>
                                <?php
                            }
                            ?>
                        </table>
                    </td>
                </tr>
                <?php
            }
            ?>
            <tr>
                <th scope="row">Ngày khởi tạo đơn hàng:</th>
                <td><?php echo get_the_time('d/m/Y H:i:s', $post); ?></td>
            </tr>
            <tr>
                <th scope="row">Phương thức thanh toán:</th>
                <td><?php
                    if ($order_payment_method == 2) {
                        echo 'ATM ngân hàng nội địa';
                    } elseif ($order_payment_method == 3) {
                        echo 'Visa/Master Card';
                    } else {
                        echo 'Tiền mặt';
                    }
                    ?></td>
            </tr>
            <?php if ($campaign_influence): ?>
                <tr>
                    <th scope="row">Chương trình khuyến mãi:</th>
                    <td><?php
                        echo 'Giảm giá : '.aj_format_number($campaignInfo['decrease']).' Đ';
                        $campaignInfo['listBonus']['listProdBonus'] = empty($campaignInfo['listBonus']['listProdBonus']) ? [] : $campaignInfo['listBonus']['listProdBonus'];
                        foreach ($campaignInfo['listBonus']['listProdBonus'] as $p):
                            if ($p['quantity'] > 0 ) {
                                echo '<div class="campaign-prod">+ '.$p['post_title'].' <span> ['.$p['size'].'] '.$p['quantity'].' x 0 Đ</span></div>';
                            }
                        endforeach;

                        $campaignInfo['listSpecialPrice']['listProdBonus'] = empty($campaignInfo['listSpecialPrice']['listProdBonus']) ? [] : $campaignInfo['listSpecialPrice']['listProdBonus'];
                        foreach ($campaignInfo['listSpecialPrice']['listProdBonus'] as $p):
                            if ($p['quantity'] > 0 ) {
                                echo '<div class="campaign-prod">+ '.$p['post_title'].' <span> ['.$p['size'].'] '.$p['quantity'].' x '.aj_format_number($p['price']).' Đ</span></div>';
                            }
                        endforeach;
                        ?>
                    </td>
                </tr>

                <tr>
                    <th scope="row">Chi tiết - khuyến mãi:</th>
                    <td><?php
                        echo '<div class=show-more><span class="detail">Chi tiết <i class="fa fa-angle-double-down" aria-hidden="true"></i></span><content class="content">';
                        foreach ($campaignInfo['detail'] as $d):
                             if (intval($d['value']) > 0 || $d['type'] == 'multi_product' || $d['type'] == 'buy_money_product') :
                            echo '<div class="campaign-prod title"><strong>'.$d['title'].'</strong></div>';
                            echo '<div class="campaign-prod"><strong>- Mô tả chương trình : </strong>'.$d['description'].'</div>';
                            echo '<div class="campaign-prod"><strong>- Bắt đầu ngày : </strong>'.$d['dateAfter'].'</div>';
                            echo '<div class="campaign-prod"><strong>- Kết thúc ngày : </strong>'.$d['dateBefore'].'</div>';
                            if (intval($d['value']) > 0){
                                echo '<div class="campaign-prod"><strong>- Giảm: </strong>'.aj_format_number($d['value']).' Đ</div>';
                            }
                             endif;
                        endforeach;
                        echo '</content></div>';
                        ?>
                    </td>
                </tr>

            <?php endif; ?>
            <?php if ($isVip): ?>
                <tr>
                    <th scope="row">Thẻ thành viên:</th>
                    <td><input type="hidden" name="vip" id="vip"
                               value="<?php echo $vipAfter; ?>"><?php echo (1 - $vipAfter) * 100 . '%'; ?></td>
                </tr>
            <?php endif; ?>
            <?php if ($hasCoupon): ?>
                <tr>

                    <th scope="row">Coupon: </th>
                    <td><input type="hidden" name="coupon" id="coupon"
                               value="<?php echo $couponName; ?>"><?php echo $couponName . ' (giảm giá ' . aj_format_number($couponSaleoffValue) . 'VNĐ)'; ?>
                    </td>
                </tr>
            <?php endif; ?>
            <tr>
                <th scope="row">Thành tiền (trước khi trừ giảm giá):</th>
                <td><input type="hidden" name="total" 
                           value="<?php echo $order_info['orderInfo']['totalBill']; ?>"><strong><?php echo aj_format_number($order_info['orderInfo']['totalBill']) . 'VNĐ'; ?></strong>
                </td>
            </tr>
            <tr>
                <th scope="row">Giá trị đơn hàng:</th>
                <?php $order_total = get_post_meta($post_id, 'order_total', true); ?>
                <td><input type="hidden" name="total" id="total"
                           value="<?php echo $order_total; ?>"><strong><?php echo aj_format_number($order_total) . 'VNĐ'; ?></strong>
                </td>
            </tr>
            <tr>
                <th scope="row">Danh sách mặt hàng:</th>
                <td>
                 <span  id="counter" style="display: none" ><?php echo count($order_info['products']) ?></span>
                    <table class="form_inner_table" id="products_table">
                        <tr>
                            <th width="30px">STT</th>
                            <th>Hình</th>
                            <th>Tên</th>
                            <th>Màu</th>
                            <th>Kích thước</th>
                            <th>Số lượng</th>
                            <th>Tồn kho hiện tại</th>
                            <th width="100px">Đơn giá</th>
                        </tr>

                        <?php
                        $number = 0;

                        foreach ($order_info['products'] as $pkey => $p) {
                            $number++;
                            $onhands = get_post_meta($p['id'], 'post_onhands', true);
                            $store_with_onhand = get_post_meta($p['id'], 'stores_with_onhand', true); ?>

                            <tr class="each-product" data-product-id="<?php echo $p['id']; ?>">
                                <td><?php echo $number; ?> <a href="javascript:;" class="drop-product-item">Xoá</a></td>
                                <td><img style="width: 60px;" src="<?php echo $p['thumb']; ?>"/></td>
                                <td><a class="product_name<?php echo $number ?>" href="<?php echo get_permalink($p['id']); ?>"
                                       target="_blank"><?php echo $p['name']; ?></a></td>
                                <td><?php echo $p['color_code']; ?><br/><?php echo $p['color_name']; ?></td>
                                <td><?php $sizes = $p['all_sizes']; ?>
                                    <select name="products[<?php echo $pkey; ?>][size]"
                                            class="can-changed size"><?php foreach ($sizes as $skey => $s) {
                                            echo '<option value="' . $s . '" ' . selected($p['size'], $s, false) . '>' . $s . '</option>';
                                        } ?>
                                    </select></td>
                                <td><input class="product_qty<?php echo $number ?>" type="number" min="1" name="products[<?php echo $pkey; ?>][quantity]"
                                           class="can-changed quantity" value="<?php echo $p['quantity']; ?>"
                                           style="width:50px"></td>
                                <td><?php if (isset($store_with_onhand[$p['size']])) echo str_replace(array(',', '|'), array('<br>', ': '), $store_with_onhand[$p['size']]); ?></td>
                                <td><input class="product_price<?php echo $number ?>" type="hidden" name="products[<?php echo $pkey; ?>][price]"
                                           class="can-changed price"
                                           value="<?php echo $p['price_saleoff']['value']; ?>"><?php echo number_format($p['price_saleoff']['value'],0,'.','.'); ?> Đ
                                </td>
                            </tr>
                        <?php }
                        $specialPriceBuy = empty($campaignInfo['listSpecialPrice']['listProdBonus']) ? [] : $campaignInfo['listSpecialPrice']['listProdBonus'];
                        foreach ($specialPriceBuy as $sp) {
                            if ($sp['quantity'] > 0) :
                        $number++;
                        $onhands = get_post_meta($sp['ID'], 'post_onhands', true);
                        $store_with_onhand = get_post_meta($sp['ID'], 'stores_with_onhand', true); ?>
                        <tr class="each-product" data-product-id="<?php echo $sp['id']; ?>">
                            <td><?php echo $number; ?> <a href="javascript:;" class="drop-product-item">Xoá</a></td>
                            <td><img style="width: 60px;" src="<?php echo $sp['thumb']; ?>"/></td>
                            <td><a href="<?php echo get_permalink($sp['ID']); ?>"
                                   target="_blank"><?php echo $sp['post_title']; ?></a></td>
                            <td> ... </td>
                            <td><?php $sizes = $sp['all_size']; ?>
                                <select name="products[<?php echo $pkey; ?>][size]"
                                        class="can-changed size"><?php foreach ($sizes as $skey => $s) {
                                        echo '<option value="' . $s . '" ' . selected($sp['size'], $s, false) . '>' . $s . '</option>';
                                    } ?>
                                </select></td>
                            <td><input type="number" min="1" name="products[<?php echo $pkey; ?>][quantity]"
                                       class="can-changed quantity" value="<?php echo $sp['quantity']; ?>"
                                       style="width:50px"></td>
                            <td><?php if (isset($store_with_onhand[$p['size']])) echo str_replace(array(',', '|'), array('<br>', ': '), $store_with_onhand[$sp['size']]); ?></td>
                            <td><input type="hidden" name="products[<?php echo $pkey; ?>][price]"
                                       class="can-changed price"
                                       value="<?php echo $sp['price']; ?>"><?php echo number_format($sp['price'],0,'.','.'); ?> Đ
                            </td>
                        </tr>
                        <?php endif; }

                        $listBonus = empty($campaignInfo['listBonus']['listProdBonus']) ? [] :$campaignInfo['listBonus']['listProdBonus'];
                        foreach ($listBonus as $sp) {
                        if ($sp['quantity'] > 0) :
                        $number++;
                        $onhands = get_post_meta($sp['ID'], 'post_onhands', true);
                        $store_with_onhand = get_post_meta($sp['ID'], 'stores_with_onhand', true); ?>
                        <tr class="each-product" data-product-id="<?php echo $sp['id']; ?>">
                            <td><?php echo $number; ?> <a href="javascript:;" class="drop-product-item">Xoá</a></td>
                            <td><img style="width: 60px;" src="<?php echo $sp['thumb']; ?>"/></td>
                            <td><a href="<?php echo get_permalink($sp['ID']); ?>"
                                   target="_blank"><?php echo $sp['post_title']; ?></a></td>
                            <td> ... </td>
                            <td><?php $sizes = $sp['all_size']; ?>
                                <select name="products[<?php echo $pkey; ?>][size]"
                                        class="can-changed size"><?php foreach ($sizes as $skey => $s) {
                                        echo '<option value="' . $s . '" ' . selected($sp['size'], $s, false) . '>' . $s . '</option>';
                                    } ?>
                                </select></td>
                            <td><input type="number" min="1" name="products[<?php echo $pkey; ?>][quantity]"
                                       class="can-changed quantity" value="<?php echo $sp['quantity']; ?>"
                                       style="width:50px"></td>
                            <td><?php if (isset($store_with_onhand[$p['size']])) echo str_replace(array(',', '|'), array('<br>', ': '), $store_with_onhand[$sp['size']]); ?></td>
                            <td><input type="hidden" name="products[<?php echo $pkey; ?>][price]"
                                       class="can-changed price"
                                       value="0">0 Đ
                            </td>
                        </tr>
                        <?php endif; } ?>


                    </table>
                </td>
            </tr>
            <tr style="border-top: 1px solid #ccc;">
               
                <th scope="row">Mã khách hàng:</th>
                <td><input type="text" name="display_cus_name" value="<?php echo $csCode ?>" disabled="">
                </td>
            </tr>
            <tr style="border-top: 1px solid #ccc;">
                <th scope="row">Tên khách hàng:</th>
                <td><input type="text" name="display_cus_name" value="<?php echo empty($order_info['customer']['fullname']) ? $customer['fullname'] : $order_info['customer']['fullname']; ?>" disabled="">
                </td>
            </tr>
            <tr>
                <th scope="row">Email khách hàng:</th>
                <td><input type="text" name="display_name" value="<?php echo empty($order_info['customer']['email'])?$customer['email'] : $customer['email'] ; ?>" disabled=""></td>
            </tr>
            <tr>
                <th scope="row">SĐT khách hàng:</th>
                <td><input type="text" name="phone" class="can-changed" value="<?php echo empty($order_info['customer']['phone']) ? $customer['phone'] : $customer['phone'] ; ?>"></td>
            </tr>
            <tr style="border-top: 1px solid #ccc;">
                <th scope="row">Tỉnh/Thành Phố:</th>
                <td><input type="text" name="cus_province" value="<?php echo empty($order_info['customer']['province']['name']) ? '': $order_info['customer']['province']['name']; ?>" disabled="">
                </td>
            </tr>
            <tr>
                <th scope="row">Địa chỉ khách hàng:</th>
                <td><textarea name="address" class="can-changed"
                              style="width:100%"><?php echo empty($order_info['customer']['address']) ? $customer['address'] : $order_info['customer']['address']; ?></textarea></td>
            </tr>
            <?php if (!empty($receiver)): ?>
                <tr>
                    <th scope="row">Tên người nhận:</th>
                    <td><input type="text" name="receiver_cus_name" class="can-changed"
                               value="<?php echo isset($receiver['fullname']) ? $receiver['fullname'] : ''; ?>"></td>
                </tr>
                <tr>
                    <th scope="row">Email người nhận:</th>
                    <td><input type="text" name="receiver_name" class="can-changed"
                               value="<?php echo $receiver['email']; ?>"></td>
                </tr>
                <tr>
                    <th scope="row">SĐT người nhận:</th>
                    <td><input type="text" name="receiver_phone" class="can-changed"
                               value="<?php echo $receiver['phone']; ?>"></td>
                </tr>
                <tr>
                    <th scope="row">Địa chỉ người nhận:</th>
                    <td><textarea name="receiver_address" class="can-changed"
                                  style="width:100%"><?php echo $receiver['address']; ?></textarea></td>
                </tr>
            <?php else: ?>
                <tr>
                    <th scope="row">Người nhận:</th>
                    <td>Người gửi đồng thời là người nhận hàng</td>
                </tr>
            <?php endif; ?>

            <tr style="border-top: 1px solid #ccc;">
                <th scope="row">Ghi chú:</th>
                <td><?php echo isset($order_info['checkoutNote']) ? $order_info['checkoutNote']:''; ?></td>
            </tr>
            </tbody>
        </table>
        <script>
            jQuery(document).ready(function () {
                jQuery('.can-changed').change(function (event) {
                    if (jQuery(this).hasClass('quantity')) {
                        if (parseInt(jQuery(this).val()) < 1) {
                            alert('Số lượng sản phẩm tối thiểu là 1!');
                            event.preventDefault();
                            jQuery(this).val('1');
                            update_cart_total();
                            return false;
                        }
                    }
                    jQuery('#changed').val('1');
                    update_cart_total();
                });

                jQuery('.drop-product-item').click(function (event) {
                    event.preventDefault();
                    jQuery(this).parents('.each-product').find('td').hide();
                    jQuery(this).parents('.each-product').prepend('<td class="undo" colspan="7" style="text-align:center"><a href="javascript:;" class="confirm-product-item">Xác nhận</a> - <a href="javascript:;" class="undo-product-item">Quay lại</a></td>');
                    return false;
                });

                jQuery('.drop-gift').click(function (event) {
                    event.preventDefault();
                    if (confirm('Bạn đang xoá dịch vụ gói quà/thiệp. Bạn có chắc chắn muốn xoá không?')) {

                    }
                    else {
                        return false;
                    }
                });

                jQuery('.each-product').delegate('.confirm-product-item', 'click', function (event) {
                    event.preventDefault();
                    if (jQuery('#products_table').load().find('.each-product').length <= 1) {
                        if (confirm('Chỉ còn sản phẩm này trong giỏ hàng! Nếu xoá sản phẩm này, giỏ hàng sẽ tự động chuyển về trạng thái "Huỷ đơn hàng"!')) {
                            jQuery.ajax({
                                url: '<?php echo admin_url("admin-ajax.php"); ?>',
                                type: 'POST',
                                dataType: 'json',
                                data: {
                                    action: 'update_cart_status_ajax',
                                    cart_id: <?php echo $post_id; ?>,
                                    status: 'cancel'
                                }
                            })
                                .done(function (res) {
                                    alert(res.message);
                                });

                        }
                        else {
                            jQuery(this).parents('.each-product').remove();
                        }
                    }

                    return false;
                });

                jQuery('.each-product').delegate('.undo-product-item', 'click', function (event) {
                    event.preventDefault();
                    jQuery(this).parents('.each-product').find('td').show();
                    jQuery(this).parents('.undo').remove();
                    return false;
                });

                function update_cart_total() {
                    var t = jQuery('.each-product');
                    var total = 0;
                    if (t.length <= 0) {
                        // confirm('Không còn sản phẩm trong giỏ hàng! Bạn muốn huỷ đơn hàng?');
                    }
                    else {
                        jQuery(t).each(function (index, el) {
                            var quantity = parseInt(jQuery(el).find('.quantity').val());
                            var price = parseInt(jQuery(el).find('.price').val());

                            total += quantity * price;
                        });

                        jQuery('#totalCart').val(total);
                        jQuery('#totalCart').parent().text(format_currency(total));
                    }
                }

                function format_currency(x) {
                    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") + ' Đ';
                }
            });
        </script>
        <?php

        // echo '<pre>';
        // print_r ($order_info);
        // echo '</pre>';
        ?>
        <?php
    }

    add_meta_box(
        'metabox_order_general',
        'Thông tin đơn hàng',
        'display_metabox_order_general',
        'orders',
        'normal',
        'high'
    );

    function display_metabox_order_side($post)
    {
        $post_id = $post->ID;

        $order_verify = get_post_meta($post_id, 'order_verify', true);

        $order_status = get_post_meta($post_id, 'order_status', true);
        $order_payment = get_post_meta($post_id, 'order_payment_status', true);
        $order_pos = get_post_meta($post_id, 'order_pos', true);
        $transport_code = get_post_meta($post_id, 'transport_code', true);
        $transport_name = get_post_meta($post_id, 'transport_name', true);

        add_thickbox();

        if ($order_verify == '1') { ?>
            <div style="padding: 10px;">
                <input type="hidden" name="do" value="update">
                <?php $link_popup = get_template_directory_uri() . '/includes/admin-view-onhands.php?orderid=' . $post_id . '&TB_iframe=true&width=600&height=400';
                $title_popup = 'Xem tồn kho đơn hàng ' . get_post_meta($post_id, 'order_code', true);
                echo '<a href="' . $link_popup . '"  data-orderid="' . $post_id . '" title="' . $title_popup . '" class="button thickbox">Xem tồn kho trước</a>'; ?>
                <p style="text-align:center"><select name="order_status" id="order_status_changer" style="width:100%">
                        <option title="Vừa hoàn thành đơn hàng xong, QTĐH sẽ gọi điện confirm với khách"
                                value="neworder" <?php if ($order_status == 'neworder') echo 'selected=""'; ?> <?php if (is_author_m()) echo 'disabled=""'; ?>>
                            Mới đặt hàng
                        </option>
                        <option title="QTĐH confirm OK với khách, tìm kho để chuyển hàng"
                                value="pending" <?php if ($order_status == 'pending') echo 'selected=""'; ?>>Đang xử lý
                            tại kho
                        </option>
                        <option title="Ship đến, nhưng khách không nhận hàng"
                                value="not_receiving" <?php if ($order_status == 'not_receiving') echo 'selected=""'; ?> <?php if (is_author_m() && is_admin_author()) echo 'disabled=""'; ?>>
                            Khách hàng không nhận
                        </option>
                        <option title="Khách hàng đặt mua, nhưng không confirm"
                                value="cancel" <?php if ($order_status == 'cancel') echo 'selected=""'; ?> <?php if (is_author_m() && is_admin_author()) echo 'disabled=""'; ?>>
                            Hủy đơn hàng
                        </option>
                        <option title="Hoàn thành"
                                value="done" <?php if ($order_status == 'done') echo 'selected=""'; ?> <?php if (is_author_m() && is_admin_author()) echo 'disabled=""'; ?>>
                            Đã hoàn thành
                        </option>
                        <option title="Hàng trả về"
                                value="return" <?php if ($order_status == 'return') echo 'selected=""'; ?> <?php if (is_author_m() && is_admin_author()) echo 'disabled=""'; ?>>
                            Hàng trả về
                        </option>
                        <option title="QTK nhận được đơn hàng, đang chuyển hàng"
                                value="shipping" <?php if ($order_status == 'shipping') echo 'selected=""'; ?> <?php if (is_contributor_m() && is_admin_author()) echo 'disabled=""'; ?>>
                            Đang giao hàng
                        </option>
                        <option title="QTK trả lại đơn hàng cho QTĐH, do kho bị hết"
                                value="not_available" <?php if ($order_status == 'not_available') echo 'selected=""'; ?> <?php if (is_contributor_m() && is_admin_author()) echo 'disabled=""'; ?>>
                            Hết hàng
                        </option>
                    </select></p>

                <p style="text-align:center; <?php echo (in_array($order_status, array('pending', 'shipping'))) ? 'display: block;' : 'display: none;' ?>">
                    <select name="order_pos" id="order_pos"><?php $poss = get_option('pos_options');
                        if ($poss) foreach ($poss as $poskey => $pos) {
                            echo '<option value="' . $poskey . '" ' . selected($order_pos, $poskey, false) . '>' . $pos['name'] . '</option>';
                        } ?>
                    </select>
                </p>

                <p style="text-align:center; <?php echo (in_array($order_status, array('shipping'))) ? 'display: block;' : 'display: none;' ?>">
                    <input type="text" name="transport_code" id="transport_code" value="<?php echo $transport_code; ?>"
                           placeholder="Mã vận đơn"><br/><br/>
                    <?php
                    /*
                    VN Post: 1
                    Fast Delivery: 2
                    */
                    ?>
                    <select type="text" name="transport_name" id="transport_name"
                            placeholder="Đơn vị triển khai" style="width: 220px; text-align: center;">
                        <option value="0">Chọn Đơn Vị Triển Khai</option>
                        <option <?php if (isset($transport_name) && $transport_name == 1) echo 'selected="selected"' ?>
                            value="1">VN POST
                        </option>
                        <option <?php if (isset($transport_name) && $transport_name == 2) echo 'selected="selected"' ?>
                            value="2">Fast Delivery
                        </option>
                    </select>
                </p>

                <p style="text-align:center"><select name="order_payment" style="width:100%">
                        <option value="0" <?php echo selected($order_payment, 0); ?>>Chưa thanh toán</option>
                        <option value="1" <?php echo selected($order_payment, 1); ?>>Đã thanh toán</option>
                    </select></p>
                <p style="text-align:center"><input style="" type="submit" name="submit" id="submit"
                                                    class="button button-primary" value="Cập nhật"></p>
            </div>
            <script type="text/javascript">
                jQuery(document).ready(function ($) {
                    $('#order_status_changer').change(function (event) {
                        if ($(this).val() == 'pending' || $(this).val() == 'shipping') {
                            $('#order_pos').parent('p').show();

                            if ($(this).val() == 'shipping')
                                $('#transport_code').parent('p').show();
                            else
                                $('#transport_code').parent('p').hide();
                        }
                        else {
                            $('#order_pos').parent('p').hide();
                        }
                    });
                });
            </script>
            <div style="padding: 10px;">
                <h3 style="padding:8px 0"><strong>Các trạng thái đơn hàng</strong></h3>
                <hr>
                <ul style="list-style:inside">
                    <li><strong>Mới đặt hàng</strong>: Vừa hoàn thành đơn hàng xong, QTĐH sẽ gọi điện confirm với khách
                    </li>
                    <li><strong>Đang xử lý tại kho</strong>: QTĐH confirm OK với khách, tìm kho để vc</li>
                    <li><strong>Đang giao hàng</strong>: QTK nhận dc đơn hàng, đang ship</li>
                    <li><strong>Hết hàng</strong>: QTK trả lại đơn hàng cho QTĐH, do kho bị hết</li>
                    <li><strong>Không nhận hàng</strong>: Ship đến, nhưng khách ko nhận hàng</li>
                    <li><strong>Hủy</strong>: Khách hàng đặt mua, nhưng ko confirm</li>
                    <li><strong>Hoàn thành</strong>: Đơn hàng hoàn thành. Xác nhận đơn hàng hoàn thành cũng đồng thời
                        xác nhận đơn hàng đã thanh toán
                    </li>
                </ul>
                <p><strong><i>Ghi chú:</i></strong></p>
                <p><i>QTĐH: Quản trị đơn hàng</i></p>
                <p><i>QTK: Quản trị kho</i></p>
            </div>

            <hr>

            <?php $user_update_stack = get_post_meta($post_id, 'user_update_stack', true);
            if ($user_update_stack != '') {
                $user_update_stack_arr = explode(',', $user_update_stack);
                if (!empty($user_update_stack_arr)) {
                    $user_id = $user_update_stack_arr[count($user_update_stack_arr) - 1];
                    echo 'Người cập nhật cuối cùng: <a href="' . get_edit_user_link($user_id) . '" title="">' . get_userdata($user_id)->display_name . '</a> lúc ' . get_post($post_id)->post_modified;
                }
            }

        } else { ?>

            <input type="hidden" name="verify" value="1">
            <p style="text-align: center;">Vui lòng verify đơn hàng để có thể thực hiện các bước tiếp theo</p>
            <p style="text-align: center;">
                <button type="submit" class="button">Verify order</button>
            </p>

            <p style="text-align: center;">
                <button type="submit" class="button cancel-order">Hủy đơn hàng</button>
                <input type="hidden" name="order_status" value="">
                <script>jQuery(document).ready(function($){
                            $('.cancel-order').on('click',function(){
                                $('input[name="order_status"]').val('cancel');
                            });
                    });</script>
            </p>

        <?php }
    }

    add_meta_box(
        'metabox_order_side',
        'Trạng thái đơn hàng',
        'display_metabox_order_side',
        'orders',
        'side',
        'high'
    );

    remove_meta_box('submitdiv', 'orders', 'side');
}

add_action('save_post', 'save_metabox_order');
function save_metabox_order($post_id)
{
    // update_post_meta($post_id,'updated','1');
    // $update = get_post_meta($post_id,'update',true);
    // if($update == '0'){
    //     update_post_meta($post_id,'updated','1');
    // }
    $user_update_stack = get_post_meta($post_id, 'user_update_stack', true);
    update_post_meta($post_id, 'user_update_stack', $user_update_stack . ',' . get_current_user_id());
    
    if (get_post_type() == 'orders' && !empty($_POST['order_status']) && $_POST['order_status']==='cancel' && !empty($_POST['verify'])){ // Hủy đơn hàng - lần click đầu tiên (khi nút verify order còn hiện)
        update_post_meta($post_id, 'order_verify', '1');
       
        update_post_meta($post_id, 'order_status', 'cancel');
        $user_id = get_post_meta($post_id, 'order_user_id', true);
        if ($user_id != 0 && $user_id != '')
            mail__announce_orders_cancel(get_userdata($user_id), get_post($post_id));
        return;
    }

    if (get_post_type() == 'orders' && isset($_POST['verify'])) {

        update_post_meta($post_id, 'order_verify', '1');
        
        if (!empty($_POST['order_status']) && $_POST['order_status']==='cancel'){ // Hủy đơn hàng
            update_post_meta($post_id, 'order_status', 'cancel');
            $user_id = get_post_meta($post_id, 'order_user_id', true);
            if ($user_id != 0 && $user_id != '')
                mail__announce_orders_cancel(get_userdata($user_id), get_post($post_id));
            return;
        }
        
        // Update onhand
        $order_info = get_post_meta($post_id, 'order_info', true);
        foreach ($order_info['products'] as $pkey => $details) {
            $id = intval($details['id']);
            $quantity = intval($details['quantity']);
            $size = $details['size'];
            $price = intval($details['price_saleoff']);

            $product_onhands = get_post_meta($id, 'post_onhand', true);
            $product_onhands_all = get_post_meta($id, 'post_onhand_all', true);
            if (isset($product_onhands[$size])) {
                $product_onhands[$size] = $product_onhands[$size] - $quantity;
                update_post_meta($id, 'post_onhand', $product_onhands);
            }

            update_post_meta($id, 'post_onhand_all', $product_onhands_all - $quantity);
        }
    } else {
        if (isset($_POST['do']) && $_POST['do'] == 'update') {  
            
            /*$new_order_info = get_post_meta($post_id, 'order_info', true);
            $order_has_restored = get_post_meta($post_id, 'order_has_restored', true);
            // Restore
            if( in_array( $_POST['order_status'], array( 'cancel', 'not_receiving' ) ) && $order_has_restored != 'true'){
                foreach ($new_order_info['products'] as $pkey => $details) {
                    $id = intval($details['id']);
                    $quantity = intval($details['quantity']);
                    $size = $details['size'];

                    $product_onhands = get_post_meta( $id, 'post_onhand', true );
                    if( isset($product_onhands[$size]) ){
                        $product_onhands[$size] = $product_onhands[$size] + $quantity;
                        update_post_meta( $id, 'post_onhand', $product_onhands );
                        update_post_meta( $post_id, 'order_has_restored', 'true' );
                    }
                }
            }*/

            if (in_array($_POST['order_status'], array('neworder', 'pending', 'shipping', 'not_available', 'not_receiving', 'cancel', 'done'))) {
                update_post_meta($post_id, 'order_status', $_POST['order_status']);

                if ($_POST['order_status'] == 'done') {
                    update_post_meta($post_id, 'order_payment_status', true);
                    // update_post_meta($post_id, 'order_payment_code', true);
                }

                if ($_POST['order_status'] == 'cancel') {
                    $user_id = get_post_meta($post_id, 'order_user_id', true);
                    if ($user_id != 0 && $user_id != '')
                        mail__announce_orders_cancel(get_userdata($user_id), get_post($post_id));
                }

                if (in_array($_POST['order_status'], array('pending'))) {
                    update_post_meta($post_id, 'order_pos', $_POST['order_pos']);
                    // Push orders bill to CRM
                    $r = sync_push_orders($post_id);
                     //var_dump($r);
                     //exit;
                    if ($r !== true) {
                        //wp_die( 'Tiến trình đẩy đơn hàng lên CRM xảy ra lỗi<br>Lỗi: '. $r  );
                        //exit;
                    }
                }

                if ($_POST['order_status'] == 'shipping') {
                    update_post_meta($post_id, 'transport_code', $_POST['transport_code']);
                    update_post_meta($post_id, 'transport_name', $_POST['transport_name']);
                }
            }

            if (isset($_POST['order_payment'])) {
                if ($_POST['order_payment'] == '1') {
                    update_post_meta($post_id, 'order_payment_status', true);
                    // update_post_meta($post_id, 'order_payment_code', true);
                } else {
                    update_post_meta($post_id, 'order_payment_status', false);
                    // update_post_meta($post_id, 'order_payment_code', false);
                }
            }

            if (isset($_POST['changed']) && $_POST['changed'] == '1') {
                // Old values
                $order_info = get_post_meta($post_id, 'order_info', true);
                $customer = get_post_meta($post_id, 'customer', true);

                $products = isset($_POST['products']) ? $_POST['products'] : array();
                $products_keys = array_keys($products);
                $phone = isset($_POST['phone']) ? $_POST['phone'] : $order_info['phone'];
                $address = isset($_POST['address']) ? $_POST['address'] : $order_info['address'];

                $total = 0;
                foreach ($order_info['products'] as $pkey => $p) {
                    if (in_array($pkey, $products_keys)) {
                        $order_info['products'][$pkey]['size'] = $products[$pkey]['size'];
                        $order_info['products'][$pkey]['quantity'] = $products[$pkey]['quantity'];
                        $total += $products[$pkey]['quantity'] * $products[$pkey]['price'];
                    } else {
                        unset($order_info['products'][$pkey]);
                    }
                }
                $order_info['total_formated'] = aj_format_number($total) . ' Đ';

                $order_info['phone'] = $phone;
                $order_info['address'] = $address;

                if (!isset($order_info['products']) || empty($order_info['products']) || empty($order_info)) {
                    update_post_meta($post_id, 'order_status', 'cancel');
                } else {
                    update_post_meta($post_id, 'order_info', $order_info);

                    update_post_meta($post_id, 'customer', array(
                        'email' => $customer['email'],
                        'phone' => $phone,
                        'address' => $address
                    ));

                    update_post_meta($post_id, 'receiver', array(
                        'email' => $_POST['receiver_email'],
                        'phone' => $_POST['receiver_phone'],
                        'address' => $_POST['receiver_address'],
                    ));
                }
            }
        }
    }
}


// add filter to admin posts list
add_action('restrict_manage_posts', 'admin_posts_filter_restrict_manage_posts');
function admin_posts_filter_restrict_manage_posts()
{
    $post_type = 'post';
    if (isset($_GET['post_type']))
        $post_type = $_GET['post_type'];

    if ($post_type == 'orders') { ?>
        <select name="filter_by_payment_status" id="filter_by_payment_status">
            <option value="">Tình trạng đơn hàng</option>
            <option
                value="neworder" <?php echo (isset($_GET['filter_by_payment_status']) && $_GET['filter_by_payment_status'] == 'neworder') ? 'selected=""' : ''; ?>>
                Mới đặt hàng
            </option>
            <option
                value="pending" <?php echo (isset($_GET['filter_by_payment_status']) && $_GET['filter_by_payment_status'] == 'pending') ? 'selected=""' : ''; ?>>
                Đang xử lý tại kho
            </option>
            <option
                value="shipping" <?php echo (isset($_GET['filter_by_payment_status']) && $_GET['filter_by_payment_status'] == 'shipping') ? 'selected=""' : ''; ?>>
                Đang giao hàng
            </option>
            <option
                value="not_available" <?php echo (isset($_GET['filter_by_payment_status']) && $_GET['filter_by_payment_status'] == 'not_available') ? 'selected=""' : ''; ?>>
                Hết hàng
            </option>
            <option
                value="not_receiving" <?php echo (isset($_GET['filter_by_payment_status']) && $_GET['filter_by_payment_status'] == 'not_receiving') ? 'selected=""' : ''; ?>>
                Khách hàng không nhận
            </option>
            <option
                value="return" <?php echo (isset($_GET['filter_by_payment_status']) && $_GET['filter_by_payment_status'] == 'return') ? 'selected=""' : ''; ?>>
                Hàng trả về kho
            </option>
            <option
                value="cancel" <?php echo (isset($_GET['filter_by_payment_status']) && $_GET['filter_by_payment_status'] == 'cancel') ? 'selected=""' : ''; ?>>
                Hủy đơn hàng
            </option>
            <option
                value="done" <?php echo (isset($_GET['filter_by_payment_status']) && $_GET['filter_by_payment_status'] == 'done') ? 'selected=""' : ''; ?>>
                Đã hoàn thành
            </option>
        </select>

        <?php add_thickbox(); ?>
        <div id="view-onhands-iframe" style="display:none;">
            <?php $order_id = isset($_GET['orderid']) ? $_GET['orderid'] : 0; ?>
            <p>Đơn hàng: <?php echo $order_id; ?></p>
        </div>

        <style type="text/css">
            .widefat thead tr th#date {
                width: 8%;
            }

            .widefat thead tr th#cta {
                width: 5%;
            }
        </style>
    <?php }
}

add_action('parse_query', 'custom_post_filter');
function custom_post_filter($query)
{
    global $pagenow;
    $post_type = 'post';
    if (isset($_GET['post_type']))
        $post_type = $_GET['post_type'];

    if ('orders' == $post_type && is_admin() && $pagenow == 'edit.php' && isset($_GET['filter_by_payment_status']) && $_GET['filter_by_payment_status'] != '') {
        $query->query_vars['meta_key'] = 'order_status';
        $query->query_vars['meta_value'] = $_GET['filter_by_payment_status'];
    }
}

add_filter('manage_edit-orders_columns', 'post_orders_columns_head');
function post_orders_columns_head($defaults)
{
    $defaults = array_slice($defaults, 0, 2, true) + array('payment-status' => 'Trạng thái', 'price' => 'Giá trị đơn hàng') + array_slice($defaults, 2, count($defaults) - 1, true);
    // $defaults = array_slice($defaults, 0, 4, true) + array('order_payment_transaction' => 'Mã thanh toán', 'order_pos' => 'POS', 'transport_code' => 'Mã bưu gửi', 'transport_name' => 'Đơn vị vận đơn') + array_slice($defaults, 4, count($defaults) - 1, true);
    $defaults = array_slice($defaults, 0, 4, true) + array('order_payment_transaction' => 'Mã thanh toán', 'order_pos' => 'POS', 'transport_code' => 'Mã bưu gửi') + array_slice($defaults, 4, count($defaults) - 1, true);
    $defaults = $defaults + array('cta' => 'Tồn kho');
    return $defaults;
}

add_action('manage_orders_posts_custom_column', 'post_series_custom_column', 10, 2);
function post_series_custom_column($column, $post_id)
{
    if (get_post_type($post_id) == 'orders') {
        switch ($column) {
            case 'payment-status':
                $order_status = get_post_meta($post_id, 'order_status', true);
                $order_payment_status = get_post_meta($post_id, 'order_payment_status', true);
                // $order_payment_code = get_post_meta($post_id, 'order_payment_code', true);
                // $order_payment_status_text = ($order_payment_status == true && $order_payment_code) ? ' <b style="color:green">(Đã thanh toán)</b>' : ' <b style="color:red">(Chưa thanh toán)</b>';
                $order_payment_status_text = '<br>';
                $order_payment_status_text .= ($order_payment_status == true) ? ' <b style="color:green">(Đã thanh toán)</b>' : ' <b style="color:red">(Chưa thanh toán)</b>';

                switch ($order_status) {
                    case 'neworder':
                        echo '<span style="color:brown">Mới đặt hàng</span>' . $order_payment_status_text;
                        break;
                    case 'pending':
                        echo '<span style="color:brown">Đang xử lý tại kho</span>' . $order_payment_status_text;
                        break;
                    case 'shipping':
                        echo '<span style="color:brown">Đang giao hàng</span>' . $order_payment_status_text;
                        break;
                    case 'not_available':
                        echo '<span style="color:brown">Hết hàng</span>' . $order_payment_status_text;
                        break;
                    case 'not_receiving':
                        echo '<span style="color:brown">Khách hàng không nhận</span>' . $order_payment_status_text;
                        break;
                    case 'return':
                        echo '<span style="color:red">Hàng trả về kho</span>' . $order_payment_status_text;
                        break;
                    case 'cancel':
                        echo '<span style="color:red">Hủy đơn hàng</span>' . $order_payment_status_text;
                        break;
                    case 'done':
                        echo '<span style="color:green">Đã hoàn thành</span>' . $order_payment_status_text;
                        break;
                }
                break;
            case 'price':
                $price = get_post_meta($post_id, 'order_total', true);
                echo '<strong>' . number_format(doubleval($price), 0, ',', '.') . '</strong>';
                break;
            case 'order_payment_transaction':
                echo get_post_meta($post_id, 'order_payment_transaction', true);
                break;
            case 'order_pos':
                echo get_post_meta($post_id, 'order_pos', true);
                break;
            case 'transport_code':
                $link_delivery = '';
                $name_delivery = get_post_meta($post_id, 'transport_name', true);
                if ($name_delivery == '2') {
                    $link_delivery = 'https://5sao.ghn.vn/Tracking/ViewTracking/' . get_post_meta($post_id, 'transport_code', true);
                } else {//if( $name_delivery == '1'){
                    $link_delivery = 'http://www.vnpost.vn/vi-vn/dinh-vi/buu-pham?key=' . get_post_meta($post_id, 'transport_code', true);
                }
                echo '<a target="_blank" href="' . $link_delivery . get_post_meta($post_id, 'transport_code', true) . '">' . get_post_meta($post_id, 'transport_code', true) . '</a>';
                break;
            // case 'transport_name':
            //     echo '<a target="_blank" href="https://5sao.ghn.vn/Tracking/ViewTracking/' . get_post_meta($post_id, 'transport_name', true) . '">' . get_post_meta($post_id, 'transport_name', true) . '</a>';
            //     break;
            case 'cta':
                // $link_popup = '#TB_inline?orderid='. $post_id .'&width=600&height=400&inlineId=view-onhands-iframe';
                $link_popup = get_template_directory_uri() . '/includes/admin-view-onhands.php?orderid=' . $post_id . '&TB_iframe=true&width=600&height=400';
                $title_popup = 'Xem tồn kho đơn hàng ' . get_post_meta($post_id, 'order_code', true);
                echo '<a href="' . $link_popup . '"  data-orderid="' . $post_id . '" title="' . $title_popup . '" class="button thickbox">Xem</a>';
                break;
            default:
                break;
        }
    }
}

add_filter('post_row_actions', 'remove_quick_edit_order_post_list', 10, 2);
function remove_quick_edit_order_post_list($actions, $post)
{
    if ('orders' == $post->post_type) {
        $actions['edit'] = '<a href="' . get_edit_post_link($post->ID) . '">View orders</a>';
        unset($actions['inline hide-if-no-js']);
    }
    return $actions;
}

add_filter("manage_edit-orders_sortable_columns", "orders_sortable_columns");
function orders_sortable_columns()
{
    return array(
        'payment-status' => 'payment-status',
        'price' => 'price'
    );
}


/* Cart Service */
add_action('wp_ajax_cart_service_ajax', 'cart_service_ajax');
add_action('wp_ajax_nopriv_cart_service_ajax', 'cart_service_ajax');
function cart_service_ajax()
{
    $do_ajax = isset($_GET['do_ajax']) ? esc_js(sanitize_text_field($_GET['do_ajax'])) : '';

    if ($do_ajax == 'get_cart_info') {
        $cart_info = get_cart_info_from_session();
        echo json_encode($cart_info);
        exit;
    }

    if ($do_ajax == 'get_user_info') {
        echo json_encode(CartSession::getUserInfoFromDBFillToCart());
        exit;
    }

    if ($do_ajax == 'get_static_info') {
        echo json_encode(array(
            // 'stores' => get_all_stores(),
            'gift_method_fee' => array(
                'box' => (int)get_option('wrap_fee'),
                'card' => (int)get_option('card_fee'),
            ),
            // 'transport_methods' => get_transport_methods()
        ));
        exit;
    }

    if ($do_ajax == 'get_campaign_info') {
        $runningCamp = CampUtils::getRunningCamp();
        if (!empty($runningCamp)) {
            echo json_encode(array('result' => 1, 'return' => $runningCamp));
        } else {
            echo json_encode(array('result' => 0));
        }
        exit;
    }

    if ($do_ajax == 'parse_camp') {
        echo json_encode(CampUtils::parseCampPriced());
        exit;
    }

    if ($do_ajax == 'remove_from_cart') {
        unset($_SESSION['cart']['products'][$_GET['cart_id']]);
        echo json_encode(CartSession::updateCartSession($_SESSION['cart'])); // Sau khi remove sản phẩm trong giỏ hàng - update lại thông tin cart
        exit;
    }

    if ($do_ajax == 'get_transport_fee') {
        if (isset($_GET['weight']) && isset($_GET['transport_type'])) {
            $weight = (int)$_GET['weight'];
            $provinceid = (int)$_GET['provinceid'];
            $transport_type = (int)$_GET['transport_type'];
            $transport_type = $transport_type == 2 ? $transport_type : 1;

            $region = 3;
            if (in_array($provinceid, array(52, 48, 64, 62, 46, 54, 44, 49, 51, 45))) {
                $region = 1;
            } elseif ($provinceid == 56) {
                $region = 0;
            }

            $transport_fee = get_transport_fee($transport_type, $weight, $region);

            echo json_encode(array(
                'transport_type' => $transport_type,
                'transport_fee' => $transport_fee,
                'transport_formated' => aj_format_number($transport_fee) . ' Đ',
                'transport_weight' => $weight
            ));
            exit;
        }
    }

    if ($do_ajax == 'check_payment_method') {
        $cart = stripslashes($_GET['cart']);
        $cart = json_decode($cart, true);

        echo json_encode(CartSession::updateCartSession($cart));
        exit;
    }

    if ($do_ajax == 'view-cart') {
        CartSession::viewCartSession();
    }
    if ($do_ajax == 'unset-cart') {
        session_start();
// Unset all of the session variables.
        $_SESSION = array();
        session_unset();
        print_r(session_destroy());

//        CartSession::detroyCartSession();
    }
    if ($do_ajax == 'verify_list_list_bonus_special') {
        $cart = json_decode(file_get_contents('php://input'), true)['cart'];
        echo json_encode(CartSession::verifyListBonusAndSpecial($cart));
    }

    if ($do_ajax == 'checkout') {
        $data = file_get_contents("php://input");
        $dataArray = json_decode($data, true);

        $checkOut = CheckOutUtil::checkOut($dataArray);
        echo json_encode($checkOut);
        exit;
    }

    // Update cart - product quantity and size
    if ($do_ajax == 'update_cart') {
        $cart = json_decode(file_get_contents('php://input'), true)['cart'];

        echo json_encode(CartSession::updateCartSession($cart));
        exit();
    }

    if ($do_ajax == 'init-cart') {

        $cart = json_decode(file_get_contents('php://input'), true)['cart'];
        if (empty($cart)) { // Request with empty data
            
            $cart = get_cart_info_from_session();
           
        }
        if(empty(CartSession::initCart($cart))){
           return null;
        }else{
            echo json_encode(CartSession::initCart($cart));
        }
        // print_r(CartSession::initCart($cart));
        exit();
    }

    if ($do_ajax == 'mail') {
        $_mail_configs = array();
        $_mail_configs[] = 'Content-Type: text/html; charset=UTF-8';
        $_mail_configs[] = 'Cc: thoitrangkhatoco@gmail.com';
        echo '<pre>';
        /*try{
            $a = wp_mail( 'tuzi@maileme101.com', 'test wp', 'ah ihi', $_mail_configs );
        }
        catch (Exception $e){
            print_r ($e);
        }

        if ($a){
            echo 'Mail đã sent thành công.';
        } else {
            print_r ($a);
        }*/


        exit();
    }



// Coupon check with current user
    if ($do_ajax == 'verify_coupon') {
        $coupon = json_decode(stripslashes($_GET['coupon']), true);

        $uid = get_current_user_id();
        $response = check_invalid_coupon($coupon, $uid);

        echo json_encode($response);
        exit();
    }

// Check VIP member
    if ($do_ajax == 'verify_member_identified') {
        $member_identified = strval(json_decode(stripslashes($_GET['member_identified']), true));

        $user = check_member_identified($member_identified);

        if (!$user) {
            echo json_encode(array('result' => false));
            exit();
        }

        $phone = get_user_meta((int)$user['id'], 'user_phone', true); // get phone number of user
        $send_result = vip_checker_pre_send_hash_code($phone);

        if ($send_result['result'] == 1) {
            echo json_encode(array(
                'result' => 1,
                'message' => 'Chúng tôi sẽ gửi mã số vào điện thoại để xác nhận đây đúng là mã số thẻ thành viên của bạn!',
                'vip' => $user['lv'] // VIP Level
            ));
            exit();
        } else {
            echo json_encode($send_result);
            exit();
        }
    }

// Check OTP code
    if ($do_ajax == 'verify_member_identified_otp') {
        $otp_code = $_GET['otp_code'];

        if (compare_hash_code($otp_code)) {
            echo json_encode(array('result' => true));
            exit();
        }

        echo json_encode(array('result' => false));
        exit();
    }

    exit;
}

add_action('wp_ajax_update_cart_status_ajax', 'update_cart_status_ajax');
add_action('wp_ajax_nopriv_update_cart_status_ajax', 'update_cart_status_ajax');
function update_cart_status_ajax()
{
    $cart_id = isset($_POST['cart_id']) ? $_POST['cart_id'] : 0;
    $status_des = $_POST['status'];
    if ($cart_id) {
        if (in_array($status_des, array('neworder', 'pending', 'shipping', 'not_available', 'not_receiving', 'cancel', 'done'))) {
            update_post_meta($cart_id, 'order_status', $status_des);
            update_post_meta($cart_id, 'order_info', array());

            if ($status_des == 'done') {
                update_post_meta($cart_id, 'order_payment_status', true);
                // update_post_meta($cart_id, 'order_payment_code', true);
            }

            echo json_encode(array('message' => 'Đã cập nhật thành công! Nhấp F5 để thấy sự thay đổi'));
        }
    } else {
        echo json_encode(array('message' => 'Có lỗi xảy ra! Vui lòng thử lại'));
    }
    exit;
}

// $_onepay_settings = array(
//     'domestic' => array(
//         'merchant' => 'ONEPAY',
//         'access_code' => 'D67342C2',
//         'sercure_secret' => 'A3EFDFABA8653DF2342E8DAC29B51AF0',
//         'payment_url' => 'http://mtf.onepay.vn/onecomm-pay/vpc.op'
//     ),
//     'international' => array(
//         'merchant' => 'TESTONEPAY',
//         'access_code' => '6BEB2546',
//         'sercure_secret' => '6D0870CDE5F24F34F3915FB0045120DB',
//         'payment_url' => 'http://mtf.onepay.vn/vpcpay/vpcpay.op'
//     )
// );
// Settting để test

$_onepay_settings = array(
    'domestic' => array(
        'merchant' => 'KHATOCO',
        'access_code' => 'L2CM9Z63',
        'sercure_secret' => 'C93EC163C1E79DBAAD49F182C7241A2B',
        'payment_url' => 'https://onepay.vn/onecomm-pay/vpc.op'
    ),
    'international' => array(
        'merchant' => 'KHATOCO',
        'access_code' => '1033023B',
        'sercure_secret' => '92D011AE03A381901BD5F577FBC605EE',
        'payment_url' => 'https://onepay.vn/vpcpay/vpcpay.op'
    )
);
//Setting để chạy thật

$paymentStatus = array(
    'status' => 'failed',
    'message' => 'Giao dịch không thành công. Vui lòng thử lại sau ít phút nữa!'
);

function get_real_ip()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP']))
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else
        $ip = $_SERVER['REMOTE_ADDR'];
    return $ip;
}

function redirect_onepay_url($params = array())
{
    global $_onepay_settings;

    $ip = get_real_ip();
    $cancel_url = get_permalink(get_page_by_path('khatoco-loi')).'?error='.urlencode('Thanh toán không thành công').'&descr='.urlencode('Lỗi thanh toán không thành công Onepay');

    if ($params['paymentMethod'] == 2) {
        $onepay_merchant = $_onepay_settings['domestic']['merchant'];
        $onepay_access_code = $_onepay_settings['domestic']['access_code'];
        $onepay_sercure_secret = $_onepay_settings['domestic']['sercure_secret'];
        $onepay_payment_url = $_onepay_settings['domestic']['payment_url'];

        $vpc_parameters = array(
            'vpc_Merchant' => $onepay_merchant,
            'vpc_AccessCode' => $onepay_access_code,
            'vpc_Amount' => $params['amount'] * 100,
            'vpc_ReturnURL' => home_url('/') . 'wp-content/themes/khatoco/includes/onepay-checkhash/domestic_dr.php',
            'vpc_Version' => 2,
            'vpc_Locale' => "vn",
            'vpc_Currency' => "VND",
            'vpc_Command' => 'pay',
            'vpc_Customer_Email' => $params['user_email'],
            'vpc_Customer_Id' => $params['user_id'],
            'vpc_TicketNo' => $ip,
            'vpc_MerchTxnRef' => 'OP' . uniqid() . time() . '-' . date('Ymd'),
            'vpc_OrderInfo' => $params['order_code'],
            'AgainLink' => $cancel_url,
            'Title' => 'Thanh toán đơn hàng'
        );
    } elseif ($params['paymentMethod'] == 3) {
        $onepay_merchant = $_onepay_settings['international']['merchant'];
        $onepay_access_code = $_onepay_settings['international']['access_code'];
        $onepay_sercure_secret = $_onepay_settings['international']['sercure_secret'];
        $onepay_payment_url = $_onepay_settings['international']['payment_url'];

        $vpc_parameters = array(
            'vpc_Merchant' => $onepay_merchant,
            'vpc_AccessCode' => $onepay_access_code,
            'vpc_Amount' => $params['amount'] * 100,
            'vpc_ReturnURL' => home_url('/') . 'wp-content/themes/khatoco/includes/onepay-checkhash/international_dr.php',
            'vpc_Version' => 2,
            'vpc_Locale' => "vn",
            'vpc_Command' => 'pay',
            'vpc_Customer_Email' => $params['user_email'],
            'vpc_Customer_Id' => $params['user_id'],
            'vpc_TicketNo' => $ip,
            'vpc_MerchTxnRef' => 'OP' . uniqid() . time() . '-' . date('Ymd'),
            'vpc_OrderInfo' => $params['order_code'],
            'AgainLink' => $cancel_url,
            'Title' => 'Payment order with Visa or Master Card'
        );
    }

    ksort($vpc_parameters);
    $vpc_url = $onepay_payment_url . "?";

    $stringHashData = "";
    $appendAmp = 0;
    foreach ($vpc_parameters as $key => $value) {
        if (strlen($value) > 0) {
            if ($appendAmp == 0) {
                $vpc_url .= urlencode($key) . '=' . urlencode($value);
                $appendAmp = 1;
            } else {
                $vpc_url .= '&' . urlencode($key) . "=" . urlencode($value);
            }
            if ((strlen($value) > 0) && ((substr($key, 0, 4) == "vpc_") || (substr($key, 0, 5) == "user_"))) {
                $stringHashData .= $key . "=" . $value . "&";
            }
        }
    }
    $stringHashData = rtrim($stringHashData, "&");
    if (strlen($onepay_sercure_secret) > 0) {
        $vpc_url .= "&vpc_SecureHash=" . strtoupper(hash_hmac('SHA256', $stringHashData, pack('H*', $onepay_sercure_secret)));
    }

    return $vpc_url;
}

/* Meta Boxes */



/* Function */
function get_cart_info_from_session()
{
    return CartSession::getCartSession();
}

require_once(get_template_directory() . '/includes/cart/CartSession.php');
/*
$region:
    0: Nội tỉnh
    1: Vùng 1: Bình Định 52, Đà Nẵng 48, Gia Lai 64, Kontum 62, Huế 46, Phú Yên 54, Quảng Bình 44, Quảng Nam 49, Quảng Ngãi 51, Quảng Trị 45
    3: Các vùng còn lại 
*/
function get_transport_fee($transport_type, $weight = 0, $region = 0)
{
    $transport_fee = 0;

    if ($weight == 0)
        return 0;

    $regions = array(
        0 => array(
            '50' => 8000,
            '100' => 8000,
            '250' => 10000,
            '500' => 12500,
            '1000' => 15000,
            '1500' => 18000,
            '2000' => 21000,
            '2000+' => 1600,
        ),
        1 => array(
            '50' => 8500,
            '100' => 12500,
            '250' => 16500,
            '500' => 23500,
            '1000' => 33000,
            '1500' => 40000,
            '2000' => 48500,
            '2000+' => 3800,
        ),
        3 => array(
            '50' => 10000,
            '100' => 14000,
            '250' => 22500,
            '500' => 29500,
            '1000' => 43500,
            '1500' => 55500,
            '2000' => 67500,
            '2000+' => 9500,
        ),
    );

    //Chuyển phát nhanh
    if ($transport_type == 2) {
        $prices = $regions[$region];
        foreach ($prices as $k => $pr) {
            if (strpos($k, '+') !== false) {
                $k = substr($k, 0, -1);
                $transport_fee = $prices[$k];

                while ($weight > intval($k)) {
                    $weight = $weight >= 500 ? ($weight - 500) : 0;
                    $transport_fee += $pr;
                }
                break;
            } else {
                if ($weight <= intval($k)) {
                    $transport_fee = $pr;
                    break;
                }
            }
        }
    }
    //Gửi đảm bảo
    /*else{
        if(250 < $weight && $weight <= 500){
            $transport_fee = 10000;
        }
        elseif(500 < $weight && $weight <= 1000){
            $transport_fee = 16000;
        }
        elseif(1000 < $weight && $weight <= 1500){
            $transport_fee = 22000;
        }
        elseif(1500 < $weight && $weight <= 2000){
            $transport_fee = 28000;
        }
        elseif(2000 < $weight){
            $transport_fee = 0;
        }
        else{
            $transport_fee = 0;
        }
    }*/

    return $transport_fee;
}

function get_transport_methods()
{
    return array(
        '1' => array(
            'method_type' => 1,
            'method_name' => 'Giao hàng đảm bảo (VNPT)'
        ),
        '2' => array(
            'method_type' => 2,
            'method_name' => 'Chuyển phát nhanh (VNPT)'
        )
    );
}

function get_response_description($responseCode, $oid)
{
    switch ($responseCode) {
        case '0' :
            $result = 'Giao dịch thành công - Approved';
            break;
        case '1' :
            $result = 'Ngân hàng từ chối giao dịch - Bank Declined';
            break;
        case '3' :
            $result = 'Mã đơn vị không tồn tại - Merchant not exist';
            break;
        case '4' :
            $result = 'Không đúng access code - Invalid access code';
            break;
        case '5' :
            $result = 'Số tiền không hợp lệ - Invalid amount';
            break;
        case '6' :
            $result = 'Mã tiền tệ không tồn tại - Invalid currency code';
            break;
        case '7' :
            $result = 'Lỗi không xác định - Unspecified Failure ';
            break;
        case '8' :
            $result = 'Số thẻ không đúng - Invalid card Number';
            break;
        case '9' :
            $result = 'Tên chủ thẻ không đúng - Invalid card name';
            break;
        case '10' :
            $result = 'Thẻ hết hạn/Thẻ bị khóa - Expired Card';
            break;
        case '11' :
            $result = 'Thẻ chưa đăng ký sử dụng dịch vụ - Card Not Registed Service(internet banking)';
            break;
        case '12' :
            $result = 'Ngày phát hành/Hết hạn không đúng - Invalid card date';
            break;
        case '13' :
            $result = 'Vượt quá hạn mức thanh toán - Exist Amount';
            break;
        case '21' :
            $result = 'Số tiền không đủ để thanh toán - Insufficient fund';
            break;
        case '99' :
            $result = 'Người sử dụng hủy giao dịch - User cancel';
            clean_draft_order_when_failed($oid);
            break;
        default :
            $result = 'Giao dịch thất bại - Failured';
            clean_draft_order_when_failed($oid);
    }

    return $result;
}

function clean_draft_order_when_failed($oid)
{
    if ($oid) {
        if (get_post_type($oid) == 'orders' && get_post_status($oid) == 'draft') {
            wp_delete_post($oid, true);
        }
    }

    return false;
}

add_action('wp_ajax_cancel_order', 'cancel_order');
add_action('wp_ajax_nopriv_cancel_order', 'cancel_order');
function cancel_order()
{
    $id = $_POST['id'];
    $user_id = get_current_user_id();
    $order = get_post($id);

    if (get_post_type($id) == 'orders' && $order->post_author == $user_id) {
        $order_status = get_post_meta($id, 'order_status', true);
        if ($order_status == 'neworder') {
            update_post_meta($id, 'order_status', 'cancel');
            echo json_encode(array('result' => 'true'));
            exit;
        }

        echo json_encode(array('result' => 'false', 'message' => 'Trạng thái đơn hàng đã được thay đổi trước đó. Vui lòng nhấn F5 để cập nhật lại trạng thái đơn hàng!'));
        exit;
    }

    echo json_encode(array('result' => 'false', 'message' => ''));
    exit;
}


add_action('wp_ajax_frontend__rollback_order_draft', 'frontend__rollback_order_draft_cb');
function frontend__rollback_order_draft_cb()
{
    $hash = isset($_POST['hash']) ? sanitize_text_field($_POST['hash']) : '';
    if ($hash) {
        var_dump($hash);
    }
    exit;
}
