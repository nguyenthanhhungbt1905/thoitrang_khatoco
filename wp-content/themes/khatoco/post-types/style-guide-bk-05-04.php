<?php 
// Register new post type: Style Guide
add_action('init', 'posttype_style_guide');
function posttype_style_guide() {
	$labels = array(
		'name' => 'Style Guide',
		'singular_name' => 'Style Guide',
		'all_items' => 'Tất cả',
		'add_new' => 'Thêm mới',
		'add_new_item' => 'Thêm mới',
		'edit_item' => 'Chỉnh sửa',
		'new_item' => 'Style guide mới',
		'view_item' => 'Xem chi tiết',
		'search_items' => 'Tìm kiếm',
		'not_found' =>  'Không tìm thấy bài đăng nào',
		'not_found_in_trash' => 'Không tìm thấy bài đăng nào trong thùng rác',
		'view' =>  'Xem'
	);

	$args = array(
		'labels' => $labels,
		'description' => '',
		'public' => true,
		'exclude_from_search' => false,
		'publicly_queryable' => true,
		'show_in_nav_menus' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'rewrite' => true,
		'has_archive' => false,
		'menu_position' => 5,
		'menu_icon' => 'dashicons-star-filled',
		'supports' => array('title', 'thumbnail', 'editor'),
	);
	register_post_type('style-guide', $args);

	register_taxonomy('style-guide-category', 'style-guide', array(
		'hierarchical' => true,
		'label' => 'Danh mục Style Guide',
		'singular_label' => 'Danh mục Style Guide',
		'rewrite' => array('slug' => 'thoi-trang', 'hierarchical' => true),
		// 'show_ui' => false,
		'show_admin_column' => true,
	));
	// flush_rewrite_rules();
}

add_action('restrict_manage_posts', 'admin_style_guide_filter_restrict_manage_posts');
function admin_style_guide_filter_restrict_manage_posts() {
	$post_type = 'post';
	if(isset($_GET['post_type']))
		$post_type = $_GET['post_type'];

	if($post_type == 'style-guide') {
		$terms = get_terms('style-guide-category', array('hide_empty' => 0)); ?>
		<select name="filter_by_series">
			<option value="">Danh mục</option><?php $current_s = (isset($_GET['filter_by_series'])) ? $_GET['filter_by_series']:'';
			foreach ($terms as $tkey => $term) {
			echo '<option value="'. $term->slug .'" '. (($current_s == $term->slug) ? 'selected=""':'') .'>'. $term->name .'</option>'. "\n";
		} ?>
		</select>
	<?php
	}
}

add_filter('parse_query', 'custom_style_guide_filter');
function custom_style_guide_filter($query) {
	global $pagenow;
	$post_type = 'post';
	if(isset($_GET['post_type']))
		$post_type = $_GET['post_type'];

	if ('style-guide' == $post_type && is_admin() && $pagenow == 'edit.php' && isset($_GET['filter_by_series']) && $_GET['filter_by_series'] != '') {
		$query->query_vars['style-guide-category'] = $_GET['filter_by_series'];
	}
}

// Remove style-guide-categorydiv meta box in style-guide post edit
add_action('admin_menu', 'remove_style_guide_category_div', 999);
function remove_style_guide_category_div() {
	remove_meta_box('style-guide-categorydiv', 'style-guide', 'advanced');
}

// Add meta boxes for page template
add_action('add_meta_boxes', 'khatoco_style_guide_metabox');
function khatoco_style_guide_metabox() {
	add_meta_box('khatoco_style_guide_metabox', 'Chi tiết style guide (Enable Javascript Required)', 'khatoco_style_guide_callback', 'style-guide', 'normal', 'high');
}

function khatoco_style_guide_callback($post) {
	$tax = 'style-guide-category';
	$selected = '';

	wp_nonce_field('khatoco_style_guide_metabox', 'khatoco_style_guide_metabox_nonce');
	$target_saved = get_post_meta($post->ID, 'style_guide_save', true);
	if(is_null($target_saved) || empty($target_saved)) $target_saved = array(); ?>
	<input type="hidden" name="do" value="style_guide_savepost">
	<input type="hidden" name="target_save" value="" id="target_save"><?php $terms = get_terms($tax, array('orderby' => 'id', 'order' => 'ASC', 'hide_empty' => false, 'parent' => 0));
	$current_term = get_the_terms($post->ID, $tax);
	$current_term = $current_term[0];
	
	echo '<p><select name="tax_select" id="tax_select"><option value="-1" data-tax-slug="">Chọn một danh mục để tiếp tục</option>';
	foreach($terms as $term) {
		if($term->term_id == $current_term->term_id)
			$selected = 'selected';
		else $selected = '';
		echo '<option value="'. $term->term_id .'" data-tax-ref="'. $term->slug .'_content" data-tax-slug="#'. $term->slug .'" '. $selected .'>'. $term->name .'</option>';
	}
	echo '</select></p>'; ?>

	<div id="popup-image">
		<img src="#" alt="">
	</div>

	<div id="xu-huong-thoi-trang" class="tax_select_inner hidden">
		<?php wp_editor($post->post_content, 'xu_huong_thoi_trang_content', array('media_buttons' => true, 'textarea_row' => 50)); ?>
		<h3><strong>Chọn những sản phẩm phù hợp</strong></h3>
		<p><label for="disable-preview-1"><input type="checkbox" name="" id="disable-preview-1" class="disable-preview"><strong>Tắt chức năng xem ảnh sản phẩm</strong></label></p>
		<hr>
		<ul class="clearfix" style="max-height: 500px;overflow-y: auto;"><?php $all_products = get_posts(array('posts_per_page' => -1));
		foreach($all_products as $p) {
			$pid = $p->ID;
			$checked = (in_array($pid, $target_saved)) ? 'checked' : '';
			$thumb = get_post_thumbnail_id($pid);
			$thumb = wp_get_attachment_image_src($thumb, 'thumbnail');
			$thumb = (isset($thumb[0])) ? $thumb[0]:'';

			if($thumb == '') {
				$product_colors = get_post_meta($pid, 'post_colors', true);
				$thumb = isset($product_colors[0]['thumb']) ? $product_colors[0]['thumb']:'';
			}

			echo '<li id="post-'. $pid .'" class="product-item" data-thumb="'. $thumb .'">
				<label for="in-post-'. $pid .'" class="selectit"><input type="checkbox" name="xu_huong_thoi_trang[posts][]" id="in-post-'. $pid .'" value="'. $pid .'" '. $checked .'>'. $p->post_title .'</label> - 
				<a href="'. admin_url('/') .'post.php?post='. $pid .'&action=edit' .'" title="Chi tiết" target="_blank">Chi tiết sản phẩm</a> - 
				<a href="'. get_permalink($pid) .'" title="Xem" target="_blank">Xem sản phẩm</a>
			</li>';
		} ?>
		</ul>
	</div>

	<div id="meo-hay-cho-phong-cach" class="tax_select_inner hidden">
		<h3><strong>Nguyên tắc, lý giải, giới thiệu về mẹo</strong></h3>
		<?php wp_editor($post->post_content, 'meo_hay_cho_phong_cach_content', array('media_buttons' => true, 'textarea_row' => 50)); ?>
		<h3><strong>Chọn những sản phẩm phù hợp</strong></h3>
		<p><label for="disable-preview-2"><input type="checkbox" name="" id="disable-preview-2" class="disable-preview">Tắt chức năng xem ảnh sản phẩm</label></p>
		<hr>
		<ul class="clearfix" style="max-height: 500px;overflow-y: auto;"><?php $all_products = get_posts(array('posts_per_page' => -1));
		foreach($all_products as $p) {
			$pid = $p->ID;
			$checked = (in_array($pid, $target_saved)) ? 'checked' : '';
			$thumb = get_post_thumbnail_id($pid);
			$thumb = wp_get_attachment_image_src($thumb, 'thumbnail');
			$thumb = (isset($thumb[0])) ? $thumb[0]:'';

			echo '<li id="post-'. $pid .'" class="product-item" data-thumb="'. $thumb .'">
				<label for="at-post-'. $pid .'" class="selectit"><input type="checkbox" name="meo_hay_cho_phong_cach[posts][]" id="at-post-'. $pid .'" value="'. $pid .'" '. $checked .'>'. $p->post_title .'</label> - 
				<a href="'. admin_url('/') .'post.php?post='. $pid .'&action=edit' .'" title="Chi tiết" target="_blank">Chi tiết sản phẩm</a> - 
				<a href="'. get_permalink($pid) .'" title="Xem" target="_blank">Xem sản phẩm</a>
			</li>';
		} ?>
		</ul>
	</div>

	<div id="hinh-tuong-thoi-trang" class="tax_select_inner hidden">
		<h3><strong>Giới thiệu về hình tượng</strong></h3>
		<?php wp_editor($post->post_content, 'hinh_tuong_thoi_trang_content', array('media_buttons' => true, 'textarea_row' => 50)); ?>
	</div>

	<div id="cham-soc-trang-phuc" class="tax_select_inner hidden">
		<?php wp_editor($post->post_content, 'cham_soc_trang_phuc_content', array('media_buttons' => true, 'textarea_row' => 50)); ?>
	</div>

	<style type="text/css">
	.tax_select_inner h3{ padding: 10px 0 !important; border: 0 !important; cursor: default !important }
	.hidden{ display: none !important }
	#popup-image{position:fixed;top:100px;left:170px;z-index:10000;display:none;border:7px solid rgba(0,0,0,0.7);border-radius:5px;max-width: 250px}
	#popup-image img{width: 100%}
    #postdivrich{
        display: none !important;
    }
	</style>

	<script type="text/javascript">
	jQuery(document).ready(function($) {
		var tab_show = jQuery('#tax_select').find('option:selected').attr('data-tax-slug');
		var tabRef = jQuery('#tax_select').find('option:selected').attr('data-tax-ref').split('-').join('_');
		if(tab_show != '' || typeof tab_show != 'undefined') {
			jQuery(tab_show).removeClass('hidden');
			jQuery('#target_save').val(tab_show.substr(1));
		}
		else {
			jQuery('tax_select_inner').addClass('hidden');
			jQuery('#target_save').val('');
		}

		jQuery('#tax_select').on('change', function(event) {
			var tax_id = jQuery(this).val();
			var tax_slug = jQuery(this).find('option:selected').attr('data-tax-slug');
            tabRef = jQuery('#tax_select').find('option:selected').attr('data-tax-ref').split('-').join('_');

            //handle editor for yoast seo
            setTimeout(function () {
                var fromEditor = tinyMCE.get(tabRef)
                var contentEditor = tinyMCE.get('content')
                contentEditor.setContent(fromEditor.getContent())
                console.log(contentEditor.getContent())
                fromEditor.onChange.add(function (ed, e) {
                    var newContent = ed.getContent()
                    contentEditor.setContent(newContent);
                })
            }, 500)


			if(tax_id != '-1') {
				jQuery('#target_save').val(tax_slug.substr(1));
				jQuery('.tax_select_inner').addClass('hidden');
				jQuery(tax_slug).removeClass('hidden');
			}
			else {
				alert('Vui lòng chọn một danh mục để tiếp tục!');
				return false;
			}
		});

		jQuery('.product-item').hover(function() {
			if(jQuery(this).parents('.tax_select_inner').find('.disable-preview').is(':checked'))
				return false;
			var src = jQuery(this).attr('data-thumb');
			jQuery('#popup-image img').attr('src', src);
			jQuery('#popup-image').show();
		}, function() {
			jQuery('#popup-image img').attr('src', '#');
			jQuery('#popup-image').hide();
		});



		//handle editor
        setTimeout(function () {
            var fromEditor = tinyMCE.get(tabRef)
            var contentEditor = tinyMCE.get('content');
            fromEditor.onChange.add(function (ed, e) {
                var newContent = ed.getContent()
                contentEditor.setContent(newContent);
            })
        }, 500)
	});
	</script>
<?php 
}
