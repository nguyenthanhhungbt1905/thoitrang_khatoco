<?php 
/* Register CONTACT */
add_action('init', 'posttype_contact');
function posttype_contact() {
	$labels = array(
		'name' => 'Liên hệ',
		'singular_name' => 'Liên hệ',
		'all_items' => 'Liên hệ',
		'add_new' => 'Liên hệ',
		'add_new_item' => 'Liên hệ',
		'edit_item' => 'Liên hệ',
		'new_item' => 'Thêm mới',
		'view_item' => 'Xem chi tiết',
		'search_items' => 'Tìm kiếm',
		'not_found' =>  'No items was found with that criteria',
		'not_found_in_trash' => 'No items found in the Trash with that criteria',
		'view' =>  'Xem'
	);

	$args = array(
		'labels' => $labels,
		'description' => '',
		'public' => false,
		'publicly_queryable' => true,
		'exclude_from_search' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => false,
		'rewrite' => true,
		'menu_position' => 26,
		'menu_icon' => 'dashicons-email-alt',
		'supports' => array(''),
	);
	register_post_type('contact', $args);
}

add_action('admin_menu', 'custom_admin_contact_menu', 999);
function custom_admin_contact_menu() {
	if(post_type_exists('contact'))
		remove_submenu_page('edit.php?post_type=contact', 'post-new.php?post_type=contact');
}

add_action('admin_head','remove_add_new_button_edit_page_contact');
function remove_add_new_button_edit_page_contact() {
	global $post, $pagenow;
	if(is_admin() && (isset($_GET['post_type']) || isset($post))){
		if(($pagenow == 'edit.php' && isset($_GET['post_type']) && $_GET['post_type'] == 'contact') || ($pagenow == 'post.php' && $post->post_type == 'contact')){
			echo '<style>.add-new-h2{display: none !important}</style>';
		}
	}
}

add_filter('post_row_actions', 'edit_contact_list', 10, 2);
function edit_contact_list($actions, $post){
	if('contact' == get_post_type()){
		$actions['edit'] = '<a href="'. get_edit_post_link( $post->ID ) .'">'. (($post->post_status == 'publish') ? 'View details':'Reply') .'</a>';

		unset($actions['inline hide-if-no-js']);
		unset($actions['view']);
	}
    return $actions;
}

/* Remove meta box Product_category */
add_action('admin_init', 'remove_meta_box_contact');
function remove_meta_box_contact() {
    if(post_type_exists('contact')) remove_meta_box('submitdiv', 'contact', 'normal');
}

add_action('admin_init', 'add_metabox_contact');
function add_metabox_contact() {
	add_meta_box('metabox_contact_general', 'Thư liên hệ', 'display_metabox_contact_general', 'contact', 'normal', 'high');
	function display_metabox_contact_general($post) {
		$post_id = $post->ID;
		$title = $post->post_title;
		$name = get_post_meta($post_id, 'contact_name', true);
		$to_email = get_post_meta($post_id, 'contact_email', true);
		$phone = get_post_meta($post_id, 'contact_phone', true);
		$answer = get_post_meta($post_id, 'answer', true);
		$question = $post->post_content; ?>
		<div class="wrap">
			<p><strong><?php echo $name; ?> (<?php echo $to_email; ?>)</strong> đã gửi tin nhắn vào ngày <span><?php echo get_the_time('d/m/Y h:i:s', $post_id); ?></span> với nội dung:</p>
			<p>Điện thoại: <strong><i><?php echo $phone; ?></i></strong></p>
			<p>Tiêu đề: <strong><i><?php echo $title; ?></i></strong></p>
			<p>Nội dung: <span style="font-style: italic"><?php echo $question; ?></span></p>
			<div class="clear"></div>
			<hr>
			<p><strong>Trả lời</strong><?php if($answer || $answer != '') echo ' <i>(Đã trả lời)</i>'; ?></p>
			<textarea name="answer" id="answer" style="width: 100%" rows="8"><?php echo $answer; ?></textarea>
		</div>
		<div class="clear"></div>
		<style>#post-body-content{display: none} p.submit{text-align: center}</style>
	<?php }

	add_meta_box('metabox_contact_submit', 'Trả lời', 'display_metabox_contact_submit', 'contact', 'side', 'low');
	function display_metabox_contact_submit($post) {
		$stt = get_post_status($post->ID);
		if($stt == 'publish')
			submit_button('Đã trả lời', 'disable');
		else submit_button('Trả lời', 'primary');
	}
}

// Save answer
add_action('save_post', 'save_answer');
function save_answer($post_id) {
	if(get_post_type($post_id) == 'contact') {
		if(isset($_POST['answer']) && get_post_status( $post_id ) == 'pending') {
			update_post_meta($post_id, 'answer', sanitize_text_field($_POST['answer']));
			wp_publish_post( $post_id );

			wp_mail(get_post_meta($post_id, 'contact_email', true), '[Thoi trang Khatoco] Trả lời thư liên hệ', sanitize_text_field($_POST['answer']));
		}
		else {
			return;
		}
	}
}

add_action( 'wp_before_admin_bar_render', 'notification_contact_admin_bar' );
function notification_contact_admin_bar() {
	global $wp_admin_bar;

	$contacts = get_posts( array('post_type' => 'contact', 'posts_per_page' => -1, 'post_status' => 'pending') );
	if(count($contacts) > 0){
		$wp_admin_bar->add_node(array(
			'id' => 'has-contact-email',
			'title' => '<span class="ab-icon"></span> <span class="ab-label pending-count count-1">'. count($contacts) .'</span>',
			'href' => admin_url('edit.php?post_status=pending&post_type=contact'),
			'meta' => array(
				'title' => 'Bạn có '. count($contacts) .' thư chưa trả lời'
			)
		));
	} ?>
<?php }

add_action('admin_head', 'add_custom_style_mail_notifications');
function add_custom_style_mail_notifications(){ ?>
<style>
#wpadminbar #wp-admin-bar-has-contact-email .ab-icon::before {
	font-family: "dashicons" !important;
	content: "\f466" !important;
	top: 3px;
}
</style>
<?php }

add_action('admin_menu', 'notification_bubble_in_admin_menu_contact');
function notification_bubble_in_admin_menu_contact() {
    global $menu;
    // print_r($menu[27][0]);
    // die;
    $contacts = get_posts( array('post_type' => 'contact', 'posts_per_page' => -1, 'post_status' => 'pending') );
    if(!empty($menu[27][0])){
    $menu[27][0] .= $contacts ? "&nbsp;<span class='update-plugins count-1' title='Bạn có ". count($contacts) ." thư chưa trả lời'><span class='update-count'>". count($contacts) ."</span></span>" : '';
    	
    }
}
