<?php
/* Register News Type */
add_action('init', 'reg_post_type_news');
function reg_post_type_news()
{
    $labels = [
        'name'               => 'Tin tức',
        'singular_name'      => 'Tin tức',
        'menu_name'          => 'Tin tức',
        'all_items'          => 'Tất cả tin tức',
        'add_new'            => 'Thêm mới',
        'add_new_item'       => 'Thêm mới tin tức',
        'edit_item'          => 'Sửa tin tức',
        'new_item'           => 'Tin tức mới',
        'view_item'          => 'Xem chi tiết',
        'search_items'       => 'Tìm kiếm',
        'not_found'          => 'Không tìm thấy bài tin tức nào',
        'not_found_in_trash' => 'Không có bài tin tức nào trong thùng rác',
        'view'               => 'Xem tin tức',
    ];

    $args = [
        'labels'              => $labels,
        'description'         => 'Tin tức, khacoto',
        'public'              => true,
        'publicly_queryable'  => true,
        'exclude_from_search' => true,
        'show_in_nav_menus'   => false,
        'show_ui'             => true,
        'rewrite'             => ['slug' => 'tin-tuc'],
        'menu_position'       => 5,
        'menu_icon'           => 'dashicons-rss',
        'supports'            => ['title', 'editor', 'thumbnail'],
        'has_archive'         => 'tin-tuc',
        // 'taxonomies' => array('post_tag')
    ];

    register_post_type('news', $args);

    /* Register taxonomy */
    register_taxonomy('news_category', 'news', [
        "hierarchical"      => true,
        "label"             => "Danh mục Tin tức",
        "singular_label"    => "Danh mục Tin tức",
        "rewrite"           => ['slug' => 'danh-muc-tin-tuc', 'hierarchical' => true],
        "show_admin_column" => true,
    ]);

    /* Register news tag */
    register_taxonomy('news_tags', 'news', [
        "hierarchical"      => false,
        "label"             => "Từ khoá tin tức",
        "singular_label"    => "Từ khoá tin tức",
        "rewrite"           => true,
        'show_in_nav_menus' => false,
        "show_admin_column" => true,
    ]);
}


/* *
 * BACKEND
 */
/* Metabox */
add_action('admin_init', 'add_metabox_news');
add_action('admin_init', 'add_metabox_short_description');
function add_metabox_short_description()
{
    function display_metabox_short_description($post)
    {
        $post_id   = $post->ID;
        $shortDesc = get_post_meta($post_id, 'news_short_description');
        ?>
        <div style="width:100%">
            <input type="hidden" name="do" value="post"/>
            <textarea style="width:100%;min-height:100px;"
                      name="short_description"><?php echo @$shortDesc[0] ?></textarea>
        </div>

        <?php
    }

    add_meta_box(
        'display_metabox_short_description',
        'Short Description',
        'display_metabox_short_description',
        'news',
        'normal',
        'high'
    );
}

add_action('save_post', 'save_metabox_short_description');
function save_metabox_short_description($post_id)
{
    if (get_post_type() == 'news' && isset($_POST['do']) && $_POST['do'] == 'post') {
        if (isset($_POST['short_description'])) {
            update_post_meta($post_id, 'news_short_description', $_POST['short_description']);
        }
    }
}

function add_metabox_news()
{
    function display_metabox_news($post)
    {
        $post_id     = $post->ID;
        $news_is_hot = get_post_meta($post_id, 'news_is_hot', true);
        ?>
        <table class="form-table">
            <tbody>
            <tr>
                <th scope="row"><label for="is_hot">Đưa lên HOT</label></th>
                <td>
                    <input type="hidden" name="do" value="post"/>
                    <input type="checkbox" id="is_hot" name="is_hot" value="1" <?php if ($news_is_hot == 'true')
                        echo 'checked="checked"'; ?> />
                </td>
            </tr>
            </tbody>
        </table>
        <?php
    }

    add_meta_box(
        'display_metabox_news',
        'Thuộc tính mở rộng',
        'display_metabox_news',
        'news',
        'side',
        'high'
    );
}

add_action('save_post', 'save_metabox_news');
function save_metabox_news($post_id)
{
    if (get_post_type() == 'news' && isset($_POST['do']) && $_POST['do'] == 'post') {
        if (isset($_POST['is_hot'])) {
            $is_hot = (int) $_POST['is_hot'] ? 'true' : 'false';
            update_post_meta($post_id, 'news_is_hot', $is_hot);
        }
    }
}

/**
 * FUNCTIONS
 */

function display_each_news($post_id)
{
    $post_obj  = get_post($post_id);
    $permalink = get_permalink($post_id);
    $excerpt   = wp_trim_words(apply_filters('the_content', $post_obj->post_content), 20); ?>
    <div class="col-md-4 normal-news">
        <div class="vitem-outer">
            <div class="vitem-inner clearfix">
                <div class="vitem-thumb">
                    <a class="vitem-thumb-inner" rel="nofollow" href="<?php echo $permalink; ?>">
                        <?php if (has_post_thumbnail())
                            echo get_the_post_thumbnail(get_the_id(), 'thumbnail-news');
                        else
                            echo '<img src="' . get_template_directory_uri() . '/images/front_news.png' . '" class="attachment-thumbnail-news wp-post-image" alt="front-news">';
                        ?>
                    </a>
                </div>
                <div class="vitem-info">
                    <h2 class="vitem-title condensed">
                        <a href="<?php echo $permalink; ?>"
                           title="<?php echo get_the_title(); ?>"><?php echo wp_trim_words(get_the_title($post_id), 15, '...'); ?></a>
                        <?php echo '<span class="date"><i class="fa fa-calendar"></i> ' . get_the_date() . '</span>'; ?>
                    </h2>
                    <?php /*<p class="news-excerpt"><?php echo $excerpt; ?></p> */ ?>
                    <?php /* echo '<a href="'. $permalink .'" class="news-view-more"><i class="fa fa-eye"></i> Xem thêm</a>'; */ ?>
                </div>
            </div>
        </div>
    </div>
    <?php
}

// Display news post at homepage
function aj_display_each_home_news($post_obj, $class = 'odd')
{//odd - even
    $post_id   = $post_obj->ID;
    $permalink = get_permalink($post_id);
    $post_date = date('d/m/Y', strtotime(get_the_date()));
    $excerpt   = wp_trim_words(apply_filters('the_content', $post_obj->post_content), 15); ?>
    <div class="item col-md-12 col-xs-6 nopadding">

        <div class="item-outer clearfix">

            <div class="item-inner <?php echo $class; ?> clearfix">
                <div class="item-thumb">
                    <a class="item-thumb-inner" rel="nofollow"
                       href="<?php echo $permalink; ?>"><?php echo get_the_post_thumbnail($post_id, 'thumbnail-news'); ?></a>
                    <span class="news-arrow"></span>
                </div>
                <div class="item-info">
                    <h2 class="item-title condensed"><a
                            href="<?php echo $permalink; ?>"><?php echo get_the_title($post_id); ?></a></h2>
                    <!-- <p class="item-metas">
    	                    <span class="meta"><b class="fa fa-comment"></b><i><?php //echo '<fb:comments-count href="'. $permalink .'</fb:comments-count>';
                    ?></i></span> |
    	                    <span class="meta"><b class="fa fa-calendar"></b><i><?php echo $post_date; ?></i></span>
    	                </p> -->
                    <a class="item-thumb-inner" style="color: #000" rel="nofollow" href="<?php echo $permalink; ?>"><p
                            class="item-excerpt"><?php echo $excerpt; ?></p></a>
                </div>
            </div>

        </div>

    </div>

    <?php
}

function display_news_at_homepage()
{
    global $wpdb;
    $wp_    = $wpdb->prefix;
    $query  = "SELECT {$wp_}posts.ID as 'id', {$wp_}posts.post_title as 'title', short_desc.meta_value as 'short_description' FROM {$wp_}posts 
LEFT JOIN {$wp_}postmeta `short_desc` ON {$wp_}posts.ID = short_desc.post_id AND short_desc.meta_key = 'news_short_description'
LEFT JOIN {$wp_}term_relationships `term_rela` ON {$wp_}posts.ID = term_rela.object_id
LEFT JOIN {$wp_}term_taxonomy `term_taxonomy` ON term_rela.term_taxonomy_id = term_taxonomy.term_taxonomy_id
LEFT JOIN {$wp_}terms `terms` ON terms.term_id = term_taxonomy.term_id
WHERE {$wp_}posts.post_type = 'news' AND {$wp_}posts.post_status ='publish' AND terms.slug = 'tin-khuyen-mai' ORDER BY {$wp_}posts.ID DESC LIMIT 6 
";
    $result = $wpdb->get_results($query, ARRAY_A);
    foreach ($result as $key => $postItem) {
        $url                           = wp_get_attachment_url(get_post_thumbnail_id($postItem['id']), 'thumbnail');
        $link                          = get_permalink($postItem['id']);
        $result[$key]['feature_image'] = $url;
        $result[$key]['link']          = $link;
    }

    return $result;
}

/**
 * @param        $type
 * @param int    $offset
 * @param string $post_type
 * @return mixed
 *             Display news and style guide with filter
 */
function display_post_with_filter($type, $offset = 0,$post_type = 'news')
{
    global $wpdb;
    $wp_    = $wpdb->prefix;
    if($post_type == "news"){
        $shortDescKey = "news_short_description";
    }else{
        $shortDescKey = "style_guide_short_description";

    }
    $query  = "SELECT {$wp_}posts.ID as 'id', {$wp_}posts.post_title as 'title', short_desc.meta_value as 'short_description' FROM {$wp_}posts 
LEFT JOIN {$wp_}postmeta `short_desc` ON {$wp_}posts.ID = short_desc.post_id AND short_desc.meta_key = '{$shortDescKey}'
LEFT JOIN {$wp_}term_relationships `term_rela` ON {$wp_}posts.ID = term_rela.object_id
LEFT JOIN {$wp_}term_taxonomy `term_taxonomy` ON term_rela.term_taxonomy_id = term_taxonomy.term_taxonomy_id
LEFT JOIN {$wp_}terms `terms` ON terms.term_id = term_taxonomy.term_id
WHERE {$wp_}posts.post_type = '{$post_type}' AND {$wp_}posts.post_status ='publish' AND terms.slug = '{$type}' ORDER BY {$wp_}posts.ID DESC LIMIT 9 OFFSET {$offset}
";
    $result = $wpdb->get_results($query, ARRAY_A);
    foreach ($result as $key => $postItem) {
        $url                           = wp_get_attachment_url(get_post_thumbnail_id($postItem['id']), 'thumbnail');
        $link                          = get_permalink($postItem['id']);
        $result[$key]['feature_image'] = $url;
        $result[$key]['link']          = $link;
    }

    return $result;
}

function get_related_news($termSlug)
{
    global $wpdb;
    $wp_    = $wpdb->prefix;
    $query  = "SELECT {$wp_}posts.ID as 'id', {$wp_}posts.post_title as 'title', short_desc.meta_value as 'short_description' FROM {$wp_}posts 
LEFT JOIN {$wp_}postmeta `short_desc` ON {$wp_}posts.ID = short_desc.post_id AND short_desc.meta_key = 'news_short_description'
LEFT JOIN {$wp_}term_relationships `term_rela` ON {$wp_}posts.ID = term_rela.object_id
LEFT JOIN {$wp_}term_taxonomy `term_taxonomy` ON term_rela.term_taxonomy_id = term_taxonomy.term_taxonomy_id
LEFT JOIN {$wp_}terms `terms` ON terms.term_id = term_taxonomy.term_id
WHERE {$wp_}posts.post_type = 'news' AND {$wp_}posts.post_status ='publish' AND terms.slug = '{$termSlug}' ORDER BY {$wp_}posts.ID DESC LIMIT 6 OFFSET 0
";
    $result = $wpdb->get_results($query, ARRAY_A);
    foreach ($result as $key => $postItem) {
        $url                           = wp_get_attachment_url(get_post_thumbnail_id($postItem['id']), 'thumbnail');
        $link                          = get_permalink($postItem['id']);
        $result[$key]['feature_image'] = $url;
        $result[$key]['link']          = $link;
    }

    return $result;
}

add_action('wp_ajax_ajax_loading_news', 'ajax_loading_news');
add_action('wp_ajax_nopriv_ajax_loading_news', 'ajax_loading_news');
function ajax_loading_news()
{
    $type   = $_POST['type'];
    $offset = $_POST['offset'];
    $postType = $_POST['postType'];
    if($postType == "news"){
        $shortDescKey = "news_short_description";
    }else{
        $shortDescKey = "style_guide_short_description";

    }
    global $wpdb;
    $wp_    = $wpdb->prefix;
    $query  = "SELECT {$wp_}posts.ID as 'id', {$wp_}posts.post_title as 'title', short_desc.meta_value as 'short_description' FROM {$wp_}posts 
LEFT JOIN {$wp_}postmeta `short_desc` ON {$wp_}posts.ID = short_desc.post_id AND short_desc.meta_key = '{$shortDescKey}'
LEFT JOIN {$wp_}term_relationships `term_rela` ON {$wp_}posts.ID = term_rela.object_id
LEFT JOIN {$wp_}term_taxonomy `term_taxonomy` ON term_rela.term_taxonomy_id = term_taxonomy.term_taxonomy_id
LEFT JOIN {$wp_}terms `terms` ON terms.term_id = term_taxonomy.term_id
WHERE {$wp_}posts.post_type = '{$postType}' AND {$wp_}posts.post_status ='publish' AND terms.slug = '{$type}' ORDER BY {$wp_}posts.ID DESC LIMIT 9 OFFSET {$offset}
";
    $result = $wpdb->get_results($query, ARRAY_A);
    $html   = "";
    $count = $offset;
    if ($result) {

        foreach ($result as $key => $postItem) {
            $url                           = wp_get_attachment_url(get_post_thumbnail_id($postItem['id']), 'thumbnail');
            $link                          = get_permalink($postItem['id']);
            $postItem['feature_image'] = $url;
            $postItem['link']          = $link;

            ob_start();
             _get_template_part('partials/event-item', null, ['event' => $postItem]);
            $itemPart = ob_get_contents();
            ob_end_clean();

            $html .= $itemPart;
            $count = $count+1;
        }
    }
    echo json_encode(['html'=>$html,'nextOffset'=>$count]);die;
}
function getSingleEvent(){
    global $wpdb;
    global $post;
    $postId = $post->ID;
    $wp_ = $wpdb->prefix;
    $query = "SELECT {$wp_}posts.ID as 'id',{$wp_}posts.post_content as 'content', {$wp_}posts.post_title as 'title',term_taxonomy.description as 'category_name',terms.slug as 'slug' FROM {$wp_}posts
    LEFT JOIN {$wp_}term_relationships `term_relation` ON {$wp_}posts.ID = term_relation.object_id
    LEFT JOIN {$wp_}term_taxonomy `term_taxonomy` ON term_relation.term_taxonomy_id = term_taxonomy.term_taxonomy_id
    LEFT JOIN {$wp_}terms `terms` ON terms.term_id = term_taxonomy.term_id
    WHERE {$wp_}posts.ID = '{$postId}' AND {$wp_}posts.post_status = 'publish' AND {$wp_}posts.post_type = 'news' AND term_taxonomy.taxonomy = 'news_category'
";
    $result = $wpdb->get_results($query,ARRAY_A);
    return $result;
}
