<?php
 	//add submenu active serial
    add_action('admin_menu', 'registerSubmenuExportProduct');
    function registerSubmenuExportProduct(){
        add_submenu_page(
            'edit.php',
            __( 'Export Sản Phẩm', 'khatoco' ),
            __( 'Export Sản Phẩm', 'khatoco' ),
            'manage_options',
            'export-product',
            'exportProduct'
        );
    }

    function exportProduct(){ 
    	$args = array('post_type' => 'post',
    			'posts_per_page' => -1,
				'orderby' => 'date',
				'order' => 'ASC');
    	$posts = get_posts($args);
    	$data = array();
    //     global $wpdb;
    //     $table_name = $wpdb->prefix . 'customer_register_cards';
    //     $table_user_register = $wpdb->prefix . 'customer_register';
    	foreach ($posts as $key => $p) {
    		$temp =array();
            $temp['product_name'] = $p->post_title;
    		$temp['sku'] = get_post_meta($p->ID,'post_sku',true);
            $temp['color'] = get_post_meta($p->ID, 'post_colors', true)[0]['color_id'];
            $sale = get_post_meta($p->ID, 'sale_off', true);
            $price = get_post_meta($p->ID, 'post_price', true);
            if ($price == '')
                $temp['price'] = 0;
            $temp['price'] = number_format($price, 0, ',', '.').' đ';
           
            if ($sale != '' && $sale != '0')
                $temp['sale'] = number_format($price * (100 - $sale) / 100, 0, ',', '.').' đ';
            $temp['sale'] = '';
            $temp['like_count'] = get_post_meta($p->ID, 'like_count', true) == '' ? '0' : get_post_meta($p->ID, 'like_count', true);
            array_push($data, $temp);
    	}
        // echo "<pre>";
        // print_r($data);
        // echo "</pre>";
?>
	<form action="<?php echo get_template_directory_uri(); ?>/includes/product/export-product.php" id="report-seri-code" method="POST">
		<?php submit_button('Download Export'); ?>
	</form>
<?php

    }
?>