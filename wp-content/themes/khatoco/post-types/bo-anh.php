<?php 
// Register new post type: Đồng phục
add_action('init', 'posttype_fashion_collection');
function posttype_fashion_collection() {
	$labels = array(
		'name' => 'Bộ ảnh',
		'singular_name' => 'Bộ ảnh',
		'all_items' => 'Tất cả bộ ảnh',
		'add_new' => 'Thêm mới',
		'add_new_item' => 'Thêm mới',
		'edit_item' => 'Chỉnh sửa',
		'new_item' => 'Bộ ảnh mới',
		'view_item' => 'Xem chi tiết',
		'search_items' => 'Tìm kiếm',
		'not_found' =>  'Không tìm thấy bộ ảnh nào',
		'not_found_in_trash' => 'Không tìm thấy bộ ảnh nào trong thùng rác',
		'view' =>  'Xem chi tiết'
	);

	$args = array(
		'labels' => $labels,
		'description' => '',
		'public' => true,
		'exclude_from_search' => false,
		'publicly_queryable' => true,
		'show_in_nav_menus' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'rewrite' => true,
		'has_archive' => false,
		'menu_position' => 5,
		'menu_icon' => 'dashicons-images-alt',
		'supports' => array('title', 'editor'),
	);
	register_post_type('bo-anh', $args);

	register_taxonomy('bo-suu-tap', 'bo-anh', array(
		'hierarchical' => true,
		'label' => 'Bộ sưu tập',
		'singular_label' => 'Bộ sưu tập',
		'rewrite' => true,
		'show_admin_column' => true,
	));

	/* Register news tag */ 
    register_taxonomy('tags_bo_anh', 'bo-anh', array(
        "hierarchical" => false,
        "label" => "Từ khoá bộ ảnh",
        "singular_label" => "Từ khoá bộ ảnh",
        "rewrite" => true,
        "show_admin_column" => true
    ));
    // flush_rewrite_rules();
}

// Manage post column
add_action('manage_edit-bo-anh_columns', 'add_download_count_column');
function add_download_count_column($defaults) {
    global $post;
    $post_type = get_post_type($post );
    if($post_type == 'bo-anh') {
        $defaults = array_slice($defaults, 0, 4, true) + array('download' => 'Lượt download') + array_slice($defaults, 4, count($defaults)-4, true);
    }
    return $defaults;
}

add_action('manage_bo-anh_posts_custom_column' , 'post_download_count_custom_column', 10, 2);
function post_download_count_custom_column($column, $post_id){
    if(get_post_type( $post_id ) == 'bo-anh') {
        switch ($column) {
            case 'download':
                $count = (int) get_post_meta($post_id, 'download_count', true);
				if($count == '') $count = 0;
                echo '<p><strong style="color:#655;font-size:14px">'. $count .'</strong></p>';
                break;
        }
    }
}

// add_filter( "manage_edit-bo-anh_sortable_columns", "bo_anh_sortable_columns" );
function bo_anh_sortable_columns() {
    return array(
        'download' => 'download'
    );
}

// add_filter('request', 'bo_anh_column_orderby');
function bo_anh_column_orderby($vars) {
    if(isset($vars['orderby']) && 'download' == $vars['orderby']) {
		$vars = array_merge($vars, array(
		    'meta_key' => 'download',
		    'orderby' => 'meta_value_num'
		));
    }
    return $vars;
}

// Add meta boxes for page template
add_action('add_meta_boxes', 'khatoco_collection_metabox');
function khatoco_collection_metabox() {
	add_meta_box('khatoco_page_metabox', 'Bộ ảnh', 'khatoco_collection_callback', 'bo-anh', 'normal', 'high');
}

function khatoco_collection_callback($post) {
	wp_nonce_field('khatoco_add_page_metabox', 'khatoco_add_page_metabox_nonce');
	$allPostsObj = get_posts(array('posts_per_page' => -1));
	$allPosts = array();
	foreach ($allPostsObj as $key => $p) {
		setup_postdata($p);
		$thumb = get_post_thumbnail_id($p->ID);
		$thumb = wp_get_attachment_image_src($thumb, 'thumbnail');
		$thumb = (isset($thumb[0])) ? $thumb[0]:'';

		$allPosts[$key]['id'] = $p->ID;
		$allPosts[$key]['title'] = $p->post_title;
		$sku = get_post_meta( $p->ID, 'post_sku', true );
		$colors = get_post_meta( $p->ID, 'post_colors', true );
		$allPosts[$key]['code'] = $sku .' '. $colors[0]['color_id'];
		$allPosts[$key]['thumb'] = $thumb;
	}
	$galery = get_post_meta($post->ID, 'collection_galery', true);

	$count = (int) get_post_meta($post->ID, 'download_count', true);
	if($count == '') $count = 0;
	echo '<p style="padding:0 20px"><strong>Download Count: '. $count .'</strong></p>'; ?>
	<input type="hidden" name="do" value="collection_savepost" />
	<div id="popup-image">
		<img src="#" alt="">
	</div>
	<table id="images_list" class="form-table">
		<tbody>
			<tr class="images-more">
				<th scope="row">
					<button type="button" class="button images-button-upload" >Chọn ảnh</button>
					<p>Chọn tất cả ảnh</p>
				</th>
				<td>&nbsp;</td>
			</tr><?php $index = 1;
			if(sizeof($galery) > 0 && is_array($galery)) {
				foreach ($galery as $key => $p) {
					foreach ($p as $img => $pid) {
						$img_src = wp_get_attachment_image_src($img, 'medium');
						$img_src = $img_src[0];
						echo '<tr>';
						echo '<th scope="scope">Image '. $index .'</th>';
						echo '<td><div class="outer"><p><img src="'. $img_src .'" />';
						echo '<input class="img_post" type="hidden" name="image['. $index .']" value=\'{"'. $img .'": "'. $pid .'"}\' />';
						echo '<select name="post_'. $index .'" for="'. $img .'"><option value="-1">Chọn sản phẩm đính kèm ảnh</option>';
						foreach ($allPosts as $value) {
							$select = ($pid == $value['id']) ? ' selected' : '';
							echo '<option value="'. $value['id'] .'" title="'. $value['title'] .'" data-thumb="'. $value['thumb'] .'" '. $select .'>'. $value['code'] .'</option>';
						}
						echo '</select>';
						echo '</p><span class="dashicons dashicons-dismiss button-dismiss" title="Xoá"></span></div></td>';
						echo '</tr>';
						$index++;
					}
				}
			} ?>
			<tr class="images-more">
				<th scope="row">
					<button type="button" class="button images-button-upload" >Chọn ảnh</button>
				</th>
				<td>&nbsp;</td>
			</tr>
		</tbody>
	</table>
	<script type="text/javascript">
	jQuery(document).ready(function() {
		var allProducts = '<?php echo json_encode($allPosts); ?>';
		allProducts = JSON.parse(allProducts);

		jQuery('#images_list').find('select').select2();

		//Upload
		jQuery('#images_list .images-button-upload').live('click', function(event) {
			var _this = jQuery(this);

			var file_frame = wp.media.frames.file_frame = wp.media({
				title: 'Select image',
				library: {},
				button: {text: 'Select'}, 
				//frame: 'post',
				multiple: true
			});

			file_frame.on('select', function() {
				var attachment_ids = [];
				attachment = file_frame.state().get('selection').toJSON();
				imgs_html = '';

				jQuery('#images_list').html('<tr class="images-more"><th scope="row"><button type="button" class="button images-button-upload" >Chọn ảnh</button></th><td>&nbsp;</td></tr>');
				jQuery.each(attachment, function(index, item){
					createImgRow(index, item.id, item.url);
				});
			});

			file_frame.open();
		});

		jQuery('#images_list').delegate('select', 'change', function(event) {
			var post_follow = jQuery(this).val();
			var for_img = jQuery(this).attr('for');
			var for_json = {};
			for_json[for_img] = post_follow;
			jQuery(this).parents('td').find('input.img_post').val(JSON.stringify(for_json));
		});

		// Select posts
		function select_box(allProducts, index, f) {
			var selectPosts = '<select name="post_'+ index +'" for="'+ f +'"><option value="-1">Chọn sản phẩm đính kèm ảnh</option>';
			for(var i in allProducts) {
				selectPosts += '<option value="'+ allProducts[i]['id'] +'" data-thumb="'+ allProducts[i]['thumb'] +'">'+ allProducts[i]['code'] +'</option>';
			}
			selectPosts += '</select>';
			return selectPosts;
		}

		// Create Each Images Row
		function createImgRow(index, img_id, img_url) {
			var default_img = {};
			default_img[img_id] = "-1";
			var html = '';
			html += '<tr>';
			html += '<th scope="row">Image '+ (index+1) +'</th>';
			html += '<td><div class="outer"><p><img src="'+ img_url +'" /><input class="img_post" type="hidden" name="image['+ index +']" value=\''+ JSON.stringify(default_img) +'\' />' + select_box(allProducts, index+1, img_id) +'</p><span class="dashicons dashicons-dismiss button-dismiss" title="Xoá"></span></div></td>';
			html += '</tr>';
			jQuery('#images_list').append(html);
			jQuery('#images_list').find('select').select2();
		}

		//Delete
		jQuery('#images_list').delegate('.button-dismiss', 'click', function(event) {
			var _this = jQuery(this);
			var _this_row = _this.parents('tr');
			_this_row.fadeOut(600);
			_this_row.remove();
		});

		jQuery('#images_list').delegate('option', 'mouseover', function(event) {
			var src = jQuery(this).attr('data-thumb');
			jQuery('#popup-image img').attr('src', src);
			jQuery('#popup-image').show();
		});

		jQuery('#images_list').delegate('option', 'mouseout', function(event) {
			jQuery('#popup-image img').attr('src', '#');
			jQuery('#popup-image').hide();
		});
	});
	</script>
	<style>
	#khatoco_page_metabox .inside {padding: 0 !important}
	#popup-image{position:fixed;top:100px;left:170px;z-index:10000;display:none;border:7px solid rgba(0,0,0,0.7);border-radius:5px}
	#images_list th{width: 100px}
	#images_list tr td, #images_list tr th{padding: 20px 10px}
	#images_list tr:nth-of-type(even) {background-color: #f0f0f0}
	#images_list tr.images-more{background-color:#ccc}
	#images_list .outer{position:relative}
	#images_list select{vertical-align: top; max-width: 300px; margin-left: 20px;}
	#images_list img{width: 300px; max-width: 300px; border: 1px solid #777}
	#images_list .button-dismiss{position:absolute;top:5px;right:10px;cursor:pointer}
	.select2-container{margin: 0 0 0 10px; vertical-align: top;}
	</style>
<?php 
}

add_action('wp_ajax_get_post_follow_collection_item_ajax', 'get_post_follow_collection_item');
add_action('wp_ajax_nopriv_get_post_follow_collection_item_ajax', 'get_post_follow_collection_item');
function get_post_follow_collection_item() {
	$p = get_post($_POST['post_id']);

	$post_id = $p->ID;
	$post_url = get_permalink($post_id);
	$post_title = $p->post_title;
	$post_imgs = $post_colors = array();

	$p_colors = get_post_meta($post_id, 'post_colors', true);
	foreach ($p_colors as $c_key => $c) {
		$post_colors[$c_key]['code'] = $c['code'];
		$post_colors[$c_key]['name'] = $c['name'];
		$post_colors[$c_key]['code_id'] = $c_key;

		$p_imgs = explode(',', $c['images_ids']);
		$post_imgs[$c_key]['img_src'] = $post_imgs[$c_key]['thumb_src'] = array();
		foreach ($p_imgs as $key => $img_id) {
			$img_src = wp_get_attachment_image_src($img_id, 'full');
			$thumb_src = wp_get_attachment_image_src($img_id, array(35, 50));
			array_push($post_imgs[$c_key]['img_src'], $img_src[0]);
			array_push($post_imgs[$c_key]['thumb_src'], $thumb_src[0]);
		}
	}
	$post_sku = get_post_meta($post_id, 'post_sku', true );
	$post_price = number_format(get_post_meta($post_id, 'post_price', true ), 0, ',', '.');
	$post_sizes = get_post_meta($post_id, 'post_sizes', true);

	$return = array(
		'id' => $post_id,
		'url' => $post_url,
		'title' => $post_title,
		'imgs' => $post_imgs,
		'colors' => $post_colors,
		'sku' => $post_sku,
		'price' => $post_price,
		'sizes' => $post_sizes,
		'user_id' => get_current_user_id()
	);
	echo json_encode($return);
	exit();
}

?>