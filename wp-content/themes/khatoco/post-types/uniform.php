<?php 
/* Register Uniform result post-type */
add_action('init', 'posttype_uniform');
function posttype_uniform() {
	$labels = array(
		'name' => 'Mẫu đồng phục',
		'singular_name' => 'Mẫu đồng phục',
		'all_items' => 'Tất cả các mẫu',
		'add_new' => 'Thêm mẫu',
		'add_new_item' => 'Thêm mẫu',
		'edit_item' => 'Sửa',
		'new_item' => 'Mẫu đồng phục mới',
		'view_item' => 'Xem chi tiết',
		'search_items' => 'Tìm kiếm',
		'not_found' =>  'No Result was found with that criteria',
		'not_found_in_trash' => 'No Result found in the Trash with that criteria',
		'view' =>  'Xem'
	);

	$args = array(
		'labels' => $labels,
		'description' => 'This is the holding location for all ecas',
		'public' => false,
		'publicly_queryable' => true,
		'exclude_from_search' => true,
		'show_ui' => true,
		'show_in_nav_menus' => false,
		'rewrite' => array('slug' => 'mau-dong-phuc', 'with-front' => false),
		'has_archive' => true,
		'menu_position' => 4,
		'menu_icon' => 'dashicons-welcome-widgets-menus',
		'supports' => array('title'),
	);
	register_post_type('uniform', $args);

	register_taxonomy('uni_type', 'uniform', array(
		'hierarchical' => true,
		'label' => 'Kiểu đồng phục',
		'singular_label' => 'Kiểu đồng phục',
		'rewrite' => true,
		'show_in_nav_menus' => false,
		'show_admin_column' => true
	));
}

add_action('admin_init', 'add_metabox_uniform');
function add_metabox_uniform() {
	function display_metabox_uniform_general($post) {
		$post_id = $post->ID;
		$p_info = get_post_meta($post_id, 'uni_info', true);
		$p_type = (get_post_meta($post_id, 'uni_fabric', true)) ? get_post_meta($post_id, 'uni_fabric', true):array();
		$p_form = (get_post_meta($post_id, 'uni_exform', true)) ? get_post_meta($post_id, 'uni_exform', true):array(); ?>
		<input type="hidden" name="do" value="save_uniform_metabox">
		<div class="wrap-metabox clearfix">
			<div class="grid6 clearfix">
				<p><strong>Giới thiệu về loại vải</strong></p>
				<textarea name="uni_info" id="uni_info" width="100%" rows="6"><?php echo $p_info; ?></textarea>
				<div id="fabric_type">
					<p><strong>Kiểu vải</strong><input type="button" id="add-new-fabric-type" class="button" value="+" title="Thêm kiểu vải" style="margin-left: 15px"></p>
					<input type="hidden" id="number_of_fabric" name="number_of_fabric" value="<?php echo (sizeof($p_type) == 0) ? 1:sizeof($p_type); ?>">
					<?php foreach ($p_type as $t_key => $t) {
						echo '<div class="row">
							<input type="text" name="uni_fabric['. $t_key .'][title]" placeholder="Tên kiểu vải" value="'. $t['title'] .'">
							<input type="hidden" class="uni_fabric_img_hide" name="uni_fabric['. $t_key .'][imgs]" value="'. implode(',', $t['imgs']) .'">
							<ul class="img-lst fabric-type clearfix">';
							foreach ($t['imgs'] as $i_key => $i) {
								$img = wp_get_attachment_image_src($i, array(35, 35));
								echo '<li><img src="'. $img[0] .'" alt=""></li>';
							}
						echo '</ul>
							<a class="add-img button" href="javascript:void(0);">Thêm ảnh mẫu vải</a>
							<a class="del-row button" href="javascript:void(0);">Xoá mẫu</a>
						</div>';
					} ?>
					<div class="row">
						<input type="text" name="uni_fabric[<?php echo (int)sizeof($p_type); ?>][title]" placeholder="Tên kiểu vải">
						<input type="hidden" class="uni_fabric_img_hide" name="uni_fabric[<?php echo (int)sizeof($p_type); ?>][imgs]" value="">
						<ul class="img-lst fabric-type clearfix"></ul>
						<a class="add-img button" href="javascript:void(0);">Thêm ảnh mẫu vải</a>
						<a class="del-row button" href="javascript:void(0);">Xoá mẫu</a>
					</div>
				</div>
			</div>
			<div id="exform" class="grid6 clearfix">
				<p><strong>Các mẫu sản phẩm đồng phục có sẵn</strong></p>
				<input type="hidden" name="uni_exform" class="uni_exform_img_hide" value="<?php echo implode(',', $p_form); ?>">
				<ul class="img-lst exform clearfix"><?php 
				foreach ($p_form as $i_key => $i) {
					$img = wp_get_attachment_image_src($i, 'full');
					echo '<li><img src="'. $img[0] .'" alt=""></li>';
				}
				 ?></ul>
				<p><input type="button" id="add-exform-img" class="button" value="Thêm ảnh"></p>
			</div>
		</div>
		<style>
		.clearfix:before, .clearfix:after {content: ""; display: block; height: 0; overflow: hidden}
		.clearfix:after {clear: both}
		.fl{float: left} .fr{float: right}
		.wrap-metabox, .row{width: 100%; margin: 13px 0}
		.wrap-metabox{margin: 0; padding: 0}
		.grid6{float: left; width: 50%; padding: 0 13px; box-sizing: border-box; -moz-box-sizing: border-box; -webkit-box-sizing: border-box}
		.grid6 textarea, .grid6 input[type="text"]{width: 90%; margin: 0 10% 0 0; resize: none}
		ul.img-lst li{width: 35px; height: 35px; margin-right: 10px; float: left; cursor: pointer}
		ul.img-lst.exform li{width: 30%; height: auto; margin-right: 3%}
		ul.img-lst.fabric-type li img{width: 35px; height: 35px}
		ul.img-lst.exform li img{width: 100%; height: auto}
		</style>
		<script>
		jQuery(document).ready(function() {
			// add fabric type
			jQuery('#add-new-fabric-type').click(function(event) {
				event.preventDefault();

				var current_num_row = parseInt(jQuery('#number_of_fabric').val()) + 1;
				jQuery('#number_of_fabric').val(current_num_row);

				var row_html = '<div class="row">';
				row_html += '	<input type="text" name="uni_fabric['+ current_num_row +'][title]" placeholder="Tên kiểu vải">';
				row_html += '	<input type="hidden" class="uni_fabric_img_hide" name="uni_fabric['+ current_num_row +'][imgs]" value="">';
				row_html += '	<ul class="img-lst fabric-type clearfix"></ul>';
				row_html += '	<a class="add-img button" href="javascript:void(0);">Thêm ảnh mẫu vải</a>';
				row_html += '	<a class="del-row button" href="javascript:void(0);">Xoá mẫu</a>';
				row_html += '</div>';

				jQuery('#fabric_type').append(row_html);
			});
			// add fabric type image
			jQuery('#fabric_type').delegate('.add-img', 'click', function(event) {
				var _this = jQuery(this);
				var file_frame = wp.media.frames.file_frame = wp.media({
					title: 'Chọn ảnh',
					library: {},
					button: {text: 'Chọn'},
					multiple: true
				});

				file_frame.on('select', function() {
					var attachment_ids = [];
					attachments = file_frame.state().get('selection').toJSON();
					imgs_html = '';
					jQuery.each(attachments, function(index, item){
						attachment_ids.push(item.id);
						imgs_html += '<li><img src="'+ item.url +'" /></li>';
					});

					var _this_row = _this.parents('.row');

					_this_row.find('.uni_fabric_img_hide').val(attachment_ids.join(','));
					_this_row.find('.img-lst').html(imgs_html);
				});

				file_frame.open();
			});
			// delete fabric type
			jQuery('#fabric_type').delegate('.del-row', 'click', function(event) {
				jQuery(this).parents('.row').remove();
			});
			// add exform image
			jQuery('#add-exform-img').click(function(event) {
				var _this = jQuery('#exform');
				var file_frame = wp.media.frames.file_frame = wp.media({
					title: 'Chọn ảnh',
					library: {},
					button: {text: 'Chọn'},
					multiple: true
				});

				file_frame.on('select', function() {
					var attachment_ids = [];
					attachments = file_frame.state().get('selection').toJSON();
					imgs_html = '';
					jQuery.each(attachments, function(index, item){
						attachment_ids.push(item.id);
						imgs_html += '<li><img src="'+ item.url +'" /></li>';
					});

					_this.find('.uni_exform_img_hide').val(attachment_ids.join(','));
					_this.find('.img-lst').html(imgs_html);
				});

				file_frame.open();
			});
		});
		</script>
	<?php }
	add_meta_box(
        'metabox_uniform_general',
        'Thông tin đồng phục',
        'display_metabox_uniform_general',
        'uniform',
        'normal',
        'high'
    );
}

function display_uniforms_tab() {
	$terms = get_terms('uni_type', array('hide_empty' => 0, 'orderby' => 'id', 'order' => 'ASC'));
	$t_posts = array();

	echo '<ul class="tab-title list-style-none clearfix">';
	foreach ($terms as $t_key => $term) {
		echo '<li><a class="tab-item" href="#tab'. ($t_key+1) .'">'. $term->name .'</a>';
		$temp = get_posts(array('posts_per_page' => -1, 'post_type' => 'uniform', 'uni_type' => $term->slug));
		$t_posts[$t_key] = $temp;
		echo '	<ul class="tab-subtitle list-style-none clearfix">';
		if($temp) {
			$num_posts = sizeof($temp);
			foreach ($t_posts[$t_key] as $p_key => $p) {
				echo '		<li><a class="tab-item" href="sub'. ($p_key+1) .'">'. $p->post_title .'</a>';
				if($p_key < ($num_posts-1))
					echo '<span>&nbsp;|&nbsp;&nbsp;</span></li>';
				else
					echo '</li>';
			}
		}
		echo '	</ul>';
		echo '</li>';
	}

	foreach ($t_posts as $t_key => $ps) {
		if(sizeof($ps)) {
			foreach ($ps as $p_key => $p) {
				$post_id = $p->ID;
				$p_info = get_post_meta($post_id, 'uni_info', true);
				$p_type = (get_post_meta($post_id, 'uni_fabric', true)) ? get_post_meta($post_id, 'uni_fabric', true):array();
				$p_form = (get_post_meta($post_id, 'uni_exform', true)) ? get_post_meta($post_id, 'uni_exform', true):array();

				echo '<div id="tab'. ($t_key+1) .'sub'. ($p_key+1) .'" class="tab-content clearfix">
				<div class="grid6 intro pl0">
					<p><strong>Giới thiệu về loại vải:</strong></p>
					'. apply_filters('the_content', $p_info);
					foreach ($p_type as $pt_key => $pt) {
						echo '<p><strong>'. $pt['title'] .'</strong></p>';
						echo '<ul class="fabric-wrap clearfix">';
						foreach ($pt['imgs'] as $i_key => $i) {
							$img = wp_get_attachment_image_src($i, 'full');
							echo '	<li><img nopin="nopin" src="'. $img[0] .'" alt=""></li>';
						}
						echo '</ul>';
					}
				echo '</div>
				<div class="grid6 preview">
					<p><strong>Các mẫu sản phẩm đồng phục có sẵn</strong></p>
					<ul class="list-style-none clearfix">';
					foreach ($p_form as $i_key => $i) {
						$img = wp_get_attachment_image_src($i, 'large');
						echo '	<li class="grid4"><a class="fancybox" href="'. $img[0] .'" rel="tab'. ($t_key+1) .'sub'. ($p_key+1) .'"><img nopin="nopin" src="'. $img[0] .'" alt=""></a></li>';
					}
				echo '	</ul>
				</div>';
				echo '</div>';
			}
		}
	}
}