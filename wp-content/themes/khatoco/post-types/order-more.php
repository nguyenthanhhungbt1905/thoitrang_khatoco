<?php 

/*if(get_current_user_id() == -100){
	$args = array(
		'post_type' => 'orders',
		'posts_per_page' => -1,
		'meta_query' => array(
			array(
				'compare' => 'LIKE',
				'key' => 'campaignInfo',
				'value' => serialize(  )
			)
		),
		'fields' => 'ids'
	);

	$os = get_posts( $args );

	foreach ($os as $key => $oid) {
		$c = get_post_meta( $oid, 'campaignInfo', true );
		if(!empty( $c )){
			echo $oid .' ';
		}
	}
	echo 'end';

	// print_r(get_posts( $args ));
}*/

/* Register Uniform Orders result post-type */
add_action('init', 'posttype_uniform_orders');
function posttype_uniform_orders() {
	$labels = array(
		'name' => 'Results',
		'singular_name' => 'Result',
		'all_items' => 'All Result',
		'add_new' => 'Add Result',
		'add_new_item' => 'Add New Result',
		'edit_item' => 'Edit Result',
		'new_item' => 'New Result',
		'view_item' => 'View Details',
		'search_items' => 'Search Result',
		'not_found' =>  'No Result was found with that criteria',
		'not_found_in_trash' => 'No Result found in the Trash with that criteria',
		'view' =>  'View Result'
	);

	$args = array(
		'labels' => $labels,
		'description' => 'This is the holding location for all ecas',
		'public' => false,
		'publicly_queryable' => true,
		'exclude_from_search' => true,
		'show_ui' => false,
		'rewrite' => array('slug' => 'dat-hang-dong-phuc', 'with-front' => false),
		'has_archive' => true,
		'menu_position' => 20,
		'menu_icon' => 'dashicons-format-aside',
		'supports' => array('title'),
	);
	register_post_type('uniform_orders', $args);
}

// Add Uniform Orders Page into Orders Menu
add_action('admin_menu', 'add_uniform_orders_page', 999);
function add_uniform_orders_page() {
	add_submenu_page('edit.php?post_type=orders', 'Đặt hàng đồng phục', 'Đặt hàng đồng phục', 'edit_posts', 'uniform_orders', 'add_uniform_orders_page_callback');
}

function add_uniform_orders_page_callback() {?>
	<div class="wrap">
		<h2>Đặt hàng đồng phục</h2><?php if(isset($_GET['deleted']) && $_GET['deleted'] == '1'): ?>
		<div class="updated below-h2" id="message"><p>Xoá thành công.</p></div><?php endif; ?>
		<div class="updated below-h2" id="update-message" style="padding:0 12px"></div>
		<p><strong><i>Thận trọng khi thay đổi trạng thái đơn hàng!</i></strong></p>
		<table class="wp-list-table widefat fixed" style="margin: 15px 0;">
			<thead>
				<tr>
					<th width="7%">Ngày</th>
					<th width="12%"><strong>Trạng thái</strong></th>
					<th width="10%">Họ và tên</th>
					<th width="10%">SĐT</th>
					<th width="18%">Email</th>
					<th width="9%">Công ty</th>
					<th width="35%">Nội dung</th>
				</tr>
			</thead>
			<tbody><?php 
				$paged = isset($_GET['paged']) ? abs((int) $_GET['paged']) : 1;

				$status = array(
					'uni_new_order' => 'Đơn hàng mới',
					'uni_pending' => 'Bộ phận HĐP',
					'uni_done' => 'Hoàn thành',
					'uni_cancel' => 'Huỷ'
				);

				$result_query = new WP_Query(array(
					'post_type' => 'uniform_orders',
					'posts_per_page' => 20,
					'paged' => $paged
				));

				global $wp_query;

				$i = 0; while ($result_query->have_posts()) : $result_query->the_post(); $i++;

				$order_status = get_post_meta(get_the_id(), 'uni_status', true);

				$trClass = $i%2 == 1 ? 'alternate' : '';?>
				<tr class="<?php echo $trClass;?>" id="uniform_orders_<?php echo get_the_id(); ?>">
					<td><?php the_time('d/m/Y');?></td>
					<td><select data-uni-id="<?php echo get_the_id(); ?>" class="uni_status_change" style="width:100%"><?php foreach ($status as $skey => $s) {
						echo '<option value="'. $skey .'" '. selected($skey, $order_status, false) .'>'. $s .'</option>';
					} ?>
					</select></td>
					<td><strong><?php the_title();?></strong></td>
					<td>
						<p><?php echo get_post_meta(get_the_id(), 'uni_phone', true);?></p>
					</td>
					<td>
						<p><?php echo get_post_meta(get_the_id(), 'uni_email', true);?></p>
					</td>
					<td>
						<p><?php echo get_post_meta(get_the_id(), 'uni_company', true);?></p>
					</td>
					<td><?php the_content(); ?></td>
				</tr>	
				<?php endwhile;?>
			</tbody>
		</table>
		
		<div class="tablenav bottom">
			<div class="tablenav-pages pagination-links">
				<?php $big = 999999999;
				echo paginate_links(array(
					'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
					'format' => '?paged=%#%',
					'current' => $paged,
					'total' => $result_query->max_num_pages,
					'prev_text' => '«',
					'next_text' => '»'
				));?>
			</div>
		</div>

		<script type="text/javascript">
		jQuery(document).ready(function() {
			jQuery('.uni_status_change').change(function(event) {
				var uni_id = jQuery(this).attr('data-uni-id');
				var uni_new_status = jQuery(this).val();

				jQuery.ajax({
					url: '<?php echo admin_url("admin-ajax.php"); ?>',
					type: 'POST',
					dataType: 'json',
					data: {
						action: 'change_uni_order_status',
						uni_id: uni_id,
						uni_new_status: uni_new_status
					}
				})
				.done(function() {
					jQuery('#update-message').html('<p style="margin:10px 0px;font-size:14px;font-weight:bold">Thay đổi trạng thái thành công!</p>').show().delay(3000).fadeOut();
				})
				.error(function() {
					alert('Không kết nối được đến server! Vui lòng F5 rồi thử lại!');
				});;
				
			});
		});
		</script>
		
		<?php wp_reset_postdata(); ?>		
	</div><?php
}

add_action('wp_ajax_change_uni_order_status', 'change_uni_order_status');
add_action('wp_ajax_nopriv_change_uni_order_status', '__return_false');
function change_uni_order_status(){
	$uni_id = $_POST['uni_id'];
	$uni_new_status = $_POST['uni_new_status'];

	update_post_meta($uni_id, 'uni_status', $uni_new_status);
	exit;
}

// Add Terms and Conditional Page into Orders Menu
add_action('admin_menu', 'add_term_and_conditional_page', 999);
function add_term_and_conditional_page() {
	add_submenu_page('edit.php?post_type=orders', 'Điều khoản thanh toán', 'Điều khoản thanh toán', 'manage_options', 'term_and_conditional', 'add_term_and_conditional_page_callback');
}

function add_term_and_conditional_page_callback() {
	$editor_id = 'term_and_conditional';
	$content = get_option($editor_id);
	if(!$content) {
		add_option($editor_id, '');
	}
	if(isset($_POST[$editor_id])) {
		update_option($editor_id, $_POST[$editor_id]);
	} ?>
	<div class="wrap">
		<h2>Điều khoản thanh toán</h2>
		<form action="" method="POST">
			<p><button class="button button-primary">Cập nhập</button></p>
			<?php $settings = array();
			wp_editor($content, $editor_id, $settings); ?>
		</form>
	</div>
	<div class="clear"></div>
<?php }

// Orders Reports
add_action('admin_menu', 'add_orders_reports_page');
function add_orders_reports_page() {
	add_submenu_page('edit.php?post_type=orders', 'Báo cáo - Thống kê', 'Báo cáo - Thống kê', 'manage_options', 'orders_reports', 'add_orders_reports_page_callback');
}

function add_orders_reports_page_callback() {
	global $wpdb, $wp_locale;
	$post_type = 'orders';
	$export_orders = array(
		'order_code' => array(
			'required' => 1,
			'label' => 'Mã đơn hàng'
		),
		/*'order_info' => array(
			'required' => 0,
			'label' => 'Chi tiết đơn hàng'
		),*/
		'order_status' => array(
			'required' => 1,
			'label' => 'Trạng thái đơn hàng'
		),
		'order_price' => array(
			'required' => 1,
			'label' => 'Giá trị đơn hàng'
		),
		'order_price_all' => array(
			'required' => 1,
			'label' => 'Tổng giá trị đơn hàng'
		),
		'order_shipping_transport_method' => array(
			'required' => 0,
			'label' => 'Phương thức giao hàng'
		),
		'order_payment_method' => array(
			'required' => 0,
			'label' => 'Phương thức thanh toán'
		),
		'order_date' => array(
			'required' => 0,
			'label' => 'Ngày đặt hàng'
		),
		'order_user_id' => array(
			'required' => 0,
			'label' => 'Khách hàng'
		),
		'order_customer_email' => array(
		'required' => 0,
		'label' => 'Email khách hàng'
		),
		'order_customer_phone' => array(
			'required' => 0,
			'label' => 'SĐT khách hàng'
		),
		'order_customer_address' => array(
			'required' => 0,
			'label' => 'Tỉnh/Thành Phố'
		),
		'order_campaign' => array(
			'required' => 0,
			'label' => 'Chương trình khuyến mại'
		),
		'order_coupon' => array(
			'required' => 0,
			'label' => 'Mã giảm giá'
		),
	);
	$export_products = array(
		'post_sku' => array(
			'required' => 1,
			'label' => 'SKU'
		),
		'post_title' => array(
			'required' => 0,
			'label' => 'Tên sản phẩm'
		),
		'post_color' => array(
			'required' => 0,
			'label' => 'Màu sản phẩm'
		),
		'post_size' => array(
			'required' => 0,
			'label' => 'Size'
		),
		'post_price' => array(
			'required' => 0,
			'label' => 'Đơn giá'
		),
		'be_bought' => array(
			'required' => 1,
			'label' => 'Số lượng đã bán (thực tế)'
		),
		'price_total' => array(
			'required' => 0,
			'label' => 'Tổng doanh thu thực tế'
		),
	);

	$export_campaigns = array(
		'name' => array(
			'required' => 1,
			'label' => 'Tên chương trình'
		),
		'time' => array(
			'required' => 1,
			'label' => 'Thời gian thực hiện'
		),
		'status' => array(
			'required' => 1,
			'label' => 'Trạng thái'
		),
		'deals' => array(
			'required' => 1,
			'label' => 'Ưu đãi khác (thẻ thành viên, coupon hay hình thức thanh toán)'
		),
		/*'view_counter' => array(
			'required' => 0,
			'label' => 'Lượt truy cập'
		),*/
		'orders_created' => array(
			'required' => 0,
			'label' => 'Số đơn hàng phát sinh'
		),
		'orders_completed' => array(
			'required' => 0,
			'label' => 'Số đơn hàng thành công'
		),
		'users_register' => array(
			'required' => 0,
			'label' => 'Thành viên đăng ký mới'
		),
		'coupon_counter' => array(
			'required' => 0,
			'label' => 'Số coupon đã sử dụng'
		),
	);

	$months = $wpdb->get_results( $wpdb->prepare( "
		SELECT DISTINCT YEAR( post_date ) AS year, MONTH( post_date ) AS month
		FROM $wpdb->posts
		WHERE post_type = %s
		ORDER BY post_date DESC
	", $post_type ) );

	$months = apply_filters( 'months_dropdown_results', $months, $post_type );
	$quarters = array(); ?>
	<div class="wrap">
		<h2>Báo cáo - Thống kê</h2>
		<?php echo (isset($_GET['response']) && $_GET['response'] == 'empty') ? '<p><strong>Không có dữ liệu trả về</strong></p>':''; ?>
		<hr>
		<form action="<?php echo get_template_directory_uri(); ?>/includes/exports.php" id="export_orders_reports" method="POST">
			<?php wp_nonce_field('export', 'wp_nonce_export_data'); ?>
			<div class="pre-export">
				<h3>Lựa chọn thời gian xuất báo cáo</h3>
				<p>
					<label for="export-date"><input type="radio" name="export_time" class="export_time" value="date" checked="" id="export-date" required="">Theo ngày: Từ ngày&nbsp;</label>
					<input type="text" name="export_date_from" class="datetime_picker">&nbsp;đến ngày&nbsp;
					<input type="text" name="export_date_to" class="datetime_picker">
				</p>
				<p>
					<label for="export-month"><input type="radio" name="export_time" class="export_time" value="month" id="export-month" required="">Theo tháng</label>
					<label for="filter-by-date" class="screen-reader-text">Chọn tháng</label>
					<select name="export_month_year" id="filter-by-date"><?php 
						foreach ( $months as $arc_row ) {
							if ( 0 == $arc_row->year )
								continue;

							$quarter;
							$m = (int) $arc_row->month;
							$year = $arc_row->year;

							if($m < 4)
								$quarter = '01';
							elseif($m > 3 && $m < 7)
								$quarter = '02';
							elseif($m > 6 && $m < 10)
								$quarter = '03';
							elseif($m > 9)
								$quarter = '04';

							if(!in_array($year .'-'. $quarter, $quarters)){
								array_push($quarters, $year .'-'. $quarter);
							}

							$month = zeroise( $arc_row->month, 2 );

							printf( "<option value='%s'>%s</option>\n",
								esc_attr( $arc_row->year .'-'. $month ),
								sprintf( '%1$s, %2$d', 'Tháng '. $month, $year )
								// sprintf( '%1$s %2$d', $wp_locale->get_month( $month ), $year )
							);
						} ?>
					</select>
				</p>
				<p>
					<label for="export-quarter"><input type="radio" name="export_time" class="export_time" value="quarter" id="export-quarter">Theo quý</label>
					<select name="export_quarter_month" id="filter-by-date"><?php if(!empty($quarters))
						foreach ($quarters as $quarter) {
							list($y, $q) = explode('-', $quarter);
							echo '<option value="'. $quarter .'">Quý '. $q .' năm '. $y .'</option>';
						} ?>
					</select>
				</p>
				<p>
					<label for="export-campaign"><input type="radio" name="export_time" class="export_time" value="campaign" id="export-campaign">Theo các chương trình khuyến mãi</label>
				</p>
				<ul id="export-campaign-attrs" style="padding-left: 25px;"><?php foreach ($export_campaigns as $ckey => $c): ?>
					<li><?php echo (1 == $c['required']) ? '<strong>'. $c['label'] .'</strong>':$c['label']; ?></li>
				<?php endforeach; ?></ul>
			</div>
			<div class="choice-export">
				<h3>Lựa chọn nội dung xuất báo cáo</h3>
				<p>
					<label for="export_orders"><input type="radio" name="export_what" value="orders" class="export-what" id="export_orders" required="">Báo cáo chi tiết đơn hàng</label>
					<br><span><i>Báo cáo chi tiết đơn hàng cho biết giá trị đơn hàng, phương thức thanh toán được sử dụng cũng như đơn hàng được mua bởi khách hàng nào...</i></span>
				</p>
				<ul class="toggle-what"><?php foreach ($export_orders as $ekey => $e): ?>
					<li><?php echo (1 == $e['required']) ? '<strong>'. $e['label'] .'</strong>':$e['label'];
					if($ekey == 'order_status'): ?>
					<select name="export_orders_status[]" class="select-two" multiple="multiple">
						<option value="neworder">Mới đặt hàng</option>
						<option value="pending">Đang xử lý tại kho</option>
						<option value="shipping">Đang giao hàng</option>
						<option value="not_available">Hết hàng</option>
						<option value="not_receiving">Khách hàng không nhận</option>
						<option value="return">Hàng trả về kho</option>
						<option value="cancel">Hủy đơn hàng</option>
						<option value="done">Đã hoàn thành</option>
					</select>
				<?php endif; ?></li>
				<?php endforeach; ?></ul>
				<p>
					<label for="export_products"><input type="radio" name="export_what" checked="" value="products" class="export-what" id="export_products">Báo cáo chi tiết sản phẩm</label>
					<br><span><i>Báo cáo chi tiết sản phẩm cho biết thời gian, số lượng, giá cả, màu sắc,... của các sản phẩm đã bán được</i></span>
				</p>
				<ul class="toggle-what"><?php foreach ($export_products as $pkey => $p): ?>
					<li><?php echo (1 == $p['required']) ? '<strong>'. $p['label'] .'</strong>':$p['label']; ?></li>
				<?php endforeach; ?></ul>
			</div>
			<p><?php submit_button('Download Export'); ?></p>
		</form>
	</div>
	<div class="clear"></div>
	<style type="text/css">
	.toggle-what{padding-left:25px;display:none}
	.toggle-what .select2{min-width: 200px;}
	</style>
	<script type="text/javascript">
	jQuery(window).load(function() {
		jQuery('.export-what').each(function(event) {
			if(jQuery(this).is(':checked')){
				jQuery(this).parents('p').next('.toggle-what').show();
			}
		});

		if( jQuery('#export-campaign').is(':checked') ){
			jQuery('.choice-export').hide();
		}
	});

	jQuery(document).ready(function(){
		jQuery('.datetime_picker').datetimepicker({
			defaultTime: '00:00',
			lang: 'vi'
		});

		jQuery('.export_time').change(function() {
			if(jQuery(this).attr('id') == 'export-campaign') {
				if(jQuery(this).is(':checked'))
					jQuery('.choice-export').hide();
				else
					jQuery('.choice-export').show();
			}
			else
				jQuery('.choice-export').show();
		});

		jQuery('.export-what').change(function(event) {
			if(jQuery(this).is(':checked')){
				jQuery('.toggle-what').hide();
				jQuery(this).parents('p').next('.toggle-what').show();
			}
		});

		jQuery('.select-two').select2();
	});
	</script>
<?php 
}
