<?php 


add_filter('post_row_actions', 'remove_quick_view_coupon_frontend_post_list', 10, 2);
function remove_quick_view_coupon_frontend_post_list($actions, $post){
	if('coupon' == $post->post_type){
		unset($actions['view']);
	}
	return $actions;
}

add_action('wp_ajax_check_coupon_available', 'check_coupon_available_ajax');
add_action('wp_ajax_nopriv_check_coupon_available', 'check_coupon_available_ajax');
function check_coupon_available_ajax(){
	$coupon_to_check = isset($_POST['coupon']) ? $_POST['coupon']:'';
	echo json_encode(array('result' => check_coupon_available($coupon_to_check)));
	exit();
}

add_action('wp_ajax_filter_users_coupon_by_metakey', 'filter_users_coupon_by_metakey_ajax');
// add_action('wp_ajax_nopriv_filter_users_coupon_by_metakey', 'filter_users_coupon_by_metakey_ajax');
function filter_users_coupon_by_metakey_ajax(){
	$key = sanitize_text_field($_POST['key']);
	$value = $_POST['value'];
	$users = array();
	$args = array(
		'role' => 'subscriber',
		'meta_query' => array(
			'relation' => 'OR',
		),
	);

	// Create meta query
	switch ($key) {
		case 'card':
			if( is_array( $value ) ){
				foreach ($value as $val) {
					$args['meta_query'][] = array(
						'key' => 'user_card',
						'value' => strval($val),
						'compare' => '=',
					);
					if($val == '0'){
						$args['meta_query'][] = array(
							'key' => 'user_card',
							'value' => '',
							'compare' => '=',
						);
					}
				}
			}
			break;

		case 'birth':
			if( is_array( $value ) ){
				foreach ($value as $val) {
					$args['meta_query'][] = array(
						'key' => 'user_birthday_txt',
						'value' => '/'. str_pad(strval($val), 2, '0', STR_PAD_LEFT) .'/',
						'compare' => 'LIKE',
					);
				}
			}
			break;

		case 'gender':
			if( is_array( $value ) ){
				foreach ($value as $val) {
					if($val == '1'){
						$val = array('id' => 'male', 'name' => 'Nam');
					}
					elseif($val == '0'){
						$val = array('id' => 'female', 'name' => 'Nữ');
					}

					$args['meta_query'][] = array(
						'key' => 'user_gender',
						'value' => maybe_serialize($val),
						'compare' => '=',
					);
				}
			}
			break;

		default: break;
	}

	$users = get_users( $args );
	$users_return = array();
	foreach ($users as $ukey => $u){
		$temp = array();
		$temp['id'] = $u->ID;

		$crm_code = get_user_meta( $u->ID, 'code_crm', true ) ? get_user_meta( $u->ID, 'code_crm', true ) : 'WEB'. str_pad($u->ID, 9, '0', STR_PAD_LEFT);
		$phone = (get_user_meta( $u->ID, 'user_phone', true ) != '') ? ' - '. get_user_meta( $u->ID, 'user_phone', true ) : '';
		$user_info = $crm_code . $phone;

		$temp['code'] = $user_info;

		$users_return[] = $temp;
		unset($temp);
	}
	echo json_encode( $users_return );
	exit;
}

// Check coupon available
function check_coupon_available($coupon){
	if(!$coupon) return false;

	$args = array('posts_per_page' => -1, 'post_type' => 'coupon', 'meta_key' => 'coupon_code', 'meta_value' => $coupon);
	$res = get_posts( $args );

	if(!count($res))
		return false;
	else {
		return $res[0]->ID;
	}
}

/* Frontend FUNCS */
function check_coupon_available_with_user(){
	$user_id = get_current_user_id();
	/*if($user_id <= 0){

	}*/

	$args = array(
		'post_type' => 'coupon',
		'posts_per_page' => -1,
		'post_status' => 'publish'
	);
	$coupons = get_posts( $args );

	if(!$coupons)
		return false;
	else{
		$coupons_can_used = array();

		foreach ($coupons as $ckey => $coupon) {
			$check = true;
			$coupon_id = $coupon->ID;
			$coupon_metadata = get_post_meta( $coupon_id );

			if(check_coupon_is_runnable_in_campaign($coupon_id)){
				// Login required
				if( !isset( $coupon_metadata['coupon_login_required'][0] ) )
					$coupon_metadata['coupon_login_required'][0] = 0;
				if( $user_id <= 0 || !$coupon_metadata['coupon_login_required'][0] ){
					$check = false;
				}
				else{
				}
				// Coupon could be used by this user
				$coupon_users_only = maybe_unserialize( $coupon_metadata['coupon_users_only'][0] );
				if( empty($coupon_users_only) ){
					// Check doi tuong dang nhap
					if( isset( $coupon_metadata['coupon_users_filter'] ) ){
						$by = $coupon_metadata['coupon_users_filter'][0]['keys'];
						switch ($by) {
							case 'card':
								$card = get_user_meta( $user_id, 'user_card', true );
								if( $card == '1' || $card == 'SILVER' ) $card = '1';
								if( $card == '2' || $card == 'GOLD' ) $card = '2';
								if( $card == '3' || $card == 'DIAMOND' ) $card = '3';

								if( !in_array( $card, maybe_unserialize($coupon_metadata['coupon_users_filter'][0]['values']) ) ){
									$check = false;
								}
								break;
							case 'birth':
								$user_birthday = get_user_meta( $user_id, 'user_birthday', true );
								if( in_array( $user_birthday['month'], maybe_unserialize($coupon_metadata['coupon_users_filter'][0]['values']) ) ){
									$check = false;
								}
								break;
							case 'gender':
								$user_gender = get_user_meta( $user_id, 'user_gender', true );
								$gender = '';
								if( isset( $user_gender['id'] ) ){
									if($user_gender['id'] == 'male'){
										$gender = '1';
									}
									if($user_gender['id'] == 'female'){
										$gender = '0';
									}
								}

								if( !in_array( $gender, maybe_unserialize($coupon_metadata['coupon_users_filter'][0]['values']) ) ){
									$check = false;
								}
								break;
							default: break;
						}
					}
				}
				elseif( !in_array( $user_id, $coupon_users_only ) ){
					$check = false;
				}
				else{
					// Check the time used by coupon
					$coupon_time_select = get_post_meta($coupon_id, 'coupon_time_select', true);
					$$coupon_time_select = get_post_meta($coupon_id, $coupon_time_select, true);

					$start_date;
					$end_date;
					if($coupon_time_select == 'fromto') {
						$start_date = $fromto[0];
						$end_date = $fromto[1];
					}
					elseif($coupon_time_select == 'range') {
						$user_data = get_userdata( $user_id );
						$start_date = $user_data->user_registered;
						$time_use_date_format = (int)$range[0]*(int)$range[1];
						$end_date = date('Y-m-d H:i:s', strtotime($start_date) + (24*3600*$time_use_date_format));
					}
					else{
						$check = false;
					}

					if(!check_current_date_in_range($start_date, $end_date)){
						$check = false;
					}
					else{
						if( intval( $coupon_metadata['coupon_quota'][0] ) > intval( $coupon_metadata['coupon_used_time'][0] ) ){
							$coupon_used = get_user_meta($user_id, 'coupon_used', true);
							if($coupon_used == '') $coupon_used = array();
							$coupon_time_use = $coupon_metadata['coupon_time_use'][0];
							if( isset( $coupon_used[$coupon_id] ) && count( $coupon_used[$coupon_id] ) >= $coupon_time_use ){
								$check = false;
							}
						}
						else{
							$check = false;
						}
					}
				}
			}
			else{
				$check = false;
			}

			if( $check )
				$coupons_can_used[] = array(
					'id' => $coupon_id,
					'code' => get_post_meta( $coupon_id, 'coupon_code', true ),
					'kind' => get_post_meta($coupon_id, 'coupon_kind', true)
				);
		}

		return $coupons_can_used;
	}
}

/* Check a text input which is a invalid coupon - when apply coupon in frontend*/
function check_invalid_coupon($coupon_user, $user_id){
	$coupon_id = check_coupon_available($coupon_user);
	if(!$coupon_id) {
		return array('result' => 0, 'message' => 'Coupon này không tồn tại!');
	}

	if(!check_coupon_is_runnable_in_campaign($coupon_id)){
		return array('result' => 0, 'message' => 'Coupon không được sử dụng trong đợt khuyến mại này!');
	}

	$coupon_login_required = get_post_meta( $coupon_id, 'coupon_login_required', true );
    $coupon_login_required = empty($coupon_login_required) ? 0 : intval($coupon_login_required);

	if( $user_id <= 0 && $coupon_login_required > 0 ){ // Yêu cầu đăng nhập
		return array('result' => 0, 'message' => 'Coupon yêu cầu khách hàng đã đăng nhập hệ thống!');
	}

    $valiate_coupon_by_users = valiate_coupon_by_users($coupon_id,$user_id);
    if ($valiate_coupon_by_users['result'] == 0)
        return $valiate_coupon_by_users;
	
	$coupon_quota = intval( get_post_meta($coupon_id, 'coupon_quota', true) ); // Số lần sử dụng tối đa của coupon
	$coupon_used_time = intval( get_post_meta($coupon_id, 'coupon_used_time', true)) + 1; // +1 cho lần sử dụng này

	if( $coupon_quota != '' && $coupon_quota < $coupon_used_time ){
		return array('result' => 0, 'message' => 'Coupon tới hạn số lần sử dụng!');
	}
	else{
		$coupon_used = get_user_meta($user_id, 'coupon_used', true);
		if($coupon_used == '') $coupon_used = array();
		$coupon_time_use = get_post_meta( $coupon_id, 'coupon_time_use', true );
		if( isset( $coupon_used[$coupon_id] ) && count( $coupon_used[$coupon_id] ) >= $coupon_time_use ){
			return array('result' => 0, 'message' => 'Bạn đã sử dụng coupon này!');
		}
	}

	$coupon_time_select = get_post_meta($coupon_id, 'coupon_time_select', true);
	$$coupon_time_select = get_post_meta($coupon_id, $coupon_time_select, true);

	$start_date = null;
	$end_date = null;
	if($coupon_time_select == 'fromto') {
		$start_date = $fromto[0];
		$end_date = $fromto[1];
	}
	elseif($coupon_time_select == 'range') {
		$user_data = get_userdata($user_id);
		$start_date = $user_data->user_registered;
		$time_use_date_format = (int)$range[0]*(int)$range[1];
		$end_date = date('Y-m-d H:i:s', strtotime($start_date) + (24*3600*$time_use_date_format));
	}
	/*else {
		return array('result' => 0, 'message' => 'Coupon hết hạn');
	}*/

	if(!check_current_date_in_range($start_date, $end_date)){
		return array('result' => 0, 'message' => 'Coupon hết hạn sử dụng');
	}

	// Everything OK
	$coupon_kind = get_post_meta($coupon_id, 'coupon_kind', true);
	$limit = get_post_meta($coupon_id,'limit_decrease',true);
	$limit = empty($limit) ? 0 : intval($limit);
	$coupon_kind []= $limit;
	return array('result' => 1, 'message' => 'OK', 'return' => $coupon_kind);
}

function valiate_coupon_by_users($coupon_id,$user_id){ 
    // return array('result'=>...,'message'=>....) valiate coupon by users admin selected in backend
    
    $coupon_users_filter = get_post_meta( $coupon_id, 'coupon_users_filter', true );
    $message = '';
    
    if( !empty($coupon_users_filter) ){ 
		
		$list_val = maybe_unserialize($coupon_users_filter['values']); 
        $check = true;
		switch ($coupon_users_filter['keys']) {
			case 'card':
				$card = get_user_meta($user_id, 'user_card', true );
				if( $card == '1' || $card == 'SILVER' ) $card = '1';
				if( $card == '2' || $card == 'GOLD' ) $card = '2';
				if( $card == '3' || $card == 'DIAMOND' ) $card = '3';

				if( !in_array( $card, $list_val) && $list_val[0]!=0){ 
                    // Card not match with card selected by admin && card selected must != 0
					$check = false;
                    $message = 'Loại thẻ của bạn không được áp dụng trong mã khuyến mãi này.';
				}
				break;
			case 'birth':
				$user_birthday = get_user_meta( $user_id, 'user_birthday', true );
				if( !in_array($user_birthday['month'],$list_val) && $list_val[0]!=0) {
					$check = false;
                    $message = 'Ngày sinh của bạn không được áp dụng vào mã khuyến mãi này.';
				}
				break;
			case 'gender':
				$user_gender = get_user_meta( $user_id, 'user_gender', true );
				$gender = '';
                
				if( isset($user_gender['id']) )
				$gender = ($user_gender['id'] == 'male') ? '1' : '0';

				if( !in_array( $gender, $list_val ) && $list_val[0]!=2 ){
					$check = false;
                    $message = 'Giới tính của bạn không được áp dụng trong mã khuyến mãi này.';
				}
				break;
			default: break;
		}

		if( !$check )
        return array('result' => 0, 'message' => 'Bạn không được sử dụng coupon này vì: '.$message);
	}
    else {
        
       //This coupon use for target list users selected before
        $coupon_users_only = get_post_meta( $coupon_id, 'coupon_users_only', true );
        if (!empty($coupon_users_only) && !in_array($user_id, $coupon_users_only) )
		return array('result' => 0, 'message' => 'Bạn không được sử dụng coupon này vì: Không thuộc danh sách đối tượng được sử dụng coupon.');
        
    }
    
    return array('result' => 1, 'message' => 'Coupon look fine');
}

require_once(get_template_directory().'/post-types/coupon-backend.php');
