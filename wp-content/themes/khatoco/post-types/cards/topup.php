<?php
    add_action('admin_menu', 'registerSubmenuTopup');
    function registerSubmenuTopup(){
        add_submenu_page(
            'edit.php?post_type=cards',
            __( 'Mệnh giá thẻ cào', 'khatoco' ),
            __( 'Mệnh giá thẻ cào', 'khatoco' ),
            'manage_options',
            'topup-manager',
            'topupAction'
        );
    }
    function topupAction(){
        global $wpdb;
        $valueCardData = $wpdb->get_results("select * from `{$wpdb->prefix}value_cards`");
        ?>
            <div class="wrap">
                <h1>Mệnh Giá Thẻ Cào</h1>
                <div style="margin-top: 10px;" id="alertCardValue"></div>
                <table class="form-table" style="width: 50%">
                    <tbody>
                    <?php
                        if($valueCardData):
                            foreach ($valueCardData as $vcKey => $vCard):
                    ?>
                        <tr class="value-card-item">
                            <th>
                                <label for="">Mệnh giá</label>
                            </th>
                            <td>
                                <input type="text" name="value" data-value="<?= $vCard->card_value ?>" value="<?= $vCard->card_value/1000 ?>k" disabled="">
                            </td>
                            <td>
                                <input type="text" name="quantity" data-value="<?= $vCard->card_quantity ?>" value="<?= $vCard->card_quantity ?>" style="float: right">
                                <br/>
                                <br/>
                                <p class="alert-quantity text-danger"></p>
                            </td>
                        </tr>
                    <?php
                            endforeach;
                        endif;
                    ?>
                        <tr>
                            <td colspan="3" style="text-align: right">
                                <button type="button" id="saveCard">Submit</button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <script type="text/javascript">
                jQuery(document).ready(function ($) {

                    $('#saveCard').click(function () {
                        var cards = [];
                        var errors = false;
                        $('.value-card-item').each((index, item)=>{
                            var cvalue = $(item).find('input[name="value"]').attr('data-value');
                            var cquantity = $(item).find('input[name="quantity"]').val();

                            if(isNaN(cvalue) || isNaN(cquantity) || $.trim(cquantity)=="" || parseInt(cquantity) < 0){
                                $(item).find('.alert-quantity').text("Số lượng không hợp lệ").css('float','right');
                                errors = true;
                            }else{
                                $(item).find('.alert-quantity').text("");
                                cards.push({ value: cvalue , quantity : cquantity });
                            }

                        })
                        if(!errors){
                            $.ajax({
                                url: '<?= admin_url('admin-ajax.php'); ?>',
                                type: 'post',
                                data: { action: 'saveValueCard', cards: cards },
                                beforeSend: function () {

                                },
                                success: function (response) {
                                    var rsp = $.parseJSON(response)
                                    if(rsp.status === 'success'){
                                        $('#alertCardValue').text(rsp.message).css({'color':'#0275d8'});
                                    }else{
                                        $('#alertCardValue').text(rsp.message).css({'color':'red'});
                                    }
                                }
                            })
                        }

                    })

                })
            </script>

        <?php
    }


    //check phone exists
    add_action('wp_ajax_saveValueCard', 'saveValueCardInit');
    add_action('wp_ajax_nopriv_saveValueCard', 'saveValueCardInit');
    function saveValueCardInit(){

        global $wpdb;
        $cards = $_POST['cards'];
        $errors = [];
        if(!$cards){
            $errors['status'] ='errors';
            $errors['message'] = 'Đã có lỗi xảy ra xin vui lòng thử lại!';
            echo json_encode($errors);
            wp_die();
        }
        foreach ($cards as $cKey => $card){
            $quantity = $card['quantity'];
            $valueCardData = $wpdb->get_results("select * from `{$wpdb->prefix}value_cards` where `card_value`=".$card['value']);
            if($valueCardData){
                $id = $valueCardData[0]->id;

                $wpdb->update($wpdb->prefix."value_cards", ['card_quantity' => $quantity], ['id' => $id]);

            }
        }


        $errors['status'] = 'success';
        $errors['message'] = 'Mệnh giá thẻ cào đã được cập nhật thành công';
        echo json_encode($errors);
        wp_die();

    }


?>