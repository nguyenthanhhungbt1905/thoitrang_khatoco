<?php

    //add submenu active serial
    add_action('admin_menu', 'registerSubmenuActive');
    function registerSubmenuActive(){
        add_submenu_page(
            'edit.php?post_type=cards',
            __( 'Active thẻ cào', 'khatoco' ),
            __( 'Active thẻ cào', 'khatoco' ),
            'manage_options',
            'active-serial-code',
            'activeAction'
        );
    }
    function activeAction(){
        ?>
            <style type="text/css">
                .wrap {
                    margin: 10px 20px 0 2px;
                }
                .import-container {
                    border: 1px solid #0073aa;
                    background: #f2f2f2;
                    margin-bottom: 20px;
                }
                .import-header {
                    padding: 10px 15px;
                    background: #0073aa;
                    color: #fff;
                }
                .import-header h3 {
                    font-size: 20px;
                }
                .import-wrap {
                    padding: 40px;
                    text-align: center;
                }
                .fileContainer {
                    overflow: hidden;
                    position: relative;
                    background: #fff;
                    padding: 0px 50px;
                    height: 40px;
                    line-height: 40px;
                    color: #333;
                    border: 1px solid #ccc;
                }
                label {
                    display: inline-block;
                }
                .wp-core-ui .active-button{
                    height: 40px;
                    line-height: 40px;
                    background: #333;
                    color: #fff;
                    padding: 0 30px;
                    border: 0;
                    border-radius: 0;
                    font-size: 18px;
                }
                .fileContainer span {
                    line-height: 35px;
                }
                .wp-admin input[type=file] {
                    padding: 3px 0;
                }
                .fileContainer [type=file] {
                    cursor: inherit;
                    display: block;
                    font-size: 999px;
                    filter: alpha(opacity=0);
                    min-height: 100%;
                    min-width: 100%;
                    opacity: 0;
                    position: absolute;
                    right: 0;
                    text-align: right;
                    top: 0;
                }
                .wp-core-ui .button.button-wait {
                    display: none;
                }
                .fileContainer span {
                    line-height: 35px;
                }
            </style>
            <div class="wrap">
                <div class="import-container">
                    <div class="import-header">
                        <h3>Active thẻ cào</h3>
                        <!--p>
                            <a href="https://local-khatoco.co/wp-content/plugins/bigin-import-products/public/files/product_import_construct.xlsx">Click Here</a> to Download Template File
                        </p-->
                    </div>
                    <div class="import-wrap">
                        <form method="post" action="" id="activeForm" enctype="multipart/form-data">
                            <div class="upload_message"></div>
                            <div id="activeUploadValue"></div>
                            <label class="fileContainer">
                                <span class="dashicons dashicons-upload"></span> Select File
                                <input type="file" name="active_cards_list" id="active_cards_list" class="inputfile inputfile-6">
                            </label>
                            <input type="button" id="activeButton" class="button active-button" value="Active">
                            <input type="button" class="button active-button button-wait" value="Importing ...">
                        </form>
                    </div>
                </div>
            </div>

            <script type="text/javascript">
                jQuery(document).ready(function ($) {

                    $('#active_cards_list').on('change', function (event) {
                        $('#activeUploadValue').text($(this).val())
                    })

                    $('#activeButton').click(function () {
                        if($('#active_cards_list')[0].files[0] !=undefined && $('#active_cards_list')[0].files[0] != null){
                            var formActiveData = new FormData()
                            formActiveData.append('action', 'activeAction')
                            formActiveData.append('active_cards_list', $('#active_cards_list')[0].files[0])
                            $.ajax({
                                url: '<?= admin_url('admin-ajax.php'); ?>',
                                type: 'post',
                                contentType: false,
                                processData: false,
                                cache: false,
                                data: formActiveData,
                                beforeSend: function () {
                                    $('#activeButton').hide()
                                    $('.active-button.button-wait').show()
                                },
                                success: function (response) {

                                    $('#activeButton').show()
                                    $('.active-button.button-wait').hide()
                                    $('.upload_message').children().remove()

                                    var rsp = $.parseJSON(response)
                                    var listErrors = '<ul>';
                                    for(var msg in rsp){
                                        if(msg != 'status'){
                                            listErrors+=`<li>${rsp[msg].message[0]}</li>`
                                        }
                                    }

                                    listErrors+=`</ul>`
                                    $('.upload_message').append(listErrors)
                                },
                                error: function (error) {

                                }
                            })
                        }else{
                            $('.upload_message').append(`<p>Đã có lỗi khi xử lý, vui lòng chọn file để active!</p>`)
                        }
                    })
                })
            </script>

        <?php
    }

    add_action('wp_ajax_activeAction', 'activeActionInit');
    add_action('wp_ajax_nopriv_activeAction', 'activeActionInit');

    function activeActionInit(){
        global $wpdb;
        $errors=[];
        $table_name = $wpdb->prefix . 'cards_active_temp';
        $listCards = $_FILES['active_cards_list']['tmp_name'];

        $ext = pathinfo($_FILES["active_cards_list"]["name"])['extension'];

        if(empty($listCards) || $ext!='xlsx'){
            $errors['status']   = 'errors';
            $errors[]['message'][]  = 'Template không đúng định dạng';
            echo json_encode($errors);
            wp_die();
        }

        //clear old data in tmp
        clearOldData($table_name);

        $objFile = PHPExcel_IOFactory::identify($listCards);
        $objData = PHPExcel_IOFactory::createReader($objFile);

        $objData->setReadDataOnly(true);

        $objPHPExcel = $objData->load($listCards);

        $sheet = $objPHPExcel->setActiveSheetIndex(0);

        $totalRow = $sheet->getHighestRow();



        $lastColumn = $sheet->getHighestColumn();
        $totalCol = PHPExcel_Cell::columnIndexFromString($lastColumn);

        $errors =[];

        //check limit rows
        if($totalRow>500){
            $errorsTmp = [];
            $errorsTmp['message'][] = 'File active không được nhiều hơn 500 dòng';
            $errors['status'] = 'errors';
            $errors[] = $errorsTmp;
            echo json_encode($errors);
            wp_die();

        }

        $data = [];
        $dataImport = [];
        $current_user = wp_get_current_user();
        for ($i = 2; $i <= $totalRow; $i++) {
            for ($j = 0; $j < $totalCol; $j++) {
                $data[$i - 2][$j] = $sheet->getCellByColumnAndRow($j, $i)->getValue();
            }

            $arr = [];
            $arr['serial'] = $data[$i - 2][0];
            $arr['product_id'] = $data[$i - 2][1];
            $arr['date_scan'] = $data[$i - 2][2];
            $arr['warehouse'] = $data[$i - 2][3];
            $arr['market'] = $data[$i - 2][4];
            $arr['agency'] = $data[$i - 2][5];
            $arr['user_active'] = $current_user->user_login;
            $arr['row'] = $i;
            $arr['note'] = null;

            $checkData = checkNullData($arr, $errors);
            if(!$checkData)
                $arr['note'] = 'error';
            if($arr['serial'] !='')
                $wpdb->insert( $table_name, ['serial' => $data[$i - 2][0], 'product_id' => $data[$i - 2][1], 'date_scan' => $data[$i - 2][2], 'warehouse' => $data[$i - 2][3], 'market' => $data[$i - 2][4], 'agency' => $data[$i - 2][5], 'rowNumb' => $i, 'note' => $arr['note'], 'user_active'=> $arr['user_active']] );
        }

        //active action
        //$query = "CALL activeCard()";
        $wpdb->query("CALL activeCard()");
        if(!isset($errors['status'])){
            $errors['status'] = 'success';
            $errors[0]['message'][] = 'Danh sách đã được active thành công!';
        }
        echo json_encode($errors);
        wp_die();

    }


    function checkNullData($data, &$errors){
        $errorsTmp = [];
        if($data['serial'] == '')
            $errorsTmp['message'][] = 'Serial rỗng, lỗi ở dòng '.$data['row'];
        if($data['product_id'] == '')
            $errorsTmp['message'][] = 'Số serial: '.$data['serial'].' - ID sản phẩm rỗng, lỗi ở dòng '.$data['row'];
        if($data['date_scan'] == '')
            $errorsTmp['message'][] = 'Số serial: '.$data['serial'].' - Ngày scan rỗng, lỗi ở dòng '.$data['row'];
        if($data['warehouse'] == '')
            $errorsTmp['message'][] = 'Số serial: '.$data['serial'].' - Kho xuất rỗng, lỗi ở dòng '.$data['row'];
        if($data['market'] == '')
            $errorsTmp['message'][] = 'Số serial: '.$data['serial'].' - Thị trường rỗng, lỗi ở dòng '.$data['row'];
        if(isset($errorsTmp['message'])){
            $errors['status'] = 'errors';
            $errors[] = $errorsTmp;
            return false;
        }
        return true;
    }






//    function activeSeriCode($data){
//        global $wpdb;
//        //active code
//        //$post = new WP_Query(['posts_per_page' => -1, 'post_type' => 'cards', 'meta_key' => 'serial', 'meta_value' => $data['serial']]);
//        $postid = $wpdb->get_var( "SELECT ID FROM $wpdb->posts WHERE post_title = '" . $data['serial'] . "'" );
//
//
//        $current_user = wp_get_current_user();
//
//        if(get_post_meta($postid,'status',true) === 'blank'){
//            update_post_meta($postid, 'status', 'active');
//
//            metadata_exists('post', $postid, 'userUpdate') ? update_post_meta($postid, 'userUpdate', $current_user->user_login) : add_post_meta($postid, 'userUpdate', $current_user->user_login);
//
//            metadata_exists('post', $postid, 'dateActive') ? update_post_meta($postid, 'dateActive', date('d-m-Y H:i:s')) : add_post_meta($postid, 'dateActive', date('d-m-Y H:i:s'));
//
//            metadata_exists('post', $postid, 'product_id') ? update_post_meta($postid, 'product_id', $data['product_id']) : add_post_meta($postid, 'product_id', $data['product_id']);
//
//            metadata_exists('post', $postid, 'scanned_at') ? update_post_meta($postid, 'scanned_at', $data['date_scan']) : add_post_meta($postid, 'scanned_at', $data['date_scan']);
//
//            metadata_exists('post', $postid, 'ware_house') ? update_post_meta($postid, 'ware_house', $data['warehouse']) : add_post_meta($postid, 'ware_house', $data['warehouse']);
//
//            metadata_exists('post', $postid, 'market_place') ? update_post_meta($postid, 'market_place', $data['market']) : add_post_meta($postid, 'market_place', $data['market']);
//
//            metadata_exists('post', $postid, 'agency') ? update_post_meta($postid, 'agency', $data['agency']) : add_post_meta($postid, 'agency', $data['agency']);
//        }
//    }

?>