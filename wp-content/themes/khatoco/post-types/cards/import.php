<?php

//add submenu active serial
add_action('admin_menu', 'registerSubmenuImport');
function registerSubmenuImport(){
    add_submenu_page(
        'edit.php?post_type=cards',
        __( 'Import thẻ cào', 'khatoco' ),
        __( 'Import thẻ cào', 'khatoco' ),
        'manage_options',
        'import-serial-code',
        'importAction'
    );
}
function importAction(){
    ?>
    <style type="text/css">
        .wrap {
            margin: 10px 20px 0 2px;
        }
        .import-container {
            border: 1px solid #0073aa;
            background: #f2f2f2;
            margin-bottom: 20px;
        }
        .import-header {
            padding: 10px 15px;
            background: #0073aa;
            color: #fff;
        }
        .import-header h3 {
            font-size: 20px;
        }
        .import-wrap {
            padding: 40px;
            text-align: center;
        }
        .fileContainer {
            overflow: hidden;
            position: relative;
            background: #fff;
            padding: 0px 50px;
            height: 40px;
            line-height: 40px;
            color: #333;
            border: 1px solid #ccc;
        }
        label {
            display: inline-block;
        }
        .wp-core-ui .import-button {
            height: 40px;
            line-height: 40px;
            background: #333;
            color: #fff;
            padding: 0 30px;
            border: 0;
            border-radius: 0;
            font-size: 18px;
        }
        .fileContainer span {
            line-height: 35px;
        }
        .wp-admin input[type=file] {
            padding: 3px 0;
        }
        .fileContainer [type=file] {
            cursor: inherit;
            display: block;
            font-size: 999px;
            filter: alpha(opacity=0);
            min-height: 100%;
            min-width: 100%;
            opacity: 0;
            position: absolute;
            right: 0;
            text-align: right;
            top: 0;
        }
        .wp-core-ui .button.button-wait {
            display: none;
        }
    </style>
    <div class="wrap">
        <div class="import-container">
            <div class="import-header">
                <h3>Import thẻ cào</h3>
                <!--p>
                    <a href="https://local-khatoco.co/wp-content/plugins/bigin-import-products/public/files/product_import_construct.xlsx">Click Here</a> to Download Template File
                </p-->
            </div>
            <div class="import-wrap">
                <form method="post" action="" id="importForm" enctype="multipart/form-data">
                    <div class="upload_message"></div>
                    <div id="importUploadValue"></div>
                    <label class="fileContainer">
                        <span class="dashicons dashicons-upload"></span> Select File
                        <input type="file" name="cards_list" id="cards_list" class="inputfile inputfile-6" >
                    </label>
                    <input type="button" id="importButton" class="button import-button" value="Import">
                    <input type="button" class="button import-button button-wait" value="Importing ...">
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        jQuery(document).ready(function ($) {

            $('#cards_list').on('change', function (event) {
                $('#importUploadValue').text($(this).val())
            })

            $('#importButton').click(function () {
                var formData = new FormData()
                if($('#cards_list')[0].files[0] != undefined && $('#cards_list')[0].files[0] != null){
                    formData.append('action', 'importAction')
                    formData.append('cards_list', $('#cards_list')[0].files[0])
                    $.ajax({
                        url: '<?= admin_url('admin-ajax.php'); ?>',
                        type: 'post',
                        contentType: false,
                        processData: false,
                        cache: false,
                        data: formData,
                        beforeSend: function () {
                            $('.upload_message').children().remove()
                            $('#importButton').hide()
                            $('.import-button.button-wait').show()
                        },
                        success: function (response) {

                            $('#importButton').show()
                            $('.import-button.button-wait').hide()
                            console.log(response);
                            var rsp = JSON.parse(response)
                            console.log(rsp);
                            if(rsp.status == 'errors'){
                                var listErrors = '<ul>'
                                rsp.message.forEach((item, index)=>{
                                    listErrors+=`<li>${item}</li>`
                                })
                                listErrors +=`</ul>`
                                $('.upload_message').append(listErrors)
                            }else{
                                $('.upload_message').append(`<p>${rsp.message}</p>`)
                            }
                        },
                        error: function (error) {
                            $('.upload_message').append(`<p>Đã có lỗi khi xử lý, vui lòng import lại!</p>`)
                        }
                    })
                }else{
                    $('.upload_message').append(`<p>Đã có lỗi khi xử lý, vui lòng chọn file để import!</p>`)
                }
                
            })
        })
    </script>

    <?php
}

add_action('wp_ajax_importAction', 'importActionInit');
add_action('wp_ajax_nopriv_importAction', 'importActionInit');
function importActionInit(){
    $errors = [];
    if(isset($_FILES['cards_list'])){
        global $wpdb;
        $table_name = $wpdb->prefix . 'cards_temp';
        $listCards = $_FILES['cards_list']['tmp_name'];

        //clear old data in tmp
        clearOldData($table_name);


        $objFile = PHPExcel_IOFactory::identify($listCards);
        $objData = PHPExcel_IOFactory::createReader($objFile);

        $objData->setReadDataOnly(true);

        $objPHPExcel = $objData->load($listCards);

        $sheet = $objPHPExcel->setActiveSheetIndex(0);

        $totalRow = $sheet->getHighestRow();
        $lastColumn = $sheet->getHighestColumn();
        $totalCol = PHPExcel_Cell::columnIndexFromString($lastColumn);

        $data = [];
        $dataImport =[];
        for ($i = 2; $i <= $totalRow; $i++) {
            for ($j = 0; $j < $totalCol; $j++) {
                $data[$i - 1][$j] = $sheet->getCellByColumnAndRow($j, $i)->getValue();
            }
            $arr = [];
            $arr['code'] = $data[$i - 1][0];
            $arr['pin'] = $data[$i - 1][1];
            $arr['row'] = $i;

            array_push($dataImport, $arr);
            //$wpdb->insert( $table_name, ['code' => $data[$i - 1][0], 'pin' => $data[$i - 1][1], 'row' => $i] );
        }


        //validate data import
        $validator = validateDataImport($dataImport);

        if($validator['status']=='errors'){
            echo json_encode($validator);
            wp_die();
        }

        $insertData = addData($dataImport);
        echo json_encode($insertData);

        wp_die();
    }
}


function addData($dataImport)
{
    global $wpdb;
    $table_name = $wpdb->prefix . 'cards_temp';
    //get all data in temp table
    //$dataTmp = $wpdb->get_results('select * from ' . $table_name);

    if (!$dataImport) return false;
    $current_user = wp_get_current_user();
    try {
        foreach ($dataImport as $dataKey => $data) {
            $postId = wp_insert_post([
                'post_type' => 'cards',
                'post_title' => $data['code'],
                'post_status' => 'publish',
                'comment_status' => 'closed',
                'ping_status' => 'closed',
            ]);
            if ($postId) {
                add_post_meta($postId, 'serial', $data['code']);
                add_post_meta($postId, 'pin', $data['pin']);
                add_post_meta($postId, 'status', 'blank');
                add_post_meta($postId, 'userId', $current_user->user_login);

                add_post_meta($postId, 'createdAt', date("d-m-Y H:i:s"));
            }
        }
        return ['status' => 'success', 'message' => 'Danh sách mã thẻ đã được import thành công!'];
    } catch (Exception $e) {
        return ['status' => 'errors', 'message' => $e->getMessage()];
    }

}


function validateDataImport($dataImport){
    $errors = [];
    global $wpdb;
    $table_name = $wpdb->prefix . 'cards_temp';
    //get all data in temp table
    //$dataTmp = $wpdb->get_results('select * from '.$table_name);

    if(!$dataImport) return false;
    foreach ($dataImport as $dataKey => $data){

        //check data null
        if($data['code'] == '')
            $errors['message'][] = 'Số serial không được rỗng. Lỗi tại dòng: '.$data['row'];
        if($data['pin'] == '')
            $errors['message'][] = 'Mã PIN không được rỗng. Lỗi tại dòng: '.$data['row'];


        //check serial exist
        $postCheckSerial = new WP_Query(['posts_per_page' => -1, 'post_type' => 'cards', 'meta_key' => 'serial', 'meta_value' => $data['code']]);
        $postCheckPin = new WP_Query(['posts_per_page' => -1, 'post_type' => 'cards', 'meta_key' => 'pin', 'meta_value' => $data['pin']]);
        if($postCheckSerial->posts)
            $errors['message'][] = 'Số serial: '.$data['code'].' đã được import. Vui lòng xóa mã để tiếp tục import các mã mới. Lỗi tại dòng: '.$data['row'];
        if($postCheckPin->posts)
            $errors['message'][] = 'Mã PIN: '.$data['pin'].' đã được import. Vui lòng xóa mã để tiếp tục import các mã mới. Lỗi tại dòng: '.$data['row'];

        $exSerial=0;
        $exPin = 0;

        foreach($dataImport as $itKey => $item){
            if($item['code'] == $data['code'])
                $exSerial+=1;
            if($item['pin'] == $data['pin'])
                $exPin+=1;
        }
        

        if($exSerial>1)
            $errors['message'][] = 'So serial: '.$data['code'].' da xuat hien 2 lan. Loi tai dong: '.$data['row'];
        if($exPin>1)
            $errors['message'][] = 'Ma PIN: '.$data['pin'].' da xuat hien 2 lan. Loi tai dong: '.$data['row'];

    }

    $errors['status'] = 'success';
    if(isset($errors['message']))
        $errors['status'] = 'errors';
    return $errors;

}



?>