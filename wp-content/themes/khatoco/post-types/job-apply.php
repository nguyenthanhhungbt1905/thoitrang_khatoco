<?php 
/* Register Job Define */
// add_action('init', 'posttype_job');
function posttype_job() {
	$labels = array(
		'name' => 'Tuyển dụng',
		'singular_name' => 'Vị trí tuyển dụng',
		'all_items' => 'Tuyển dụng',
		'add_new' => 'Đăng tuyển dụng',
		'add_new_item' => 'Đăng tuyển dụng',
		'edit_item' => 'Sửa tin tuyển dụng',
		'new_item' => 'Tin tuyển dụng mới',
		'view_item' => 'Xem chi tiết',
		'search_items' => 'Tìm kiếm',
		'not_found' =>  'No Job was found with that criteria',
		'not_found_in_trash' => 'No Job found in the Trash with that criteria',
		'view' =>  'Xem'
	);

	$args = array(
		'labels' => $labels,
		'description' => 'This is the holding location for all ecas',
		'public' => true,
		'publicly_queryable' => true,
		'exclude_from_search' => true,
		'show_ui' => false,
		'rewrite' => array('slug' => 'tuyen-dung', 'hierarchical' => true),
		'menu_position' => 20,
		'_builtin' => false,
		'menu_icon' => 'dashicons-businessman',
		'supports' => array('title', 'editor'),
	);
	register_post_type('job', $args);
}

/* Register Apply result post-type */
// add_action('init', 'posttype_job_apply');
function posttype_job_apply() {
	$labels = array(
		'name' => 'Results',
		'singular_name' => 'Result',
		'all_items' => 'All Result',
		'add_new' => 'Add Result',
		'add_new_item' => 'Add New Result',
		'edit_item' => 'Edit Result',
		'new_item' => 'New Result',
		'view_item' => 'View Details',
		'search_items' => 'Search Result',
		'not_found' =>  'No Result was found with that criteria',
		'not_found_in_trash' => 'No Result found in the Trash with that criteria',
		'view' =>  'View Result'
	);

	$args = array(
		'labels' => $labels,
		'description' => 'This is the holding location for all ecas',
		'public' => false,
		'publicly_queryable' => true,
		'exclude_from_search' => false,
		'show_ui' => false,
		'has_archive' => true,
		'menu_position' => 20,
		'menu_icon' => 'dashicons-format-aside',
		'supports' => array('title', 'editor'),
	);
	register_post_type('job_apply', $args);
}

/* Register HR posttype */
add_action('init', 'posttype_job_hr');
function posttype_job_hr() {
	$labels = array(
		'name' => 'Câu hỏi thường gặp',
		'singular_name' => 'Câu hỏi thường gặp',
		'all_items' => 'Câu hỏi thường gặp',
		'add_new' => 'Thêm câu hỏi',
		'add_new_item' => 'Thêm câu hỏi',
		'edit_item' => 'Sửa câu hỏi',
		'new_item' => 'Thêm mới',
		'view_item' => 'Xem chi tiết',
		'search_items' => 'Tìm kiếm',
		'not_found' =>  'No posts was found with that criteria',
		'not_found_in_trash' => 'No posts found in the Trash with that criteria',
		'view' =>  'Xem'
	);

	$args = array(
		'labels' => $labels,
		'description' => 'This is the holding location for all ecas',
		'public' => false,
		'publicly_queryable' => true,
		'exclude_from_search' => true,
		'show_ui' => true,
		'show_in_nav_menus' => false,
		'rewrite' => true,
		'show_in_menu' => true,
		'menu_icon' => 'dashicons-editor-help',
		'supports' => array('title', 'editor')
	);
	register_post_type('job-hr', $args);
	// flush_rewrite_rules();
}

/* Register taxonomy for Job HR posttype */
add_action('init', 'taxonomy_for_job_hr');
function taxonomy_for_job_hr() {
	register_taxonomy(
		'job-hr-type',
		'job-hr',
		array(
			'labels' => array(
				'name' => 'Chủ đề câu hỏi',
				'add_new_item' => 'Thêm mới',
				'menu_name' => 'Chủ đề câu hỏi'
			),
			'show_ui' => true,
			'hierarchical' => true,
			'show_admin_column' => true,
			'show_in_nav_menus' => true,
			'rewrite' => array('slug' => 'cau-hoi-thuong-gap'),
		)
	);
	// flush_rewrite_rules();
}

// Add metabox for Job HR Question Posttype


/* add the admin options page */
// add_action('admin_menu', 'add_job_hr_type_page');
function add_job_hr_type_page() {
	add_submenu_page('edit.php?post_type=job', 'Chủ đề câu hỏi', 'Chủ đề câu hỏi', 'manage_categories', 'edit-tags.php?post_type=job&taxonomy=job-hr-type');
}

// add_action('admin_menu', 'add_job_apply_page');
function add_job_apply_page() {
	add_submenu_page('edit.php?post_type=job', 'Danh sách ứng tuyển', 'Danh sách ứng tuyển', 'manage_categories', 'apply', 'job_applying_result_callback');
}

/* display the admin options page */
function job_applying_result_callback() {?>
	<div class="wrap">
		<h2>Danh sách ứng tuyển</h2><?php if(isset($_GET['deleted']) && $_GET['deleted'] == '1'): ?>
		<div class="updated below-h2" id="message"><p>Xoá thành công.</p></div><?php endif; ?>
		<table class="wp-list-table widefat fixed" style="margin: 15px 0;">
			<thead>
				<tr>
					<th width="7%">Ngày</th>
					<th width="20%">Họ và tên</th>
					<th width="10%">SĐT</th>
					<th width="20%">Vị trí ứng tuyển</th>
					<th width="30%">Lời ngỏ</th>
					<th width="10%">Download CV</th>
					<th width="3%">Xoá</th>
				</tr>
			</thead>
			<tbody><?php 
				$paged = isset($_GET['paged']) ? abs((int) $_GET['paged']) : 1;

				$result_query = new WP_Query(array(
					'post_type' => 'job_apply',
					'posts_per_page' => 20,
					'paged' => $paged
				));

				global $wp_query;

				$i = 0; while ($result_query->have_posts()) : $result_query->the_post(); $i++; 

				$trClass = $i%2 == 1 ? 'alternate' : '';?>
				<tr class="<?php echo $trClass;?>" id="job_applying_<?php echo get_the_id(); ?>">
					<td><?php the_time('d/m/Y');?></td>
					<td><strong><?php the_title();?></strong></td>
					<td>
						<p>Phone: <?php echo get_post_meta(get_the_id(), 'apply_phone', true);?></p>
					</td>
					<td>
						<p>Phone: <?php echo get_post_meta(get_the_id(), 'apply_position', true);?></p>
					</td>
					<td>
						<p>Note: <?php echo get_post_meta(get_the_id(), 'apply_note', true);?></p>
					</td>
					<td><?php 
						if(get_post_meta(get_the_id(), 'apply_cv', true)) {
							echo '<p><a href="'. wp_get_attachment_url(get_post_meta(get_the_id(), 'apply_cv', true)) .'">CV</a></p>';
						} ?>
					</td>
					<td>
						<a href="<?php echo get_delete_post_link(get_the_id(), '', true); ?>" title="Xoá">
							<span class="dashicons dashicons-dismiss color-button-dismiss"></span>
						</a>
					</td>
				</tr>	
				<?php endwhile;?>
			</tbody>
		</table>
		
		<div class="tablenav bottom">
			<div class="tablenav-pages pagination-links">
				<?php $big = 999999999; // need an unlikely integer
				echo paginate_links(array(
					'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
					'format' => '?paged=%#%',
					'current' => $paged,
					'total' => $result_query->max_num_pages,
					'prev_text' => '«',
					'next_text' => '»'
				));?>
			</div>
		</div>
		
		<?php wp_reset_postdata();?>		
	</div><?php
}
