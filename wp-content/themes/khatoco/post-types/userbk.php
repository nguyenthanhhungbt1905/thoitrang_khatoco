<?php
/*
user "card":
	0: ko có thẻ
	1: thẻ bạc
	2: thẻ vàng
	3: thẻ kim cương
*/

add_filter('wpmu_welcome_notification', '__return_false');
add_filter('send_password_change_email', '__return_false');

add_filter('manage_users_columns', 'pippin_add_user_turn_over_column');
function pippin_add_user_turn_over_column($columns)
{
	$columns['user_display_name'] = 'Tên';
	$columns['user_card'] = 'Hạng thẻ<br>Mã thẻ';
    $columns['user_phone'] = 'Điện thoại';
    $columns['user_birth'] = 'Ngày sinh';
    $columns['user_turn_over'] = 'Giá trị mua hàng tích luỹ';
    $columns['join_date'] = 'Ngày đăng ký';

    // Unset
    unset($columns['name']);
    unset($columns['email']);
    unset($columns['posts']);
    unset($columns['role']);
    return $columns;
}

add_filter('manage_users_sortable_columns', 'register_users_sortable_columns');
function register_users_sortable_columns( $columns ){
	$columns['user_display_name'] = 'user_display_name';
	$columns['join_date'] = 'join_date';
	return $columns;
}

add_action('manage_users_custom_column', 'show_user_metadata_column_content', 10, 3);
function show_user_metadata_column_content($value, $column_name, $user_id)
{
    $user = get_userdata($user_id);
	if('user_display_name' == $column_name){return $user->display_name;}
    if ('user_turn_over' == $column_name)
        return get_turnover_by_user($user_id);

    if ('user_card' == $column_name) {
        $display = get_user_meta($user_id, 'code_crm', true) ? get_user_meta($user_id, 'code_crm', true) : 'WEB' . str_pad($user_id, 9, '0', STR_PAD_LEFT);
        $display .= '<br>';
        $card = get_user_meta($user_id, 'user_card', true);
        if ($card == '') $card = '0';

        switch (strtoupper($card)) {
            case '1':
            case 'SILVER':
                $display .= 'Thành viên thẻ Bạc';
                break;
            case '2':
            case 'GOLD':
                $display .= 'Thành viên thẻ Vàng';
                break;
            case '3':
            case 'DIAMOND':
                $display .= 'Thành viên thẻ Kim Cương';
                break;
            default :
                $display .= 'Thành viên';
        }
        $display .= '<br>' . get_user_meta($user_id, 'user_card_id', true);
        return $display;
    }

    if ('user_birth' == $column_name) {
        update_user_meta($user_id, 'coupon_used', '');
        return get_user_meta($user_id, 'user_birthday_txt', true);
    }

    if ('user_phone' == $column_name)
        return get_user_meta($user_id, 'user_phone', true);

    if ('join_date' == $column_name) {
        $join_date = get_user_meta($user_id, 'user_registered', true);
        if ($join_date == '') {
            return get_date_from_gmt( date( 'Y-m-d H:i:s', strtotime($user->user_registered) ), 'd-m-Y - H:i:s' );
        }
        return $join_date;
    }

    return $value;
}

//customize user list sort order
add_action('pre_user_query', 'change_user_order');

function change_user_order($query) {
	if($query->query_vars['orderby']=='user_display_name'){
		$query->query_orderby = ' ORDER BY display_name '.($query->query_vars["order"] == 'ASC' ? 'ASC ' : 'DESC ') ;
	}
	else if($query->query_vars['orderby']=='join_date'){
		$query->query_orderby = ' ORDER BY user_registered '.($query->query_vars["order"] == 'ASC' ? 'ASC ' : 'DESC ');
	}
	else {
		$query->query_orderby = ' ORDER BY user_registered DESC';
	}
}

add_filter('user_row_actions', 'edit_user_post_list', 10, 2);
function edit_user_post_list($actions, $user)
{
    $user_id = $user->ID;
    $actions['edit'] = '<a href="' . get_edit_user_link($user_id) . '">View details</a>';
    return $actions;
}

add_action('wp_ajax_backend__update_test_user', 'backend__update_test_user');
function backend__update_test_user()
{
    $id = $_GET['id'];
    var_dump($id);
    var_dump(sync_updateCustomer($id));
    exit;
}

/* user box meta */
add_action('show_user_profile', 'action_personal_options_func');
add_action('edit_user_profile', 'action_personal_options_func');
function action_personal_options_func($user)
{ ?>
    <h3>User Information</h3>
    <?php
    echo '<pre>';
    print_r(sync_buildCustomerObj($user->ID));
    echo '</pre>';

    $code_crm = get_user_meta($user->ID, 'code_crm', true);
    $CustID = ($code_crm != '') ? $code_crm : 'WEB' . str_pad($user->ID, 9, '0', STR_PAD_LEFT);
    $CustName = isset($user->display_name) ? $user->display_name : get_user_meta($user->ID, 'name', true);
    $Email = $user->user_email;
    $CardID = get_user_meta($user->ID, 'user_card_id', true);
    $RepEmail = get_user_meta($user->ID, 'repemail', true);
    $IsB2B = get_user_meta($user->ID, 'isb2b', true);
    $CustName = get_user_meta($user->ID, 'aliasname', true);
    $CustName2 = get_user_meta($user->ID, 'aliasname2', true);
    $CustFullName = $user->display_name;
    $Address = get_user_meta($user->ID, 'address', true);
    $PhoneNo1 = get_user_meta($user->ID, 'user_phone', true);
    $CellPhone = get_user_meta($user->ID, 'user_phone', true);
    $PhoneNo2 = get_user_meta($user->ID, 'user_phone', true);
    $RegVATID = get_user_meta($user->ID, 'vatid', true);
    $Birthday = get_user_meta($user->ID, 'user_birthday', true);
    if (is_array($Birthday)) {
        $Birthday = str_pad($Birthday['day'], 2, '0', STR_PAD_LEFT) . '/' . str_pad($Birthday['month'], 2, '0', STR_PAD_LEFT) . '/' . $Birthday['year'];
    }
    $SkypeID = get_user_meta($user->ID, 'skypeid', true);
    $Twitter = get_user_meta($user->ID, 'twitter', true);
    $Facebook = get_user_meta($user->ID, 'facebook', true);
    $YahooID = get_user_meta($user->ID, 'yahoo', true);
    $EstablishDay = get_user_meta($user->ID, 'establishday', true);
    $user_card = get_user_meta($user->ID, 'user_card', true);
    $user_card_id = get_user_meta($user->ID, 'user_card_id', true);
    $AnnualRevenue = get_user_meta($user->ID, 'annual_revenue', true); ?>

    <p>
        <button class="button" id="sync_user" type="button">Sync This User</button>
    </p>

    <p><label style="width: 200px; display: inline-block;" for="">Customer ID:</label> <input class="regular-text"
                                                                                              type="text" name=""
                                                                                              value="<?php echo $CustID; ?>"
                                                                                              readonly=""></p>
    <p><label style="width: 200px; display: inline-block;" for="">Customer Name:</label> <input class="regular-text"
                                                                                                type="text" name=""
                                                                                                value="<?php echo $CustName; ?>"
                                                                                                readonly=""></p>
    <p><label style="width: 200px; display: inline-block;" for="">Card ID:</label> <input class="regular-text"
                                                                                          type="text" name=""
                                                                                          value="<?php echo $user_card; ?>"
                                                                                          readonly=""></p>
    <p><label style="width: 200px; display: inline-block;" for="">Card Number:</label> <input class="regular-text"
                                                                                              type="text" name=""
                                                                                              value="<?php echo $user_card_id; ?>"
                                                                                              readonly=""></p>
    <p><label style="width: 200px; display: inline-block;" for="">Email:</label> <input class="regular-text" type="text"
                                                                                        name=""
                                                                                        value="<?php echo $Email; ?>"
                                                                                        readonly=""></p>
    <p><label style="width: 200px; display: inline-block;" for="">Address:</label> <input class="regular-text"
                                                                                          type="text" name=""
                                                                                          value="<?php echo $Address; ?>"
                                                                                          readonly=""></p>
    <p><label style="width: 200px; display: inline-block;" for="">PhoneNo1:</label> <input class="regular-text"
                                                                                           type="text" name=""
                                                                                           value="<?php echo $PhoneNo1; ?>"
                                                                                           readonly=""></p>
    <p><label style="width: 200px; display: inline-block;" for="">CellPhone:</label> <input class="regular-text"
                                                                                            type="text" name=""
                                                                                            value="<?php echo $CellPhone; ?>"
                                                                                            readonly=""></p>
    <p><label style="width: 200px; display: inline-block;" for="">PhoneNo2:</label> <input class="regular-text"
                                                                                           type="text" name=""
                                                                                           value="<?php echo $PhoneNo2; ?>"
                                                                                           readonly=""></p>
    <p><label style="width: 200px; display: inline-block;" for="">Birthday:</label> <input class="regular-text"
                                                                                           type="text" name=""
                                                                                           value="<?php echo $Birthday; ?>"
                                                                                           readonly=""></p>
    <p><label style="width: 200px; display: inline-block;" for="">EstablishDay:</label> <input class="regular-text"
                                                                                               type="text" name=""
                                                                                               value="<?php echo $EstablishDay; ?>"
                                                                                               readonly=""></p>
    <p><label style="width: 200px; display: inline-block;" for="">AnnualRevenue:</label> <input class="regular-text"
                                                                                                type="text" name=""
                                                                                                value="<?php echo $AnnualRevenue; ?>"
                                                                                                readonly=""></p>

    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $('#sync_user').click(function (event) {
                $.ajax({
                    url: '<?php echo admin_url("admin-ajax.php"); ?>',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        action: 'backend__sync_update_user_once',
                        id: <?php echo $user->ID; ?>
                    },
                })
                    .done(function (response) {
                        console.log(response);
                        if (response.result) {
                            // location.reload();
                        }
                    });

            });
        });
    </script>

    <!-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->

    <h3>Orders List</h3>
    <p>
        <span><strong>Mã khách hàng:</strong> <?php ?></span>
        <span><strong>Số mã thẻ:</strong> <?php ?></span>
        <span><strong>Hạng thẻ:</strong> <?php ?></span>
        <span class="fr">Giá trị mua hàng tích luỹ: <span
                class="price"><?php echo get_turnover_by_user($user->ID); ?></span></span>
    </p>
    <table id="user-order-list">
        <thead>
        <tr>
            <th width="15%">Mã đơn hàng</th>
            <th width="13%">Trạng thái</th>
            <th width="44%">Mặt hàng</th>
            <th width="13%">Ngày</th>
            <th width="15%">Thành tiền</th>
        </tr>
        </thead>
        <tbody><?php $args = array(
            'posts_per_page' => -1,
            'post_type' => 'orders',
            'orderby' => 'date',
            'order' => 'DESC',
            'meta_query' => array(
                array(
                    'key' => 'order_user_id',
                    'value' => $user->ID
                )
            )
        );

        $orders = get_posts($args);
        if ($orders):
            $i = 1;
            foreach ($orders as $o_key => $o):
                $class = ($i % 2 == 1) ? '' : 'alternate';
                $o_id = $o->ID;
                $order_info = get_post_meta($o_id, 'order_info', true);
                $orders_products = (isset($order_info['products'])) ? $order_info['products'] : array();
                $order_status = get_post_meta($o_id, 'order_status', true);
                $order_status_txt = '';
                switch ($order_status) {
                    case 'neworder':
                        $order_status_txt = 'Mới đặt hàng';
                        break;
                    case 'pending':
                        $order_status_txt = 'Đang xử lý tại kho';
                        break;
                    case 'shipping':
                        $order_status_txt = 'Đang giao hàng';
                        break;
                    case 'not_available':
                        $order_status_txt = 'Hết hàng';
                        break;
                    case 'not_receiving':
                        $order_status_txt = 'Khách hàng không nhận';
                        break;
                    case 'return':
                        $order_status_txt = 'Hàng trả về kho';
                        break;
                    case 'cancel':
                        $order_status_txt = 'Hủy đơn hàng';
                        break;
                    case 'done':
                        $order_status_txt = 'Đã hoàn thành';
                        break;
                }
                $order_payment_status = get_post_meta($o_id, 'order_payment_status', true);
                $order_payment_code = get_post_meta($o_id, 'order_payment_code', true);
                $order_payment_status_bool = ($order_payment_status == true && $order_payment_code) ? 1 : 0; ?>
                <tr class="<?php echo $class; ?>">
                    <td data-order-id="<?php echo $o_id; ?>"><?php echo get_post_meta($o_id, 'order_code', true); ?></td>
                    <td><?php echo $order_status_txt; ?></td>
                    <td><?php foreach ($orders_products as $op_key => $op) {
                            echo $op['name'];
                            if ($op_key <= sizeof($orders_products) - 1) echo ', ';
                        } ?>&nbsp;</td>
                    <td><?php echo get_the_time('d-m-Y', $o_id); ?></td>
                    <td style="color:<?php echo ($order_payment_status_bool) ? '#00b' : 'red'; ?>"><?php echo isset($order_info['total_formated']) ? $order_info['total_formated'] : '0 Đ'; ?></td>
                </tr>
                <?php $i++; endforeach;
        else:
            echo '<tr><td colspan="5" style="text-align: center">Không có đơn hàng nào!</td></tr>';
        endif; ?>
        </tbody>
    </table>
    <style type="text/css" media="screen">
        #user-order-list table {
            width: 100%;
            border-radius: 3px;
            border-collapse: collapse;
            overflow: hidden;
        }

        #user-order-list thead th, #user-order-list tbody td {
            padding: 8px 15px;
            text-align: left;
        }

        #user-order-list thead th {
            color: #fff;
            font-size: 16px;
            font-weight: normal;
            background-color: #4f5c77;
        }

        #user-order-list thead tr {
            border-top: 1px solid #dcddde;
            border-left: 1px solid #dcddde;
            border-right: 1px solid #dcddde;
            border-radius: 3px 3px 0 0;
        }

        #user-order-list tbody td {
            background-color: #fff;
            color: #000;
        }

        #user-order-list tbody tr.alternate td {
            background-color: #eaeaea;
        }

        #user-order-list tbody tr {
            border-left: 1px solid #dcddde;
            border-right: 1px solid #dcddde;
        }

        #user-order-list tbody tr:last-child {
            border-bottom: 1px solid #dcddde;
        }
    </style>
<?php }

add_action('wp_ajax_backend__sync_update_user_once', 'backend__sync_update_user_once_cb');
function backend__sync_update_user_once_cb()
{
    $id = intval($_POST['id']);
    $results = array(
        'result' => 0,
        'message' => 'Đồng bộ thất bại. Vui lòng thử lại trong ít phút',
    );

    $code_crm = get_user_meta($id, 'code_crm', true);
    $code_crm = ($code_crm != '') ? $code_crm : 'WEB' . str_pad($id, 9, '0', STR_PAD_LEFT);

    if ($code_crm) {
        $datas = sync_getCustomer($code_crm);

        if ($datas) {
            $data = sync_getCustomerInfo($datas);
            print_r($data);
            $result = sync_update_user($data, true);
            if ($result) {
                $results = array(
                    'result' => 1,
                    'message' => 'Đồng bộ thành công!',
                );
            }
        } else {
            $userdata = sync_buildCustomerObj($id);
            $result_add_customer = sync_addCustomerRegister($userdata);
            if ($result_add_customer) {
                $results = array(
                    'result' => 1,
                    'message' => 'Đồng bộ thành công!',
                );
            } else {
                $result['message'] = $result_add_customer;
            }
        }
    }

    echo json_encode($results);
    exit;
}

/* form_login_ajax */
add_action('wp_ajax_form_login_ajax', 'form_login_ajax');
add_action('wp_ajax_nopriv_form_login_ajax', 'form_login_ajax');
function form_login_ajax()
{
    $result = array(
        'redirect' => false,
        'message' => 'Có lỗi xảy ra, vui lòng thử lại sau.',
    );

    $user_info = wp_get_current_user();
    if ($user_info->ID > 0) {
        echo json_encode(array(
            'redirect' => home_url('/'),
            'message' => '',
        ));
        exit;
    }

    if (wp_verify_nonce($_POST['login_nonce_field'], 'login_nonce_action')) {
        if ($_POST['email']) {
            $loginInfo = array(
                'user_login' => esc_js($_POST['email']),
                'user_password' => sanitize_text_field($_POST['password']),
            );
            if (isset($_POST['lg_remember'])) {
                $loginInfo['remember'] = true;
            }

            $loginUser = wp_signon($loginInfo, false);

            if (is_wp_error($loginUser)) {
                $message = $loginUser->get_error_message();
                $message = str_replace('<a href="' . wp_lostpassword_url() . '" title="Password Lost and Found">Lost your password</a>?', '', $message);
                $result['message'] = 'Email hoặc mật khẩu không đúng. (' . $message . ')';
            } else {
                $result['message'] = 'Đăng nhập thành công, đang chuyển hướng...';
                $result['redirect'] = $_POST['redirect'];
                $result['email'] = $_POST['email'];

                realtime_update_crm_user_data($loginUser->ID);
            }
        } else {
            $result['message'] = 'Email không hợp lệ.';
        }
    } else {
        $result['message'] = "Bạn nhập thông tin quá lâu, đã hết phiên làm việc, vui lòng ấn F5 và thử lại.";
    }

    echo json_encode($result);
    exit();
}

function realtime_update_crm_user_data($user_id)
{
    $crm_id = 'WEB' . str_pad(strval($user_id), 9, '0', STR_PAD_LEFT);
    $code_crm = get_user_meta($user_id, 'code_crm', true);
    if ($code_crm != '')
        $crm_id = $code_crm;

    $userdata = sync_getCustomer($crm_id);
    $userdata = sync_getCustomerInfo($userdata);

    if ($userdata !== false) {
        update_user_meta($user_id, 'user_card', $userdata['card_id']);
        update_user_meta($user_id, 'user_card_id', $userdata['card_number']);
    }
}

/* User Service */
add_action('wp_ajax_user_service_ajax', 'user_service_ajax');
add_action('wp_ajax_nopriv_user_service_ajax', 'user_service_ajax');
function user_service_ajax()
{
    $isLogin = false;

    $do_ajax = sanitize_text_field($_GET['do_ajax']);

    $user_info = wp_get_current_user();
    if (isset($user_info->ID) && $user_info->ID) {
        $isLogin = true;
        $user_id = $user_info->ID;
        $current_province = get_user_meta($user_id, 'user_provinceid', true);
        $current_district = get_user_meta($user_id, 'user_districtid', true);
        $current_ward = get_user_meta($user_id, 'user_wardid', true);
    }

    if (isset($_GET['do_ajax']) && $do_ajax == 'get_provinces') {
        echo json_encode(array(
            'success' => 'true',
            'result' => aj_get_provinces(),
            'current' => isset($current_province) && $current_province ? '_' . $current_province : 0
        ));
    }

    if (isset($_GET['do_ajax']) && $do_ajax == 'get_districts' && isset($_GET['id']) && sanitize_text_field($_GET['id'])) {
        echo json_encode(array(
            'success' => 'true',
            'result' => aj_get_districts($_GET['id']),
            'current' => isset($current_district) && $current_district ? $current_district : 0
        ));
    }

    if (isset($_GET['do_ajax']) && $do_ajax == 'get_wards' && isset($_GET['id']) && sanitize_text_field($_GET['id'])) {
        echo json_encode(array(
            'success' => 'true',
            'result' => aj_get_wards($_GET['id']),
            'current' => isset($current_ward) && $current_ward ? $current_ward : 0
        ));
    }

    if (isset($_GET['do_ajax']) && $do_ajax == 'register_user_info') {
        $result = array(
            'redirect' => false,
            'message' => '',
            'result' => 0,
        );

        /*$user_info = wp_get_current_user();
        if($user_info->ID > 0){
            echo json_encode(array(
                'redirect' => home_url('/'),
                'message' => 'Bạn đã đăng nhập!',
            ));
            exit;
        }*/

        $data = file_get_contents("php://input");
        $dataArray = json_decode($data, true);

        if (isset($dataArray['gender']['id']))
            $gender = sanitize_text_field($dataArray['gender']['id']) == 'male' ? array('id' => 'male', 'name' => 'Nam') : array('id' => 'female', 'name' => 'Nữ');
        else
            $gender = array('id' => 'male', 'name' => 'Nam');

        $gender_crm = '1';
        if ($gender['id'] == 'female')
            $gender_crm = '2';

        $birthday = $dataArray['birthday']['year'] . '/' . str_pad($dataArray['birthday']['month'], 2, '0', STR_PAD_LEFT) . '/' . str_pad($dataArray['birthday']['day'], 2, '0', STR_PAD_LEFT);

        $fulladdress = sanitize_text_field($dataArray['address']);

        $post_info = array(
            'fullname' => sanitize_text_field($dataArray['fullname']),
            'email' => strtolower(sanitize_email($dataArray['email'])),
            'phone' => sanitize_text_field($dataArray['phone']),
            'address' => sanitize_text_field($dataArray['address']),
            'password' => sanitize_text_field($dataArray['password']),
            'gender' => $gender,
            'birthday' => $birthday,
        );

        $temp = $addr_temp = '';
        if (isset($dataArray['province'])) {
            $post_info['user_provinceid'] = isset($dataArray['province']['provinceid']) ? sanitize_text_field($dataArray['province']['provinceid']) : 0;
            $temp = aj_get_province($post_info['user_provinceid']);
            $addr_temp = $temp['name'];

            if (isset($dataArray['district'])) {
                $post_info['user_districtid'] = isset($dataArray['district']['districtid']) ? sanitize_text_field($dataArray['district']['districtid']) : 0;
                $temp = aj_get_district($post_info['user_districtid']);
                $addr_temp = $temp['name'] . ', ' . $addr_temp;

                if (isset($dataArray['ward'])) {
                    $post_info['user_wardid'] = isset($dataArray['ward']['wardid']) ? sanitize_text_field($dataArray['ward']['wardid']) : 0;
                    $temp = aj_get_ward($post_info['user_wardid']);
                    $addr_temp = $temp['name'] . ', ' . $addr_temp;
                }
            }
        }

        if ($addr_temp) {
            $fulladdress .= ', ' . $addr_temp;
            $post_info['fulladdress'] = $fulladdress;
        }

        if (wp_verify_nonce($dataArray['nonce'], 'register_nonce_action')) {
            foreach ($post_info as $pkey => $pinfo) {
                if (!$pinfo) {
                    if ($pkey == 'email') {
                        $result['message'] = 'Email chưa nhập hoặc không hợp lệ.';
                    }
                    break;
                }
            }

            $securimage = new Securimage();

            if (!isset($dataArray['password'])) {
                $result['message'] = 'Chưa nhập mật khẩu';
            } elseif (!isset($dataArray['repassword'])) {
                $result['message'] = 'Chưa nhập xác nhận mật khẩu';
            } elseif ($post_info['password'] != sanitize_text_field($dataArray['repassword'])) {
                $result['message'] = 'Mật khẩu xác nhận không trùng khớp.';
            } elseif ($securimage->check(strtoupper(trim($dataArray['captcha']))) == false) {
                $result['message'] = 'Mã xác nhận chưa chính xác.';
            }

            if (!$result['message']) {
                // Realtime push user data
                global $wpdb;
                $query = 'SELECT AUTO_INCREMENT as next_id FROM information_schema.TABLES WHERE TABLE_SCHEMA = "' . DB_NAME . '" AND TABLE_NAME = "' . $wpdb->users . '"';
                $next_id = $wpdb->get_var($query);

                $userdata = array(
                    'CustID' => 'WEB' . str_pad($next_id, 9, '0', STR_PAD_LEFT),
                    'CustName' => sanitize_text_field($post_info['fullname']),
                    'Email' => $post_info['email'],
                    'RepEmail' => $post_info['email'],
                    'CustFullName' => sanitize_text_field($post_info['fullname']),
                    'Address' => isset($post_info['fulladdress']) ? $post_info['fulladdress'] : $post_info['address'],
                    'PhoneNo1' => sanitize_text_field($post_info['phone']),
                    'CellPhone' => sanitize_text_field($post_info['phone']),
                    'Birthday' => str_replace(' ','T',date("Y-m-d H:i:s", strtotime($post_info['birthday']))),
                    'EstablishDay' => str_replace(' ','T',date("Y-m-d H:i:s", time())),
                    'CheckEmail' => true,
                    'CheckSMS' => true,
                    'Sex' => $gender_crm,
                );
                $error_text= '';
                try {
                    $result_add_customer = sync_addCustomerRegister($userdata);
                    $error_text = 'API Return';

                } catch (Exception $ex) {
                    $result_add_customer = true;
                    $error_text = $ex->getTraceAsString();
                }
                $action_next = '';
                if ($result_add_customer === true) {
                    $action_next = 'add_new';
                } elseif ($result_add_customer === false) {
                    $action_next = 'error';
                    $error_message = 'Tiến trình đăng ký xảy ra lỗi. Vui lòng thử lại trong ít phút nữa.';
                } else {
                    $id_pos = strpos($result_add_customer, '|');
                    // Available CRM user
                    if ($id_pos !== false && $id_pos != strlen($result_add_customer) - 1) {
                        $action_next = 'get_crm';
                    } // Available CRM user, but at list
                    elseif ($id_pos !== false && $id_pos == strlen($result_add_customer) - 1) {
                        $action_next = 'add_new';
                    } else {
                        $action_next = 'error';
                        $error_message = $result_add_customer;
                    }
                }

                // Add Customer at CRM success
                if ($action_next === 'add_new') {
                    $new_user_id = wp_insert_user(array(
                        'user_login' => $post_info['phone'],
                        'user_email' => $post_info['email'],
                        'user_pass' => $post_info['password'],
                        'role' => 'subscriber'
                    ));

                    if (is_wp_error($new_user_id)) {
                        $result['message'] = $new_user_id->get_error_message();

                        if ($result['message'] == 'Sorry, that username already exists!') {
                            $result['message'] = 'Tên người dùng đã có người sử dụng.';
                        } elseif ($result['message'] == 'Sorry, that email address is already used!') {
                            $result['message'] = 'Email đã có người sử dụng.';
                        }
                    } else {
                        wp_update_user(array(
                            'ID' => $new_user_id,
                            'display_name' => $post_info['fullname'],
                        ));

                        update_user_meta($new_user_id, 'user_cellphone', $post_info['phone']);
                        update_user_meta($new_user_id, 'user_phone', $post_info['phone']);
                        update_user_meta($new_user_id, 'user_phone2', $post_info['phone']);
                        update_user_meta($new_user_id, 'user_address', $post_info['address']);
                        update_user_meta($new_user_id, 'user_provinceid', $post_info['user_provinceid']);
                        update_user_meta($new_user_id, 'user_districtid', $post_info['user_districtid']);
                        update_user_meta($new_user_id, 'user_wardid', $post_info['user_wardid']);

                        update_user_meta($new_user_id, 'user_birthday', $dataArray['birthday']);
                        update_user_meta($new_user_id, 'user_birthday_txt', $birthday);

                        $gender = $post_info['gender']['id'] == 'male' ? array('id' => 'male', 'name' => 'Nam') : array('id' => 'female', 'name' => 'Nữ');
                        update_user_meta($new_user_id, 'user_gender', $gender);

                        // update_user_meta( $new_user_id, 'actived', 'no' ); // active or not

                        wp_set_current_user($new_user_id);
                        wp_set_auth_cookie($new_user_id);

                        mail__announce_new_user_register(get_userdata($new_user_id));

                        $result['message'] = 'Chúc mừng bạn đã đăng ký thành công.';
                        $result['redirect'] = $dataArray['redirect'];
                        $result['result'] = 1;
                    }
                } // Add Customer at CRM failed
                elseif ($action_next === 'error') {
                    $result['message'] = $error_message;
                } // Add Customer at CRM has error
                elseif ($action_next == 'get_crm') {
                    $result_add_customer_arr = explode('|', $result_add_customer); // get crm id at the last of string, sereprate by |
                    $crm_id = $result_add_customer_arr[count($result_add_customer_arr) - 1];


                    // Get data of above user and update user on system
                    $crm_user = sync_getCustomer($crm_id);
                    $crm_user_converted = sync_getCustomerInfo($crm_user);

                    // Update smthg. data follow the register form
                    $crm_user_converted['email'] = $post_info['email'];
                    $crm_user_converted['repemail'] = $post_info['email'];
                    // $crm_user_converted['fullname'] = $post_info['fullname']; // No update fullname, update this from CRM
                    // $crm_user_converted['birthday'] = date( "Y-m-d\TH:i:s", strtotime( $post_info['birthday'] ) );
                    $crm_user_converted['gender'] = $gender_crm;

                    $new_crm_user_id = sync_update_user($crm_user_converted);

                    if (is_int($new_crm_user_id) && $new_crm_user_id > 0) {
                        $args_update_later = array(
                            'ID' => $new_crm_user_id,
                            'user_pass' => $post_info['password'],
                            'display_name' => $crm_user_converted['fullname'],
                        );

                        $crm_userdata = get_userdata($new_crm_user_id);

                        if (strpos($crm_userdata->user_email, 'ktccrm') !== false) {
                            $args_update_later['user_email'] = $post_info['email'];
                            $args_update_later['display_name'] = $post_info['fullname'];
                        }

                        wp_update_user($args_update_later);

                        update_user_meta($new_crm_user_id, 'user_cellphone', $post_info['phone']);
                        update_user_meta($new_crm_user_id, 'user_phone', $post_info['phone']);
                        update_user_meta($new_crm_user_id, 'user_phone2', $post_info['phone']);

                        // Update address if have
                        update_user_meta($new_crm_user_id, 'user_address', $post_info['address']);
                        update_user_meta($new_crm_user_id, 'user_provinceid', $post_info['user_provinceid']);
                        update_user_meta($new_crm_user_id, 'user_districtid', $post_info['user_districtid']);
                        update_user_meta($new_crm_user_id, 'user_wardid', $post_info['user_wardid']);

                        // update_user_meta($new_crm_user_id, 'user_birthday', $dataArray['birthday']);
                        // update_user_meta($new_crm_user_id, 'user_birthday_txt', $birthday );

                        /*$gender = $post_info['gender']['id'] == 'male' ? array('id' => 'male', 'name' => 'Nam') : array('id' => 'female', 'name' => 'Nữ');
                        update_user_meta($new_crm_user_id, 'user_gender', $gender);*/

                        // Auto login
                        wp_set_current_user($new_crm_user_id);
                        wp_set_auth_cookie($new_crm_user_id);

                        mail__announce_new_user_register(get_userdata($new_crm_user_id));

                        $result['message'] = '
CẢM ƠN BẠN ĐÃ ĐẾN VỚI THỜI TRANG KHATOCO.
<br>Thông tin tài khoản của bạn đã được cập nhật tự động từ hệ thống Chăm sóc Khách Hàng của Khatoco.
<br>Bạn sử dụng Số Điện Thoại và Mật Khẩu Vừa Nhập để đăng nhập!
<br>Chi tiết xem tại <a href="' . get_page_link(2503) . '">Trang thông tin cá nhân</a>
';
                        $result['redirect'] = $dataArray['redirect'];
                        $result['result'] = 1;
                    } // Available web user
                    elseif ($new_crm_user_id === 0 || !empty($new_crm_user_id)) {
                        $result['message'] = 'Số điện thoại hoặc Email đã được sử dụng.';
                    } else {
                        //$result['message'] = 'Đã có lỗi xảy ra, vui lòng ấn F5 và thử lại.';
						//temp cheating
						$result['message'] = 'Số điện thoại hoặc Email đã được sử dụng.';
                    }
                } else {
                    //$result['message'] = 'Đã có lỗi xảy ra, vui lòng ấn F5 và thử lại.';
					//temp cheating
					$result['message'] = 'Số điện thoại hoặc Email đã được sử dụng.';
                }
            }
        } else {
            $result['message'] = "Bạn nhập thông tin quá lâu, đã hết phiên làm việc, vui lòng ấn F5 và thử lại.";
        }
//        G24mY6J4

        echo json_encode($result);
        exit();
    }

    /*if($isLogin == false){
        echo json_encode(array(
            'success' => 'false',
            'message' => 'Có lỗi xảy ra, vui lòng thử lại sau.'
        ));
        exit;
    }*/

    if (isset($_GET['do_ajax']) && $do_ajax == 'get_user_info') {
        $userInfo = $user_info->data;

        $card = get_user_meta($user_id, 'user_card', true);
        $card_id = get_user_meta($user_id, 'user_card_id', true);
        if ($card == '') {
            $card = '0';
            update_user_meta($user_id, 'user_card', '0');
        }

        $birthday = get_user_meta($user_id, 'user_birthday', true);
        if (!is_array($birthday))
            if ($birthday == '') {
                $birthday = array(
                    'year' => 1990,
                    'month' => 1,
                    'day' => 1
                );
            } else {
                list($__d, $__m, $__y) = explode('/', $birthday);
                $birthday = array(
                    'year' => $__y,
                    'month' => $__m,
                    'day' => $__d,
                );
            }

        $gender = get_user_meta($user_id, 'user_gender', true);
        $gender = (isset($gender['id']) && $gender['id'] == 'male') ? array('id' => 'male', 'name' => 'Nam') : array('id' => 'female', 'name' => 'Nữ');

        $fullname = $userInfo->display_name;
        $name = get_user_meta($user_id, 'name', true);
        if ($name) $fullname = $name;
        $name = get_user_meta($user_id, 'fullname', true);
        if ($name) $fullname = $name;

        $result = array(
            'username' => $userInfo->user_login,
            'fullname' => $fullname,
            'email' => $userInfo->user_email,
            'birthday' => $birthday,
            'gender' => $gender,
            'address' => get_user_meta($user_id, 'user_address', true),
            'phone' => get_user_meta($user_id, 'user_cellphone', true) ? get_user_meta($user_id, 'user_cellphone', true) : get_user_meta($user_id, 'user_phone', true),
            'card' => $card,
            'card_id' => $card_id,
        );

        echo json_encode(array(
            'success' => 'true',
            'result' => $result
        ));
    }
    // has webservice
    if (isset($_GET['do_ajax']) && $do_ajax == 'save_user_info') {
        $data = file_get_contents("php://input");
        $dataArray = json_decode($data, true);

        if (!wp_verify_nonce($dataArray['nonce'], 'profile_nonce_action')) {
            echo json_encode(array(
                'success' => 'false',
                'message' => 'Đã hết phiên làm việc, vui lòng F5 và thử lại.'
            ));
            exit;
        }

        $check_card = 0;
        if (isset($user_id) && !empty($user_id)) {
            $wpdb = new wpdb(DB_USER, DB_PASSWORD, DB_NAME, DB_HOST);
            $querystr = "  SELECT khatoco_usermeta.*
                            FROM khatoco_usermeta
                            WHERE khatoco_usermeta.meta_key = 'code_crm' &&  khatoco_usermeta.meta_value != '' &&  khatoco_usermeta.user_id = " . $user_id;

            $getcard = $wpdb->get_results($querystr, OBJECT);
            if ($getcard) {
                $check_card = 1;
            }
        }

        if ($check_card == 0) {
            update_user_meta($user_id, 'name', sanitize_text_field($dataArray['fullname']));
            update_user_meta($user_id, 'fullname', sanitize_text_field($dataArray['fullname']));
            update_user_meta($user_id, 'user_address', sanitize_text_field($dataArray['address']));
            update_user_meta($user_id, 'user_phone', sanitize_text_field($dataArray['phone']));
            update_user_meta($user_id, 'user_birthday', $dataArray['birthday']);
            $birthday_txt = strval($dataArray['birthday']['year']) . '/' . str_pad(strval($dataArray['birthday']['month']), 2, '0', STR_PAD_LEFT) . '/' . str_pad(strval($dataArray['birthday']['day']), 2, '0', STR_PAD_LEFT);
            update_user_meta($user_id, 'user_birthday_txt', $birthday_txt);

            wp_update_user(array(
                'ID' => $user_id,
                'display_name' => sanitize_text_field($dataArray['fullname'])
            ));

            if (isset($dataArray['gender']['id']))
                $gender = $dataArray['gender']['id'] == 'male' ? array('id' => 'male', 'name' => 'Nam') : array('id' => 'female', 'name' => 'Nữ');
            else
                $gender = array('id' => 'male', 'name' => 'Nam');
            update_user_meta($user_id, 'user_gender', $gender);
        }


        if (isset($dataArray['province'])) {
            $post_info['user_provinceid'] = isset($dataArray['province']['provinceid']) ? sanitize_text_field($dataArray['province']['provinceid']) : 0;
            update_user_meta($user_id, 'user_provinceid', $post_info['user_provinceid']);

            if (isset($dataArray['district'])) {
                $post_info['user_districtid'] = isset($dataArray['district']['districtid']) ? sanitize_text_field($dataArray['district']['districtid']) : 0;
                update_user_meta($user_id, 'user_districtid', $post_info['user_districtid']);

                if (isset($dataArray['ward'])) {
                    $post_info['user_wardid'] = isset($dataArray['ward']['wardid']) ? sanitize_text_field($dataArray['ward']['wardid']) : 0;
                    update_user_meta($user_id, 'user_wardid', $post_info['user_wardid']);
                }
            }
        }


        $code_crm = get_user_meta($user_id, 'code_crm', true);
        $code_crm = ($code_crm != '') ? $code_crm : 'WEB' . str_pad(get_current_user_id(), 9, '0', STR_PAD_LEFT);
        $card = get_user_meta($user_id, 'user_card', true);
        // Only sync user on web and no card
        if (strpos($code_crm, 'WEB') !== false && ($card == '' || $card == '0')) {
            sync_updateCustomer($user_id);
        }

        if (isset($dataArray['password'])) {
            $user_r = wp_update_user(array(
                'ID' => $user_id,
                'user_pass' => sanitize_text_field($dataArray['password'])
            ));

            if (!is_wp_error($user_r)) {
                mail__announce_password_change($user_info, sanitize_text_field($dataArray['password']));
            }
        }

        /*if( isset( $dataArray['request'] ) ){
        	// Send mail thong bao cho nhan vien ve viec thay doi the hoac dang ky the
        	if( isset( $dataArray['request']['register'] ) ){
        		// Dang ky moi
        	}
			if( isset( $dataArray['request']['change'] ) ){
        		// Thay doi thong tin
        	}
			if( isset( $dataArray['request']['remove'] ) ){
        		// Huy the
        	}
        }*/

        echo json_encode(array(
            'success' => 'true',
            'message' => 'Thông tin đã được cập nhật'
        ));

    }

    if (isset($_GET['do_ajax']) && $do_ajax == 'register_member_card') {
        $data = json_decode(stripslashes($_GET['data']), true);
    }
    if (isset($_GET['do_ajax']) && $do_ajax == 'get_user_member_card') {

        $data = file_get_contents("php://input");
        $dataArray = json_decode($data, true);
        $_securimage = new Securimage();
        if ($_securimage->check(strtoupper(trim($dataArray['captcha']))) == false) {
            echo json_encode(array(
                'success' => false,
                'message' => "Mã xác nhận chưa chính xác.",
                'item' => null
            ));
            exit();
        }

        $ret = sync_getCustomer($dataArray['cardcode']);
        if ($ret != null && $ret['CellPhone'] == $dataArray['cellphone'])
            echo json_encode(array(
                'success' => true,
                'item' => $ret
            ));
        else
            echo json_encode(array(
                'success' => false,
                'message' => 'Thông tin thẻ thành viên không tồn tại. Vui lòng kiểm tra và thử lại.',
                'item' => null
            ));
    }

    exit;
}

// Check vip code invalid or not
function check_member_identified($member_identified)
{
    if (!$member_identified) return false;
    // Get all vip codes
    $users = get_users(array(
        'meta_query' => array(
            'relation' => 'OR',
            array(
                'key' => 'user_card',
                'value' => '0',
                'compare' => '!=',
            ),
            array(
                'key' => 'user_card',
                'value' => '',
                'compare' => '!=',
            ),
        ),
    ));

    /*$user_id = 2;
    $vip_level = '3';
    return array('id' => $user_id, 'lv' => $vip_level);*/

    foreach ($users as $ukey => $u) {
        $card = get_user_meta($u->ID, 'user_card', true);
        $card_id = get_user_meta($u->ID, 'user_card_id', true);
        if ($card_id != '' && $card_id == $member_identified)
            return array('id' => $u->ID, 'lv' => $card);
    }

    /*if(in_array($member_identified, $all_vips)){
        $user_id = 2;
        $vip_level = '3';
        return array('id' => $user_id, 'lv' => $vip_level); // ID and vip level of member
    }*/
    return false;
}

// Detect user password change
add_action('profile_update', 'user_profile_update');
function user_profile_update($user_id)
{
    if (!isset($_POST['pass1']) || '' == $_POST['pass1']) {
        return;
    }

    if ($_POST['pass1'] == $_POST['pass2']) {
        mail__announce_password_change($user_id, $_POST['pass1']);
    }
}

add_action('wp_ajax_register_user_over_fb', 'register_user_over_fb');
add_action('wp_ajax_nopriv_register_user_over_fb', 'register_user_over_fb');
function register_user_over_fb()
{
    $returns = array();
    $fid = $_POST['id'];
    $email = sanitize_email($_POST['email']);
    $name = sanitize_text_field($_POST['name']);
    $gender = sanitize_text_field($_POST['gender']);

    $gender = $gender == 'male' ? array('id' => 'male', 'name' => 'Nam') : array('id' => 'female', 'name' => 'Nữ');

    $email_check = email_exists($email);
    if ($email_check) {
        wp_set_current_user($email_check);
        wp_set_auth_cookie($email_check);

        $returns['result'] = 1;
        /*$hash = strtoupper( substr( sha1( $email_check . 'f' . $fid ), 7, 14 ) );
        update_post_meta( $email_check, 'facebook_account_verification',  );
        wp_mail( $email, '[Khatoco] Xác minh tài khoản facebook', '
<p>Chào bạn,</p>
<p>Chúng tôi gửi email này để xác minh khi có người cố gắng truy cập tài khoản của bạn thông qua Facebook. Nếu đó không phải bạn, vui lòng bỏ qua email này!</p>
<p>Nếu bạn là chủ nhân của yêu cầu trên, hãy sử dụng mã sau để xác minh tài khoản: <strong>'. $hash .'</strong></p>
            ', $headers );

        $returns['result'] = 2;
        $returns['message'] = 'Người dùng đã tồn tại trên hệ thống. Để xác minh người này là bạn, vui lòng kiểm tra lại hòm thư của email đăng ký tài khoản facebook của bạn.';
        $returns['redirect'] = add_query_arg( array( 'action' => 'facebook_account_verification' ), get_page_link(2505) );*/
    } else {
        $new_user_id = wp_insert_user(array(
            'user_login' => $email,
            'user_email' => $email,
            'user_pass' => wp_generate_password(12, true),
            'role' => 'subscriber',
            'display_name' => $name,
        ));

        if (!is_wp_error($new_user_id)) {
            update_user_meta($new_user_id, 'user_gender', $gender);
            update_user_meta($new_user_id, 'user_way', 'f' . $fid);
            update_user_meta($new_user_id, 'actived', 'no');

            wp_set_current_user($new_user_id);
            wp_set_auth_cookie($new_user_id);

            $result = sync_addCustomer($new_user_id);

            if ($result !== true) {
                $returns['result'] = 1;
                $returns['message'] = 'Đăng ký thành công, nhưng chưa cập nhật lên hệ thống...';
                $returns['redirect'] = add_query_arg(array('action' => 'continue_update_userdata'), get_page_link(2503));
            } else {
                $returns['result'] = 1;
                $returns['message'] = 'Đăng ký thành công, đang chuyển hướng...';
                $returns['redirect'] = add_query_arg(array('action' => 'continue_update_userdata'), get_page_link(2503));
            }

            mail__announce_new_user_register(get_userdata($new_user_id));
        } else {
            $returns['result'] = 0;
            $returns['message'] = 'Có lỗi xảy ra, vui lòng thử lại';
        }
    }

    echo json_encode($returns);
    exit;
}

/* FRONTEND FUNCS */
add_action('wp_ajax_frontend__user_subscribe', 'frontend__user_subscribe');
add_action('wp_ajax_nopriv_frontend__user_subscribe', 'frontend__user_subscribe');
function frontend__user_subscribe()
{
    $messages = array(
        'result' => 0,
        'message' => '',
    );
    $email = sanitize_email($_POST['email']);
//    $phone = sanitize_text_field($_POST['phone']);

    $email_check = email_exists($email);
    if ($email_check) {
        $subscribes = get_user_meta($email_check, 'subscribes', true);
        if ($subscribes == '') $subscribes = array();
        if (!isset($subscribes['email'])) {
            $subscribes['email'] = array('news');
        } else {
            $subscribes['email'][] = 'news';
        }

        update_user_meta($email_check, 'subscribes', $subscribes);
        $messages = array(
            'result' => 1,
            'message' => 'Bạn đã đăng ký nhận bản tin thành công!',
        );
    } else {
        $new_user = wp_insert_user(array(
            'user_login' => $email,
            'user_email' => $email,
            'user_pass' => wp_generate_password(10, false),
            'role' => 'subscriber',
        ));

        if (!is_wp_error($new_user)) {
            // update_user_meta( $new_user, 'actived', 0 );
            update_user_meta($new_user, 'subscribes', array('email' => array('news')));
            update_user_meta($new_user, 'actived', 0);

            $obj = new stdClass();
            $obj->Email = $email;
//             $obj->CellPhone = $phone;
//             $obj->PhoneNo1 = $phone;
//             $obj->PhoneNo2 = $phone;
            $args = array(
                'Key' => '',
                'Customer' => $obj,
            );
            $res = client_call_service('AddCustomer', $args, 'POST');

            if ($res === true) {
                $messages = array(
                    'result' => 1,
                    'message' => 'Bạn đã đăng ký nhận bản tin thành công!',
                );
            } else {
                $messages = array(
                    'result' => 0,
                    'message' => 'Đã có lỗi xảy ra, vui lòng thử lại!',
                );
            }
        } else {
            $messages = array(
                'result' => 0,
                'message' => 'Đã có lỗi xảy ra, vui lòng thử lại!!',
            );
        }
    }

    echo json_encode($messages);
    exit;
}
