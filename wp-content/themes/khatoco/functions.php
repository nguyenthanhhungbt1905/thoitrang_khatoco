<?php 
// In syncing process, system will pause all process at backend
if( is_admin() && isset( $_COOKIE['in_sync_process'] ) && $_COOKIE['in_sync_process']){
    wp_die( 'Hệ thống đang trong quá trình đồng bộ dữ liệu. Vui lòng quay lại sau!', 'Hệ thống đang đồng bộ...');
    exit;
}

/*// Detect old or new customers
$khatoco_logined = current_time('mysql', 0);
if(!isset($_COOKIE['khatoco_logined'])) {
    setcookie('khatoco_logined', $khatoco_logined, time() + (86400*30));
    // echo 'Chào mừng bạn đến với Thời trang Khatoco!';
}
else {
    if(time() - strtotime($_COOKIE['khatoco_logined']) > 86400) {
        //echo 'Chào mừng bạn quay trở lại';
    }
    else {

    }
}*/

// Update user every hours
/*$user_time_requested = current_time( 'timestamp', 0 );
if( is_user_logged_in() ){
    if( !isset( $_COOKIE['user_time_requested'] ) || is_null( $_COOKIE['user_time_requested'] ) ) {
        $_COOKIE['user_time_requested'] = $user_time_requested;
        setcookie( 'user_time_requested', $user_time_requested, $user_time_requested + 60*60, '/beta', $_SERVER['SERVER_NAME'] );
    }
    elseif( $user_time_requested - $_COOKIE['user_time_requested'] >= 60*60 ) {
        setcookie( 'user_time_requested', $user_time_requested, $user_time_requested + 60*60, '/beta', $_SERVER['SERVER_NAME'] );
        realtime_update_crm_user_data( get_current_user_id() );
    }
}*/

// count a post's word
function countWords() {
	global $post;
	$content = $post->post_content;
	$decode_content = html_entity_decode($content);
	$strip_shortcode = strip_shortcodes($decode_content);
	$strip_wp_tags = wp_strip_all_tags($strip_shortcode, true);
	$countwords = str_word_count($strip_wp_tags);
	return $countwords;
}

// Message center
// add_action('wp_ajax_message_center_announce', 'message_center_announce');
// add_action('wp_ajax_nopriv_message_center_announce', 'message_center_announce');
function message_center_announce() {
    $promotion_announce = check_campaign_is_running();
    $coupon_announce = check_coupon_available_with_user();

    $result = 'false';
    $content = '';
    if($promotion_announce){
        $content .= '<p>Chương trình khuyến mại <strong>'. $promotion_announce['name'] .'</strong> hiện đang chạy!</p>';
        $result = 'true';
    }

    if($coupon_announce){
        $content .= '<p>Hiện tại bạn có thể sử dụng coupon:</p>';
        $content .= '<ul>';
        foreach ($coupon_announce as $ckey => $c) {
            $content .= '<li>'. $c['code'] .': giảm giá '. $c['kind'][1] . (($c['kind'][0] == '0') ? '%':'VND') .'</li>';
        }
        $content .= '</ul>';
        
        $result = 'true';
    }

    echo json_encode(array(
        'result' => $result,
        'html' => $content
    ));
    exit;
}

//Get name of card by $id . Example : 1 => Thẻ bạc , 2=> thẻ vàng, 3 => thẻ kim cương
function get_card_name ($id_card){
    switch ($id_card) {
        case 1:
            return 'thẻ bạc';
            break;
        case 2:
            return 'thẻ vàng';
            break;
         case 3:
            return 'thẻ kim cương';
            break;
        default: 
            return 'không có thẻ';
            break;
    }
}


$user_id = get_current_user_id();
if($user_id > 0){
    $user_activities = get_user_meta( $user_id, '_user_activities', true );
    if($user_activities == ''){
        $user_activities = array();
    }

    if(count($user_activities) > 100){
        array_pop($user_activities);
    }


    $time = current_time( 'mysql', 0 );
    // More than 5 minutes
    if(!isset($user_activities[0]) || (isset($user_activities[0]) && (strtotime($time) - strtotime($user_activities[0])) > 300)){
        array_unshift($user_activities, $time);
        update_user_meta( $user_id, '_user_activities', $user_activities );
    }
}

remove_action( 'shutdown', 'wp_ob_end_flush_all', 1 );

require_once('includes/breadcrumb.php');
require_once('includes/initialize.php');

//Captcha
require_once(ABSPATH.'/wp-includes/captcha/securimage.php');
$securimage = new Securimage();
if(is_admin_author()){
    include_once(get_template_directory() . '/includes/campaign.php');
    include_once(get_template_directory() . '/post-types/coupon.php');
    include_once(get_template_directory() . '/post-types/voucher-sms.php');
    include_once(get_template_directory() . '/post-types/uniform.php');
    include_once(get_template_directory() . '/post-types/contact.php');
}
include_once(get_template_directory() . '/includes/sync-template.php');
include_once(get_template_directory() . '/includes/mail-config.php');

// Custom posttypes
include_once(get_template_directory() . '/post-types/post.php');
include_once(get_template_directory() . '/post-types/user.php');
include_once(get_template_directory() . '/post-types/other.php');

/*var_dump(is_admin_author_contributor());
var_dump(is_admin_editor());*/

if(is_admin_author_contributor() ) {
    include_once(get_template_directory() . '/post-types/order.php');

}
// print_r(is_admin_editor());exit();
// if (is_admin()) {
//      include_once(get_template_directory() . '/post-types/uniform.php');
// }
if(is_admin_editor()) {
    include_once(get_template_directory() . '/post-types/news.php');
    include_once(get_template_directory() . '/post-types/bo-anh.php');
    include_once(get_template_directory() . '/post-types/job-apply.php');
    include_once(get_template_directory() . '/post-types/video.php');
    include_once(get_template_directory() . '/post-types/style-guide.php');
    include_once(get_template_directory() . '/post-types/showroom.php');
    
    
    // include_once(get_template_directory() . '/post-types/coupon.php');
    // include_once(get_template_directory() . '/includes/campaign.php');
}


/** include cards **/
if(!is_subscriber()){
    include_once( get_template_directory().'/post-types/cards.php' );
}


include_once(get_template_directory() . '/post-types/banner.php');

add_filter( 'nonce_life', function () { return 6 * HOUR_IN_SECONDS; } );

add_filter('wp_title', 'theme_name_wp_title', 10, 2);
function theme_name_wp_title( $title, $sep ) {
    if ( is_feed() ) {
        return $title;
    }
    
    global $page, $paged;

    // Add the blog name
    $title .= " $sep " . get_bloginfo( 'name', 'display' );

    // Add the blog description for the home/front page.
    $site_description = get_bloginfo( 'description', 'display' );
    if ( $site_description && ( is_home() || is_front_page() ) ) {
        $title .= " $sep $site_description";
    }

    // Add a page number if necessary:
    if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() ) {
        $title .= " $sep " . sprintf( __( 'Trang %s', '_s' ), max( $paged, $page ) );
    }

    return $title;
}

// Favicons
add_action('admin_head', 'add_favicon');
add_action('wp_head', 'add_favicon');
function add_favicon(){
    echo '<link type="image/x-icon" rel="shortcut icon" href="'. get_template_directory_uri() .'/assets/img/favicon.png" />';
}

add_filter('login_headerurl', 'loginpage_custom_link');
function loginpage_custom_link() {
    return home_url('/');
}

add_filter('login_headertitle', 'change_title_on_logo');
function change_title_on_logo() {
    return get_option('blogname');
}

add_action('login_enqueue_scripts', 'change_wp_admin_logo');
function change_wp_admin_logo() { ?>
<style type="text/css">
body.login div#login{padding: 3% 0px 0px}
body.login div#login h1 a{background: url("<?php echo get_template_directory_uri() .'/assets/img/logo.png'; ?>") no-repeat; width: 162px; height: 108px; padding-bottom: 30px}
</style>
<?php }

// Forbidden subscribers access to wp-admin
add_action( 'init', 'banned_user_no_permission' );
function banned_user_no_permission(){
    if( is_admin() && !current_user_can( 'edit_posts' ) && !defined( 'DOING_AJAX' ) && !DOING_AJAX ){
        wp_redirect( home_url( '/' ) );
        exit;
    }
}

// TAXONOMY META
add_action('admin_init', 'lh_register_taxonomy_meta_boxes'); // temp_hidden
function lh_register_taxonomy_meta_boxes() {
    if (!class_exists('RW_Taxonomy_Meta'))
        return;

    $meta_sections = array();
    $meta_sections[] = array(
        'title' => 'More Description',
        'taxonomies' => array('category'),
        'id' => 'product-cat-description',
        'fields' => array(
            array(
                'name' => 'Background Image',
                'id' => 'product-cat-img',
                'type' => 'image'
            )
        )
    );

    $meta_sections[] = array(
        'title' => 'More Description',
        'taxonomies' => array('style-guide-category'),
        'id' => 'cat-description',
        'fields' => array(
            array(
                'name' => 'Background Image',
                'id' => 'cat-bg-img',
                'type' => 'image'
            ),
            array(
                'name' => 'Banner Image',
                'id' => 'cat-banner-img',
                'type' => 'image'
            ),
        )
    );

    $meta_sections[] = array(
        'title' => 'More Description',
        'taxonomies' => array('bo-suu-tap'),
        'id' => 'cat-description',
        'fields' => array(
            array(
                'name' => 'Index Background Image',
                'id' => 'cat-index-bg-img',
                'type' => 'image'
            ),
            array(
                'name' => 'Collection Background Image',
                'id' => 'collection-bg-img',
                'type' => 'image'
            )
        )
    );

    foreach ($meta_sections as $meta_section) {
        new RW_Taxonomy_Meta($meta_section);
    }
}

/* Custom dashboard */
add_action('wp_dashboard_setup', 'add_custome_orders_announce');
function add_custome_orders_announce() {
    if(current_user_can('manage_options')){
        add_meta_box('orders_report', 'Thống kê số đơn hàng phát sinh và số đơn hàng hoàn thành', 'custom_orders_list_template_callback', 'dashboard', 'normal', 'core');
        add_meta_box('products_report', 'Sản phẩm bán chạy nhất', 'custom_products_list_template_callback', 'dashboard', 'normal', 'core');
        // add_meta_box('orders_report', 'Users Report', 'custom_users_list_template_callback', 'dashboard', 'normal', 'core');
    }

    // Remove the old dashboard widgets
    remove_meta_box('dashboard_right_now', 'dashboard', 'normal');
    remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');
    remove_meta_box('dashboard_incoming_links', 'dashboard', 'normal');
    remove_meta_box('dashboard_plugins', 'dashboard', 'normal');
    remove_meta_box('dashboard_quick_press', 'dashboard', 'side');
    remove_meta_box('dashboard_recent_drafts', 'dashboard', 'side');
    remove_meta_box('dashboard_primary', 'dashboard', 'side');
    remove_meta_box('dashboard_secondary', 'dashboard', 'side');
}

function custom_orders_list_template_callback() {
    global $wpdb, $wp_locale;
    $post_type = 'orders';

    $months = $wpdb->get_results( $wpdb->prepare( "
        SELECT DISTINCT YEAR( post_date ) AS year, MONTH( post_date ) AS month, DAY( post_date ) AS date
        FROM $wpdb->posts
        WHERE post_type = %s
        ORDER BY post_date DESC
        LIMIT 20
    ", $post_type ) );

    $months = apply_filters( 'months_dropdown_results', $months, $post_type );
    $months = array_reverse($months);

    $all_orders_stt = array();
    foreach ($months as $arc_row) {
        if ( 0 == $arc_row->year )
            continue;
        
        $date = (int) $arc_row->date;
        $month = (int) $arc_row->month;
        $year = (int) $arc_row->year;

        $args = array(
            'post_type' => 'orders',
            'posts_per_page' => -1,
            'date_query' => array(
                array(
                    'month' => $month,
                    'year' => $year,
                    'day' => $date
                )
            )
        );
        $orders_list = get_posts( $args );
        $orders_created = 0;
        $orders_completed = 0;
        $money_all = 0;
        $money_real = 0;

        foreach ($orders_list as $okey => $o) {
            $order_info = get_post_meta( $o->ID, 'order_info', true );
            if( !$order_info )
                continue;

            $orders_created++;

            $order_info['total'] = get_post_meta( $o->ID, 'order_total', true );
            $money_all += intval( $order_info['total'] );
            $order_payment_status = get_post_meta( $o->ID, 'order_payment_status', true );
            $order_payment_code = get_post_meta( $o->ID, 'order_payment_code', true );
            if( $order_payment_status == true && $order_payment_code ){
                $orders_completed++;
                $money_real += intval( $order_info['total'] );
            }
        }

        $temp = array();
        $temp['created'] = $orders_created;
        $temp['completed'] = $orders_completed;
        $temp['money_all'] = $money_all;
        $temp['money_real'] = $money_real;

        $all_orders_stt[] = $temp;

        // if(get_current_user_id() == 2) print_r($all_orders_stt);
    } ?>
    <div style="width: 100%;">
        <p><a href="<?php echo admin_url( 'edit.php?post_type=orders&page=orders_reports' ); ?>">Xuất báo cáo cụ thể</a></p>
    </div>
    <div style="width: 100%;">
        <h4>Đơn hàng (đơn vị: số đơn hàng/ngày)</h4>
        <canvas id="orders_report_canvas" height="300" width="400"></canvas>
        <p>
            <span style="width: 40px; height: 20px; float: left; background-color: rgba(220,220,220,0.2); border-top: 3px solid rgb(220,220,220); margin-right: 5px;"></span>
            <span>Đơn hàng phát sinh</span>
        </p>
        <p>
            <span style="width: 40px; height: 20px; float: left; background-color: rgba(151,187,205,0.2); border-top: 3px solid rgb(151,187,205); margin-right: 5px;"></span>
            <span>Đơn hàng hoàn thành</span>
        </p>
    </div>
    <div style="width: 100%; margin: 50px 0 0;">
        <h4>Doanh thu (đơn vị: VND)</h4>
        <canvas id="orders_money_report_canvas" height="300" width="400"></canvas>
        <p>
            <span style="width: 40px; height: 20px; float: left; background-color: rgba(220,220,220,0.2); border-top: 3px solid rgb(220,220,220); margin-right: 5px;"></span>
            <span>Doanh thu tổng</span>
        </p>
        <p>
            <span style="width: 40px; height: 20px; float: left; background-color: rgba(151,187,205,0.2); border-top: 3px solid rgb(151,187,205); margin-right: 5px;"></span>
            <span>Doanh thu thực tế</span>
        </p>
    </div>

    <script>
    var dataOrders = {
        labels: [<?php foreach ( $months as $mkey => $arc_row ) {
            if ( 0 == $arc_row->year )
                continue;
            $date = zeroise( $arc_row->date, 2 );
            $month = zeroise( $arc_row->month, 2 );
            $year = $arc_row->year;
            if($mkey > 0) echo ',';
            echo '"'. sprintf( '%1$s/%2$s/%3$d', $date, $month, $year ) .'"';
        } ?>],
        datasets: [
            {
                label: "Đơn hàng phát sinh",
                fillColor: "rgba(220,220,220,0.2)",
                strokeColor: "rgba(220,220,220,1)",
                pointColor: "rgba(220,220,220,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(220,220,220,1)",
                data: [<?php foreach ($all_orders_stt as $key => $m_orders) {
                    echo $m_orders['created'];
                    if($key < count($all_orders_stt)) echo ',';
                } ?>]
            },
            {
                label: "Đơn hàng thành công",
                fillColor: "rgba(151,187,205,0.2)",
                strokeColor: "rgba(151,187,205,1)",
                pointColor: "rgba(151,187,205,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(151,187,205,1)",
                data: [<?php foreach ($all_orders_stt as $key => $m_orders) {
                    echo $m_orders['completed'];
                    if($key < count($all_orders_stt)) echo ',';
                } ?>]
            }
        ]
    };

    var dataOrdersMoney = {
        labels: [<?php foreach ( $months as $mkey => $arc_row ) {
            if ( 0 == $arc_row->year )
                continue;
            $date = zeroise( $arc_row->date, 2 );
            $month = zeroise( $arc_row->month, 2 );
            $year = $arc_row->year;
            if($mkey > 0) echo ',';
            echo '"'. sprintf( '%1$s/%2$s/%3$d', $date, $month, $year ) .'"';
        } ?>],
        datasets: [
            {
                label: "Doanh thu tổng",
                fillColor: "rgba(220,220,220,0.2)",
                strokeColor: "rgba(220,220,220,1)",
                pointColor: "rgba(220,220,220,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(220,220,220,1)",
                data: [<?php foreach ($all_orders_stt as $key => $m_orders) {
                    echo $m_orders['money_all'];
                    if($key < count($all_orders_stt)) echo ',';
                } ?>]
            },
            {
                label: "Doanh thu thực tế",
                fillColor: "rgba(151,187,205,0.2)",
                strokeColor: "rgba(151,187,205,1)",
                pointColor: "rgba(151,187,205,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(151,187,205,1)",
                data: [<?php foreach ($all_orders_stt as $key => $m_orders) {
                    echo $m_orders['money_real'];
                    if($key < count($all_orders_stt)) echo ',';
                } ?>]
            }
        ]
    };


    var ctx1 = document.getElementById("orders_report_canvas").getContext("2d");
    var ctx2 = document.getElementById("orders_money_report_canvas").getContext("2d");
    window.myLine = new Chart(ctx1).Line(dataOrders);
    window.myLine = new Chart(ctx2).Line(dataOrdersMoney);
    </script>
<?php }

function random_color_part() {
    return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
}

function random_color() {
    return '#'. random_color_part() . random_color_part() . random_color_part();
}

function custom_products_list_template_callback(){
    $args = array(
        'post_type' => 'orders',
        'posts_per_page' => -1,
    );
    $orders_list = get_posts( $args );
    $product_total = array();

    foreach ($orders_list as $okey => $o) {
        $post_id = $o->ID;
        $info = get_post_meta($post_id, 'order_info', true);
        $payment_status = get_post_meta($post_id, 'order_payment_status', true);
        $payment_code = get_post_meta($post_id, 'order_payment_code', true);

        if(!$info || !isset($info['products'])) continue; // Skip if no products in orders

        foreach ($info['products'] as $ipkey => $ip) {
            if(!isset($product_total[$ipkey]))
                $product_total[$ipkey] = array(
                    'total' => 0,
                    'sold' => 0,
                    'color' => random_color(),
                    'highlight' => random_color(),
                    'label' => $ip['sku'] . ' - Size '. $ip['size'],
                    'id' => intval($ip['id']),
                    'permalink' => get_permalink( intval($ip['id']) )
                );

            $product_total[$ipkey]['total'] += intval( $ip['quantity'] );
            if($payment_status == true && $payment_code){
                $product_total[$ipkey]['sold'] += intval( $ip['quantity'] );
            }
            else
                $product_total[$ipkey]['sold'] += 0;
        }

    }

    $total = array();
    $sold = array();
    foreach($product_total as $pkey => $p) {
        $total[$pkey] = $p['total'];
        $sold[$pkey] = $p['sold'];
    }

    array_multisort($total, SORT_DESC, $sold, SORT_DESC, $product_total); ?>
    <div style="width: 100%;">
        <table class="widefat fixed">
            <thead>
                <tr>
                    <th width="60%">Sản phẩm</th>
                    <th width="20%">Đặt hàng</th>
                    <th width="20%">Đã mua hàng</th>
                </tr>
            </thead>
            <tbody><?php $number = 1;
            foreach ($product_total as $key => $p):
                if($number++ > 10) break; ?>
                <tr>
                    <td><a href="<?php echo $p['permalink']; ?>" id="product-<?php echo $p['id']; ?>"><?php echo $p['label']; ?></a></td>
                    <td><?php echo $p['total']; ?></td>
                    <td><?php echo $p['sold']; ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<?php }

function get_single_taxonomy_meta($meta_id, $term_id, $field_id) {
    $meta = get_option($meta_id);
    if (empty($meta)) $meta = array();
    if (!is_array($meta)) $meta = (array) $meta;
    $meta = isset($meta[$term_id]) ? $meta[$term_id] : array();
    $value = $meta[$field_id][0];
    return $value;
}

// Custom save post
add_action( 'save_post', 'save_metabox_post' );
function save_metabox_post($post_id) {
    if( wp_is_post_revision( $post_id ) )
        return;

    if(get_post_type() == 'bo-anh' && isset($_POST['do']) && $_POST['do'] == 'collection_savepost') {
        if(isset($_POST['image'])) {
            $json_result = array();
            foreach ($_POST['image'] as $key => $value) {
                if($value) {
                    $val = stripslashes($value);
                    $val = json_decode($val, true);
                    if(is_array($val) && !empty($val)) {
                        array_push($json_result, $val);
                    }
                }
            }
            update_post_meta($post_id, 'collection_galery', $json_result);
        }
    }
    elseif(get_post_type() == 'style-guide' && isset($_POST['do']) && $_POST['do'] == 'style_guide_savepost') {
        $tax = 'style-guide-category';
        if($_POST['tax_select'] != '-1') {
            $r = wp_set_object_terms($post_id, (int)$_POST['tax_select'], $tax);
            if(!is_wp_error($r)) {
                $target_save = str_replace('-', '_', $_POST['target_save']);
                $target_content_save = $target_save .'_content';

                if(isset($_POST[$target_save]['posts'])) {
                    $arr_save = array();
                    foreach ($_POST[$target_save]['posts'] as $value) {
                        $arr_save[] = $value;
                    }
                    update_post_meta($post_id, 'style_guide_save', $arr_save);
                }
                if(isset($_POST[$target_content_save])) {
                    if (!wp_is_post_revision($post_id)) {
                        remove_action('save_post', 'save_metabox_post');
                        wp_update_post(array('ID' => $post_id, 'post_content' => $_POST[$target_content_save]));
                        add_action('save_post', 'save_metabox_post');
                    }
                }
            }
        }
    }
    /*elseif(get_post_type() == 'showroom') {
        $do = isset($_POST['original_post_status']) ? $_POST['original_post_status']: '';
        $term_name = isset($_POST['agency_address']) ? $_POST['agency_address']: '';
        $tax = 'store';
        if($do == 'auto-draft') {
            $res = wp_insert_term($term_name, $tax, array('description' => $term_name));
            if(!is_wp_error($res))
                update_post_meta($post_id, 'store_id_follow', $res['term_id']);
            else {
                remove_action('save_post', 'save_metabox_post');
                wp_die('Có lỗi xảy ra! Vui lòng thử lại...');
            }
        }
        elseif($do == 'publish') {
            $t_id = get_post_meta($post_id, 'store_id_follow', true);
            wp_update_term($t_id, $tax, array(
                'name' => $term_name,
                'description' => $term_name
            ));
        }
    }*/
    elseif(get_post_type() == 'uniform' && isset($_POST['do']) && $_POST['do'] == 'save_uniform_metabox') {
        // if(!isset($_POST['uni_info']) || !isset($_POST['uni_exform']) || !isset($_POST['uni_fabric'])) {
        //     return;
        // }
        // info
        if(isset($_POST['uni_info'])) {
            update_post_meta($post_id, 'uni_info', $_POST['uni_info']);
        }
        if(isset($_POST['uni_fabric'])) {
            $uni_fabric = array();
            foreach ($_POST['uni_fabric'] as $t_key => $t) {
                if($t['title'] != '' && $t['imgs'] != '') {
                    $uni_fabric[] = array(
                        'title' => $t['title'],
                        'imgs' => explode(',', $t['imgs'])
                    );
                }
            }
            update_post_meta($post_id, 'uni_fabric', $uni_fabric);
        }
        // exform
        if(isset($_POST['uni_exform'])) {
            $exform = explode(',', $_POST['uni_exform']);
            update_post_meta($post_id, 'uni_exform', $exform);
        }
    }
}

/*add_action('trashed_post', 'delete_metabox_post');
function delete_metabox_post($post_id) {
    if(get_post_type() == 'showroom') {
        $t_id = get_post_meta($post_id, 'store_id_follow', true);
        $res = wp_delete_term($t_id, 'store');
        if(is_wp_error($res)) {
            remove_action('trashed_post', 'delete_metabox_post');
            wp_die('Có lỗi xảy ra! Vui lòng thử lại...');
        }
    }
}*/

/*add_filter('post_row_actions', 'remove_row_actions', 10, 1);
function remove_row_actions($actions) {
    if(get_post_type() == 'showroom') {
        unset($actions['trash']);
        $actions['delete'] = '<a href="'. get_delete_post_link(get_the_id(), '', true) .'" title="Delete this showroom/agency">Delete</a>';
    }
    return $actions;
}*/

//add_filter('gettext','custom_enter_title');
function custom_enter_title($input) {

    global $post_type;
    if(is_admin() && 'Enter title here' == $input && 'showroom' == $post_type)
        return 'Nhập địa chỉ cụ thể (Enter your full address)';
    return $input;
}

function get_current_url() {
    $pageURL = 'http';
    $pageURL .= "://";
    if ($_SERVER["SERVER_PORT"] != "80") {
        $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
    }
    else {
        $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
    }
    return $pageURL;
}

add_filter('excerpt_length', 'custom_excerpt_length');
function custom_excerpt_length( $length ) {
    global $post;
    if('news' == $post->post_type)
        return 12;
    else
        return $length;
}

// ADD PRODUCTS INTO WISHLIST
add_action('wp_ajax_add_to_wishlist_ajax', 'add_to_wishlist');
add_action('wp_ajax_nopriv_add_to_wishlist_ajax', 'login_require');

function add_to_wishlist() {
    $post_id = $_POST['product_id'];
    $user_id = get_current_user_id();
    $message = '';
    $result = 1;

    $current_post_count = get_post_meta($post_id, 'like_count', true);
    if($current_post_count == '') $current_post_count = 0;

    $user_wishlist = get_user_meta($user_id, 'wishlist', true);
    if(is_null($user_wishlist) || empty($user_wishlist) || $user_wishlist == '') $user_wishlist = array();

    if(in_array($post_id, $user_wishlist)){
        // array_splice($user_wishlist, array_search($post_id, $user_wishlist), 1);
        $pos = array_search($post_id, $user_wishlist);
        // echo $pos;
        unset($user_wishlist[$pos]);
        update_user_meta($user_id, 'wishlist', $user_wishlist);
        
        if($current_post_count > 0) {
            update_post_meta($post_id, 'like_count', ($current_post_count-1));
        }
        $message = 'Sản phẩm này đã bị xoá khỏi danh sách yêu thích của bạn';
        $result = 0;
    }
    else {
        array_push($user_wishlist, $post_id);
        $r = update_user_meta($user_id, 'wishlist', $user_wishlist);
        
        update_post_meta($post_id, 'like_count', ($current_post_count+1));

        $result = 1;
        $message = 'Sản phẩm đã được thêm vào danh sách yêu thích của bạn!';
    }
    echo json_encode(array('result' => $result, 'msg' => $message));
    exit();
}

function login_require() {
    echo json_encode(array('result' => $result, 'msg' => 'Bạn phải đăng nhập trước đó'));
    exit();
}

add_action('wp_ajax_top_live_search', 'top_live_search');
add_action('wp_ajax_nopriv_top_live_search', 'top_live_search');
function top_live_search(){
    $key = sanitize_text_field($_POST['key']);
    $keys = explode(' ', $key);
    $keys_bold = array();
    foreach($keys as $i => $k) {
        $keys_bold[] = '<strong>'. $k .'</strong>';
        $keys[$i] = '/'. $k .'/';
    }
    $args = array( 'posts_per_page' => 10, 'post_type' => 'post', 's' => $key );
    $posts = new WP_Query( $args );
    $html = '';

    if($posts->have_posts()){
        while ($posts->have_posts()) {
            $posts->the_post();
            $product_colors = get_post_meta(get_the_id(), 'post_colors', true);
            foreach ($product_colors as $c_key => $c) {
                $product_imgs = explode(',', $c['images_ids']);
                $html_full = $html_thumb = '';
                foreach ($product_imgs as $key => $img_id) {
                    $img_src = wp_get_attachment_image_src($img_id, 'thumbnail');
                    break;
                }
                break;
            }
            $title = get_the_title( get_the_id() );
            $title = preg_replace( $keys, $keys_bold, $title );

            $html .= '<li><div><img src="'. $img_src[0] .'" alt=""><a href="'. get_permalink(get_the_id()) .'">'. $title .'</a></li></div>';
        }
        wp_reset_postdata();
    }
    else{
        $html = '<li><p>Không có kết quả phù hợp!</p></li>';
    }

    echo json_encode(array('html' => $html));
    exit;
}

/*
 * FUNCTIONS 
 */
function aj_get_term($parent_id, $taxonomy){
    $term = get_term($parent_id, $taxonomy);
    if($term->parent == 0){
        return array($term);
    }else{
        return array_merge(aj_get_term($term->parent, $taxonomy), array($term));
    }
}

function aj_get_f1_terms_by_parent($parent_id, $taxonomy = 'category'){
    $result = get_terms($taxonomy, array(
        'orderby'       => 'name', 
        'order'         => 'ASC',
        'parent' => $parent_id,
        'hide_empty'    => false
    ));
    return $result;
}

function aj_get_post_term($post_id, $taxonomy = 'category', $string = true, $itemprop = ''){
    $terms = wp_get_post_terms($post_id, $taxonomy);
    if(!empty($terms) && !isset($terms->errors)){
        $term = reset($terms);
        $term_url = get_term_link($term->term_id, $taxonomy);
        if(isset($term_url->errors)){
            $term_url = '';
        }
        if($string == true){
            return '<a '.$itemprop.' href="'.$term_url.'">'.$term->name.'</a>';
        }else{
            return $term;
        }
    }
    return false;
}

function aj_get_all_shirt_sizes(){
    return array(
//         'XXS' => 'XXS',
//         'XS' => 'XS',
//         'SS' => 'SS',
        'S' => 'S',
        'M' => 'M',
        'L' => 'L',
        'XL' => 'XL',
        'XXL' => 'XXL',
        '3XL' => '3XL'
//         '4XL' => '4XL',
    );
}

function aj_get_all_pants_sizes(){
    return array(
//         '27' => '27',
        '28' => '28',
        '29' => '29',
        '30' => '30',
        '31' => '31',
        '32' => '32',
        '33' => '33',
        '34' => '34',
        '35' => '35',
        '36' => '36',
        '37' => '37',
        '38' => '38',
//         '39' => '39',
//         '40' => '40',
//         '41' => '41',
//         '41' => '41',
//         '42' => '42',
        '00' => 'Size chung'
//        '50-30' => 'Thùng',
    );
}

function aj_get_image_src($id, $size = 'full'){
    if($id > 0){
        $imageInfo = wp_get_attachment_image_src( $id, $size);
        return $imageInfo[0];
    }
    return false;
}

function aj_wp_get_post_terms($post_id, $taxonomy = 'category', $string = true, $itemprop = ''){
    $terms = wp_get_post_terms($post_id, $taxonomy);
    if(!empty($terms) && !isset($terms->errors)){
        if($string == true){
            $rs = array();
            $itemprop = $itemprop ? 'itemprop="'.$itemprop.'"' : '';
            foreach($terms as $t){
                $term_url = get_term_link($t->term_id, $taxonomy);
                if(isset($term_url->errors)){
                    $term_url = '';
                }
                $rs[] = '<a '.$itemprop.' href="'.$term_url.'">'.$t->name.'</a>';
            }
            return implode(', ', $rs);
        }else{
            return $terms;
        }
    }
    return false;
}

function aj_nav_menu($menu_name){
    return wp_nav_menu( array( 'theme_location' => $menu_name, 'container' => 'false', 'items_wrap' => '%3$s') );
}

// Cart
function get_turnover_by_user($uid) {
    $order_revenue = get_user_meta( $uid, 'order_revenue', true );
    if( $order_revenue != '' ){
        return aj_format_number( intval( $order_revenue ) ) .'Đ';
    }

    $args = array(
        'posts_per_page' => -1,
        'post_type' => 'orders',
        'orderby' => 'date',
        'order' => 'DESC',
        'meta_query' => array(
            array(
                'key' => 'order_user_id',
                'value' => $uid
            ),
            array(
                'key' => 'order_payment_status',
                'value' => true
            )
        )
    );
    $orders = get_posts( $args );
    $total = 0;
    foreach ($orders as $o_key => $o):
        $o_id = $o->ID;
        $order_info = get_post_meta($o_id, 'order_info', true);
        $total += empty($order_info['total']) ? 0 : $order_info['total'];
    endforeach;
    return aj_format_number($total) .'Đ';
}

// add_action('wp_ajax_social__get_fb_feeds', 'social__get_fb_feeds');
// add_action('wp_ajax_nopriv_social__get_fb_feeds', 'social__get_fb_feeds');
function social__get_fb_feeds() {
    $fbs = get_fb_feeds();
    $fb_a = array();

    $fb_info = array(
        'name' => $fbs['page_info']['name'],
        'link' => $fbs['page_info']['link'],
        'picture' => '<img src="https://graph.facebook.com/'. $fbs['page_info']['id'] .'/picture?width=60&height=60" style="width: 60px; height: 60px; display: block">'
    );
    foreach ($fbs['posts']['data'] as $key => $fb) {
        if(isset($fb['message']))
            if(!isset($fb['story_tags']) || !isset($fb['status_type'])) {
                $fb_a[] = array(
                    'id' => substr($fb['id'], strpos($fb['id'], '_') + 1),
                    'content' => isset($fb['message']) ? wp_trim_words($fb['message'], 20, '...'):'',
                    'link' => $fb_info['link'] .'/posts/'. substr($fb['id'], strpos($fb['id'], '_') + 1),
                    'date_created' => isset($fb['created_time']) ? date_format(new DateTime($fb['created_time']), 'M d, Y'):'',
                    'date_updated' => isset($fb['updated_time']) ? date_format(new DateTime($fb['updated_time']), 'M d, Y'):'',
                    'likes' => get_fb_likes_count($fb['id']),
                    'comments' => get_fb_comments_count($fb['id'])
                );
            }
    }
    echo json_encode(array('results' => $fb_a));
    exit();
}

// Check date in range date
function check_current_date_in_range($start_date, $end_date){
    
    $start_ts = strtotime($start_date);
    $end_ts = strtotime($end_date);
    $user_ts = current_time('timestamp', 0);

    return (($user_ts >= $start_ts) && ($user_ts <= $end_ts));
}


add_action( 'admin_init', 'my_remove_menu_pages' );
function my_remove_menu_pages() {

    // global $user_ID;

    if ( current_user_can( 'editor' ) ) {
    remove_menu_page( 'edit.php' );
    remove_menu_page( 'tools.php' );
    remove_menu_page( 'edit.php?post_type=orders' );
        remove_menu_page( 'edit.php?post_type=page' );
        remove_menu_page( 'edit.php?post_type=coupon' );
        remove_menu_page( 'edit.php?post_type=campaign' );
        remove_menu_page( 'edit.php?post_type=uniform' );
        
    }
}

/**
 * Like get_template_part() but lets you pass args to the template file
 * Args are available in the template as $args array.
 * Args can be passed in as url parameters, e.g 'key1=value1&key2=value2'.
 * Args can be passed in as an array, e.g. ['key1' => 'value1', 'key2' => 'value2']
 * Filepath is available in the template as $file string.
 * @param string      $slug The slug name for the generic template.
 * @param string|null $name The name of the specialized template.
 * @param array       $args The arguments passed to the template
 */

function _get_template_part( $slug, $name = null, $args = array() ) {
    if ( isset( $name ) && $name !== 'none' ) $slug = "{$slug}-{$name}.php";
    else $slug = "{$slug}.php";
    $dir = get_template_directory();
    $file = "{$dir}/{$slug}";

    ob_start();
    $args = wp_parse_args( $args );
    $slug = $dir = $name = null;
    require( $file );
    echo ob_get_clean();
}