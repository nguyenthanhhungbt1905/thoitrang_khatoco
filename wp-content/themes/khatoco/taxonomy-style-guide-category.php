<?php
get_header();
$term_slug = get_query_var('term');
$tax = get_query_var('taxonomy');
$term = get_term_by('slug', $term_slug, $tax);
$terms = get_terms( $tax, array( 'hide_empty' => 0 ) );
?>
    <script type="text/javascript" src="<?php echo get_template_directory_uri().'/assets/js/style-guide.js' ?>"></script>

    <section id="style-guide-category-page">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h3 class="masthead">Gu Đàn Ông</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="group-category break-line">
                            <?php $categoryList = get_style_guide_category(); ?>
                            <?php foreach($categoryList as $key => $singleItem){ ?>
                            <div class="item <?php if($term_slug == $singleItem['slug']){ echo "active"; } ?>" category-type="<?php echo $singleItem['slug'] ?>" data-toggle="tab" href="<?php echo "#".$singleItem['slug']."-tab"; ?>"
                                 aria-expanded="true"><span class="name"><?php echo $singleItem['title'] ?></span></div>
                        <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 tab-content">
                        <?php foreach($categoryList as $key => $singleItem){ ?>
                        <?php if($singleItem['slug'] == $term_slug){
                            ?>
                            <div id="<?php echo $singleItem['slug']."-tab"?>" class="tab-pane fade active in">
                                <div class="row">
                                            <div class="col-xs-12">
                                        <img class="event-banner img-fluid d-none"
                                             src="<?php echo wp_upload_dir()['baseurl'] . "/" . get_settings('banner-event-news') ?>"
                                             alt="">
                                        <img class="event-banner img-fluid w-100"
                                             src="<?php echo  $singleItem['banner_image'] ?>"
                                             alt="">
                                    </div>
                                    <div class="col-xs-12">
                                        <p class="masthead"><?php echo $singleItem['title'] ?></p>
                                        <p class="short-description"><?php echo $singleItem['description'] ?></p>
                                        <div class="break-line">
                                        </div>
                                        <div class="row" id="list-<?php echo $singleItem['slug'] ?>-render">
                                            <?php $eventList = display_post_with_filter($singleItem['slug'],0,'style-guide') ?>
                                            <!--                                            --><?php //$eventList = display_news_with_filter('su-kien') ?>
                                            <?php foreach($eventList as $event){ ?>
                                                <div class="col-xs-12 col-sm-6 col-lg-4">
                                                    <div class="group-event">
                                                        <div class="image-side">
                                                            <!--                                <img class="img-fluid" src="--><?php //echo $event['feature_image'] ?><!--" alt="--><?php //echo $event['title'] ?><!--">-->
                                                            <img class="img-fluid" src="<?php echo get_template_directory_uri()."/images/new/test-image.png" ?>" alt="<?php echo $event['title'] ?>">
                                                        </div>
                                                        <div class="content-side">
                                                            <a href="<?php echo $event['link'] ?>">
                                                                <p class="name"><?php echo $event['title'] ?></p>
                                                            </a>
                                                            <p class="description"><?php echo $event['short_description'] ?></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 text-center">
                                                <a href="javascript:void(0);" data-render="list-<?php echo $singleItem['slug'] ?>-render" class="all-event-cta" data-offset="<?php echo count($eventList) ?>" data-type="<?php echo $singleItem['slug'] ?>" data-post="style-guide">XEM THÊM</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php }else{ ?>
                        <div id="<?php echo $singleItem['slug'] ?>-tab" class="tab-pane fade">
                            <div class="row">
                                <div class="col-xs-12">
                                    <img class="event-banner img-fluid d-none"
                                         src="<?php echo wp_upload_dir()['baseurl'] . "/" . get_settings('banner-event-news') ?>"
                                         alt="">
                                    <img class="event-banner img-fluid w-100"
                                         src="<?php echo  $singleItem['banner_image'] ?>"
                                         alt="">
                                </div>
                                <div class="col-xs-12">
                                    <p class="masthead"><?php echo $singleItem['title'] ?></p>
                                    <p class="short-description"><?php echo $singleItem['description'] ?></p>
                                    <div class="break-line">
                                    </div>
                                    <div class="row" id="list-<?php echo $singleItem['slug'] ?>-render"></div>
                                    <div class="row">
                                        <div class="col-xs-12 text-center">
                                            <a href="javascript:void(0);" data-render="list-<?php echo $singleItem['slug'] ?>-render" class="all-event-cta" data-offset="0" data-type="<?php echo $singleItem['slug'] ?>" data-post="style-guide">XEM THÊM</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
<?php echo get_template_part('partials/event-banner'); ?>
<?php get_footer(); ?>