'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var gulpCopy = require('gulp-copy');
sass.compiler = require('node-sass');

gulp.task('sass', function () {
    return gulp.src('./assets/sass/*.scss')
        .pipe(sass.sync().on('error', sass.logError))
        .pipe(gulp.dest('./assets/css/convert'));
});

gulp.task('slick', function () {
    return gulp.src('./assets/plugins/slick/*.scss')
        .pipe(sass.sync().on('error', sass.logError))
        .pipe(gulp.dest('./assets/css/convert/plugins'));
});

gulp.task('sass:watch', function () {
    gulp.watch('./assets/sass/*.scss', ['sass']);
});
