<?php 
/*
Template Name: Kết nối mạng xã hội
*/
get_header(); ?>
<div class="block-outer block-social">
	<div class="block-inner">
		<div class="containerpd clearfix">
			<div class="block-title-outer"><h2 class="block-title block-title-inner condensed">Kết nối mạng xã hội</h2></div>
			<div class="block-description-outer clearfix">hiểu biết thêm về thương hiệu thời trang khatoco trong lòng cộng đồng mạng</div>
		</div>
		<div class="block-content container fluid">
			<div class="grid3 fl youtube-block social-block">
				<div class="item-outer">
					<div class="item-title">
						<p><a href="https://www.youtube.com/channel/UCipoobgi12E4u51tM2QWsVA" target="_blank" rel="publisher"><img nopin="nopin" src="<?php echo get_template_directory_uri(); ?>/images/youtube-logo.png" alt=""></a></p>
					</div>
					<div class="item-content clearfix">
						<div class="item-content-inner"><?php $yts = get_yt_feeds();
						$yt_a = array();
						foreach ($yts['items'] as $key => $yt) {
						 	if(isset($yt['contentDetails']['upload']))
						 		$yt_a[]['id'] = $yt['contentDetails']['upload']['videoId'];
						}
						if(!is_null($yt_a) || !empty($yt_a)) {
							foreach ($yt_a as $key => $ytId) {
								$yt_details = get_video_details($ytId['id']);
								$content = file_get_contents('http://gdata.youtube.com/feeds/api/videos/'. $ytId['id'] .'?fields=title&format=5&alt=json');
								$content = json_decode($content, true);
								$yt_a[$key]['title'] = $content['entry']['title']['$t'];
								$yt_a[$key]['statistics'] = $yt_details['modelData']['items'][0]['statistics'];
							}
						}
						foreach($yt_a as $yt): ?>
							<div class="row yt-link clearfix">
								<p><a href="<?php echo 'http://youtube.com/watch?v='. $yt['id']; ?>" data-src="<?php echo $yt['id']; ?>" target="_blank"><img nopin="nopin" src="<?php echo 'http://img.youtube.com/vi/'. $yt['id'] .'/0.jpg'; ?>"></a></p>
								<h3><a href="<?php echo 'http://youtube.com/watch?v='. $yt['id']; ?>" data-src="<?php echo $yt['id']; ?>" target="_blank"><?php echo $yt['title']; ?></a></h3>
								<p style="padding-bottom: 10px"><?php echo $yt['statistics']['viewCount'] .' lượt xem'; ?></p>
							</div>
						<?php endforeach; ?></div>
					</div>
				</div>
			</div>
			<div id="fb-block" class="grid3 fl fb-block social-block">
				<div class="item-outer" style="overflow: hidden">
					<div class="item-title">
						<p><a href="http://facebook.com/thoitrangkhatoco" target="_blank" rel="publisher"><img src="<?php echo get_template_directory_uri(); ?>/images/fb-logo.png" alt=""></a></p>
					</div>
					<div class="item-content clearfix">
						<!-- <p class="tac loading"><i class="fa fa-refresh fa-spin"></i></p> -->
						<div class="item-content-inner">
						<div class="fb-page" data-href="https://www.facebook.com/thoitrangkhatoco" data-witdh="280" data-hide-cover="true" data-show-facepile="false" data-show-posts="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/thoitrangkhatoco"><a href="https://www.facebook.com/thoitrangkhatoco">Thoi Trang Khatoco</a></blockquote></div></div>
						<?php /*$fbs = get_fb_feeds();
						$fb_a = array();
						// print_r($fbs['posts']);
						$fb_info = array(
							'name' => $fbs['page_info']['name'],
							'link' => $fbs['page_info']['link'],
							'picture' => '<img src="https://graph.facebook.com/'. $fbs['page_info']['id'] .'/picture?width=60&height=60" style="width: 60px; height: 60px; display: block">'
						);
						foreach ($fbs['posts']['data'] as $key => $fb) {
							if(isset($fb['message']))
								if(!isset($fb['story_tags']) || !isset($fb['status_type'])) {
									$fb_a[] = array(
										'id' => substr($fb['id'], strpos($fb['id'], '_') + 1),
										'content' => isset($fb['message']) ? wp_trim_words($fb['message'], 20, '...'):'',
										'link' => $fb_info['link'] .'/posts/'. substr($fb['id'], strpos($fb['id'], '_') + 1),
										'date_created' => isset($fb['created_time']) ? date_format(new DateTime($fb['created_time']), 'M d, Y'):'',
										'date_updated' => isset($fb['updated_time']) ? date_format(new DateTime($fb['updated_time']), 'M d, Y'):'',
										'likes' => get_fb_likes_count($fb['id']),
										'comments' => get_fb_comments_count($fb['id'])
									);
								}
						}
						// print_r($fb_a);
						foreach ($fb_a as $key => $fb): ?>
							<div class="row clearfix">
								<div class="clearfix">
									<a href="<?php echo $fb['link']; ?>" style="display: inline-block; float: left" target="_blank"><?php echo $fb_info['picture']; ?></a>
									<div class="fl" style="padding-left: 5px">
										<h4><a href="<?php echo $fb['link']; ?>" target="_blank"><?php echo $fb_info['name']; ?></a></h4>
										<p><span class="date"><i class="fa fa-calendar"></i> <?php echo $fb['date_updated'] ?></p>
									</div>
								</div>
								<div class="the_content clearfix">
									<?php echo wp_trim_words($fb['content'], 20, '...');
									$img = @file_get_contents('http://graph.facebook.com/'. $fb['id'] .'/picture');
									if($img) echo '<img src="" alt="">' ?>
									
								</div>
								<div class="fb_count clearfix">
									<p>
										<span class="like_count"><?php echo isset($fb['likes']['summary']['total_count']) ? $fb['likes']['summary']['total_count']:'0'; ?></span>&nbsp;<span class="comment_count"><?php echo isset($fb['comments']['summary']['total_count']) ? $fb['comments']['summary']['total_count']:'0'; ?></span>
									</p>
								</div>
							</div>
						<?php endforeach;*/ ?></div>
					</div>
				</div>
			</div>
			<div class="grid3 fl ggplus-block social-block">
				<div class="item-outer" style="overflow: hidden">
					<div class="item-title">
						<p><a href="https://plus.google.com/101732736942318540751" target="_blank" rel="publisher"><img src="<?php echo get_template_directory_uri(); ?>/images/ggplus-logo.png" alt=""></a></p>
					</div>
					<div class="item-content clearfix">
						<div class="item-content-inner"><?php $gps = get_gp_feeds();
						$gp_a = array();

						foreach($gps['modelData']['items'] as $gp) {
							$gp_a[] = array(
								'actor' => $gp['actor'],
								'title' => isset($gp['title']) ? $gp['title']:'',
								'content' => $gp['object']['content'],
								'link' => isset($gp['url']) ? $gp['url']:'',
								'type' => isset($gp['object']['type']) ? $gp['object']['type']:'',
								'date_updated' => isset($gp['updated']) ? date_format(new DateTime($gp['updated']), 'M d, Y'):date_format(new DateTime($gp['published']), 'M d, Y'),
							);
						}
						foreach ($gp_a as $key => $gp): ?>
							<div class="row clearfix">
								<div class="clearfix">
									<a class="fl" href="<?php echo $gp['link']; ?>" target="_blank"><img src="<?php echo $gp['actor']['image']['url']; ?>" style="width: 60px; height: 60px; background-color: #fff; border-radius: 100%"></a>
									<div class="fl" style="padding-left: 5px">
										<h4><a href="<?php echo $gp['link']; ?>" target="_blank"><?php echo 'Thời trang Khatoco';//$gp['actor']['displayName']; ?></a></h4>
										<p><span class="date"><i class="fa fa-calendar"></i> <?php echo $gp['date_updated']; ?></p>
									</div>
								</div>
								<div class="the_content clearfix">
									<?php echo $gp['title']; ?>
									<?php //echo wp_trim_words($gp['content'], 15, '...'); ?>
								</div>
								<div class="gp_action clearfix">
								</div>
							</div>
						<?php endforeach; ?>
						</div>
					</div>
				</div>
			</div>
			<div class="grid3 fl pinterest-block social-block">
				<div class="item-outer" style="overflow: hidden">
					<div class="item-title">
						<p><a href="http://pinterest.com/khatoco/"><img src="<?php echo get_template_directory_uri(); ?>/images/pinterest-logo.png" alt=""></a></p>
					</div>
					<div class="item-content clearfix">
						<div class="item-content-inner"><?php $pin_lst = json_decode(curl_get_content(), true);
						$pin_a = array();
						foreach($pin_lst['data']['pins'] as $pin) {
							$pin_a[] = array(
								/*'pinner' => array(
									'id' => $pin['pinner']['id'],
									'name' => $pin['pinner']['full_name'],
									'link' => $pin['pinner']['profile_url'],
								),
								'description' => $pin['description'],
								'id' => $pin['id'],*/
								'img' => $pin['images']['237x']['url'],
								'link' => 'http://pinterest.com/pin/'. $pin['id'],
							);
						}
						foreach ($pin_a as $key => $pin): ?>
							<div class="row clearfix">
								<div class="clearfix">
									<a href="<?php echo $pin['link']; ?>" target="_blank"><img src="<?php echo $pin['img'] ?>" alt=""></a>
								</div>
								<div class="pin_action clearfix">
									
								</div>
							</div>
						<?php endforeach; ?></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="video-popup" class="hidden">
	<div class="video-popup-inner">
		<iframe width="595" height="395" src="" frameborder="0" allowfullscreen></iframe>
		<span class="close">x</span>
	</div>
</div>
<?php get_footer(); ?>