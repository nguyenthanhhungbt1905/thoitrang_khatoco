<?php get_header();
global $post;
$cur_post_id = $post->ID;
$tax = 'bo-suu-tap';
$terms = get_the_terms( $post, $tax );
$term = array_shift($terms);
$term_slug = $term->slug; ?>
<div class="block-outer block-collection">
    <div class="container fluid clearfix">
        <div class="block-inner">
            <div class="block-title-outer"><h2 class="block-title block-title-inner condensed"><?php echo $term->name; ?></h2></div>
            <div id="collection_masonry"><?php $args = array('posts_per_page' => 4, 'post_type' => 'bo-anh', $tax => $term_slug);
            $clt = new WP_Query($args);
            if($clt->have_posts()):
               $i = 1; ?>
                <!--p class="tab-title"><?php while($clt->have_posts()) {
                    $clt->the_post(); 
                    if($i++ > 1) echo '<span class="header-top-separator"> | </span>';
                    else $cur_p = $post;
                    echo '<a '. (($cur_post_id == $post->ID) ? 'class="active"' : '') .' href="'. get_permalink( $post->ID ) .'">'. get_the_title() .'</a>';
                }
                endif; wp_reset_postdata(); ?></p--> <?php $galery = get_post_meta($cur_post_id, 'collection_galery', true); ?>
                <div class="tab-content">
                    <div class="collection-desc clearfix the-content">
                        <?php echo apply_filters('the_content', $post->post_content); ?>
                    </div>
                    <div class="row download-collection clearfix">
                        <p><a href="<?php echo home_url('/') .'download.php?collection='. $cur_post_id; ?>">Tải xuống bộ ảnh</a></p>
                    </div>
                    <ul class="masonry-holder list-style-none clearfix"><?php if(sizeof($galery) > 0 && is_array($galery)):
                        foreach ($galery as $key => $p):
                            foreach ($p as $img => $pid):
                                $img_src = wp_get_attachment_image_src($img, 'large');
                                $img_src = $img_src[0];
                                $full_img_src = wp_get_attachment_image_src($img, 'full');
                                $full_img_src = $full_img_src[0]; ?>
                                <li class="grid6 mansonry-item collection-item clearfix"><a href="#" data-post-id="<?php echo $pid; ?>" data-link="<?php echo get_permalink($pid); ?>">
                                    <img src="<?php echo $img_src; ?>" alt="">
                                    <span class="condensed">Xem ngay</span>
                                </a></li><?php
                            endforeach; 
                        endforeach;
                    endif; ?>
                    </ul>
                    <div class="row download-collection clearfix">
                        <p><a href="<?php echo home_url('/') .'download.php?collection='. $cur_post_id; ?>">Tải xuống bộ ảnh</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="popup" class="popup-outer hidden">
    </div>
</div>
<?php get_footer(); ?>