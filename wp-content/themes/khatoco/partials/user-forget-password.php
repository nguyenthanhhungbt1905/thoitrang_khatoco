<?php 
/*
Template Name: Trang cá nhân - Quên mật khẩu
*/

if( isset( $_GET['verify'] ) && isset( $_GET['email'] ) ){
	$email = isset( $_GET['email'] ) ? sanitize_email( $_GET['email'] ) : '';
	if( is_email( $email ) ){
		$user_id = email_exists( $email );
		if( $user_id ){
			$request = get_user_meta( $user_id, 'request_change_pw', true );
			if( !isset( $request['verify_nonce'] ) || ( isset( $request['verify_nonce'] ) && $request['verify_nonce'] != $_GET['verify'] ) ){
				wp_redirect( get_permalink() );
				exit;
			}
		}
	}
}

if( isset( $_POST['submit_forgot'] ) && isset( $_POST['nonce'] ) ){
	$email = isset( $_POST['email'] ) ? sanitize_email( $_POST['email'] ) : '';
	if( wp_verify_nonce( sanitize_text_field( $_POST['nonce'] ), 'forget_password' ) ){
		if( is_email( $email ) ){
			$user_id = email_exists( $email );
			if($user_id ){
				$verify = wp_generate_password( 8, false );
				$time = current_time( 'd-m-Y H:i:s' );
				
				$headers = array();
				$headers[] = 'Content-Type: text/html; charset=UTF-8';
				$headers[] = 'Cc: thoitrangkhatoco@gmail.com';
				$subject = '[Khatoco] Lấy lại mật khẩu';
				$message .= '<p>Chào bạn,</p>';
				$message .= '<p>Chúng tôi nhận được yêu cầu lấy lại mật khẩu của bạn lúc '. $time .'. Nếu đó là bạn, vui lòng nhấn vào link sau để thay đổi mật khẩu</p>';
				$message .= '<p>'. add_query_arg( array( 'verify' => $verify, 'email' => $email ), get_permalink() ) .'</p>';
				$message .= '<p>Chân thành cám ơn!</p>';

				wp_mail( $email, $subject, $message, $headers );
				
				update_user_meta( $user_id, 'request_change_pw', array(
					'time' => $time,
					'verify_nonce' => $verify,
				) );
				
				$_SESSION['fg_message'] = 'Thư đã được gửi đến Email của bạn. Vui lòng kiểm tra hộp thư để thực hiện lấy lại mật khẩu';
				$_SESSION['verify_forgot_completed'] = 1;
			}
			else{
				$_SESSION['fg_message'] = 'Tài khoản email bạn nhập không tồn tại';
			}
		}
		else{
			$_SESSION['fg_message'] = 'Email bạn nhập không đúng';
		}
	}
	else{
		$_SESSION['fg_message'] = 'Vui lòng thử lại';
	}

	wp_redirect( get_permalink() );
	exit;
}

if( isset( $_POST['submit_verify'] ) ){
	if( wp_verify_nonce( sanitize_text_field( $_POST['vr_nonce'] ), 'verify_password' ) ){
		if( $user_id ){
			wp_update_user( array(
				'ID' => $user_id,
				'user_pass' => $_POST['new_password'],
			) );
			
			$_SESSION['vr_message'] = 'Bạn đã thay đổi mật khẩu thành công! Dùng mật khẩu bạn vừa đổi để đăng nhập.';
			// Tự login vào sau khi đổi mật khẩu thành công
			$creds = array(
					'user_login'    => get_userdata($user_id)->user_login,
					'user_password' => $_POST['new_password'],
					'remember'      => true
			);
			$user = wp_signon( $creds, false );
			if ( is_wp_error( $user ) ) {
				echo $user->get_error_message();
			} else {
				wp_redirect( get_home_url() );
				exit;
			}

		}
	}
	else{
		$_SESSION['vr_message'] = 'Vui lòng thử lại';
	}

	wp_redirect( add_query_arg( array( 'verify' => $verify, 'email' => $email ), get_permalink() ) );
	exit;
}

get_header(); ?>
<div class="block-outer block-account block-users-pass">
	<div class="container fluid block-inner clearfix">
		<div class="block-title-outer"><h2 class="block-title block-title-inner condensed">Quên mật khẩu</h2></div>
		<div class="block-inner clearfix">
			<?php if( !isset( $_GET['verify'] ) ): ?>
			<form action="" method="POST" accept-charset="utf-8" class="tac"><?php if( isset( $_SESSION['fg_message'] ) ): ?>
				<p><?php echo $_SESSION['fg_message']; unset($_SESSION['fg_message']); ?></p>
				<?php else: ?>
				<p>Nhập Email mà bạn đã dùng để đăng ký. Chúng tôi sẽ gửi thông tin thay đổi mật khẩu tới Email của bạn.</p>
				<?php endif; ?>
				<input type="hidden" name="nonce" value="<?php echo wp_create_nonce( 'forget_password' ); ?>">
				<input type="text" name="email" id="email" placeholder="Email *" required="" <?php if( !empty($_SESSION['verify_forgot_completed'])) {echo 'style="display:none"';}?>>

				<?php if( empty($_SESSION['verify_forgot_completed']) ) {?>
				<button type="submit" name="submit_forgot">Gửi</button>
				<?php } else { ?>
					<button type="button" name="submit_forgot" onclick="window.location.href='<?php echo get_home_url()?>'">Quay về trang chủ</button>
				<?php unset($_SESSION['verify_forgot_completed']); } ?>
			</form>
			<?php else: ?>
			<form action="" method="POST" accept-charset="utf-8" class="tac"><?php if( isset( $_SESSION['vr_message'] ) ): ?>
				<p><?php echo $_SESSION['vr_message']; unset($_SESSION['vr_message']); ?></p>
				<?php else: ?>
				<p>Nhập mật khẩu mới của bạn</p>
				<?php endif; ?>
				<input type="hidden" name="vr_nonce" value="<?php echo wp_create_nonce( 'verify_password' ); ?>">
				<input type="password" name="new_password" value="" required="">
				<button type="submit" name="submit_verify">Cập nhật</button>
			</form>
			<?php endif; ?>
		</div>
	</div>
</div>

<style type="text/css">
	.block-users-pass .block-inner{padding: 30px 0;}
	.block-users-pass form{
		width: 320px;
		margin: 0 auto;
	}
	.block-users-pass p{
		padding: 10px 15px;
		border-left: 3px solid #c61d20;
		background-color: #f1f1f1;
		text-align: left;
	}
	.block-users-pass input, 
	.block-users-pass button{
		width: 100%;
		padding: 5px;
		margin: 5px 0;
		font-size: 18px;
	}
	.block-users-pass button{
		display: inline-block;
		width: auto;
		border: 0;
		padding: 5px 15px;
		background-color: #c61d20;
		color: #fff;
	}
</style>

<?php get_footer(); ?>
