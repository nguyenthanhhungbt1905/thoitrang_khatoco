<?php 
/*
Template Name: Trang cá nhân - Sản phẩm yêu thích
*/

get_header(); ?>
<div class="block-outer block-account block-users-wishlist">
	<div class="block-inner">
	<div class="container fluid clearfix">
		<div class="block-title-outer"><h2 class="block-title block-title-inner condensed"><span>Sản phẩm yêu thích</span></h2></div>
		<div class="block-list-products clearfix"><?php $user_id = get_current_user_id();
	    $user_wishlist = get_user_meta($user_id, 'wishlist', true);
	    if(is_null($user_wishlist) || empty($user_wishlist) || $user_wishlist == '')
	    	echo '<p style="text-align:center">Bạn chưa chọn được sản phẩm yêu thích nào. <a href="'. get_page_link(16) .'" style="color:#c61d20">Dạo một vòng</a> để có được những sản phẩm ưng ý!</p>';
	    else {
		echo '<ul class="list-style-none lists grid-style clearfix">';
	    foreach ($user_wishlist as $wkey => $w):
	    	$w = intval($w); ?>
	    	<li class="grid3 single-item-product">
				<div class="item-outer clearfix">
					<div class="item-inner clearfix"><?php $product_colors = get_post_meta($w, 'post_colors', true);
					if(!$product_colors) $product_colors = array();
					foreach($product_colors as $c_key => $c):
						$images_ids = explode(',', $c['images_ids']);
						$images_ids = $images_ids[0];
						$images_ids = wp_get_attachment_image_src($images_ids, array(350, 1000)); ?>
						<div class="item-thumb" data-color="<?php echo $c_key; ?>">
							<a href="<?php echo get_permalink($w); ?>"><img src="<?php echo $images_ids[0]; ?>" /></a>
							<span class="add-to-wishlist fav" data-product-id="<?php echo $w; ?>"><i class="fa fa-heart"></i></span>
						</div><?php endforeach; ?>
						<h3 class="item-title condensed"><a href="<?php echo get_permalink($w); ?>"><?php echo get_the_title($w); ?></a></h3>
						<p class="item-price">Giá <?php echo number_format(get_post_meta($w, 'post_price', true ), 0, ',', '.'); ?> Đ</p>
						<p class="item-color clearfix"><?php foreach ($product_colors as $c_key => $c) { ?>
							<span class="color" style="background-color: <?php echo $c['code']; ?>" title="<?php echo $c['name']; ?>" data-color="<?php echo $c_key; ?>"></span>
						<?php } ?></p>
					</div>
				</div>
			</li>
	    <?php endforeach;
	    echo '</ul>';
	    } ?>
		</div>
	</div>
	</div>
</div>

<?php get_footer(); ?>
