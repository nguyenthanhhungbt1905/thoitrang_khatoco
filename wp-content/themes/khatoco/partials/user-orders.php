<?php if(!isset($_SESSION)) session_start();
/*
Template Name: Trang cá nhân - Danh sách đơn hàng
*/

if( get_current_user_id() <= 0 && !isset( $_GET['order_code'] ) ){
	wp_redirect(home_url('/'));
	exit;
}

$_paid = false;
$_paymentStatus = array(
    'status' => 'failed',
    'message' => 'Giao dịch không thành công. Vui lòng thử lại sau ít phút nữa!'
);

if(isset($_GET['paid']) && isset($_GET['statusPayment']) && isset($_GET['orderID']) && isset($_GET['_nonce'])) {
	if( !wp_verify_nonce( sanitize_text_field($_GET['_nonce']), 'hash_dr' ) ){
		$_paid = true;
		$_paymentStatus = array(
			'status' => 'failed',
			'message' => 'Cheatin\' huh?'
		);
	}
	else{
		$_paid = true;
		$temp_status = isset($_GET['statusPayment']) ? sanitize_text_field( $_GET['statusPayment'] ) : '';
		$transaction = isset($_GET['transaction']) ? sanitize_text_field( $_GET['transaction'] ) : '';
		unset($_GET['statusPayment']);
		unset($_GET['transaction']);

		if($temp_status == '0') {
			$_paymentStatus = array(
				'status' => 'success',
				'message' => 'Cám ơn Quý khách! Đơn hàng đã được hệ thống ghi nhận. Chúng tôi sẽ liên hệ với Quý khách để xác nhận thông tin đơn hàng trong vòng 24 tiếng và tiến hành giao hàng.'
			);

			session_destroy();
		}
		elseif($temp_status == '1') {
			$_paymentStatus = array(
				'status' => 'pending',
				'message' => 'Giao dịch đang được xử lý.'
			);
		}
	}
}

get_header();

// View all orders
if( !isset($_GET['order_code']) || esc_js($_GET['order_code']) == ''): ?>

<div id="message-payment" class="block-outer <?php echo ($_paid) ? $_paymentStatus['status']:'hidden'; ?>">
	<div class="container fluid clearfix">
		<p class="condensed"><?php echo ($_paid) ? $_paymentStatus['message']:''; ?></p>
		<span class="close">x</span>
	</div>
</div>

<div class="block-outer block-account block-account-orders">
	<div class="container fluid clearfix">
		<h2 class="title-cart">Đơn hàng</h2>
		<div class="block-inner clearfix">
			<div class="row clearfix">
				<div class="grid9 clearfix">
					<form action="<?php echo get_permalink(); ?>" method="GET" id="orders_filter"><?php $curdate = current_time('Y-m-d');
					$curdate_a = explode('-', $curdate); ?>
						<input type="hidden" name="view" value="orders">
						<div class="row clearfix">
							<h2>Tìm kiếm đơn hàng</h2>
							<div class="row clearfix">
								<label for="orders_id">Mã đơn hàng</label>
								<input type="text" id="orders_id" name="orders_id">
							</div>
							<div class="row clearfix">
								<label for="orders_time">Thời gian</label>
								<select name="orders_time" id="orders_time">
									<option value="">Chọn khoảng thời gian</option>
									<option value="1-tuan">1 tuần</option>
									<option value="2-tuan">2 tuần</option>
									<option value="1-thang">1 tháng</option>
									<option value="3-thang">3 tháng</option>
									<option value="custom" <?php echo (isset($_GET['orders_st']) || isset($_GET['orders_fn'])) ? 'selected':''; ?>>Tùy chọn</option>
								</select>
							</div>
							<div class="row clearfix">
								<label for="orders_st" style="width: 22%">Từ ngày</label>
								<input name="orders_st" id="orders_st" type="text" class="datepicker_tg" value="<?php echo isset($_GET['orders_st']) ? $_GET['orders_st']:''; ?>" style="width: 21%; margin-right: 1%">
								<label for="orders_fn" style="width: 7%">đến</label>
								<input name="orders_fn" id="orders_fn" type="text" class="datepicker_tg" value="<?php echo isset($_GET['orders_fn']) ? $_GET['orders_fn']:''; ?>" style="width: 21%; margin-right: 23%">
							</div>
							<div class="row clearfix">
								<label for="">&nbsp;</label>
								<input type="submit" class="btn btn-submit" value="Tìm">
							</div>
						</div>
					</form>
					<script type="text/javascript">
					jQuery(document).ready(function($) {
						$('.datepicker_tg').datepicker({dateFormat: 'yy-mm-dd'});

						var curdate = [<?php echo $curdate_a[0] ?>, <?php echo $curdate_a[1] ?>, <?php echo $curdate_a[2] ?>];
						var d_date_a;

						$('#orders_time').change(function() {
							var _this_val = $(this).val();
							if(_this_val == 'custom' || _this_val == '') {
								$('#orders_st').val('');
								$('#orders_fn').val('');
								return false;
							}
							var _this_val_a = _this_val.split('-');
							if(_this_val_a[1] == 'tuan') {
								var d_date = new Date(curdate[0], curdate[1]-1, (curdate[2]-(_this_val_a[0]*7)));
								d_date_a = [d_date.getFullYear(), d_date.getMonth()+1, d_date.getDate()];
							}
							else if(_this_val_a[1] == 'thang') {
								var d_date = new Date(curdate[0], curdate[1]-_this_val_a[0]-1, curdate[2]);
								d_date_a = [d_date.getFullYear(), d_date.getMonth()+1, d_date.getDate()];
							}
							$('#orders_st').val(d_date_a.join('-'));
							$('#orders_fn').val(curdate.join('-'));
							return false;
						});

						$('#orders_st, #orders_fn').each(function() {
							$(this).change(function(event) {
								$('#orders_time').find('option[value="custom"]').prop('selected', true);
							});
						});
					});
					</script>
				</div>
				<div class="grid3">
					<!-- <img nopin="nopin" src="<?php echo get_template_directory_uri(); ?>/images/donhang_03.png" alt=""> -->
				</div>
			</div>
			<div class="row grid12"><?php $u_id = get_current_user_id();
				$u_info = get_userdata($u_id);

				$crm_id = get_user_meta( $u_id, 'code_crm', true );
				if( !$crm_id )
					$crm_id = 'WEB'. str_pad($u_id, 9, '0', STR_PAD_LEFT);
				$card_id = get_user_meta($u_id, 'user_card', true);
				switch(strtoupper($card_id)){
					case '1':
					case 'SILVER':
						$card_id = 'Thẻ Bạc'; break;
					case '2': 
					case 'GOLD':
						$card_id = 'Thẻ Vàng'; break;
					case '3':
					case 'DIAMOND':
						$card_id = 'Thẻ Kim Cương'; break;
					default : $card_id = 'Thành viên';
				}
    			$card_number = get_user_meta($u_id, 'user_card_id', true); ?>
				<h2>Danh sách đơn hàng</h2>
				<p>
					<span><strong>Mã khách hàng:</strong> <?php echo $crm_id; ?></span>
					<span><strong>Mã thẻ:</strong> <?php echo $card_number; ?></span>
					<span><strong>Hạng thẻ:</strong> <?php echo $card_id; ?></span>
					<span class="fr">Doanh thu tích luỹ: <span class="price"><?php echo get_turnover_by_user($u_id); ?></span></span>
				</p>
				<table class="clearfix">
					<thead>
						<tr>
							<th><span>Mã đơn hàng</span></th>
							<th><span>Ngày đặt hàng</span></th>
							<th><span>Trạng thái</span></th>
							<th><span>Mặt hàng</span></th>
							<th><span>Thành tiền</span></th>
							<th><span>Mã bưu gửi</span></th>
                            <!-- <th width="14%">Đơn vị vận đơn</th> -->
						</tr>
					</thead>
					<tbody><?php $args = array(
						'posts_per_page' => -1,
						'post_type' => 'orders',
						'orderby' => 'date',
						'order' => 'DESC',
						'meta_query' => array(
							array(
								'key' => 'order_user_id',
								'value' => $u_id
							)
						)
					);
					if(isset($_GET['orders_id']) && $_GET['orders_id'] != '') {
						$args['meta_query'][] = array('key' => 'order_code', 'value' => $_GET['orders_id'], 'compare' => 'LIKE');
					}
					$args['date_query'][0] = array();
					if(isset($_GET['orders_st']) && $_GET['orders_st'] != '') {
						$args['date_query'][0]['after'] = $_GET['orders_st'];
					}
					if(isset($_GET['orders_fn']) && $_GET['orders_fn'] != '') {
						$args['date_query'][0]['before'] = $_GET['orders_fn'];
					}
					$orders = get_posts($args);
					if($orders):
						$i = 1;
						foreach ($orders as $o_key => $o):
						$class = ($i%2 == 1) ? '':'alternate';
						$o_id = $o->ID;
						$order_code = get_post_meta($o_id, 'order_code', true);
						$order_info = get_post_meta($o_id, 'order_info', true);
						$orders_products = (isset($order_info['products'])) ? $order_info['products']:array();
						$order_status = get_post_meta($o_id, 'order_status', true);
						$order_status_txt = '';
						switch($order_status){
							case 'neworder': $order_status_txt = 'Mới đặt hàng'; break;
							case 'pending': $order_status_txt = 'Đang xử lý tại kho'; break;
							case 'shipping': $order_status_txt = 'Đang giao hàng'; break;
							case 'not_available': $order_status_txt = 'Hết hàng'; break;
							case 'not_receiving': $order_status_txt = 'Khách hàng không nhận'; break;
							case 'return': $order_status_txt = 'Hàng trả về kho'; break;
							case 'cancel': $order_status_txt = 'Hủy đơn hàng'; break;
							case 'done': $order_status_txt = 'Đã hoàn thành'; break;
						}
						$order_payment_status = get_post_meta($o_id, 'order_payment_status', true);
				        // $order_payment_code = get_post_meta($o_id, 'order_payment_code', true);
				        // $order_payment_status_bool = ($order_payment_status == true && $order_payment_code) ? 1:0;
				        $order_payment_status_bool = ($order_payment_status == true) ? 1:0; ?>
						<tr class="<?php echo $class; ?>">
							<td data-order-id="<?php echo $o_id; ?>"><a href="<?php echo add_query_arg(array('order_code' => $order_code), get_permalink()); ?>" title="Xem chi tiết đơn hàng <?php echo $order_code; ?>"><?php echo $order_code; ?></a></td>
							<td><?php echo get_the_time('d/m/Y', $o_id); ?></td>
							<td><?php echo $order_status_txt; ?></td>
							<td><?php $n = 1;
							foreach ($orders_products as $op_key => $op) {
								echo '<a href="'. get_permalink((int) $op['id']) .'">'. get_post_meta((int) $op['id'], 'post_sku', true) .'</a> (size: '. $op['size'] .', số lượng: '. $op['quantity'] .')';
								if($n++ <= sizeof($orders_products)-1) echo ',<br>';
							} ?>&nbsp;</td>
							<td style="color:<?php echo ($order_payment_status_bool) ? '#00b':'red'; ?>"><?php echo aj_format_number(get_post_meta( $o_id, 'order_total', true )) .'VNĐ';?></td>
							<td>
								<?php
								$link_delivery = '';
								$name_delivery = get_post_meta( $o_id, 'transport_name', true );
								if($name_delivery == '2'){
									$link_delivery = 'https://5sao.ghn.vn/Tracking/ViewTracking/'.get_post_meta( $o_id, 'transport_code', true );
								}
								else {//if( $name_delivery == '1'){
									$link_delivery = 'http://www.vnpost.vn/vi-vn/dinh-vi/buu-pham?key='.get_post_meta( $o_id, 'transport_code', true );
								}
								?>
                                <a target="_blank" href="<?=$link_delivery?>">
                                    <?php echo get_post_meta( $o_id, 'transport_code', true ); ?>
                                </a>
                            </td>
                           <!--  <td><a target="_blank" href="https://5sao.ghn.vn/Tracking/ViewTracking/<?php echo get_post_meta( $o_id, 'transport_name', true ); ?>">
                                    <?php echo get_post_meta( $o_id, 'transport_name', true ); ?>
                                </a></td> -->
						</tr>
						<?php $i++; endforeach; 
					else:
						echo '<tr><td colspan="5" style="text-align: center">Không có đơn hàng nào!</td></tr>';
					endif; ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<?php else: ?>

<div class="block-outer block-account block-account-orders">
	<div class="container fluid clearfix order-check-page">
		<div class="block-title-outer"><h1 class="block-title block-title-inner condensed"><span>Đơn hàng</span></h1></div>
		<div class="block-inner clearfix">
			<div class="row grid12"><?php $u_id = get_current_user_id();
				$u_info = get_userdata($u_id); ?>
				<h2>Đơn hàng chi tiết</h2>
				
				<div class="row clearfix"><?php $args = array(
						'posts_per_page' => -1,
						'post_type' => 'orders',
						'orderby' => 'date',
						'order' => 'DESC',
						'meta_query' => array(
							'relation' => 'AND',
							array(
								'key' => 'order_code',
								'value' => $_GET['order_code']
							),
						)
					);

					if( $u_id > 0 ) {
						$args[] = array(
							'key' => 'order_user_id',
							'value' => $u_id
						);
					}
					
					$orders = get_posts($args);
					if($orders):
						$i = 1;
						foreach ($orders as $o_key => $o):
							$o_id = $o->ID;
							$order_info = get_post_meta($o_id, 'order_info', true);
							if(empty($order_info)) {
								echo 'Đơn hàng bị huỷ do bị xoá hết sản phẩm!';
								break;
							}
							$orders_products = (isset($order_info['products'])) ? $order_info['products']:array();
							$order_status = get_post_meta($o_id, 'order_status', true);
							$order_status_txt = '';
							switch($order_status){
								case 'neworder': $order_status_txt = 'Mới đặt hàng'; break;
								case 'pending': $order_status_txt = 'Đang xử lý tại kho'; break;
								case 'shipping': $order_status_txt = 'Đang giao hàng'; break;
								case 'not_available': $order_status_txt = 'Hết hàng'; break;
								case 'not_receiving': $order_status_txt = 'Khách hàng không nhận'; break;
								case 'return': $order_status_txt = 'Hàng trả về kho'; break;
								case 'cancel': $order_status_txt = 'Hủy đơn hàng'; break;
								case 'done': $order_status_txt = 'Đã hoàn thành'; break;
							}
							$order_payment_status = get_post_meta($o_id, 'order_payment_status', true);
							$order_payment_status_bool = ($order_payment_status == true) ? 1:0;
							$order_total = get_post_meta( $o_id, 'order_total', true );

							$campaign_influence = get_post_meta($o_id, 'campaign_influence', true);
					        if($campaign_influence == '' || $campaign_influence == '0')
					            $campaign_influence = false;
					        else
					            $campaign_influence = true;
					        $campaignInfo = get_post_meta($o_id, 'campaignInfo', true);

					        $hasCoupon = get_post_meta($o_id, 'hasCoupon', true);
					        $couponName = get_post_meta($o_id, 'couponName', true);
					        $couponSaleoffValue = get_post_meta($o_id, 'couponSaleoffValue', true);

					        $isVip = get_post_meta($o_id, 'isVip', true);
					        $vipAfter = get_post_meta($o_id, 'vipAfter', true); ?>

							<p>Mã đơn hàng: <strong><?php echo $_GET['order_code']; ?></strong></p>
							<p>Trạng thái đơn hàng: <strong><?php echo $order_status_txt; ?></strong></p>

                                <?php
                                global $wpdb;
                                $query = "SELECT * FROM `khatoco_order_status` WHERE `order_id` = $o_id GROUP BY `order_status`";
                                $result = $wpdb->get_results($query . " ORDER BY `created_date` DESC");
                                if ($result)
                                {
                                ?>
                                    <p>Chi tiết trạng thái:</p>
                                    <p>
                                    <table class="form_inner_table" id="products_table">
                                                <thead>
                                                <tr>
                                                    <th style="width: 200px">Ngày - Giờ</th>
                                                    <th>Mô Tả</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                foreach($result as $key => $value){
                                                    ?>
                                                    <tr>
                                                        <td><?php echo get_date_from_gmt( date( 'Y-m-d H:i:s', strtotime($value->created_date) ), 'd-m-Y - H:i:s' );?></td>
                                                        <td><?php
                                                            switch ($value->order_status) {
                                                                case 'neworder':
                                                                    echo 'Mới đặt hàng' ;
                                                                    break;
                                                                case 'verify':
                                                                    echo 'Đã xác nhận';
                                                                    break;
                                                                case 'pending':
                                                                    echo 'Đang xử lý tại kho';
                                                                    break;
                                                                case 'shipping':
                                                                    echo 'Đang giao hàng';
                                                                    break;
                                                                case 'not_available':
                                                                    echo 'Hết hàng';
                                                                    break;
                                                                case 'not_receiving':
                                                                    echo 'Khách hàng không nhận';
                                                                    break;
                                                                case 'return':
                                                                    echo 'Hàng trả về kho';
                                                                    break;
                                                                case 'cancel':
                                                                    echo 'Hủy đơn hàng';
                                                                    break;
                                                                case 'done':
                                                                    echo 'Đã hoàn thành';
                                                                    break;
                                                            }
                                                            ?>
                                                        </td>
                                                    </tr>
                                                <?php
                                                }
                                                ?>
                                                </tbody>
                                            </table>
                                        </p>
                                    </p>
                                <?php
                                }
                                ?>
							<p>Giá trị đơn hàng: <strong><?php echo aj_format_number($order_total) .'VNĐ'; ?></strong></p>
							<?php if($campaign_influence): ?>
				            <p>Chương trình khuyến mãi:
								<fieldset> Giảm giá: <?php echo aj_format_number($campaignInfo['decrease']).' Đ';?></fieldset>
								<?php if (!empty($campaignInfo['listBonus']['listProdBonus'])) : ?>
								<fieldset> Danh sách tặng phẩm:</fieldset>
								<?php foreach($campaignInfo['listBonus']['listProdBonus'] as $bonus) :
										if($bonus['quantity']>0):?>
								<fieldset> + <?php echo $bonus['post_title'] ;?> <span><?php echo '['.$bonus['size'].'] '. $bonus['quantity'] . ' x 0 Đ'; ?></span></fieldset>
								<?php endif;
										endforeach; ?>

								<?php endif; ?>

								<?php if (!empty($campaignInfo['listSpecialPrice']['listProdBonus'])) : ?>
								<fieldset> Danh sách sản phẩm mua giá ưu đãi:</fieldset>
								<?php foreach($campaignInfo['listSpecialPrice']['listProdBonus'] as $bonus) :
										if($bonus['quantity']>0):?>
								<fieldset> + <?php echo $bonus['post_title'] ;?> <span><?php echo '['.$bonus['size'].'] '. $bonus['quantity'] . ' x ' . aj_format_number($bonus['price']) .' Đ'; ?></span></fieldset>
								<?php endif;
										endforeach; ?>

								<?php endif; ?>

							</p>
				            <?php endif; ?>

				            <?php if($isVip): ?>
				            <p>Thẻ thành viên: <strong><?php echo (1-$vipAfter)*100 .'%';?></strong></p>
				            <?php endif; ?>

				            <?php if($hasCoupon): ?>
				            <p>Coupon: <strong><?php echo $couponName . ' (giảm giá '. aj_format_number($couponSaleoffValue) .'VNĐ)';?></strong></p>
				            <?php endif; ?>

							<table>
								<thead>
									<tr>
										<th width="30px">STT</th>
										<th>Tên</th>
										<th>Màu</th>
										<th>Kích thước</th>
										<th>Số lượng</th>
										<th width="100px">Đơn giá</th>
									</tr>
								</thead>
								<tbody><?php $number = 0;
								foreach($orders_products as $pkey => $p):
									$number++; ?>
									<tr class="each-product" data-product-id="<?php echo $p['id']; ?>">
										<td><?php echo $number; ?></td>
										<td><a href="<?php echo get_permalink($p['id']); ?>" target="_blank"><?php echo $p['name']; ?></a></td>
										<td>
											<span style="background: <?php echo $p['color_code']; ?>; display: inline-block; width: 30px; height: 30px;"></span>
											<br /><?php echo $p['color_name']; ?>
										</td>
										<td><?php echo $p['size']; ?></td>
										<td><?php echo $p['quantity']; ?></td>
										<td><?php if(isset($p['price_formated'])) echo $p['price_formated']; else echo aj_format_number($p['price_saleoff']['value']).' Đ'; ?></td>
									</tr>
								<?php endforeach; ?>
								</tbody>
							</table>
							<?php if( $order_status == 'neworder' && !$order_payment_status_bool ): ?>
							<p id="message_return"></p>
							<p><a class="fr" href="#cancel_order" id="cancel_order" style="color: #666;">Huỷ đơn hàng</a></p>
							<script>
							jQuery(document).ready(function($) {
								$('#cancel_order').click(function(event) {
									event.preventDefault();
									if(confirm('Bạn có chắc chắn muốn huỷ đơn hàng này?'))
										$.ajax({
											url: '<?php echo admin_url("admin-ajax.php") ?>',
											type: 'POST',
											dataType: 'json',
											data: {
												action: 'cancel_order',
												id: <?php echo $o_id; ?>
											},
										})
										.done(function(response) {
											if( response.result == 'true' )
												window.location.replace('<?php echo get_permalink(); ?>');
											else{
												$('#message_return').text( response.message );
												$(this).remove();
											}
										});

									return false;
								});
							});
							</script>
							<?php endif; ?>
						<?php $i++; endforeach; 
					else:
						echo '<tr><td colspan="5" style="text-align: center">Mã đơn hàng không tồn tại! <a href="'. get_permalink() .'">Quay lại trang Danh sách đơn hàng!</a></td></tr>';
					endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>

<?php endif;
get_footer(); ?>
