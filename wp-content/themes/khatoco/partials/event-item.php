<?php $event =  $args['event']; ?>
<div class="col-xs-12 col-sm-6 col-lg-4">
    <div class="group-event">
        <div class="image-side">
            <!--                                <img class="img-fluid" src="--><?php //echo $event['feature_image'] ?><!--" alt="--><?php //echo $event['title'] ?><!--">-->
            <img class="img-fluid" src="<?php echo get_template_directory_uri()."/images/new/test-image.png" ?>" alt="<?php echo $event['title'] ?>">
        </div>
        <div class="content-side">
            <a href="<?php echo $event['link'] ?>">
                <p class="name"><?php echo $event['title'] ?></p>
            </a>
            <p class="description"><?php echo $event['short_description'] ?></p>
        </div>
    </div>
</div>
