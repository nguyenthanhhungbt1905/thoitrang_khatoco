<div class="wrapper-related-container">
    <div class="related-absolute">
        <section id="related-post">

            <div class="row">
                <div class="col-xs-12">
                    <h3 class="title">
                        Tin tức liên quan
                    </h3>
                    <div class="break-line"></div>
                </div>
                <div class="col-xs-12">
                    <div class="row scroll-related">
                        <div class="col-xs-12">
                            <?php
                            $posts = $args['post'];
                            foreach($posts as $post){?>
                                <div class="related-item">
                                    <div class="image-side">
                                        <img class="img-fluid" src="<?php echo get_template_directory_uri()."/images/new/test-image.png"?>" alt="">
                                    </div>
                                    <div class="content-side">
                                        <a href="<?php echo $post['link'] ?>">
                                        <p class="name">
                                            <?php echo $post['title'] ?>
                                        </p>
                                        </a>
                                        <p class="description">
                                            <?php echo $post['short_description'] ?>
                                        </p>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
