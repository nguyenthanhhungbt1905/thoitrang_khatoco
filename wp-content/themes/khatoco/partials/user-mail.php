<?php if(get_current_user_id() == 0) {
	wp_redirect(home_url('/'));
	exit();
} ?>
<div class="block-outer block-account block-account-orders">
	<div class="container fluid clearfix" data-ng-app="appMail">
		<div class="block-title-outer"><h1 class="block-title block-title-inner condensed"><span>Hộp thư</span></h1></div>
		<div class="block-inner clearfix" data-ng-controller="MailController">
			<div class="row clearfix">
				<div class="grid3">
					<a href="javascript:;" class="create-mail" data-ng-click="createMail()">Viết mail</a>
				</div>
				<div class="grid9">
					<div class="mail-lists-title" data-ng-show="mailList()">
						<input type="checkbox" id="" class="fl" data-ng-change="selectAll()" data-ng-model="selectAll">
						<button data-ng-click="deleteMails()" class="fl">Xoá</button>
						<ul class="list-style-none fr mail-paginate clearfix">
							{{mailPaginate}}
						</ul>
					</div>
					<div class="mail-content-title" data-ng-show="mailContent()">
						<p>{{mail_single.title}}</p>
					</div>
				</div>
			</div>
			<div class="row clearfix">
				<div class="grid3">
					<ul class="list-style-none clearfix">
						<li><a href="javascript:;" data-ng-click="getInbox()">Thư đến</a></li>
						<li><a href="javascript:;" data-ng-click="getOutbox()">Thư đi</a></li>
						<li><a href="javascript:;" data-ng-click="getDraft()">Thùng rác</a></li>
					</ul>
				</div>
				<div class="grid9">
					<ul class="list-style-none mail-lists clearfix" data-ng-show="mailList()">
						<!-- <li data-ng-repeat="mail in mails">
							<input type="checkbox" name="" id="">
						</li> -->
					</ul>
					<div class="mail-content clearfix">
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
	var app = angular.module('appMail', []);
	
	app.service('MailServices', ['$http', '$q', '$timeout', function($http, $q, $timeout){
		var getInboxService = function(off){
			if(typeof off == 'undefined') off = 0;
			return $http({
				method: 'GET',
				url: ajaxurl,
				params: {action: 'mail_service_ajax', do_ajax: 'get_inbox', offset: off}
			});
		};

		var getOutboxService = function(off){
			if(typeof off == 'undefined') off = 0;
			return $http({
				method: 'GET',
				url: ajaxurl,
				params: {action: 'mail_service_ajax', do_ajax: 'get_outbox', offset: off}
			});
		};

		var getDraftService = function(off){
			if(typeof off == 'undefined') off = 0;
			return $http({
				method: 'GET',
				url: ajaxurl,
				params: {action: 'mail_service_ajax', do_ajax: 'get_draft', offset: off}
			});
		};

		var sendMailService = function(subject, content){
			return $http({
				method: 'GET',
				url: ajaxurl,
				params: {action: 'mail_service_ajax', do_ajax: 'send_mail'}
			});
		};

		var deleteMailsService = function(mail){
			return $http({
				method: 'GET',
				url: ajaxurl,
				params: {action: 'mail_service_ajax', do_ajax: 'delete_mails', list: mail}
			});
		};

		return {
			getInboxService: getInboxService,
			getOutboxService: getOutboxService,
			getDraftService: getDraftService,
			sendMailService: sendMailService,
			deleteMailsService: deleteMailsService
		};
	}]);
	
	app.controller('MailController', ['$scope', '$sce', 'MailServices' function($scope, $sce, MailServices){
	}])
</script>