<?php $menu =  $args['data'];

?>
<div class="row">
    <div class="col-12">
        <ul class="breadcrumbs-list">
            <?php foreach($menu as $key => $item){ ?>
                <li class="breadcrumb-item">
                    <?php if($item['link'] != "#"){ ?>
                    <a href="<?php echo $item['link'] ?>">
                    <?php echo $item['name'];  ?>
                    </a>
                    <?php }else{
                        echo $item['name'];
                    }?>
                </li>
            <?php  } ?>
            </ul>
    </div>
</div>