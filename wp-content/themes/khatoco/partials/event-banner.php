<section id="banner-winter"
         style="background-image: url('<?php echo get_template_directory_uri() . '/images/new/banner-winter.jpg' ?>')">
    <div class="group-text">
        <div class="item-left">
            <p class="text">
                CHRIST-
                <br/>MAS
                <br/>SALE
            </p>
            <p class="text">
                20/11 - 12/12
            </p>
        </div>
        <div class="item-right">
            <a href="/" class="see-more">XEM THÊM</a>
        </div>
    </div>
</section>