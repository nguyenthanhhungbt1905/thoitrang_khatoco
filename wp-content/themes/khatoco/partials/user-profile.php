<?php
$user_info = wp_get_current_user();
//echo "<pre>";
//
//print_r($user_info);

$check_card = 0; $attr_check_card = '';
if(isset($user_info->ID) && !empty($user_info->ID)){
    $querystr = "
    SELECT khatoco_usermeta.*
    FROM khatoco_usermeta
    WHERE khatoco_usermeta.meta_key = 'code_crm' &&  khatoco_usermeta.meta_value != '' &&  khatoco_usermeta.user_id = ".$user_info->ID;

    $getcard = $wpdb->get_results($querystr, OBJECT);
    if($getcard){
        $check_card = 1;
        $attr_check_card = 'readonly disabled=""';
    }
}




if(!isset($user_info->ID)){
	wp_redirect(get_page_link(169));
	exit;
}

/*
Template Name: Trang cá nhân - Thông tin cá nhân
*/
get_header();

$verify_text = '';
if(isset($_GET['verify'])){
	$verify = sanitize_text_field( $_GET['verify'] );
	switch ($verify) {
		case 'login_necessary':
			$verify_text = 'Yêu cầu đăng nhập để xác nhận tài khoản!';
			break;

		case 'email_unavailable':
			$verify_text = 'Địa chỉ email không tồn tại hoặc không đúng!';
			break;

		case 'success':
			$verify_text = 'Bạn đã xác nhận tài khoản thành công. Hãy xem lại thông tin dưới đây.';
			break;

		case 'failed':
			$verify_text = 'Bạn đã xác nhận sai. Nhấn vào đây để gửi lại mail xác nhận tài khoản.';
			break;
	}
}
if(!is_user_logged_in() && empty($verify_text)){
	wp_redirect( home_url( '/dang-nhap' ), 301 );
}
?>

<?php /*

$code_crm = get_user_meta( get_current_user_id(), 'code_crm', true );

echo ( $code_crm != '' ) ? $code_crm : 'WEB'. str_pad( get_current_user_id(), 9, '0', STR_PAD_LEFT ); ?>

<p><button id="update_test" class="button">Update thông tin</button></p>
<div id="result_test"></div>

<script type="text/javascript">
jQuery(document).ready(function($) {
	$('#update_test').click(function(event) {
		event.preventDefault();
		$.ajax({
			url: '<?php echo admin_url("admin-ajax.php"); ?>',
			type: 'GET',
			dataType: 'html',
			data: {
				action: 'backend__update_test_user',
				id: <?php echo get_current_user_id(); ?>
			}
		})
		.done(function( response ) {
			$('#result_test').html( response );
		});
	});
});
</script>
<?php */ ?>

<div data-ng-app="khatocoUserProfile" class="block-outer block-account logined">
	<div class="block-inner">
	<div data-ng-controller="UserController" class="container fluid">		
        <div class="col-xs-12">
        	<h2 class="page-title">Thông tin cá nhân</h2>
        	<?php if(!empty($verify_text)) echo '<p><strong>'.$verify_text.'</strong></p>'; ?>	
        	<p class="description">Thông tin cá nhân Quý khách nhập dưới đây có thể được sử dụng cho chương trình Thẻ thành viên Khatoco. 
        	Để biết thêm thông tin về chương trình, vui lòng xem thêm <a href="<?php echo get_page_link(1057); ?>" target="blank">tại đây</a>.</p>
        </div>
		<div class="block-inner clearfix">
			<form method="POST" data-ng-submit="formSubmit()">
				<!-- Verify text message -->
				<div class="col-md-6 col-xs-12">

				<!-- <p class="card clearfix">
					<label class="grid6" for="card-on"><input type="radio" name="card" id="card-on" data-ng-checked="has_card" disabled="" />Khách hàng có thẻ</label>
					<label class="grid6" for="card-off"><input type="radio" name="card" id="card-off" data-ng-checked="!has_card" disabled="" />Khách hàng chưa có thẻ</label>
				</p>
				<p class="clearfix" data-ng-show="has_card">{{card_text}}</p> -->
				
				<h2>Thông tin tài khoản</h2>
				<p class="clearfix">
					<label for="username">Tài khoản <span class="required">*</span></label>
					<input class="field" data-ng-model="datapost.username" value="" type="text" required="" disabled="" />
				</p>
				<p class="clearfix">
					<label for="fullname">Họ và tên <span class="required">*</span></label>
					<input class="field" data-ng-model="datapost.fullname" value="" type="text" placeholder="Họ và Tên" required="" <?php echo $attr_check_card;?> />
				</p>
				<p class="clearfix">
					<label for="email">Email <span class="required">*</span></label>
					<input class="field" data-ng-model="datapost.email" value="" required="" disabled="" />
				</p>

				<label for="birthday">Ngày sinh <span class="required">*</span></label>		
				<p class="birthday">			
					<select name="" data-ng-model="datapost.birthday.day" required="" <?php echo $attr_check_card;?> >
						<option value="">Ngày</option>
						<option data-ng-repeat="day in days" data-ng-selected="datapost.birthday.day==day" value="{{day}}">{{day}}</option>
					</select>
				</p>
				<p class="birthday">					
					<select name="" data-ng-model="datapost.birthday.month" required="" <?php echo $attr_check_card;?> >
						<option value="">Tháng</option>
						<option data-ng-repeat="month in months" data-ng-selected="datapost.birthday.month==month" value="{{month}}">{{month}}</option>
					</select>
				</p>					
				<p class="birthday last">
					<select name="" data-ng-model="datapost.birthday.year" required="" <?php echo $attr_check_card;?> >
						<option value="">Năm</option>
						<option data-ng-repeat="year in years" data-ng-selected="datapost.birthday.year==year" value="{{year}}">{{year}}</option>
					</select>
					<?php /* ?><input class="field" data-ng-model="datapost.birthday" value="" type="text" required /><?php */ ?>
				</p>
				<p class="clearfix">
					<label for="gender">Giới tính <span class="required">*</span></label>
					<select data-ng-model="datapost.gender" data-ng-options="gen as gen.name for gen in genders" id="gender" required="" <?php echo $attr_check_card;?> >
						<option value="">--Lựa chọn--</option>
					</select>
				</p>
				<p class="clearfix">
					<label for="phone">Số điện thoại <span class="required">*</span></label>
					<input class="field" data-ng-model="datapost.phone" value="" type="text" pattern="\d*" required="" placeholder="Điện thoại | (09xxxxxxxx) hoặc (01xxxxxxxxx)" <?php echo $attr_check_card;?> />
				</p>
				</div>
				<div class="col-md-6 col-xs-12">
				<h2>Thay đổi mật khẩu</h2>
				<p class="clearfix">
					<label for="password">Mật khẩu </label>
					<input class="field" data-ng-model="datapost.password" value="" placeholder="Mật khẩu" type="password"/>
				</p>
				<p class="clearfix">
					<label for="repassword">Nhập lại mật khẩu </label>
					<input class="field" data-ng-model="datapost.repassword" value="" placeholder="Xác nhận mật khẩu" type="password"/>
				</p>
				<h2 class="sub-title">Địa chỉ nhận hàng</h2>
				<p class="birthday">
					<label for="province">Tỉnh/TP</label>
					<select data-ng-model="datapost.province" data-ng-options="province as province.name for province in provinces" data-ng-change="setCurrentProvince()" name="province" id="province" class="address-select" >
						<option value="">-- Lựa chọn --</option>
					</select>
				</p>
				<p class="birthday">
					<label for="district" data-ng-show="datapost.province.provinceid">Quận/Huyện</label>
					<select data-ng-model="datapost.district" class="birthday" data-ng-show="datapost.province.provinceid" data-ng-options="district as district.name for district in districts" data-ng-change="setCurrentDistrict()" name="district" id="district" class="address-select" >
						<option value="">-- Lựa chọn --</option>
					</select>
				</p>
				<p class="birthday last">
					<label for="ward" data-ng-show="datapost.district.districtid && datapost.province.provinceid">Phường/Xã</label>
					<select data-ng-model="datapost.ward" class="birthday" data-ng-show="datapost.district.districtid && datapost.province.provinceid"data-ng-options="ward as ward.name for ward in wards" data-ng-change="setCurrentWard()" name="ward" id="ward" class="address-select">
						<option value="">-- Lựa chọn --</option>
					</select>
				</p>
				<p class="clearfix">
					<label for="addr">Tên đường, số nhà</label>
					<input class="field" data-ng-model="datapost.address" value="" type="text" placeholder="Tên đường, số nhà"/>
				</p>
				<div ng-show="has_card">
					<h2>Thông tin thẻ thành viên Khatoco</h2>
					<p class="clearfix">
						<label for="">Mã thẻ</label>
						<input class="field" data-ng-model="datapost.card_id" value="" readonly required="" disabled=""  />
					</p>
					<p class="clearfix">
						<label for="">Loại thẻ</label>
						<span>{{card_text}}</span>
					</p>
					<!-- <p class="clearfix">
						<input type="checkbox" name="" value="" id="card_change" ng-model="datapost.requests.change" style="width: auto; margin: 5px;">
						<label for="card_change" style="width: 60%;">Thay đổi thông tin thẻ </label>
					</p> -->
					<!-- <p class="clearfix">
						<input type="checkbox" name="" value="" id="card_remove" ng-model="datapost.requests.remove" style="width: auto; margin: 5px;">
						<label for="card_remove" style="width: 60%;">Huỷ thẻ </label>
					</p> -->
				</div>

				<!-- <div ng-show="!has_card">
					<h2>Đăng ký thẻ thành viên Khatoco</h2>
					<p class="clearfix">
						<input type="checkbox" name="" value="" id="card_register" ng-model="datapost.requests.register" style="width: auto; margin: 5px;">
						<label for="card_register" style="width: 60%;">Đăng ký thẻ thành viên </label>
					</p>
				</div> -->

				<p>
					<label for="">&nbsp;</label>
					<button class="button" type="submit">{{submitButton}}</button>
					<input type="hidden" name="redirect" value="<?php echo get_page_link(169); ?>" /> 
					<input type="hidden" name="action" value="form_profile_ajax" /> 
					<input type="hidden" id="register_nonce_field" name="register_nonce_field" value="<?php echo wp_create_nonce('register_nonce_action'); ?>" />
					<input type="hidden" name="_wp_http_referer" value="<?php echo get_permalink(); ?>" />
				</p>
				<p class="form-status" data-ng-show="submitMessage">{{submitMessage}}</p>
				</div>
			</form>
		</div>
	</div>
</div>
</div>

<script type="text/javascript">

	var apiUrl = "<?php echo admin_url('admin-ajax.php'); ?>";
	var app = angular.module("khatocoUserProfile", []);

	app.service('userService', ['$http', '$q', '$timeout', function($http, $q, $timeout){

		var getAddressInfo = function(action, id){
			return $http({
				method: 'GET',
				url: apiUrl,
				params: {action: 'user_service_ajax', do_ajax: action, id: id}
			});
		};

		var getUserInfo = function(){
			return $http({
				method: 'GET',
				url: apiUrl,
				params: {action: 'user_service_ajax', do_ajax: 'get_user_info'}
			});
		};

		var register_member_card = function(data){
			return $http({
				method: 'POST',
				url: apiUrl,
				params: {action: 'user_service_ajax', do_ajax: 'register_member_card', data: data}
			});
		};

		var verify_otp_code = function(otp_code){
			return $http({
				method: 'POST',
				url: apiUrl,
				params: {action: 'user_service_ajax', do_ajax: 'verify_otp_code', otp_code: otp_code}
			});
		};

		var saveUserInfo = function(data){
			var d = $q.defer();

			$timeout(function () {
				$http({
					method: 'POST',
					url: apiUrl,
					params: {action: 'user_service_ajax', do_ajax: 'save_user_info'},
					data: data,
					//headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'}
				}).success(function(data, status, headers){
					d.resolve(data);
				}).error(function(data, status, headers){
					d.reject(data);
				});
			}, 1000);
		
			return d.promise;
		};

		return {
			getAddressInfo: getAddressInfo,
			getUserInfo: getUserInfo,
			saveUserInfo: saveUserInfo,
			register_member_card: register_member_card,
			verify_otp_code: verify_otp_code
		};
	}]);

	app.controller('UserController', ['$scope', '$sce', 'userService', function($scope, $sce, userService){
		
		$scope.submitButton = 'Cập nhật';
		$scope.submitMessage = '';

		$scope.wards = [];
		$scope.districts = [];
		$scope.provinces = [];
		$scope.datapost = {};
		$scope.years = [2010,2009,2008,2007,2006,2005,2004,2003,2002,2001,2000,1999,1998,1997,1996,1995,1994,1993,1992,1991,1990,1989,1988,1987,1986,1985,1984,1983,1982,1981,1980,1979,1978,1977,1976,1975,1974,1973,1972,1971,1970,1969,1968,1967,1966,1965,1964,1963,1962,1961,1960,1959,1958,1957,1956,1955,1954,1953,1952,1951,1950,1949,1948,1947,1946,1945,1944,1943,1942,1941,1940,1939,1938,1937,1936,1935,1934,1933,1932,1931,1930];
		$scope.months = [1,2,3,4,5,6,7,8,9,10,11,12];
		$scope.days = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31];

		$scope.genders = [{id: 'male', name: 'Nam'}, {id: 'female', name: 'Nữ'}];

		$scope.datapost.province = {};
		$scope.datapost.district = {};
		$scope.datapost.ward = {};
		$scope.datapost.gender = {};
		$scope.datapost.birthday = {};
		$scope.datapost.address = '';
		$scope.datapost.phone = '';
		$scope.datapost.nonce = '<?php echo wp_create_nonce("profile_nonce_action"); ?>';

		$scope.datapost.card = 1;
		$scope.card_text = '';
		
		$scope.userInfo = {};
		$scope.phoneMessage = '';

		$scope.error = false;

		$scope.getWards = function(districtid){
			userService.getAddressInfo('get_wards', districtid)
				.success(function(data, status, headers){
					if(data.success == 'true'){
						$scope.reloadSelectBox('#ward');
						$scope.wards = $scope.objectToArray(data.result);
						if(data.current){
							$scope.datapost.ward = data.result[data.current]
						}
					}
				})
				.error(function(data, status, headers){
					$scope.error = true;
				});
		};

		$scope.getDistricts = function(provinceid){
			userService.getAddressInfo('get_districts', provinceid)
				.success(function(data, status, headers){ //console.log(data);
					if(data.success == 'true'){
						$scope.reloadSelectBox('#district');
						$scope.districts = $scope.objectToArray(data.result);
						if(data.current){
							$scope.datapost.district = data.result[data.current];
							$scope.getWards(data.current);
						}
					}
				})
				.error(function(data, status, headers){
					$scope.error = true;
				});
		};

		$scope.getProvinces = function(){
			userService.getAddressInfo('get_provinces', 0)
				.success(function(data, status, headers){
					if(data.success == 'true'){ //console.log(data.result);
						$scope.provinces = $scope.objectToArray(data.result);
						if(data.current){
							$scope.datapost.province = data.result[data.current]
							$scope.getDistricts(data.current);
						}
					}
				})
				.error(function(data, status, headers){
					$scope.error = true;
				});
		};

		$scope.getUserInfo = function(){
			userService.getUserInfo()
				.success(function(data, status, headers){
					if(data.success == 'true'){
						$scope.datapost.username = data.result.username;
						$scope.datapost.fullname = data.result.fullname;
						$scope.datapost.email = data.result.email;
						$scope.datapost.birthday = data.result.birthday;
						$scope.datapost.address = data.result.address;
						$scope.datapost.phone = data.result.phone;
						$scope.datapost.card = data.result.card;
						$scope.datapost.card_id = data.result.card_id;

						switch($scope.datapost.card){
							case '1':
							case 'SILVER':
								$scope.card_text = 'Thẻ Bạc'; break;
							case '2':
							case 'GOLD':
								$scope.card_text = 'Thẻ Vàng'; break;
							case '3':
							case 'DIAMOND':
								$scope.card_text = 'Thẻ Kim Cương'; break;
							default : $scope.card_text = '';
						}
						
						if($scope.datapost.card == '0')
							$scope.has_card = 0;
						else 
							$scope.has_card = 1;

						for(genkey in $scope.genders){
							if(data.result.gender['id'] == $scope.genders[genkey]['id']){
								$scope.datapost.gender = $scope.genders[genkey];
							}
						}
					}
				})
				.error(function(data, status, headers){
					$scope.error = true;
				});
		};

		$scope.getProvinces();
		$scope.getUserInfo();

		if($scope.error == true){
			alert('Không thể kết nối đến server, vui lòng F5 và thử lại!'); return;
		}
		
		$scope.formSubmit = function(){

			var tmpSubmitButton = $scope.submitButton;
			$scope.submitButton = $scope.submitButton+'...'
			$scope.submitMessage = '';

			if($scope.datapost.password){
				if($scope.datapost.password != $scope.datapost.repassword){
					$scope.submitMessage = 'Mật khẩu xác nhận không trùng khớp.';
					$scope.datapost.password = '';
					return;
				}
			}

			userService.saveUserInfo($scope.datapost)
				.then(function(data){
					if(data.success == 'true'){

					}
					if(data.message){
						$scope.submitMessage = data.message;
					}
					$scope.submitButton = tmpSubmitButton;
				}, function(err){
					$scope.submitMessage = 'Không thể kết nối đến máy chủ, F5 để thử lại.';
					$scope.submitButton = tmpSubmitButton;
				});
		};

		$scope.register_member_card = function(id){
			userService.register_member_card($scope.datapost)
				.success(function(data, status, headers){
					console.log(data.result);
				})
				.error(function(data, status, headers){
					
				});
		};

		$scope.reloadSelectBox = function(id){

		};
		
		$scope.setCurrentProvince = function(){
			//console.log($scope.currentProvince);
			$scope.getDistricts($scope.datapost.province.provinceid);
		};

		$scope.setCurrentDistrict = function(){
			$scope.getWards($scope.datapost.district.districtid);
		};

		$scope.setCurrentWard = function(){

		};

		$scope.objectToArray = function(object){
			var result = [];
			for(ob in object){
				result.push(object[ob]);
			}
			return result;
		};

	}]);
</script>
<?php get_footer(); ?>