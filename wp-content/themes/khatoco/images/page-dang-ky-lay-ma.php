<?php 
/*
Template Name: Page Đăng ký lấy mã
*/
?>
<?php get_header() ?>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/dncss/vendors/bootstrap/bootstrap.min.css" crossorigin="anonymous">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/dncss/css/style.css" crossorigin="anonymous">
<!-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous"> -->
<style type="text/css">
    #footer{
        margin-top: 0!important;
    }
    select,input{
        color: black;
    }
    .display{
        display: none!important;
    }
    .form-data{
        margin-left: 80px
    }

    .no-wrap {
      white-space: nowrap;
    }
    .card-gold > .img-product,
    .card-silver > .img-product,
    .card-dimond > .img-product {
      margin: 0 auto;
    }
    .tmg-ngay {
      display: block;
      clear: both;
      width: 100%;
      margin-bottom: 20px;
    }
    .promo .mt-0 {
      margin-top: 0;
    }
    .text-content-mid {
      font-size: 55px;
      font-family: sans-serif;
    }
    .text-content-campaign {
      font-size: 45px;
    }
    .content {
      margin-top: 110px;

    }
    .content-campaign {
        padding-left: 264px;
        margin: 10px;
    }

    .popup .popup-inner-custom {
        position: relative;
        z-index: 9999;
        padding: 3%;
        background-color: #fff;
        max-width: 400px;
        width: 80%;
        margin: 11% auto;
    }

    @media screen and (max-width: 768px) {
        .form-data{
            margin-left: 0px;
        }

        .content-campaign {
        padding-left: 0;
      }
      .text-35 {
        max-width: 100%;
      }
      .promo {
        padding: 0 10px;
      }
      .input-number {
        padding-right: 0;
      }
      .padding-0 {
        padding: 0;
      }
      .content {
        margin-top: 0;
      }
      .ml-5 {
        margin-left: 0 !important;
      }
        .popup .popup-inner-custom {
            position: relative;
            z-index: 9999;
            padding: 9%;
            background-color: #fff;
            max-width: 400px;
            width: 90%;
            margin: 37% auto;
        }

        .popup .popup-inner-custom .popup-message p{
            margin: 2px 0;
            vertical-align: middle;
            padding: 5px;
        }
        .card-dimond > .img-product {
            height: 119px;
            /*width: auto;*/
        }
        .btn-custom-confirm{
            width: 220px;
        }
    }
</style>
<style type="text/css">
  .btnBlue{
   color: #fff;
      background-color: #00A1E0;
      border-color: #00A1E0;
  }
  .display{
   display: none;
  }
  .lht-loader{
         filter:alpha(opacity=80);position:fixed;width:100%;height:100%;left:0;top:0;bottom:0;right:0;background:#000;opacity:.8;z-index:9999
     }
     .loader:before, .loader:after {
         position: absolute;
         z-index: 100000;
         top: 50%;
         left: 50%;
         border-radius: 50%;
         border-style: solid;
         border-top-color: #ECD078;
         border-right-color: #C02942;
         border-bottom-color: #542437;
         border-left-color: #53777A;
         content: '';
         transform: translate(-50%, -50%);
         animation: rotate 1.5s  infinite ease-in-out;
     }
     .loader:before {
         border-width: 10vh;
     }
     .loader:after {
         width: 30vh;
         height: 30vh;
         border-width: 2.5vh;
         animation-direction: reverse;
     }
     @keyframes rotate {
       0% {
         transform: translate(-50%, -50%) rotate(0);
       }
       100% {
         transform: translate(-50%, -50%) rotate(360deg);
       }
     }
</style>
<div class="lht-loader display" id="lht-loader">
     <div class='loader'></div>
</div>
<body>

    <div class="header-content pb-5" style="background-color: white;">
       

        <div class="banner container">
            <div class="content row mt-5 promo">
                <div class="col-md-4">
                    <div class="row"><div class="col-md-6" style="border-top:2px solid white; margin-right: 50%;"></div></div>
                    <div class="row pt-3">
                        <h3>ĐĂNG KÝ LIỀN TAY</h3>
                    </div>
                    <div class="row pb-3">
                        <h3 style="text-align: right; display: block; width: 100%;">NHẬN NGAY ƯU ĐÃI</h3>
                    </div>
                    <div class="row"><div class="offset-6 col-md-6" style="border-top:2px solid white"></div></div>
                    <div class="row pt-5">
                        <p>
                            Nhanh tay Đăng ký Số Điện Thoại của Bạn để được nhận ngay tin nhắn chứa mã giảm giá 35% khi mua hàng trên website thoitrangkhatoco.vn hoặc Showroom KHATOCO
                        </p>
                        <p>
                          Nếu bạn đã là <span class="text-org">Khách Hàng Thân Thiết (Hạng Thẻ BẠC, VÀNG, KIM CƯƠNG)</span> khi đăng ký tham gia chương trình sẽ nhận được <span class="text-org">10 Voucher giảm 35% và Phiếu Quà tặng</span> tương ứng với từng Hạng thẻ Bên Dưới</span> được gửi qua đường bưu điện (EMS)
                        </p>
                        <p>
                            Thời hạn chương trình: đến hết 31/8/2018
                        </p>
                    </div>
                </div>
                <div class="col-md-8">
               
                
                    <div data-ng-app="khatocoUserProfile" class="block-outer block-account" style="">
                        <div class="">
                            <div data-ng-controller="UserController" class="container fluid">
                                <div  class="row">
                                    <div class="col-xs-12">
                                        <!-- <div class="block-title-outer"><h2 class="block-title block-title-inner condensed">Đăng ký lấy mã</h2></div> -->
                                        <p class="text-org" style="font-size: 25px"><i class="fa fa-caret-left"></i> <span class="ml-2">Thông Tin Khách Hàng</span> </p>
                                        <p class="clearfix ml-3">Số điện thoại: {{return.phone}}</p>
                                    </div>
                                </div>
                                <div class="row ml-1" data-ng-hide="return.code == -1">

                                        <div class="col-md-4">
                                            <p for=""><b class="text-org">Mã khách hàng:</b><br> <span>{{return.card_code}}</span></p>
                                        </div>
                                        <div class="col-md-4"> 
                                            <p for=""><b class="text-org">Hạng Thẻ:</b><br> <span>{{return.cType}}</span></p>
                                        </div>
                                        <div class="col-md-4">
                                            <p for=""><b class="text-org">Điểm Tích Lũy:</b><br> <span>{{return.cardbonnuspoint}}</span></p>
                                        </div>

                                    </div>
                                <form name="registerForm" id="registerForm" method="POST" data-ng-submit="formSubmit()" class="ml-3">
                                    <div  class="row">
                                        <div class="col-md-6 col-xs-12">
                                            <!-- <h2>Thông tin tài khoản</h2> -->
                                                            
                                            <p class="clearfix">
                                                <label for="fullname">Họ và tên <span class="required">*</span></label>
                                                <input class="field" data-ng-model="datapost.fullname" value="{{return.fullname}}" type="text" placeholder="Họ và Tên"required />
                                            </p>
                                            <p class="clearfix">
                                                <label for="email">Email (không bắt buộc)</label>
                                                <input class="field" data-ng-model="datapost.email" value="{{return.email}}" type="email" placeholder="Email"/>
                                            </p>
                                             
                            
                                            
                                        </div>
                                        <div class="col-md-6 col-xs-12">
                                            <!-- <h2>Địa chỉ</h2> -->
                                             <p class="clearfix">
                                                <label for="addr">Địa chỉ <span class="required">*</span> (Số nhà, Tên đường, ...)</label>
                                                <input class="field" data-ng-model="datapost.address" value="{{return.address}}" type="text" placeholder="Tên đường, số nhà"/>
                                            </p>
                                            <p class="birthday" id="province">
                                                <label for="province">Tỉnh/TP</label>
                                                <select data-ng-model="datapost.province"  -ng-options="province as province.name for province in provinces" data-ng-change="setCurrentProvince()" name="province" id="province" class="address-select">
                                                    <option value="" selected>-- Lựa chọn --</option>
                                                </select>
                                            </p>
                                            <p class="birthday" id="district" data-ng-show="datapost.province.provinceid">                        
                                                <label for="district" data-ng-show="datapost.province.provinceid">Quận/Huyện</label>
                                                <select data-ng-model="datapost.district" data-ng-options="district as district.name for district in districts" data-ng-change="setCurrentDistrict()" name="district" id="district" class="address-select">
                                                    <option value="" selected>-- Lựa chọn --</option>
                                                </select>
                                            </p>
                                            <p class="birthday last" id="ward" data-ng-show="datapost.district.districtid && datapost.province.provinceid">                       
                                                <label for="ward" data-ng-show="datapost.district.districtid && datapost.province.provinceid">Phường/Xã</label>
                                                <select data-ng-model="datapost.ward" data-ng-options="ward as ward.name for ward in wards" data-ng-change="setCurrentWard()" name="ward" id="ward" class="address-select">
                                                    <option value="" selected>-- Lựa chọn --</option>
                                                </select>
                                            </p>
                                           
                                           
                                           
                                        </div>
                                    </div>

                                    <div id="member_info" class="display">
                                        <div  class="row">
                                            <div class="col-md-6">
                                                <p class="clearfix">
                                                    <label for="gender">Giới tính <span class="required">*</span></label>
                                                    <select data-ng-model="datapost.gender" data-ng-options="gen as gen.name for gen in genders" id="gender" >
                                                        <option value="" selected>Giới tính</option>
                                                    </select>
                                                </p>
                                              
                                            </div>
                                            <div class="col-md-6">
                                                <label for="birthday">Ngày sinh <span class="required">*</span></label> 
                                                <div  class="row">
                                                    <div class="col-md-4 " >   
                                                      
                                                        <p class="clearfix">
                                                            <select name="" data-ng-model="datapost.birthday.day" id="date">
                                                                <option value="">Ngày</option>
                                                                <option data-ng-repeat="day in days" data-ng-selected="datapost.birthday.day==day" value="{{day}}">{{day}}</option>
                                                            </select>
                                                        </p>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <p class="clearfix">                        
                                                            <select name="" data-ng-model="datapost.birthday.month" id="month">
                                                                <option value="">Tháng</option>
                                                                <option data-ng-repeat="month in months" data-ng-selected="datapost.birthday.month==month" value="{{month}}">{{month}}</option>
                                                            </select>
                                                        </p>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <p class="clearfix">                        
                                                            <select name="" data-ng-model="datapost.birthday.year" id="year" >
                                                                <option value="">Năm</option>
                                                                <option data-ng-repeat="year in years" data-ng-selected="datapost.birthday.year==year" value="{{year}}">{{year}}</option>
                                                            </select>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="row"  >
                                            
                                                    
                                                    
                                                </div>
                                            </div>
                                           
                                           
                                        </div>
                                        
                                        
                                       
                                        <div id="mode_old_user">
                                            <h2>Thiết lập mật khẩu</h2>
                                            <div  class="row">
                                                <div class="col-md-6">
                                                    
                                                    <p class="clearfix">
                                                        <label for="password">Mật khẩu <span class="required">*</span></label>
                                                        <input class="field" data-ng-model="datapost.password" value="" type="password" placeholder="Mật khẩu" id="password" />
                                                    </p>
                                                </div>
                                                <div class="col-md-6">
                                                    <p class="clearfix">                        
                                                        <label for="repassword">Nhập lại mật khẩu <span class="required">*</span></label>
                                                        <input class="field" data-ng-model="datapost.repassword" value="" type="password" placeholder="Xác nhận mật khẩu" id="repassword"/>
                                                    </p>              
                                                </div> 
                                            </div>
                                        </div>

                                           
                                       
                                    </div>
                                    
                                
                                    <div  class="row">
                                        <div class="col-md-12">
                                            <p class="clearfix display" id="more_info_to_register_member">
                                                <label for="checkbox_required checkbox_custom">
                                                    <input style="width: auto; float: none; vertical-align: middle;" type="checkbox" name="checkbox_required" id="checkbox_required" onchange="setCheckedBox()" value="on"> Bạn có muốn trở thành, thành viên của Khatoco.
                                                    <span class="checkmark"></span>
                                                </label>
                                            </p>
                                           
                                            <p class="clearfix" id="submit_voucher">                       
                                                <button class="btn  btn-custom text-uppercase btn-custom-confirm" type="submit">
                                                    <span data-ng-hide="loading">Xác nhận</span>
                                                    <span data-ng-show="loading">Xác nhận <i class="fa fa-refresh fa-spin"></i></span>
                                                </button>
                                            </p>
                                             
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
              

                </div>
            </div>
        </div>
       
    </div>
    <div class="container mt-4">
        <div class="row mt-3">
            <div class="card-gold col-md-4 col-lg-4 card-gold-bound text-center">

                <img class="img-product mt-4"  src="<?php echo get_template_directory_uri(); ?>/assets/landing-page/balo.png">
                <p class="text-uppercase mt-3 text-black" >Hạng Thẻ Kim Cương<br>(Balo laptop)</p>
            </div>
            <div class="card-dimond col-md-4 col-lg-4 card-dimond-bound text-center">

                <img class="img-product mt-5" src="<?php echo get_template_directory_uri(); ?>/assets/landing-page/tuixach.png">
                <p class="text-uppercase mt-5 text-black" >Hạng Thẻ Vàng<br>(Túi đựng thể thao)</p>
            </div>
            <div class="card-silver col-md-4 col-lg-4 card-silver-bound text-center">

                <img class="img-product mt-4" src="<?php echo get_template_directory_uri(); ?>/assets/landing-page/khan.png">
                <p class="text-uppercase mt-5 text-black" >Hạng Thẻ Bạc<br>(Khăn tắm)</p>
            </div>
        </div>
    </div>
    <div class="footer-content mt-4">
        <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="text-center">ĐỊA CHỈ ÁP DỤNG VOUCHER VÀ ĐỔI QUÀ</h3>
                        <!-- <p class="pt-3"> -->
                            <p class="text-org">HỒ CHÍ MINH</p>
                            <p class="ml-5">- 259A Hai Bà Trưng,Phường 6, Quận 3</p>
                            <p class="ml-5">- 282 Quang Trung,Phường 10, Quận Gò Vấp </p>
                            <p class="ml-5">- 969 Cách Mạng Tháng 8, Phường 7, Quận Tân Bình</p>
                            <p class="ml-5">- 319 Cách Mạng Tháng 8, Phường 12, Quận 10</p>
                            <p class="ml-5">- 6M Nguyễn Ảnh Thủ, P.Trung Mỹ Tây, Quận 12</p>
                        <!-- </p> -->
                        <p>
                            <span class="text-org">CẦN THƠ</span>
                            <p class="ml-5">- 3A Mậu Thân, P. Xuân Khánh, Q. Ninh Kiều </p>
                            <p class="ml-5">- 18 Nguyễn Trãi, P. An Hội, Q.Ninh Kiều</p>
                        </p>
                        <p>
                            <span class="text-org">KHÁNH HÒA</span>
                            <p class="ml-5">- Số B4 Chưng cư Vĩnh Phước, Đường 2/4,TP. Nha Trang</p>
                            <p class="ml-5">- 80C Quang Trung, P. Lộc Thọ, TP. Nha Trang</p>
                            <p class="ml-5">- 221 Thống Nhất, P. Phương Sài, TP. Nha Trang</p>
                            <p class="ml-5">- TTTM Nha Trang Center, 20 Trần Phú, TP. Nha Trang</p>
                        </p>
                        <p>
                            <span class="text-org">BÌNH ĐỊNH</span>
                            <p class="ml-5">- 164 Nguyễn Thái Học, P. Ngô Mây , TP. Quy Nhơn</p>
                            <p class="ml-5">- 231 Lê Hồng Phong, P. Lê Hồng Phong TP.Quy Nhơn</p>
                        </p>
                        <p>
                            <span class="text-org">ĐỒNG NAI</span>
                            <p class="ml-5">- 1137 Phạm Văn Thuận (Đối diện ngã ba Máy Cưa), Khu phố 1, P. Tân Tiến, TP. Biên Hòa</p>
                        </p>
                            <span class="text-org">MUA ONLINE:</span>
                            <p class="ml-5">- <a href="http://thoitrangkhatoco.vn">http://thoitrangkhatoco.vn</a></p>
                    </div>
                </div>
        </div>
    </div>
    <div id="popup" class="popup close-wrap hidden">
        <div class="close-bg"></div>
        <div class="popup-inner-custom">
            <div id="popup-message" class="message-wrap row clearfix">
            </div>
            <a href="#close" class="close">x</a>
            <div class="button-close row clearfix" id="popup_close">
                
            </div>
        </div>
    </div>
</body>
<script type="text/javascript">
    var apiUrl = "<?php echo admin_url('admin-ajax.php'); ?>";
    var app = angular.module("khatocoUserProfile", []);
    var phone = 0+location.pathname.split('/')[2];
    app.service('userService', ['$http', '$q', '$timeout', function($http, $q, $timeout){

        var getAddressInfo = function(action, id){
            return $http({
                method: 'GET',
                url: apiUrl,
                params: {action: 'user_service_ajax', do_ajax: action, id: id,cellphone:phone}
            });
        };

         var getCardUserInfo = function (data) {
            var d = $q.defer();
            $timeout(function () {
                $http({
                    method: 'POST',
                    url: apiUrl,
                    params: {action: 'user_service_ajax', do_ajax: 'get_user_for_sms',cellphone:phone},
                    data: data
                }).success(function (data, status, headers) {
                    d.resolve(data);
                }).error(function (data, status, headers) {
                    d.reject(data);
                });
            }, 1000);

            return d.promise;
        };


        var saveUserInfo = function(data){
            var d = $q.defer();

            $timeout(function () {
                $http({
                    method: 'POST',
                    url: apiUrl,
                    params: {action: 'user_service_ajax', do_ajax: 'save_user_info_sms'},
                    data: data,
                    //headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'}
                }).success(function(data, status, headers){
                    d.resolve(data);
                }).error(function(data, status, headers){
                    d.reject(data);
                });
            }, 1000);

            return d.promise;
        };

        return {
            getAddressInfo: getAddressInfo,
            saveUserInfo: saveUserInfo,
             getCardUserInfo: getCardUserInfo
        };

       
        
    }]);

    app.controller('UserController', ['$scope', '$sce', 'userService', function($scope, $sce, userService){
        $scope.submitMessage = '';
        $scope.loading = false;

        $scope.years = [2010,2009,2008,2007,2006,2005,2004,2003,2002,2001,2000,1999,1998,1997,1996,1995,1994,1993,1992,1991,1990,1989,1988,1987,1986,1985,1984,1983,1982,1981,1980,1979,1978,1977,1976,1975,1974,1973,1972,1971,1970,1969,1968,1967,1966,1965,1964,1963,1962,1961,1960,1959,1958,1957,1956,1955,1954,1953,1952,1951,1950,1949,1948,1947,1946,1945,1944,1943,1942,1941,1940,1939,1938,1937,1936,1935,1934,1933,1932,1931,1930];
        $scope.months = [1,2,3,4,5,6,7,8,9,10,11,12];
        $scope.days = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31];

        $scope.wards = [];
        $scope.districts = [];
        $scope.provinces = [];
        $scope.datapost = {};
        $scope.return = {};
        $scope.return.phone = phone;
        $scope.genders = [{id: 'male', name: 'Nam'}, {id: 'female', name: 'Nữ'}];

        $scope.datapost.province = {};
        $scope.datapost.district = {};
        $scope.datapost.ward = {};
        $scope.datapost.gender = {};
        $scope.datapost.birthday = {};
        $scope.datapost.address = '';
        $scope.datapost.pro = '';
        $scope.datapost.cellphone = phone;
        $scope.datapost.nonce = "<?php echo wp_create_nonce('register_nonce_action'); ?>";
        $scope.datapost.redirect = "<?php echo home_url('/'); ?>";

        $scope.userInfo = {};
        $scope.phoneMessage = '';

        $scope.datapost.captcha = '';

        

        $scope.error = false;

       

        if($scope.error == true){
            alert('Không thể kết nối đến server, vui lòng F5 và thử lại!'); return;
        }

        $scope.formSubmit = function(){
           

            $scope.loading = true;
            $scope.submitMessage = '';

            console.log( $scope.datapost );
             // return false;
            document.getElementById('lht-loader').classList.remove('display')
            userService.saveUserInfo($scope.datapost)
                .then(function(data){
                    // alert('a')
                     document.getElementById('lht-loader').classList.add('display')
                    if( data.success ){

                   
                        

                       document.getElementById('popup-message').innerHTML = '<p style="text-align: justify;" class="mb-3">' + data.message + '</p>';
                        document.getElementById('popup_close').innerHTML = '<p style="margin:auto;"><a class="btn btn-custom btn-lg btn-custom-confirm" href="http://thoitrangkhatoco.vn/tri-an-khach-hang-than-thiet-2018" id="link" class="button">OK</a></p>';
                        
                        document.getElementById('popup').className = document.getElementById('popup').className.replace(/hidden/i, '');

                      
                    }else{

                        document.getElementById('popup-message').innerHTML = '<p style="text-align: justify;" class="mb-3">' + data.message + '</p>';
                        document.getElementById('popup_close').innerHTML = '<p style="margin:auto;"><a  href="javascript:;" id="link" class="btn btn-custom btn-lg btn-custom-confirm" class="button">Đóng lại</a></p>';                   }
                        document.getElementById('popup').className = document.getElementById('popup').className.replace(/hidden/i, ''); 
                    
                    $scope.loading = false;
                    // document.location.href = '<?php echo home_url("/"); ?>';

                }, function(err){
                    $scope.submitMessage = 'Không thể kết nối đến máy chủ, F5 để thử lại.';
                    $scope.loading = false;
                });
        };

        $scope.setCurrentProvince = function(){
            $scope.getDistricts($scope.datapost.province.provinceid);
        };

      
        $scope.setCurrentDistrict = function(){
            $scope.getWards($scope.datapost.district.districtid);
        };

        $scope.objectToArray = function(object){
            var result = [];
            for(ob in object){
                result.push(object[ob]);
            }
            return result;
        };
         $scope.getWards = function(districtid){
            userService.getAddressInfo('get_wards_sms', districtid)
                .success(function(data, status, headers){
                    if(data.success == 'true'){
                        $scope.wards = $scope.objectToArray(data.result);
                        if(data.current){
                            $scope.datapost.ward = data.result[data.current]
                        }
                    }
                })
                .error(function(data, status, headers){
                    $scope.error = true;
                });
        };

        $scope.getDistricts = function(provinceid){
            userService.getAddressInfo('get_districts_sms', provinceid)
                .success(function(data, status, headers){ //console.log(data);
                    if(data.success == 'true'){
                        $scope.districts = $scope.objectToArray(data.result);
                        if(data.current){
                            $scope.datapost.district = data.result[data.current];
                            $scope.getWards(data.current);
                        }
                    }
                })
                .error(function(data, status, headers){
                    $scope.error = true;
                });
        };


        $scope.getProvinces = function(){
            userService.getAddressInfo('get_provinces_sms', 0)
                .success(function(data, status, headers){
                    if(data.success == 'true'){
                    
                        $scope.provinces = $scope.objectToArray(data.result);
                        
                        if(data.current){
                            $scope.datapost.province = data.result[data.current];
                            $scope.getDistricts(data.current);
                        }
                    }
                })
                .error(function(data, status, headers){
                    $scope.error = true;
                });
        };

        $scope.getProvinces();

        $scope.getProvince = function(current){
            userService.getAddressInfo('get_provinces_sms', 0)
                .success(function(data, status, headers){
                    if(data.success == 'true'){
                        // alert(current);
                        $scope.provinces = $scope.objectToArray(data.result);
                        data.current = current;
                        // alert(data.current)
                        if(data.current){
                            $scope.datapost.province = data.result['_'+data.current];
                            // $scope.getDistricts(data.current);
                        }
                    }
                })
                .error(function(data, status, headers){
                    $scope.error = true;
                });
        };

        
        $scope.getUserInfo = function(){
            userService.getCardUserInfo($scope.datapost)
            .then(function (data) {
                if (data.success) {
                    if(data.message === 'Đây là thành viên mới'){
                        // document.getElementById('popup-message').innerHTML = '<p>' + data.message + '</p>';
                        // document.getElementById('popup').className = document.getElementById('popup').className.replace(/hidden/i, '');
                        //  document.getElementById('popup_close').innerHTML = '<p><a href="<?php echo home_url("/"); ?>" id="link" class="button">ok</a></p>';
                        document.getElementById('district').classList.add('display');
                        document.getElementById('ward').classList.add('display');
                        document.getElementById('province').classList.remove('birthday');
                        $scope.return.code= -1;

                    }
                    else if(data.message === 'Đây là thành viên từ CRM'){

                        document.getElementById('district').classList.add('display');
                        document.getElementById('ward').classList.add('display');
                        document.getElementById('province').classList.remove('birthday');
                        $scope.return.fullname = data.user.fullname
                        $scope.return.address = data.user.address
                        $scope.return.card_code = data.user.card_code
                        $scope.datapost.address = data.user.address
                        $scope.getProvince(data.user.province);
                        $scope.datapost.fullname = data.user.fullname

                        switch(data.user.card)
                        {
                            case 'DIAMONDNB':
                                $scope.return.cType= 'Thẻ kim cương';
                                break;
                            case 'DIAMOND':
                                $scope.return.cType= 'Thẻ kim cương';
                                break;
                            case 'GOLD':
                                $scope.return.cType= 'Thẻ vàng' ;
                                break;
                            case 'GOLDNB':
                                $scope.return.cType= 'Thẻ vàng' ;
                                break;
                            case 'SILVER':
                                $scope.return.cType= 'Thẻ bạc';
                                break;
                            case 'SILVERNB':
                                $scope.return.cType= 'Thẻ bạc';
                                break;
                            case 'SLIVERNB':
                                $scope.return.cType= 'Thẻ bạc';
                                break;
                            case 'NOIBO':
                                $scope.return.cType= 'Thẻ nội bộ' ;
                                break;
							case 'NOTREG':
                                $scope.return.cType= 'Thẻ thành viên' ;
                                break;
                            default :
                                $scope.return.cType= data.user.card;
                        }
                        var nf = Intl.NumberFormat();
                        $scope.return.cardbonnuspoint = nf.format(data.user.orderRevenue);
                        $scope.return.code= 1;
                    }
                    else{
                        // $scope.return.code= -1;
                        document.getElementById('more_info_to_register_member').classList.add('display');
                        document.getElementById('mode_old_user').innerHTML = "";
                         
                        document.getElementById('member_info').classList.remove('display');
                        $scope.return.cellphone = data.message.phone
                        $scope.return.fullname = data.message.fullname
                        $scope.return.email = data.message.email
                        $scope.datapost.birthday = data.message.birthday;
                        $scope.return.email = data.message.email
                        $scope.return.username = data.message.username
                        // alert( $scope.return.username)
                        $scope.return.address = data.message.address
                        $scope.return.card_code = data.message.card_code
                        $scope.datapost.fullname = data.message.fullname
                        $scope.datapost.email = data.message.email
                        // document.getElementById('province').value = "object:"+data.result.province
                        // console.log($scope.return.cellphone)
                        // document.getElementById('phone').textContent = data.result.phone;
                        // document.getElementById('popup-message').innerHTML = '<p>' + data.message + '</p>';
                        // document.getElementById('popup').className = document.getElementById('popup').className.replace(/hidden/i, '');

                        // window.location.href ='http://khatocofashion.net/';
                        // window.location.href = '<?php echo home_url("/"); ?>';
                        for(genkey in $scope.genders){
                            if(data.message.gender['id'] == $scope.genders[genkey]['id']){
                                $scope.datapost.gender = $scope.genders[genkey];
                            }
                        }
                        // if(data.message.card == 0){
                        //     $scope.return.code= -1;
                        // }else{
                            // 
                        // }
                        // alert( $scope.return.code)
                        switch(data.message.card)
                        {
                            case 'DIAMONDNB':
                                $scope.return.cType= 'Thẻ kim cương';
                                break;
                            case 'DIAMOND':
                                $scope.return.cType= 'Thẻ kim cương';
                                break;
                            case 'GOLD':
                                $scope.return.cType= 'Thẻ vàng' ;
                                break;
                            case 'GOLDNB':
                                $scope.return.cType= 'Thẻ vàng' ;
                                break;
                            case 'SILVER':
                                $scope.return.cType= 'Thẻ bạc';
                                break;
                            case 'SILVERNB':
                                $scope.return.cType= 'Thẻ bạc';
                                break;
                            case 'NOIBO':
                                $scope.return.cType= 'Thẻ nội bộ' ;
                                break;
                            default :
                                $scope.return.cType= 'Thành viên';
                        }
                        var nf = Intl.NumberFormat();
                        $scope.return.cardbonnuspoint = nf.format(data.message.orderRevenue);
                    }    
                }
                else {

                    if (data.message) {
                        // $scope.submitMessage = data.message;
                        // document.getElementById('more_info_to_register_member').classList.remove('display');
                        $scope.return.phone = '';
                        if(data.message !== 'Đây là thành viên mới'){
                            document.getElementById('popup-message').innerHTML = '<p style="text-align: justify;" class="mb-3">' + data.message + '</p>';
                            document.getElementById('popup').className = document.getElementById('popup').className.replace(/hidden/i, '');
                             // document.getElementById('popup_close').innerHTML = '<p><a href="<?php echo home_url("/"); ?>" id="link" class="button">ok</a></p>';
                             document.getElementById('popup_close').innerHTML = '<p style="margin:auto;"><a class="btn btn-custom btn-custom-confirm btn-lg" href="http://thoitrangkhatoco.vn/tri-an-khach-hang-than-thiet-2018" id="link" class="button">OK</a></p>';
                            
                        }
                       
                       
                    }
                    $scope.return.code= -1;
                }
                $scope.loading = false;
               
            }, function (err) {
                $scope.return.code= -1;
                $scope.submitMessage = 'Không thể kết nối đến máy chủ, F5 để thử lại.';
                $scope.loading = false;
            });
        };
        $scope.getUserInfo();

       
         // console.log($scope.datapost.pro)
    }]);
    function setCheckedBox(){
           
            if(document.getElementById('checkbox_required').checked){
                document.getElementById('member_info').classList.remove('display');
                // console.log(document.getElementById('registerForm').getAttribute('data-ng-submit'));
                document.getElementById('gender').setAttribute("required","");
                document.getElementById('date').setAttribute("required","");
                document.getElementById('month').setAttribute("required","");
                document.getElementById('year').setAttribute("required","");
                document.getElementById('password').setAttribute("required","");
                document.getElementById('repassword').setAttribute("required","");

                return true;
            }else{
                document.getElementById('member_info').classList.add('display');
                document.getElementById('gender').removeAttribute("required");
                document.getElementById('date').removeAttribute("required");
                document.getElementById('month').removeAttribute("required");
                document.getElementById('year').removeAttribute("required");
                document.getElementById('password').removeAttribute("required");
                document.getElementById('repassword').removeAttribute("required");
                return false;
            }
        };

</script>
<?php get_footer() ?>
