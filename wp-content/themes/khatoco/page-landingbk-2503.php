<?php
/*
Template Name: Landing page
*/
wp_head();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>
        <?php
            //echo str_replace('"', '', $metaProperty['title']);
            echo get_the_title();
        ?>
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta charset="<?php bloginfo('charset'); ?>" />
    <meta content="text/html; charset=utf-8" http-equiv="content-type">

    <?php if (!isset($_SERVER['HTTP_USER_AGENT']) || stripos($_SERVER['HTTP_USER_AGENT'], 'Chrome-Lighthouse') === false): ?>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/dncss/vendors/bootstrap/bootstrap.min.css" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/dncss/css/style.css" crossorigin="anonymous">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,700,300,600,400|Open+Sans+Condensed:700,300&subset=latin,vietnamese,latin-ext' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <?php
        endif;
    ?>
    
    <style type="text/css">
        body{
            <?php if (!isset($_SERVER['HTTP_USER_AGENT']) || stripos($_SERVER['HTTP_USER_AGENT'], 'Chrome-Lighthouse') === false): ?>
                background-image: url("<?=get_template_directory_uri(); ?>/images/landing_background.jpg");
            <?php
                endif;
            ?>
            -webkit-background-size: cover;
            background-size: cover;
            background-repeat: no-repeat;
            background-position: 0 50%;
            /*background-position: 50% 10%;*/
            /*height: 100%;*/
        }
        .header-captions h3{
            color: #fff;
            font-weight: bold;
            font-size: 50px;
        }
        .header-captions p{
            color: #E5E5E5;
        }
        .collapse.in {
            display: block !important;
        }
        a.promotion-rules{
            text-transform: uppercase !important;
            background: transparent !important;
            border: transparent !important;
            outline: none !important;
            box-shadow: none !important;
            width: 100%;
            text-align: left;
        }
        a.promotion-rules:before{
            float: right !important;
            font-family: FontAwesome;
            content:"\f068";
            padding-right: 5px;
        }

        a.promotion-rules.collapsed:before {
            float: right !important;
            content:"\f067";
        }

        #promotion-rules>ul li{
            /*list-style-type: lower-alpha;*/
            font-weight: 400;
            margin-bottom: 20px;
        }
        #promotion-rules>ul.decimal>li{
            list-style-type: decimal;
        }
        #promotion-rules>ul>li>ul{
            margin-top: 10px;
        }
        #promotion-rules>ul>li>ul>li{
            list-style-type: disc;
            padding-left: 35px;
            margin-top: 6px;
        }


        /* Hide all steps by default: */
        .tab {
            display: none;
        }

        button {
            /*background-color: #4CAF50;*/
            color: #ffffff;
            border: none;
            padding: 10px 20px;
            /*font-size: 17px;*/
            font-size: 13px;
            cursor: pointer;
        }

        button:hover {
            opacity: 0.8;
        }

        #prevBtn {
            background-color: #bbbbbb;
        }

        /* Make circles that indicate the steps of the form: */
        .step-container{
            text-align: center;
            /*margin-top: 40px;*/
            display: flex;
            width: 100%;
            align-items: center;
        }
        .step {
            position: relative;
            width: calc(32.5% - 8px);
            margin-right: 8px;
            background-color: #fff;
            border-radius: 10px;
            height: 100px;
            flex: 1;
            padding: 8px;
            box-shadow: 2px 1px 15px rgba(0,0,0,1);
        }
        .step-container .step:nth-child(3){
            margin-right: 0;
        }
        .step .circle-step{
            position: absolute;
            top: 50%;
            transform: translateY(-50%);
            right: -25px;
            width: 50px;
            height: 50px;
            background-color: #fe5909;
            color: white;
            z-index: 3;
            border-radius: 50%;
        }
        .step .circle-step:after {
            content: attr(data-count);
            position: absolute;
            top: -10px;
            left: -10px;
            right: -10px;
            bottom: -10px;
            background: rgba(255, 0, 0, 0.2);
            border-radius: 50%;
            z-index: 2;
            display: flex;
            align-items: center;
            justify-content: center;
        }
        .step .circle-step:before {
            content: ' ';
            position: absolute;
            top: -5px;
            left: -5px;
            right: -5px;
            bottom: -5px;
            background: rgba(255, 0, 0, 0.2);
            border-radius: 50%;
            z-index: 1;
        }
        .step:last-child {
            margin-right: 0;
        }
        #formLanding>.form-header{
            display: flex;
            flex-direction: row;
        }
        .step>.step-icon>img{
            width: 50px;
            margin: 0 auto;
        }
        .step>.step-text>p{
            margin: 5px auto 0 auto;
            font-size: 12px;
            font-weight: 600;
        }
        @media all and (max-width: 480px) {
            .step>.step-text>p{
                font-size: 11px;
            }
        }

        .step.active {
            opacity: 1;
        }

        /* Mark the steps that are finished and valid: */
        .step.finish {
            background-color: #fae1d0;
        }

        .rules{
            background: rgba(0, 0, 0, 0.5);
            border-radius: 6px;
            padding: 15px 30px;
            color: white;
            margin-top: 20px;
            margin-bottom: 20px;
            /* box-shadow: 2px 1px 15px rgba(0,0,0,1); */
        }
        .promotion-rules{
            font-weight: 600;
            font-size: 18px;
        }
        #promotion-rules{
            margin-top: 20px;
        }
        #formLanding .form-body{
            background: #fff;
            border-radius: 6px;
            /*padding: 15px 30px;*/
            padding: 30px;
            /*padding-top: 20px;*/
            margin-top: 10px;
            box-shadow: 2px 1px 15px rgba(0,0,0,1);
        }
        #formLanding .form-body select.form-control{
            /*-webkit-appearance: none;*/
        }
        #formLanding .form-body .form-control{
            width: 100%;
            /*padding: 8px 12px;*/
            border: 1px solid #dcddde;
            border-radius: 0;
        }

        .wp-items-card{}
        .wp-items-card .items-card{}
        .wp-items-card .items-card .form-control{
            width: 80%;
        }
        .form-group label{
            /*color: gray;*/
            margin-bottom: 3px;
        }
        input.invalid{
            border: 1px solid #b32017 !important;
        }

        .items-card{
            margin-bottom: 10px;
        }
        .select2-container--default .select2-selection--single .select2-selection__arrow{
                height: calc(2.25rem + 2px);
        }
        .select2-container .select2-selection--single .select2-selection__rendered{
                height: calc(2.25rem + 2px);
    padding: .375rem .75rem;
        }
        .select2-container .select2-selection--single{
            height: auto;
        }

        .step.finish{
            cursor: pointer;
        }

        .header-captions img{
            max-width: 362px;
        }
        @media(max-width: 320px){
            .rules{
                padding: 15px 10px;
            }
            .formLanding{
                margin-bottom: 20px;
            }
            .header-captions img{
                max-width: auto !important;
                width: auto !important;
            }
        }

    </style>

</head>
<body>

    <div class="wrapper">
        <div class="container">
            <header>
                <div class="row my-3">
                    <div class="col-md-6 d-flex align-items-center justify-content-center">
                        <a href="<?php echo home_url('/'); ?>" title="<?php echo get_option( 'blogname' ); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" /></a>
                    </div>
                    <div class="col-md-6 header-captions text-right">

                        <img src="<?php echo get_template_directory_uri(); ?>/images/mua_he_bali.png" class="img-responsive" style="margin: 0 auto; width: 100%"/>

                    </div>
                </div>
            </header>
            <div class="row">
                <div class="col-md-6 p-0">
                    <div class="col-md-12" id="wpVideos">
                        <div class="embed-responsive-1by1">
                            <?php
                                $useragent=$_SERVER['HTTP_USER_AGENT'];
                                if (!isset($useragent) || stripos($useragent, 'Chrome-Lighthouse') === false): 
                                    if(!preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))):
                            ?>

                                <iframe style="width: 100%;" height="300" class="embed-responsive-item" src="https://www.youtube.com/embed/PUX4a7JWPJ0?autoplay=1&loop=1&playlist=PUX4a7JWPJ0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


                            <?php endif; endif; ?>
                        </div>

                    </div>
                    <div class="col-md-12">
                        <div class="rules">
                            <a href="#promotion-rules" class="btn btn-info promotion-rules" data-toggle="collapse"> <i class="more-less glyphicon glyphicon-plus"></i> Thể Lệ Chương Trình</a>
                                <!-- <div id="promotion-rules" class="collapse in" style="max-height: 300px; overflow-y: auto;"> -->
                                <div id="promotion-rules" class="collapse in" style="max-height: 212px; overflow-y: auto;">
                                    <p>&ldquo;M&Ugrave;A H&Egrave; BALI &ndash; TƯNG BỪNG BIỂN GỌI C&Ugrave;NG THỜI TRANG KHATOCO&rdquo;</p>
                                    
                                    <ol>
                                        <li>C&Aacute;CH THỨC THAM GIA</li>
                                    </ol>
                                    
                                    <p>- Kh&aacute;ch h&agrave;ng chọn mua c&aacute;c sản phẩm c&oacute; gắn thẻ c&agrave;o tr&uacute;ng thưởng tr&ecirc;n tem nh&atilde;n thẻ b&agrave;i.</p>
                                    <p>- Kh&aacute;ch h&agrave;ng c&agrave;o nhẹ lớp phủ bạc để nhận 01 m&atilde; dự thưởng tham gia chương tr&igrave;nh.</p>
                                    <p>- Kh&aacute;ch h&agrave;ng truy cập v&agrave;o trang th&ocirc;ng tin của chương tr&igrave;nh <strong><a href="https://thoitrangkhatoco.vn/MuaheBali" class="text-danger">thoitrangkhatoco.vn/MuaheBali</a></strong> điền c&aacute;c th&ocirc;ng tin theo hướng dẫn (bao gồm số điện thoại di động) v&agrave; nhập m&atilde; dự thưởng kể tr&ecirc;n để đồng thời được:</p>
                                    
                                    <p style="padding-left:20px;">+ Đăng k&yacute; tham gia chương tr&igrave;nh &ldquo;M&ugrave;a h&egrave; BALI&rdquo; với giải thưởng Voucher du lịch l&ecirc;n đến 20.000.000đ cho cặp đ&ocirc;i đến Thi&ecirc;n Đường Biển BALI. <em>(1*)</em></p>
                                    
                                    <p style="padding-left:20px;">+ Nhận ngay 01 lần nạp tiền điện thoại, mệnh gi&aacute; ngẫu nhi&ecirc;n từ 10.000đ tới 100.000đ v&agrave;o số điện thoại kh&aacute;ch h&agrave;ng đ&atilde; điền ở bước tr&ecirc;n. <em>(2*)</em></p>
                                    <p><em>(1*) 01 kh&aacute;ch h&agrave;ng mua nhiều sản phẩm 01 lần, được nhập nhiều m&atilde; dự thưởng tr&ecirc;n hệ thống, tương ứng với nhiều cơ hội tham gia quay thưởng giải thưởng voucher du lịch.</em></p>
                                    <p><em>(2*) Mỗi số điện thoại chỉ được tham gia chương tr&igrave;nh 01 lần v&agrave; tương ứng với 01 lần nạp tiền điện thoại</em>.</p>
                                    
                                    <p>- Sau khi ho&agrave;n th&agrave;nh bước đăng k&yacute;, qu&yacute; kh&aacute;ch sẽ nhận được 01 tin nhắn x&aacute;c nhận m&atilde; dự thưởng tham gia quay số tr&uacute;ng c&aacute;c voucher du lịch của chương tr&igrave;nh &ldquo;M&ugrave;a H&egrave; BALI&rdquo;.</p>
                                    
                                    <ol start="2">
                                        <li>CƠ CẤU GIẢI THƯỞNG</li>
                                    </ol>
                                    
                                    <p>- <strong>Giải thưởng trao ngay:</strong></p>
                                    <ul>
                                        <li>Trao dưới h&igrave;nh thức tiền điện thoại, nạp trực tiếp v&agrave;o số điện thoại của kh&aacute;ch h&agrave;ng, sau khi kh&aacute;ch h&agrave;ng ho&agrave;n th&agrave;nh thao t&aacute;c với m&atilde; dự thưởng theo hướng dẫn ở mục thể lệ chương tr&igrave;nh.</li>
                                        <li>Mỗi giải c&oacute; mệnh gi&aacute; ngẫu nhi&ecirc;n từ 10.000đ tới 100.000đ.</li>
                                        <li>Ngo&agrave;i ra, mỗi m&atilde; dự thưởng tương ứng với 01 cơ hội tham gia quay thưởng voucher du lịch.</li>
                                        <li>Thời gian thực hiện: 01/04/2019 đến 15/05/2019.</li>
                                    </ul>
                                    <p><strong>- Giải thưởng may mắn: 7 giải.</strong></p>
                                    <ul>
                                        <li>2 Giải Nhất, mỗi giải l&agrave; một Voucher du lịch đến Thi&ecirc;n Đường Biển BALI, trị gi&aacute; 20.000.000 đ/giải <em>(3*).</em></li>
                                        <li>5 Giải Nh&igrave;, mỗi giải l&agrave; một Voucher du lịch tại hệ thống Khu Du Lịch v&agrave; Giải Tr&iacute; Vinpearl, trị gi&aacute; 5.000.000 đ/giải <em>(4*).</em></li>
                                        <li>Danh s&aacute;ch kết quả c&aacute;c giải thưởng voucher du lịch được Khatoco tổ chức c&ocirc;ng khai quay thưởng hoặc bốc thăm may mắn v&agrave; c&ocirc;ng bố tr&ecirc;n website https://thoitrangkhatoco.vn.</li>
                                        <li>Thời gian quay thưởng v&agrave; c&ocirc;ng bố: 19/05/2019.</li>
                                    </ul>
                                    
                                    <p><em>(3*)</em><em> Voucher trị gi&aacute; 20.000.000 đồng l&agrave; voucher sử dụng dịch vụ du lịch (hay c&ograve;n gọi l&agrave; coupon) của hệ thống du lịch Viettravel v&agrave; tu&acirc;n thủ c&aacute;c điều kiện sử dụng voucher (coupon) của Viettravel.</em></p>
                                    <p><em>(4*) Voucher trị gi&aacute; 5.000.000 đồng l&agrave; voucher sử dụng dịch vụ ph&ograve;ng nghỉ của hệ thống kh&aacute;ch sạn thuộc chuỗi Vinpearl Resort v&agrave; tu&acirc;n thủ c&aacute;c điểu kiện sử dụng voucher của Vinpearl.</em></p>
                                    
                                    <ol start="3">
                                        <li>QUY ĐỊNH VỀ BẰNG CHỨNG V&Agrave; C&Aacute;CH THỨC X&Aacute;C THỰC TR&Uacute;NG THƯỞNG</li>
                                    </ol>
                                    <p>- Đối với c&aacute;c giải thưởng trao ngay bằng h&igrave;nh thức nạp tiền điện thoại trực tiếp: Kh&aacute;ch h&agrave;ng nhập ch&iacute;nh x&aacute;c số điện thoại v&agrave; m&atilde; code tr&ecirc;n tem thẻ c&agrave;o tại website <strong><a href="https://thoitrangkhatoco.vn/MuaheBali" class="text-danger">thoitrangkhatoco.vn/MuaheBali</a></strong> để nhận được tiền điện thoại nạp v&agrave;o số điện thoại di động kh&aacute;ch h&agrave;ng đ&atilde; cung cấp.</p>
                                    <p>- Đối với c&aacute;c giải thưởng voucher du lịch: Kh&aacute;ch h&agrave;ng được xem l&agrave; tr&uacute;ng giải thưởng voucher du lịch khi đưa ra được tem thẻ c&agrave;o hoặc tin nhắn của tổng đ&agrave;i Khatoco c&oacute; chứa 01 trong số c&aacute;c d&atilde;y số tr&ugrave;ng khớp ho&agrave;n to&agrave;n với kết quả quay thưởng của Khatoco; v&agrave; kh&aacute;ch h&agrave;ng đưa ra được CMND/ CCCD c&oacute; họ t&ecirc;n, ng&agrave;y th&aacute;ng năm sinh tr&ugrave;ng khớp với th&ocirc;ng tin c&aacute; nh&acirc;n đ&atilde; được khai b&aacute;o tr&ecirc;n webiste <strong><a href="https://thoitrangkhatoco.vn/MuaheBali" class="text-danger">thoitrangkhatoco.vn/MuaheBali</a>.</strong></p>
                                    <ol start="4">
                                        <li>THỜI GIAN, ĐỊA ĐIỂM, C&Aacute;CH THỨC TRAO THƯỞNG:</li>
                                    </ol>
                                    <p>- Địa điểm trao thưởng:&nbsp;C&ocirc;ng ty TNHH Thương mại Khatoco tại đại chỉ Số 07 V&otilde; Thị S&aacute;u, Phường Vĩnh Nguy&ecirc;n, Th&agrave;nh phố Nha Trang, Tỉnh Kh&aacute;nh H&ograve;a hoặc gửi trực tiếp đến địa chỉ của kh&aacute;ch h&agrave;ng sau khi được x&aacute;c nhận của kh&aacute;ch h&agrave;ng.</p>
                                    <p>- C&aacute;ch thức trao thưởng:&nbsp;</p>
                                    <ul>
                                        <li>Đối với giải khuyến m&atilde;i tiền điện thoại: Kh&aacute;ch h&agrave;ng nhận được tiền điện thoại trực tiếp v&agrave;o số điện thoại di động đăng k&iacute; tham gia chương tr&igrave;nh ngay sau khi ho&agrave;n tất c&aacute;c th&ocirc;ng tin tr&ecirc;n website&nbsp;<strong><a href="https://thoitrangkhatoco.vn/MuaheBali" class="text-danger">thoitrangkhatoco.vn/MuaheBali</a>. </strong></li>
                                        <li>Đối với giải voucher du lịch: Khatoco thực hiện buổi trao thưởng tập trung tại C&ocirc;ng ty TNHH Thương mại Khatoco (Số 07 V&otilde; Thị S&aacute;u, phường Vĩnh Nguy&ecirc;n, Th&agrave;nh phố Nha Trang, tỉnh Kh&aacute;nh H&ograve;a) hoặc li&ecirc;n hệ trực tiếp kh&aacute;ch h&agrave;ng để chuyển voucher du lịch đến địa chỉ do kh&aacute;ch h&agrave;ng x&aacute;c nhận nếu kh&aacute;ch h&agrave;ng kh&ocirc;ng thể đến nhận trực tiếp.</li>
                                    </ul>
                                    <p>- Thủ tục trao thưởng: Kh&aacute;ch h&agrave;ng nhận thưởng giải voucher du lịch cầm theo CMND/ CCCD để đối chiếu với Họ t&ecirc;n, Ng&agrave;y th&aacute;ng năm sinh kh&aacute;ch h&agrave;ng đ&atilde; khai b&aacute;o tr&ecirc;n hệ thống. V&agrave;, kh&aacute;ch h&agrave;ng đưa ra được tem thẻ c&agrave;o c&ograve;n nguy&ecirc;n vẹn, kh&ocirc;ng bị tẩy x&oacute;a hoặc tin nhắn SMS của tổng đ&agrave;i chăm s&oacute;c kh&aacute;ch h&agrave;ng Khatoco x&aacute;c nhận c&aacute;c m&atilde; code dự thưởng của kh&aacute;ch h&agrave;ng tr&ugrave;ng khớp với kết quả quay thưởng đ&atilde; được c&ocirc;ng bố. Mọi trường hợp nhận thay phải c&oacute; ủy quyền do chủ sở hữu giải thưởng k&iacute; ủy quyền v&agrave; c&oacute; x&aacute;c nhận của địa phương nơi cư tr&uacute;.</p>
                                    <p>- Thời hạn kết th&uacute;c trao thưởng:&nbsp;Trong v&ograve;ng 20 ng&agrave;y kể từ ng&agrave;y giải thưởng được c&ocirc;ng bố, kh&aacute;ch h&agrave;ng kh&ocirc;ng nhận thưởng hoặc kh&ocirc;ng li&ecirc;n hệ tổng đ&agrave;i Khatoco để nhận thưởng, giải thưởng sẽ được hủy bỏ.</p>
                                    
                                    <ol start="5">
                                        <li>C&Aacute;C QUY ĐỊNH KH&Aacute;C</li>
                                    </ol>
                                    <p>- Đầu mối giải đ&aacute;p thắc mắc cho kh&aacute;ch h&agrave;ng về c&aacute;c vấn đề li&ecirc;n quan đến chương tr&igrave;nh: hotline 1800.585860 &ndash; miễn ph&iacute;, trong giờ h&agrave;nh ch&iacute;nh.</p>
                                    <p>- Khatoco sẽ kh&ocirc;ng chịu tr&aacute;ch nhiệm cho bất kỳ chi ph&iacute; ph&aacute;t sinh n&agrave;o của kh&aacute;ch h&agrave;ng để tham gia chương tr&igrave;nh khuyến m&atilde;i n&agrave;y hoặc để nhận giải thưởng.</p>
                                    <p>- Khatoco sẽ kh&ocirc;ng c&oacute; tr&aacute;ch nhiệm trong trường hợp kh&aacute;ch h&agrave;ng kh&ocirc;ng nhận được giải thưởng do bất k&igrave; sai s&oacute;t, hay lỗi của kh&aacute;ch h&agrave;ng v&agrave; th&ocirc;ng tin kh&ocirc;ng ch&iacute;nh x&aacute;c do kh&aacute;ch h&agrave;ng cung cấp./.</p>
                                </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 p-0" id="wpFormLanding">
                    <div id="formLanding" class="col-12">

                        <div class="form-header">
                            <!-- Circles which indicates the steps of the form: -->
                            <div class="step-container">
                                <div class="step">
                                    <div class="step-icon">
                                        <img src="<?=get_template_directory_uri(); ?>/images/icon_register_user.png" />
                                    </div>
                                    <div class="step-text">
                                        <p>Thông tin người nhận thưởng</p>
                                    </div>
                                    <div class="circle-step" data-count="1/3"></div>
                                </div>
                                <div class="step">
                                    <div class="step-icon">
                                        <img src="<?=get_template_directory_uri(); ?>/images/icon_question.png" />
                                    </div>
                                    <div class="step-text">
                                        <p>Câu hỏi nhỏ</p>
                                    </div>
                                    <div class="circle-step" data-count="2/3"></div>
                                </div>
                                <div class="step">
                                    <div class="step-icon">
                                        <img src="<?=get_template_directory_uri(); ?>/images/icon_confirm.png" />
                                    </div>
                                    <div class="step-text">
                                        <p>Xác nhận thông tin</p>
                                    </div>
                                </div>
                                <div class="step" style="display: none"></div>
                            </div>
                        </div>
                        <div class="form-body">
                            <form id="regForm" >
                                <div class="alert hidden" id="alertMessage"></div>
                                <div class="tab" data-id="0">
                                    <div class="form-group">
                                        <label>Số Điện Thoại</label>
                                        <input type="text" id="prPhone" class="form-control" placeholder="Số ĐT sẽ được nạp tiền" name="prPhone">
                                        <p class="text-danger" id="errorPhone"></p>
                                    </div>
                                    <div class="form-group">
                                        <label>Họ Và Tên</label>
                                        <input type="text" id="prFullName" class="form-control" placeholder="Họ và Tên​" name="prFullName">
                                        <p class="text-danger" id="errorName"></p>
                                    </div>
                                    <div class="form-group">
                                        <div class="custom-control custom-radio custom-control-inline ">

                                          <input type="radio" <?= $gender['id'] ==='male' ? 'checked': '' ?> <?= $gender == '' ? 'checked' : '' ?> id="male" name="gender" class="custom-control-input proGender" value="Nam">
                                          <input type="radio" checked id="male" name="gender" class="custom-control-input proGender" value="Nam">
                                          <label class="custom-control-label" for="male">Nam</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                          <input type="radio" id="female" name="gender" class="custom-control-input proGender" value="Nũ">
                                          <label class="custom-control-label" for="female">Nữ</label>
                                        </div>

                                    </div>
                                    <i style="margin-bottom: 5px;">(*) Ngày Tháng Năm sinh theo CMND (Để xác thực thông tin khi bạn trúng thưởng)</i>
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <div class="form-group">
												<label>Ngày</label>
                                                <select class="form-control" id="prDate">
                                                    <?php
                                                        for($i=1; $i<=31; $i++):
                                                    ?>
                                                         <option value="<?= $i ?>"><?= $i ?></option>
                                                    <?php
                                                        endfor;
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-xs-4 pl-0 pl-sm-3">
                                            <div class="form-group">
												<label>Tháng</label>
                                                <select class="form-control" id="prMonth">
                                                    <?php
                                                        for($i=1; $i<=12; $i++):
                                                    ?>
                                                        <option value="<?= $i ?>"><?= $i ?></option>
                                                    <?php
                                                        endfor;
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-xs-4 pl-0 pl-sm-3">
                                            <div class="form-group">
												<label>Năm</label>
                                                <select class="form-control" id="prYear">
                                                    <?php
                                                    for($i=2010; $i>=1930; $i--):
                                                        ?>
                                                        <option value="<?= $i ?>"><?= $i ?></option>
                                                    <?php
                                                    endfor;
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <div class="form-group">
                                                <label>Tỉnh/Thành Phố</label>
                                                <select class="form-control" id="countryProvince"></select>
                                            </div>
                                        </div>
                                        <div class="col-xs-6">
                                            <div class="form-group">
                                                <label>Quận/Huyện</label>
                                                <select class="form-control" id="districtCity"></select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab" data-id="1">
                                    <div class="form-group">
                                        <label class="d-block">Thương hiệu thời trang công sở nào bạn thường mua hoặc sử dụng (có thể chọn nhiều phương án)</label>

                                        <?php
                                            $logoIcons=[['khatoco_logo', 'KHATOCO'], ['garco_logo', 'May 10'], ['viettien_logo', 'Viettien'], ['novelty_logo', 'Novelty'], ['an_phuoc_group', 'An Phước'], ['merriman_logo', 'Merriman'], ['owen_logo', 'Owen'], ['aritino_logo', 'Aristino'], ['vinhtien_logo', 'Vĩnh Tiến']];
                                        ?>
                                        <?php foreach ($logoIcons as $icKey => $icon) { ?>
                                            <div style="display: inline-flex; flex-direction: column; margin-bottom: 10px;">
                                                <div class="checkbox checkbox-img sm form-check-inline mb-0 mr-0">
                                                    <input id="choose-checkbox-img-<?= $icKey ?>" class="checkbox-question-1" type="checkbox" name="optionsRadios" value="<?=$icon[1]?>">
                                                    <label for="choose-checkbox-img-<?= $icKey ?>">
                                                        <div class="background" style="background: white url('<?=get_template_directory_uri(); ?>/images/logos/<?=$icon[0]?>.png') no-repeat; background-size: cover; background-position: 100% 100%;"></div>
                                                    </label>
                                                </div>
                                                <small class="w-100 text-center"><?= $icon[1] ?></small>
                                            </div>
                                            <?php } ?>

                                            <div style="display: inline-flex; flex-direction: column;" id="">
                                                <div class="checkbox checkbox-img sm form-check-inline mb-0 mr-0">
                                                    <input id="choose-checkbox-img-<?= $icKey+1 ?>" class="checkbox-question-1" type="checkbox" name="optionsRadios">
                                                    <label for="choose-checkbox-img-<?= $icKey+1 ?>">
                                                        <div class="background" style="background: white url('<?=get_template_directory_uri(); ?>/images/logos/<?=$icon?>.png') no-repeat; background-size: cover; background-position: 100% 100%;"></div>
                                                    </label>
                                                    <div id="wpDiffAnsw" style="margin-left: 30px;">
                                                        <input type="text" name="diffAnsw" class="form-control" id="diffAnsw" disabled>
                                                        <p class="m-0" id="errorDiffAnsw"></p>
                                                    </div>
                                                </div>
                                                <small class="text-center" style="
                                                    max-width: 57.6px;
                                                    display: block;
                                                ">Khác</small>
                                            </div>

                                    </div>
                                    <div class="form-group">
                                        <label class="d-block">Sản phẩm nào của Thời trang Khatoco bạn từng mua hoặc sử dụng? (có thể chọn nhiều phương án)</label>
                                        <?php
                                            $products = [['AO-SO-MI-TAY-NGAN', 'Sơ mi tay ngắn'], ['AO-SO-MI-TAY-DAI', 'Sơ mi tay dài'], ['AO-THUN','Áo thun, áo polo cộc tay'], ['Short', 'Quần short'], ['QUAN-KAKI', 'Quần kaki'], ['QUAN-TAY', 'Quần tây'], ['AO-THUN-LOT---TAY-NGAN', 'Áo thun tay ngắn'], ['AO-THUN-LOT-KO-TAY', 'Áo thun lót không tay'], ['QUAN-LOT', 'Quần lót'], ['TAT', 'Tất']];
                                        ?>
                                        <?php foreach ($products as $ipKey => $product) { ?>
                                        <div style="display: inline-flex; flex-direction: column;">
                                            <div class="checkbox checkbox-img clothes form-check-inline mb-0 mr-0">
                                                <input id="choose-checkbox-img--<?php echo $ipKey ?>" class="checkbox-question-2" type="checkbox" value="<?= $product[1] ?>" name="optionsRadios-">
                                                <label for="choose-checkbox-img--<?php echo $ipKey ?>">
                                                    <div class="background" style="background: white url('<?=get_template_directory_uri(); ?>/images/logos/<?= $product[0] ?>.png') no-repeat; background-size: cover; background-position: 100% 100%;"></div>
                                                </label>
                                            </div>
                                            <small class="w-100 text-center"><?= $product[1] ?></small>
                                        </div>

                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="tab" data-id="2" id="tab-cards">
                                    <div class="wp-items-card">
                                        <div class="items-card">
                                            <label>Khách hàng nhập mã số dự thưởng dưới lớp thẻ cào</label>
                                            <div style="display: flex;">
                                                <input type="text" name="pin" class="form-control" placeholder="" style="width: 100% !important;">&nbsp;
                                                <button type="button" class="addCards">Thêm</button>&nbsp;
                                                <button type="button" class="removeCards" style="background-color: #bbbbbb;">Xoá</button>
                                            </div>
                                            <p class="card-errors text-danger"></p>
                                        </div>


                                    </div>
                                </div>
                                <div class="tab" id="last-tab">
                                    <div class="confirm-informations text-center">
                                        <h5 style="
    font-weight: 700;
    text-transform: uppercase !important;
    color: #bb2216;
    margin-bottom: 25px;
">Mã số hợp lệ.</h5>
                                        <p>Thông tin đăng ký của bạn là:</p>
                                        <p class="mb-0">
                                            <b id="confirm-name"></b>
                                        </p>
                                        <p>
                                            <b style="
    color: gray;
" id="confirm-phone"></b>
                                        </p>
                                    </div>
                                </div>
                                <div id="btnFunction" style="/* overflow:auto; */display: flex;justify-content: space-between;margin-top: 15px;">
                                    <button type="button" id="prevBtn" onclick="nextPrev(-1, 'prev')" style="display: inline;">Quay lại</button>
                                    <button type="button" id="nextBtn" onclick="nextPrev(1, 'next')">Tôi đồng ý</button>
                                </div>
                                <div class="text-center mt-4">
                                    <a href="tel:1800585860" class="text-danger" style="font-size: 13px; text-transform: uppercase; font-weight: 600;">Hotline CSKH – 1800 585860 (miễn phí – giờ hành chính)</a>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.js"></script>

    <script type="text/javascript">
        var currentTab = 0; // Current tab is set to be the first tab (0)
        showTab(currentTab); // Display the current tab

        function showTab(n) {
            // This function will display the specified tab of the form...
            var x = document.getElementsByClassName("tab");
            x[n].style.display = "block";
            //... and fix the Previous/Next buttons:
            if (n == 0) {
                document.getElementById("prevBtn").style.display = "none";
            } else {
                document.getElementById("prevBtn").style.display = "inline";
            }
            if (n == (x.length-1)) {
                document.getElementById("nextBtn").innerHTML = "Tôi đồng ý";
            } else {
                document.getElementById("nextBtn").innerHTML = "Tiếp theo";
                if(jQuery(x[n]).attr('id') == 'tab-cards')
                    document.getElementById("nextBtn").innerHTML = "Gửi mã dự thưởng";

            }
            //... and run a function that will display the correct step indicator:
            fixStepIndicator(n)
        }

        function nextPrev(n, action) {

            //jQuery('#nextBtn').text('Tiếp theo')

            if(currentTab == 0){//handle check phone number
                var error = false;
                var phone = jQuery('#prPhone').val()
                jQuery.ajax({
                    url: "<?= admin_url('admin-ajax.php'); ?>",
                    type: 'post',
                    data: { action: 'checkPhonePromotion', phone: phone },
                    async: false,
                    beforeSend: function () {

                    },
                    success: function (response) {
                        response = jQuery.parseJSON(response)
                        if(response.status == 'errors'){
                            jQuery('#errorPhone').text(response.message)
                            jQuery('#prPhone').addClass('invalid')
                            error = true
                        }else{
                            jQuery('#errorPhone').text('')
                            jQuery('#prPhone').removeClass('invalid')

                        }
                    }
                })
                if(error)
                    return false;
            }

            if(currentTab == 2 && action=='next'){//handle validate
                var error = false
                var cards = new Array()


                if(!validateForm()) return false;//validate before check pin


                jQuery('.wp-items-card .items-card').each((index, item)=>{

                        var pin = jQuery(item).find('input[name="pin"]').val()
                        cards.push(pin)
                })

                jQuery.ajax({
                    url: "<?= admin_url('admin-ajax.php'); ?>",
                    type: "post",
                    data: { action: 'checkPINCode', cards: cards },
                    async: false,
                    beforeSend: function () {

                    },
                    success: function (response) {
                        var rsp = jQuery.parseJSON(response)
                        if(rsp.status =='errors'){
                            jQuery('.wp-items-card .items-card').each((index, item)=>{
                                var ipVal = jQuery(item).find('input[name="pin"]').val()
                                var allIpVal = jQuery(item).find('input[type="text"]')
                                if(rsp.code == ipVal){
                                jQuery(item).find('p.card-errors').text(rsp.message)
                                jQuery(allIpVal).addClass('invalid')

                                error = true


                                }else{
                                    jQuery(item).find('p.card-errors').text('')
                                    jQuery(allIpVal).removeClass('invalid')
                                }
                            })
                        }else{

                            var phone = jQuery('#prPhone').val()
                            var fullName = jQuery('#prFullName').val()
                            jQuery('#confirm-name').text(fullName)
                            jQuery('#confirm-phone').text(phone)

                        }

                    }
                })
                if(error)
                    return false
            }



            //this flag wii display current tab if error exists
            // This function will figure out which tab to display
            var x = document.getElementsByClassName("tab");
            // Exit the function if any field in the current tab is invalid:
            if (n == 1 && !validateForm()) return false;
            // Hide the current tab:
            lastTab = x.length
            if(currentTab == lastTab){
                x[currentTab-1].style.display = "none";
                jQuery('#alertMessage').addClass('hidden')
                //jQuery('#nextBtn').addClass('hidden')
            }else{
                x[currentTab].style.display = "none";
                //jQuery('#nextBtn').removeClass('hidden')
            }
            // Increase or decrease the current tab by 1:
            currentTab = currentTab + n;
            // if you have reached the end of the form...

            // if(jQuery(x[currentTab]).attr('id') == 'tab-cards'){
            //     console.log('herererererer')
            //     document.getElementById("nextBtn").innerHTML = 'Gửi mã dự thưởng';
            // }else{
            //     jQuery('#nextBtn').text('Tiếp theo')
            // }



            if (currentTab >= x.length) {

                // ... the form gets submitted:
                //document.getElementById("regForm").submit();

                var phone = jQuery('#prPhone').val()
                var fullName = jQuery('#prFullName').val()
                var dateOfBirth = jQuery('#prDate').val()+'-'+jQuery('#prMonth').val()+'-'+jQuery('#prYear').val()
                var address = 'Tỉnh/Thành Phố: ' + jQuery('#countryProvince option:selected').text() + ', Quận/Huyện: '+ jQuery('#districtCity option:selected').text()
                var gender = jQuery('.proGender:checked').val()
                var questions = new Array()
                var answer1 = new Array()
                var answer2 = new Array()
                //get data question 1
                jQuery('.checkbox-question-1:checked').each((index, item)=>{
                    if(jQuery(item).attr('id') != 'choose-checkbox-img-9')
                        answer1.push(jQuery(item).val())
                })
                jQuery('.checkbox-question-2:checked').each((index, item)=>{
                    answer2.push(jQuery(item).val())
                })

                //choose answ khac
                if(jQuery('#choose-checkbox-img-9').is(':checked')){
                    answer1.push(jQuery('#diffAnsw').val())
                }


                questions = {'answer1' : answer1, 'answer2' : answer2 }

                var cards = new Array()

                jQuery('.wp-items-card .items-card').each((index, item)=>{

                    var pin = jQuery(item).find('input[name="pin"]').val()
                    cards.push(pin)
            })


                jQuery.ajax({
                    url: "<?= admin_url('admin-ajax.php'); ?>",
                    type: 'post',
                    data: { action: 'registerPromotion', phone: phone, fullName: fullName, dateOfBirth: dateOfBirth, address: address, gender: gender, questions: questions, cards: cards},
                    beforeSend: function () {

                    },
                    success: function (response) {
                        var rsp = jQuery.parseJSON(response)
                        if(rsp.status =='errors'){
                            if(rsp.code === 'global'){
                                jQuery('#alertMessage').removeClass('hidden alert-success').addClass('alert-danger').text(rsp.message)
                                currentTab = 0
                                showTab(currentTab)
                                return false;
                            }
                            jQuery('.wp-items-card .items-card').each((index, item)=>{
                                var ipVal = jQuery(item).find('input[name="pin"]').val()
                                var allIpVal = jQuery(item).find('input[type="text"]')
                                if(rsp.code == ipVal){
                                jQuery(item).find('p.card-errors').text(rsp.message)
                                jQuery(allIpVal).addClass('invalid')
                            }else{
                                jQuery(item).find('p.card-errors').text('')
                                jQuery(allIpVal).removeClass('invalid')
                            }
                        })

                            //jQuery('#alertMessage').removeClass('hidden alert-success').addClass('alert-danger').text(rsp.message)
                            currentTab = 2
                            showTab(currentTab)
                        }else{
                            jQuery('#alertMessage').removeClass('hidden alert-danger').addClass('alert-success').html(rsp.message)
                            jQuery('#nextBtn').addClass('d-none')
                            jQuery('#prevBtn').addClass('d-none')
                            jQuery('#btnFunction').css('justify-content', 'center')
                            jQuery('#btnFunction').append('<button type="button" onclick="window.location.href=\'<?=get_home_url() ?>\'">Tôi đã nhận thông tin</button>')
                        }
                    }
                })
                return false;


            }

            // Otherwise, display the correct tab:
            showTab(currentTab);
            window.innerWidth > 480 && (jQuery('#promotion-rules').css( 'maxHeight', (jQuery('#formLanding').outerHeight() - jQuery('#wpVideos').outerHeight()-108)));
        }

        function validateForm() {
            // This function deals with validation of the form fields
            var x, y, i, valid = true;
            x = document.getElementsByClassName("tab");
            y = x[currentTab].getElementsByTagName("input");
            // A loop that checks every input field in the current tab:
            for (i = 0; i < y.length; i++) {

                //check phone
                if(y[i].name == 'prPhone'){
                    var phone = y[i].value;
                    //if(isNaN(phone) || phone.length!=10){
                    if(phone.length!=10){
                        document.getElementById('errorPhone').textContent= 'Số điện thoại không đúng đinh dạng.'
                        valid = false;
                    }else{
                        document.getElementById('errorPhone').textContent='';
                        valid = true;
                    }

                }

                // If a field is empty...
                if (y[i].value == "") {

                    valid = false;
                    // add an "invalid" class to the field:
                    y[i].className += " invalid";
                    if(y[i].name === 'prFullName'){
                        document.getElementById('errorName').textContent= 'Vui lòng nhập họ và tên.'
                        valid = false;
                    }

                    if(y[i].name === 'serial' || y[i].name === 'pin'){
                        jQuery(y[i]).closest('.items-card').find('p.card-errors').text('Vui lòng nhập mã số quay thưởng')
                        valid = false
                    }


                    if(y[i].name == 'diffAnsw'){
                        if(jQuery('#choose-checkbox-img-9').is(':checked'))
                            jQuery('#errorDiffAnsw').text('Vui lòng nhập câu trả lời khác.').css('color', 'red')
                        else
                            valid = true
                    }


                }else{
                    if(y[i].name === 'prFullName'){
                        document.getElementById('errorName').textContent='';
                        jQuery(y[i]).removeClass('invalid');
                    }

                    if(y[i].name === 'serial' || y[i].name === 'pin'){
                        jQuery(y[i]).closest('.items-card').find('p.card-errors').text('')
                        jQuery(y[i]).removeClass('invalid');

                    }

                    if(y[i].name == 'diffAnsw'){
                        jQuery('#errorDiffAnsw').text('')
                        jQuery(y[i]).removeClass('invalid')
                    }

                }
            }
            // If the valid status is true, mark the step as finished and valid:
            if (valid) {
                // console.log('currentTab: ' + currentTab)
                // console.log(document.getElementsByClassName("step")[currentTab])
                document.getElementsByClassName("step")[currentTab].className += " finish";
            }
            return valid; // return the valid status
        }

        function fixStepIndicator(n) {
            // This function removes the "active" class of all steps...
            var i, x = document.getElementsByClassName("step");
            for (i = 0; i < x.length; i++) {
                x[i].className = x[i].className.replace(" active", "");
            }
            //... and adds the "active" class on the current step:
            x[n].className += " active";
        }




        //get provinde

        jQuery(document).ready(function ($) {
            <?php if (!isset($_SERVER['HTTP_USER_AGENT']) || stripos($_SERVER['HTTP_USER_AGENT'], 'Chrome-Lighthouse') === false): ?>
                var privinceId = $('#countryProvince').val()
                function getAddressData(action, id){
                    return $.get("<?php echo admin_url('admin-ajax.php'); ?>", { action: 'user_service_ajax', do_ajax: action, id: id })
                }

                getAddressData('get_provinces', 0).then((response)=>{
                    var cities = $.parseJSON(response)
                   for(var pKey in cities.result){
                       $('#countryProvince').append(`<option value="${cities.result[pKey].provinceid}">${cities.result[pKey].name}</option>`)
                   }
                    privinceId = $('#countryProvince').val()

                    getAddressData('get_districts', privinceId).then((response)=>{
                            var provinces = $.parseJSON(response)
                            for(var pKey in provinces.result){
                                $('#districtCity').append(`<option value="${provinces.result[pKey].provinceid}">${provinces.result[pKey].name}</option>`)
                            }
                            //console.log(response)
                    })

                })

                $('#countryProvince').on('change', function () {
                        $('#districtCity').children('option').remove()
                        getAddressData('get_districts', $(this).val()).then((response)=>{
                            var provinces = $.parseJSON(response)
                            for(var pKey in provinces.result){
                            $('#districtCity').append(`<option value="${provinces.result[pKey].provinceid}">${provinces.result[pKey].name}</option>`)
                        }

                    })
                })
            <?php endif; ?>


            $('#countryProvince').select2({})
            $('#districtCity').select2({})





            $(document).on('click', '.wp-items-card .items-card button.addCards', function () {
                $('.wp-items-card').append(`<div class="items-card">
                                            <label>Khách hàng nhập mã số dự thưởng dưới lớp thẻ cào</label>
                                            <div style="display: flex;">&nbsp;
                                                <input type="text" name="pin" placeholder="" class="form-control" style="width: 100% !important;">&nbsp;
                                                <button type="button" class="addCards">Thêm</button>&nbsp;
                                                <button type="button" class="removeCards" style="background-color: #bbbbbb;">Xoá</button>
                                            </div>
                                            <p class="card-errors text-danger"></p>
                                        </div>`);
            })

            $(document).on('click', '.wp-items-card .items-card button.removeCards', function () {
                var allItems = $('.wp-items-card .items-card').length;
                if(allItems>1)
                    $(this).closest('.items-card').remove()
                else
                    $(this).closest('.items-card').find('input[type="text"]').val('')
            })

                $(document).on('click', '#choose-checkbox-img-9', function () {
                    if(window.innerWidth>480){
                        if($(this).is(':checked')){
                            $('#diffAnsw').removeClass('invalid');
                            $('#errorDiffAnsw').text('');
                            $('#wpDiffAnsw').show()

                        }else{
                            $('#diffAnsw').removeClass('invalid');
                            $('#errorDiffAnsw').text('');
                            $('#wpDiffAnsw').hide()
                        }
                    }else{
                        if($(this).is(':checked')){
                            $('#diffAnsw').removeAttr('disabled')
                            $('#diffAnsw').removeClass('invalid')
                            $('#errorDiffAnsw').text('');
                        }else{
                            $('#diffAnsw').attr('disabled', 'disabled')
                            $('#diffAnsw').removeClass('invalid')
                            $('#errorDiffAnsw').text('');
                        }
                    }


                })

            if(window.innerWidth<=480){

                //$('#wpVideos').insertAfter('#wpFormLanding');
                //$('#wpVideos').addClass('my-4');
                $('#wpVideos').remove()
                $('[href="#promotion-rules"]').trigger('click');

                //$('#upDiffAns').addClass('clearfix');
            }else{
                $('#wpDiffAnsw').hide()
                $('#diffAnsw').removeAttr('disabled')
            }
            window.innerWidth > 480 && ($('#promotion-rules').css( 'maxHeight', ($('#formLanding').outerHeight() - $('#wpVideos').outerHeight()-108)));

                $(document).on('focus', '.wp-items-card .items-card input[name="pin"]', function () {
                    $(this).removeClass('invalid')
                    $(this).closest('.items-card').find('.card-errors').text('')
                })





        })



    </script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- Include all compiled plugins (below), or include individual files as needed -->
<!--<script src="js/bootstrap.min.js"></script>-->
</body>
</html>
<?php wp_footer(); ?>
