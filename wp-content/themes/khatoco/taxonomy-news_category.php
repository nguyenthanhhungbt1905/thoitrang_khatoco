<?php get_header();
$tag_slug = get_query_var('term');
$tax = get_query_var('taxonomy');
$tag = get_term_by('slug', $tag_slug, $tax); 
$paged = (get_query_var('page')) ? get_query_var('page') : 1;

$posts_per_page = 12;

?>
<div class="block-outer block-news">
    <div class="container clearfix fluid">
        <div class="block-title-outer"><h1 class="block-title block-title-inner condensed"><span><?php single_tag_title(); ?></span></h1></div>
        <div class="block-inner clearfix"><?php $args = array('posts_per_page' => $posts_per_page, 'post_type' => 'news', $tax => $tag_slug, 'orderby' => 'date', 'order' => 'DESC', 'paged' => $paged);
            $hot_news = new WP_Query($args);
            $p_counts = $hot_news->found_posts;
            $big = 999999999;
            if($hot_news->have_posts()):
                while($hot_news->have_posts()):
                    $hot_news->the_post(); 
                    display_each_news(get_the_id());
                endwhile;
            ?>
            <div class="pagination clearfix col-md-12">
                <div class="grid5 fl total"><?php $p_start = ($paged - 1)*$posts_per_page + 1;
                    $p_end = $paged*$posts_per_page;
                    $p_end = ($p_end > $p_counts) ? $p_counts : ($paged*$posts_per_page); 
                    $pagenum_link = html_entity_decode( get_pagenum_link() );
                    $url_parts    = explode( '?', $pagenum_link );
                    $pagenum_link = trailingslashit( $url_parts[0] ) . '%_%';
                    ?>
                    <p><?php echo 'Từ '. $p_start .' đến '. $p_end .' (trong '. $p_counts .' sản phẩm)'; ?></p>
                </div>
                <div class="grid7 fr paginate clearfix">
                    <ul class="list-style-none fr">
                        <div id="paginate-hidden" class="hidden">
                            <?php 
                                echo paginate_links( array(
                                    'base' => $pagenum_link,
                                    'format' => '?page=%#%',
                                    'show_all' => true,
                                    'prev_text' => '',
                                    'next_text' => '',
                                    'current' => max( 1, $paged ),
                                    'total' => $hot_news->max_num_pages
                                ));  wp_reset_postdata(); 
                            ?>
                        </div>
                        <li class="fl"><a href="" id="page_n_prev" class="prev page-numbers"></a></li>
                        <li class="fl select-holder" id="paginate-show">
                            <p>
                                Trang <select class="" name="paginate" id="paginate" ></select> trong <?php echo $hot_news->max_num_pages; ?>
                            </p>
                        </li>
                        <li class="fl"><a href="" id="page_n_next" class="next page-numbers"></a></li>
                    </ul>
                    <script type="text/javascript">
                        jQuery(document).ready(function() {
                            var page_n = [];
                            var page_n_curt = '';
                            var page_n_prev = '%_%';
                            var page_n_next = '%_%';

                           
                            jQuery('#paginate-hidden').find('.page-numbers').each(function() {
                                if(jQuery(this).hasClass('current'))
                                    page_n_curt = jQuery(this).text();
                                else if(jQuery(this).hasClass('next'))
                                    page_n_next = jQuery(this).attr('href');
                                else if(jQuery(this).hasClass('prev'))
                                    page_n_prev = jQuery(this).attr('href');
                                else 
                                    page_n.push(jQuery(this).attr('href'));
                            });
                            page_n.splice(page_n_curt-1, 0, '');
                            if(page_n[0] == '') page_n[0] = '?page=1';
                            if(page_n[page_n.length-1] == '') page_n[page_n.length-1] = '?page='+ page_n.length;
                            console.log(page_n)
                            if(page_n_prev != '%_%')
                                jQuery('#page_n_prev').attr('href', page_n_prev);
                            else
                                jQuery('#page_n_prev').addClass('disabled');

                            if(page_n_next != '%_%')
                                jQuery('#page_n_next').attr('href', page_n_next);
                            else
                                jQuery('#page_n_next').addClass('disabled');

                            var html = '';
                            for(var i in page_n) {
                                var index = parseInt(i);
                                
                                if((index+1) == page_n_curt) {
                                    html += '<option selected="">'+ (index+1) +'</option>';
                                    continue;
                                }
                                var redirect_uri = page_n[i];
                                if(redirect_uri != '')
                                    html += '<option data-replace="'+ redirect_uri +'">'+ (index+1) +'</option>';
                                /*else
                                    html += '<option data-replace="">'+ (index+1) +'</option>';*/
                            }
                            jQuery('#paginate').append(html);

                            jQuery('.page-numbers.disabled').click(function(event) {
                                event.preventDefault();
                                return false;
                            });

                            jQuery('#paginate').change(function(event) {
                                var replace = jQuery(this).find('option:selected').attr('data-replace');
                                window.location.replace(replace);
                                return false;
                            });
                        });
                    </script>
                </div>
            <?php                
                else:
                    echo '<p style="text-align:center;padding:50px 0 100px">Không tìm thấy bài đăng phù hợp</p>';
                endif; 
            ?>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>