﻿<?php 
/*
Template Name: Đăng ký lấy mã test 1
*/?>
<?php get_header() ?>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/dncss/vendors/bootstrap/bootstrap.min.css" crossorigin="anonymous">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/dncss/css/style.css" crossorigin="anonymous">
<!-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous"> -->
<style type="text/css">
    #footer{
        margin-top: 0!important;
    }
    select,input{
        color: black;
    }
    .display{
        display: none!important;
    }

    .form-data{
        margin-left: 80px
    }

    @media screen and (max-width: 768px) {
        .form-data{
            margin-left: 0px;
        }
    }
</style>
<body>

    <div class="header-content pb-5" style="background-color: white;">
       

        <div class="banner container">
            <div class="content row mt-5">
                <div class="col-md-4">
                    <div class="row"><div class="col-md-6" style="border-top:3px solid white"></div></div>
                    <div class="row pt-3 pb-3">
                        <h3>TRI ÂN KHÁCH HÀNG</h3>
                    </div>
                    <div class="row"><div class="offset-6 col-md-6" style="border-top:3px solid white"></div></div>

                    <div class="row pt-5">
                        <p>
                            Thời trang <span class="text-org">Khatoco</span> xin cảm ơn sự tin tưởng, ủng hộ và đồng hành của quý khách trong suốt thời gian vừa qua. Thay lời tri ân, Khatoco xin tặng quý khách
                            <span class="text-org">10 phiếu ưu đãi 35%</span> và <span class="text-org">một món quà</span>.
                        </p>
                        <p>
                            Quý khách có thể chia sẻ một phần ưu đãi này tới những người mình quan tâm. Và đừng quên khám phá món quà dành cho quý khách ở mặt sau bức thư này nhé!
                        </p>
                    </div>
                </div>
                <div class="col-md-8">
               
                
                    <div data-ng-app="khatocoUserProfile" class="block-outer block-account form-data" style="">
                        <div class="">
                            <div data-ng-controller="UserController" class="container fluid">
                                <div  class="row">
                                    <div class="col-xs-12">
                                        <!-- <div class="block-title-outer"><h2 class="block-title block-title-inner condensed">Đăng ký lấy mã</h2></div> -->
                                        <p class="text-org" style="font-size: 25px"><i class="fa fa-caret-left"></i> <span class="ml-2">Mã Giảm Giá</span> </p>
                                        <p class="clearfix ml-3">Số điện thoại: {{return.phone}}</p>
                                    </div>
                                </div>
                                <form name="registerForm" id="registerForm" method="POST" data-ng-submit="formSubmit()" class="ml-3">
                                    <div  class="row">
                                        <div class="col-md-6 col-xs-12">
                                            <h2>Thông tin tài khoản</h2>
                                                            
                                            <p class="clearfix">
                                                <label for="fullname">Họ và tên <span class="required">*</span></label>
                                                <input class="field" data-ng-model="datapost.fullname" value="{{return.fullname}}" type="text" placeholder="Họ và Tên"required />
                                            </p>
                                            <p class="clearfix">
                                                <label for="email">Email </label>
                                                <input class="field" data-ng-model="datapost.email" value="{{return.email}}" type="email" placeholder="Email"/>
                                            </p>
                                             
                            
                                            
                                        </div>
                                        <div class="col-md-6 col-xs-12">
                                            <h2>Địa chỉ</h2>
                                             <p class="clearfix">
                                                <label for="addr">Tên đường, số nhà</label>
                                                <input class="field" data-ng-model="datapost.address" value="{{return.address}}" type="text" placeholder="Tên đường, số nhà"/>
                                            </p>
                                            <p class="birthday" id="province">
                                                <label for="province">Tỉnh/TP</label>
                                                <select data-ng-model="datapost.province"  -ng-options="province as province.name for province in provinces" data-ng-change="setCurrentProvince()" name="province" id="province" class="address-select">
                                                    <option value="" selected>-- Lựa chọn --</option>
                                                </select>
                                            </p>
                                            <p class="birthday" id="district" data-ng-show="datapost.province.provinceid">                        
                                                <label for="district" data-ng-show="datapost.province.provinceid">Quận/Huyện</label>
                                                <select data-ng-model="datapost.district" data-ng-options="district as district.name for district in districts" data-ng-change="setCurrentDistrict()" name="district" id="district" class="address-select">
                                                    <option value="" selected>-- Lựa chọn --</option>
                                                </select>
                                            </p>
                                            <p class="birthday last" id="ward" data-ng-show="datapost.district.districtid && datapost.province.provinceid">                       
                                                <label for="ward" data-ng-show="datapost.district.districtid && datapost.province.provinceid">Phường/Xã</label>
                                                <select data-ng-model="datapost.ward" data-ng-options="ward as ward.name for ward in wards" data-ng-change="setCurrentWard()" name="ward" id="ward" class="address-select">
                                                    <option value="" selected>-- Lựa chọn --</option>
                                                </select>
                                            </p>
                                           
                                           
                                           
                                        </div>
                                    </div>

                                    <div id="member_info" class="display">
                                        <div  class="row">
                                            <div class="col-md-6">
                                                <p class="clearfix">
                                                    <label for="gender">Giới tính <span class="required">*</span></label>
                                                    <select data-ng-model="datapost.gender" data-ng-options="gen as gen.name for gen in genders" id="gender" >
                                                        <option value="" selected>Giới tính</option>
                                                    </select>
                                                </p>
                                                <div class="row" data-ng-hide="return.code == -1">
                                                    <div class="col-md-6">
                                                        <p for=""><b class="text-org">Mã khách hàng:</b><br> <span>{{return.card_code}}</span></p>
                                                    </div>
                                                    <div class="col-md-6"> 
                                                        <p for=""><b class="text-org">Hạng Thẻ:</b><br> <span>{{return.cType}}</span></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="birthday">Ngày sinh <span class="required">*</span></label> 
                                                <div  class="row">
                                                    <div class="col-md-4 " >   
                                                      
                                                        <p class="clearfix">
                                                            <select name="" data-ng-model="datapost.birthday.day" id="date">
                                                                <option value="">Ngày</option>
                                                                <option data-ng-repeat="day in days" data-ng-selected="datapost.birthday.day==day" value="{{day}}">{{day}}</option>
                                                            </select>
                                                        </p>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <p class="clearfix">                        
                                                            <select name="" data-ng-model="datapost.birthday.month" id="month">
                                                                <option value="">Tháng</option>
                                                                <option data-ng-repeat="month in months" data-ng-selected="datapost.birthday.month==month" value="{{month}}">{{month}}</option>
                                                            </select>
                                                        </p>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <p class="clearfix">                        
                                                            <select name="" data-ng-model="datapost.birthday.year" id="year" >
                                                                <option value="">Năm</option>
                                                                <option data-ng-repeat="year in years" data-ng-selected="datapost.birthday.year==year" value="{{year}}">{{year}}</option>
                                                            </select>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="row"  >
                                            
                                                    <div class="col-md-12" data-ng-hide="return.code == -1">
                                                        <p for=""><b class="text-org">Điểm Tích Lũy:</b><br> <span>{{return.cardbonnuspoint}}</span></p>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                           
                                           
                                        </div>
                                        
                                        
                                       
                                        <div id="mode_old_user">
                                            <h2>Thiết lập mật khẩu</h2>
                                            <div  class="row">
                                                <div class="col-md-6">
                                                    
                                                    <p class="clearfix">
                                                        <label for="password">Mật khẩu <span class="required">*</span></label>
                                                        <input class="field" data-ng-model="datapost.password" value="" type="password" placeholder="Mật khẩu" id="password" />
                                                    </p>
                                                </div>
                                                <div class="col-md-6">
                                                    <p class="clearfix">                        
                                                        <label for="repassword">Nhập lại mật khẩu <span class="required">*</span></label>
                                                        <input class="field" data-ng-model="datapost.repassword" value="" type="password" placeholder="Xác nhận mật khẩu" id="repassword"/>
                                                    </p>              
                                                </div> 
                                            </div>
                                        </div>

                                           
                                       
                                    </div>
                                    <div  class="row">
                                        <div class="col-md-12">
                                            <p class="clearfix" id="more_info_to_register_member">
                                                <label for="checkbox_required checkbox_custom">
                                                    <input style="width: auto; float: none; vertical-align: middle;" type="checkbox" name="checkbox_required" id="checkbox_required" onchange="setCheckedBox()" value="on"> Bạn có muốn trở thành, thành viên của Khatoco.
                                                    <span class="checkmark"></span>
                                                </label>
                                            </p>
                                           
                                            <p class="clearfix" id="submit_voucher">                       
                                                <button class="btn  btn-custom text-uppercase" type="submit">
                                                    <span data-ng-hide="loading">Lấy mã</span>
                                                    <span data-ng-show="loading">Lấy mã <i class="fa fa-refresh fa-spin"></i></span>
                                                </button>
                                            </p>
                                             
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
              

                </div>
            </div>
        </div>
        <div class="container">
            <div class="row mt-4"><div class="offset-4 col-md-4" style="border-top:3px solid white"></div></div>
        </div>
        <div class="container mt-4" style="color: white">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="text-org">HỆ THỐNG PHẤN PHỐI</h3>
                    <p>HỆ THỐNG SHOWROOM</p>
                    <p class="pt-3">
                        <p class="text-org">HỒ CHÍ MINH</p> 
                        <p class="ml-5">- 259A Hai BÀ Trưng,P.6,Q.3,TP.HCM </p>
                        <p class="ml-5"> - 282 Quang Trung,P.10,Q.Gò Vấp, Tp.HCM </p>
                        <p class="ml-5"> - 969 Cách Mạng Tháng 8, P.7, Q.Tân Bình, Tp.HCM </p>
                        <p class="ml-5"> - 319 Cách Mạng Tháng 8, P.12, Q.10, Tp.HCM </p>
                        <p class="ml-5"> - 6M Nguyễn Ảnh Thủ, P.Trung Mỹ TY, Q.12, Tp.HCM</p>
                    </p>
                    <p>
                        <p class="text-org">CẦN THƠ</p> 
                        <p class="ml-5">- 3A Mậu THN,P.XUN KHÁNH,Q.NINH KIỀU,TP.CẦN THƠ </p>
                        <p class="ml-5"> - 18 NGUYỄN TRÃI,P.AN HỘ,Q.NINH KIỀU,TP.CẦN THƠ</p>
                    </p>
                    <p>
                        <p class="text-org">NHA TRANG</p> 
                        <p class="ml-5">- SỐ B4 CHUNG CƯ VĨNH PHƯỚC, ĐƯỜNG 2/4,TP.NHA TRANG </p>
                        <p class="ml-5"> - 80C QUANG TRUNG, P.LỘC THỌ, TP.NHA TRANG </p>
                        <p class="ml-5"> - 221 THỐNG NHẤT, P.PHƯƠNG SÀI, TP.NHA TRANG </p>
                        <p class="ml-5"> - TRUNG TÂM THƯƠNG MẠI NHA TRANG, 20 TRẦN PHÚ, TP.NHA TRANG</p>
                    </p>
                    <p>
                        <p class="text-org">BÌNH ĐỊNH</p> 
                        <p class="ml-5">- 164 NGUYỄN THÁI HỌC, P.NGÔ MY, TP.QUY NHƠN, BÌNH ĐỊNH </p>
                        <p class="ml-5"> - 231 LÊ HỒNG PHONG, P.LÊ HỒNG PHONG, TP.QUY NHƠN, BÌNH ĐỊNH</p>
                    </p>
                    <p>
                        <p class="text-org">ĐỒNG NAI</p> - 1137 PHẠM VĂN THUẬN,(ĐỐI DIỆN NGÃ BA BẢY CƯA), KHU PHỐ 1,P.TN TIẾN, TP.BIÊN HÒA
                    </p>

                        <h5 class="text-org pt-3">LIÊN HỆ:</h5>
                        <p>Công ty tnhh KhATOCO Võ Thị Sáu, phường Vĩnh Nguyên, thành phố Nha Trang, tỉnh Khánh Hòa, Việt Nam 07 ĐT:(84) 258.388 9978 - Fax: (84) 258.388 6422 CHỨNG NHẬN. Giấy chứng nhận đăng ký kinh doanh số 4200485207 do Sở Kế hoạch Đầu tư Tỉnh Khánh Hòa cấp</p>
                </div>
            </div>
        </div>
    </div>
     <div id="popup" class="popup close-wrap hidden">
        <div class="close-bg"></div>
        <div class="popup-inner">
            <div id="popup-message" class="message-wrap row clearfix">
            </div>
            <a href="#close" class="close">x</a>
            <div class="button-close row clearfix" id="popup_close">
                
            </div>
        </div>
    </div>
</body>
<script type="text/javascript">
    var apiUrl = "<?php echo admin_url('admin-ajax.php'); ?>";
    var app = angular.module("khatocoUserProfile", []);
    var phone = 0+location.pathname.split('/')[2];
    app.service('userService', ['$http', '$q', '$timeout', function($http, $q, $timeout){

        var getAddressInfo = function(action, id){
            return $http({
                method: 'GET',
                url: apiUrl,
                params: {action: 'user_service_ajax', do_ajax: action, id: id,cellphone:phone}
            });
        };

         var getCardUserInfo = function (data) {
            var d = $q.defer();
            $timeout(function () {
                $http({
                    method: 'POST',
                    url: apiUrl,
                    params: {action: 'user_service_ajax', do_ajax: 'get_user_for_sms',cellphone:phone},
                    data: data
                }).success(function (data, status, headers) {
                    d.resolve(data);
                }).error(function (data, status, headers) {
                    d.reject(data);
                });
            }, 1000);

            return d.promise;
        };


        var saveUserInfo = function(data){
            var d = $q.defer();

            $timeout(function () {
                $http({
                    method: 'POST',
                    url: apiUrl,
                    params: {action: 'user_service_ajax', do_ajax: 'save_user_info_sms'},
                    data: data,
                    //headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'}
                }).success(function(data, status, headers){
                    d.resolve(data);
                }).error(function(data, status, headers){
                    d.reject(data);
                });
            }, 1000);

            return d.promise;
        };

        return {
            getAddressInfo: getAddressInfo,
            saveUserInfo: saveUserInfo,
             getCardUserInfo: getCardUserInfo
        };

       
        
    }]);

    app.controller('UserController', ['$scope', '$sce', 'userService', function($scope, $sce, userService){
        $scope.submitMessage = '';
        $scope.loading = false;

        $scope.years = [2010,2009,2008,2007,2006,2005,2004,2003,2002,2001,2000,1999,1998,1997,1996,1995,1994,1993,1992,1991,1990,1989,1988,1987,1986,1985,1984,1983,1982,1981,1980,1979,1978,1977,1976,1975,1974,1973,1972,1971,1970,1969,1968,1967,1966,1965,1964,1963,1962,1961,1960,1959,1958,1957,1956,1955,1954,1953,1952,1951,1950,1949,1948,1947,1946,1945,1944,1943,1942,1941,1940,1939,1938,1937,1936,1935,1934,1933,1932,1931,1930];
        $scope.months = [1,2,3,4,5,6,7,8,9,10,11,12];
        $scope.days = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31];

        $scope.wards = [];
        $scope.districts = [];
        $scope.provinces = [];
        $scope.datapost = {};
        $scope.return = {};
        $scope.return.phone = phone;
        $scope.genders = [{id: 'male', name: 'Nam'}, {id: 'female', name: 'Nữ'}];

        $scope.datapost.province = {};
        $scope.datapost.district = {};
        $scope.datapost.ward = {};
        $scope.datapost.gender = {};
        $scope.datapost.birthday = {};
        $scope.datapost.address = '';
        $scope.datapost.cellphone = phone;
        $scope.datapost.nonce = "<?php echo wp_create_nonce('register_nonce_action'); ?>";
        $scope.datapost.redirect = "<?php echo home_url('/'); ?>";

        $scope.userInfo = {};
        $scope.phoneMessage = '';

        $scope.datapost.captcha = '';

        console.log($scope.datapost);

        $scope.error = false;

        $scope.getWards = function(districtid){
            userService.getAddressInfo('get_wards_sms', districtid)
                .success(function(data, status, headers){
                    if(data.success == 'true'){
                        $scope.wards = $scope.objectToArray(data.result);
                        if(data.current){
                            $scope.datapost.ward = data.result[data.current]
                        }
                    }
                })
                .error(function(data, status, headers){
                    $scope.error = true;
                });
        };

        $scope.getDistricts = function(provinceid){
            userService.getAddressInfo('get_districts_sms', provinceid)
                .success(function(data, status, headers){ //console.log(data);
                    if(data.success == 'true'){
                        $scope.districts = $scope.objectToArray(data.result);
                        if(data.current){
                            $scope.datapost.district = data.result[data.current];
                            $scope.getWards(data.current);
                        }
                    }
                })
                .error(function(data, status, headers){
                    $scope.error = true;
                });
        };

        $scope.getProvinces = function(){
            userService.getAddressInfo('get_provinces_sms', 0)
                .success(function(data, status, headers){
                    if(data.success == 'true'){
                        $scope.provinces = $scope.objectToArray(data.result);
                        // console.log($scope.provinces)
                        if(data.current){
                            $scope.datapost.province = data.result[data.current];
                            $scope.getDistricts(data.current);
                        }
                    }
                })
                .error(function(data, status, headers){
                    $scope.error = true;
                });
        };

        $scope.getProvinces();

        if($scope.error == true){
            alert('Không thể kết nối đến server, vui lòng F5 và thử lại!'); return;
        }

        $scope.formSubmit = function(){
           

            $scope.loading = true;
            $scope.submitMessage = '';

            console.log( $scope.datapost );
             // return false;

            userService.saveUserInfo($scope.datapost)
                .then(function(data){
                    // alert('a')
                    if( data.success ){

                   
                        

                        document.getElementById('popup-message').innerHTML = '<p>'+ data.message +'</p>';
                         document.getElementById('popup_close').innerHTML = '<p><a href="<?php echo home_url("/"); ?>" id="link" class="button">ok</a></p>';
                        
                        document.getElementById('popup').className = document.getElementById('popup').className.replace(/hidden/i, '');

                      
                    }else{

                        document.getElementById('popup-message').innerHTML = '<p>'+ data.message +'</p>';
                        document.getElementById('popup_close').innerHTML = '<p><a href="javascript:;" id="link" class="button">Đóng lại</a></p>';                   }
                        document.getElementById('popup').className = document.getElementById('popup').className.replace(/hidden/i, ''); 
                    
                    $scope.loading = false;
                    // document.location.href = '<?php echo home_url("/"); ?>';

                }, function(err){
                    $scope.submitMessage = 'Không thể kết nối đến máy chủ, F5 để thử lại.';
                    $scope.loading = false;
                });
        };

        $scope.setCurrentProvince = function(){
            $scope.getDistricts($scope.datapost.province.provinceid);
        };

      
        $scope.setCurrentDistrict = function(){
            $scope.getWards($scope.datapost.district.districtid);
        };

        $scope.objectToArray = function(object){
            var result = [];
            for(ob in object){
                result.push(object[ob]);
            }
            return result;
        };
        $scope.getUserInfo = function(){
            userService.getCardUserInfo($scope.datapost)
            .then(function (data) {
                if (data.success) {
                    if(data.message === 'Đây là thành viên mới'){
                        // document.getElementById('popup-message').innerHTML = '<p>' + data.message + '</p>';
                        // document.getElementById('popup').className = document.getElementById('popup').className.replace(/hidden/i, '');
                        //  document.getElementById('popup_close').innerHTML = '<p><a href="<?php echo home_url("/"); ?>" id="link" class="button">ok</a></p>';
                        document.getElementById('district').classList.add('display');
                        document.getElementById('ward').classList.add('display');
                        document.getElementById('province').classList.remove('birthday');
                        $scope.return.code= -1;

                    }else{
                        // $scope.return.code= -1;
                        document.getElementById('more_info_to_register_member').classList.add('display');
                        document.getElementById('mode_old_user').innerHTML = "";
                         
                        document.getElementById('member_info').classList.remove('display');
                        $scope.return.cellphone = data.message.phone
                        $scope.return.fullname = data.message.fullname
                        $scope.return.email = data.message.email
                        $scope.datapost.birthday = data.message.birthday;
                        $scope.return.email = data.message.email
                        $scope.return.username = data.message.username
                        // alert( $scope.return.username)
                        $scope.return.address = data.message.address
                        $scope.return.card_code = data.message.card_code
                        $scope.datapost.fullname = data.message.fullname
                        $scope.datapost.email = data.message.email
                        // document.getElementById('province').value = "object:"+data.result.province
                        // console.log($scope.return.cellphone)
                        // document.getElementById('phone').textContent = data.result.phone;
                        // document.getElementById('popup-message').innerHTML = '<p>' + data.message + '</p>';
                        // document.getElementById('popup').className = document.getElementById('popup').className.replace(/hidden/i, '');

                        // window.location.href ='http://khatocofashion.net/';
                        // window.location.href = '<?php echo home_url("/"); ?>';
                        for(genkey in $scope.genders){
                            if(data.message.gender['id'] == $scope.genders[genkey]['id']){
                                $scope.datapost.gender = $scope.genders[genkey];
                            }
                        }
                        // if(data.message.card == 0){
                        //     $scope.return.code= -1;
                        // }else{
                            // 
                        // }
                        // alert( $scope.return.code)
                        switch(data.message.card)
                        {
                            case 'DIAMONDNB':
                                $scope.return.cType= 'Thẻ kim cương';
                                break;
                            case 'DIAMOND':
                                $scope.return.cType= 'Thẻ kim cương';
                                break;
                            case 'GOLD':
                                $scope.return.cType= 'Thẻ vàng' ;
                                break;
                            case 'GOLDNB':
                                $scope.return.cType= 'Thẻ vàng' ;
                                break;
                            case 'SILVER':
                                $scope.return.cType= 'Thẻ bạc';
                                break;
                            case 'SILVERNB':
                                $scope.return.cType= 'Thẻ bạc';
                                break;
                            case 'NOIBO':
                                $scope.return.cType= 'Thẻ nội bộ' ;
                                break;
                            default :
                                $scope.return.cType= 'Thành viên';
                        }
                        var nf = Intl.NumberFormat();
                        $scope.return.cardbonnuspoint = nf.format(data.message.orderRevenue);
                    }    
                }
                else {

                    if (data.message) {
                        // $scope.submitMessage = data.message;
                        // document.getElementById('more_info_to_register_member').classList.remove('display');
                        $scope.return.phone = '';
                        if(data.message !== 'Đây là thành viên mới'){
                            document.getElementById('popup-message').innerHTML = '<p>' + data.message + '</p>';
                            document.getElementById('popup').className = document.getElementById('popup').className.replace(/hidden/i, '');
                             document.getElementById('popup_close').innerHTML = '<p><a href="<?php echo home_url("/"); ?>" id="link" class="button">ok</a></p>';
                            
                        }
                       
                       
                    }
                    $scope.return.code= -1;
                }
                $scope.loading = false;
               
            }, function (err) {
                $scope.return.code= -1;
                $scope.submitMessage = 'Không thể kết nối đến máy chủ, F5 để thử lại.';
                $scope.loading = false;
            });
        };
        $scope.getUserInfo();
        
    }]);
    function setCheckedBox(){
           
            if(document.getElementById('checkbox_required').checked){
                document.getElementById('member_info').classList.remove('display');
                // console.log(document.getElementById('registerForm').getAttribute('data-ng-submit'));
                document.getElementById('gender').setAttribute("required","");
                document.getElementById('date').setAttribute("required","");
                document.getElementById('month').setAttribute("required","");
                document.getElementById('year').setAttribute("required","");
                document.getElementById('password').setAttribute("required","");
                document.getElementById('repassword').setAttribute("required","");

                return true;
            }else{
                document.getElementById('member_info').classList.add('display');
                document.getElementById('gender').removeAttribute("required");
                document.getElementById('date').removeAttribute("required");
                document.getElementById('month').removeAttribute("required");
                document.getElementById('year').removeAttribute("required");
                document.getElementById('password').removeAttribute("required");
                document.getElementById('repassword').removeAttribute("required");
                return false;
            }
        };

</script>
<?php get_footer() ?>
