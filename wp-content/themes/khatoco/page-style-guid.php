<?php
/*
Template Name: Page Style Guid
*/
get_header(); ?>
    <section id="banner-top">
        <div id="banner-slide">

                <div class="detail-slide">
                    <div class="image-filter"
                         style="background-image:url('<?php echo get_template_directory_uri() . "/images/new/banner.jpg" ?>')"></div>
                    <!--                <img class="img-fluid" src="-->
                    <?php //echo get_template_directory_uri()."/images/new/banner.jpg" ?><!--" alt="banner" >-->
                </div>
        </div>
    </section>
    <section id="style-guide-overview">
        <?php
        $categoryList = get_style_guide_category();
//        var_dump($categoryList);die;
        ?>
        <div class="container">
            <?php
            foreach ($categoryList as $key => $categoryItem) { ?>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="style-guide-group <?php if ($key == 1) {
                            echo "row-reverse";
                        } ?>">
                            <div class="style-guide-item">
                                <img class="img-fuild" src="<?php echo $categoryItem['image_preview'] ?>" alt="<?php echo $categoryItem['title'] ?>">
                            </div>
                            <div class="style-guide-item">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <a class="visible-mb" href="<?php echo $categoryItem['link'] ?>">
                                            <p class="name"><?php echo $categoryItem['title'] ?></p>
                                        </a>
                                        <p class="name hidden-mb"><?php echo $categoryItem['title'] ?></p>
                                        <p class="short-description"> <?php echo $categoryItem['description'] ?></p>
                                        <a href="<?php echo $categoryItem['link'] ?>">
                                            XEM THÊM <i class="fa fa-angle-double-right" aria-hidden="true"></i>

                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php if($categoryItem['slug'] == 'cach-phoi-do-nam'){ ?>
                    <div class="row mb-visible">
                        <div class="col-xs-12">
                            <div class="row">
                                <?php $relatedPost = get_limit_with_category($categoryItem['id'],3); ?>
                                <?php foreach($relatedPost as $post){ ?>
                                <div class="col-xs-12">
                                    <div class="related-item">
                                        <div class="image-side">
                                            <img class="img-fluid" src="<?php echo get_template_directory_uri()."/images/new/test-image.png"?>" alt="">
                                        </div>
                                        <div class="content-side">
                                            <a href="<?php echo $post['link'] ?>">
                                                <p class="name">
                                                    <?php echo $post['title'] ?>
                                                </p>
                                            </a>
                                            <p class="description">
                                                <?php
                                                echo wp_trim_words($post['short_description'], 10, '...');
                                                  ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                        <?php } ?>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
            <?php } ?>
        </div>
    </section>
<?php echo get_template_part('partials/event-banner'); ?>
<?php get_footer(); ?>