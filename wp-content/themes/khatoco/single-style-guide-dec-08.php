<?php get_header();
global $post;
$tax = 'style-guide-category';
$term = get_the_terms($post, $tax);
$term = array_values($term);
$term = $term[0];
$terms = get_terms( $tax, array( 'hide_empty' => 0 ) );
$post_id = $post->ID;
 ?>
<style type="text/css">
	.item-lists.item-products li.item img{
		/*height: 400px !important;*/
		object-fit: cover;
    	object-position: top;
	}
    @media (max-width: 411px) {
        .block-style-guide .item-lists .item-outer{
            border: 0 !important;
        }
        .block-style-guide .item-lists.item-products .item h4 a{
            font-size: 15px;
        }
    }
</style>
<div class="block-outer block-style-guide">
	<div class="block-inner container fluid clearfix">
		<div class="col-xs-12 top-widget">
			<ul class="left-widget list-style-none">
				<?php foreach ($terms as $tkey => $t) {
                                    $splitName = explode(" - ", $t->name);
					echo '<li '. ( $t->term_id == $term->term_id ? 'class="current-menu-item"':'' ) .'><a href="'. get_term_link( $t, $tax ) .'" title="'. $t->name .'">'. $splitName[0];
					if (array_key_exists(1, $splitName))
						echo '<span>'.$splitName[1].'</span></a></li>';
					else 
						echo '</a></li>';
				} ?>
			</ul>
		</div>
		<div class="col-xs-12 mobile-nopadding">
			<div class="banner"><?php $img_src = wp_get_attachment_image_src(get_single_taxonomy_meta('cat-description', $term->term_id, 'cat-banner-img'), 'full'); ?>
				<img style="" nopin="nopin" src="<?php echo $img_src[0]; ?>" />
				
			</div>
				
			
			<!--<div class="item-lists-holder" style="padding-bottom: 0;">
				<div class="top-block clearfix" style="margin: 0">
					<h3><?php echo $post->post_title; ?></h3>
				</div>
			</div>
					
			<div id="content-img-fancy" class="post_content clearfix">
				<?php echo apply_filters('the_content', $post->post_content); ?>
			</div>-->

		<div class="col-md-8 col-xs-12 ">
			<div class="block-content">
				<div class="item-lists-holder" style="padding-bottom: 0;">
					<div class="top-block clearfix" style="margin: 0">
				<h3 style="margin-left: -15px;"><?php echo $post->post_title; ?></h3>
				</div>
				</div>
                <div class="entry-date m-b-md"><span><i class="fa fa-comment"></i>&nbsp;<?php echo get_comments_number(); ?></span> | <i class="fa fa-calendar"></i>&nbsp;<span><?php echo get_the_date(); ?></span></div>
				<div class="the-content-style-guide"><?php echo apply_filters('the_content', $post->post_content); ?></div>
			</div>
			
		</div>

		<div class="col-md-4 block-style-guide-relative">
			
			<h3 class="title title-inner condensed" style="margin-top:15px;">CÓ THỂ BẠN QUAN TÂM</h3>
			
			<ul class="clearfix"><?php $style_guide_term = get_the_terms($post,'style-guide-category');?>
					<?php 
						$args = array('posts_per_page' => 7, 'post_type' => $post->post_type,'post_not_in' => array($post->ID),'orderby' => 'date','order' => 'DESC');
						if($style_guide_term){
							$style_guide_term = array_values($style_guide_term);
							$style_guide_term = $style_guide_term[0];
							$args['style-guide-category'] = $style_guide_term->slug;
						}
						$relative_posts = new WP_Query($args);
						while($relative_posts->have_posts()): $relative_posts->the_post();?>
						<li><a href="<?php the_permalink()?>"><?php the_title() ?></a> <span class="date">(<?php echo get_the_date()?>)</span></li>
						<?php endwhile; wp_reset_postdata();?>
					</ul>
				
		</div>

		<div id="fancybox-hidden"></div>
			

			<div class="item-lists-holder">
				<div class="top-block clearfix"><?php $text_intro = '';
				if($term->slug == 'xu-huong-thoi-trang') $text_intro = 'xu hướng ';
				// elseif($term->slug == 'meo-hay-cho-phong-cach') $text_intro = '' ?>

					<!--h3>Sản phẩm phù hợp với <?php echo $text_intro . $post->post_title; ?></h3-->
                    <h3 style="width: 100%;">Sản phẩm phù hợp</h3>

					<p class="sortby-price fr">Sắp xếp theo giá <a href="?sort=inc" class="<?php echo (isset($_GET['sort']) && $_GET['sort'] == 'inc') ? 'current':''; ?> inc">TĂNG</a> | <a href="?sort=dec" class="<?php echo (isset($_GET['sort']) && $_GET['sort'] == 'dec') ? 'current':''; ?> dec">GIẢM</a></p>
				</div>
				<ul class="item-lists item-products list-style-none clearfix">
                    <?php $target_saved = get_post_meta($post->ID, 'style_guide_save', true);
				if(is_null($target_saved) || empty($target_saved)) $target_saved = array();
				$order = 'ASC';
				if(isset($_GET['sort']) && $_GET['sort'] == 'dec') $order = 'DESC';
				$args = array('posts_per_page' => 12, 'post__in' => $target_saved, 'meta_key' => 'post_price', 'orderby' => 'meta_value_num', 'order' => $order);
				if($target_saved):
					$follow_posts = new WP_Query($args);

					while($follow_posts->have_posts()): $follow_posts->the_post();
					$pid = get_the_id(); ?>
						<li class="item col-md-4 col-sm-4 col-xs-6 fl"><!-- grid4 -->
							<div class="item-outer clearfix">
								<div class="item-inner"><?php $product_colors = get_post_meta($post->ID, 'post_colors', true);
									foreach($product_colors as $c_key => $c){
										$images_ids = explode(',', $c['images_ids']);
										$images_ids = $images_ids[0];
										$images_ids = wp_get_attachment_image_src($images_ids, array(350, 1000));
										break;
									} ?>
									<a href="<?php echo get_permalink($pid); ?>"><img src="<?php echo $images_ids[0]; ?>" alt=""></a>
								</div>
								<h4><a href="<?php echo get_permalink($pid); ?>" title=""><?php the_title(); ?></a></h4>
								<p><?php echo aj_format_number(get_post_meta($pid, 'post_price', true)); ?> VNĐ</p>
							</div>
						</li><?php endwhile; wp_reset_postdata(); ?>
				<?php endif; ?>
				</ul>
			</div>

		</div>
	</div>
	
</div>

<script>
jQuery(document).ready(function($) {
	if($('#content-img-fancy').find('img').length > 0){
		var html = '';
		$('#content-img-fancy').find('img').each(function(index, el) {
			var src = $(this).attr('src');
			html += '<a rel="group" href="'+ src +'"><img src="'+ src +'" alt="" /></a>';
		});
		$('#fancybox-hidden').html(html).hide();

		$('#content-img-fancy a').click(function(event) {
			if( $(this).find('img').length > 0 )
				event.preventDefault();
		});

		$('#content-img-fancy img').click(function(event) {
			$('#fancybox-hidden a').fancybox({
				overlayShow: false
			});
			$('#fancybox-hidden a:first').click();
		});
	}
});
</script>
<script>
	jQuery(document).ready(function(){
		var wrap = jQuery(".block-style-guide .block-style-guide-relative")
		jQuery(window).on("scroll",function(e){
			if(jQuery(this).outerWidth() > 720){
				fixedScroll();
			}
			else{
				wrap.removeClass("fix-top");
				wrap.attr('style','');
			}
		
	});

	function fixedScroll(){
		wrap.removeClass("fix-top");
		wrap.attr('style','');
		var left = wrap.position().left;
		var width = wrap.outerWidth();
		var height = wrap.outerHeight();
		var scrollTop = jQuery('body').scrollTop();

		if (scrollTop > wrap.position().top) {
			var wrapHeight = jQuery('.block-outer.block-style-guide').outerHeight();
			if (scrollTop + height > wrapHeight){
				wrap.css({'position': 'fixed','top':-(scrollTop + height - wrapHeight),'left':left,'width': width});
			} else {
				wrap.css({'left':left,'width': width});
				wrap.addClass("fix-top");
			}
		} else {
			wrap.removeClass("fix-top");
		}
	}
 });
</script>
<style>.block-style-guide-relative.fix-top {top: 0;position: fixed;margin-left:50px;padding:10px;}</style>
<?php get_footer(); ?>
