<?php $metaProperty = array(
	'app_id' => '1447368075554272',
	'type' => 'article',
	'title' => get_option('blogname') .' | '. get_option('blogdescription'),
	'url' => home_url() . $_SERVER['REQUEST_URI'],
	'image' => get_template_directory_uri() . '/screenshot.png',
	'description' => get_option('blogdescription'),
);
if( is_tax() || is_category() || is_tag() ) {
	$metaProperty['title'] = single_term_title( '', false ) .' | '. get_option('blogname');
	$metaProperty['description'] = term_description() ? term_description() : $metaProperty['description'];
}
if( is_search() ) {
	$metaProperty['title'] = 'Tìm kiếm: '. get_query_var('s') .' | '. get_option('blogname');
	$metaProperty['description'] = 'Kết quả tìm kiếm "'. get_query_var('s') .'" from '. get_option('blogname');
}
if( is_author() ) {
	$authorID = get_query_var('author');
	$authorData = get_userdata( $authorID );
	$metaProperty['title'] = $authorData->display_name .' @ '. get_option('blogname');
	$metaProperty['description'] = $authorData->descriptionn ? $authorData->description : $metaProperty['description'];
}
if( is_single() || is_page() ) {
	if(get_post_type() == 'post'){
		$product_colors = get_post_meta(get_the_id(), 'post_colors', true);
		foreach($product_colors as $c_key => $c){
			$images_ids = explode(',', $c['images_ids']);
			$images_ids = $images_ids[0];
			$imageSource = wp_get_attachment_image_src($images_ids, 'large');
		}
	}
	else{
		$imageSource = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );
	}
	$postDescription = $post->post_excerpt ? $post->post_excerpt : wp_trim_words( $post->post_content );
	$metaProperty['title'] = $post->post_title;
	if( get_post_type() == 'post' )
		$metaProperty['title'] = 'Sản phẩm '. $metaProperty['title'];
	$metaProperty['description'] = strip_tags( $postDescription );
	$metaProperty['image'] = $imageSource ? $imageSource[0] : $metaProperty['image'];
}
if( is_paged() ) {
	$metaProperty['title'] .= ' | Page '. get_query_var('paged');
	$metaProperty['description'] .= ' | Page '. get_query_var('paged');
}
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
	<title><?php 
	// if(is_home()) bloginfo('name'); else wp_title();
	echo str_replace('"', '', $metaProperty['title']);
	 ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta charset="<?php bloginfo('charset'); ?>" /> 
	<meta content="text/html; charset=utf-8" http-equiv="content-type">


	<meta property="fb:app_id" content="<?php echo $metaProperty['app_id'];?>" />
	<meta property="og:type" content='<?php echo $metaProperty['type'];?>' />
	<meta property="og:title" content="<?php echo str_replace('"', '', $metaProperty['title']);?>" />
	<meta property="og:url" content="<?php echo $metaProperty['url'];?>" />
	<meta property="og:image" content="<?php echo $metaProperty['image'];?>" />
	<meta property="og:description" content="<?php echo str_replace('"', '', $metaProperty['description']);?>" />

	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,700,300,600,400|Open+Sans+Condensed:700,300&subset=latin,vietnamese,latin-ext' rel='stylesheet' type='text/css'>
	<link type="image/x-icon" rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon.png" />
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?> >
	<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=1447368075554272&version=v2.3";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	// GooglePlus
	(function() {
		var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
		po.src = 'https://apis.google.com/js/plusone.js?onload=onLoadCallback';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
	})();
	</script>
	
	<!-- Piwik -->
	<script type="text/javascript">
	var _paq = _paq || [];
	_paq.push(["setDomains", ["*.thoitrangkhatoco.vn"]]);
	_paq.push(['trackPageView']);
	_paq.push(['enableLinkTracking']);
	(function() {
		var u="//khatocofashion.org/piwik/";
		_paq.push(['setTrackerUrl', u+'piwik.php']);
		_paq.push(['setSiteId', '1']);
		var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
		g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
	})();
	</script>
	<noscript><p><img src="//khatocofashion.org/piwik/piwik.php?idsite=1" style="border:0;" alt="" /></p></noscript>
	<!-- End Piwik Code -->

	<!-- Google Tag Manager -->
	<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-5CZCRW"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-5CZCRW');</script>
	<!-- End Google Tag Manager -->

	<div class="page-tools">
		<a id="order-check-cta-anchor" href="<?php echo get_permalink(167); ?>" title="">
			<span class="icon"><i class="fa fa-shopping-cart"></i></span>
			<span class="cart-num" id="load_cart_num_anchor"></span>
			<span class="note">Giỏ hàng</span>
		</a>
	</div>

	<div class="popup close-wrap hidden" id="order-check">
		<div class="close-bg"></div>
		<div class="popup-inner">
			<h2>Tra cứu đơn hàng</h2>			
			<form action="<?php echo get_page_link( 2505 ); ?>" method="get" accept-charset="utf-8">
				<p class="popup-hint">Nhập mã đơn hàng (hệ thống đã gửi qua email cho bạn)</p>
				<input type="text" name="order_code" placeholder="Mã đơn hàng *">
				<!-- <input type="text" name="order_phone" placeholder="Nhập số điện thoại"> -->
				<input type="submit" value="Kiểm tra">
			</form>
			<a href="#close" class="close">x</a>
		</div>
	</div>

	<div class="popup close-wrap hidden" id="login-modal">
		<div class="close-bg"></div>
		<div class="popup-inner">
			<h2>ĐĂNG NHẬP</h2>
			<div class="mid clearfix">
				<form id="" class="account-form" action="" method="POST" autocompleted="off">
					<p class="clearfix">						
					</p>
					<p class="clearfix">
						<input class="field" type="text" name="email" id="lg_email" placeholder="Số điện thoại" autocompleted="off" required />
					</p>
					<p class="clearfix">
						<input class="field" name="password" id="lg_password" placeholder="Mật khẩu" autocompleted="off" type="password" required />
					</p>	
					<p class="clearfix"></p>
					<p class="clearfix">
						<input type="checkbox" class="checkbox" name="lg_remember" id="lg_remember">
						<label for="lg_remember"style="padding-left:30px">Ghi nhớ đăng nhập</label>
						<a href="<?php echo get_page_link(10954); ?>" class="fr">Quên mật khẩu?</a>
					</p>
					<p class="clearfix"></p>
					<p class="clearfix">
						<button type="submit">Đăng nhập</button>
						<a href="#login-facebook" id="facebook-login"><i class="fa fa-facebook"></i></a>
						<input type="hidden" name="redirect" value="<?php echo get_current_url(); ?>" /> 
						<input type="hidden" name="action" value="form_login_ajax" /> 
						<input type="hidden" id="login_nonce_field" name="login_nonce_field" value="<?php echo wp_create_nonce('login_nonce_action'); ?>" />
						<input type="hidden" name="_wp_http_referer" value="/" />
					</p>
					<p></p>
					<p id="login_status" class="form-status"></p>
				</form>
			</div>
			<a href="#close" class="close">x</a>
		</div>
	</div>

	<div class="side-menu-mobile hidden">
		<ul class="list-style-none clearfix">
			<h3><?php if(is_user_logged_in()){
				$user_info = wp_get_current_user();
				echo $user_info->data->display_name ? $user_info->data->display_name : $user_info->data->user_email;
			}
			else echo 'Menu'; ?></h3><?php if(is_user_logged_in()){ ?>
			<li class="personal personal-mobile"><a href="#collapse-menu" data-toggle="collapse" aria-expanded="false" aria-controls="collapse-menu" style="border-bottom: 1px solid #2d2b2b;">Hồ sơ cá nhân&nbsp;&nbsp; <i class="fa fa-angle-right pull-right angle-rotate-menu" aria-hidden="true"></i></a>	
				<ul id="collapse-menu" style="padding-left: 15px;height:auto !important;">
					<li class="personal"><a href="<?php echo get_page_link(2503); ?>"><i class="fa fa-user" aria-hidden="true"></i>&nbsp;&nbsp; Thông tin cá nhân</a></li>
				    <li class="personal"><a href="<?php echo get_page_link(2505); ?>"><i class="fa fa-list-alt" aria-hidden="true"></i>&nbsp;&nbsp; Đơn hàng</a></li>
					<li class="personal"><a href="<?php echo get_page_link(2507); ?>"><i class="fa fa-heart" aria-hidden="true"></i>&nbsp;&nbsp; Sản phẩm yêu thích</a></li>
					<li class="personal-last"><a href="<?php echo wp_logout_url(get_current_url()); ?>"><i class="fa fa-sign-out" aria-hidden="true"></i>&nbsp;&nbsp; Đăng xuất</a></li>
				</ul>
			</li>
			<?php }
			else{ ?>
			<li class="personal"><a href="<?php echo get_page_link(171); ?>">Đăng ký</a></li>
			<li class="personal last"><a href="<?php echo get_page_link(169); ?>">Đăng nhập</a></li>
			<?php } ?>
			<?php wp_nav_menu(array('theme_location' => 'main-menu-mobile', 'container' => false, 'items_wrap' => '%3$s', 'walker' => new Mobile_Nav_Menu())); ?>
		</ul>
		<div id="mobile_menu_close"></div>
	</div>
	<header id="header" class="header wow fadeIn" data-wow-duration="1s">
	<div class="top-header">
		<div class="container">
			<div id="hambuger-top-menu" class="clearfix"></div>
			<div id="search-hide-mobile" style="display: none;">
				<a class="icon-search" style="color: #fff"><i class="fa fa-search"></i></a>
			</div>
			<!-- <div class="col-sm-4">
				<div class="left-logo-mobie" style="display: none">
					<form action="<?php// bloginfo('siteurl'); ?>" method="get" class="top-search-form">
						<i class="fa fa-search"></i>
						<input onKeyPress="return submitenter(this,event)" type="text" name="s" id="header-top-search-text" value="<?php// if(isset($_GET['s'])) echo $_GET['s']; ?>" placeholder="Tìm kiếm" required>
						<button id="header-top-search-cancel"><i class="fa fa-times-circle"></i></button>
						<input type="submit" style="display: none;" />
					</form>
					<div id="header-top-search-results" class="search-results posa">
						<ul class="list-style-none clearfix"></ul>
					</div>
				</div>
			</div> -->
			<a href="<?php echo get_permalink(167); ?>" class="mobile">
				<i class="fa fa-shopping-cart"></i>
				<span class="cart-num" id="load_cart_num_mobile"></span>
			</a>
			<div class="left-top-header">
				<ul>
					<?php aj_nav_menu('top-left-menu'); ?>
				</ul>
			</div>
			<div class="right-top-header">
				<ul>
					<li><a href="<?php echo get_permalink( get_page_by_path( 'tra-cuu' ) ); ?>">thẻ thành viên</a></li>
					<li><a id="order-check-cta" href="javascript:;">tra cứu đơn hàng</a></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="background-opacity"></div>
	<div class="logo">
		<div class="container">
			<div class="row">
				<div class="col-xs-4 search-mobile">
					<div class="left-logo test">
						<form action="<?php echo home_url(); ?>" method="get" class="top-search-form">
							<i class="fa fa-search"></i>
							<input onKeyPress="return submitenter(this,event)" type="text" name="s" id="header-top-search-text" value="<?php if(isset($_GET['s'])) echo $_GET['s']; ?>" placeholder="Tìm kiếm" required>
							<button id="header-top-search-cancel"><i class="fa fa-times-circle"></i></button>
							<input type="submit" style="display: none;" />
							<button id="header-top-search-close" style="color:black; display: none;position: absolute;top: -70px;right: -28px;"><i class="fa fa-times-circle"></i></button>
						</form>
						<div id="header-top-search-results" class="search-results posa">
							<ul class="list-style-none clearfix"></ul>
						</div>
					</div>
				</div>
				<div class="ct-logo col-xs-4">
					<a href="<?php echo home_url('/'); ?>" title="<?php echo get_option( 'blogname' ); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" /></a>
				</div>
			<div class="right-logo col-xs-4">
			<ul>
			<?php
			if(!is_user_logged_in()){
			?>
				<li><a href="<?php echo get_page_link(171); ?>">ĐĂNG KÝ</a></li>
				<li><span> | </span></li>
				<li>
				<a id="user_login_btn" href="javascript:;">ĐĂNG NHẬP</a>				
				</li>
				<?php
				} else {
					$user_info = wp_get_current_user();
				?>
				<li>
					<a id="user-info-page" href="#"><?php $fullname = $user_info->user_login;
					$name = get_user_meta( $user_info->ID, 'name', true );
					if( $name ) $fullname = $name;
					$name = get_user_meta( $user_info->ID, 'fullname', true );
					if( $name ) $fullname = $name;
					echo $fullname; ?></a>
					<i class="fa fa-caret-down"></i>
					<div class="sub-menu user-info">
						<div class="sub-menu-wrapper col-md-12">
							<div class="login-item header-top-item clearfix">
								<div class="row clearfix"><a href="<?php echo get_page_link(2503); ?>">Thông tin cá nhân</a></div>
								<div class="row clearfix"><a href="<?php echo get_page_link(2505); ?>">Đơn hàng</a></div>
								<div class="row clearfix"><a href="<?php echo get_page_link(2507); ?>">Sản phẩm yêu thích</a></div>
								<div class="row clearfix"><a href="<?php echo wp_logout_url(get_current_url()); ?>">Đăng xuất</a></div>	
							</div>
						</div>
					</div>
				</li>
				<?php
				}
				?>								
				<li><span> | </span></li>				
				<li>
				<a href="javascript:;" data-redirect="<?php echo get_permalink(167); ?>">
					<i class="fa fa-shopping-cart"></i>
					<span class="cart-num" id="load_cart_num"></span>
				</a>				
				<div class="sub-menu cart" id="cart_link">
				<div class="sub-menu-wrapper col-md-12">
					<div class="cart-item header-top-item clearfix">
						<div class="mid clearfix">
							<div id="load_top_cart"></div>
							<div class="row submit-holder clearfix">
								<!-- <a class="condensed fl" href="">Liên hệ cửa hàng</a> -->
								<div class="fr">
									<a href="<?php echo get_permalink(167); ?>">Xem đơn hàng<i class="fa fa-long-arrow-right"></i></a>									
								</div>
							</div>
						</div>
					</div>
				</div>
				</div>
				</li>	
			</ul>
	</div>
	</div>
	</div>
	</div>
	</div>
	
	<div class="my-menu container">
			
		<div class="menu-top">
			<button class="btn-menu"><i class="fa fa-bars"></i></button>
			<ul class="main-menu">
				<?php wp_nav_menu(array('theme_location' => 'left-main-menu', 'container' => false, 'items_wrap' => '%3$s', 'walker' => new Main_Nav_Menu())); ?>
				<?php wp_nav_menu(array('theme_location' => 'right-main-menu', 'container' => false, 'items_wrap' => '%3$s', 'walker' => new Main_Nav_Menu())); ?>
			</ul>
		</div>
	</div>

	<!-- <div class="title-top">
		<div class="top-tip-slide container">
			<div class="item">
				<span>Giao Hàng Miễn Phí Toàn Quốc</span>
			</div>
			<div class="item">
				<span>KHATOCO - Tự Hào Thương Hiệu Quốc Gia</span>
			</div>
			<div class="item">
				<span>Giao Hàng Miễn Phí Toàn Quốc</span>
			</div>
		</div>
	</div> -->

	<div id="top-slide">
		<div class="top-tip-slide">
			<?php
				for ($i=1; $i < 6; $i++) { 
					$intro_text = get_option('intro-text'.$i);
					if($intro_text != ''){
						$bg_color = (get_option('intro-bg-color'.$i) != '') ? '#'.get_option('intro-bg-color'.$i) : '#b32017';
						$text_color = (get_option('intro-text-color'.$i) != '') ? '#'.get_option('intro-text-color'.$i) : '#fff';
						$intro_link = get_option('intro-link'.$i);
						?>
						<div class="item">
							<div style="background:<?php echo $bg_color; ?>;" class="title-top">
								<a href="<?php echo $intro_link; ?>"><span style="color:<?php echo $text_color; ?>"><?php echo $intro_text; ?></span></a>
							</div>
						</div>
						<?php
					}
				}
			?>
		</div>
	</div>


</header>
