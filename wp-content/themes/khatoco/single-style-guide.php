<?php get_header();
global $post;
$tax = 'style-guide-category';
$term = get_the_terms($post, $tax);
$term = array_values($term);
$term = $term[0];
$terms = get_terms( $tax, array( 'hide_empty' => 0 ) );
$post_id = $post->ID;
 ?>

<?php get_header();
$post = getSingleStyleGuide();
$permalink = get_permalink($post['id']); ?>
<section id="style-guide-detail">
    <div class="container">
        <?php
        $link = get_permalink(get_page_by_title('Style Guide'));
        _get_template_part('partials/breadcrumb-render', null, ['data'=>[
            'category'=>['name'=>'Gu Đàn Ông','link'=>$link],
            'subCategory'=>['name'=>$post[0]['category_name'],'link'=>'#'],
            'currentPage'=>['name'=>$post[0]['title'],'link'=>$permalink]
        ]]);
        ?>
        <div class="row">
            <div class="col-xs-12 col-lg-8">
                <h1 class="masthead"><?php echo $post[0]['title'] ?></h1>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="content">
                            <?php echo $post[0]['content'] ?>
                        </div>
                        <div class="break-line"></div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-lg-4">

                <?php
                $relatedNews = get_related_style_guide($post[0]['slug']);
                _get_template_part('partials/related-post', null, ['post' => $relatedNews]);
                ?>
            </div>
        </div>
    </div>
</section>
<section id="break-line"></section>
<section id="related-product">
    <?php $saleProducts = aj_get_home_posts(0, 8, 'hot');
    ?>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1 class="masthead">Sản Phẩm Liên Quan</h1>
            </div>
        </div>
        <div class="row">
            <?php foreach ($saleProducts->posts as $item) {
                ?>
                <div class="col-xs-4 col-md-3 pb-4 ">
                    <div class="wrapping-tag">
                        <?php
                        $is_sale = get_post_meta($item->ID,'sale_off');
                        if($is_sale[0] > 0){
                            ?>
                            <div class="tag-sale">SALE</div>
                        <?php } ?>
                        <div class="product-card">
                            <a class="redirect-link" href="<?php echo get_permalink($item->ID) ?>"></a>

                            <div class="image-wrapper">
                                <?php
                                $product_colors = get_post_meta($item->ID, 'post_colors', true);
                                $images_ids     = explode(',', $product_colors[0]['images_ids']);
                                $images_ids     = $images_ids[0];
                                $images_ids     = wp_get_attachment_image_src($images_ids, [350, 1000]);
                                ?>
                                <!--                            <img class="img-fluid" src="--><?php //echo $images_ids[0] ?><!-- ">-->
                                <img class="img-fluid" src="http://khatoco.local/wp-content/uploads/2019/02/A2MN438R2-VNMA031-2009-N-1-768x960.jpg">
                            </div>
                            <div class="product-info">

                                <div class="price-group text-left">
                            <span class="item-price condensed">
    <?php
    /*$saleoff_info = get_saleoff_info($post->ID);*/
    $price        = (int) get_post_meta($item->ID, 'post_price', true);
    $saleoff_info = ProductUtils::getProductSaleOffPrice($item->ID);
    if ($saleoff_info['percent'] > 0) {
        echo '<b>' . number_format($saleoff_info['value'], 0, ',', '.') . ' Đ </b> &nbsp;';
        echo '<span class="blink-text">' . aj_format_number($price) . ' Đ</span>';
    } else {
        echo '<b>' . aj_format_number($price) . ' Đ </b>';
    }
    ?>

                </span>
                                </div>
                                <p class="name text-left"><?php echo $item->post_title ?></p>
                            </div>
                            <div class="add-card">
                                <a class="cta" href="<?php echo get_permalink($item->ID) ?>">
                                    <i class="fa fa-shopping-cart" aria-hidden="true"></i> Thêm vào giỏ hàng
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</section>

<?php echo get_template_part('partials/event-banner'); ?>
<?php get_footer(); ?>


<?php get_footer(); ?>
