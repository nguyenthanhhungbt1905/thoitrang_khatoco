<?php
/*
Template Name: Page Tra Cứu
*/
get_header(); ?>

<div data-ng-app="khatocoUserProfile" class="block-outer block-account">
	<div class="block-inner">
    <div data-ng-controller="UserController" class="container fluid clearfix">
        <div class="block-title-outer"><h2 class="block-title block-title-inner condensed">Tra cứu thẻ thành viên</h2></div>
        <div class="block-inner clearfix">
            <form class="grid6 clearfix" name="registerForm" method="POST" data-ng-submit="formSubmit()">
                <h2>Thông tin tra cứu</h2>
                <p>Bạn nhập thông tin thẻ tại đây.</p>
                <p class="clearfix">
                    <label for="cardcode">Mã Khách Hàng<span class="required">*</span></label>
                    <input class="field" data-ng-model="datapost.cardcode" value="" type="text" required/>
                </p>
                <p class="clearfix">
                    <label for="cellphone">Số Điện Thoại <span class="required">*</span></label>
                    <input class="field" data-ng-model="datapost.cellphone" value="" required/>
                </p>
                <p class="clearfix">
					<span style="width: 30%; float: left;">
						<img id="captcha"
                             src="<?php echo esc_url(home_url('/wp-includes/captcha/')); ?>securimage_show.php"
                             alt="CAPTCHA Image"/> <br/>
						<a style="margin-left: 18px;" href="javascript:;" id="click-captcha"
                           onclick="document.getElementById('captcha').src = '<?php echo esc_url(home_url('/wp-includes/captcha/')); ?>securimage_show.php?' + Math.random(); return false">[ Tải Mã Khác ]</a>
					</span>
                    <label style="width: 70%; float: left;">
                        <input class="field" data-ng-model="datapost.captcha" placeholder="Nhập mã kiểm tra hình bên"
                               type="text" name="captcha_code" size="10" maxlength="4"/>
                    </label>
                </p>

                <p class="clearfix">
                    <label for="">&nbsp;</label>
                    <button class="button" type="submit">
                        <span data-ng-hide="loading">Tra Cứu</span>
                        <span data-ng-show="loading">Tra Cứu <i class="fa fa-refresh fa-spin"></i></span>
                    </button>
                </p>
                <div class="clearfix" data-ng-hide="return.code != 1">
                    <p for="">Mã khách hàng: <span>{{return.cusID}}</span></p>
                    <p for="">Tên khách hàng: <span>{{return.name}}</span></p>
                    <p for="">Hạng Thẻ: <span>{{return.cType}}</span></p>
                    <p for="">Điểm Tích Lũy: <span>{{return.cardbonnuspoint}}</span></p>
                </div>

            </form>

            <div class="grid6">
                <a href="<?php echo get_page_link(1057); ?>"><img nopin="nopin"
                                                                  src="<?php echo get_template_directory_uri(); ?>/images/account.png"
                                                                  alt=""></a>
                <div class="content">
                    <p><strong><a href="<?php echo get_page_link(1057); ?>">Giới thiệu về chương trình Thẻ thành viên
                                Khatoco</a></strong></p>
                    <p>- Lợi ích khi trở thành thành viên chương trình Thẻ thành viên Khatoco.</p>
                    <p>- Thể lệ chương trình Thẻ thành viên Khatoco.</p>
                    <p>- Các câu hỏi thắc mắc về chương trình Thẻ thành viên Khatoco.</p>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<div id="popup" class="popup close-wrap hidden">
	<div class="close-bg"></div>
    <div class="popup-inner">
        <div id="popup-message" class="message-wrap row clearfix">
        </div>
        <a href="#close" class="close">x</a>
        <div class="button-close row clearfix">
            <p><a href="javascript:;" id="link" class="button">Đóng lại</a></p>
        </div>
    </div>
</div>

<script type="text/javascript">
    var apiUrl = "<?php echo admin_url('admin-ajax.php'); ?>";
    var app = angular.module("khatocoUserProfile", []);
    app.service('userService', ['$http', '$q', '$timeout', function ($http, $q, $timeout) {

        var getCardUserInfo = function (data) {
            var d = $q.defer();
            $timeout(function () {
                $http({
                    method: 'POST',
                    url: apiUrl,
                    params: {action: 'user_service_ajax', do_ajax: 'get_user_member_card'},
                    data: data
                }).success(function (data, status, headers) {
                    d.resolve(data);
                }).error(function (data, status, headers) {
                    d.reject(data);
                });
            }, 1000);

            return d.promise;
        };

        return {
            getCardUserInfo: getCardUserInfo
        };
    }]);

    app.controller('UserController', ['$scope', '$sce', 'userService', function ($scope, $sce, userService) {
        $scope.submitMessage = '';
        $scope.loading = false;
        $scope.datapost = {};
        $scope.return = {};
        $scope.datapost.cellphone = '';
        $scope.datapost.nonce = "<?php echo wp_create_nonce('register_nonce_action'); ?>";


        $scope.datapost.captcha = '';

        $scope.error = false;

        if ($scope.error == true) {
            alert('Không thể kết nối đến server, vui lòng F5 và thử lại!');
            return;
        }

        $scope.formSubmit = function () {

            $scope.loading = true;
            $scope.submitMessage = '';

            /*console.log( $scope.datapost );
             return false;*/

            userService.getCardUserInfo($scope.datapost)
                .then(function (data) {
                    if (data.success) {
                        //chua xac dinh cac bien
                        var item = data.item;
                        $scope.return.name= item.CustName;
                        $scope.return.cusID= item.CustID;
                        $scope.return.code= 1;
                       
                        switch(item.TypeCardID)
                        {
                            case 'DIAMONDNB':
                                $scope.return.cType= 'Thẻ kim cương';
                                break;
                            case 'DIAMOND':
                                $scope.return.cType= 'Thẻ kim cương';
                                break;
                            case 'GOLD':
                                $scope.return.cType= 'Thẻ vàng' ;
                                break;
                            case 'GOLDNB':
                                $scope.return.cType= 'Thẻ vàng' ;
                                break;
                            case 'SILVER':
                                $scope.return.cType= 'Thẻ bạc';
                                break;
                            case 'SILVERNB':
                                $scope.return.cType= 'Thẻ bạc';
                                break;
                            case 'NOIBO':
                                $scope.return.cType= 'Thẻ nội bộ' ;
                                break;
                        }
                        var nf = Intl.NumberFormat();
                        $scope.return.cardbonnuspoint = nf.format(item.OrderRevenue);
                        
                        
                    }
                    else {
                        if (data.message) {
                            // $scope.submitMessage = data.message;
                            document.getElementById('popup-message').innerHTML = '<p>' + data.message + '</p>';
                            document.getElementById('popup').className = document.getElementById('popup').className.replace(/hidden/i, '');
                            document.getElementById('captcha').src = '<?php echo esc_url(home_url('/wp-includes/captcha/'));?>securimage_show.php?' + Math.random();
                        }
                        $scope.return.code= -1;
                    }
                    $scope.loading = false;
                    document.getElementById('captcha').src = '<?php echo esc_url(home_url('/wp-includes/captcha/')); ?>securimage_show.php?' + Math.random()
                }, function (err) {
                    $scope.return.code= -1;
                    $scope.submitMessage = 'Không thể kết nối đến máy chủ, F5 để thử lại.';
                    $scope.loading = false;
                });
        };

        $scope.objectToArray = function (object) {
            var result = [];
            for (ob in object) {
                result.push(object[ob]);
            }
            return result;
        };
    }]);
</script>

<?php get_footer(); ?>
