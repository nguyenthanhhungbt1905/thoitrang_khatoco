<?php 
/*
Template Name: Giới thiệu
*/

global $post;
get_header(); ?>
 
<?php /* ?> <div class="banner">
	<img nopin="nopin" src="<?php echo get_template_directory_uri(); ?>/images/gioithieu_01.png" alt="">
	<?php echo get_the_post_thumbnail( $post->ID, 'full', array( 'nopin' => 'nopin' ) ); ?>
</div>
 */ ?>
<div class="block-outer block-introduce">
	<div class="container fluid clearfix">
		<div class="block-inner clearfix">
			<div class="block-content container clearfix fluid">
				<div class="row-fluid">
					<div class="col-xs-12 col-md-6">
						<div class="item-outer">
							<div class="item-inner">
								<div class="item-thumb">
									<a href="<?php 
												$permalink = get_page_link(618);
												echo $permalink?>">
										<?php echo get_the_post_thumbnail( 618, 'full', array( 'nopin' => 'nopin' ) ); ?>
									</a>
								</div>
								<div class="item-info">
									<h2 class="item-title condensed"><a href="<?php echo $permalink?>">Về Chúng Tôi</a>
									</h2>
									<p class="item-excerpt">Khatoco - thương hiệu thời trang nam phong cách, nam tính và hiện đại</p>
								</div>			
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-md-6">
						<div class="item-outer">
							<div class="item-inner">
								<div class="item-thumb">
									<a href="<?php 
												$permalink = get_page_link(622);
												echo $permalink?>">
										<?php echo get_the_post_thumbnail(622, 'full', array( 'nopin' => 'nopin' )); ?>
									</a>
								</div>
									<div class="item-info">
										<h2 class="item-title condensed"><a href="<?php echo $permalink?>">Lịch sử hình thành</a>
										</h2>
										<p class="item-excerpt">Ra đời năm 2002 cùng với sự ra đời của tổng công ty Khánh Việt</p>
									</div>			
							</div>
						</div>
					</div>
				</div>
				<div class="row-fluid">
					<div class="col-xs-12 col-md-6">
						<div class="item-outer">
							<div class="item-inner">
								<div class="item-thumb">
									<a href="<?php 
												$permalink = get_page_link(620);
												echo $permalink?>">
										<?php echo get_the_post_thumbnail( 620, 'full', array( 'nopin' => 'nopin' ) ); ?>
									</a>
								</div>
									<div class="item-info">
										<h2 class="item-title condensed"><a href="<?php echo $permalink?>">Tầm Nhìn - Sứ Mệnh</a>
										</h2>
										<p class="item-excerpt">Mang đến chuỗi giá trị cung ứng sản phẩm, dịch vụ khác biệt, thân thiện với môi trường</p>
									</div>			
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-md-6">
						<div class="item-outer">
							<div class="item-inner">
								<div class="item-thumb">
									<a href="http://thuongmai.khatoco.com/tabid/1788/Career.aspx">
										<img nopin="nopin" src="<?php echo home_url( '/wp-content/uploads/2014/09/gioithieu_6.jpg' ); ?>" alt="">
									</a>
								</div>
								<div class="item-info">
									<h2 class="item-title condensed"><a href="<?php echo $permalink?>">Tuyển dụng</a>
									</h2>
									<p class="item-excerpt">Nồng nhiệt chào đón các ứng viên tài năng, trí tuệ, có phẩm chất đạo đức và say mê công việc</p>
								</div>			
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>