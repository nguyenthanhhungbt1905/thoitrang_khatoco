<?php 
/* 
Template Name: Giới thiệu - Subpage
*/
get_header();
global $post;
$page_id = $post->ID; ?>
<div class="banner">
	<img nopin="nopin" src="<?php echo get_template_directory_uri(); ?>/images/vechungtoi.png" alt="">
</div>
<div class="block-outer block-introduce-subpage">
	<div class="container fluid clearfix">
		<div class="block-inner">
			<div class="block-content">
				<div class="col-md-3 col-xs-12">
					<div class="menu-subpage-outer">
					<ul class="menu-subpage"><?php $my_wp_query = new WP_Query(array('post_type' => 'page', 'post_parent' => 341, 'orderby' => 'menu_order', 'order' => 'ASC'));
					if($my_wp_query->have_posts()){
						while ($my_wp_query->have_posts()) {
							$my_wp_query->the_post();
							if($post->ID == $page_id) $class = 'class="current"';
							else $class = '';
							echo '<li '. $class .'><a href="'. get_page_link($post->ID) .'">'. $post->post_title .'</a></li>';
						}
					}
					/*$all_wp_pages = $my_wp_query->query(array('post_type' => 'page'));
					$subpages = get_page_children(341, $all_wp_pages);
					$subpages = array_reverse($subpages);*/
					/*foreach ($subpages as $s_key => $sub) {
						$class = '';
						if($sub->ID == $page_id) $class = 'class="current"';
						echo '<li '. $class .'><a href="'. get_page_link($sub->ID) .'">'. $sub->post_title .'</a></li>';
					}*/
					wp_reset_postdata(); ?>
					</ul>
					</div>
				</div>
				<div class="col-md-9 col-xs-12">
					<div class="top-block clearfix">
						<div class="intro-header-section">
							<div class="intro-header-title"><?php echo get_the_title($page_id); ?></div>
						</div>
					</div>
					<div class="main-page">
						<div class="the-content clearfix">
							<?php echo apply_filters('the_content', get_the_content($page_id)); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>