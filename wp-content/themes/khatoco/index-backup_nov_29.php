<?php get_header(); ?>
<div class="home-slide-holder clearfix">
	<div class="home-slide clearfix">
		<?php get_banner_at('trang-chu'); ?>
	</div>
</div>

<div class="home-intro">
<!--
	<div class="container">
		<div class="background-intro">
			<div class="left-intro">
				<a href="<?php echo get_option('banner-link1'); ?>" class="thumb-intro-stand">
				    <img src="<?php echo home_url() . '/wp-content/uploads/' . get_option('banner1'); ?>" alt="">
			    </a>
			</div>
			<div class="right-intro">
				<?php for ($i=2; $i < 6; $i++) { 
					$banner = get_option('banner'.$i);
					$cl = ($i==3 || $i==4) ? 'thumb-intro-small' : 'thumb-intro-big';
				?>
				<a href="<?php echo get_option('banner-link'.$i); ?>" class="<?php echo $cl; ?>">
				    <img src="<?php echo home_url() . '/wp-content/uploads/' . $banner; ?>" alt="">
			    </a>			     
				<!-- <a href="/dang-ky/" class="thumb-intro-small">
	         		<img src="<?php echo get_template_directory_uri(); ?>/images/intro3.jpg" alt="">
	        	</a>

	        	<a href="/tin-tuc/thoi-trang-nam-khatoco-nhan-danh-hieu-thuong-hieu-quoc-gia-nam-2016/" class="thumb-intro-small">
	         		<img src="<?php echo get_template_directory_uri(); ?>/images/intro4.jpg" alt="">
	        	</a>
				<a href="/chuong-trinh-thanh-vien-khatoco/" class="thumb-intro-big">
				    <img src="<?php echo get_template_directory_uri(); ?>/images/intro5.jpg" alt="">
			    </a>
			    <?php } ?>
			</div><div class="clear-fix"></div>
		</div>
	</div>
	-->
</div>
<div class="block-outer block-home-cat block-home-cat-custom">
	<div class="block-inner">
		<!-- <div class="containerpd clearfix">
			<div class="block-title-outer"><h2 class="block-title block-title-inner condensed">Mua sắm theo danh mục sản phẩm</h2></div>
		</div> -->
		<div class="block-content container clearfix fluid home-cat-slide"><?php $args = array('number' => 6, 'parent' => 0, 'hide_empty' => 0, 'orderby' => 'id');
		$terms = get_categories($args);
		$i = 0;
		foreach ($terms as $key => $term):
			if($i < 5) {?>
			<div class="item col-md-12 col-xs-6 nopadding">
				<div class="item-outer">
					<div class="item-inner">
						<a href="<?php echo get_category_link($term); ?>" id="category_<?php echo $term->term_id; ?>"><?php $img_src = wp_get_attachment_image_src(get_single_taxonomy_meta('product-cat-description', $term->term_id, 'product-cat-img'), 'full'); ?>
							<span class="item-thumb">
								<!-- <img nopin="nopin" src="<?php echo $img_src[0]; ?>" alt=""> -->
								<img nopin="nopin"src="<?php echo $img_src[0]; ?>" alt="">
							</span>
						</a>
					</div>
					<!-- <span class="item-title condensed"><?php echo $term->name; ?></span> -->
				</div>
			</div>
			<?php 
			} 
			$i++;
		endforeach; ?>
		</div>
	</div>
</div>

<div class="block-outer block-home-product">
	<div class="block-inner">
		<div class="containerpd clearfix">
			<div class="block-title-outer"><h2 class="block-title block-title-inner condensed">Sản phẩm nổi bật</h2></div>
			<div class="block-description-outer clearfix">Những sản phẩm đang được ưa chuộng tại Khatoco</div>
		</div>
		<div class="block-content container clearfix fluid product-home-slide">
			<?php
                $homeHotProducts = aj_get_home_posts(0, 16, 'hot');
                $index = 0;
                while ($homeHotProducts->have_posts()):
                	$index ++;
                	$homeHotProducts->the_post();
                    aj_display_each_post_home($index);
                endwhile;
                wp_reset_postdata(); 
                if ($index & 1){
                	echo '</div>';
                }
                ?>
                
		</div>
	</div>
</div>


<?php get_footer(); ?>
<style type="text/css">
	.block-inner .item-inner .item-thumb{
		max-height: unset;
	}
	.block-inner .item-inner .item-thumb img{
		min-height: unset !important;
	}
	@media (width: 414px){
		.item{
			margin-bottom: 25px !important;
		}
	}
	@media (max-width: 414px){
		.item{
			height: 217px;
		}
	}
</style>
<script type="text/javascript">
	jQuery(document).ready(function() {
		if (window.innerWidth <= 414){
			// console.log();
			var itemLength = jQuery('.block-home-cat-custom .block-content .item').length;
			console.log(itemLength % 2);
			if(itemLength % 2 != 0 ){
				jQuery('.block-home-cat-custom .block-content .item')[itemLength - 1].classList.remove('col-xs-6')
				jQuery('.block-home-cat-custom .block-content .item')[itemLength - 1].classList.add('col-xs-12')
			}
			
		}
	});
</script>