<?php get_header();
$term_slug = get_query_var('term');
$tax = get_query_var('taxonomy');
$term = get_term_by('slug', $term_slug, $tax); ?>
<div class="block-outer block-collection">
	<div class="container fluid clearfix">
		<div class="block-inner">
			<div class="block-title-outer"><h2 class="block-title block-title-inner condensed"><?php echo $term->name; ?></h2></div>
			<div id="collection_masonry"><?php $args = array('posts_per_page' => 4, 'post_type' => 'bo-anh', $tax => $term_slug);
			$clt = new WP_Query($args);
			$cur_p;
			if($clt->have_posts()):
				$i = 1; ?>
				<p class="tab-title"><?php while($clt->have_posts()) {
					$clt->the_post(); 
					if($i++ > 1) echo '<span class="header-top-separator"> | </span>';
					else $cur_p = $post;
					echo '<a '. (($i == 1) ? 'class="active"' : '') .' href="'. get_permalink( $post->ID ) .'">'. get_the_title() .'</a>';
					$i++;
				}
				endif; wp_reset_postdata(); ?></p><?php if(isset($cur_p->ID)):
				$post_id = $cur_p->ID;
				$galery = get_post_meta($post_id, 'collection_galery', true); ?>
				<div class="tab-content">
					<div class="collection-desc clearfix the-content">
						<?php echo apply_filters('the_content', $cur_p->post_content); ?>
					</div>
					<div class="row download-collection clearfix">
						<p><a href="<?php echo home_url('/') .'download.php?collection='. $cur_p->ID; ?>">Tải xuống bộ ảnh</a></p>
					</div>
					<ul class="masonry-holder list-style-none clearfix"><?php if(sizeof($galery) > 0 && is_array($galery)):
						foreach ($galery as $key => $p):
							foreach ($p as $img => $pid):
								$img_src = wp_get_attachment_image_src($img, 'large');
								$img_src = $img_src[0];
								$full_img_src = wp_get_attachment_image_src($img, 'full');
								$full_img_src = $full_img_src[0]; ?>
								<li class="grid6 mansonry-item collection-item clearfix"><a href="#" data-post-id="<?php echo $pid; ?>" data-link="<?php echo get_permalink($pid); ?>">
									<img src="<?php echo $img_src; ?>" alt="">
									<span class="condensed">Xem ngay</span>
								</a></li><?php
							endforeach; 
						endforeach;
					endif; ?>
					</ul>
					<div class="row download-collection clearfix">
						<p><a href="<?php echo home_url('/') .'download.php?collection='. $cur_p->ID; ?>">Tải xuống bộ ảnh</a></p>
					</div>
				<?php else: ?>
				<p class="tac">Chưa có bộ sưu tập nào ở đây.</p>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<div id="popup" class="popup-outer hidden">
	</div>
</div>
<?php get_footer(); ?>