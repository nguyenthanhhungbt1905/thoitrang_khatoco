<?php 
/*
Template Name: Hệ thống phân phối
*/
get_header(); ?>
<script type="text/javascript" src="//maps.googleapis.com/maps/api/js?v=3&amp;callback=googleLoadMapCallback&amp;libraries=visualization,drawing,geometry,places&amp;key=AIzaSyAVh6TYSf6kJ5s7EKXTL6ecbZmctgzDeFU&amp;language=vn"></script>

<!-- <script type="text/javascript" src="//maps.googleapis.com/maps/api/js?v=3&amp;callback=googleLoadMapCallback&amp;libraries=visualization,drawing,geometry,places&amp;key=AIzaSyD5zkYneNP5vHND2L8VGqLZhKEMoqtSUV8&amp;language=vn"></script> -->
<!-- <script async defer src="//maps.googleapis.com/maps/api/js?key=AIzaSyD5zkYneNP5vHND2L8VGqLZhKEMoqtSUV8&callback=googleLoadMapCallback;libraries=visualization,drawing,geometry,places" type="text/javascript"></script> -->
<div class="block-outer">
	<div class="container fluid clearfix">
		<div class="block-inner">
			<div class="block-title-outer"><h2 class="block-title block-title-inner condensed">Hệ thống phân phối</h2></div>
			<div class="block-description-outer col-xs-12">Thông tin về các điểm bán thời trang Khatoco trên toàn quốc giúp quý khách có thể tìm kiếm địa chỉ mua hàng gần nhất</div>
			<div class="block-content clearfix">
				<div class="block-showroom">
					<div class="showroom clearfix">
						<div class="col-md-5 col-xs-12 clearfix">
							<h2>Tìm cửa hàng</h2>
							<div class="select-holder">
								<label for="showroom-selector">Tỉnh/Thành Phố</label>
								<select id="showroom-selector">
									<option value="-1">-- Tất cả --</option><?php $terms = get_terms('city', array('hide_empty' => 0));
									foreach ($terms as $t_key => $term) {
										echo '<option value="'. $term->term_id .'">'. $term->name .'</option>';
									} ?>
								</select>
							</div>
							<div class="select-holder">
								<label for="showroom-type-selector">Đại lý/Showroom</label>
								<select id="showroom-type-selector">
									<option value="-1">-- Tất cả --</option>
									<option value="1">Showroom</option>
									<option value="2">Điểm bán siêu thị</option>
									<option value="3">Đại lý</option>
									<option value="4">Gian hàng online</option>
								</select>
							</div>
							<p class="clearfix"><button id="showroom-filter-btn">Xem</button></p>							
						</div>
						<div class="col-md-7 col-xs-12">
							<h2>Danh sách cửa hàng</h2>
							<div class="showroom-lists-outer">
								<ul class="showroom-lists list-style-none clearfix"><?php $args = array(
									'posts_per_page' => -1,
									'post_type' => 'showroom'
								);
								$posts = get_posts($args);
								foreach ($posts as $p_key => $p) {
									$pid = $p->ID;
									$p_addr = get_post_meta($pid, 'agency_address', true);
									$p_phone = get_post_meta($pid, 'agency_phone', true);
									$p_type = get_post_meta($pid, 'agency_type', true);
									$term_id = get_the_terms($pid, 'city');

									if( $term_id ){
										$term_id = $term_id[0]->term_id;
									}
									echo '<li>
										<h3><a class="showroom-item" href="javascript:void(0);" data-term-id="'. $term_id .'" data-type="'. $p_type .'" data-latlng="'. get_post_meta($pid, 'agency_latlng', true) .'" data-phone="'. $p_phone .'" data-addr="'. $p_addr .'">'. $p->post_title .'</a></h3>
										<p>'. $p_addr .'</p>
										<p>Điện thoại: '. $p_phone .'</p>
									</li>';
								} ?>
								</ul>
							</div>														
						</div>		
						<div id="map"></div>				
					</div>
					
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>