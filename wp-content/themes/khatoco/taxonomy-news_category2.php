﻿<?php get_header();
$tag_slug = get_query_var('term');
$tax = get_query_var('taxonomy');
$tag = get_term_by('slug', $tag_slug, $tax); 
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
?>
<div class="block-outer block-news">
    <div class="container clearfix fluid">
        <div class="block-title-outer"><h1 class="block-title block-title-inner condensed"><span><?php single_tag_title(); ?></span></h1></div>
        <div class="block-inner clearfix"><?php $args = array('posts_per_page' => 12, 'post_type' => 'news', $tax => $tag_slug, 'orderby' => 'date', 'order' => 'DESC', 'paged' => $paged);
        $hot_news = new WP_Query($args);
       
        if($hot_news->have_posts()):
            while($hot_news->have_posts()):
                $hot_news->the_post(); 
                display_each_news(get_the_id());
            endwhile;

            $big = 999999999;
            echo paginate_links( array(
                'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                'format' => '/page/%#%',
                'current' => max( 1, get_query_var('paged') ),
                'total' => $hot_news->max_num_pages,
                'type' => 'list',
                'prev_text' => '<i class="fa fa-angle-double-left"></i>',
                'next_text' => '<i class="fa fa-angle-double-right"></i>',
            ));

            wp_reset_postdata();
        else:
            echo '<p style="text-align:center;padding:50px 0 100px">Không tìm thấy bài đăng phù hợp</p>';
        endif; ?>
        </div>
    </div>
</div>
<?php get_footer(); ?>