
jQuery(window).load(function(){

	if(jQuery('body').find('.promotion_campaign_type').length > 0) {

		//Begin hide and show content for type campaign
		var dest = jQuery('.promotion_campaign_type:checked').val();

		jQuery('.campaigns-table').each(function(index, el) {
			if(jQuery(this).hasClass(dest))
				jQuery(this).show();
			else
				jQuery(this).hide();
		});

		if(dest == 'orders'){
			jQuery('#campaign_meta').hide();
		}
		else{
			jQuery('#campaign_meta').show();
		}
		
	}

	
	if(jQuery('body').find('.promotion_campaign_products').length > 0){
		jQuery('.promotion_campaign_products:checked').each(function(){
			var dest = jQuery(this).val();
			if(dest == '1'){
				jQuery(this).parents('p').siblings('.choice_special_products').hide();
			}
			else{
				jQuery(this).parents('p').siblings('.choice_special_products').show();
			}
		});
	}

	jQuery('.buy_money_product_option').change(function(event){

		var option = jQuery(this).val();

		if( option == 'buy_money_product_price' )
		{
			jQuery('.buy_money_product_price').css('display','block');
			jQuery('.buy_money_product_gift').css('display','none');
		}
		else{
			jQuery('.buy_money_product_price').css('display','none');
			jQuery('.buy_money_product_gift').css('display','block');
		}
	});
});

;(function($, window, undefined){
	'use strict';
	var campaignPage = (function(){
		var init = function(){
			validateForm();
			selectBox();
		},

		validateForm = function(){
			var post_type = $("input[name='post_type']").attr('value');

			if( post_type == "campaign" )
			{
				$('#campaign_meta_card').hide();

				$("input[type='submit']").click(function(){
					
					var hasError = false;

					$('.errorForm').remove();

					$('.promotion_campaign_type').each(function(){

						var attr = $(this).attr('checked');

						if( typeof attr !== typeof undefined && attr !== false )
						{
							var name = $(this).val();

							if( name == 'card' )
							{
								$('#campaign_meta_card').hide();
							}
							else{
								$('#campaign_meta_card').show();
							}

							// validate target CARD
							if( name == "card" )
							{
								$('#campaigns-table-card :input').each(function(){

									var attrCard = $(this).attr('type');
									if( attrCard == "checkbox" && typeof attrCard !== typeof undefined )
									{
										var val = $(this).closest('p').find("input[type='number']").val();

										if( val >= 100 )
										{
											$('#campaigns-table-card td').append("<p class='errorForm' style='color:red'>Phần trăm giảm giá phải nhỏ hơn.</p>");
											$(this).focus();
											hasError = true;
										}

										if( val != 0 &&  typeof attrCard === typeof undefined )
										{
											$('#campaigns-table-card td').append("<p class='errorForm' style='color:red'>Bạn chưa chọn thẻ thành viên.</p>");
											$(this).focus();
											hasError = true;
										}
									}
								});
							}


							// validate target ORDER
							if( name == "orders" )
							{
								var total = 0;
								
								$('#campaigns-table-orders :input').each(function(){

									if( $(this).attr('type') == "number" )
									{
										var valPercent = parseInt($(this).val());
										total = total + valPercent;	
									}
									
									if( $(this).attr('type') == "text" )
									{
										var valPrice = parseInt($(this).val());
										if( valPrice == 0 )
										{
											$('#campaigns-table-orders tr').last().append("<p class='errorForm' style='color:red'>Bạn chưa nhập % giảm giá.</p>");
											$(this).focus();
											hasError = true;
										}
									}
								});
								
								if( total >= 100 )
								{
									$('#campaigns-table-orders tr').last().append("<p class='errorForm' style='color:red'>Tổng % giảm giá phải nhỏ hơn 100%.</p>");
									$(this).focus();
									hasError = true;
								}
							}
							

							if( name == "multi-product" )
							{
								$('.choice_special_products').click(function(){
									console.log($(this).val());
								});
							}
						}

					});

					if( hasError )
						return false;
				});
				// end check click submit form
			}
			// end check campaign page
			
		},
		selectBox = function(){

			var listedProduct = [];

			$('.number_multi_product').focus(function(){
				
				listedProduct = [];

				// selected all item 
				$('.select_multi_product_buy').find('ul li').each(function(){
					var title = $(this).attr('title');
					if( typeof title !== typeof undefined ){
						listedProduct.push(title);
					}					
				}); 
				
				if( listedProduct != null )
				{
					// remove all item selected in multi product promotion
					$('.select_multi_product_gift option').each(function(){
						var text = $(this).text();	
						if( $.inArray(text,listedProduct) != -1){
							$(this).css('display','none');
						}
					});
				}
			});

		};
		return { init: init }
	})();

	
	$(document).ready(function(){
		campaignPage.init();
	});

}(jQuery, window));