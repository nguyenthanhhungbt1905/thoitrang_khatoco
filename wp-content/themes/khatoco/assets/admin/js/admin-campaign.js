var appCampaignAdmin = angular.module("campaignAdmin", []);
appCampaignAdmin.controller('campaignAdminController', ['$scope', 'campaignAdminService', function ($scope, campaignAdminService) {
    $scope.camp = {};
    $scope.isLoading = true;
    $scope.isOk = false;

    $scope.init = function () {
        campaignAdminService.getCampInfo(postIDCampaign).success(function (data) {
            if (angular.isUndefined(data.form))
                data.form = 'bonus';
            $scope.camp = data;
            $scope.camp.total = angular.isUndefined(data.total) ? 0 : parseInt(data.total);
            $scope.isLoading = false;
            $scope.applyCard = {
                noCard : data.applyCard.indexOf("0") >=0,
                silverCard : data.applyCard.indexOf("1")>=0,
                goldCard : data.applyCard.indexOf("2")>=0,
                diamondCard : data.applyCard.indexOf("3")>=0,
            }
            
            
            console.log($scope.camp);
            $scope.applyPayment = {
                internal : data.applyPayment.indexOf("2")>=0,
                international : data.applyPayment.indexOf("3") >=0
            }
        });
    }

    $scope.submitCampaign = function (e) {
        $scope.isOk = true;
        if (!$scope.isOk){
            var error = $scope.validateBeforeSubmit();
            if (error) {
                alert(error.error);
                e.preventDefault();
            } else {
                e.preventDefault();
                campaignAdminService.validateSubmit(jQuery('form#post').serialize()).success(function (data) {
                    if (data)
                        alert(data);
                    else{
                        $scope.isOk = true;
                        jQuery('#publish').click();
                    }
                });
            }
        }
    }

    $scope.validateBeforeSubmit = function () {

        if (
            !$scope.camp.title || !$scope.camp.name || !$scope.camp.dateAfter || !$scope.camp.dateBefore
        ) {
            return {error: 'Bạn chưa nhập đầy đủ thông tin'}
        }

        var start = new Date($scope.camp.dateAfter);
        var end = new Date($scope.camp.dateBefore);
        if (start.getTime() - end.getTime() >= 0)
            return {error: 'Ngày bắt đầu phải trước ngày kết thúc'}

        if (!jQuery.isNumeric($scope.camp.priority) && $scope.camp.multiEvent)
            return {error: 'Thứ tự ưu tiên phải lớn hơn 0'}
    }

    $scope.init();
}]);

var urlApiCampAdmin = 'admin-ajax.php';
appCampaignAdmin.service('campaignAdminService', ['$http', function ($http) {

    var getCampInfo = function (postID) {
        return $http({
            method: 'GET',
            url: urlApiCampAdmin,
            params: {action: 'camp_admin_service_ajax', do_ajax: 'get_camp_info', postID: postID}
        });
    };

    var validateSubmit = function (data) {
        return $http({
            method: 'GET',
            url: urlApiCampAdmin,
            params: {action: 'camp_admin_service_ajax', do_ajax: 'validate_camp_submit', data: data}
        });
    }

    var postCamp = function (data) {
        return $http({
            method: 'POST',
            url: 'post.php',
            data: data
        });
    }

    return {
        getCampInfo: getCampInfo,
        postCamp : postCamp,
        validateSubmit: validateSubmit
    }

}]);

appCampaignAdmin.directive('submitEvent', function () {
    return {
        restrict: 'A',
        link: function ($scope, elem) {
            elem.on('click', function (e) {
                $scope.submitCampaign(e);
            });
        }
    }
});