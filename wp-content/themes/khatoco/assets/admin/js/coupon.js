function make_coupon(){
    var text = '';
    var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var length = parseInt(document.getElementById('coupon_length').value);

    for(var i = 0; i < length; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

jQuery(window).load(function() {
    if(jQuery('#title').val() == '' && jQuery('#code').val() != ''){
        jQuery('#title').val(jQuery('#code').val()).focus();
    }

    var time_select_event = jQuery('#time_select_event option:selected').val();
    if(time_select_event){
        jQuery('.time_select_event').hide();
        jQuery('#time_select_event_'+ time_select_event).show();
    }

    jQuery('.filter-by-des').each(function() {
        var _id = 'filter_by_' + jQuery('#coupon_users_filter').val();
        if(jQuery(this).attr('id') == _id)
            jQuery(this).show();
    });
});

jQuery(document).ready(function($){
    // Filter users - lvl 1
    $('#coupon_users_filter').change(function(event) {
        var des = $(this).val();
        $('.filter-by-des').hide();
        if (des != '')
            $('#filter_by_gender').next().next().next().hide(); // Hide select user if destination isn't a people
        else
            $('#filter_by_gender').next().next().next().show();

        $('#filter_by_'+ des).show();
    });

    // Validate before update
    $('#publish').click(function(event){
        // Validate Filter coupon
        var coupon_filter_select = $('#coupon_users_filter');
        if (coupon_filter_select.val()!=''){
            var select_by = coupon_filter_select.val();
            var filter_by_val = $('#filter_by_' + select_by).val();
            if (filter_by_val ==null || filter_by_val == ''){
                event.preventDefault();
                $('#error-coupon').html('Bạn chưa chọn giá trị');
                $('#error-coupon').fadeIn();
            }
            if ( filter_by_val!=null){
                if (filter_by_val.length > 1){
                    var is_select_none = false;
                    is_select_none = select_by!='gender' ? jQuery.inArray('0', filter_by_val) : jQuery.inArray('2', filter_by_val);

                    if (is_select_none > -1){
                        event.preventDefault();
                        $('#error-coupon').html('Bạn không được chọn mục *Không áp dụng* song song với các giá trị khác');
                        $('#error-coupon').fadeIn();
                    }

                }

            }


        }
    });


    // Select 2
    $('#coupon_users_only').select2({
        placeholder: 'Chọn đối tượng cụ thể'
    });

    // Generate coupon
    $('.generate-coupon').click(function(event) {
        event.preventDefault();

        var coupon = make_coupon();
        $(this).siblings('.generate-coupon-load').show();

        $.ajax({
            url: '<?php echo admin_url("admin-ajax.php"); ?>',
            type: 'POST',
            dataType: 'json',
            data: {
                action: 'check_coupon_available',
                coupon: coupon
            }
        })
            .done(function(res) {
                if(res.result == false){
                    $('#code').val(coupon);
                    $('#title').val(coupon).focus();
                }
                else{
                    $(this).click();
                }
            });
        $(this).siblings('.generate-coupon-load').hide();
        return false;
    });

    // Kind select
    var kindSelect = function(type){
        var kindEl = $('#kind_select');

        if(type == '1'){
            $('.limit-decrease-row').fadeOut();
            kindEl.siblings('.unit').text('VND');
        }
        else{
            kindEl.siblings('.unit').text('%');
            $('.limit-decrease-row').fadeIn();
        }
    }

    $('#kind_select').change(function(event) {
        kindSelect($(this).find('option:selected').val());
    });

    kindSelect(kindInit);

    // Time select
    $('#time_select_event').change(function(event) {
        var time_select_event = jQuery('#time_select_event option:selected').val();
        if(time_select_event){
            jQuery('.time_select_event').hide();
            jQuery('#time_select_event_'+ time_select_event).show();
        }
    });

    // Datetimepicker
    $('.datetime_picker').datetimepicker();
});