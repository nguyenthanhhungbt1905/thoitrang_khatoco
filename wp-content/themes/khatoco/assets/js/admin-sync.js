'use strict';

var ajaxurl = wp_js_vars['ajaxurl'];
var sync = angular.module('khatocoSync', []);

sync.service('SyncService', ['$http', '$q', function($http, $q){
	var submitSync = function(data,postStatus){
		console.log(data);
		return $http({
			method: 'POST',
			url: ajaxurl,
			params: {action: 'sync', do_ajax: 'submit_sync', dataSync: data, postStatus:postStatus}
		});
	};

	var syncProcess = function(data, obj) {
		return $http({
			method: 'POST',
			url: ajaxurl,
			params: {action: 'sync', do_ajax: 'sync_process', object: obj},
			data: {dataSync: data},
			headers: {'Content-Type': 'application/json'}
		});
	};

	var lastSync = function() {
		return $http({
			method: 'POST',
			url: ajaxurl,
			params: {action: 'sync', do_ajax: 'save_sync_time'}
		});
	};

	var getLastSyncInfo = function() {
		return $http({
			method: 'POST',
			url: ajaxurl,
			params: {action: 'sync', do_ajax: 'last_sync_time'}
		});
	}

	var getSyncSettings = function() {
		return $http({
			method: 'POST',
			url: ajaxurl,
			params: {action: 'sync', do_ajax: 'get_sync_settings'}
		});
	}

	return {
		submitSync: submitSync,
		syncProcess: syncProcess,
		lastSync: lastSync,
		getLastSyncInfo: getLastSyncInfo,
		getSyncSettings: getSyncSettings
	}
}]);

sync.controller('SyncController', ['$scope', '$sce', 'SyncService', function($scope, $sce, SyncService){
	$scope.syncing = false;
	$scope.justSyncVar = false;

	$scope.sync_customers;
	$scope.sync_products;
	$scope.sync_other = 1;

	$scope.process = 4;
	$scope.messageSync = '';
	$scope.response = [];

	$scope.last_sync_time = '';
	$scope.last_sync_user = '';

	$scope.steps = '';
	$scope.stepsHTML = '';
	$scope.step_inline = 0;

	// Calculated process percent
	$scope.processFunc = function(p) {
		console.log(p);
		if(($scope.process + p) >= 100){
			$scope.process = 100;
		}
		else
			$scope.process += p;
	};

	// Announce sync process is being done
	$scope.isSyncing = function() {
		if($scope.syncing)
			set_in_syncing();
		else
			set_out_syncing();

		return $scope.syncing;
	};

	// Save the current time syncing
	$scope.saveSyncTime = function() {
		SyncService.lastSync()
			.success(function(data, status, headers){
				$scope.last_sync_time = data.msg;
			})
			.error(function(data, status, headers){
				console.log('Error saveSynceTime:', data);
			});
	};

	// Get the last time syncing
	$scope.getLastSyncInfo = function() {
		SyncService.getLastSyncInfo()
			.success(function(data, status, headers){
				$scope.last_sync_time = data.msg;
			})
			.error(function(data, status, headers){
				console.log(data);
			});
	}

	// Write log steps by steps in while syncing
	$scope.log = function(string, inline){
		if(typeof inline == 'undefined')
			inline = 0;

		if(inline != $scope.step_inline)
			$scope.step_inline = inline;

		if($scope.step_inline == 0 && inline == 0)
			$scope.steps += '<p>'+ string +'</p>';
		else if($scope.step_inline == 0 && inline == 1)
			$scope.steps += '<p>'+ string;
		else if($scope.step_inline == 1 && inline == 0)
			$scope.steps += string +'</p>';
		else
			$scope.steps += string;

		$scope.step_inline = inline;

		$scope.stepsHTML = $sce.trustAsHtml($scope.steps);
	}

	// Sync Processing - Core of sync
	$scope.syncSubmitInner = function(dataS) {
		var each_process = 96/dataS.length;

		$scope.log('Start syncing...');
		angular.forEach(dataS, function(d, dkey){
			// Start each sync process
			SyncService.submitSync(d, $scope.postStatus).success(function(data, status, headers){
					$scope.log('Send request '+ (d.toUpperCase()) +' successfully!');

					// Data available
					if(!angular.isUndefined(data.result) && data.result == 'data-available') {
						var all = data.datas.length;

						if(angular.isArray(data.datas) && all > 0) {
							var added = 0, process = 0;
							var dstr = d;
							var action = 'Import';

							// Import each data
							$scope.log('Have data to sync. Process '+ action + ' ' + (d.toUpperCase()) + ' is being started!');
							$scope.log(action +' data '+ (d.toUpperCase()) +' ('+ all +' '+ dstr +')!', 1);
							angular.forEach(data.datas, function(r, rkey){
								SyncService.syncProcess(r, d)
									.success(function(dataP, statusP, headersP){
										// $scope.log('Data sync request:'+ dataP);
										$scope.processFunc( each_process/all );
										if(dataP.result == 'added'){
											added += 1;
											$scope.log('Completed insert/update '+ added +'/'+ all +' '+ dstr +'!');
										}
										else if(dataP.result == 'failed'){
											$scope.log('Failed insert/update! Object '+ d +' id: <b>'+ r +'</b>');
										}

										/*if($scope.process >= 100 && process == all) {
											$scope.log('Completed sync '+ d.toUpperCase() +'!');
										}*/
									})
									.error(function(dataP, statusP, headersP){
										$scope.log('Insert/update an element in '+ d.toUpperCase() +' process failed, try again later!');
									});
							});
						}
						else {
							$scope.processFunc(each_process);
							$scope.log('No '+ (d.toUpperCase()) +' data return!');
						}
					}
					else{
						$scope.processFunc(each_process);
						$scope.log('Completed sync '+ d.toUpperCase() +'!');
					}
				})
				.error(function(data, status, headers){
					$scope.log('Error sync: '+ data);
				});
		});

		/*$scope.process = 100;
		$scope.log('Completed Syncing!');
		$scope.messageSync = 'Completed Syncing!';
		$scope.justSyncVar = true;*/
	};

	// Annouce
	$scope.justSync = function(){
		return $scope.justSyncVar;
	}

	// Function been call when click button
	$scope.submitSync = function(dataSync) {
		// Check syncing what, if not process selected, return false, stop process
		if(!$scope.sync_customers && !$scope.sync_products && !$scope.sync_other ) {
			alert('Vui lòng chọn ít nhất một tiến trình đồng bộ!');
			return false;
		}

		// Begin syncing
		$scope.syncing = true;
		var dataSync = [];
		if($scope.sync_customers){
			dataSync.push('customers');
		}

		if($scope.sync_products){
			dataSync.push('products');
		}

		if($scope.sync_other){
			dataSync.push('pos');
		}

		$scope.syncSubmitInner(dataSync);
		$scope.saveSyncTime();
	}

	/*$scope.init = function(){
		var dataSync = [];
		SyncService.getSyncSettings()
			.success(function(data, status, headers){
				if(data.result == 'true'){
					$scope.submitSync(data.datas);
				}
			});
	}*/

	// Calling the init function
	$scope.getLastSyncInfo();
}]);

// Messages about coupon and promotion campaigns
function set_in_syncing(){
	set_cookie('admin_in_syncing', 'yes', 1);
}

function set_out_syncing(){
	delete_cookie('admin_in_syncing');
}

function check_in_syncing(){
	var cookie = get_cookie('admin_in_syncing');
	if(cookie == 'yes')
		return true;
	return false;
}

function get_cookie(cname) {
	var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}

function set_cookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function delete_cookie(cname) {
	document.cookie = cname + "=; expires=Thu, 01 Jan 1970 00:00:00 UTC"; 
}

