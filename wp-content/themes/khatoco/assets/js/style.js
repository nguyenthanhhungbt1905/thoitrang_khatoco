$(document).ready(function () {
    $('#toggle-menu-mobile').css('padding-top', $('#mobile-menu').height() + 15);
    $('.clickable-item').on('click', function () {
        let target = $(this).attr('data-target');
        $('#toggle-menu-mobile ').find('.style-dropdown').each(function (key, value) {
            if (!$(this).hasClass('d-none')) {
                $(this).addClass('d-none');
            }
        });
        $('#' + target).removeClass('d-none');
        let toggleDiv = $('#toggle-menu-mobile');
        if (toggleDiv.hasClass('close')) {
            toggleDiv.removeClass('close');
            toggleDiv.addClass('active');
        } else {
            toggleDiv.removeClass('active');
            toggleDiv.addClass('close');
        }
    });

    $('.item-collapse').each(function (key, value) {
        $(this).on('shown.bs.collapse', function () {
            $(this).closest('.collapse-group').find('.item-header i ').addClass('move');
        });
        $(this).on('hidden.bs.collapse', function () {
            $(this).closest('.collapse-group').find('.item-header i ').removeClass('move');
        });
    });
    $('#banner-slide').slick({
        slidesToShow: 1,
        fade: true,
        cssEase: 'ease-in-out',
        dots: true,
        arrows: false
    });
    $('#style-guide-slides').slick({
        slidesToShow: 1,
        centerMode: true,
        centerPadding: '20px',
        cssEase: 'ease-in-out',
        dots: true,
        arrows: false
    });
    $('.category-item').on('click', function () {
        $('.category-item').each(function (key, value) {
            $(this).removeClass('active');
        });
        $(this).addClass('active');
        let catID = $(this).attr('category-id');
        $.ajax({
            url: '/wp-admin/admin-ajax.php',
            type: 'POST',
            dataType: 'json',
            data: {
                action: 'filter_product_home_display',
                category: catID,
                filter: 'hot'
            }

        }).done(function (result) {
            $('.product-prepend').find('.col-xs-4').each(function (key, val) {
                $(this).remove();
            });
            $('.product-prepend').append(result.html);
            console.log(result)
        })
    });
    if ($('#event-detail').length) {
        $(window).on('scroll', function () {
            if ($(window).scrollTop() >= $('.wrapper-related-container').offset().top - 100) {
                $('#related-post').css('top', '50px');
                // $('.scroll-related').css('max-height', 'calc(100vh/2)');
                $('.scroll-related').addClass('expand-height');
            } else {
                $('#related-post').css('top', $('.wrapper-related-container').offset().top);
                // $('.scroll-related').css('max-height', 'calc(100vh/2.5)');
                $('.scroll-related').removeClass('expand-height');
            }
            if ($(window).scrollTop() >= ($('.content').offset().top + $('.content').outerHeight() - $('#related-post').height())) {
                $('#related-post').addClass('hidden-post');
            } else {
                $('#related-post').removeClass('hidden-post');
            }
        });
    }
    if ($('#style-guide-detail').length) {
        $(window).on('scroll', function () {
            if ($(window).scrollTop() >= $('.wrapper-related-container').offset().top - 100) {
                $('#related-post').css('top', '50px');
                // $('.scroll-related').css('max-height', 'calc(100vh/2)');
                $('.scroll-related').addClass('expand-height');
            } else {
                $('#related-post').css('top', $('.wrapper-related-container').offset().top);
                // $('.scroll-related').css('max-height', 'calc(100vh/2.5)');
                $('.scroll-related').removeClass('expand-height');
            }
            if ($(window).scrollTop() >= ($('.content').offset().top + $('.content').outerHeight() - $('#related-post').height())) {
                $('#related-post').addClass('hidden-post');
            } else {
                $('#related-post').removeClass('hidden-post');
            }
        });
    }
});
