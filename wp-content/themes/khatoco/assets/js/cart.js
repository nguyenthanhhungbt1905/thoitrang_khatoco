var apiUrl = wp_vars.ajaxurl;
var app = angular.module("khatocoCart", []);

app.service('cartService', ['$http', '$q', '$timeout', function ($http, $q, $timeout) {

    var getCartInfo = function () {
        return $http({
            method: 'GET',
            url: apiUrl,
            params: {action: 'cart_service_ajax', do_ajax: 'get_cart_info'}
        });
    };

    var getAddressInfo = function (action, id) {
        return $http({
            method: 'GET',
            url: apiUrl,
            params: {action: 'user_service_ajax', do_ajax: action, id: id}
        });
    };

    var updateVoucherSms = function (action, phone) {
        return $http({
            method: 'GET',
            url: apiUrl,
            params: {action: 'user_service_ajax', do_ajax: action, phone: phone}
        });
    };

    var getUserInfo = function () {
        return $http({
            method: 'GET',
            url: apiUrl,
            params: {action: 'cart_service_ajax', do_ajax: 'get_user_info'}
        });
    };

    var getStaticInfo = function () {
        return $http({
            method: 'GET',
            url: apiUrl,
            params: {action: 'cart_service_ajax', do_ajax: 'get_static_info'}
        });
    };

    var getCampaignInfo = function () {
        return $http({
            method: 'POST',
            url: apiUrl,
            params: {action: 'cart_service_ajax', do_ajax: 'get_campaign_info'}
        });
    };

    var verifyListBonusAndSpecial = function(cart){ // Kiểm tra số lượng sản phẩm tặng phẩm + giá ưu đãi có vượt quá kho
        return $http({
            method: 'POST',
            url: apiUrl,
            params: {action: 'cart_service_ajax', do_ajax: 'verify_list_list_bonus_special'},
            data : {cart : cart}
        });
    }

    var updateCart = function (cart) {
        return $http({
            method: 'POST',
            url: apiUrl,
            data: {cart : cart},
            params: {action: 'cart_service_ajax', do_ajax: 'update_cart'}
        });
    };

    var initCart = function (cart) {
        return $http({
            method: 'POST',
            url: apiUrl,
            params: {action: 'cart_service_ajax', do_ajax: 'init-cart'},
            data : {cart: cart}
        });
    };

    var removeFromCart = function (product) {
        return $http({
            method: 'GET',
            url: apiUrl,
            params: {action: 'cart_service_ajax', do_ajax: 'remove_from_cart', cart_id: product},
            //headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'}
        });
    };

    var getTransportFee = function (weight, transport_type, provinceid) {
        return $http({
            method: 'GET',
            url: apiUrl,
            params: {
                action: 'cart_service_ajax',
                do_ajax: 'get_transport_fee',
                weight: weight,
                transport_type: transport_type,
                provinceid: provinceid
            }
            //headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'}
        });
    };

    var verify_coupon = function (coupon) {

        return $http({
            method: 'GET',
            url: apiUrl,
            params: {action: 'cart_service_ajax', do_ajax: 'verify_coupon', coupon: coupon}
        });
    }

    var check_payment_method = function (cart) {
        return $http({
            method: 'GET',
            url: apiUrl,
            params: {action: 'cart_service_ajax', do_ajax: 'check_payment_method', cart: cart}
        });
    }

    var verify_member_identified = function (member_identified) {
        return $http({
            method: 'GET',
            url: apiUrl,
            params: {
                action: 'cart_service_ajax',
                do_ajax: 'verify_member_identified',
                member_identified: member_identified
            }
        });
    }

    var verify_member_identified_otp = function (otp_code) {
        return $http({
            method: 'GET',
            url: apiUrl,
            params: {action: 'cart_service_ajax', do_ajax: 'verify_member_identified_otp', otp_code: otp_code}
        });
    }

    var checkout = function (data) {
        var nonce = document.getElementById('_checkout_nonce').value;
        data['_checkout_nonce'] = nonce;
        // if(data.cart.coupon){
        //     data.cart.coupon.code = data.cart.coupon.code.substring(0,6);
        // }
       
        // console.log(data.cart.coupon.code);
        return $http({
            method: 'POST',
            url: apiUrl,
            params: {action: 'cart_service_ajax', do_ajax: 'checkout'},
            data: data,
            //headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'}
        });
    };

    return {
        getCartInfo: getCartInfo,
        getUserInfo: getUserInfo,
        getAddressInfo: getAddressInfo,
        getStaticInfo: getStaticInfo,
        getCampaignInfo: getCampaignInfo,
        updateCart: updateCart,
        removeFromCart: removeFromCart,
        getTransportFee: getTransportFee,
        verify_coupon: verify_coupon,
        check_payment_method: check_payment_method,
        verify_member_identified: verify_member_identified,
        verify_member_identified_otp: verify_member_identified_otp,
        verifyListBonusAndSpecial : verifyListBonusAndSpecial,
        initCart: initCart,
        checkout: checkout,
        updateVoucherSms:updateVoucherSms

    }
}]);

app.controller('CartController', ['$scope', '$sce', '$q', 'cartService', '$http', function ($scope, $sce, $q, cartService, $http) {
    $scope.updating = false;
    $scope.isEmpty = false;
    $scope.pre_init = false;
    $scope.isFirstCheckOut = false; // First checkOut
    $scope.pre_submit = true;

    $scope.cartInfo = [];
    $scope.userInfo = [];
    $scope.receiver = {};
    $scope.stores = [];

    $scope.districts = [];
    $scope.districtsReceiver = [];
    $scope.provinces = [];
    $scope.datapost = {};

    $scope.otherReceiver = 0;

    $scope.campaignInfo = null;
    $scope.hasCamp = false;

    $scope.giftEnabled = false;
    $scope.giftMethodFee = [];
    $scope.selectedGiftProductIds = [];
    $scope.selectedGiftProducts = [];
    $scope.selectedGiftMethods = [];
    $scope.giftFeeTotal = 0;
    $scope.giftMessage = '';

    $scope.selectedShippingMethod = 1;
    $scope.showingShippingFee = false;

    $scope.transportMethods = []
    $scope.selectedTransport = {"type": 2, "province": 1, 'fee': 0};
    $scope.selectedTransportMethodType = 0;
    $scope.showingTransportMethod = true;
    $scope.transportFeeTotalFormated = '0 Đ';
    $scope.transportFeeTotal = 0;

    $scope.showingStoreList = false;
    $scope.selectedStore;
    $scope.selectedPaymentMethod = 1;

    $scope.checkoutNote = '';
    $scope.order_total = 0;
    $scope.total_bill = 0;

    $scope.order_total_not_sale = 0;

    $scope.orders_campaign_sale = [];

    $scope.card = '0';

    $scope.isVip = 0;
    $scope.vip_text = 'Thẻ thành viên';
    $scope.vipAfter = 1;
    $scope.vip_action = '';
    $scope.vip_lv = '';

    $scope.hasIncentive = false;
    $scope.incentive = 0;
    $scope.incent_text = '';

    $scope.hasCoupon = 0;
    $scope.coupon = '';
    $scope.coupon_action = '';
    $scope.coupon_saleoff_val = 0;

    $scope.member_identified_verify = '';
    $scope.member_identified_verify_show = 1;
    $scope.member_identified = '';
    $scope.verify_coupon_message = '';
    $scope.verify_member_identified_message = '';

    $scope.remainBonus = -1;
    $scope.remainSpecial = -1;

    $scope.getIndexOfValue = function(obj,value){
        var keys = Object.keys(obj);
        for (var i=0; i< keys.length; i++){
            var key = keys[i];
            if (obj[key] === value)
                return i;
        }
        return -1;
    }
    $scope.switchCamp = function (value) {
        if (value === 'bonus') {
            $scope.campaignInfo.showBonus = true;
            $scope.campaignInfo.showSpecialPrice = false;
        } else {
            $scope.campaignInfo.showBonus = false;
            $scope.campaignInfo.showSpecialPrice = true;
        }

    }

    $scope.setTransportFeeLocal = function () { // we should we calculate transport fee in local for better performance
        if ($scope.cartInfo.total_weight == 0) {
            $scope.selectedTransport.fee = 0;
            return;
        }
        var provinceId = 0;
        if ($scope.otherReceiver == 1) {
            provinceId = typeof ($scope.receiver.province) != 'undefined' ? $scope.receiver.province.provinceid : 0;
        } else {
            provinceId = typeof ($scope.datapost.province) != 'undefined' ? $scope.datapost.province.provinceid : 0;
        }

        $scope.selectedTransport.province = parseInt(provinceId);

        if ($scope.selectedTransport.type == 2) {
            var region = 3;
            if ([52, 48, 64, 62, 46, 54, 44, 49, 51, 45].indexOf(parseInt(provinceId)) !== -1) {
                region = 1;
            } else if (provinceId == 56) {
                region = 0;
            }

            var region_price = {
                "0": {
                    '50': 8000,
                    '100': 8000,
                    '250': 10000,
                    '500': 12500,
                    '1000': 15000,
                    '1500': 18000,
                    '2000': 21000,
                    '2001+': 1600
                },
                "1": {
                    '50': 8500,
                    '100': 12500,
                    '250': 16500,
                    '500': 23500,
                    '1000': 33000,
                    '1500': 40000,
                    '2000': 48500,
                    '2001+': 3800
                },
                "3": {
                    '50': 10000,
                    '100': 14000,
                    '250': 22500,
                    '500': 29500,
                    '1000': 43500,
                    '1500': 55500,
                    '2000': 67500,
                    '2001+': 9500
                }
            };

            var list_prices = region_price[region];
            for (var i = 0; i < Object.keys(list_prices).length; i++) {
                var weight_price = parseInt(Object.keys(list_prices)[i.toString()]);

                if (weight_price == 2001 && $scope.cartInfo.total_weight >= weight_price) { // > 2001 gam , đơn giá tính trên mỗi 500g tiếp theo
                    $scope.selectedTransport.fee = list_prices['2000'];
                    var count_weight = $scope.cartInfo.total_weight - 2000;
                    $scope.selectedTransport.fee += Math.ceil(count_weight / 500) * list_prices[weight_price.toString() + '+'];

                }
                else if ($scope.cartInfo.total_weight <= weight_price) {
                    $scope.selectedTransport.fee = list_prices[weight_price.toString()];
                    $scope.selectedTransport.fee = $scope.selectedTransport.fee.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")  + ' Đ';
                    break;
                }
            }
        }

        // $scope.transportFeeTotal = data.transport_fee;
    }

    $scope.disableInternalPayment = false;
    $scope.otherReceiverClick = function (){
        if ($scope.otherReceiver){
            if ($scope.selectedPaymentMethod == 1)
                $scope.setSelectedPaymentMethod(2);
            $scope.disableInternalPayment = true;
        } else {
            $scope.disableInternalPayment = false;
        }
    }
    $scope.getTransportFee = function () {
        $scope.setTransportFeeLocal();
    }


    $scope.getWards = function (districtid) {
        cartService.getAddressInfo('get_wards', districtid)
            .success(function (data, status, headers) {
                if (data.success == 'true') {
                    $scope.wards = $scope.objectToArray(data.result);
                    if (data.current && data.current != 0)
                        $scope.datapost.ward = data.result[data.current]
                }
            })
            .error(function (data, status, headers) {
                $scope.error = true;
            });
    };

    $scope.getDistricts = function (provinceid) {
        cartService.getAddressInfo('get_districts', provinceid)
            .success(function (data, status, headers) {
                if (data.success == 'true') {
                    $scope.districts = $scope.objectToArray(data.result);
                    if (data.current && data.current != 0) {
                        $scope.datapost.district = data.result[data.current];
                        $scope.getWards(data.current);
                    }
                }
            })
            .error(function (data, status, headers) {
                $scope.error = true;
            });
    };

    $scope.getProvinces = function () {
        cartService.getAddressInfo('get_provinces', 0).success(function (data, status, headers) {
            if (data.success == 'true') {
                $scope.provinces = data.result;
                if (data.current && data.current != 0) {
                    $scope.datapost.province = $scope.provinces[data.current];
                    $scope.getDistricts(data.current);
                }
                $scope.provinces = $scope.objectToArray(data.result);
                $scope.setTransportFeeLocal();
            }
        })
            .error(function (data, status, headers) {
                $scope.error = true;
            });
    };

    $scope.setCustomerDistrict = function () {
        $scope.getWards($scope.datapost.district.districtid);
    };


    $scope.setCustomerProvince = function () {
        $scope.getDistricts($scope.datapost.province.provinceid);
        $scope.getTransportFee();
    };

    $scope.setReceiverDistrict = function () {
        cartService.getAddressInfo('get_wards', $scope.receiver.district.districtid)
            .success(function (data, status, headers) {
                if (data.success == 'true') {
                    $scope.wardsReceiver = $scope.objectToArray(data.result);
                }
            })
            .error(function (data, status, headers) {
                $scope.error = true;
            });
    };

    $scope.setReceiverProvince = function () {
        $scope.getTransportFee();
        /*cartService.getAddressInfo('get_districts', $scope.receiver.province.provinceid).success(function (data, status, headers) {
         if (data.success == 'true') {
         $scope.districtsReceiver = $scope.objectToArray(data.result);
         $scope.getTransportFee();
         }
         })
         .error(function (data, status, headers) {
         $scope.error = true;
         });*/
    };

    $scope.loadStaticInfo = function () {
        if (!$scope.pre_init)
            $scope.getProvinces();

        cartService.getStaticInfo().success(function (data, status, headers) {
            // $scope.stores = data.stores;
            $scope.giftMethodFee = data.gift_method_fee;
            $scope.transportMethods = data.transport_methods;
        }).error(function (data, status, headers) {
            showKhatocoPopup('Không thể kết nối đến máy chủ, F5 để thử lại.', 'Lỗi kết nối');
        });
    };
    $scope.loadCartInfo = function (dataResponse) {
        console.log('data:'+dataResponse);
        if (dataResponse == null || dataResponse == 'null' || dataResponse == 0 || dataResponse == '0') {
            $scope.isEmpty = true;
            return;
        }else{
            // Load cart
            $scope.cartInfo = dataResponse;
    //            load payment method
            $scope.selectedPaymentMethod = dataResponse.payment;
    //            load user info
            $scope.userInfo = dataResponse.user_info;
    //            load campaign info

            $scope.loadCampInfo(dataResponse);
            $scope.getProvinces();

            // Transport Fee
    //            $scope.getTransportFee();

            // Vip customer
            $scope.checkVip();

            // Coupon
            if ($scope.coupon) {
                $scope.verify_coupon();
            }

            // Calculate last price
            $scope.calculateLastTotal();
        }
            
        
        
    };

    $scope.calculateLastTotal = function () {
        $scope.cartInfo.total = 0; // Tổng Giá gốc sản phẩm
        $scope.cartInfo.total_sale_off = 0; // Tổng giá sau khi giảm (sale_off)
        $scope.cartInfo.total_bill = 0; // Tổng thành tiền ( sau khi + thêm giá các sản phẩm mua thêm - trong trường hợp khuyến mãi mua thêm sản phẩm với giá đặc biệt )
        $scope.cartInfo.order_total = 0; // Tổng thành tiền cuối cùng khách hàng phải trả
        if ($scope.cartInfo.length === 0) {
            return;
        }
        for (var i = 0; i < Object.keys($scope.cartInfo.products).length; i++) {
            var key = Object.keys($scope.cartInfo.products)[i];
            $scope.cartInfo.total += $scope.cartInfo.products[key].price * $scope.cartInfo.products[key].quantity;
            $scope.cartInfo.total_sale_off += $scope.cartInfo.products[key].price_saleoff.value * $scope.cartInfo.products[key].quantity;
        }
        $scope.cartInfo.total_bill = $scope.cartInfo.total_sale_off;
        if ($scope.cartInfo.campaignInfo) {
            if ($scope.campaignInfo.listSpecialPrice.totalPrice && $scope.campaignInfo.listSpecialPrice.totalPrice > 0) {
                $scope.cartInfo.total_bill += $scope.campaignInfo.listSpecialPrice.totalPrice; // + price when user get bonus special product
            }
        }
        $scope.cartInfo.order_total = $scope.cartInfo.total_bill;
        if ($scope.cartInfo.campaignInfo && $scope.cartInfo.campaignInfo.detail){
            for (var i =0; i<$scope.cartInfo.campaignInfo.detail.length; i++){
                var type = $scope.cartInfo.campaignInfo.detail[i].type;
                if (type !== undefined && type !== 'product'){
                    $scope.cartInfo.order_total -= $scope.cartInfo.campaignInfo.detail[i].value;
                }
            }
        }

        if ($scope.cartInfo.coupon && $scope.cartInfo.coupon.type!==null){
            if ($scope.cartInfo.coupon.type === 0){
                $scope.cartInfo.coupon.saleoff_val = $scope.cartInfo.total_bill * $scope.cartInfo.coupon.value/100;
            }
            if ($scope.cartInfo.coupon.type === 1){
                $scope.cartInfo.coupon.saleoff_val = $scope.cartInfo.coupon.value;
            }

            $scope.cartInfo.coupon.saleoff_val = Math.round($scope.cartInfo.coupon.saleoff_val);
            if ($scope.cartInfo.coupon.saleoff_val > $scope.cartInfo.coupon.limit && $scope.cartInfo.coupon.limit > 0)
                $scope.cartInfo.coupon.saleoff_val = $scope.cartInfo.coupon.limit
            $scope.cartInfo.order_total -= $scope.cartInfo.coupon.saleoff_val; // - coupon when use apply coupon
        }

        $scope.order_total = $scope.cartInfo.order_total; // Sync order_total
        $scope.total_bill = $scope.cartInfo.total_bill; // Sync total_bill
    }
//        $scope.loadUserInfo = function () {
//            cartService.getUserInfo().success(function (data, status, headers) {
//                // No logined
//                if (data.fullname != '' || data.email != '' || data.phone != '' || data.address != '') {
//                    $scope.userInfo = data;
//                    $scope.orderAddress = data.address;
//                    $scope.datapost.address_only = data.address_only;
//                    $scope.card = data.card;
//
//                    if (data.card != '0' && !angular.isUndefined($scope.campaignInfo.incentives)) {
//                        var card = '0';
//                        switch (data.card) {
//                            case '1':
//                            case 'SILVER':
//                                card = '1';
//                                break;
//                            case '2':
//                            case 'GOLD':
//                                card = '2';
//                                break;
//                            case '3':
//                            case 'DIAMOND':
//                                card = '3';
//                                break;
//                        }
//
//                        var incentives_vip = $scope.objectToArray($scope.campaignInfo.incentives.vip_cards);
//                        if (!angular.isUndefined(incentives_vip[card].id)) {
//                            $scope.campaign_influence = true;
//                            $scope.isVip = 1;
//                            $scope.vip_text = 'Thẻ thành viên';
//                            $scope.vipAfter = 1 - (incentives_vip[card].value / 100);
//
//                        }
//                    }
//                }
//
//            })
//                    .error(function (data, status, headers) {
//                        showKhatocoPopup('Không thể kết nối đến máy chủ, F5 để thử lại.');
//                    });
//        };

    $scope.cloneObj = function(target, newObj){
        if (target == null || target == undefined ){
            target = newObj;
            return;
        }

        var keys = Object.keys(newObj);
        for (var i=0; i < keys.length; i++) {
            var val = newObj[keys[i]];

            if (typeof val === 'object' ){
                $scope.cloneObj(target[keys[i]], val);
            } else {
                target[keys[i]]= val;
            }
        }
        return;
    }

    $scope.initSize = function(){
        // console.log('campaign'+$scope.campaignInfo)
        if (isValid($scope.campaignInfo.listBonus) && isValid($scope.campaignInfo.listBonus.listProdBonus) ){
            for (var i=0; i < $scope.campaignInfo.listBonus.listProdBonus.length;i++ ){
                if(!isValid($scope.campaignInfo.listBonus.listProdBonus[i].size)){
                    $scope.campaignInfo.listBonus.listProdBonus[i].size = $scope.campaignInfo.listBonus.listProdBonus[i].size_default;

                }
            }
        }

        if (isValid($scope.campaignInfo.listSpecialPrice) && isValid($scope.campaignInfo.listSpecialPrice.listProdBonus) ){
            for (var i=0; i < $scope.campaignInfo.listSpecialPrice.listProdBonus.length;i++ ){
                if(!isValid($scope.campaignInfo.listSpecialPrice.listProdBonus[i].quantity)){
                    $scope.campaignInfo.listSpecialPrice.listProdBonus[i].size = $scope.campaignInfo.listSpecialPrice.listProdBonus[i].size_default;
                }
            }
        }
    }
    $scope.loadCampInfo = function (dataResponse) {
        $scope.campaignInfo = dataResponse.campaignInfo;

        $scope.initSize();
        $scope.initQuantity();
        // if (isValid($scope.campaignInfo) && isValid(dataResponse.campaignInfo)){
        //     // Merge bonus
        //     if (isValid($scope.campaignInfo.listBonus) && isValid(dataResponse.campaignInfo.listBonus)){
        //         if (isValid($scope.campaignInfo.listBonus.listProdBonus) && isValid(dataResponse.campaignInfo.listBonus.listProdBonus)){
        //             getInfoFromObjectInArraySet($scope.campaignInfo.listBonus.listProdBonus, dataResponse.campaignInfo.listBonus.listProdBonus);
        //         }
        //         else if (!isValid($scope.campaignInfo.listBonus.listProdBonus) && isValid(dataResponse.campaignInfo.listBonus.listProdBonus) )
        //             $scope.campaignInfo.listBonus.listProdBonus = dataResponse.campaignInfo.listBonus.listProdBonus;
        //     }
        //     else if (!isValid($scope.campaignInfo.listBonus) && isValid(dataResponse.campaignInfo.listBonus) )
        //         $scope.campaignInfo.listBonus = dataResponse.campaignInfo.listBonus;

        //     // Merge special price
        //     if (isValid($scope.campaignInfo.listSpecialPrice) && isValid(dataResponse.campaignInfo.listSpecialPrice)){
        //         if (isValid($scope.campaignInfo.listSpecialPrice.listProdBonus) && isValid(dataResponse.campaignInfo.listSpecialPrice.listProdBonus)){
        //             getInfoFromObjectInArraySet($scope.campaignInfo.listSpecialPrice.listProdBonus, dataResponse.campaignInfo.listSpecialPrice.listProdBonus);
        //         }
        //         else if (!isValid($scope.campaignInfo.listSpecialPrice.listProdBonus) && isValid(dataResponse.campaignInfo.listSpecialPrice.listProdBonus) )
        //             $scope.campaignInfo.listSpecialPrice.listProdBonus = dataResponse.campaignInfo.listSpecialPrice.listProdBonus;
        //     }
        //     else if (!isValid($scope.campaignInfo.listSpecialPrice) && isValid(dataResponse.campaignInfo.listSpecialPrice) )
        //         $scope.campaignInfo.listSpecialPrice = dataResponse.campaignInfo.listSpecialPrice;

        // }

        // else if (!isValid($scope.campaignInfo) && isValid(dataResponse.campaignInfo)){
        //     $scope.campaignInfo = dataResponse.campaignInfo;
        // }


        // $scope.cartInfo.campaignInfo = $scope.campaignInfo;
        // console.log($scope.cartInfo.campaignInfo) // Sync
    }

    $scope.init = function () {
        $scope.updating = true;
       
        cartService.initCart($scope.cartInfo).success(function (data) {
            // console.log('test: '+data)
            $scope.loadCartInfo(data);
            $scope.updating = false;
        }).error(function () {
            showKhatocoPopup('Không thể kết nối đến máy chủ, F5 để thử lại.', 'Lỗi kết nối');
            $scope.updating = false;
        });
    };

    // Check product is saling off
    $scope.hasSaleoff = function (product) {
        if (product.price_saleoff.value == product.price)
            return false;
        return true;
    }

    $scope.backCheckout = function(){
        $scope.isFirstCheckOut = false;
    }

    // Update cart and total after updating
    $scope.updateCarts = function () {
        for (key in $scope.cartInfo.products) {
            var temp_p = $scope.cartInfo.products[key];
            if (temp_p['quantity'] == null || temp_p['quantity'] < 1) {
                showKhatocoPopup("Số lượng sản phẩm phải > 0",'Lỗi vượt số lượng sản phẩm');
                $scope.cartInfo.products[key]['quantity'] = 1;
                return;
            }
        }
        cartService.updateCart($scope.cartInfo).success(function (data) {
            if (data.msg.error) {
                showKhatocoPopup(data.msg.error);
            }
            $scope.loadCartInfo(data);
        }).error(function () {
            showKhatocoPopup('Không thể kết nối đến máy chủ, F5 để thử lại.');
            $scope.updating = false;
        });
    }

    // Remove a product from cart
    $scope.removeFromCart = function (product) {
        if (confirm('Bạn có chắc muốn xoá sản phẩm này khỏi giỏ hàng?')) {
            cartService.removeFromCart(product).success(function (data) {
                $scope.loadCartInfo(data);
            })
                .error(function (data) {
                    showKhatocoPopup('Không thể kết nối đến máy chủ, F5 để thử lại.');
                });
        } else
            return false;
    };

    $scope.receiverChange = function () {
    }

    $scope.checkVip = function () {

    }

    $scope.setSelectedPaymentMethod = function (method) {
        $scope.cartInfo.payment = method;
        cartService.check_payment_method($scope.cartInfo).success(function (data) {
            $scope.selectedPaymentMethod = data.payment;
            $scope.loadCartInfo(data);
        }); // update paypment method to session

        $scope.selectedPaymentMethod = method;
    };

    $scope.objectToArray = function (object) {
        var result = [];
        for (ob in object) {
            result.push(object[ob]);
        }
        return result;
    };

    $scope.addGiftProducts = function () {
        if (!$scope.selectedGiftMethods['box'] && !$scope.selectedGiftMethods['card']) {
            showKhatocoPopup('Vui lòng chọn dịch vụ.');
            return false;
        }

        var tmp = {
            number: $scope.selectedGiftProducts.length + 1,
            products: [],
            name: '',
            giftbox: false,
            giftcard: false,
            message: $scope.giftMessage,
            giftTotal: 0,
            desc: '',
            showingMessage: false
        };

        var tmp_name = [];

        for (key in $scope.selectedGiftProductIds) {
            if ($scope.selectedGiftProductIds[key]) {

                $scope.cartInfo.products[key].gift = true;

                tmp.products.push($scope.cartInfo.products[key].cart_id);
                tmp_name.push($scope.cartInfo.products[key].sku + '-' + $scope.cartInfo.products[key].color_code + '-' + $scope.cartInfo.products[key].size);

            }
        }

        if (tmp.products.length > 0) {

            if ($scope.selectedGiftMethods['box'] === true) {
                tmp.giftbox = true;
                tmp.giftTotal = tmp.giftTotal + +$scope.giftMethodFee.box;
                tmp.desc = 'Gói'
            }

            if ($scope.selectedGiftMethods['card'] === true) {
                tmp.giftcard = true;
                tmp.giftTotal = tmp.giftTotal + +$scope.giftMethodFee.card;
                tmp.desc = tmp.desc ? tmp.desc + '/Thiệp' : 'Thiệp';
            }

            tmp.name = tmp_name.join('; ');

            $scope.selectedGiftProducts.push(tmp);

            $scope.giftMessage = '';
            $scope.selectedGiftProductIds = [];
            $scope.selectedGiftMethods = [];

            $scope.updateGiftFeeTotal();

        } else {
            showKhatocoPopup('Vui lòng chọn sản phẩm.');
            return false;
        }
    };

    $scope.removeSelectedGiftProducts = function (giftProduct) {
        for (pkey in giftProduct.products) {
            $scope.cartInfo.products[giftProduct.products[pkey]].gift = false;
        }
        $scope.selectedGiftProducts.splice($scope.selectedGiftProducts.indexOf(giftProduct), 1);

        $scope.updateGiftFeeTotal();
    };

    $scope.toggleShowingMessage = function (giftProduct) {
        giftProduct.showingMessage = !giftProduct.showingMessage;
    };

    $scope.updateGiftFeeTotal = function () {
        $scope.giftFeeTotal = 0;

        if ($scope.selectedGiftProducts.length > 0) {
            for (gkey in $scope.selectedGiftProducts) {
                $scope.giftFeeTotal = $scope.giftFeeTotal + +$scope.selectedGiftProducts[gkey].giftTotal;
            }
        }

        $scope.get_total();
        $scope.get_total_formated();
    };

    $scope.verify_coupon = function () {
        $scope.verify_coupon_message = '';
        if (!$scope.coupon.code) {
            $scope.verify_coupon_message = 'Bạn chưa nhập mã! Vui lòng nhập mã rồi ấn xác nhận!';
            $scope.coupon = null;
            $scope.cartInfo.coupon = $scope.coupon; // Sync coupon
            return false;
        }
         // $subStringCode = $scope.coupon.code.substring(0,6);
          $subStringCode = $scope.coupon.code;
          $subStringCode = $subStringCode.substring(0,6);
        if($subStringCode == 'ktcvne' || $subStringCode == 'KTCVNE'){
           

            codeUpper = $scope.coupon.code.toUpperCase();
            var rs;
            var token_key = CryptoJS.MD5('vne_egift'+'1032106295'+'0ceaa9f06542d492fd47555ff66dcd8d'+codeUpper+1).toString();
                

                jQuery.ajax({
                    url: wp_vars.ajaxurl,
                    
                    type: 'POST',
                    dataType: 'json',
                    
                    data:{action: 'cart_service_egift_ajax',code:codeUpper,token_key:token_key},    
                    success:function(data){
                        // $('.sale-off-panel > .messageEgift').html('<p>'+data.message+'</p>');
                        // rs = data.error;
                        if(data.error == 0){

                           cartService.verify_coupon({coupon: $subStringCode}).success(function (data) {
                                    if (data.result == 0) {
                                        $scope.verify_coupon_message = data.message;
                                        $scope.verify_coupon_message_class = 'error-msg';

                                        $scope.coupon = {};
                                    } else {
                                        var message = '';

                                        if (data.return[0] == 0) {
                                            message = 'Bạn được giảm giá ' + data.return[1] + '% đơn hàng';

                                            if (parseInt(data.return[2])>0){
                                             message +=', giảm không quá '+ data.return[2].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")+' Đ';
                                            }
                                        } else {
                                            message = 'Bạn được giảm giá ' + data.return[1].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")  + ' Đ';
                                        }
                                        $scope.coupon.type = parseInt(data.return[0]);
                                        $scope.coupon.value = parseInt(data.return[1]);
                                        $scope.coupon.limit = parseInt(data.return[2]);

                                        $scope.verify_coupon_message = message;
                                        $scope.verify_coupon_message_class = 'success-msg';

                                    }

                                    $scope.coupon.code = $subStringCode;
                                    $scope.cartInfo.coupon = $scope.coupon; // Sync coupon
                                    // $scope.cartInfo.coupon.code = codeUpper;
                                    $scope.cartInfo.coupon.isVNECODE = codeUpper;
                                    $scope.cartInfo.coupon.updated = 0;
                                 
                                    console.log($scope.cartInfo.coupon);
                                $scope.calculateLastTotal();
                                // $scope.coupon.code = codeUpper;
                                }).error(function () {
                                showKhatocoPopup('Không thể kết nối đến máy chủ, F5 để thử lại.');
                            });
                              
                        }    
                       else{
                           showKhatocoPopup(data.message)
                        }  
                    }   
                    
                    
                });
              

                // return rs;
            
 



        }
        
        else{
            cartService.verify_coupon({coupon: $scope.coupon.code}).success(function (data) {
                console.log($scope.coupon.code);
                    if (data.result == 0) {
                        $scope.verify_coupon_message = data.message;
                        $scope.verify_coupon_message_class = 'error-msg';
                        $scope.coupon = {};
                    } else {
                        var message = '';

                        if (data.return[0] == 0) {
                            message = 'Bạn được giảm giá ' + data.return[1] + '% đơn hàng';

                            if (parseInt(data.return[2])>0){
                             message +=', giảm không quá '+ data.return[2].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") + ' Đ';
                            }
                        } else {
                            message = 'Bạn được giảm giá ' + data.return[1].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") + ' Đ';
                        }
                        $scope.coupon.type = parseInt(data.return[0]);
                        $scope.coupon.value = parseInt(data.return[1]);
                        $scope.coupon.limit = parseInt(data.return[2]);
                       
                        $scope.verify_coupon_message = message;
                        $scope.verify_coupon_message_class = 'success-msg';

                    }
                
                $scope.cartInfo.coupon = $scope.coupon; // Sync coupon
                $scope.cartInfo.coupon.updatedToEgift = '0';

                console.log($scope.cartInfo.coupon);
                $scope.calculateLastTotal();

                }).error(function () {
                showKhatocoPopup('Không thể kết nối đến máy chủ, F5 để thử lại.');
            });
        }

    }

    $scope.verify_member_identified = function (member_identified) {
        $scope.verify_member_identified_message = '';
        if (!$scope.member_identified) {
            $scope.verify_member_identified_message = 'Bạn chưa nhập mã! Vui lòng nhập mã rồi ấn xác nhận!';
            return false;
        }

        cartService.verify_member_identified($scope.member_identified)
            .success(function (data, status, headers) {
                if (data.result == false) {
                    $scope.verify_member_identified_message = 'Mã thẻ Quý khách đã nhập không đúng';
                    return false;
                }
                $scope.vip_lv = data.vip;
                $scope.verify_member_identified_message = data.message;

                if (data.result == 1) {
                    $scope.member_identified_verify_show = 0;
                }
                return false;
            })
            .error(function (data) {
                showKhatocoPopup('Không thể kết nối đến máy chủ, F5 để thử lại.');
            });
    }

    $scope.verify_member_identified_otp = function (member_identified_verify) {
        $scope.verify_member_identified_message = '';
        if (!$scope.member_identified_verify) {
            $scope.verify_member_identified_message = 'Bạn chưa nhập mã! Vui lòng nhập mã rồi ấn xác nhận!';
            return false;
        }

        cartService.verify_member_identified_otp($scope.member_identified_verify)
            .success(function (data, status, headers) {
                if (data.result == true) {
                    $scope.verify_member_identified_message = 'Thẻ thành viên được chấp nhận';
                    $scope.card = $scope.vip_lv;
                    $scope.get_total();
                    $scope.get_total_formated();
                    $scope.vip_action = '(' + $scope.formatCurrentcy(Math.round($scope.cartInfo.total * (1 - $scope.vipAfter))) + ')';
                } else
                    $scope.verify_member_identified_message = 'Thẻ thành viên không đúng. Vui lòng F5 rồi nhập lại!';
                return false;
            })
            .error(function () {
                showKhatocoPopup('Không thể kết nối đến máy chủ, F5 để thử lại.');
            });
    }

    $scope.get_total = function () {
        // Check campaign orders sale
        $scope.checkVip();
        $scope.order_total = Math.round($scope.giftFeeTotal + $scope.cartInfo.total - $scope.campaignInfo.decrease - $scope.coupon_saleoff_val);
        return $scope.order_total;
    };

    $scope.get_total_formated = function () {
        $scope.checkVip();
        $scope.order_total_formated = $scope.formatCurrentcy(Math.round($scope.order_total));

        return $scope.order_total_formated;
    };

    $scope.formatCurrentcy = function (x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") + ' Đ';
    };

    $scope.checkout = function () {
        if (!$scope.cartInfo.total) {
            showKhatocoPopup('Chưa có sản phẩm nào trong giỏ hàng.');
            return false;
        }

        if ($scope.pre_submit != 1) {
            showKhatocoPopup('Bạn phải đồng ý với các điều khoản của Khatoco trước khi thanh toán');
            return false;
        }

        var dataCheckout = {
            cart : $scope.cartInfo,
            firstCheckOut : $scope.isFirstCheckOut,
            checkoutNote: $scope.checkoutNote,
            transportFeeTotal: $scope.selectedTransport.fee,
            paymentMethod: $scope.selectedPaymentMethod,
            shippingMethod: 2, // Chuyen phat nhanh

            isVip: $scope.isVip,
            vipAfter: $scope.vipAfter, // vip sale value
            otherReceiver : $scope.otherReceiver
        };

        dataCheckout.customer = {
            fullname: $scope.userInfo.fullname,
            email: $scope.userInfo.email,
            phone: $scope.userInfo.phone,
            province: $scope.datapost.province,
            district: $scope.datapost.district,
            ward: $scope.datapost.ward,
            address: $scope.userInfo.address
        };

        dataCheckout.receiver = {
            fullname : $scope.receiver.fullname,
            email : $scope.receiver.email,
            phone : $scope.receiver.phone,
            province : $scope.receiver.province,
            address : $scope.receiver.address
        }

        if ($scope.otherReceiver) {
            if (!$scope.receiver.phone || !$scope.receiver.province || !$scope.receiver.address) {
                showKhatocoPopup('Quý khách nhập thiếu thông tin người nhận!');
                return false;
            } else
                dataCheckout.receiver = $scope.receiver;
        }
        $scope.updating = true;
        cartService.checkout(dataCheckout).success(function (data) {
            if (data.result == 0) {
                showKhatocoPopup(data.error);
            }
            else if (data.result == 1){
                cartService.updateVoucherSms('update_voucher_sms', dataCheckout.customer.phone)
                .success(function (data, status, headers) {
                    if (data.success == 'true') {
                        
                    }
                })
                .error(function (data, status, headers) {
                    // $scope.error = true;
                });            
                $scope.isFirstCheckOut = true;
                $scope.fco = data.data;
            }
            else if (data.result == 2 || data.result == 3){
                cartService.updateVoucherSms('update_voucher_sms', dataCheckout.customer.phone)
                .success(function (data, status, headers) {
                    if (data.success == 'true') {
                        
                    }
                })
                .error(function (data, status, headers) {
                    // $scope.error = true;
                });            
                showKhatocoPopup (data.message);
                window.location.href = data.nextUrl;
            }
            $scope.updating = false;
           
        }).error(function () {
                showKhatocoPopup('Không thể kết nối đến máy chủ, F5 để thử lại.');
                $scope.updating = false;
            });

    };

    $scope.verifyListBonusAndSpecialPrice = function () {
        var dataCheckout = {
            cart : $scope.cartInfo,
            firstCheckOut : $scope.isFirstCheckOut,
            checkoutNote: $scope.checkoutNote,
            transportFeeTotal: $scope.selectedTransport.fee,
            paymentMethod: $scope.selectedPaymentMethod,
            shippingMethod: 2, // Chuyen phat nhanh

            isVip: $scope.isVip,
            vipAfter: $scope.vipAfter, // vip sale value
            otherReceiver : $scope.otherReceiver
        };

        dataCheckout.customer = {
            fullname: $scope.userInfo.fullname,
            email: $scope.userInfo.email,
            phone: $scope.userInfo.phone,
            province: $scope.datapost.province,
            district: $scope.datapost.district,
            ward: $scope.datapost.ward,
            address: $scope.userInfo.address
        };

        dataCheckout.receiver = {
            fullname : $scope.receiver.fullname,
            email : $scope.receiver.email,
            phone : $scope.receiver.phone,
            province : $scope.receiver.province,
            address : $scope.receiver.address
        }

        if ($scope.otherReceiver) {
            if (!$scope.receiver.phone || !$scope.receiver.province || !$scope.receiver.address) {
                showKhatocoPopup('Quý khách nhập thiếu thông tin người nhận!');
                return false;
            } else
                dataCheckout.receiver = $scope.receiver;
        }
        $scope.updating = true;
        cartService.checkout(dataCheckout).success(function (data) {
            if (data.result == 0) {
                showKhatocoPopup(data.error);
            }
            else if (data.result == 1){
                $scope.isFirstCheckOut = true;
                $scope.fco = data.data;
            }
            else if (data.result == 2 || data.result == 3){
                showKhatocoPopup (data.message);
                window.location.href = data.nextUrl;
            }
            $scope.updating = false;
        }).error(function () {
            showKhatocoPopup('Không thể kết nối đến máy chủ, F5 để thử lại.');
            $scope.updating = false;
        });

    };

    $scope.countRemainBonus = function (index) {
        $scope.cartInfo.campaignInfo = $scope.campaignInfo;
        cartService.verifyListBonusAndSpecial($scope.cartInfo).success(function(data){
            if(data.error!=''){
                showKhatocoPopup(data.error);
            }
            $scope.loadCartInfo(data.cart);

            var totalGet = 0;
            if ($scope.campaignInfo != undefined && $scope.campaignInfo.listBonus.listProdBonus != undefined && $scope.campaignInfo.listBonus.listProdBonus.length > 0) {
                for (var i = 0; i < $scope.campaignInfo.listBonus.listProdBonus.length; i++) {
                    totalGet += $scope.campaignInfo.listBonus.listProdBonus[i].quantity != undefined ? parseInt($scope.campaignInfo.listBonus.listProdBonus[i].quantity) : 0;
                }
                var beforeQuantity = parseInt($scope.campaignInfo.listBonus.listProdBonus[index].quantity);
                $scope.remainBonus = parseInt($scope.campaignInfo.listBonus.total) - parseInt(totalGet);
                if ($scope.remainBonus < 0) {
                    $scope.campaignInfo.listBonus.listProdBonus[index].quantity = 0;
                    totalGet -= beforeQuantity;
                }
                $scope.remainBonus = parseInt($scope.campaignInfo.listBonus.total) - parseInt(totalGet);
            }
        });
    }

    $scope.countRemainSpecial = function (index) {
        $scope.cartInfo.campaignInfo = $scope.campaignInfo;
        console.log(scope.cartInfo.campaignInfo);
        cartService.verifyListBonusAndSpecial($scope.cartInfo).success(function(data){
            if(data.error!=''){
                showKhatocoPopup(data.error);
            }
            $scope.loadCartInfo(data.cart);

            var totalGet = 0;
            if ($scope.campaignInfo != undefined && $scope.campaignInfo.listSpecialPrice.listProdBonus != undefined && $scope.campaignInfo.listSpecialPrice.listProdBonus.length > 0) {
                for (var i = 0; i < $scope.campaignInfo.listSpecialPrice.listProdBonus.length; i++) {
                    totalGet += $scope.campaignInfo.listSpecialPrice.listProdBonus[i].quantity != undefined ? parseInt($scope.campaignInfo.listSpecialPrice.listProdBonus[i].quantity) : 0;
                }
                var beforeQuantity = parseInt($scope.campaignInfo.listSpecialPrice.listProdBonus[index].quantity);
                $scope.remainSpecial = parseInt($scope.campaignInfo.listSpecialPrice.total) - parseInt(totalGet);
                if ($scope.remainSpecial < 0) {
                    $scope.campaignInfo.listSpecialPrice.listProdBonus[index].quantity = 0;
                    totalGet -= beforeQuantity;
                }
                $scope.remainSpecial = parseInt($scope.campaignInfo.listSpecialPrice.total) - parseInt(totalGet);

//                Fix total price
                var priceOfSpecial = 0;
                for (var i = 0; i < $scope.campaignInfo.listSpecialPrice.listProdBonus.length; i++) {
                    priceOfSpecial += $scope.campaignInfo.listSpecialPrice.listProdBonus[i].quantity != undefined ? parseInt($scope.campaignInfo.listSpecialPrice.listProdBonus[i].quantity) * parseInt($scope.campaignInfo.listSpecialPrice.listProdBonus[i].price) : 0;
                }
                $scope.campaignInfo.listSpecialPrice.totalPrice = priceOfSpecial;
                $scope.cartInfo.campaignInfo = $scope.campaignInfo; // Sync campagin info
                $scope.calculateLastTotal();
            }

        });

    }

    $scope.initQuantity = function(){
        if (isValid($scope.campaignInfo.listBonus) && isValid($scope.campaignInfo.listBonus.listProdBonus) ){
            for (var i=0; i < $scope.campaignInfo.listBonus.listProdBonus.length;i++ ){
                if(!isValid($scope.campaignInfo.listBonus.listProdBonus[i].quantity)){
                    $scope.campaignInfo.listBonus.listProdBonus[i].quantity = 0;
                }
            }
        }

        if (isValid($scope.campaignInfo.listSpecialPrice) && isValid($scope.campaignInfo.listSpecialPrice.listProdBonus) ){
            for (var i=0; i < $scope.campaignInfo.listSpecialPrice.listProdBonus.length;i++ ){
                if(!isValid($scope.campaignInfo.listSpecialPrice.listProdBonus[i].quantity)){
                    $scope.campaignInfo.listSpecialPrice.listProdBonus[i].quantity = 0;
                }
            }
        }
    }

    $scope.showDetailCampaign = function(){
        var message = '';
        for (var i =0; i< $scope.campaignInfo.detail.length; i++){
            if ($scope.campaignInfo.detail[i]['value'] > 0) {
                message += '<h3>'+$scope.campaignInfo.detail[i]['title'] + '</h3>';
                message += '<p><strong>Mô tả : </strong> '+$scope.campaignInfo.detail[i]['description'] + '</p>';
                message += '<p><strong>Giảm : </strong> '+$scope.campaignInfo.detail[i]['value'].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") + ' Đ</p>';
            }

        }
        showKhatocoPopup(message,'Chi tiết');
    }

    $scope.init();
}]);

app.filter('vndCurrency', function () {
    return function (amount) {
        if (amount != undefined)
            return amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") + ' Đ';
        else
            return 0 + ' Đ';
        ;
    }

});
app.filter('paymentLbl', function () {
    return function (type) {
        if (type != undefined)
            if (parseInt(type) == 1)
                return 'Thanh toán khi nhận hàng';
            if (parseInt(type) == 2)
                return 'Thanh toán ATM nội địa';
            if (parseInt(type) == 3)
                return 'Thanh toán Master/Visa card';
        else
            return 'Chưa xác định được loại thanh toán';
    }
});

function isValid(test){
    return !(test==null || test==undefined);
}
function getInfoFromObjectInArraySet(arraySource, arrayTarget){
    for (var i=0; i < arraySource.length; i++){
        var keys = Object.keys(arraySource[i]);
        for (var j = 0; j < keys.length; j++){
            var valSource = arraySource[i][keys[j]];
            var valTarget = arrayTarget[i][keys[j]];

            if (valSource != valTarget)
                arraySource[i][keys[j]] = arrayTarget[i][keys[j]];
        }
    }
}
