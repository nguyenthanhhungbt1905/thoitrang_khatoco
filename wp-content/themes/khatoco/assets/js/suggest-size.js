
function suggestSize(type, form, args) { 
    // Type : Loại áo/quần (sơ mi/thun , short / dài / 0 ly / 1 ly)
    // Form : Slimfit / Tailor / Regular ...
    // Args : Các thông số về chiều cao, cân nặng, vòng cổ ...
    
    if (typeof args.height === 'undefined' && typeof args.chestSize != 'undefined' && args.chestSize > 0){
        return getSuggestByChest(type, form, args);
    }
    else if (typeof args.bellySize != 'undefined' && typeof args.bellySize != null && args.bellySize > 0){
        return getSuggestByBelly(type, form, args);
    }
    else if (args.height > 0 && args.weight > 0) {
        return getSuggestByHW(type, form, args);
    }
    return null;
}
function getSuggestByHW(type, form, args){
    var checkHeight = false;
    var checkWeight = false;
    var listSize = [];
    for (var i=0; i < Object.keys(window[type].size).length; i++){
        var size = Object.keys(window[type].size)[i];
        var sizeObj = getSize(type, size, form);
        if (args.height <= sizeObj.height && !checkHeight){
            listSize.push(size);
            checkHeight = true;
        }
        if (args.weight <= sizeObj.weight && !checkWeight){
            listSize.push(size);
            checkWeight = true;
        }

        if (listSize.length == 2) break;
    }

    return listSize.length == 2 ? listSize[listSize.length-1] : null ;
}

function getSuggestByChest(type, form, args){
    var checkChest = false;
    var checkNeck = (typeof args.neckSize === 'undefined' || args.neckSize == null) ? true : false;
    var checkShoulder = ( typeof args.shoulderSize === 'undefined' || args.shoulderSize == null ) ? true : false;

    var listSize = [];
    for (var i=0; i < Object.keys(window[type].size).length; i++){
        var size = Object.keys(window[type].size)[i];
        var sizeObj = getSize(type, size, form);
        if (args.chestSize <= sizeObj.chestSize && !checkChest){
            listSize.push(size);
            checkChest = true;
        }

        if (args.neckSize <= sizeObj.neckSize && !checkNeck){
            listSize.push(size);
            checkNeck = true;
        }

        if (args.shoulderSize <= sizeObj.shoulderSize && !checkShoulder){
            listSize.push(size);
            checkShoulder = true;
        }

        if (listSize.length == 3) break;
    }

    if (typeof args.neckSize!= 'undefined' && args.neckSize != null && args.neckSize > 0 && !checkNeck){
        return null;
    }
    if (typeof args.shoulderSize!= 'undefined' && args.shoulderSize != null && args.shoulderSize > 0 && !checkShoulder){
        return null;
    }
    
    return listSize.length > 0 ? listSize[listSize.length-1] : null ;
}

function getSuggestByBelly(type, form, args){
    var checkBelly = false;
    var checkRear = (typeof args.rearSize === 'undefined' || args.rearSize == null) ? true : false;
    var checkThigh = ( typeof args.thighSize === 'undefined' || args.thighSize == null ) ? true : false;

    var listSize = [];
    for (var i=0; i < Object.keys(window[type].size).length; i++){
        var size = Object.keys(window[type].size)[i];
        var sizeObj = getSize(type, size, form);
        if (typeof sizeObj === 'undefined' || sizeObj == null) 
            return null;
        if (args.bellySize <= sizeObj.bellySize && !checkBelly){
            listSize.push(size);
            checkBelly = true;
        }

        if (args.rearSize <= sizeObj.rearSize && !checkRear){
            listSize.push(size);
            checkRear = true;
        }

        if (args.thighSize <= sizeObj.thighSize && !checkThigh){
            listSize.push(size);
            checkThigh = true;
        }

        if (listSize.length == 3) break;
    }

    if (typeof args.rearSize!= 'undefined' && args.rearSize != null && args.rearSize > 0 && !checkRear){
        return null;
    }
    if (typeof args.thighSize!= 'undefined' && args.thighSize != null && args.thighSize > 0 && !checkThigh){
        return null;
    }
    
    return listSize.length > 0 ? listSize[listSize.length-1] : null ;
}

function getSize(obj, size, form){
    var listForm = window[obj].size[size];
    if (obj === 'tShirt') return listForm[0];
    for (var i=0; i< listForm.length; i++){
        if (listForm[i].form === form){
            return listForm[i];
        }
    }
    return null;
}

var shirt = {
    size : {
        'S' :  [
            {
                'form' : 'regular',
                'height' : 165,
                'weight' : 67,
                'neckSize' : 39.5,
                'chestSize' : 109,
                'shoulderSize' : 46.5
            },
            {
                'form' : 'tailor',
                'height' : 165,
                'weight' : 67,
                'neckSize' : 39.5,
                'chestSize' : 105,
                'shoulderSize' : 44.5
                
            },
            {
                'form' : 'slimfit',
                'height' : 160,
                'weight' : 53,
                'neckSize' : 39.5,
                'chestSize' : 101,
                'shoulderSize' : 42
            }
        ],
        'M' :  [
            {
                'form' : 'regular',
                'height' : 172,
                'weight' : 70,
                'neckSize' : 40.5,
                'chestSize' : 113,
                'shoulderSize' : 47.5
            },
            {
                'form' : 'tailor',
                'height' : 172,
                'weight' : 70,
                'neckSize' : 40.5,
                'chestSize' : 109,
                'shoulderSize' : 45.5
            },
            {
                'form' : 'slimfit',
                'height' : 165,
                'weight' : 60,
                'neckSize' : 40.5,
                'chestSize' : 105,
                'shoulderSize' : 43.5
            }
        ],
        'L' :  [
            {
                'form' : 'regular',
                'height' : 175,
                'weight' : 75,
                'neckSize' : 41.5,
                'chestSize' : 117,
                'shoulderSize' : 49
            },
            {
                'form' : 'tailor',
                'height' : 175,
                'weight' : 75,
                'neckSize' : 41.5,
                'chestSize' : 113,
                'shoulderSize' : 46.5
            },
            {
                'form' : 'slimfit',
                'height' : 170,
                'weight' : 70,
                'neckSize' : 41.5,
                'chestSize' : 109,
                'shoulderSize' : 45
            }
        ],
        'XL' :  [
            {
                'form' : 'regular',
                'height' : 180,
                'weight' : 80,
                'neckSize' : 42.5,
                'chestSize' : 121,
                'shoulderSize' : 52
            },
            {
                'form' : 'tailor',
                'height' : 180,
                'weight' : 80,
                'neckSize' : 42.5,
                'chestSize' : 117,
                'shoulderSize' : 47.5
            },
            {
                'form' : 'slimfit',
                'height' : 180,
                'weight' : 75,
                'neckSize' : 42.5,
                'chestSize' : 113,
                'shoulderSize' : 46.5
            }
        ],
        'XXL' :  [
            {
                'form' : 'regular',
                'height' : 185,
                'weight' : 85,
                'neckSize' : 43.5,
                'chestSize' : 127,
                'shoulderSize' : 54
            },
            {
                'form' : 'tailor',
                'height' : 185,
                'weight' : 85,
                'neckSize' : 43.5,
                'chestSize' : 121,
                'shoulderSize' : 48.5
            },
            {
                'form' : 'slimfit',
                'height' : 185,
                'weight' : 80,
                'neckSize' : 43.5,
                'chestSize' : 119,
                'shoulderSize' : 48
            }
        ],
        '3XL' :  [
            {
                'form' : 'regular',
                'height' : 200,
                'weight' : 200,
                'neckSize' : 45.5,
                'chestSize' : 136,
                'shoulderSize' : 57.5
            },
            {
                'form' : 'tailor',
                'height' : 200,
                'weight' : 200,
                'neckSize' : 45.5,
                'chestSize' : 126,
                'shoulderSize' : 50.5
            },
            {
                'form' : 'slimfit',
                'height' : 200,
                'weight' : 200,
                'neckSize' : 45.5,
                'chestSize' : 126,
                'shoulderSize' : 50
            }
        ]
    }
}

var tShirt = {
    size : {
        'S' :  [
            {
                'form' : '',
                'height' : 160,
                'weight' : 53
            }
        ],
        'M' :  [
            {
                'form' : '',
                'height' : 165,
                'weight' : 60
            }
        ],
        'L' :  [
            {
                'form' : '',
                'height' : 170,
                'weight' : 70
            }
        ],
        'XL' :  [
            {
                'form' : '',
                'height' : 180,
                'weight' : 75
            }
        ],
        'XXL' :  [
            {
                'form' : '',
                'height' : 185,
                'weight' : 80
            }
        ],
        '3XL' :  [
            {
                'form' : '',
                'height' : 200,
                'weight' : 105
            }
        ]
    }
}

var pant0 = {
    size : {
        '28' :  [
            {
                'form' : 'regular',
                'bellySize' : 74,
                'rearSize' : 92,
                'thighSize' : 59
            }
        ],
        '29' :  [
            {
                'form' : 'regular',
                'bellySize' : 77,
                'rearSize' : 95,
                'thighSize' : 60.5
            }
        ],
        '30' :  [
            {
                'form' : 'regular',
                'bellySize' : 79,
                'rearSize' : 97,
                'thighSize' : 61.5
            }
        ],
        '31' :  [
            {
                'form' : 'regular',
                'bellySize' : 82,
                'rearSize' : 99.5,
                'thighSize' : 62.5
            }
        ],
        '32' :  [
            {
                'form' : 'regular',
                'bellySize' : 84,
                'rearSize' : 102,
                'thighSize' : 63.5
            }
        ],
        '33' :  [
            {
                'form' : 'regular',
                'bellySize' : 86,
                'rearSize' : 104,
                'thighSize' : 64.5
            }
        ],
        '34' :  [
            {
                'form' : 'regular',
                'bellySize' : 89,
                'rearSize' : 107,
                'thighSize' : 65.5
            }
        ],
        '35' :  [
            {
                'form' : 'regular',
                'bellySize' : 92,
                'rearSize' : 109,
                'thighSize' : 67
            }
        ],
        '36' :  [
            {
                'form' : 'regular',
                'bellySize' : 94,
                'rearSize' : 112,
                'thighSize' : 68
            }
        ],
        '37' :  [
            {
                'form' : 'regular',
                'bellySize' : 96,
                'rearSize' : 114,
                'thighSize' : 69
            }
        ],
        '38' :  [
            {
                'form' : 'regular',
                'bellySize' : 200,
                'rearSize' : 200,
                'thighSize' : 200
            }
        ]
    }
}

var pant1 = {
    size : {
        '28' :  [
            {
                'form' : 'regular',
                'bellySize' : 74,
                'rearSize' : 92,
                'thighSize' : 59
            }
        ],
        '29' :  [
            {
                'form' : 'regular',
                'bellySize' : 77,
                'rearSize' : 95,
                'thighSize' : 60.5
            }
        ],
        '30' :  [
            {
                'form' : 'regular',
                'bellySize' : 79,
                'rearSize' : 97,
                'thighSize' : 61.5
            }
        ],
        '31' :  [
            {
                'form' : 'regular',
                'bellySize' : 82,
                'rearSize' : 99.5,
                'thighSize' : 62.5
            }
        ],
        '32' :  [
            {
                'form' : 'regular',
                'bellySize' : 84,
                'rearSize' : 102,
                'thighSize' : 63.5
            }
        ],
        '33' :  [
            {
                'form' : 'regular',
                'bellySize' : 86,
                'rearSize' : 104,
                'thighSize' : 64.5
            }
        ],
        '34' :  [
            {
                'form' : 'regular',
                'bellySize' : 89,
                'rearSize' : 107,
                'thighSize' : 65.5
            }
        ],
        '35' :  [
            {
                'form' : 'regular',
                'bellySize' : 92,
                'rearSize' : 109,
                'thighSize' : 67
            }
        ],
        '36' :  [
            {
                'form' : 'regular',
                'bellySize' : 94,
                'rearSize' : 112,
                'thighSize' : 68
            }
        ],
        '37' :  [
            {
                'form' : 'regular',
                'bellySize' : 96,
                'rearSize' : 114,
                'thighSize' : 69
            }
        ],
        '38' :  [
            {
                'form' : 'regular',
                'bellySize' : 200,
                'rearSize' : 200,
                'thighSize' : 200
            }
        ]
    }
}

var pantKaki0 = {
    size : {
        '28' :  [
            {
                'form' : 'regular',
                'bellySize' : 71.5,
                'rearSize' : 95,
                'thighSize' : 58.5
            }
        ],
        '29' :  [
            {
                'form' : 'regular',
                'bellySize' : 75,
                'rearSize' : 97,
                'thighSize' : 59.5
            }
        ],
        '30' :  [
            {
                'form' : 'regular',
                'bellySize' : 77.5,
                'rearSize' : 99,
                'thighSize' : 60.5
            }
        ],
        '31' :  [
            {
                'form' : 'regular',
                'bellySize' : 80,
                'rearSize' : 101,
                'thighSize' : 61.5
            }
        ],
        '32' :  [
            {
                'form' : 'regular',
                'bellySize' : 82.5,
                'rearSize' : 103,
                'thighSize' : 62.5
            }
        ],
        '33' :  [
            {
                'form' : 'regular',
                'bellySize' : 85,
                'rearSize' : 105,
                'thighSize' : 63.5
            }
        ],
        '34' :  [
            {
                'form' : 'regular',
                'bellySize' : 87.5,
                'rearSize' : 107,
                'thighSize' : 64.5
            }
        ],
        '35' :  [
            {
                'form' : 'regular',
                'bellySize' : 90.5,
                'rearSize' : 109,
                'thighSize' : 65.5
            }
        ],
        '36' :  [
            {
                'form' : 'regular',
                'bellySize' : 92.5,
                'rearSize' : 111,
                'thighSize' : 66.5
            }
        ],
        '37' :  [
            {
                'form' : 'regular',
                'bellySize' : 95,
                'rearSize' : 113,
                'thighSize' : 67.5
            }
        ],
        '38' :  [
            {
                'form' : 'regular',
                'bellySize' : 200,
                'rearSize' : 200,
                'thighSize' : 200
            }
        ]
    }
}

var pantKaki1 = {
    size : {
        '28' :  [
            {
                'form' : 'regular',
                'bellySize' : 71.5,
                'rearSize' : 95,
                'thighSize' : 60.5
            }
        ],
        '29' :  [
            {
                'form' : 'regular',
                'bellySize' : 75,
                'rearSize' : 97,
                'thighSize' : 61.5
            }
        ],
        '30' :  [
            {
                'form' : 'regular',
                'bellySize' : 77.5,
                'rearSize' : 99,
                'thighSize' : 62.5
            }
        ],
        '31' :  [
            {
                'form' : 'regular',
                'bellySize' : 80,
                'rearSize' : 101,
                'thighSize' : 63.5
            }
        ],
        '32' :  [
            {
                'form' : 'regular',
                'bellySize' : 82.5,
                'rearSize' : 103,
                'thighSize' : 64.5
            }
        ],
        '33' :  [
            {
                'form' : 'regular',
                'bellySize' : 85,
                'rearSize' : 105,
                'thighSize' : 65.5
            }
        ],
        '34' :  [
            {
                'form' : 'regular',
                'bellySize' : 87.5,
                'rearSize' : 107,
                'thighSize' : 66.5
            }
        ],
        '35' :  [
            {
                'form' : 'regular',
                'bellySize' : 90.5,
                'rearSize' : 109,
                'thighSize' : 67.5
            }
        ],
        '36' :  [
            {
                'form' : 'regular',
                'bellySize' : 92.5,
                'rearSize' : 111,
                'thighSize' : 68.5
            }
        ],
        '37' :  [
            {
                'form' : 'regular',
                'bellySize' : 95,
                'rearSize' : 113,
                'thighSize' : 69.5
            }
        ],
        '38' :  [
            {
                'form' : 'regular',
                'bellySize' : 200,
                'rearSize' : 200,
                'thighSize' : 200
            }
        ]
    }
}

var pantKaki = {
    size : {
        '28' :  [
            {
                'form' : 'slimfit',
                'bellySize' : 71.5,
                'rearSize' : 95,
                'thighSize' : 58.5
            }
        ],
        '29' :  [
            {
                'form' : 'slimfit',
                'bellySize' : 75,
                'rearSize' : 97,
                'thighSize' : 59.5
            }
        ],
        '30' :  [
            {
                'form' : 'slimfit',
                'bellySize' : 77.5,
                'rearSize' : 99,
                'thighSize' : 60.5
            }
        ],
        '31' :  [
            {
                'form' : 'slimfit',
                'bellySize' : 80,
                'rearSize' : 101,
                'thighSize' : 61.5
            }
        ],
        '32' :  [
            {
                'form' : 'slimfit',
                'bellySize' : 82.5,
                'rearSize' : 103,
                'thighSize' : 62.5
            }
        ],
        '33' :  [
            {
                'form' : 'slimfit',
                'bellySize' : 85,
                'rearSize' : 105,
                'thighSize' : 63.5
            }
        ],
        '34' :  [
            {
                'form' : 'slimfit',
                'bellySize' : 87.5,
                'rearSize' : 107,
                'thighSize' : 64.5
            }
        ],
        '35' :  [
            {
                'form' : 'slimfit',
                'bellySize' : 90.5,
                'rearSize' : 109,
                'thighSize' : 65.5
            }
        ],
        '36' :  [
            {
                'form' : 'slimfit',
                'bellySize' : 92.5,
                'rearSize' : 111,
                'thighSize' : 66.5
            }
        ],
        '37' :  [
            {
                'form' : 'slimfit',
                'bellySize' : 95,
                'rearSize' : 113,
                'thighSize' : 67.5
            }
        ],
        '38' :  [
            {
                'form' : 'slimfit',
                'bellySize' : 200,
                'rearSize' : 200,
                'thighSize' : 200
            }
        ]
    }
}

var pantShort = {
    size : {
        '28' :  [
            {
                'form' : 'regular',
                'bellySize' : 73,
                'rearSize' : 97.5,
                'thighSize' : 58.5
            },
            {
                'form' : 'slimfit',
                'bellySize' : 73,
                'rearSize' : 97.5,
                'thighSize' : 58.5
            }
        ],
        '29' :  [
            {
                'form' : 'regular',
                'bellySize' : 75,
                'rearSize' : 99.5,
                'thighSize' : 59.5
            },
            {
                'form' : 'slimfit',
                'bellySize' : 75,
                'rearSize' : 99.5,
                'thighSize' : 59.5
            }
        ],
        '30' :  [
            {
                'form' : 'regular',
                'bellySize' : 78,
                'rearSize' : 101.5,
                'thighSize' : 60.5
            },
            {
                'form' : 'slimfit',
                'bellySize' : 78,
                'rearSize' : 101.5,
                'thighSize' : 60.5
            }
        ],
        '31' :  [
            {
                'form' : 'regular',
                'bellySize' : 80,
                'rearSize' : 102.5,
                'thighSize' : 61.5
            },
            {
                'form' : 'slimfit',
                'bellySize' : 80,
                'rearSize' : 102.5,
                'thighSize' : 61.5
            }
        ],
        '32' :  [
            {
                'form' : 'regular',
                'bellySize' : 82.5,
                'rearSize' : 103.5,
                'thighSize' : 62.5
            },
            {
                'form' : 'slimfit',
                'bellySize' : 82.5,
                'rearSize' : 103.5,
                'thighSize' : 62.5
            }
        ],
        '33' :  [
            {
                'form' : 'regular',
                'bellySize' : 85.5,
                'rearSize' : 105,
                'thighSize' : 63.5
            },
            {
                'form' : 'slimfit',
                'bellySize' : 85.5,
                'rearSize' : 105,
                'thighSize' : 63.5
            }
        ],
        '34' :  [
            {
                'form' : 'regular',
                'bellySize' : 88,
                'rearSize' : 106.5,
                'thighSize' : 64.5
            },
            {
                'form' : 'slimfit',
                'bellySize' : 88,
                'rearSize' : 106.5,
                'thighSize' : 64.5
            }
        ],
        '35' :  [
            {
                'form' : 'regular',
                'bellySize' : 90,
                'rearSize' : 108.5,
                'thighSize' : 65.5
            },
            {
                'form' : 'slimfit',
                'bellySize' : 90,
                'rearSize' : 108.5,
                'thighSize' : 65.5
            }
        ],
        '36' :  [
            {
                'form' : 'regular',
                'bellySize' : 93,
                'rearSize' : 110,
                'thighSize' : 66.5
            },
            {
                'form' : 'slimfit',
                'bellySize' : 93,
                'rearSize' : 110,
                'thighSize' : 66.5
            }
        ],
        '37' :  [
            {
                'form' : 'regular',
                'bellySize' : 95.5,
                'rearSize' : 111.5,
                'thighSize' : 67.5
            },
            {
                'form' : 'slimfit',
                'bellySize' : 95.5,
                'rearSize' : 111.5,
                'thighSize' : 67.5
            }
        ],
        '38' :  [
            {
                'form' : 'regular',
                'bellySize' : 200,
                'rearSize' : 200,
                'thighSize' : 200
            },
            {
                'form' : 'slimfit',
                'bellySize' : 200,
                'rearSize' : 200,
                'thighSize' : 200
            }
        ]
    }
}