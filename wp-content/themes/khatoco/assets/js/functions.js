(function($)
{
    $.fn.aj_remove_style = function(style)
    {
        var search = new RegExp(style + '[^;]+;?', 'g');

        return this.each(function()
        {   
            if(jQuery(this).attr('style')){
                jQuery(this).attr('style', function(i, style)
                {
                    return style.replace(search, '');
                });
            }
        });
    };
}(jQuery));