var showKhatocoPopup; // this is function show popup default of khatoco website
var ajaxVNE, responseAjaxResult ;

jQuery(document).ready(function($) {
	$('.button-verify').on('click', function(event) {
		event.preventDefault();
		$('.sale-off-panel > .messageEgift').empty();
	});


	
	

	

	// $('#button-verify').on('click', function(event) {
	// 	event.preventDefault();
	// 	var code = jQuery('.text-verify').val().toUpperCase();
	// 	var subCode = jQuery('.text-verify').val().toUpperCase().substring(0,6);
	// 	if(subCode == 'KTCVNE'){
			
	// 	}
	// });
	
	
	//SLIDER
	jQuery('.home-slide').flexslider({
		animation: 'slide',
		controlNav: true,
		directionNav: true,
		prevText: '',
		nextText: '',
		animationLoop: true,
		slideshowSpeed: 7000
	});
	
	jQuery('.top-tip-slide').owlCarousel({
        autoPlay: true,
        singleItem:true,
        navigation : true,
        navigationText : ["<i class='fa fa-chevron-left'></i>",
                          "<i class='fa fa-chevron-right'></i>"],
        pagination : false,
    });
	
    jQuery('.product-cat-slide').owlCarousel({
        singleItem:true,
        navigation : true,
        navigationText : ["<i class='fa fa-chevron-left'></i>",
                          "<i class='fa fa-chevron-right'></i>"],
        pagination : false,
    });
    

	jQuery('.uni-partners-holder').flexslider({
		animation: 'slide',
		controlNav: false,
		directionNav: true,
		animationLoop: false,
		prevText: '',
		nextText: '',
		itemWidth: 170,
		// itemMargin: 40,
		minItems: 6,
		maxItems: 6,
		pauseOnHover: true,
		slideshowSpeed: 4000
	});

	jQuery('.tax-slide').flexslider({
		animation: 'slide',
		controlNav: true,
		directionNav: true,
		prevText: '',
		nextText: '',
		animationLoop: true,
		slideshowSpeed: 7000
	});
	/* POPUP */
	jQuery('.ms-popup-dialog .popup-header .close').on('click',function(){
		jQuery('.ms-popup').fadeOut();
	});
	jQuery('.ms-popup-dialog .popup-ok').on('click',function(){
		jQuery('.ms-popup').fadeOut();
	});

	showKhatocoPopup = function (content, title) {

		if (title == null)
			title = 'Khatoco thông báo';
		jQuery('.ms-popup-dialog .popup-header .title').html(title);
		jQuery('.ms-popup-dialog .popup-content').html(content);
		jQuery('.ms-popup').show();
	}

	WINDOW_W = jQuery('body').width();
	WINDOW_H = jQuery('body').height();
	jQuery(window).resize(function(event) {
		WINDOW_W = jQuery('body').width();
		WINDOW_H = jQuery('body').height();
	});

	if(WINDOW_W > 1096) {
		
	}
	
	if(WINDOW_W >= 768) {
	    $(".home-news-slide").owlCarousel({
	        items : 3,
	        navigation : true,
	        navigationText : ["", ""],
	        pagination : false,
	    });
	    $(".home-cat-slide").owlCarousel({
	        autoPlay: true,
	        items : 4,
	        itemsDesktop : [1199,4],
	        itemsDesktopSmall : [979,3],
	        itemsTablet:false,
	        navigation : false,
	        navigationText : ["", ""],
	        pagination : false,
	    });
	}
	
	if(WINDOW_W <= 1096) {
		jQuery('body').click(function() {
			if(jQuery('body').find('.header-top-menu.show').length > 0)
				jQuery('.header-top-menu').fadeOut().removeClass('show');
		});
		jQuery('#hambuger-top-menu').click(function(event) {
			if(jQuery('body').find('.side-menu-mobile.show').length <= 0){
				jQuery('.side-menu-mobile').addClass('show').show();
				jQuery('.side-menu-mobile ul').animate({left: 0}, 500).addClass('show');
				jQuery('body').css('overflow', 'hidden');

				jQuery('#mobile_menu_close').click(function(event) {
					jQuery('.side-menu-mobile ul').animate({left: '-250px'}, 500);
					jQuery('.side-menu-mobile').removeClass('show').fadeOut();
					jQuery('body').css('overflow', 'auto');
				});
			}
			/*else{
				jQuery('.side-menu-mobile ul').animate({left: '-100%'}, 500);
				jQuery('.side-menu-mobile').removeClass('show').hide();
			}*/
			return false;
		});
	}
	
	if(WINDOW_W >= 550) {
		$(".product-home-slide").owlCarousel({
	        items : 4,
	        itemsDesktop : [1199,4],
	        itemsDesktopSmall : [979,3],
	        itemsTablet:[768,3],
	        navigation : false,
	        navigationText : ["", ""],
	        pagination : false,
	    });
	}

	if(WINDOW_W >= 768) {
	    var topHeaderOffset = getOffset("div.top-header"),
		subPageMenuOffset = getOffset("div.menu-subpage-outer");
		footer = getOffset("div.top-footer");
	    
	    $(window).resize(function () {
	    	topHeaderOffset = getOffset("div.top-header");
	    	subPageMenuOffset = getOffset("div.menu-subpage-outer");
	    	footer = getOffset("div.top-footer");
	    });
	
	    $(window).scroll(function () {
	        var windowTop = $(window).scrollTop(),
	        	windowBottom = windowTop + $(window).height();
	        /*header anchor */
	        if (windowTop > topHeaderOffset) {
	            $("div.page-tools").show();
	        } else {
	            $("div.page-tools").hide();
	        }
	
	        if (windowTop > subPageMenuOffset) {
	
	            if(windowBottom > footer){
	            	$("ul.menu-subpage").css({             
	            		position: "static",
	            		top: subPageMenuOffset,
	                	width: $("ul.menu-subpage").css("width")
	                });
	            }
	            else {
	            	$("ul.menu-subpage").css({             
	                    position: "fixed",
	                    top: "0",
	                	width: $("ul.menu-subpage").css("width")
	                });
	            }
	        }
	        else {
	        	$("ul.menu-subpage").removeAttr("style");
	        }
	        
	
	    });
	    
		$('#order-check-cta').click(function(event) {
			event.preventDefault();
			$('#order-check').removeClass('hidden');
		});
/*		$('#order-check-cta-anchor').click(function(event) {
			event.preventDefault();
			$('#order-check').removeClass('hidden');
		});*/
		
	}
	
	if(WINDOW_W <= 480) {
		/*jQuery('.header-top-cart > a').click(function(event) {
			event.preventDefault();
			document.location.href = jQuery(this).attr('data-redirect');
		});*/
	}

	/*Header*/
	
	$('#viewTerms').click(function(event) {
		event.preventDefault();
		$('#popupTerms').removeClass('hidden');
	});
	$('#user_login_btn').click(function(event) {
		event.preventDefault();
		$('#login-modal').removeClass('hidden');
	});
	
    function getOffset(item, option) {
    	if(option == undefined) {
    		option = 'top'
    	}
        var allOffsets = $(item).offset();
        if(typeof(allOffsets) != 'undefined'){
	        if(option=='top'){
	        	return allOffsets.top;
	    	} else {
	    		return allOffsets.top + $(item).height();
	    	}
        }
    }
    

	
	/* Cookie */
    jQuery('#popup-link .close-bg, #popup-link a').click(function(event) {
        set_cookie('pr_ed', 'true', 1);
    });

    // Orders form
    jQuery('.close').click(function(event){
        event.preventDefault();
        jQuery(this).parents('.close-wrap').addClass('hidden');
    });
    jQuery('#link.button').click(function(event){
        event.preventDefault();
        jQuery(this).parents('.close-wrap').addClass('hidden');
    });
    jQuery('.close-bg').click(function(event){
        event.preventDefault();
        jQuery(this).parents('.close-wrap').addClass('hidden');
    });

	//jQuery('.side-menu-mobile').height(jQuery(window).height());
	// jQuery('.side-menu-mobile ul').height(jQuery(window).height()).css('max-height', jQuery(window).height());

	if(WINDOW_W > 770){
		jQuery('#hc-sticky').hcSticky({
			top: 10,
	        followScroll: false
	    });
	}

	/* First load top cart */
	load_top_cart();

	/* User account page */
	// if( jQuery('body').hasClass('page-id-169') || jQuery('body').hasClass('page-id-171') ) {
	/* Account forms submit */
	jQuery('.account-form').bind('submit', function(event) {
		var fromThis = jQuery(this);
		var formStatus = jQuery(fromThis).find('.form-status');
		var formButton = jQuery(fromThis).find('button');

		var formButtonText = jQuery(formButton).text();
		jQuery(formButton).text('Đang xử lý');

		jQuery.ajax({
			url: wp_vars.ajaxurl,
			type: 'POST',
			dataType: 'json',
			data: jQuery(fromThis).serialize(),
			success: function(response) {
				jQuery(formStatus).html( response.message );
				jQuery(formButton).text(formButtonText);
				if( response.redirect ) {
					document.location.href = response.redirect;
				}
			}
		});

		return false;
	});
	// }

// Message top
	jQuery('#message-payment .close').click(function(event) {
		jQuery('#message-payment').slideUp();
		return false;
	});

// USER TOP MENU
	var user_top_menu_width_right = jQuery('#user-info-page').outerWidth() - 1;
	var user_top_menu_width_left = 268 - user_top_menu_width_right - 2;
	jQuery('#user-info-page .top-left').width(user_top_menu_width_left);
	jQuery('#user-info-page .top-right').width(user_top_menu_width_right);

	/*jQuery('body').not().click(function() {
		if(jQuery('body').find('.header-top-user-login.user-login.hover').length > 0)
			jQuery('.header-top-user-login.user-login').removeClass('hover');
	});*/

	jQuery('.header-top-user-login.user-login > a').click(function(e) {
		e.preventDefault();
		if(jQuery(this).parent().hasClass('hover')) {
			jQuery('.header-top-user-login.user-login').removeClass('hover');
		}
		else {
			jQuery('.header-top-user-login.user-login').addClass('hover');
		}
		return false;
	});

// LEFT WIDGET MENU PRODUCT ARCHIVE TEMPLATE
	if(jQuery('body').find('.tax-left-menu').length > 0) {
		var li_level = jQuery('.tax-left-menu li');
        li_level.each(function() {
        	var is= jQuery(this);
            if (is.children('.sub-menu').length>0){
                is.children('a').prepend('<span class="expand">+</span>');
                var expand = is.children('a').children('.expand');
                expand.on('click',function(e){
                	var sub =jQuery(this).parent().next();
                    var me =jQuery(this);

					if (sub.css('display') == 'block'){
                        me.text('+');
                        sub.hide();
					}else {
                        me.text('-');
                        sub.show();
					}

					e.preventDefault();
				});
            }
        });

        jQuery('.tax-left-menu > li >a > .expand').trigger('click');

	}


// LEFT WIDGET MENU JOB PAGE
	if(jQuery('body').find('.block-applyjob').length > 0) {
		jQuery('.left-widget .menu-item').each(function() {
			var t = jQuery(this);
			if(jQuery(this).find('.sub-menu').length > 0) {
				if(jQuery(this).hasClass('current-menu-item') || jQuery(this).hasClass('current-menu-parent')) {
					return;
				}
				else {
					jQuery(this).find('.sub-menu').hide().addClass('closed');
				}
			}
		});
		jQuery('.left-widget > li > a').click(function(e) {
			if(jQuery(this).parent('.menu-item').find('.sub-menu').length > 0) {
				e.preventDefault();
				jQuery('.left-widget .sub-menu').each(function() {
					if(!jQuery(this).hasClass('closed'))
						jQuery(this).slideUp().addClass('closed');
				});
				jQuery(this).siblings('.sub-menu').slideDown().removeClass('closed');
			}
			// return false;
		});
	}

// Top search live
	function live_search(){
		var s = jQuery('#header-top-search-text').val();
		if(s != ''){
			jQuery.ajax({
				url: wp_vars.ajaxurl,
				type: 'POST',
				dataType: 'json',
				cache: false,
				data: {
					action: 'top_live_search',
					key: s
				}
			})
			.done(function(response) {
				jQuery('#header-top-search-results ul').html(response.html);
				jQuery('#header-top-search-results').fadeIn();
			});
		}
		else
			return false;
	}
	jQuery('#header-top-search-text').live('keyup', function() {
		clearTimeout(jQuery.data(this, 'timer'));

		var key = jQuery(this).val();
		if(key != ''){
			jQuery('#header-top-search-cancel').show();
			jQuery('#header-top-search-results ul').html('<li><p style="text-align: center;"><i class="fa fa-spinner fa-spin fa-2x"></i></p></li>');
			jQuery('#header-top-search-results').fadeIn();
			jQuery(this).data('timer', setTimeout(live_search, 100));
		}
		else{
			jQuery('#header-top-search-cancel').hide();
			jQuery('#header-top-search-results ul').html('');
			jQuery('#header-top-search-results').fadeOut();
		}
	});
	jQuery('#header-top-search-cancel').click(function(event){
		event.preventDefault();
		jQuery('#header-top-search-text').val('');
		jQuery('#header-top-search-results ul').html('');
		jQuery('#header-top-search-results').fadeOut();
		jQuery(this).hide();
	});

// Masonry
	function mansonry_collection() {
		if(WINDOW_W > 480) {
			var $masonry_con = jQuery('#collection_masonry');
			var $masonry = $masonry_con.find('.masonry-holder');
			$masonry_con.imagesLoaded(function(){
				$masonry.each(function() {
					jQuery(this).masonry({
						itemSelector: '.mansonry-item',
						columnWidth: '.mansonry-item'
					});
				});
			});
		}
	}

	mansonry_collection();

	$( '#subscriber_form input' ).focus(function(event) {
        $(this).siblings('.message').fadeOut();
    });

    $('.message-one-click').click(function(event) {
        event.preventDefault();
        $(this).fadeOut();
    });

    $( '#subscriber_form' ).submit(function(event) {
        event.preventDefault();

        var t = $(this);
        var data = t.serializeArray();
        var email_check = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

        if( data[0].value != '' && email_check.test( data[0].value ) ){
            $.ajax({
                url: wp_vars.ajaxurl,
                type: 'POST',
                dataType: 'json',
                data: {
                    action: 'frontend__user_subscribe',
                    email: data[0].value,
                    //phone: data[0].value,
                }
            })
            .done(function( response ) {
                // t.find('.message').text( response.message + response.obj ).fadeIn();
                $('#message_center .inner').html( '<p>'+ response.message +'</p>' );
				$('#message_center').animate({'right': 0}, 600);
            });
        }
        else{
            // t.find('.message').text('').fadeIn();
            $('#message_center .inner').html('<p>Bạn chưa nhập email hoặc địa chỉ email không đúng!</p>');
			$('#message_center').animate({'right': 0}, 600);
        }
    });

// SCROLLTOP
	jQuery(window).scroll(function(event) {
		var top = jQuery(this).scrollTop();
		if(jQuery('body').find('.scrolltop').length > 0) {
			if(top > 100) jQuery('.scrolltop').css('display', 'block');
			else jQuery('.scrolltop').css('display', 'none');
		}
	});
	jQuery('.scrolltop').click(function(event) {
		jQuery('html, body').animate({'scrollTop': 0}, 500);
	});

// SELECTBOX DROPKICK
	jQuery('.dropkick_target').dropkick({
		mobile: true,
		change: function () {
			value = this.value;
			var dropkick_id = $("li[data-value='" + value + "']").parents('div.dropkick_target').attr('id');
			var select_id = dropkick_id.substr(dropkick_id.indexOf('-')+1);
			// jQuery('#'+ select_id +'>option:selected').
			jQuery('#'+ select_id +'>option[value="'+ value +'"]').prop('selected', true);
			jQuery('#'+ select_id).trigger('change');
		}
	});

// CHANGE STYLE OF PRODUCTS LIST
	jQuery('.sort').click(function(event) {
		var date = new Date();
        date.setTime(date.getTime() + (60*60*1000));
        var expires = "; expires=" + date.toGMTString() + '; path=/';

		if(jQuery(this).hasClass('sort-grid')) {
			jQuery(this).addClass('active').siblings('.sort').removeClass('active');
			jQuery('ul.lists.list-style').addClass('hidden');
			jQuery('ul.lists.grid-style').removeClass('hidden');
			document.cookie = "view-style=grid" + expires;
		}
		else {
			jQuery(this).addClass('active').siblings('.sort').removeClass('active');
			jQuery('ul.lists.grid-style').addClass('hidden');
			jQuery('ul.lists.list-style').removeClass('hidden');
			document.cookie = "view-style=list" + expires;
		}
		console.log(document.cookie);
	});

// CHANGE COLOR - ARCHIVE TEMPLATE
	jQuery('.single-item-product').each(function() {
		var $this = jQuery(this);
		jQuery(this).find('.item-thumb').not(':first').hide();
		jQuery(this).find('.item-color .color:first').addClass('checked');

		jQuery(this).find('.item-color .color').click(function() {
			var code = jQuery(this).data('color');
			$this.find('.item-thumb').hide();
			$this.find('.item-thumb[data-color="'+ code +'"]').show();
		});
	});

// INCREASE/DECREASE AMOUNT
	jQuery('.increase').click(function(){
		jQuery('#item-amount').val(parseInt(jQuery('#item-amount').val()) + 1);
	});
	jQuery('.decrease').click(function(){
		var amount = parseInt(jQuery('#item-amount').val()) -1;
		if (amount <= 0)
			amount = 1;
		jQuery('#item-amount').val(amount);
	});

	jQuery('#item-size').bind('change', function() {
		jQuery('#item-amount').val(1).siblings('span.error').text('');
		jQuery('#add_to_cart').attr('data-size', jQuery(this).val());
	});

	jQuery('#item-color .color').on('click', function() {
		var c = jQuery(this).data('color');
		console.log(c);
		jQuery('#item-color .color').removeClass('active');
		jQuery(this).addClass('active');
		// Change preview
		jQuery('.block-product-preview').each(function(index, el) {
			if(jQuery(this).attr('data-color') == c) {
				jQuery(this).fadeIn();
			}
			else {
				jQuery(this).fadeOut();
			}
			
		});
		jQuery('.thumbnail-preview').each(function() {
			jQuery(this).find('li').not(':first').removeClass('active');
			jQuery(this).find('li:first').addClass('active');
		});
		jQuery('.preview-image').each(function() {
			jQuery(this).find('li').not(':first').hide();
			jQuery(this).find('li:first').show();
		});

		// Update ADD_TO_CART info
		jQuery('#add_to_cart').attr('data-color-number', c);
	});

	// Info size box

	jQuery('body').click(function() {
		if(jQuery('body').find('.info_size_box.show').length > 0)
			jQuery('.info_size_box').fadeOut().removeClass('show');
	});
	jQuery('.info_size a').click(function() {
		if(!jQuery('.info_size_box').hasClass('show'))
			jQuery('.info_size_box').fadeIn().addClass('show');
		else
			jQuery('.info_size_box').fadeOut().removeClass('show');
		return false;
	});
	

// TABBED
	jQuery('.tabbed').each(function(){
		var max_height_tab_content = 0;
		jQuery(this).find('.tab-content').each(function() {
			if(jQuery(this).height() > max_height_tab_content)
				max_height_tab_content = jQuery(this).height();
		});
		jQuery(this).find('.tab-content').height(max_height_tab_content);

		jQuery(this).find('.tab-title li:first').addClass('current');
		jQuery(this).find('.tab-content').not(':first').hide();

		jQuery('.tab-title a').click(function(event) {
			event.preventDefault();
			jQuery(this).parents('li').addClass('current');
			jQuery(this).parents('li').siblings().removeClass('current');

			var tab = jQuery(this).attr('href');
			jQuery(this).parentsUntil('.tabbed').siblings('.tab-content').hide();
			jQuery(tab).show();
		});
	});

	jQuery('.tabbed-2').each(function(){
		jQuery(this).find('.tab-title a:first').addClass('active');
		jQuery(this).find('.tab-content').not(':first').hide();

		jQuery('.tab-title a').click(function(event) {
			event.preventDefault();

			jQuery(this).addClass('active');
			jQuery(this).siblings().removeClass('active');

			var tab = jQuery(this).attr('href');
			jQuery(this).parentsUntil('.tabbed').siblings('.tab-content').hide();
			jQuery(tab).show();
			jQuery(tab).find('.masonry-holder').masonry({
				itemSelector: '.mansonry-item',
				columnWidth: '.mansonry-item'
			});
		});
	});


	// Fancybox
	jQuery('.fancybox').fancybox();

	jQuery('.button-download-catalogue').click(function(event) {
		event.preventDefault();
		jQuery('.fancybox:first').click();
		return false;
	});

	jQuery('.video-click').click(function(event) {
		event.preventDefault();
		var vsrc = jQuery(this).find('.video-unclick').attr('data-src');
		if(jQuery(window).width() > 770){
			jQuery('#video-popup .video-popup-inner iframe').attr('src', 'http://www.youtube.com/embed/'+ vsrc +'?autoplay=1');
			jQuery('#video-popup').removeClass('hidden');
		}
		else {
			window.open('http://www.youtube.com/embed/'+ vsrc +'?autoplay=1');
			return false;
		}
	});

	jQuery('.yt-link a').click(function(event) {
		event.preventDefault();
		var vsrc = jQuery(this).attr('data-src');
		if(jQuery(window).width() > 770){
			jQuery('#video-popup .video-popup-inner iframe').attr('src', 'http://www.youtube.com/embed/'+ vsrc +'?autoplay=1');
			jQuery('#video-popup').removeClass('hidden');
		}
		else {
			window.open('http://www.youtube.com/embed/'+ vsrc +'?autoplay=1');
			return false;
		}
	});

	jQuery('#video-popup .close').click(function(event) {
		jQuery('#video-popup').addClass('hidden');
		jQuery('#video-popup .video-popup-inner iframe').attr('src', '');
		return false;
	});

// PREVIEW IMAGES SINGLE PRODUCTS
	if(jQuery('body').find('.product-preview').length > 0) {
		var max_height_preview = 0;
		jQuery('.preview-image li').each(function(index, el) {
			if(jQuery(this).children('img').height() > max_height_preview)
				max_height_preview = jQuery(this).children('img').height();
		});
		jQuery('.preview-image').height(max_height_preview);

		jQuery('.block-product-preview').hide();
		jQuery('.block-product-preview:first').show();
		
		jQuery('.thumbnail-preview').each(function() {
			jQuery(this).find('li:first').addClass('active');
		});
		jQuery('.preview-image').each(function() {
			jQuery(this).find('li').not(':first').hide();
		});

		function click_thumbnail(cur) {
			var cur_index = jQuery('.thumbnail-preview li').index(cur);

			jQuery('.thumbnail-preview li').removeClass('active');
			jQuery(cur).addClass('active');

			jQuery('.preview-image li').not(this).fadeOut('600');
			jQuery('.preview-image li:eq(' + cur_index + ')').fadeIn('600');
			return false;
		}

		jQuery('.thumbnail-preview li').click(function(event) {
			click_thumbnail(this);
		});
	}

// CHOICE COLOR
	jQuery('.item-color span.color').click(function(event) {
		jQuery(this).addClass('checked');
		jQuery(this).parents('.item-color').find('span.color').not(this).removeClass('checked');

	});

// NOTIFICATIONS
	var window_height = jQuery(window).height();
	jQuery('#popup').height(window_height);
	jQuery(window).resize(function() {
		var window_height = jQuery(window).height();
		jQuery('#popup').height(window_height);
	});

	jQuery(document).keyup(function(e) {
		if(e.keyCode == 27) {
			jQuery('#popup').addClass('hidden');
			return false;
		}
	});

	jQuery(document).on('click', '#popup .close', function(event) {
		event.preventDefault();
		jQuery('#popup').addClass('hidden');
		return false;
	});


	jQuery('.collection-item a').click(function(e) {
		e.preventDefault();
		var pid = jQuery(this).data('postId');
		var link = jQuery(this).data('link');
		if(WINDOW_W < 600 || WINDOW_H < 400){
			window.location.href = link;
			return false;
		}
		jQuery.ajax({
			url: wp_vars.ajaxurl,
			type: 'POST',
			dataType: 'json',
			data: {
				action: 'get_post_follow_collection_item_ajax',
				post_id: pid
			},
		})
		.done(function(rep) {
			var html = '';
			html += '<div class="popup-inner fluid">';
			for(var i in rep['imgs']) {
				html += 	'<div class="preview grid6 block-product-preview clearfix" data-color="'+ rep['colors'][i]['code_id'] +'">';
				html +=			'<div class="preview-imgs">';
				html += 			'<ul>';
				for(var j in rep['imgs'][i]['img_src']) {
					html += 				'<li><img src="'+ rep['imgs'][i]['img_src'][j] +'" alt=""></li>';
				}
				html += 			'</ul>';
				html += 		'</div>';
				html += 		'<div class="thumb-imgs">';
				html += 			'<ul class="clearfix">';
				for(var j in rep['imgs'][i]['thumb_src']) {
					html += 				'<li><img src="'+ rep['imgs'][i]['thumb_src'][j] +'" alt=""></li>';
				}
				html += 			'</ul>';
				html += 		'</div>';
				html += 	'</div>';
			}
			html += 	'<div class="product-details grid6">';
			html += 		'<h2 class="condensed"><a href="'+ rep.url +'">'+ rep['title'] +'</a></h2>';
			html += 		'<div class="product-metadata">';
			html += 			'<div class="row grid12 pl0 pr0">';
			html += 				'<div class="grid4 fl pl0 pr0"><p>Mã số</p></div>';
			html += 				'<div class="grid8 fl pr0"><p class="item-id">'+ rep['sku'] +'</p></div>';
			html += 			'</div>';
			html += 			'<div class="row grid12 pl0 pr0">';
			html += 				'<div class="grid4 fl pl0 pr0"><p>Màu sắc</p></div>';
			html += 				'<div class="grid8 fl pr0">';
			html += 					'<p class="item-color clearfix product-color" id="item-color">';
			for(var i in rep['colors']) {
				html +=						'<span class="fl color" style="background-color: '+ rep['colors'][i]['code'] +'" title="'+ rep['colors'][i]['name'] +'" data-color="'+ rep['colors'][i]['code_id'] +'"></span>';
			}
			html += 					'</p>';
			html += 				'</div>';
			html += 			'</div>';
			html += 			'<div class="row grid12 pl0 pr0">';
			html += 				'<div class="grid4 fl pl0 pr0"><p>Size</p></div>';
			html += 				'<div class="grid8 fl pr0">';
			html += 					'<select name="product-size" id="item-size" class="item-size dropkick_target" style="float: left; margin-top: 3px; padding: 3px;">';
			for(var i in rep['sizes']) {
				html += 					'<option value="'+ rep['sizes'][i] +'">'+ rep['sizes'][i] +'</option>';
			}
			html += 					'</select>';
			html += 				'</div>';
			html += 			'</div>';
			html += 			'<div class="row grid12 pl0 pr0"><a class="view-more" href="'+ rep['url'] +'">Xem chi tiết >></a></div>';
			html += 			'<div class="row grid12 pl0 pr0">';
			html += 				'<a id="add_to_cart" data-product-id="'+ rep['id'] +'" data-color-number="0" data-size="M" data-quantity="1" class="add-to-cart btn" href="'+ rep.url +'"><i class="fa fa-shopping-cart" style="margin-right: 5px;"></i>Thêm vào giỏ</a>';
			html += 			'</div>';
			if(rep['user_id'] > 0) {
				html += 			'<div class="row grid12 pl0 pr0">';
				html += 				'<a class="add-to-wishlist btn" data-product-id="'+ rep['id'] +'" href="javascript:void(0);"><i class="fa fa-heart" style="margin-right: 5px;"></i>Thêm ưa thích</a>';
				html += 			'</div>';
			}
			html += 		'</div>';
			html += 	'</div>';
			html += 	'<span class="close">x</span>';
			html += '</div>';

			jQuery('#popup').html(html).load();

			jQuery('.block-product-preview').hide();
			jQuery('.block-product-preview:first').show();
			
			jQuery('.thumb-imgs').each(function() {
				jQuery(this).find('li:first').addClass('active');
			});
			jQuery('.preview-imgs').each(function() {
				jQuery(this).find('li').not(':first').hide();
			});

			function click_thumbnail(cur) {
				var cur_index = jQuery('#popup .thumb-imgs li').index(cur);

				jQuery('#popup .thumb-imgs li.active').removeClass('active');
				jQuery(cur).addClass('active');

				jQuery('#popup .preview-imgs li').hide();
				jQuery('#popup .preview-imgs li:eq('+ cur_index +')').show();
				return false;
			}

			jQuery(document).on('click', '.thumb-imgs li', function(event) {
				click_thumbnail(this);
			});

			jQuery(document).on('click', '.item-color span.color', function(event) {
				var code_id = jQuery(this).data('color');
				jQuery(this).addClass('active');
				jQuery(this).parents('.item-color').find('span.color').not(this).removeClass('active');

				jQuery('#popup .block-product-preview').hide();
				jQuery('#popup .block-product-preview[data-color="'+ code_id +'"] .preview-imgs li').hide();
				jQuery('#popup .block-product-preview[data-color="'+ code_id +'"] .thumb-imgs li').removeClass('active');
				jQuery('#popup .block-product-preview[data-color="'+ code_id +'"] .preview-imgs li:first').show();
				jQuery('#popup .block-product-preview[data-color="'+ code_id +'"] .thumb-imgs li:first').addClass('active');
				jQuery('#popup .block-product-preview[data-color="'+ code_id +'"]').show();
			});

			// Add to wishlish
			jQuery('#popup').on('click', '.add-to-wishlist', function(event) {
				var this_button = jQuery(this);
				var product_id = this_button.attr("data-product-id") || 0;
				if(product_id > 0) {
					jQuery.ajax({
	                    url: wp_vars['ajaxurl'],
	                    dataType: "json",
	                    async: true,
	                    type: "POST",
	                    cache: true,
	                    data: {
	                    	action: 'add_to_wishlist_ajax',
	                    	product_id: product_id
	                    },
	                    success: function(data) {
	                        if(data.msg){
	                        	alert(data.msg);
	                        }
	                    }
	                });
				}
			});

			jQuery('#popup').removeClass('hidden');
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
	});

// Add to wishlish
	jQuery('.add-to-wishlist').click(function(event) {
		event.preventDefault();
		var this_button = jQuery(this);
		var product_id = this_button.attr("data-product-id") || 0;		
		if(product_id > 0) {
			jQuery.ajax({
				url: wp_vars['ajaxurl'],
				dataType: "json",
				async: true,
				type: "POST",
				cache: true,
				data: {
					action: 'add_to_wishlist_ajax',
					product_id: product_id
				},
				success: function(data) {
					console.log(data);
					if(typeof data.result != 'undefined'){
						alert(data.msg);
						if(data.result) {
							//jQuery('.add-to-wishlist[data-product-id="'+ product_id +'"]').addClass('fav');
							jQuery('#add_to_wishlist').html('<i class="fa fa-heart" style="margin-right: 5px;color:#c61d20"></i>Bỏ yêu thích');
							jQuery('.add-to-wishlist[data-product-id="'+ product_id +'"]').html('<i class="fa fa-heart"></i>');
						}
						else {
							if(jQuery('body').find('.block-users-wishlist').length > 0) {
								jQuery('.add-to-wishlist[data-product-id="'+ product_id +'"]').parents('.single-item-product').hide();
							}
							//jQuery('.add-to-wishlist[data-product-id="'+ product_id +'"]').removeClass('fav');
							jQuery('#add_to_wishlist').html('<i class="fa fa-heart" style="margin-right: 5px;"></i>Thêm vào yêu thích');
							jQuery('.add-to-wishlist[data-product-id="'+ product_id +'"]').html('<i class="fa fa-heart-o"></i>');
						}
					}
				}
			});
		}
	});

// STYLE GUIDE - HINH TUONG THOI TRANG EFFECT
	if(jQuery('body').find('.item-animate-holder').length > 0 && WINDOW_W > 770){
		var h = jQuery(this);
		var click = true;

		var oh = jQuery('.item-animate-holder').offset(); // offset holder

		jQuery('.item-animate img').click(function() {
			var t = jQuery(this);
			var i = t.parents('.item');
			var i_row = i.attr('data-row');
			var oi = i.offset(); // offset item

			var style_open = {
				'position': 'absolute',
				'width': '100%',
				'padding-left': '0px',
				'padding-right': '0px'
			};

			if(i.hasClass('left-to-right'))
				style_open['left'] = '0';
			else if(i.hasClass('right-to-left'))
				style_open['right'] = '0';

			if(!i.hasClass('open')) {
				i.find('.item-title').css('visibility', 'hidden');
				i.addClass('open');

				h.find('.item[data-row="'+ i_row +'"]').not(i).hide().addClass('item-hide');
				i.css({'left': oi.left - oh.left});
				i.animate(style_open, 500).find('.content-abs').fadeIn(1000).removeClass('hidden');
			}
			else {
				i.removeClass('open');
				i.find('.item-title').css('visibility', 'visible');
				i.find('.content-abs').hide().addClass('hidden');
				i.attr('style', '');
				h.find('.item.item-hide').removeClass('.item-hide').show();
			}
		});
	}

// STYLE GUIDE - SAP XEP SAN PHAM THEO GIA
	jQuery('.sortby-price').find('a').click(function(e) {
		// e.preventDefault();
		jQuery(this).addClass('current').siblings('a').not(this).removeClass('current');
	});

// AGENCY CHOICE
	if(jQuery('body').find('#map').length > 0) {
		create_map();
	}
	jQuery('#showroom-filter-btn').click(function(e) {
		e.preventDefault();

		var city = jQuery('#showroom-selector').val();
		var type = jQuery('#showroom-type-selector').val();
		console.log(city, type);

		if(city == '-1' && type == '-1') {
			jQuery('.showroom-lists').find('li').show();
			return false;
		}
		else if(city != '-1' && type != '-1') {
			jQuery('.showroom-lists').find('li').each(function() {
				if(jQuery(this).find('a[data-term-id="'+ city +'"][data-type="'+ type +'"]').length <= 0) {
					jQuery(this).hide();
				}
				else {
					jQuery(this).show();
				}
			});
		}
		else if(city != '-1') {
			jQuery('.showroom-lists').find('li').each(function() {
				if(jQuery(this).find('a[data-term-id="'+ city +'"]').length <= 0) {
					jQuery(this).hide();
				}
				else {
					jQuery(this).show();
				}
			});
		}
		else {
			jQuery('.showroom-lists').find('li').each(function() {
				if(jQuery(this).find('a[data-type="'+ type +'"]').length <= 0) {
					jQuery(this).hide();
				}
				else {
					jQuery(this).show();
				}
			});
		}
	});
	jQuery('.showroom-item').live('click', function(event) {
		event.preventDefault();
		jQuery(this).parents('li').addClass('current').siblings('li').removeClass('current');
		var f = jQuery(this).attr('for');
		var latlng = jQuery(this).attr('data-latlng').split(/,\s*/);

		var info = {
			address: jQuery(this).attr('data-addr'),
			phone: jQuery(this).attr('data-phone'),
			title: jQuery(this).text()
		};
		create_map(latlng, info);
	})

	var map;
	var geocoder;
	function create_map(latlng, info) {
		// Create first map
		if(typeof(latlng) === 'undefined') latlng = ['12.209580', '109.201727']; // vi tri mac dinh
		geocoder = new google.maps.Geocoder();
		
		var mapProp = {
			center: new google.maps.LatLng(parseFloat(latlng[0]), parseFloat(latlng[1])),
			zoom: 16,
			disableDefaultUI: true,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};

		map = new google.maps.Map(document.getElementById('map'), mapProp);

		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(parseFloat(latlng[0]), parseFloat(latlng[1])),
			map: map
		});

		if(typeof info != 'undefined') {
			var contentString = '<div class="info"><div class="info_content">';
			contentString += '<h4>'+ info.title +'</h4>';
			contentString += '<p><strong>Địa chỉ: </strong>'+ info.address +'</p>';
			contentString += '<p><strong>SĐT: </strong>'+ info.phone +'</p>';
			contentString += '</div></div>';
			var infowindow = new google.maps.InfoWindow({
				content: contentString,
				minWidth: 500
			});
			infowindow.open(map,marker);
		}

		google.maps.event.addListener(marker, 'click', function() {
			infowindow.open(map, marker);
		});

		return false;
	}

// CUSTOM SCROLLBAR
	jQuery('.item-content').customScrollbar({
		hScroll: false
	}).addClass('gray-skin');

// Message requests
	var cookie_name = 'khatoco_message_seen';
	var cookie_value = get_cookie(cookie_name);
	if(cookie_value == ''){
		$.ajax({
			url: wp_vars.ajaxurl,
			type: 'POST',
			dataType: 'json',
			data: {action: 'message_center_announce'},
		})
		.done(function( response ) {
			if(response.result == 'true'){
				$('#message_center .inner').html(response.html);
				$('#message_center').animate({'right': 0}, 600);
			}
		});
	}

	$('#message_center .close').click(function(event) {
		$('#message_center').animate({'right': '-320px'}, 600);
		set_cookie(cookie_name, 'yes', 1); // set cookie 1 day
	});

	// login facebook
	$('#facebook-login').click(function(event) {
        event.preventDefault();
        check_facebook_login_state();
    });

// Window resize
	jQuery(window).resize(function(event) {
		if(WINDOW_W <= 1096) {
			/*jQuery('body').click(function() {
				if(jQuery('body').find('.header-top-menu.show').length > 0)
					jQuery('.header-top-menu').fadeOut().removeClass('show');
			});*/
			jQuery('#hambuger-top-menu').click(function(event) {
				if(jQuery('body').find('.side-menu-mobile.show').length <= 0){
					jQuery('.side-menu-mobile').addClass('show').show();
					jQuery('.side-menu-mobile ul').animate({left: 0}, 500).addClass('show');
					jQuery('body').css('overflow', 'hidden');	

					jQuery('#mobile_menu_close').click(function(event) {
						jQuery('.side-menu-mobile ul').animate({left: '-250px'}, 500);
						jQuery('.side-menu-mobile').removeClass('show').fadeOut();
						jQuery('body').css('overflow', 'auto');
					});
				}
				/*else{
					jQuery('.side-menu-mobile ul').animate({left: '-100%'}, 500);
					jQuery('.side-menu-mobile').removeClass('show').hide();
				}*/
				return false;
			});
		}
		//jQuery('.side-menu-mobile').height(jQuery(window).height());
		// jQuery('.side-menu-mobile ul').height(jQuery(window).height()).css('max-height', jQuery(window).height());

		if(WINDOW_W > 770){
			jQuery('#hc-sticky').hcSticky({
				top: 10,
		        followScroll: false
		    });
		}
	});
});

jQuery.fn.dropkick = function () {
	var args = Array.prototype.slice.call( arguments );
	return jQuery( this ).each(function() {
		if (this.childNodes[0].textContent.trim().length == 0) {
			return;
		}
		if ( !args[0] || typeof args[0] === 'object' ) {
			new Dropkick( this, args[0] || {} );
		} else if ( typeof args[0] === 'string' ) {
			Dropkick.prototype[ args[0] ].apply( new Dropkick( this ), args.slice( 1 ) );
		}
	});
};

function show_cart() {
	jQuery('html, body').animate({scrollTop: 0}, 500);
	jQuery('#cart_link').addClass('hover');
	setTimeout(function() {
		jQuery('#cart_link').removeClass('hover');
	}, 4000);
	return false;
}

/* Toan - Load Cart */
function load_top_cart(){
	jQuery.ajax({
        url: wp_vars.ajaxurl,
        dataType: "json",
        async: true,
        type: "POST",
        cache: true,
        data: {
        	action: 'manage_cart_ajax',
        	do_ajax: 'load_top_cart'
        },
        success: function(data) {
            if(data.success == 'true'){
                jQuery('#load_top_cart').html(data.html);
                jQuery('#load_cart_num').html(data.cartCount);
                jQuery('#load_cart_num_anchor').html(data.cartCount);
                jQuery('#load_cart_num_mobile').html(data.cartCount);
                if(data.cartCount>9){
                	jQuery('#load_cart_num').css("padding-left","2px");
                	jQuery('#load_cart_num_anchor').css("padding-left","2px");
                	jQuery('#load_cart_num_mobile').css("padding-left","2px");
                }else{
                	jQuery('#load_cart_num').removeAttr("style");
                	jQuery('#load_cart_num_anchor').removeAttr("style");
                	jQuery('#load_cart_num_mobile').removeAttr("style");
                }
                	
            }else{
                if(data.msg){
                	alert(data.msg);
                }
            }
        }
    });
}

/* Toan - Load Cart */
function remove_from_cart(cart_id){
	//var this_button = jQuery(element);

	jQuery.ajax({
        url: wp_vars.ajaxurl,
        dataType: "json",
        async: true,
        type: "POST",
        cache: true,
        data: {
        	action: 'manage_cart_ajax',
        	do_ajax: 'remove_from_cart',
        	cart_id: cart_id
        },
        success: function(data) {
        	var is_cart_page = false;
        	if(jQuery('body').hasClass('page-template-page-cart')){
        		location.reload();
        		return false;
        	}
            if(data.success == 'true'){
                load_top_cart();
            }
        }
    });
}

// Messages about coupon and promotion campaigns
function get_cookie(cname) {
	var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}

function set_cookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function delete_cookie(cname) {
	document.cookie = cname + "=; expires=Thu, 01 Jan 1970 00:00:00 UTC"; 
}

/*
Facebook needed functions
*/

// Check status when connect to facebook with param response. This function is callback one of other
function status_change_callback(response) {
    // If status is 'connected', get user info to set cookie|save user session|register for user|...
    if(response.status === 'connected') {
        facebook_login_api_callback(); 
    }
    // If 'not_authorized', redirect browser to link. You can change app_id and the uri (1) to facebook redirect after user press 'OK' or 'Cancel'. (Now is current uri, 'wp_vars.current' is my global variable ^_^).
    // Param 'way=login_fb' need to the logic when facebook call your (1)
    else if(response.status === 'not_authorized') {
        var current_permalink = wp_vars.current;
        current_permalink = updateQueryStringParameter( current_permalink, 'way', 'login_fb' );
        var url_dialog = 'https://www.facebook.com/dialog/oauth?client_id=1020385701325846&scope=email,public_profile&redirect_uri='+ current_permalink;
        window.location.replace(url_dialog);
    }
    // Else, we need user login their facebook acc :3
    else {
        FB.login(function(response) {
            // If they close the popup
            if( response.authResponse == null )
                return false;

            check_facebook_login_state();
        }, {scope: 'email,public_profile'});
    }
}

// check facebook login state
function check_facebook_login_state() {
    FB.getLoginStatus(function(response) {
        status_change_callback(response);
    });
}

// And do something with user information. Here I get some information like name, email, gender and something else for an ajax request.
function facebook_login_api_callback(){
    FB.api('/me?fields=email,name,gender', {scope: 'public_profile,email'}, function(response) {
    	// console.log(response);
        var scopeid = response.id;
        var email = response.email;
        var name = response.name;
        var gender = response.gender;
        
        jQuery.ajax({
            url: wp_vars.ajaxurl,
            type: 'POST',
            dataType: 'json',
            data: {
                action: 'register_user_over_fb',
                id: scopeid,
                email: email,
                name: name,
                gender: gender
            }
        })
        .done(function(res) {
            if(res.result == 1){
            	if( typeof res.message != 'undefined' )
            		alert(res.message);
            	if( typeof res.redirect != 'undefined' )
            		window.location.replace(res.redirect);
            	else
            		location.reload();
            }
            else{
            	alert(res.message);
            }
            return false;
        });
    });
}

// Initialize and check user press 'OK' or 'Cancel' at facebook dialog
window.fbAsyncInit = function() {
    FB.init({
        appId      : '1020385701325846', // Change it to your facebook app id
        cookie     : true, 
        xfbml      : true,
        version    : 'v2.4'
    });

    // Get param in current url and fetch its to an array
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    var user_agree = false;

    // Check 'OK' or 'Cancel'
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        // If user agree login with their facebook account, current url will havent bellow params
        if((pair[0] == 'error' && pair[1] == 'access_denied') || (pair[0] == 'error_reason' && pair[1] == 'user_denied') || (pair[0] == 'error_code' && pair[1] == '200')){
            user_agree = false;
        }
        // It only have this bellow param. See! This is our param (line 24)
        if(pair[0] == 'way' && pair[1] == 'login_fb'){
            user_agree = true;
        }
    }

    // And check last state
    if(user_agree){
        FB.getLoginStatus(function(response) {
            status_change_callback(response);
        });
    }
};

function updateQueryStringParameter(uri, key, value) {
  var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
  var separator = uri.indexOf('?') !== -1 ? "&" : "?";
  if (uri.match(re)) {
    return uri.replace(re, '$1' + key + "=" + value + '$2');
  }
  else {
    return uri + separator + key + "=" + value;
  }
}


// Messages about coupon and promotion campaigns
function get_cookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}

function set_cookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function delete_cookie(cname) {
    document.cookie = cname + "=; expires=Thu, 01 Jan 1970 00:00:00 UTC"; 
}


function submitenter(myfield,e)
{
    var keycode;
    if (window.event) keycode = window.event.keyCode;
    else if (e) keycode = e.which;
    else return true;

    if (keycode == 13)
    {
        myfield.form.submit();
        return false;
    }
    else
        return true;
}
function filter_select(myfield,e)
{
	var val = jQuery(myfield).val();
	jQuery('#sort').val(val);
	jQuery( "#form-filter" ).submit();
}