jQuery(document).ready(function($){
});

var appCalSize = angular.module("ktcCalSize", []);
appCalSize.controller('ktcControllerCalSize', ['$scope', function ($scope) {

    $scope.productType = 'shirt';   
    $scope.productForm = 'regular';
    $scope.calType = 'hW';
    $scope.suggestSize = '';

    $scope.suggest = function(){
        if ($scope.productType=='tShirt'){
            $scope.calType = 'hW';
        }

        if ($scope.productType=='shirt' && $scope.calType=='belly'){
            $scope.calType = 'hW';
        }

        if ($scope.productType.startsWith('pant')){
            $scope.calType = 'belly';
        }

        if ($scope.productType == 'pant0' || $scope.productType == 'pant1' ){
            $scope.productForm = 'regular';
        }

        if ($scope.productType == 'pantKaki0' || $scope.productType == 'pantKaki1' ){
            $scope.productForm = 'regular';
        }

        if ($scope.productType == 'pantKaki' ){
            $scope.productForm = 'slimfit';
        }

        if ($scope.productType == 'pantShort' && $scope.productForm == 'tailor'){
            $scope.productForm = 'regular';
        }

        if ( $scope.calType === 'hW' ){
            var args = {
            height : $scope.shirtHeight,
            weight :$scope.shirtWeight
            };
            $scope.suggestSize = suggestSize($scope.productType, $scope.productForm, args);
        }

        else if ( $scope.calType === 'other' && $scope.productType!='tShirt'){
            var args = {
            chestSize : $scope.chestSize,
            neckSize :$scope.neckSize,
            shoulderSize : $scope.shoulderSize
            };
            $scope.suggestSize = suggestSize($scope.productType, $scope.productForm, args);
        }

        else if ( $scope.calType === 'belly' && $scope.productType.startsWith('pant')){
            var args = {
            bellySize : $scope.bellySize,
            rearSize :$scope.rearSize,
            thighSize : $scope.thighSize
            };
            $scope.suggestSize = suggestSize($scope.productType, $scope.productForm, args);
        }

        $scope.suggestSize = $scope.suggestSize==null ? 'overload' : $scope.suggestSize;
    }

}]);

jQuery(document).ready(function(){
    jQuery('#get-size').on('click',function(e){
        var suggestSize = angular.element(jQuery(".modal-dialog")).scope().suggestSize;
        if (typeof suggestSize != 'undefined' && suggestSize!=null && suggestSize!='' && suggestSize!='overload'){
            var item = jQuery('#item-size option[value="'+suggestSize+'"]');
            if (item.length == 0){
                alert('Hiện tại không còn size này trong kho');
            } else {
                item.prop('selected',true);
                jQuery('.modal').modal('toggle');
            }
        }
    });

    jQuery('#calSizeModal').on('shown.bs.modal', function () {
        var code = jQuery('.product-details .title').text().split(/^.*( (A|Q)[A-Z0-9]+)(?=-).*$/)[1].trim();
        var productType = code.match(/^[A-Z0-9]{2}/)[0];
        var productForm = code.match(/([A-Z0-9])[0-9]$/)[1];
        var productLy = code.match(/[0-9]$/)[0];

        var productTypePopup = jQuery('#product-type');
        var productFormPopup = jQuery('#product-form');

        var scope = angular.element(jQuery(".modal-dialog")).scope();

        scope.$apply(function () {               // 3
            switch (productType) {
            case 'A1':
                scope.productType = 'shirt';
                break;
            case 'A2':
                scope.productType = 'tShirt';
                break;
            case 'Q2':
                scope.productType = 'pantKaki';
                break;
            case 'Q3':
                scope.productType = 'pantShort';
                break;
            default:
                break;
            }

            switch (productForm) {
            case 'S':
                scope.productForm = 'slimfit';
                break;
            case 'T':
                scope.productForm = 'tailor';
                break;
            case 'R':
                scope.productForm = 'regular';
                break;
            }

            if (productType == 'Q1' && productLy == '0'){
                scope.productType = 'pant0';
            }
            else if (productType == 'Q1' && productLy == '1'){
                scope.productType = 'pant1';
            }
            else if (productType == 'Q2' && productLy == '0'){
                scope.productType = 'pantKaki0';
            }
            else if (productType == 'Q2' && productLy == '1'){
                scope.productType = 'pantKaki1';
            }

            scope.suggest();

        });

        
    });


});
