$(document).ready(function () {
    $('.group-category').find('.item').each(function (key, value) {
        $(this).on('click', function () {
            $('.group-category').find('.item').each(function (key, value) {
                $(this).removeClass('active');
            });
            $(this).addClass('active');
            let categoryTypeId = $(this).attr('category-type');
            if($("#list-"+categoryTypeId+'-render').html() == ""){
                console.log($('#'+categoryTypeId+'-tab').find('.all-event-cta'));
                $('#'+categoryTypeId+'-tab').find('.all-event-cta').click();
            }



        })
    });
    $('.all-event-cta').on('click', function (e) {
        console.log(123);
        let type = $(this).attr('data-type');
        let offset = $(this).attr('data-offset');
        let postType = $(this).attr('data-post');
        if(offset == -1){
            e.preventDefault();
            return;
        }
        let renderField = $(this).attr('data-render');
        letPointer = $(this);
        $.ajax({
            url: '/wp-admin/admin-ajax.php',
            type: 'POST',
            dataType: 'json',
            data: {
                action: 'ajax_loading_news',
                type: type,
                offset: offset,
                postType:postType
            }

        }).done(function (result) {
            if (result.html == '') {
                letPointer.addClass('d-none');
                letPointer.attr('data-offset', -1);
            } else {
                letPointer.attr('data-offset', result.nextOffset);
                $('#' + renderField).append(result.html);
            }

        })
    });
});
