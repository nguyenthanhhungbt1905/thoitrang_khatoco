/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var table_camp_select = null;
var bs_error = {
    'clear_error': function () {
        jQuery('.bs-error').html('');
    },
    'set_error': function (text) {
        jQuery('.bs-error').html(text);
    }
};

var camp_tb_error = {
    'clear_error': function () {
        jQuery('.table_camp_error').html('');
    },
    'set_error': function (text) {
        jQuery('.table_camp_error').html(text);
    }
};

function render_hidden(key, value, name) {
    return value + '<br><input type="hidden" name="' + name + '" value="' + key + '">';
}

function parse_element_to_obj(dom , type) {

    if (type === undefined) type = "text";

    if (type === 'text'){
        return { key : dom.text(), value : dom.val()}
    } 
    else if (type === 'select'){
        var dom_selected = dom.children('option:selected');
        // var list_val = dom_selected.text();
        // var list_key = dom_selected.val();
        
        if (dom_selected.length > 1){ // multi dropdown
            var list_item = [];
            for (var j=0; j< dom_selected.length; j++){
                var item = { key : dom_selected[j].value , value : dom_selected[j].text }
                list_item.push(item);
            }
            return list_item;
        } else {
            return { key : dom_selected.val() , value : dom_selected.text() }
        }
        
    }
    else if (type === 'radio'){
        return { key : dom.val(), value : dom.attr('data-label') }
    }
}

function refreshDataTable(dt){
    dt.rows().invalidate(dt.rows().data()).draw();
}
jQuery(document).ready(function ($) {
//    table_camp_select = table_campaign_card_tb; // default data table
    var type_sale = $('input[name="promotion_campaign[sale][type]"]:checked').val();
    if (type_sale === 'orders')
        table_camp_select = table_campaign_orders_tb;
    else if (type_sale === 'product')
        table_camp_select = table_campaign_product_tb;
    else if (type_sale === 'multi-product')
        table_camp_select = table_campaign_multi_product_tb;
    else if (type_sale === 'buy-money-product')
        table_camp_select = table_campaign_money_product_tb;

    //    Auto create input field after submit data, to send data to server
    jQuery('#publish').on('click', function (event) {

        // if (table_camp_select === null || table_camp_select.rows().data().length === 0) {
        //     camp_tb_error.set_error('Bạn phải nhập dữ liệu mới có thể save!');
        //     event.preventDefault();
        //     return;
        // }


    });

});

