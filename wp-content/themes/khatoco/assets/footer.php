<footer id="footer" class="footer">
	<div class="top-footer wow fadeIn" data-wow-duration="1s">
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-sm-4 col-xs-12">
					<div class="contact">
						<h2 class="text-footer">Liên hệ</h2>
						<ul>
							<li>Công ty TNHH Thương Mại KHATOCO</li>
							<li>07 Võ Thị Sáu, phường Vĩnh Nguyên, thành phố Nha Trang, tỉnh Khánh Hòa, Việt Nam.</li>
							<li>ĐT: (84) 58.388 9978 - Fax: (84) 58.388 6422</li>
						</ul>
					</div>
					<div class="social">
						<h2 class="text-footer">chứng nhận</h2>
						<span>Giấy Chứng nhận đăng ký kinh doanh số 4200485207 do Sở Kế hoạch Đầu tư Tỉnh Khánh Hòa cấp.</span>
						<li><a class="footer-logo" href="http://online.gov.vn/HomePage/CustomWebsiteDisplay.aspx?DocId=23389"><img src="<?php echo get_template_directory_uri(); ?>/images/bocongthuong.png" alt=""></a></li>
						<li><a class="footer-logo" href="http://thoitrangkhatoco.vn/tin-tuc/thoi-trang-nam-khatoco-nhan-danh-hieu-thuong-hieu-quoc-gia-nam-2016"><img src="<?php echo get_template_directory_uri(); ?>/images/logo-vietnam-value.png" alt=""></a></li>
						<ul>
							<li><a target="_blank" href="<?php echo get_option('facebook_link'); ?>"><i class="fa fa-facebook"></i></a></li>
							<li><a target="_blank" href="<?php echo get_option('pinterest_link'); ?>"><i class="fa fa-pinterest"></i></a></li>
							<li><a target="_blank" href="<?php echo get_option('google_plus_link'); ?>"><i class="fa fa-google-plus"></i></a></li>
							<li><a target="_blank" href="<?php echo get_option('youtube_link'); ?>"><i class="fa fa-youtube"></i></a></li>
						</ul>
					</div>
				</div>

				<div class="col-md-4 col-sm-4 col-xs-12">
					<div class="contact">
						<ul>
							<?php wp_nav_menu(array('theme_location' => 'footer-bot-menu-col-5', 'container' => false, 'items_wrap' => '<div class="menu-col">%3$s</div>', 'walker' => new Footer_Bot_Menu())); ?>
						</ul>
					</div>
					<div class="logistic">
						<h2 class="text-footer">dịch vụ vận chuyển</h2>
						<ul>
							<li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/vnpt.png" alt=""></a></li>
							<!--<li><a href="#"><img src="<?php /*echo get_template_directory_uri(); */?>/images/TNT.png" alt=""></i></a></li>-->
						</ul>
					</div>
					<div class="payment">
						<h2 class="text-footer">hỗ trợ thanh toán</h2>
						<a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/payment.png" alt=""></a>
					</div>
				</div>

				<div class="col-md-4 col-sm-4 col-xs-12">
					<div class="register">
						<h2 class="text-footer">đăng ký email</h2>
						<span>Nhận tin nhắn khuyến mãi và ưu đãi hấp dẫn.</span>
						<form action="" method="post" class="subscriber_form" id="subscriber_form" accept-charset="utf8">
							<input type="email" name="subscribe-email" id="subscribe-email" placeholder="Email của bạn">
							<button type="submit">Đăng ký</button>
						</form>
					</div>
					<div class="facebook">
						<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fthoitrangkhatoco&tabs&width=340&height=154&small_header=true&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="100%" height="154" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
					</div>
				</div>

			</div>
		</div>
	</div>

	<div class="help">
		<div class="container">
			<span>Copyright © 2016 thoitrangkhatoco.vn</span>
		</div>
	</div>
	
		
	<div class="scrolltop"></div>
	<?php wp_footer(); ?>
	
	<!-- Pinterest -->
	<script type="text/javascript" async defer data-pin-color="red" data-pin-height="28" data-pin-hover="true" src="//assets.pinterest.com/js/pinit.js"></script>

	<!--Start of Tawk.to Script-->
	<script type="text/javascript">
	var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
	(function(){
	var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
	s1.async=true;
	s1.src='https://embed.tawk.to/573579cc7b7d53d702e1893d/default';
	s1.charset='UTF-8';
	s1.setAttribute('crossorigin','*');
	s0.parentNode.insertBefore(s1,s0);
	})();
	</script>
	<!--End of Tawk.to Script-->
	
	<!--Begin Google Analytics--
	<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	ga('create', 'UA-53463109-8', 'auto');
	ga('send', 'pageview');
	</script>
	<!--End of Google Analystic-->
	<script>
		jQuery(document).ready(function($) {
			$('#collapse-menu').height(0);

			$('.side-menu-mobile .menu-item-has-children > a').append("<i class='fa fa-angle-right pull-right angle-rotate-menu' aria-hidden='true'</i>").removeAttr('href');	

			$( ".side-menu-mobile .menu-item-has-children" ).click(function() {
				$(this).find('.sub-menu-mobile').toggle(400);

			    if ( $(this).find('.angle-rotate-menu').css( "transform" ) == 'none' ){
			        $(this).find('.angle-rotate-menu').css("transform","rotate(90deg)");
			    } else {
			        $(this).find('.angle-rotate-menu').css("transform","" );
			    }
			});

			$( ".side-menu-mobile .personal-mobile" ).click(function() {

			    if ( $(this).find('.angle-rotate-menu').css( "transform" ) == 'none' ){
			        $(this).find('.angle-rotate-menu').css("transform","rotate(90deg)");
			    } else {
			        $(this).find('.angle-rotate-menu').css("transform","" );
			    }
			});	

		});
	</script>

	<script type="text/javascript">

		(function($) {
		    var $window = $(window),
		        $html = $('html');

		    function resize() {
		        if ($window.width() < 480) {
		            $('.test').addClass('left-logo-mobile');
		            $('.test').removeClass('left-logo');
		            $('#header-top-search-cancel').css('display', 'block');

					$('.icon-search').on('click', function(event) {
						event.preventDefault();
						$('.left-logo-mobile').css('display', 'block');
						$('#header-top-search-cancel').css('display', 'block');
						$('.background-opacity').css('display', 'block');
						$('.search-mobile').css('z-index', '1010');
					});

		            $('#header-top-search-cancel').on('click', function(event) {
						event.preventDefault();
				     	$('.left-logo-mobile').css('display', 'none');
				     	$('.background-opacity').css('display', 'none');
					});

					$('.background-opacity').on('click', function(event) {
						event.preventDefault();
				     	$('.left-logo-mobile').css('display', 'none');
				     	$('.background-opacity').css('display', 'none');
					});

					return true;
		        }

		        $('.test').removeClass('left-logo-mobile');
		        $('.test').addClass('left-logo');
		    }

		    $window
		        .resize(resize)
		        .trigger('resize');
		})(jQuery);	
	</script>

	<div id="message_center" class="">
		<div class="inner"></div>
		<div class="close">X</div>
	</div>

</footer>
<div class="ms-popup">
	<div class="ms-popup-dialog clearfix">
		<div class="popup-header"><span class="title">Testing popup</span><span class="close"><i class="fa fa-times" aria-hidden="true"></i></span></div>
		<div class="popup-content">amsid asd iasdu akjdaksl djaskhd ajksdh asjkd</div>
		<div class="popup-footer">
			<button class="popup-ok">OK</button>
		</div>
	</div>
</div>

</body>
</html>