<?php get_header(); ?>
<section id="banner-top">
    <div id="banner-slide">
        <?php for ($i = 0; $i < 4; $i++) { ?>
            <div class="detail-slide">
                <div class="image-filter"
                     style="background-image:url('<?php echo get_template_directory_uri() . "/images/new/banner.jpg" ?>')"></div>
                <!--                <img class="img-fluid" src="-->
                <?php //echo get_template_directory_uri()."/images/new/banner.jpg" ?><!--" alt="banner" >-->
            </div>
        <?php } ?>
    </div>
</section>
<section id="category-banner">
    <div class="container">
        <div class="row">
            <div class="col-xs-4">
                <div class="group-category">
                    <img class="img-fluid"
                         src="<?php echo get_template_directory_uri() . "/images/new/icon/store-alt.png" ?>">
                    <span class="name">HỆ THỐNG CỬA HÀNG</span>
                </div>
            </div>
            <div class="col-xs-4 middle-item">
                <div class="group-category ">
                    <img class="img-fluid"
                         src="<?php echo get_template_directory_uri() . "/images/new/icon/list-alt.png" ?>">
                    <span class="name">HƯỚNG DẪN MUA HÀNG</span>
                </div>
            </div>
            <div class="col-xs-4">
                <div class="group-category">
                    <img class="img-fluid"
                         src="<?php echo get_template_directory_uri() . "/images/new/icon/swatchbook.png" ?>">
                    <span class="name">HƯỚNG DẪN CHỌN SIZE</span>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="guide-style">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h3 class="masthead">Gu Đàn Ông</h3>
            </div>
        </div>
        <div class="row mobile-display">
            <?php $styleGuild = getAllStyleGuide() ?>
            <div class="col-xs-12">
                <div id="style-guide-slides">
                    <?php foreach ($styleGuild as $item) { ?>
                        <div class="detail-slide">
                            <div class="row">
                                <div class="col-xs-11 mx-auto">
                                    <div class="image-style-guide image-preview"
                                         style="background-image:url('<?php echo $item['feature_image'] ?>')"></div>
                                    <h4 class="title"><?php echo $item['title'] ?></h4>
                                    <p class="short-description"><?php echo wp_trim_words($item['short_description'], 15, '...');  ?></p>
                                    <a class="link" href="<?php echo $item['link'] ?>">XEM BÀI VIẾT <i
                                            class="fa fa-angle-double-right" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>

        </div>
        <div class="row first-styles desktop-display">
            <div class="col-xs-12">

                <div class="row">
                    <div class="col-md-8">
                        <div class="image-style-guide"
                             style="background-image:url('<?php echo $styleGuild[0]['feature_image'] ?>')"></div>
                        <!--                        <img class="img-fluid" src="-->
                        <?php //echo $styleGuild[0]['feature_image'] ?><!--">-->
                    </div>
                    <div class="col-md-4 pt-4">
                        <h4 class="title"><?php echo $styleGuild[0]['title'] ?></h4>
                        <p class="short-description"><?php echo $styleGuild[0]['short_description'] ?></p>
                        <a class="link" href="<?php echo $styleGuild[0]['link'] ?>">XEM BÀI VIẾT <i
                                class="fa fa-angle-double-right" aria-hidden="true"></i></a>
                    </div>
                </div>
                <?php unset($styleGuild[0]) ?>
            </div>
        </div>
        <div class="row desktop-display">
            <?php foreach ($styleGuild as $item) { ?>
                <div class="col-md-4">
                    <div class="image-style-guide image-preview"
                         style="background-image:url('<?php echo $item['feature_image'] ?>')"></div>
                    <h4 class="title"><?php echo $item['title'] ?></h4>
                    <p class="short-description"><?php echo $item['short_description'] ?></p>
                    <a class="link" href="<?php echo $item['link'] ?>">XEM BÀI VIẾT <i class="fa fa-angle-double-right"
                                                                                       aria-hidden="true"></i></a>
                </div>
            <?php } ?>
        </div>
    </div>
</section>
<section id="banner-winter"
         style="background-image: url('<?php echo get_template_directory_uri() . '/images/new/banner-winter.jpg' ?>')">
    <div class="group-text">
        <div class="item-left">
            <p class="text">
                CHRIST-
                <br/>MAS
                <br/>SALE
            </p>
            <p class="text">
                20/11 - 12/12
            </p>
        </div>
        <div class="item-right">
            <a href="/" class="see-more">XEM THÊM</a>
        </div>
    </div>
</section>
<section id="hot-product">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 break-line">
                <div class="row">
                    <div class="col-xs-12 col-md-4 col-lg-5">
                        <h3 class="masthead">Sản phẩm nổi bật</h3>
                    </div>
                    <div class="col-xs-12 col-md-8 col-lg-7">
                        <div class="group-category">
                            <div class="category-item first-el active" category-id="0">
                                <span class="name">Sản phẩm sale</span>
                            </div>
                            <div class="category-item second-el" category-id="1">
                                <span class="name">Áo sơ mi</span>
                            </div>
                            <div class="category-item" category-id="73">
                                <span class="name">Quần Khaki</span>
                            </div>
                            <div class="category-item" category-id="12">
                                <span class="name">Quần short</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="row product-prepend">
            <div class="col-xs-12 col-md-6">
                <img class="img-fluid d-none d-md-block mb-4"
                     src="<?php echo get_template_directory_uri() . "/images/new/blackfriday-banner.png" ?>"
                     alt="black friday banner">
                <img class="img-fluid d-md-none" style="margin-bottom:10px;"
                     src="<?php echo get_template_directory_uri() . "/images/new/blackfriday-mb.png" ?>"
                     alt="black friday banner">
                <?php //echo get_category_list() ?>
                <!--                -->
            </div>
            <?php $saleProducts = aj_get_home_posts(0, 6, 'hot');
            ?>
            <?php foreach ($saleProducts->posts as $item) {
                ?>
                <div class="col-xs-4 col-md-3 pb-4 ">
                    <div class="wrapping-tag">
                        <?php
                        $is_sale = get_post_meta($item->ID,'sale_off');
                        if($is_sale[0] > 0){
                            ?>
                            <div class="tag-sale">SALE</div>
                        <?php } ?>
                    <div class="product-card">
                        <a class="redirect-link" href="<?php echo get_permalink($item->ID) ?>"></a>

                        <div class="image-wrapper">
                            <?php
                            $product_colors = get_post_meta($item->ID, 'post_colors', true);
                            $images_ids     = explode(',', $product_colors[0]['images_ids']);
                            $images_ids     = $images_ids[0];
                            $images_ids     = wp_get_attachment_image_src($images_ids, [350, 1000]);
                            ?>
<!--                            <img class="img-fluid" src="--><?php //echo $images_ids[0] ?><!-- ">-->
                            <img class="img-fluid" src="http://<?php echo $_SERVER['HTTP_HOST'] ?>/wp-content/uploads/2019/02/A2MN438R2-VNMA031-2009-N-1-768x960.jpg">
                        </div>
                        <div class="product-info">

                            <div class="price-group text-left">
                            <span class="item-price condensed">
    <?php
    /*$saleoff_info = get_saleoff_info($post->ID);*/
    $price        = (int) get_post_meta($item->ID, 'post_price', true);
    $saleoff_info = ProductUtils::getProductSaleOffPrice($item->ID);
    if ($saleoff_info['percent'] > 0) {
        echo '<b>' . number_format($saleoff_info['value'], 0, ',', '.') . ' Đ </b> &nbsp;';
        echo '<span class="blink-text">' . aj_format_number($price) . ' Đ</span>';
    } else {
        echo '<b>' . aj_format_number($price) . ' Đ </b>';
    }
    ?>

                </span>
                            </div>
                            <p class="name text-left"><?php echo $item->post_title ?></p>
                        </div>
                        <div class="add-card">
                            <a class="cta" href="<?php echo get_permalink($item->ID) ?>">
                                <i class="fa fa-shopping-cart" aria-hidden="true"></i> Thêm vào giỏ hàng
                            </a>
                        </div>
                    </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</section>
<section id="promotion-news">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h3 class="masthead">Tin khuyến mãi</h3>
            </div>
            <div class="col-xs-12">
                <img class="img-fluid mb-3" src="<?php echo get_template_directory_uri()."/images/new/news-banner.png" ?>">
            </div>
            <div class="col-xs-12">
                <div class="row">
                    <?php $eventList = display_news_at_homepage() ?>
                    <?php foreach($eventList as $event){ ?>
                    <div class="col-xs-12 col-sm-6 col-lg-4">
                        <div class="group-event">
                            <div class="image-side">
<!--                                <img class="img-fluid" src="--><?php //echo $event['feature_image'] ?><!--" alt="--><?php //echo $event['title'] ?><!--">-->
                                <img class="img-fluid" src="<?php echo get_template_directory_uri()."/images/new/test-image.png" ?>" alt="<?php echo $event['title'] ?>">
                            </div>
                            <div class="content-side">
                                <a href="<?php echo $event['link'] ?>">
                                <p class="name"><?php echo $event['title'] ?></p>
                                </a>
                                <p class="description"><?php echo $event['short_description'] ?></p>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>

            </div>
            <div class="col-xs-12 text-center">
                <a href="/tin-tuc" class="all-event-cta">XEM TẤT CẢ</a>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>