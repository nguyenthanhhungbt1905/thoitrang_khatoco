<?php
//$user = wp_get_current_user();
//$updated = update_user_meta(wp_get_current_user()->ID, 'user_card',1); // update thẻ bac vào user id=2
get_header();
$block_id = 1;
?>

    <input type="hidden" id="refreshed" value="no">
    <script type="text/javascript">
        onload = function () {
            var e = document.getElementById("refreshed");
            if (e.value == "no")
                e.value = "yes";
            else {
                e.value = "no";
                location.reload();
            }
        }

       
    </script>

    <div class="banner">
        <?php echo get_the_post_thumbnail(get_the_id(), 'full', array('nopin' => 'nopin')); ?>
    </div>

    <div data-ng-app="khatocoCart" class="block-outer block-payment">
        <div data-ng-controller="CartController" class="block-inner">
            <div class="empty-cart" ng-show="isEmpty">
                <h5>GIỎ HÀNG RỖNG</h5>
                <p>Vui lòng chọn sản phẩm trước khi thanh toán.</p>
                <a class="empty-cart-btn" href="<?php echo get_home_url();?>">TRANG CHỦ</a>
                <img src="<?php echo get_template_directory_uri();?>/images/empty-cart-icon.png">
            </div>
            <form action="" method="" name="form">
                <input type="hidden" name="_checkout_nonce" id="_checkout_nonce"
                       value="<?php echo wp_create_nonce('_checkout_cart_ktc_nonce'); ?>">
                <input type="hidden" data-ng-model="logined" data-ng-value="<?php echo is_user_logged_in() ? 1 : 0; ?>">

                <div class="container block-cart fluid clearfix">
                    <div class="col-md-9">
                        <h2 class="title-cart">GIỎ HÀNG</h2>
                        <div>
                            <table class="cart-wrap list-prod-cart">
                                <thead>
                                <tr>
                                    <td class="col-md-4 col-xs-12"><span>Sản phẩm</span></td>
                                    <td class="col-md-2 col-xs-6"><span>Kích thước</span></td>
                                    <td class="col-md-2 col-xs-6"><span>Số lượng</span></td>
                                    <td class="col-md-2 col-xs-6"><span>Đơn giá</span></td>
                                    <td class="col-md-2 col-xs-6"><span>Tổng cộng<span></td>
                                </tr>
                                </thead>
                                <tbody>

                                <tr data-ng-repeat="(key, product) in cartInfo.products" class="clearfix">
                                    <td class="prod-col col-md-4 col-xs-12 clearfix">
                                        <img src="" class="col-xs-3" data-ng-src="{{product.thumb}}">
                                        <figure class="info col-xs-9">
                                            <div class="cat">{{product.cat_name}}</div>
                                            <a href="{{product.permalink}}" class="title">{{product.name}}</a>
                                            <div class="sale-off" ng-show="product.price_saleoff.percent > 0">Giảm: {{product.price_saleoff.percent}}%</div>
                                        </figure>
                                    </td>
                                    <td class="col-md-2 col-xs-6">
                                    	<label for="size" class="mobile">Kích thước</label>
                                        <select ng-change="updateCarts()" ng-model="product.size" ng-options="size as product.all_sizes_lbl[getIndexOfValue(product.all_sizes,size)] for size in product.all_sizes">
                                        <!--<select ng-change="updateCarts()" ng-model="product.size" ng-options="size as product.all_sizes_lbl[product.all_sizes.indexOf(size)] for size in product.all_sizes">-->
                                        <!--<select data-ng-model="product.size" ng-change="updateCarts()">
                                            <option data-ng-repeat="size in product.all_sizes" data-ng-selected="size == product.size">{{product.all_sizes_lbl[$index]}}</option>
                                        </select>-->
                                    </td>
                                    <td class="col-md-2 col-xs-6">
                                    	<label for="quantity" class="mobile">Số lượng</label>
                                    	<input type="number" min="1" data-ng-model="cartInfo.products[key].quantity" data-ng-change="updateCarts()"></td>
                                    <td class="col-md-2 col-xs-6 mobile-clear">
                                    	<label for="price" class="mobile">Đơn giá</label>
                                        <span class="org-price" ng-show="product.price_saleoff.value != product.price">{{product.price | vndCurrency }}</span>
                                        <span class="sale-off-price" ng-show="product.price_saleoff.value > 0">{{product.price_saleoff.value | vndCurrency }}</span>
                                    </td>
                                    <td class="col-md-2 col-xs-6">
                                    	<label class="mobile">Tổng cộng</label>
                                        {{product.quantity * product.price_saleoff.value | vndCurrency }}
                                        <span type="button" data-ng-click="removeFromCart(key)"
                                              title="Xóa khỏi giỏ hàng"
                                              class="glyphicon glyphicon-remove remove-prod">
                                              <p class="mobile">Xóa&nbsp;<i class="fa fa-times" aria-hidden="true"></i></p>
                                              <i class="fa fa-times" aria-hidden="true"></i></span>
                                    </td>
                                </tr>


                                <tr data-ng-show="!cartInfo.total" class="cart-empty">
                                    <td colspan="6">Chưa có sản phẩm nào trong giỏ hàng</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
						<h2 data-ng-show="campaignInfo.listSpecialPrice.total != undefined && campaignInfo.listSpecialPrice.total > 0">CHỌN {{campaignInfo.listSpecialPrice.total}} MUA GIÁ ƯU ĐÃI</h2>
                        <section class="block special-price"
                                 data-ng-show="campaignInfo.listSpecialPrice.total != undefined && campaignInfo.listSpecialPrice.total > 0">                            
                            <table class="list-prod-cart">
                                <thead>
                                <tr>
                                    <td class="col-md-4 col-xs-12"><span>Sản phẩm</span></td>
                                    <td class="col-md-2 col-xs-6"><span>Kích thước</span></td>
                                    <td class="col-md-2 col-xs-6"><span>Số lượng</span></td>
                                    <td class="col-md-2 col-xs-6"><span>Đơn giá</span></td>
                                    <td class="col-md-2 col-xs-6"><span>Tổng cộng<span></td>
                                </tr>
                                </thead>
                                <tbody>
                                <tr ng-repeat="sp in campaignInfo.listSpecialPrice.listProdBonus">
                                    <td class="prod-col col-md-4 col-xs-12 clearfix">
                                        <img src="" class="col-xs-3" ng-src="{{sp.thumb}}">
                                        <figure class="info col-xs-9">
                                            <div class="cat">{{sp.cat_name}}</div>
                                            <a href="#" class="title">{{sp.post_title}}</a>
                                        </figure>
                                    </td>
                                    <td class="col-md-2 col-xs-6">
                                    	<label for="size" class="mobile">Kích thước</label>
                                        <select data-ng-model="campaignInfo.listSpecialPrice.listProdBonus[$index].size" ng-change='countRemainSpecial($index)' ng-options="size as sp.all_size_label[sp.all_size.indexOf(size)] for size in sp.all_size">
                                        </select>

                                    </td>
                                    <td class="col-md-2 col-xs-6">
                                    	<label for="quantity" class="mobile">Số lượng</label>
                                        <input ng-change="countRemainSpecial($index)" type="number" min="0" ng-model="campaignInfo.listSpecialPrice.listProdBonus[$index].quantity">
                                    </td>
                                    <td class="col-md-2 col-xs-6">
                                    	<label for="price" class="mobile">Đơn giá</label>
                                    	{{sp.price | vndCurrency }}
                                    </td>
                                    <td class="col-md-2 col-xs-6">
                                    	<label class="mobile">Tổng cộng</label>
                                        {{sp.price*campaignInfo.listSpecialPrice.listProdBonus[$index].quantity |
                                        vndCurrency }}
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </section>
                                <div class="header-campaign fluid" data-ng-show="campaignInfo.listSpecialPrice.total > 0">
                                    <span class="col-md-8 col-xs-6">
                                        <h3>Tổng sản phẩm được chọn: {{campaignInfo.listSpecialPrice.total}}</h3>
                                    </span>
                                    <span class=" col-md-4 col-xs-6" ng-show="remainSpecial>=0">
                                        Còn lại: {{remainSpecial}}
                                    </span>
                                </div>						
						<h2 data-ng-show="campaignInfo.listBonus.total != undefined && campaignInfo.listBonus.total > 0">CHỌN {{campaignInfo.listBonus.total}} TẶNG PHẨM</h2>
                        <section class="block bonus" data-ng-show="campaignInfo.listBonus.listProdBonus != undefined && campaignInfo.listBonus.listProdBonus != null && campaignInfo.listBonus.listProdBonus.length > 0">
                            <table class="list-prod-cart">
                                <thead>
                                <tr>
                                    <td class="col-md-4 col-xs-12"><span>Sản phẩm</span></td>
                                    <td class="col-md-2 col-xs-6"><span>Kích thước</span></td>
                                    <td class="col-md-2 col-xs-6"><span>Số lượng</span></td>
                                    <td class="col-md-2 col-xs-6"><span>Đơn giá</span></td>
                                    <td class="col-md-2 col-xs-6"><span>Tổng cộng<span></td>
                                </tr>
                                </thead>
                                <tbody>
                                <tr ng-repeat="bonus in campaignInfo.listBonus.listProdBonus">
                                    <td class="prod-col col-md-4 col-xs-12 clearfix">
                                        <img src="" class="col-xs-3" ng-src="{{bonus.thumb}}">
                                        <figure class="info col-xs-9">
                                            <div class="cat">{{bonus.cat_name}}</div>
                                            <a href="#" class="title">{{bonus.post_title}}</a>
                                        </figure>
                                    </td>
                                    <td class="col-md-2 col-xs-6">
                                    	<label for="size" class="mobile">Kích thước</label>
                                        <select ng-change="countRemainBonus($index)" ng-model="bonus.size" ng-options="size as bonus.all_size_label[bonus.all_size.indexOf(size)] for size in bonus.all_size">
                                            <!--<option ng-repeat="size in bonus.all_size" ng-value="size">{{bonus.all_size_label[$index]}}</option>-->
                                        </select>

                                    </td>
                                    <td class="col-md-2 col-xs-6">
                                    	<label for="quantity" class="mobile">Số lượng</label>
                                        <input ng-change="countRemainBonus($index)" type="number" min="0" data-ng-model="campaignInfo.listBonus.listProdBonus[$index].quantity">
                                    </td>
                                    <td class="col-md-2 col-xs-6">
                                    	<label for="price" class="mobile">Đơn giá</label>
                                    	{{bonus.price | vndCurrency }}
                                    </td>
                                    <td class="col-md-2 col-xs-6">
                                    	<label class="mobile">Tổng cộng</label>
                                        {{bonus.price*campaignInfo.listBonus.listProdBonus[$index].quantity | vndCurrency }}
                                    </td>
                                </tr>                                
                                </tbody>
                            </table>
                        </section>
                        <div class="header-campaign fluid" data-ng-show="campaignInfo.listBonus.total > 0">
                                    <span class="col-md-8 col-xs-6">
                                        <h3>Tổng sản phẩm được chọn : {{campaignInfo.listBonus.total}}</h3>
                                    </span>
                                    <span class="col-md-4 col-xs-6" ng-show="remainBonus>=0">
                                        Còn lại: {{remainBonus}}
                                    </span>
                        </div>
                        <section class="block form-sale">
                            <h2>Hình thức thanh toán</h2>
                            <div class="clearfix">
                                <div class="col-md-4 type-payment">
                                    <input id="payment_method" type="hidden" name="" value="">

                                    <button type="button" data-ng-click="setSelectedPaymentMethod(1)"
                                            data-ng-class="{'selected-button': 1 == selectedPaymentMethod }" ng-disabled="disableInternalPayment">
                                        Thanh toán khi nhận hàng
                                    </button>
                                </div>
                                <div class="col-md-4 type-payment">
                                    <button type="button" data-ng-click="setSelectedPaymentMethod(2)"
                                            data-ng-class="{'selected-button': 2 == selectedPaymentMethod }">
                                        ATM Ngân hàng nội địa
                                    </button>
                                </div>
                                <div class="col-md-4 type-payment">
                                    <button type="button" data-ng-click="setSelectedPaymentMethod(3)"
                                            data-ng-class="{'selected-button': 3 == selectedPaymentMethod }">Visa/Master
                                        Card
                                    </button>
                                </div>
                            </div>
                        </section>

                        <section class="block transport">
                            <h2>Hình thức giao nhận</h2>
                            <div class="transport-methods-cart">
                                <p><strong>Hình thức vận chuyển</strong>: <span class="fr">Chuyển phát nhanh</span></p>
                                <p><strong>Đơn giá vận chuyển</strong>: <span class="fr price" data-ng-bind="selectedTransport.fee" style="text-decoration: line-through;"></span>
                                </p>
                                <p>Đơn hàng sẽ được chuyển phát nhanh miễn phí trên phạm vi cả nước</p>
                            </div>
                        </section>

                        <section class="block customer-info">

                            <h2>Thông tin khách hàng</h2>
                            <table class="info">
                                <tr>
                                    <td>
                                        <input type="text" name="text" value="{{userInfo.fullname}}"
                                               data-ng-model="userInfo.fullname" placeholder="Tên khách hàng"
                                               required="" class="form-control"></td>
                                </tr>
                                <tr>
                                    <td><input class="form-control" type="text" name="email" value="{{userInfo.email}}" data-ng-model="userInfo.email" placeholder="Email" required=""></td>
                                </tr>
                                <tr>
                                    <td><input class="form-control" type="text" name="phone" value="{{userInfo.phone}}" data-ng-model="userInfo.phone" placeholder="Số điện thoại" required="">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <select data-ng-model="datapost.province"
                                                data-ng-options="province.name for province in provinces track by province.provinceid"
                                                data-ng-change="setCustomerProvince()" name="province" id="province"
                                                class="form-control">
                                                <option value="">Tỉnh/TP</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="text" class="form-control" name="address"
                                               data-ng-model="userInfo.address" placeholder="Địa chỉ" required="">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="text" class="form-control" name="address"
                                               data-ng-model="checkoutNote" placeholder="Ghi chú" required="">
                                    </td>
                                </tr>

                            </table>
                        </section>

                        <label class="other-receive">
                            <input type="checkbox" class="form-control" ng-checked="otherReceiver" ng-change="otherReceiverClick()" ng-model="otherReceiver">
                            Người nhận hàng là người khác (chỉ áp dụng cho thanh toán online)
                        </label>

                        <label class="alert alert-danger" ng-show="otherReceiver">
                            <strong>Lưu ý</strong> : Chọn "Nhận hàng là người khác" bạn sẽ không được thanh toán khi nhận hàng
                        </label>

                        <section class="block customer-info customer-receive" ng-show="otherReceiver">

                            <h2>Thông tin khách hàng nhận hàng</h2>
                            <table class="info">
                                <tr>
                                    <td>
                                        <input type="text" name="text" value="{{receiver.fullname}}"
                                               data-ng-model="receiver.fullname" placeholder="Tên khách hàng"
                                               class="form-control"></td>
                                </tr>
                                <tr>
                                    <td><input class="form-control" type="text" name="email" value="{{receiver.email}}"
                                               data-ng-model="receiver.email" placeholder="Email"></td>
                                </tr>
                                <tr>
                                    <td><input class="form-control" type="text" name="phone" value="{{receiver.phone}}"
                                               data-ng-model="receiver.phone" placeholder="Số điện thoại">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <select data-ng-model="receiver.province"
                                                data-ng-options="province as province.name for province in provinces"
                                                data-ng-change="setReceiverProvince()" name="receiver-province"
                                                id="other-receive-province"
                                                class="form-control">
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="text" class="form-control" name="address" data-ng-model="receiver.address" placeholder="Địa chỉ">
                                    </td>
                                </tr>

                            </table>
                        </section>

                        <section class="block policy">
                            <h2>Điều khoản, điều kiện và quy chế đổi trả:</h2>
                            <div class="term-conditional popup close-wrap" ng-show="openCondition" id="popupTerms">
                            	<div class="close-bg"></div>
								<div class="popup-inner">
									<h2>Điều khoản, điều kiện và quy chế đổi trả</h2>
	                                <div class="the-content">
	                                    <?php echo apply_filters('the_content', get_option('term_and_conditional')); ?>	                                    
	                                </div>
	                                <button type="button" ng-click="openCondition = false">ĐÓNG</button>
	                                <a href="#close" class="close">x</a>	                                
                                </div>                                
                            </div>
                            <p><label for="pre_submit" style="cursor:pointer"><input type="checkbox" name="pre_submit"
                                                                                     id="pre_submit"
                                                                                     ng-model="pre_submit"
                                                                                     style="vertical-align:middle"> Tôi
                                    đã đọc và đồng ý với những điều khoản trên. <code ng-click="openCondition = true" id="viewTerms">Xem
                                        điều khoản</code></label></p>
                        </section>


                        <div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <section class="block sale-off-panel">
                            <h2>Mã khuyến mãi</h2>
                            <p>
                                <!-- <input type="text" class="text-verify" placeholder="Mã khuyến mãi" >
                                <button type="button" class="button-verify" id="button-verify" 
                                       >Xác nhận mã
                                </button> -->
                                <input type="text" class="text-verify" data-ng-model="coupon.code"
                                       placeholder="Mã khuyến mãi">
                                <button type="button" class="button-verify" data-ng-click="verify_coupon(coupon)"
                                        data-ng-show="!hasCoupon">Xác nhận mã
                                </button>
                            </p>
                            <p class="message {{verify_coupon_message_class}}" style="margin:13px 0 15px">
                                {{verify_coupon_message}}</p>
                             <p class="message {{verify_coupon_message_class}} messageEgift" style="margin:13px 0 15px">
                                </p>

                            <h2 class="member-card">Thẻ thành viên</h2>
                            <?php if (!is_user_logged_in()): ?>
                                <?php  echo 'Quý khách chưa tham gia chương trình thẻ thành viên Khatoco. Để biết thêm về chương trình, vui lòng xem thêm thông tin tại <a href="' . get_page_link(1057) . '" target="blank">đây</a>'; ?>
                               <!--  <p style="display: :none">
                                    <input type="text" class="text-verify" data-ng-model="member_identified"
                                           data-ng-disabled="isVip" data-ng-show="member_identified_verify_show"
                                           placeholder="Mã thẻ">
                                    <input type="text" class="text-verify" data-ng-model="member_identified_verify"
                                           data-ng-show="!member_identified_verify_show" placeholder="Mã xác nhận OTP">
                                    <button type="button" class="button-verify"
                                            data-ng-click="verify_member_identified(member_identified)"
                                            data-ng-show="member_identified_verify_show">Xác nhận mã thẻ
                                    </button>
                                    <button type="button" class="button-verify no-border"
                                            data-ng-click="verify_member_identified_otp(member_identified_verify)"
                                            data-ng-show="!member_identified_verify_show">Xác nhận mã OTP
                                    </button>
                                </p>
                                <p class="message" style="margin:13px 0 0;display: :none">{{verify_member_identified_message}}</p> -->
                                <?php
                            else: $user_id = get_current_user_id();
                                $card = get_user_meta($user_id, 'user_card', true);
                                $card_id = get_user_meta($user_id, 'user_card_id', true);
                                if ($card == '0' || $card == '') {
                                    echo 'Quý khách chưa tham gia chương trình thẻ thành viên Khatoco. Để biết thêm về chương trình, vui lòng xem thêm thông tin tại <a href="' . get_page_link(1057) . '" target="blank">đây</a>';
                                } else {
                                    echo 'Dữ liệu thẻ thành viên của Quý khách đã được áp dụng';
                                }
                            endif;
                            ?>
                        </section>

                        <section class="block billed">
                            <h2>Hóa đơn</h2>
                            <div class="total-orders clearfix">
                            	<p data-ng-show="!(cartInfo.total_sale_off > 0) && !(campaignInfo.listSpecialPrice.totalPrice && campaignInfo.listSpecialPrice.totalPrice > 0)">Thành tiền: <span>{{cartInfo.total_bill | vndCurrency}}</span></p>
                                <p data-ng-show="cartInfo.total_sale_off > 0">Giá gốc: <span>{{cartInfo.total | vndCurrency}}</span></p>
                                <p data-ng-show="cartInfo.total_sale_off > 0">Sau giảm giá: <span>{{cartInfo.total_sale_off | vndCurrency}}</span></p>
                                <p data-ng-show="campaignInfo.listSpecialPrice.totalPrice && campaignInfo.listSpecialPrice.totalPrice > 0">Mua thêm: <span>+ {{ campaignInfo.listSpecialPrice.totalPrice | vndCurrency }}</span>
                                </p>
                                <p data-ng-show="campaignInfo.listSpecialPrice.totalPrice && campaignInfo.listSpecialPrice.totalPrice > 0">Thành tiền: <span>{{cartInfo.total_bill | vndCurrency }}</span></p>
                                <!-- <p>Phí dịch vụ gói quà: <span data-ng-bind="formatCurrentcy(giftFeeTotal)"></span></p> -->
                                <p data-ng-show="isVip">{{vip_text}}: <span>Giảm {{(1 - vipAfter)*100 | number:0}}% {{vip_action}}</span>
                                </p>
                                <p data-ng-show="coupon && coupon.saleoff_val && coupon.saleoff_val > 0">Mã giảm giá: <span>- {{coupon.saleoff_val | vndCurrency}}</span></p>
                                <p data-ng-show="campaignInfo.decrease > 0">Khuyến mãi: <span>- {{ campaignInfo.decrease | vndCurrency }}</span>
                                    <button type="button" class="campaign-detail btn-campaign-detail" ng-click="showDetailCampaign()">Chi tiết</button>
                                </p>
                                <p data-ng-show="hasIncentive">Ưu đãi thẻ ({{incent_text}}): <span>Giảm {{incentive * 100}}%</span>
                                </p>
                                <p>Phí vận chuyển: <span>Miễn phí</span></p>                                
                                <p>Phải trả: <span>{{order_total | vndCurrency }}</span></p>
                            </div>
                        </section>

                        <section class="block pay">
                            <button type="button" ng-click="checkout()" class="condensed">Thanh toán</button>
                        </section>
                    </div>
                </div>
                <div id="updating" ng-show="updating">
                    <p>Đang tải dữ liệu ...</p>
                    <div id="cssload-pgloading">
                        <div class="cssload-loadingwrap">
                            <ul class="cssload-bokeh">
                                <li></li>
                                <li></li>
                                <li></li>
                                <li></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </form>

            <!--First checkOut information-->
            <div class="first-check-out" ng-show="isFirstCheckOut">
                <h1>THÔNG TIN ĐƠN HÀNG</h1>
                <div class="content clearfix">
                    <h2 class="col-md-12">SẢN PHẨM</h2>
                    <article class="product col-md-12" ng-repeat="(ID,product) in fco.cart.products">
                        <fieldset class="col lbl">+ {{product.name}}</fieldset>
                        <fieldset class="col lbl"><span>[ {{product.size}} ]</span></fieldset>
                        <fieldset class="col lbl"><span>{{product.quantity}} x {{product.price_saleoff.value|vndCurrency}}</span></fieldset>
                    </article>
                    <h2 ng-show="fco.cart.campaignInfo.listSpecialPrice.listProdBonus" class="col-md-12">SẢN PHẨM MUA VỚI GIÁ ƯU ĐÃI</h2>
                    <article class="product col-md-12" ng-repeat="(ID,product) in fco.cart.campaignInfo.listSpecialPrice.listProdBonus" ng-show="product.quantity>0">
                        <fieldset class="col lbl">+ {{product.post_title}}</fieldset>
                        <fieldset class="col lbl"><span>[ {{product.size}} ]</span></fieldset>
                        <fieldset class="col lbl"><span>{{product.quantity}} x {{product.price|vndCurrency}}</span></fieldset>
                    </article>

                    <h2 ng-show="fco.cart.campaignInfo.listBonus.listProdBonus" class="col-md-12">TẶNG PHẨM</h2>
                    <article class="product col-md-12" ng-repeat="(ID,product) in fco.cart.campaignInfo.listBonus.listProdBonus" ng-show="product.quantity>0">
                        <fieldset class="col lbl">+ {{product.post_title}}</fieldset>
                        <fieldset class="col lbl"><span>[ {{product.size}} ]</span></fieldset>
                        <fieldset class="col lbl"><span>{{product.quantity}} x 0 Đ</span></fieldset>
                    </article>

                    <article class="user-order col-md-6">
                        <h2>Người đặt hàng</h2>
                        <fieldset class="lbl">{{fco.customer.fullname}}</fieldset>
                        <fieldset class="lbl">{{fco.customer.email}}</fieldset>
                        <fieldset class="lbl">{{fco.customer.province.name}}</fieldset>
                        <fieldset class="lbl">{{fco.customer.address}}</fieldset>
                    </article>

                    <article class="user-receive col-md-6" ng-show="fco.otherReceiver">
                        <h2>Người nhận hàng</h2>
                        <fieldset class="lbl">{{fco.receiver.fullname}}</fieldset>
                        <fieldset class="lbl">{{fco.receiver.email}}</fieldset>
                        <fieldset class="lbl">{{fco.receiver.province.name}}</fieldset>
                        <fieldset class="lbl">{{fco.receiver.address}}</fieldset>
                    </article>

                    <div class="total-bill col-md-12">
                        <fieldset class="lbl">Ghi chú : {{fco.checkoutNote}}</fieldset>
                        <fieldset class="lbl">Thanh toán : {{fco.paymentMethod|paymentLbl}}</fieldset>


                        <h2>BIỂU PHÍ</h2>
                        <fieldset class="lbl" data-ng-show="cartInfo.total_sale_off > 0">Tổng tiền mua sản phẩm: <span>{{fco.orderInfo.totalOrg|vndCurrency}}</span></fieldset>
                        <fieldset class="lbl" data-ng-show="cartInfo.total_sale_off > 0">Sau giảm giá: <span>{{cartInfo.total_sale_off | vndCurrency}}</span></fieldset>
                        <fieldset class="lbl" ng-show="fco.orderInfo.totalPlus && fco.orderInfo.totalPlus > 0">Tổng tiền mua thêm sản phẩm: + <span>{{fco.orderInfo.totalPlus|vndCurrency}}</span></fieldset>
                        <fieldset class="lbl">Thành tiền: <span>{{fco.orderInfo.totalBill|vndCurrency}}</span></fieldset>
                        <fieldset class="lbl">Phí vận chuyển: <span>{{fco.orderInfo.transportFee|vndCurrency}}</span></fieldset>
                        <fieldset class="lbl" ng-show="fco.orderInfo.decreaseCoupon && fco.orderInfo.decreaseCoupon>0">Mã giảm giá: - <span>{{fco.orderInfo.decreaseCoupon|vndCurrency}}</span></fieldset>
                        <fieldset class="lbl" ng-show="fco.orderInfo.decreaseCampaign && fco.orderInfo.decreaseCampaign>0">Chương trình khuyến mãi: - <span>{{fco.orderInfo.decreaseCampaign|vndCurrency}}</span></fieldset>
                    </div>

                    <fieldset class="final-bill col-md-12">Bạn sẽ thanh toán: <span>{{fco.orderInfo.totalOrder|vndCurrency}}</span></fieldset>
                    <fieldset class="submit col-md-12">
                        <button class="empty-cart-btn" ng-click="backCheckout()">QUAY LẠI</button>
                        <button class="empty-cart-btn" ng-click="checkout()">TIẾP TỤC THANH TOÁN</button>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>


    <style type="text/css">
        .selected-button {
            font-weight: bold;
        }

        #updating {
            position: fixed;
            display: table;
            z-index: 999999;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            width: 100%;
            height: 100%;
            background-color: rgba(26, 29, 33, 1);
        }

        #updating p {
            display: table-cell;
            width: 100%;
            height: 100%;
            vertical-align: middle;
            text-align: center;
            color: #fff;
        }

        #updating i {
            display: block;
            margin-bottom: 20px;
            font-size: 30px;
            color: #fff;
        }
    </style>

    <script>
        jQuery(document).ready(function ($) {
            $('#login_over_here').click(function (event) {
                event.preventDefault();
                $('html, body').animate({scrollTop: 0}, 500);
                $('#user_login_btn').click();
                $('#lg_email').focus();
            });
        });
    </script>

<?php
get_footer();
/* echo '<pre>';
  print_r($_SESSION); */
?>