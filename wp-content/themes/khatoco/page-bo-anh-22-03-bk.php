<?php 
/*
Template Name: Bộ ảnh
*/
get_header(); ?>
<div class="block-outer block-collection">
	<div class="container fluid clearfix">
		<div class="block-inner clearfix"><?php $args = array('hide_empty' => 0, 'number' => 4);
		$c_terms = get_terms('bo-suu-tap', $args);
		foreach ($c_terms as $t_key => $t):
			$class = 'grid6';
			if($t_key == 0) $class = 'grid12 high';
			elseif($t_key == 1) $class .= ' normal';
			else $class .= ' low';
			$img_src = wp_get_attachment_image_src(get_single_taxonomy_meta('cat-description', $t->term_id, 'collection-bg-img'), 'full'); ?>
			<div class="item-collection-outer <?php echo $class; ?>">
				<div nopin="nopin" class="item-collection-inner" style="background: url('<?php echo $img_src[0]; ?>') center center no-repeat; background-size: cover;" onclick="window.location.replace('<?php echo get_term_link($t, 'bo-suu-tap'); ?>');">
					<div class="intro">
						<h3 class="condensed"><a href="<?php echo get_term_link($t, 'bo-suu-tap'); ?>"><?php echo $t->name; ?></a></h3>
					</div>
				</div>
			</div>
		<?php endforeach; ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>