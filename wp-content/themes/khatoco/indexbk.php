<?php get_header(); ?>
<div class="home-slide-holder clearfix">
	<div class="home-slide clearfix">
		<?php get_banner_at('trang-chu'); ?>
	</div>
</div>

<div class="home-intro">
<!--
	<div class="container">
		<div class="background-intro">
			<div class="left-intro">
				<a href="<?php echo get_option('banner-link1'); ?>" class="thumb-intro-stand">
				    <img src="<?php echo home_url() . '/wp-content/uploads/' . get_option('banner1'); ?>" alt="">
			    </a>
			</div>
			<div class="right-intro">
				<?php for ($i=2; $i < 6; $i++) { 
					$banner = get_option('banner'.$i);
					$cl = ($i==3 || $i==4) ? 'thumb-intro-small' : 'thumb-intro-big';
				?>
				<a href="<?php echo get_option('banner-link'.$i); ?>" class="<?php echo $cl; ?>">
				    <img src="<?php echo home_url() . '/wp-content/uploads/' . $banner; ?>" alt="">
			    </a>			     
				<!-- <a href="/dang-ky/" class="thumb-intro-small">
	         		<img src="<?php echo get_template_directory_uri(); ?>/images/intro3.jpg" alt="">
	        	</a>

	        	<a href="/tin-tuc/thoi-trang-nam-khatoco-nhan-danh-hieu-thuong-hieu-quoc-gia-nam-2016/" class="thumb-intro-small">
	         		<img src="<?php echo get_template_directory_uri(); ?>/images/intro4.jpg" alt="">
	        	</a>
				<a href="/chuong-trinh-thanh-vien-khatoco/" class="thumb-intro-big">
				    <img src="<?php echo get_template_directory_uri(); ?>/images/intro5.jpg" alt="">
			    </a>
			    <?php } ?>
			</div><div class="clear-fix"></div>
		</div>
	</div>
	-->
</div>

<div class="block-outer block-home-product">
	<div class="block-inner">
		<div class="containerpd clearfix">
			<div class="block-title-outer"><h2 class="block-title block-title-inner condensed">Sản phẩm nổi bật</h2></div>
			<div class="block-description-outer clearfix">Những sản phẩm đang được ưa chuộng tại Khatoco</div>
		</div>
		<div class="block-content container clearfix fluid product-home-slide">
			<?php
                $homeHotProducts = aj_get_home_posts(0, 16, 'hot');
                $index = 0;
                while ($homeHotProducts->have_posts()):
                	$index ++;
                	$homeHotProducts->the_post();
                    aj_display_each_post_home($index);
                endwhile;
                wp_reset_postdata(); 
                if ($index & 1){
                	echo '</div>';
                }
                ?>
                
		</div>
	</div>
</div>

<div class="block-outer block-home-cat">
	<div class="block-inner">
		<div class="containerpd clearfix">
			<div class="block-title-outer"><h2 class="block-title block-title-inner condensed">Mua sắm theo danh mục sản phẩm</h2></div>
		</div>
		<div class="block-content container clearfix fluid home-cat-slide"><?php $args = array('number' => 6, 'parent' => 0, 'hide_empty' => 0, 'orderby' => 'id');
		$terms = get_categories($args);
		$i = 0;
		foreach ($terms as $key => $term):
			if($i < 5) {?>
			<div class="item col-md-12 col-xs-6 nopadding">
				<div class="item-outer">
					<div class="item-inner">
						<a href="<?php echo get_category_link($term); ?>" id="category_<?php echo $term->term_id; ?>"><?php $img_src = wp_get_attachment_image_src(get_single_taxonomy_meta('product-cat-description', $term->term_id, 'product-cat-img'), 'full'); ?>
							<span class="item-thumb">
								<img nopin="nopin" src="<?php echo $img_src[0]; ?>" alt="">
							</span>
						</a>
					</div>
					<span class="item-title condensed"><?php echo $term->name; ?></span>
				</div>
			</div>
			<?php 
			} 
			$i++;
		endforeach; ?>
		</div>
	</div>
</div>

<div class="block-outer block-home-collection">
	<div class="block-inner">
		<div class="containerpd clearfix">
			<div class="block-title-outer"><h2 class="block-title block-title-inner condensed">Bộ sưu tập</h2></div>
		</div>
		<div class="block-content container clearfix fluid"><?php $terms = get_terms('bo-suu-tap', array('hide_empty' => 0, 'number' => 3));
		foreach ($terms as $term): ?>
			<div class="row-fluid">
			<div class="col-xs-12 col-md-4 nopadding">
				<div class="item-outer">
					<div class="item-inner">
						<a href="<?php echo get_term_link($term, 'bo-suu-tap'); ?>">
							<span class="item-thumb"><?php $img_src = wp_get_attachment_image_src(get_single_taxonomy_meta('cat-description', $term->term_id, 'cat-index-bg-img'), 'full'); ?>
								<img src="<?php echo $img_src[0]; ?>" nopin="nopin" />
							</span>
							<h3 class="item-title condensed"><?php echo $term->name; ?></h3>
						</a>
					</div>
				</div>
			</div>
			</div><?php endforeach; ?>
		</div>
	</div>
</div>

<div class="block-outer block-home-style-guide">
	<div class="block-inner">
		<div class="block-title-outer"><h2 class="block-title block-title-inner condensed">Style Guide</h2></div>
			<div><?php $args = array('posts_per_page' => 1, 'post_type' => 'style-guide', 'style-guide-category' => 'xu-huong-thoi-trang', 'orderby' => 'date', 'order' => 'DESC');
			$lastest_styleguide_post = get_posts($args);
			$lastest_styleguide_post = $lastest_styleguide_post[0]->ID; ?>
				<img nopin="nopin" style="display: block; margin: 0 auto; cursor: pointer; width: 100%; max-width: 100%" src="<?php echo get_template_directory_uri(); ?>/assets/img/samples/home-cover.jpg" onclick="window.location.replace('<?php echo get_permalink($lastest_styleguide_post); ?>');" />
			<?php unset($lastest_styleguide_post); ?></div>
		</div>
	</div>
</div>

<div class="block-outer block-home-news-video">
	<div class="block-inner container fluid">
		<?php 
/*		<div class="grid6 block-home-video">
			<h2 class="block-title condensed">Video Clip</h2>
			<div class="block-content"><?php $args = array('posts_per_page' => 4, 'post_type' => 'video');
			$v_list = get_posts($args);
			$v_i = 1;
			foreach ($v_list as $vkey => $v):
				$v_src = get_post_meta($v->ID, 'video_youtube_code', true);
				// echo $v_src;
				if($v_i == 1): ?>
				<div class="video-display video-click posr clearfix">
					<?php echo get_the_post_thumbnail($v->ID, 'full'); ?>
					<span class="play-button"></span>
					<span class="video-unclick" data-src="<?php echo $v_src; ?>"></span>
				</div><?php else:
				if($v_i == 2) echo '<div class="video-related clearfix"><ul>'; ?>
						<li class="item-outer grid4">
							<div class="item-inner">
								<div class="item-thumb video-click posr">
									<a href="" class="">
										<?php echo get_the_post_thumbnail($v->ID, array(169, 183)); ?>
									</a>
									<span class="play-button"></span>
									<span class="video-unclick" data-src="<?php echo $v_src; ?>"></span>
								</div>
								<h3 class="item-title video-click">
									<a href=""><?php echo $v->post_title; ?></a>
									<span class="video-unclick" data-src="<?php echo $v_src; ?>"></span>
								</h3>
							</div>
						</li>
				<?php endif;
				$v_i++;
			endforeach;
			echo '</ul></div>'; ?>
			</div>
		</div>
 */?>
		<h2 class="block-title condensed">Tin tức</h2>
		<div class="block-content home-news-slide">
				<?php $args = array(
			        'post_type' => 'news',
			        'orderby' => 'date',
			        'order' => 'DESC',
			        'posts_per_page' => 4
			    );

			    $home_news = get_posts( $args );

                foreach ($home_news as $hnkey => $hn) {
                	$class = ($hnkey % 2 == 0) ? 'even' : 'odd';
                    aj_display_each_home_news($hn, $class);
                } ?>
		</div>
	</div>
</div>

<!-- <div id="video-popup" class="hidden">
	<div class="video-popup-inner">
		<iframe width="595" height="395" src="" frameborder="0" allowfullscreen></iframe>
		<span class="close">x</span>
	</div>
</div> -->


<?php 
/*	
<div class="block-outer block-social">
	<div class="block-inner">
		<div class="containerpd clearfix">
			<div class="block-title-outer"><h2 class="block-title block-title-inner condensed">Kết nối mạng xã hội</h2></div>
			<div class="block-description-outer clearfix">hiểu biết thêm về thương hiệu thời trang khatoco trong lòng cộng đồng mạng</div>
		</div>
		<div class="block-content container fluid">
			<div class="grid3 fl youtube-block social-block">
				<div class="item-outer">
					<div class="item-title">
						<p><a href="https://www.youtube.com/channel/UCipoobgi12E4u51tM2QWsVA" target="_blank" rel="publisher"><img nopin="nopin" src="<?php echo get_template_directory_uri(); ?>/images/youtube-logo.png" alt=""></a></p>
					</div>
					<div class="item-content clearfix">
						<div class="item-content-inner"><?php $yts = get_yt_feeds();
						$yt_a = array();
						foreach ($yts['items'] as $key => $yt) {
						 	if(isset($yt['contentDetails']['upload']))
						 		$yt_a[]['id'] = $yt['contentDetails']['upload']['videoId'];
						}
						if(!is_null($yt_a) || !empty($yt_a)) {
							foreach ($yt_a as $key => $ytId) {
								$yt_details = get_video_details($ytId['id']);
								$content = file_get_contents('http://gdata.youtube.com/feeds/api/videos/'. $ytId['id'] .'?fields=title&format=5&alt=json');
								$content = json_decode($content, true);
								$yt_a[$key]['title'] = $content['entry']['title']['$t'];
								$yt_a[$key]['statistics'] = $yt_details['modelData']['items'][0]['statistics'];
							}
						}
						foreach($yt_a as $yt): ?>
							<div class="row yt-link clearfix">
								<p><a href="<?php echo 'http://youtube.com/watch?v='. $yt['id']; ?>" data-src="<?php echo $yt['id']; ?>" target="_blank"><img nopin="nopin" src="<?php echo 'http://img.youtube.com/vi/'. $yt['id'] .'/0.jpg'; ?>"></a></p>
								<h3><a href="<?php echo 'http://youtube.com/watch?v='. $yt['id']; ?>" data-src="<?php echo $yt['id']; ?>" target="_blank"><?php echo $yt['title']; ?></a></h3>
								<p style="padding-bottom: 10px"><?php echo $yt['statistics']['viewCount'] .' lượt xem'; ?></p>
							</div>
						<?php endforeach; ?></div>
					</div>
				</div>
			</div>
			<div id="fb-block" class="grid3 fl fb-block social-block">
				<div class="item-outer" style="overflow: hidden">
					<div class="item-title">
						<p><a href="http://facebook.com/thoitrangkhatoco" target="_blank" rel="publisher"><img src="<?php echo get_template_directory_uri(); ?>/images/fb-logo.png" alt=""></a></p>
					</div>
					<div class="item-content clearfix">
						<div class="item-content-inner">
							<div class="fb-page" data-href="https://www.facebook.com/thoitrangkhatoco" data-witdh="280" data-hide-cover="true" data-show-facepile="false" data-show-posts="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/thoitrangkhatoco"><a href="https://www.facebook.com/thoitrangkhatoco">Thoi Trang Khatoco</a></blockquote></div></div>
						</div>
					</div>
				</div>
			</div>
			<div class="grid3 fl ggplus-block social-block">
				<div class="item-outer" style="overflow: hidden">
					<div class="item-title">
						<p><a href="https://plus.google.com/101732736942318540751" target="_blank" rel="publisher"><img src="<?php echo get_template_directory_uri(); ?>/images/ggplus-logo.png" alt=""></a></p>
					</div>
					<div class="item-content clearfix">
						<div class="item-content-inner"><?php $gps = get_gp_feeds();
						$gp_a = array();

						foreach($gps['modelData']['items'] as $gp) {
							$gp_a[] = array(
								'actor' => $gp['actor'],
								'title' => isset($gp['title']) ? $gp['title']:'',
								'content' => $gp['object']['content'],
								'link' => isset($gp['url']) ? $gp['url']:'',
								'type' => isset($gp['object']['type']) ? $gp['object']['type']:'',
								'date_updated' => isset($gp['updated']) ? date_format(new DateTime($gp['updated']), 'M d, Y'):date_format(new DateTime($gp['published']), 'M d, Y'),
							);
						}
						foreach ($gp_a as $key => $gp): ?>
							<div class="row clearfix">
								<div class="clearfix">
									<a class="fl" href="<?php echo $gp['link']; ?>" target="_blank"><img src="<?php echo $gp['actor']['image']['url']; ?>" style="width: 60px; height: 60px; background-color: #fff; border-radius: 100%"></a>
									<div class="fl" style="padding-left: 5px">
										<h4><a href="<?php echo $gp['link']; ?>" target="_blank"><?php echo 'Thời trang Khatoco';//$gp['actor']['displayName']; ?></a></h4>
										<p><span class="date"><i class="fa fa-calendar"></i> <?php echo $gp['date_updated']; ?></p>
									</div>
								</div>
								<div class="the_content clearfix">
									<?php echo $gp['title']; ?>
									<?php //echo wp_trim_words($gp['content'], 15, '...'); ?>
								</div>
								<div class="gp_action clearfix">
								</div>
							</div>
						<?php endforeach; ?>
						</div>
					</div>
				</div>
			</div>
			<div class="grid3 fl pinterest-block social-block">
				<div class="item-outer" style="overflow: hidden">
					<div class="item-title">
						<p><a href="http://pinterest.com/khatoco/"><img src="<?php echo get_template_directory_uri(); ?>/images/pinterest-logo.png" alt=""></a></p>
					</div>
					<div class="item-content clearfix">
						<div class="item-content-inner"><?php $pin_lst = json_decode(curl_get_content(), true);
						$pin_a = array();
						foreach($pin_lst['data']['pins'] as $pin) {
							$pin_a[] = array(
								'img' => $pin['images']['237x']['url'],
								'link' => 'http://pinterest.com/pin/'. $pin['id'],
							);
						}
						foreach ($pin_a as $key => $pin): ?>
							<div class="row clearfix">
								<div class="clearfix">
									<a href="<?php echo $pin['link']; ?>" target="_blank"><img src="<?php echo $pin['img'] ?>" alt=""></a>
								</div>
								<div class="pin_action clearfix">
									
								</div>
							</div>
						<?php endforeach; ?></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
 */?>
<!--
<?php $home_popup = get_option('home-popup');
$home_popup_link = get_option('home-popup-link');
if( $home_popup_link != '' && !is_user_logged_in() ): ?>
    <div class="popup close-wrap" id="popup-link">
        <div class="close-bg"></div>
		<div class="clearfix posr">
			<a href="<?php echo $home_popup_link; ?>">
				<img nopin="nopin" src="<?php echo timevn_attachment_src( $home_popup ); ?>" alt="">
			</a>
			<div class="close">x</div>
		</div>
    </div>
<?php endif; ?>
-->
<?php get_footer(); ?>