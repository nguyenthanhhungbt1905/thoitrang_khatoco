<?php
require_once(get_template_directory() . '/includes/product/ProductUtils.php');
if (!isset($_SESSION)) {
    session_start();
}
get_header();
?>
<div class="tax-slide-holder">
    <div class="tax-slide">
<?php get_banner_at('chi-tiet-san-pham'); ?>
    </div>
</div>
<div class="block-outer block-tax" id="block_san_pham">
    <div class="product-single block-inner container clearfix fluid">
        <div class="grid12 fl block-main">
            <div class="top-block clearfix">
                <div class="breadcrumb fl">
<?php blix_breadcrumbs(); ?>
                </div>
            </div>
            <div class="block-content block-mb-50 clearfix">
                <!--				<div class="block-filter-product clearfix">
                                                        <form action="<?php echo get_category_link(1); ?>" method="GET" id="form-filter">
                                                                <div class="categories-filter">
                                                                        <select class="fl form-record root-record" name="form[]" id="dong-san-pham" data-order="1">
                                                                                <option value="">Loại sản phẩm</option><?php
                $args = array('hide_empty' => 0, 'parent' => 0, 'orderby' => 'id');
                $cats_p = get_categories($args);
                foreach ($cats_p as $cat_p) {
                    echo '<option value="' . $cat_p->slug . '" data-id="' . $cat_p->term_id . '" ' . (isset($_GET['form'][0]) ? selected($_GET['form'][0], $cat_p->slug) : '') . '>' . $cat_p->name . '</option>';
                }
                ?>

                -->

                <div class="block-single-product clearfix">
                    <div class="product-preview grid6 fl pl0"><?php
                        $product_colors = get_post_meta($post->ID, 'post_colors', true);
                        foreach ($product_colors as $c_key => $c) {
                            $product_imgs = explode(',', $c['images_ids']);
                            $html_full = $html_thumb = '';
                            foreach ($product_imgs as $key => $img_id) {
                                $img_src = wp_get_attachment_image_src($img_id, 'large');
                                $html_full .= '<li class="elevate"><img src="' . $img_src[0] . '" alt="" data-zoom-image="' . $img_src[0] . '"></li>'; //style="background-color: rgba(0, 0, 0, 0.05)
                                $img_src = wp_get_attachment_image_src($img_id, array(80, 70));
                                $html_thumb .= '<li><img src="' . $img_src[0] . '" alt=""></li>';
                            }
                            ?>
                            <div class="block-content clearfix block-product-preview" data-color="<?php echo $c_key; ?>">
                                
                                <div class="thumbnail-preview-holder grid2 fl pl0 posr">
                                    <ul class="thumbnail-preview list-style-none clearfix">
    <?php echo $html_thumb; ?>
                                    </ul>
                                </div>
                                
                                <div class="preview-image-holder grid10 fl pr0">
                                    <ul class="preview-image list-style-none">
    <?php echo $html_full; ?>
                                    </ul>
                                </div>
                                
                            </div><?php } ?>
                    </div>
                    <script type="text/javascript">
                        (function () {
                            var w = jQuery(window).width();
                            jQuery(window).resize(function () {
                                w = jQuery(window).width();
                            });
                            if (w < 770)
                                return false;
                            jQuery('.elevate img').elevateZoom({
                                responsive: true,
                                cursor: "crosshair",
                                borderColour: '#c61d20',
                                lensColour: 'white',
                                zoomWindowWidth: jQuery('.preview-image-holder').width(),
                                zoomWindowHeight: 600,
                                zoomWindowOffetx: 10
                            });
                        })();
                    </script>
                    <div class="product-details item-for-cart grid6 fl pr0 pl0">
                        <div class="block-content">
                            <h2 class="condensed">
                                <span class="cat"><?php echo wp_get_post_categories($post->ID,array('fields'=> 'names'))[0];?></span>
                                <span class="title"><?php echo $post->post_title; ?></span>
                            </h2>

                            <div class="block-social clearfix">
                                <div class=" single-social google"><div class="g-plusone" data-size="medium" data-annotation="bubble"></div></div>
                                <div class="fb-like single-social facebook clearfix" data-href="<?php echo get_permalink($post->ID); ?>" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"></div>
                                <div class="pinterest single-social pinterest">
                                    <a href="http://www.pinterest.com/pin/create/button/?url=<?php echo urlencode(get_permalink($post->ID)); ?>" data-pin-do="buttonPin" data-pin-config="beside">
                                        <img src="http://assets.pinterest.com/images/pidgets/pin_it_button.png" />
                                    </a>
                                </div>
                            </div>
                            <div class="product-metadata clearfix">
                                <div class="item-price">

                                    <?php
                                    $saleOff = ProductUtils::getProductSaleOffPrice($post->ID);
                                    $price = get_post_meta($post->ID, 'post_price', true);
                                    if ($saleOff['value'] < $price) {
                                        echo '<span style="color: #655; text-decoration: line-through">' . aj_format_number($price) . '</span>&nbsp;&nbsp;';
                                        echo aj_format_number($saleOff['value']);
                                        echo '<p class="percent">Giảm giá : '.$saleOff['percent'].'%</p>';
                                    } else
                                        echo aj_format_number($price);
                                    ?>


                                </div>
                                <div class="support-choose">
                                    <a href="<?php echo home_url('/cau-hoi-thuong-gap/thong-so-san-pham/'); ?>"  target="_blank"><i class="fa fa-book" aria-hidden="true"></i> Hướng dẫn chọn size</a>
                                    <a href="#" id="calSize" data-toggle="modal" data-target="#calSizeModal"><i class="fa fa-building" aria-hidden="true"></i> Tính toán size</a>
                                </div>
                                <div class="choose-product clearfix">
                                        <?php
                                        $product_sizes = get_post_meta($post->ID, 'post_sizes', true);
                                        $onhands = get_post_meta($post->ID, 'post_onhand', true);
//                                         if ($onhands == '')
//                                             $onhands = array();
                                        $all_sizes_support = aj_get_all_shirt_sizes() + aj_get_all_pants_sizes();
                                        // $all_sizes_support = array_reverse( $all_sizes_support, true );

                                        if ($onhands){ $size_default = ''; ?>
                                        <select name="product-size" id="item-size" class="col-xs-9 item-size dropkick_target form-control"><?php
                                            $index = 0;
                                            $count = 0;
                                            foreach ($all_sizes_support as $sk => $sizename) {
                                                if (isset($onhands[$sk]) && $onhands[$sk] > 0) {
                                                    if ($index == 0)
                                                        $size_default = 'S';
                                                    $index++;

                                                    $count = $onhands[$sk];
                                                    echo '<option value="' . $sk . '" data-onhand="' . $count . '">' . $sk . ' (còn ' . $count . ' sản phẩm)</option>';
                                                }
                                            }
                                            ?>
                                        </select> 
                                        <?php
                                            $isProductSizeAvailable = $index; // Sản phẩm còn size hay không ? = 0 là hết
                                            if (empty($isProductSizeAvailable)){
                                                echo '<style>';
                                                echo '#add_to_cart, #add_to_wishlist ,#item-size,.item-amount{display : none}';
                                                echo '</style>';
                                                echo '<p class="no-product">Hết hàng</p>';
                                            }

                                        } else {
                                            echo '<input type="hidden" name="product-size" value="M">';
                                            echo '<style>';
                                            echo '#add_to_cart, #add_to_wishlist ,#item-size,.item-amount{display : none}';
                                            echo '</style>';
                                            echo '<p class="no-product">Hết hàng</p>';
                                        }
?>
                                    <input type="hidden" name="product-id" value="<?php echo get_the_id(); ?>">
                                    <fieldset class="item-amount col-xs-5 clearfix">
                                        <span class="col-xs-2 change-ia decrease" unselectable="on">-</span>
                                        <input type="text" name="product-amount" id="item-amount" disabled="" class="form-control col-xs-8">
                                        <span class="col-xs-2 change-ia increase" unselectable="on">+</span>
                                        <span class="error"></span>
                                    </fieldset>
                                </div>
                                <!--								<div class="row grid12 pl0 pr0">
                                                                                                        <div class="grid3 fl pl0 pr0"><p>Mã số</p></div>
                                                                                                        <div class="grid9 fl pr0"><p class="item-id"><?php /*echo get_post_meta($post->ID, 'post_sku', true);*/ ?></p></div>
                                                                                                </div>
                                                                                                <div class="row grid12 pl0 pr0">
                                                                                                        <div class="grid3 fl pl0 pr0"><p>Giá</p></div>
                                                                                                        <div class="grid9 fl pr0">
                                                                                                                <p class="item-price condensed"><?php
$saleoff_info = get_saleoff_info($post->ID);
$price = get_post_meta($post->ID, 'post_price', true);
if ($saleoff_info) {
    echo '<span style="color: #655; text-decoration: line-through">' . aj_format_number($price) . '</span>&nbsp;&nbsp;';
    echo $saleoff_info['new_price_formated'];
    echo '<p>(Giảm giá ' . $saleoff_info['sale_txt'] . ')</p>';
} else
    echo aj_format_number($price);
?>
                                                                                                                </p>
                                                                                                        </div>
                                                                                                </div>-->
<?php ?>
                                <!--								<div class="row grid12 pl0 pr0 hidden">
                                                                                                        <div class="grid3 fl pl0 pr0"><p>Màu sắc</p></div>
                                                                                                        <div class="grid9 fl pr0">
                                        <?php $color_id = ''; ?>
                                                                                                                <p class="item-color clearfix product-color" id="item-color"><?php foreach ($product_colors as $c_key => $c) {
                                            $color_id = $c['color_id'];
                                            ?>
                                                                                                                            <span class="fl color" style="background-color: <?php echo $c['code']; ?>" title="<?php echo $c['name']; ?>" data-color-id="<?php echo $c['color_id']; ?>" data-color="<?php echo $c_key; ?>"></span>
                                        <?php } ?></p>
                                                                                                        </div>
                                                                                                </div>-->
                                <div class="row grid12 pl0 pr0">

                                    <div class="grid9 fl pr0">

                                        &nbsp;

                                        <?php /* ?>
                                          <div class="info_size">
                                          <a href="javascript:void(0);">info size</a>
                                          <div class="info_size_box">
                                          <table>
                                          <thead>
                                          <tr>
                                          <td>Size</td>
                                          <td>S</td>
                                          <td>M</td>
                                          <td>L</td>
                                          <td>XL</td>
                                          </tr>
                                          </thead>
                                          <tbody>
                                          <tr>
                                          <td>Vòng cổ</td>
                                          <td>7</td>
                                          <td>38</td>
                                          <td>39</td>
                                          <td>40</td>
                                          </tr>
                                          <tr>
                                          <td>Vòng ngực</td>
                                          <td>102</td>
                                          <td>107</td>
                                          <td>107</td>
                                          <td>113</td>
                                          </tr>
                                          <tr>
                                          <td>Dài Thân Sau</td>
                                          <td>76</td>
                                          <td>77</td>
                                          <td>77</td>
                                          <td>78</td>
                                          </tr>
                                          <tr>
                                          <td>Dài Đô</td>
                                          <td>44</td>
                                          <td>46</td>
                                          <td>46</td>
                                          <td>48</td>
                                          </tr>
                                          <tr>
                                          <td>Dài tay</td>
                                          <td>57</td>
                                          <td>59</td>
                                          <td>59</td>
                                          <td>60.5</td>
                                          </tr>
                                          </tbody>
                                          </table>
                                          </div>
                                          </div>
                                          <?php */ ?>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix">
                                <p>
                                    <a id="add_to_cart" data-product-id="<?php echo get_the_id(); ?>" data-color-id="<?php echo $color_id; ?>" data-color-number="0" data-size="<?php echo $size_default; ?>" data-quantity="1" class="add-to-cart btn" href="javascript:void(0)"><i class="fa fa-shopping-cart" style="margin-right: 5px;"></i>Thêm vào giỏ hàng</a>
            <?php if (get_current_user_id() > 0): ?>
                                        <a id="add_to_wishlist" class="add-to-wishlist btn <?php echo (is_favorite_product(get_the_id())) ? 'fav' : ''; ?>" data-product-id="<?php echo get_the_id(); ?>" href="javascript:void(0);">
                                            <i class="fa fa-heart" style="margin-right: 5px;"></i>
                <?php
                if (is_favorite_product(get_the_id()))
                    echo 'Bỏ yêu thích';
                else
                    echo 'Thêm vào ưa thích';
                ?>
                                        </a>
<?php endif; ?>
                                </p>
                            </div>
                            <div class="clearfix">
                                <p><i>Nếu bạn có nhu cầu chọn mua sản phẩm này với số lượng lớn làm đồng phục, vui lòng liên hệ chúng tôi để có giá sản phẩm tốt nhất. Hotline: <?php echo get_option('hotline'); ?></i></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<?php /* ?>
  <div class="block-single-product-more block-accessories">
  <h2 class="condensed">Sản phẩm phối đồ</h2>
  <?php display_product_accessories(get_the_id()); ?>
  </div>
  <?php */ ?>

            <div class="more-detail clearfix">
<?php display_product_more_info(get_the_id()); ?>
            </div>

            <div class="block-single-product-more block-related">
                <h2 class="condensed">Sản phẩm tương tự</h2>
                <span class="line"></span>
<?php display_related_products(get_the_id()); ?>
            </div>
            <div class="block-single-product-more block-products-viewed">
                <h2 class="condensed">Mới vừa xem</h2>
                <span class="line"></span>
<?php echo get_products_just_seen(); ?>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(window).load(function () {
        jQuery('#add_to_cart')
                .attr('data-quantity', jQuery('#item-amount').val())
                .attr('data-size', jQuery('#item-size').val());
    });

    jQuery(document).ready(function ($) {

        // Scroll down
        if (jQuery(window).width() > 1096)
            jQuery('html, body').animate({'scrollTop': jQuery('#block_san_pham').offset().top}, 500);
        
        jQuery('#item-amount').val(1);
        // Add to cart
        jQuery('#add_to_cart').click(function (evt) {
//            alert ("Có thể menu giỏ hàng phía trên hiển thị thông tin không chính xác, sau khi thêm sản phẩm vào giỏ hàng, bạn có thể vào trang khatocofash.net/gio-hang/ để xem thông tin giỏ hàng. Thông tin sẽ giỏ hàng sẽ được khắc phục sớm nhất có thể!");
            var this_button = jQuery(this);
            var product_id = this_button.attr("data-product-id") || 0;

            if (product_id > 0) {
                var color_number = this_button.attr("data-color-number") || 0;
                var color_id = this_button.attr("data-color-id") || '';
                var product_size = this_button.attr("data-size") || 'M';
                var product_quantity = jQuery('#item-amount').val();

                jQuery.ajax({
                    url: "<?php echo admin_url('admin-ajax.php'); ?>",
                    dataType: "json",
                    async: true,
                    type: "POST",
                    cache: true,
                    data: {
                        action: 'manage_cart_ajax',
                        id: product_id,
                        color_number: color_number,
                        color_id: color_id,
                        size: product_size,
                        quantity: product_quantity,
                        do_ajax: 'add_to_cart'
                    },
                    success: function (data) {
                        if (data.success == 'true') {
                            load_top_cart();
                            show_cart();
                        } else {
                            if (data.msg) {
                                alert(data.msg);
                            }
                        }
                    }
                });
            }

            return false;
        });
    });
</script>

<!--size calculator-->
<div class="modal fade" id="calSizeModal" tabindex="-1" role="dialog" ng-app="ktcCalSize">
  <div class="modal-dialog" role="document" ng-controller="ktcControllerCalSize">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Tính toán size</h4>
      </div>
      <div class="modal-body clearfix">
        <div class="col-md-6">
            <p>
                <select class="form-control" id="product-type" ng-model="productType" ng-change="suggest()" disabled>
                <option value="shirt">Áo sơ mi</option>
                <option value="tShirt">Áo thun</option>
                <option value="pant0">Quần tây 0 ly</option>
                <option value="pant1">Quần tây 1 ly</option>
                <option value="pantKaki0">Quần kaki 0 ly</option>
                <option value="pantKaki1">Quần kaki 1 ly</option>
                <option value="pantKaki">Quần kaki</option>
                <option value="pantShort">Quần short</option>
                </select>
            </p>

            <p>
                <select class="form-control" id="product-form" ng-model="productForm" ng-change="suggest()" ng-hide="productType=='tShirt'" disabled>
                <option value="slimfit" ng-hide="productType=='pantKaki0' || productType=='pantKaki1' || productType=='pant0' || productType=='pant1'">Form Slimfit</option>
                <option value="regular" ng-hide="productType=='pantKaki'">Regular</option>
                <option value="tailor" ng-hide="productType.startsWith('pant')">Tailor</option>
                </select>
            </p>

            <p>
                <select class="form-control" id="cal-type" ng-model="calType" ng-change="suggest()">
                <option value="hW" ng-show="productType=='tShirt' || productType=='shirt'">Tính theo chiều cao / cân nặng</option>
                <option value="other" ng-show="productType=='shirt'">Tính theo vòng ngực / cổ / vai</option>
                <option value="belly" ng-hide="productType=='tShirt' || productType=='shirt'">Tính theo vòng bụng / mông / đùi</option>
                </select>
            </p>
        </div>

        <div class="col-md-6">
            <p ng-show="calType === 'hW'">
                <input type="number" min="100" max="230" ng-model="shirtHeight" ng-change="suggest()" class="form-control" id="cal-height" placeholder="Nhập chiều cao (cm)">
            </p>

            <p ng-show="calType === 'hW'">
                <input type="number" min="20" max="300" ng-model="shirtWeight" ng-change="suggest()" class="form-control" id="cal-weight" placeholder="Nhập cân nặng (kg)">
            </p>
            <p ng-show="calType === 'other' && productType!='tShirt' ">
                <input type="number" min="50" max="300" ng-model="chestSize" ng-change="suggest()" class="form-control" placeholder="(*) Nhập vòng ngực (cm)">
            </p>

            <p ng-show="calType === 'other' && productType!='tShirt'">
                <input type="number" min="10" ng-model="neckSize" ng-change="suggest()" class="form-control" placeholder="Nhập vòng cổ (cm)">
            </p>

            <p ng-show="calType === 'other' && productType!='tShirt'">
                <input type="number" min="10" ng-model="shoulderSize" ng-change="suggest()" class="form-control" placeholder="Nhập độ rộng vai (cm)">
            </p>

            <p ng-show="calType === 'belly'">
                <input type="number" min="10" ng-model="bellySize" ng-change="suggest()" class="form-control" placeholder="(*) Nhập vòng bung (cm)">
            </p>

            <p ng-show="calType === 'belly'">
                <input type="number" min="10" ng-model="rearSize" ng-change="suggest()" class="form-control" placeholder="Nhập vòng mông (cm)">
            </p>

            <p ng-show="calType === 'belly'">
                <input type="number" min="10" ng-model="thighSize" ng-change="suggest()" class="form-control" placeholder="Nhập vòng đùi (cm)">
            </p>

        </div>

        <div class="col-md-12 suggest-line" ng-show="suggestSize!='' && suggestSize != 'overload' ">
            <p><strong>SIZE ĐỀ XUẤT </strong><span class="suggest-size">{{suggestSize}}</span></p>
        </div>
        <div class="col-md-12 suggest-line" ng-show="suggestSize!='' && suggestSize == 'overload' ">
            <p><strong>Vượt quá giới hạn tính toán size</strong></p>
        </div>

      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
        <button type="button" class="btn btn-primary" id="get-size" ng-show="suggestSize!='' && suggestSize != 'overload' ">Lấy size</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<?php
//print_r($_SESSION['just_seen']);
if (!isset($_SESSION['just_seen']) || is_null($_SESSION['just_seen']) || empty($_SESSION['just_seen']))
    $_SESSION['just_seen'] = array();
global $post;
$product_id = $post->ID;
if (!in_array($product_id, $_SESSION['just_seen'])) {
    array_push($_SESSION['just_seen'], $product_id);
}
get_footer();
?>