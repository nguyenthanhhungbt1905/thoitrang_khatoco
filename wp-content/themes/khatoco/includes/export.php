<?php define('WP_USE_THEMES', false);
require_once($_SERVER['DOCUMENT_ROOT'] . '/wp-load.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/wp-content/themes/khatoco/includes/phpexcel/PHPExcel.php');

error_reporting( E_ALL );
ini_set('display_errors', 1);

// Products export fields
$export_products = array(
	'post_sku' => array(
		'required' => 1,
		'label' => 'SKU'
	),
	'post_title' => array(
		'required' => 1,
		'label' => 'Tên sản phẩm'
	),
	// 'post_price' => array(
	// 	'required' => 1,
	// 	'label' => 'Đơn giá'
	// ),
    'link' => array(
		'required' => 1,
		'label' => 'Link'
	),
    'thumb' => array(
		'required' => 1,
		'label' => 'Thumbnail'
	),
);

if( isset($_GET['alsjdkajsdoawoiqrkasamsnfjahiewhjnafasahiwkskh']) ){
    $datas = array();
	$filename = 'BaoCaoChiTiet';
	$time_exports = '';

    $args = array(
        'post_type' => 'post',
        'posts_per_page' => -1,
        'orderby' => 'date',
        'order' => 'DESC',
    );

    $posts = get_posts( $args );

    foreach ($posts as $key => $p) {
        $post_id = $p->ID;

        $product_colors = get_post_meta($post_id, 'post_colors', true);
        $thumb = '';
        foreach($product_colors as $c_key => $c){
            $images_ids = explode(',', $c['images_ids']);
            $images_ids = $images_ids[0];
            $images_ids_full = wp_get_attachment_image_src($images_ids, 'full');
            $thumb = $images_ids_full[0];
        }
        $datas[$key] = array(
            'sku' => get_post_meta( $post_id, 'post_sku', true ),
            'title' => $p->post_title,
            'link' => get_permalink( $post_id ),
            'thumb' => $thumb,
        );
    }

    // echo '<pre>';
    // var_dump($datas);
    // exit;

    if( !empty( $datas ) ){
        $objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setCreator("Thời trang Khatoco")->setLastModifiedBy("Thời trang Khatoco");

		$style = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			),
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => '666666')
			),
			'font'  => array(
			'color' => array('rgb' => 'FFFFFF'),
			'size'  => 13,
			)
		);

        $row = 1;
		$col = ord('A');

        foreach ($export_products as $epkey => $ep) {
            $objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $ep['label']);
            $objPHPExcel->getActiveSheet()->getColumnDimension(chr($col-1))->setAutoSize(true);
        }
        $objPHPExcel->getActiveSheet()->getStyle('A1:D1')->applyFromArray($style);

        foreach ($datas as $dkey => $data) {
            $row++;
            $col = ord('A');
            $objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $data['sku']);
            $objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $data['title']);
            $objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $data['link']);
            $objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $data['thumb']);
        }

        $objPHPExcel->setActiveSheetIndex(0);

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment; filename="'. $filename .'"');
		header('Cache-Control: max-age=0');
		header('Cache-Control: max-age=1');

		header ('Last-Modified: '. gmdate('D, d M Y H:i:s') .' GMT');
		header ('Cache-Control: cache, must-revalidate');
		header ('Pragma: public');

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
		exit;
    }
}
