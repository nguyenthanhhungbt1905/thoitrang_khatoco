<?php

$_mail_configs = array();
$_mail_configs[] = 'Cc: thoitrangkhatoco@gmail.com';
$_mail_configs[] = 'Content-Type: text/html; charset=UTF-8';

add_action( 'phpmailer_init', 'custom_mail_smtp_server' );
function custom_mail_smtp_server( $phpmailer ) {
	$phpmailer->isSMTP();
        $phpmailer->Host = 'localhost';
    $phpmailer->Port = 25;
	$phpmailer->SMTPAuth = true;
	$phpmailer->Username = 'info@thoitrangkhatoco.vn';
	$phpmailer->Password = 'cskhktc2016';
	 /*$phpmailer->SMTPSecure = "ssl";*/
	$phpmailer->From = 'info@thoitrangkhatoco.vn';
	$phpmailer->FromName = 'Thời trang Khatoco';
    $phpmailer->SMTPDebug = 0;

    $phpmailer->SMTPOptions = array(
        'ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
        )
    );
}

// Verify user by email
function verify_user_first_step( $user_id ){
	$verified = get_user_meta( $user_id, 'actived', true );
	// User already actived
	if($verified == 'yes'){
		return false;
	}
	else{
		if( !isset($_SESSION) ) session_start();
		$otp = substr(md5( sha1( KHATOCO_VERIFY_SALT . $user_id . time() ) ), 0, 10);
		$_SESSION['verify_user_otp'] = $otp;
		return $otp;
	}
}

// Reverify user
function reverify_user( $user_id ){
	verify_user_first_step( $user_id );
}

// THEME OPTIONS
// add_action( 'admin_menu', 'mail_content_option_page' );
function mail_content_option_page() {
	add_options_page('Mail Content Options', 'Mail Content Options', 'manage_options', 'mail-content-options', 'mail_content_options');
}

// add_action( 'admin_init', 'mail_content_settings' );
function mail_content_settings() {
	// User mail contents
	register_setting('mail-content-option', 'user_register_mail');
	register_setting('mail-content-option', 'user_password_change_mail');

	// Orders mail contents
	register_setting('mail-content-option', 'new_orders_mail');
	register_setting('mail-content-option', 'delete_orders_mail');
}

function mail_content_options() {
	$mail_options = array(
		'user_register_mail' => 'Mail thông báo người dùng mới đăng ký',
		'user_password_change_mail' => 'Mail thông báo người dùng đổi mật khẩu',
		'new_orders_mail' => 'Mail xác nhận đơn hàng',
		'delete_orders_mail' => 'Mail thông báo huỷ đơn hàng',
	); ?>
	<div class="wrap">
		<h2>Mail Options</h2>
		<p>Nội dung mail được hệ thống tự động gửi.</p>
		<form method="POST" action="options.php">
			<?php settings_fields( 'mail-content-option' );
			$settings = array( 'textarea_rows' => 7 );
			foreach ($mail_options as $mkey => $mo): ?>
				<div>
					<h3><?php echo $mo; ?></h3>
					<?php wp_editor( get_option( $mkey ), $mkey, $settings ); ?>
				</div>
				<hr>
				<div class="clear"></div>
			<?php endforeach; ?>
			<div style="position: fixed; bottom: 90px; right: 20px; z-index: 100000; padding: 0px 30px; background-color: rgba(0, 0, 0, 0.8); border-radius: 5px; text-align: center;">
				<?php submit_button( 'Save' ); ?>
			</div>
		</form>
	</div>
<?php }

// Get mail content
function get_mail_template($type, $data = array()){
	$mail_options = array(
		'user_register_mail' => 'Mail thông báo người dùng mới đăng ký',
		'user_password_change_mail' => 'Mail thông báo người dùng đổi mật khẩu',
		'new_orders_mail' => 'Mail xác nhận đơn hàng',
		'delete_orders_mail' => 'Mail thông báo huỷ đơn hàng',
	);

	$mail_template = file_get_contents(get_template_directory_uri() .'/assets/email-template/'. $type .'.html');
	$title = $mail_options[$type];
	if($data){
		foreach ($data as $key => $d) {
			$mail_template = str_replace( $key, $d, $mail_template );
		}
	}

	return $mail_template;
}

// Mail announce new user register
function mail__announce_new_user_register($user_info){
	$_mail_configs = array();
	$_mail_configs[] = 'Content-Type: text/html; charset=UTF-8';
	$_mail_configs[] = 'Cc: thoitrangkhatoco@gmail.com';
	$to = $user_info->user_email;
	$title = 'Chào mừng bạn đến với Thời trang Khatoco!';
	$otp = verify_user_first_step( $user_info->ID );
	$args = array(
		'*|MENU_LINK_1|*' => home_url( '/' ),
		'*|MENU_LINK_2|*' => home_url( '/' ) .'/san-pham/',
		'*|MENU_LINK_3|*' => home_url( '/' ) .'/hang-dong-phuc/',
		'*|MENU_LINK_4|*' => home_url( '/' ) .'/bo-anh/',
		'*|MENU_LINK_5|*' => home_url( '/' ) .'/style-guide/',
		'*|MENU_LINK_6|*' => home_url( '/' ) .'/san-pham/khuyen-mai/',

		'*|IMAGE_SRC|*' => get_template_directory_uri() .'/assets/email-template/EDM_Registered_v1',
		'*|HOME_SRC|*' => home_url( '/' ),

		'*|USER_EMAIL|*' => isset( $user_info->display_name ) ? $user_info->display_name : $to,
		'*|EMAIL|*' => $to,
		'*|VERIFY_LINK|*' => home_url('/') . 'verify_user.php?otp='. $otp .'&email='. $to,
	);
	$content = get_mail_template('user_register_mail', $args);

	wp_mail( $to, $title, $content, $_mail_configs );
}

// Mail announce change password to user
function mail__announce_password_change($user_info, $new_password){
	$_mail_configs = array();
	$_mail_configs[] = 'Content-Type: text/html; charset=UTF-8';
	$_mail_configs[] = 'Cc: thoitrangkhatoco@gmail.com';
	$to = $user_info->user_email;
	$title = '[Khatoco] Thông báo đổi mật khẩu';
	$new_password_hide = substr($new_password, 0, (strlen($new_password) - 4)) . '****';
	$content = get_mail_template('user_password_change_mail', array('*|NEW_PASSWORD|*' => $new_password_hide));

	wp_mail( $to, $title, $content, $_mail_configs );
}

// Mail announce orders created
function mail__announce_new_orders($to, $orders){
	$_mail_configs = array();
	$_mail_configs[] = 'Content-Type: text/html; charset=UTF-8';
	$_mail_configs[] = 'Cc: thoitrangkhatoco@gmail.com';
	$title = '[Khatoco] Đặt hàng thành công';

	$orders_id = $orders->ID;
	$order_code = get_post_meta( $orders_id, 'order_code', true );
	$order_permalink = add_query_arg( array( 'order_code' => $order_code ), get_page_link( 2505 ) );
	$order_info = get_post_meta( $orders_id, 'order_info', true );

	$order_user_id = get_post_meta( $orders_id, 'order_user_id', true);
    $customer = array();
    if(($order_user_id > 0 || $order_user_id != '0') && ($order_user_id != 443 || $order_user_id != '443')){
        $order_user_data = get_userdata($order_user_id);
        $customer = array(
        	// 'fullname' => isset( $order_user_data->display_name ) ? $order_user_data->display_name : get_user_meta( $order_user_data->ID, 'name', true ),
            'email' => $order_info['email'],
            'phone' => $order_info['phone'],
            'address' => $order_info['address'],
        );

        $fullname = $order_user_data->display_name;
		$name = get_user_meta( $order_user_id, 'name', true );
		if( $name ) $fullname = $name;
		$name = get_user_meta( $order_user_id, 'fullname', true );
		if( $name ) $fullname = $name;

		$customer['fullname'] = $fullname;
    }
    else {
        $customer = get_post_meta( $orders_id, 'customer', true );
    }

    if ( !isset( $customer['fullname'] ) ){
        $customer['fullname'] = 'N/A';
    }

    $receiver = get_post_meta( $orders_id, 'receiver', true );

    $campaign_influence = get_post_meta($orders_id, 'campaign_influence', true);
    if($campaign_influence == '' || $campaign_influence == '0')
        $campaign_influence = false;
    else
        $campaign_influence = true;
    $campaignInfo = get_post_meta($orders_id, 'campaignInfo', true);

    $hasCoupon = get_post_meta($orders_id, 'hasCoupon', true);
    $couponName = get_post_meta($orders_id, 'couponName', true);
    $couponSaleoffValue = get_post_meta($orders_id, 'couponSaleoffValue', true);

    $isVip = get_post_meta($orders_id, 'isVip', true);
    $vipAfter = get_post_meta($orders_id, 'vipAfter', true);

    $card_name = '';
    $card = get_user_meta( $order_user_id, 'user_card', true );
	if($card == '') $card = '0';
    switch(strtoupper($card)){
		case '1':
		case 'SILVER':
			$card_name = 'Thẻ Bạc'; break;
		case '2': 
		case 'GOLD':
			$card_name = 'Thẻ Vàng'; break;
		case '3':
		case 'DIAMOND':
			$card_name = 'Thẻ Kim Cương'; break;
		default : $card_name = 'Thành viên';
	}


	$payment_method = intval(get_post_meta( $orders_id, 'order_payment_method', true ));
	$payment_incentive = get_post_meta( $orders_id, 'payment_incentive', true );
	$incentive = 0;
	$payment_method_txt = 'COD (Thanh toán khi nhận hàng)';
	if($payment_method == 2 || $payment_method == 3){
            if( $payment_method == 2 ){
//                $incentive = $payment_incentive['domestic']['value']/100;
                $payment_method_txt = 'Thanh toán qua ATM nội địa';
            }
            elseif( $payment_method == 3 ){
//                $incentive = $payment_incentive['international']['value']/100;
                $payment_method_txt = 'Thanh toán qua Visa/Master Card';
            }
    }

	$order_info_txt = '';
	$total = 0;
	if(isset($order_info['products'])){
		$number = 1;
		foreach ($order_info['products'] as $key => $product) {
			$order_info_txt .= '<tr style="border-bottom:1px solid #e1e1e1;">';
	        $order_info_txt .= '<td width="20" valign="top" style="background:#f5f5f5;">&nbsp;</td>';
            $order_info_txt .= '<td width="150" valign="top" style="background:#f5f5f5;">';
            $order_info_txt .= '<p style="display:block;line-height:14px;text-align:left;margin:0;padding:0;color:#000;font-size:11px;font-family:Arial, sans-serif;margin-top:15px;margin-bottom:15px;"><strong>'. $product['name'] .'</strong></p>';
            $order_info_txt .= '</td>';
            $order_info_txt .= '<td width="115" valign="top" style="background:#f5f5f5;">';
            $order_info_txt .= '<p style="display:block;line-height:14px;text-align:center;margin:0;padding:0;color:#000;font-size:11px;font-family:Arial, sans-serif;margin-top:15px;margin-bottom:15px;">'. $product['sku'] .'</p>';
            $order_info_txt .= '</td>';
            $order_info_txt .= '<td width="80" valign="top" style="background:#f5f5f5;">';
            $order_info_txt .= '<p style="display:block;line-height:14px;text-align:center;margin:0;padding:0;color:#000;font-size:11px;font-family:Arial, sans-serif;margin-top:15px;margin-bottom:15px;">'. aj_format_number($product['price']) .'</p>';
            $order_info_txt .= '</td>';
            $order_info_txt .= '<td width="85" valign="top" style="background:#f5f5f5;">';
            $order_info_txt .= '<p style="display:block;line-height:14px;text-align:center;margin:0;padding:0;color:#000;font-size:11px;font-family:Arial, sans-serif;margin-top:15px;margin-bottom:15px;">'. $product['quantity'] .'</p>';
            $order_info_txt .= '</td>';
            $order_info_txt .= '<td width="90" valign="top" style="background:#f5f5f5;">';
            $order_info_txt .= '<p style="display:block;line-height:14px;text-align:right;margin:0;padding:0;color:#000;font-size:11px;font-family:Arial, sans-serif;margin-top:15px;margin-bottom:15px;"><strong>'. aj_format_number( $product['price_saleoff']['value'] ) .'</strong></p>';
            $order_info_txt .= '</td>';
            $order_info_txt .= '<td width="20" valign="top" style="background:#f5f5f5;">&nbsp;</td>';
            $order_info_txt .= '</tr>';
		}
	}

	$total = aj_format_number( get_post_meta( $orders_id, 'order_total', true ) );

	$args = array(
		'*|MENU_LINK_1|*' => home_url( '/' ),
		'*|MENU_LINK_2|*' => home_url( '/' ) . '/san-pham/',
		'*|MENU_LINK_3|*' => home_url( '/' ) . '/hang-dong-phuc/',
		'*|MENU_LINK_4|*' => home_url( '/' ) . '/bo-anh/',
		'*|MENU_LINK_5|*' => home_url( '/' ) . '/style-guide/',
		'*|MENU_LINK_6|*' => home_url( '/' ) . '/san-pham/khuyen-mai/',

		'*|BOT_MENU|*' => home_url( '/' ) . '/san-pham/hang-moi/',
		'*|BOT_MENU_1|*' => home_url( '/' ) . '/san-pham/ao-so-mi-3/',
		'*|BOT_MENU_2|*' => home_url( '/' ) . '/san-pham/ao-thun/',
		'*|BOT_MENU_3|*' => home_url( '/' ) . '/san-pham/quan-tay/',
		'*|BOT_MENU_4|*' => home_url( '/' ) . '/san-pham/quan-khakis/',

		'*|IMAGE_SRC|*' => get_template_directory_uri() .'/assets/email-template/EDM_Order_Confirmed',
		
		'*|CUS_NAME|*' => $customer['fullname'],
		'*|CUS_ADDR|*' => $customer['address'],
		'*|CUS_EMAIL|*' => $customer['email'],
		'*|CUS_PHONE|*' => $customer['phone'],
		'*|CUS_CARD|*' => $card_name,
		'*|CUS_ALL|*' => get_turnover_by_user( $order_user_id ),

		'*|ORDER_CODE|*' => $order_code,
		'*|PAYMENT_METHOD|*' => $payment_method_txt,
		'*|ORDER_DATE|*' => get_the_time( 'd/m/Y h:i:s', $orders_id ),
		'*|ORDER_INFO|*' => $order_info_txt,
		'*|ORDER_PERMALINK|*' => $order_permalink,
		'*|ORDER_TOTAL|*' => $total,
		'*|TRANSPORT_FEE|*' => aj_format_number( get_post_meta( $orders_id, 'transport_fee_total', true ) ) .'đ',
		'*|SALE_CARD|*' => ($vipAfter == 0) ? '0đ': (1-$vipAfter)*100 .'%',
		'*|SALE_COUPON|*' => aj_format_number($couponSaleoffValue) .'đ',
		'*|SALE_PAYMENT|*' => $incentive*100 .'%',
	);

	if($receiver){
		$args['*|REC_NAME|*'] = isset($receiver['fullname']) ? $receiver['fullname']:'N/A';
		$args['*|REC_ADDR|*'] = $receiver['address'];
		$args['*|REC_PHONE|*'] = $receiver['phone'];
	}
	else{
		$args['*|REC_NAME|*'] = $customer['fullname'];
		$args['*|REC_ADDR|*'] = $customer['address'];
		$args['*|REC_PHONE|*'] = $customer['phone'];
	}

	$content = get_mail_template('new_orders_mail', $args);

	wp_mail( $to, $title, $content, $_mail_configs );
}

// Mail announce cancel orders
function mail__announce_orders_cancel($to, $orders){
	$_mail_configs = array();
	$_mail_configs[] = 'Content-Type: text/html; charset=UTF-8';
	$_mail_configs[] = 'Cc: thoitrangkhatoco@gmail.com';
	$title = '[Khatoco] Thông báo huỷ đơn hàng';

	$orders_id = $orders->ID;
	$order_code = get_post_meta( $orders_id, 'order_code', true );
	$order_permalink = add_query_arg( array( 'order_code' => $order_code ), get_page_link( 2505 ) );

	$content = get_mail_template('delete_orders_mail', array(
		'*|ORDER_CODE|*' => $order_code,
		'*|ORDER_PERMALINK|*' => $order_permalink,
	));

	wp_mail( $to, $title, $content, $_mail_configs );
}
