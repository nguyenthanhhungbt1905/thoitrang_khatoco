<?php 
// sp đính kèm hoặc sp khác với giá đặc biệt
function campagin_admin_enqueue_scripts_callback() {
    
    wp_enqueue_script('bootstrap-js', 'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/js/bootstrap.min.js',array('jquery'), '1.0', true);
	$post_type = get_post_type();
	$arrAdminCss = array(
		'admin.css' => 'admin'
	);

	$arrAdminJs = array(
        'main.js'	=> 'admin-js-main'
	);

    if ($post_type === 'campaign'){
        $arrAdminJs['admin-campaign.js'] = 'campaign-admin';
        //wp_enqueue_script('data-tb-js', 'https://cdn.datatables.net/v/bs/jqc-1.12.3/dt-1.10.12/se-1.2.0/datatables.min.js',array('jquery'), '1.0', true); - Dùng link này sẽ bị conflict với jQuery 1 của wordpress do include jQuery vào datatable.min.js

		wp_enqueue_script('data-tb-js', 'https://cdn.datatables.net/v/bs/dt-1.10.15/se-1.2.2/datatables.min.js',array('jquery'), '1.0', true); // Hãy xài link này, không có include jQuery vào sẵn
        
        //wp_enqueue_script('jquery2', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js');
        //wp_enqueue_script('data-tb-js', 'https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js',array('jquery2'), '1.0', true);
    }

	$themePathAdmin = get_template_directory_uri();

	#Load CSS
	foreach ( $arrAdminCss as $src => $handle) {
		wp_enqueue_style( $handle, $themePathAdmin."/assets/admin/css/{$src}", array(), '1.0');
	}

	#Load Js
	foreach ( $arrAdminJs as $src => $handle) {
		wp_enqueue_script( $handle, $themePathAdmin."/assets/admin/js/{$src}",array('jquery'), '1.0', true);
	}
    
    wp_enqueue_style( 'data-tb-css','https://cdn.datatables.net/v/dt/jqc-1.12.3/dt-1.10.12/se-1.2.0/datatables.min.css' , array(), '1.0');
    //wp_enqueue_style( 'data-tb-css','https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css' , array(), '1.0');
    wp_enqueue_style( 'bootstrap-css','https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/css/bootstrap.min.css' , array(), '1.0');
    wp_enqueue_style( 'fontaswesome-css','https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css' , array(), '1.0');
    
    
    

}
add_action( 'admin_enqueue_scripts', 'campagin_admin_enqueue_scripts_callback' );

add_filter('post_row_actions', 'remove_view_frontend_campaign_list', 10, 2);
function remove_view_frontend_campaign_list($actions, $post){
    if('campaign' == $post->post_type){
        unset($actions['view']);
    }
    return $actions;
}

add_action( 'admin_init', 'campaign_register_setting' );
function campaign_register_setting() {
	register_setting('campaign-options', 'promotion_campaign');
	register_setting('campaign-options', 'promotion_campaign_old');
}



function get_all_campaigns() {
	$promotion_campaign_default = array(
		'id' => 0,
		'multiple_event' => '0',
		'sort_order_event' => '0',
		'name' => '',
		'description' => '',
		'date' => array(
			'after' => '',
			'before' => ''
		),
		'sale' => array(
			'type' => 'product',
			'card' => array(
				'0' => array(
					'id' => 0,
					'name' => 'Không có thẻ',
					'value' => 0
				),
				'1' => array(
					'id' => 0,
					'name' => 'Thẻ Bạc',
					'value' => 0
				),
				'2' => array(
					'id' => 0,
					'name' => 'Thẻ Vàng',
					'value' => 0
				),
				'3' => array(
					'id' => 0,
					'name' => 'Thẻ Kim cương',
					'value' => 0
				)
			),
			'product' => array(
				array(
					'number' => '0',
					'unit' => '%',
					'products' => array(
						'id' => '1',
						'list' => array()
					)
				)
			),
			'orders' => array(
				array(
					'number' => '0',
					'unit' => '%',
					'orders_total' => '0' // active when total of orders more than
				)
			),
			'multi_product' => array(
					array(
						'products' => array(
							'id' => '1',
							'list' => array()
						)
					)
				),
			'buy_money_product' => array(
				array(
					'products' => array(
						'id' => '1',
						'list' => array()
					)
				)
			),
		),
		'incentives' => array(
			'vip_cards' => array(
				'1' => array(
					'id' => 0,
					'name' => 'Thẻ Bạc',
					'value' => 0
				),
				'2' => array(
					'id' => 0,
					'name' => 'Thẻ Vàng',
					'value' => 0
				),
				'3' => array(
					'id' => 0,
					'name' => 'Thẻ Kim cương',
					'value' => 0
				)
			),
			'pay_online' => array(
				'domestic' => array(
					'id' => 0,
					'name' => 'Thanh toán nội địa - OnePay',
					'value' => 0,
				),
				'international' => array(
					'id' => 0,
					'name' => 'Thanh toán quốc tế - OnePay',
					'value' => 0,
				)
			),
		)
	);

	$campaigns = get_posts( array( 'post_type' => 'campaign', 'posts_per_page' => -1 ) );

	$promotion_campaigns = array();
	foreach ($campaigns as $pckey => $pc) {
		$campaign_details = get_post_meta( $pc->ID, 'promotion_campaign', true );
		if($campaign_details == '')
			$campaign_details = $promotion_campaign_default;

		$promotion_campaigns[] = $campaign_details;
	}

	return $promotion_campaigns;
}

/**
 * Check the new campaign is allow to running now
 * @param  Nhat Nguyen
 * @param  datetime $before - begin time campagin
 * @param  datetime $after  - end time campagin
 * @return boolance
 */
function new_campaign_allow_time_range( $before, $after ){
	$campaigns = get_all_campaigns();

	foreach ($campaigns as $pckey => $pc) {
		$check_time = strtotime($after) < strtotime($pc['date']['after']) || strtotime($before) > strtotime($pc['date']['before']);
		if( $pc['is_running'] == '1' && $check_time )
			continue;
		else
			return false;
	}

	return true;
}


// Check date in range date
/*function check_current_date_in_range($start_date, $end_date){
    $start_ts = strtotime($start_date);
    $end_ts = strtotime($end_date);
    $user_ts = current_time('timestamp', 0);

    return (($user_ts >= $start_ts) && ($user_ts <= $end_ts));
}*/

// Check runnable campaign
function check_campaign_is_running(){
	$promotion_campaign = get_all_campaigns();

	foreach ($promotion_campaign as $pckey => $pc) {
		if( !isset($pc['is_running']) || $pc['is_running'] == '0' || $pc['is_running'] == '' )
			continue;
		elseif(check_current_date_in_range($pc['date']['after'], $pc['date']['before'])){
			return $pc;
		}
		else 
			continue;
	}

	return false;
}

// Check product is being sale off by campaign, return false if not; else return product price after sale-off and the saleoff value
function check_product_is_saleoff_by_campaign($product_id){
	$price_after = false;
	$check = check_campaign_is_running();

	if(is_array($check) && !empty($check)){
		$type = $check['sale']['type'];
		// If campaign destiation is 'orders', return false
		if($type == 'orders') {
			return array(
				'type' => $type,
				'orders' => $check['sale']['orders']
			);
		}

		$last_index = 0;
		foreach ($check['sale'][$type] as $cskey => $cs) {
			if(isset($cs['products']['id']) && ($cs['products']['id'] == '1' || ($cs['products']['id'] == '0' && in_array(strval($product_id), $cs['products']['list'])))){
				$current_index = $cs['index'];
				if( $current_index > $last_index ){
					$price = (int) get_post_meta( $product_id, 'post_price', true );

					list($number, $unit, $products) = array_values($cs);

					$number = floatval($number);
					if($unit == '%'){
						$new_price = $price * (100 - $number) / 100;
						$price_after = array(
							'name' => $check['name'],
							'type' => $type,
							'sale' => array(
								'number' => (int) $number,
								'unit' => $unit
							),
							'sale_txt' => $number . $unit,
							'new_price' => $new_price,
							'new_price_formated' => aj_format_number($new_price),
						);
					}
					elseif($unit == 'VND'){
						$new_price = $price - $number;
						$price_after = array(
							'name' => $check['name'],
							'type' => $type,
							'sale' => array(
								'number' => (int) $number,
								'unit' => $unit
							),
							'sale_txt' => $number . $unit,
							'new_price' => $new_price,
							'new_price_formated' => aj_format_number($new_price),
						);
					}
					$last_index = $current_index;
				}
			}
		}
	} 
	return $price_after;
}

function check_order_is_saleoff_by_campaign($cart_info){
	$check = check_campaign_is_running();

	if(is_array($check) && !empty($check)){
		$type = $check['sale']['type'];
		// If campaign destiation is 'orders', return false
		if($type == 'orders') {
			return array(
				'type' => $type,
				'orders' => $check['sale']['orders']
			);
		}
	}
	return false;
}

// Check coupon with campaign, return true if the campaign not influence, or return false if not
function check_coupon_is_runnable_in_campaign($coupon_id){
	$cart_has_product_in_campaign = false;
	$cart = get_cart_info_from_session();
	if($cart['products']){
		foreach ($cart['products'] as $pkey => $p) {
			if( check_product_is_saleoff_by_campaign( intval($p['id']) ) ){
				$cart_has_product_in_campaign = true;
				break;
			}
		}
	}

	if(!$cart_has_product_in_campaign)
		return true;

	$check = check_campaign_is_running();

	if(!isset($check['incentives']['coupon']) || $check['incentives']['coupon']['id'] != '1' || !in_array(strval($coupon_id), $check['incentives']['coupon']['list'])){
		return false;
	}

	return true;
}

if( isset( $_GET['reset'] ) ){
	// Reset khuyen-mai category
	$all_products = get_posts( array( 'posts_per_page' => -1, 'post_type' => 'post' ) );
	if( !empty($all_products) ){
		foreach ($all_products as $pkey => $p) {
			$sale_off = get_post_meta($p->ID, 'sale_off', true );
			if( ($sale_off == '0' || $sale_off == '') && in_category( 111, $p->ID ) ){
				wp_remove_object_terms( $p->ID, 111, 'category' );
			}
		}
	}
}

require_once(get_template_directory().'/includes/campaign/backend/campaign-backend.php');