<?php


/* Custom menu */
if (current_user_can('manage_options')) {
    add_action('admin_menu', 'add_custom_sync_menu');
}
function add_custom_sync_menu()
{
    add_menu_page('Đồng bộ', 'Đồng bộ', 'manage_options', 'sync-process', 'custom_sync_template_popup_callback', 'dashicons-update', 101);
    // add_menu_page('Đồng bộ', 'Đồng bộ', 'manage_options', 'sync-process', 'custom_sync_template_settings_callback', 'dashicons-update', 99);
}

function custom_sync_template_popup_callback()
{
    $result = 'false';
    $datas = array(); ?>
    <div id="InSyncProcess" style="display: block;">
        <div class="wrap" data-ng-app="khatocoSync">
            <h2>Đồng bộ dữ liệu</h2>

            <div data-ng-controller="SyncController">
                <form id="sync-wrap" ng-submit="submitSync()">
                    <div class="steps logs" ng-show="!isSyncing()">
                        <p><i>Đồng bộ lần cuối lúc {{last_sync_time}} GMT+7</i></p>
                        <p><label for="sync_products"><input type="checkbox" id="sync_products" data-ng-model="sync_products" checked=""> Đồng bộ sản phẩm tồn kho</label></p>
                        <p ng-show="sync_products"><label>Đồng bộ các sản phẩm có status : </label><select ng-init="postStatus = 'any'" ng-model="postStatus"><option value="any">Tất cả</option><option value="publish">Publish</option><option value="pending">Pending</option><option value="private">Private</option><option value="draft">Draft</option></select></p>
                        <p><label for="sync_customers"><input type="checkbox" id="sync_customers"
                                                              data-ng-model="sync_customers"> Đồng bộ thông tin khách
                                hàng</label></p>
                        <p><label for="sync_other"><input type="checkbox" id="sync_other" data-ng-model="sync_other"
                                                          disabled="" ng-checked="1"> Đồng bộ POS, kho và các thông tin
                                cơ bản khác</label></p>
                        <p>
                            <button id="start_sync" class="button button-primary">Đồng bộ</button>
                        </p>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>

                    <div class="steps" ng-show="isSyncing()">
                        <p><strong>Lưu ý: Hệ thống đang trong quá trình đồng bộ. Vui lòng không để trình duyệt tắt cho
                                đến khi tiến trình hoàn thành!</strong></p>
                        <div class="process-wrapper clearfix">
                            <div class="process" style="width:{{process}}%"></div>
                            <p class="process-percent">{{process | number:2}}%</p>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <h3 ng-show="isSyncing()">Các tiến trình đồng bộ <span class="pin-load spinner"
                                                                           ng-show="!justSync()"></span></h3>
                    <div class="steps logs log-wrap" ng-bind-html="stepsHTML"></div>
                    <div class="clear"></div>
                </form>
            </div>
        </div>
    </div>
    <style type="text/css">
        .clearfix:before, .clearfix:after {
            content: " ";
            display: table
        }

        .clearfix:after {
            clear: both
        }

        .steps {
            width: 100%;
            box-sizing: border-box;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            padding: 15px 0;
            overflow-x: hidden;
            overflow-y: auto
        }

        .steps .process-wrapper {
            width: 100%;
            height: 25px;
            overflow: hidden;
            position: relative;
            border: 1px solid #0E3950;
            border-radius: 3px;
            box-shadow: 0 0 1px 1px #ccc inset
        }

        .steps .process {
            width: 0;
            height: 25px;
            background-color: #00A3EF;
            box-shadow: 0px 1px 0px rgba(120, 200, 230, 0.6) inset;
            -webkit-transition: width 0.4s ease-in;
            transition: width 0.4s ease-in;
        }

        .steps .process-wrapper p {
            font-size: 13px;
            font-weight: 600;
            width: 100%;
            position: absolute;
            top: 0;
            left: 0;
            text-align: center;
            margin: 3px 0;
        }

        .steps .process-percent {
            margin: 3px 0;
        }

        .steps.logs p {
            font-weight: 400;
            position: relative;
            text-align: left;
            margin: 5px 0;
        }

        .hidden {
            display: none
        }

        .pin-load.spinner {
            display: inline-block;
            float: left;
        }
    </style>
<?php }

add_action('wp_ajax_sync', 'sync_processing');
add_action('wp_ajax_nopriv_sync', 'sync_cancel');
function sync_processing()
{
    $do = sanitize_text_field($_GET['do_ajax']);
    $uid = get_current_user_id();
  


    if ($do == 'submit_sync') {

        $data = sanitize_text_field($_GET['dataSync']);
        $postStatus = sanitize_text_field($_GET['postStatus']);
        $postStatus = empty($postStatus) ? 'any' : $postStatus;

        if ($data == 'products') {
            $args = array(
                'post_type' => 'post',
                'posts_per_page' => -1,
                'fields' => 'ids',
                'post_status' => $postStatus
            );
            $products = get_posts($args);
            if ($products) {
                echo json_encode(array('result' => 'data-available', 'datas' => $products));
            } else {
                echo json_encode(array('result' => 'no-data'));
            }
        } elseif ($data == 'customers') {
            /*$users = sync_getCustomers();
            $returns = array();
            foreach ($users as $ukey => $u) {
                $c = sync_getCustomerInfo($u);
                $returns[] = $c;
            }*/


            $args = array(
                'role' => 'subscriber',
                'fields' => 'ids',
            );
            $returns = get_users($args);

            // print_r($returns);exit();

            if ($returns) {
                echo json_encode(array('result' => 'data-available', 'datas' => $returns));
            } else {
                echo json_encode(array('result' => 'no-data'));
            }
        }
        exit;
    } elseif ($do == 'sync_process') {
       
        $r = true;
        $result = array('result' => 'added');
        $obj = sanitize_text_field($_GET['object']);
     
        $data = json_decode(file_get_contents("php://input"), true);
            
        if (isset($data['dataSync'])) {
            $data = $data['dataSync'];

            $result['obj'] = $data;
        }

        switch ($obj) {
            case 'customers':
                $crm_id = get_user_meta($data, 'code_crm', true);

                if ($crm_id == '')
                    $crm_id = 'WEB' . str_pad($data, 9, '0', STR_PAD_LEFT);
                $data_request = sync_getCustomer($crm_id);
                $data_request = sync_getCustomerInfo($data_request);
                // print_r($data_request);
                $r = sync_update_user($data_request, true);
                break;
            case 'products':
                $r = sync_update_products($data);
                break;
            case 'pos':
                sync_getPOS();
                sync_getStore();
                break;
            default:
                $r = true;
        }

        if (!$r)
            $result['result'] = 'failed';

        echo json_encode($result);
        exit;
    } elseif ($do == 'save_sync_time') {

        $now = current_time('mysql', 0);
        if (!get_option('sync_time')) {
            $r = add_option('sync_time', $now);
            echo json_encode(array(
                'msg' => $now,
                'uid' => $uid,
            ));
            exit;
        }
        $r = update_option('sync_time', $now);
        echo json_encode(array(
            'msg' => $now,
            'uid' => $uid
        ));
        exit;
    } elseif ($do == 'last_sync_time') {
        $o = get_option('sync_time');
        if (!$o) {
            $t = current_time('mysql', 0);
            $r = add_option('sync_time', $t);
            echo json_encode(array(
                'msg' => $t,
                'uid' => $uid
            ));
            exit;
        }
        echo json_encode(array(
            'msg' => $o,
            'uid' => $uid
        ));
        exit;
    } elseif ($do == 'get_sync_settings') {
        $result = 'false';
        $datas = array('');

        echo json_encode(array(
            'result' => $result,
            'datas' => $datas
        ));
        exit;
    }

    echo json_encode(array('result' => 'Stop doing that, please!'));
    exit;
}

function sync_cancel()
{
    echo json_encode(array('result' => '403 Forbidden'));
    exit;
}

/*$default = array(
			'sync_customers' => '',
			'sync_orders' => '',
			'sync_products' => '',
			'sync_customers_client' => '',
			'sync_orders_client' => '',
			'sync_time' => '00:00:00',
			'save_time' => ''
		);
		$sync_options = get_option( 'sync_options', $default );
		$data_get = array(
			'sync_customers' => '',
			'sync_products' => '',
			'sync_orders' => '',
			'sync_customers_client' => '',
			'sync_orders_client' => '',
		);
		foreach ($data_get as $dkey => $value) {
			if(isset($sync_options[$dkey]) && $sync_options[$dkey] == 'on'){
				$result = 'true';
				$datas[] = substr($dkey, 5, strlen($dkey));
			}
		}*/

// Settings of sync process
function custom_sync_template_settings_callback()
{
    $default = array(
        'sync_customers' => '',
        // 'sync_orders' => '',
        'sync_products' => '',
        'sync_customers_client' => '',
        'sync_orders_client' => '',
        'sync_time' => '00:00:00',
        'save_time' => '',
    );

    if (isset($_POST['sync'])) {
        if ($_POST['sync']['sync_time'] != '') {
            // Check the old scheduling and delete it
            $sync_options = get_option('sync_options');
            unset($sync_options['save_time']);
            $next_schedule = wp_next_scheduled('sync_daily_process_event', array('args' => $sync_options));
            if ($next_schedule) {
                wp_unschedule_event($next_schedule, 'sync_daily_process_event', array('args' => $sync_options));
            }

            // Format data
            foreach ($default as $dkey => $value) {
                if (!isset($_POST['sync'][$dkey]))
                    $_POST['sync'][$dkey] = $value;
            }

            // Create and save new schedule
            $time = time();
            $_POST['sync']['sync_time'] = date('d-m-Y', $time) . ' ' . $_POST['sync']['sync_time'];
            $_POST['sync']['save_time'] = current_time('mysql', 0);

            update_option('sync_options', $_POST['sync']);
            unset($_POST['sync']['save_time']);
            wp_schedule_event(strtotime($_POST['sync']['sync_time']) + 86400, 'daily', 'sync_daily_process_event', array('args' => $_POST['sync']));
        }
    }
    if (current_user_can('administrator')): $sync_options = get_option('sync_options', $default); ?>
        <div class="wrap">
            <h2>Cài đặt đồng bộ dữ liệu</h2>
            <div>
                <form action="" method="POST" id="sync-wrap">
                    <div class="steps logs">
                        <p><i>Đồng bộ lần cuối lúc <?php echo get_option('sync_time'); ?> GMT+7</i></p>
                        <div class="sync">
                            <h4>Đồng bộ dữ liệu từ CRM về website</h4>
                            <p><label for="sync_customers"><input type="checkbox"
                                                                  name="sync[sync_customers]" <?php checked('on', $sync_options['sync_customers']); ?>
                                                                  id="sync_customers">Lấy dữ liệu Khách hàng</label></p>
                            <?php /* ?><p><label for="sync_orders"><input type="checkbox" name="sync[sync_orders]" <?php checked('on', $sync_options['sync_orders']); ?> id="sync_orders" disabled="">Lấy dữ liệu Đơn hàng (tạm khoá)</label></p><?php */ ?>
                            <p><label for="sync_products"><input type="checkbox"
                                                                 name="sync[sync_products]" <?php checked('on', $sync_options['sync_products']); ?>
                                                                 id="sync_products">Lấy dữ liệu Sản phẩm</label></p>
                            <h4>Đồng bộ dữ liệu từ website lên CRM</h4>
                            <p><label for="sync_customers_client"><input type="checkbox"
                                                                         name="sync[sync_customers_client]" <?php checked('on', $sync_options['sync_customers_client']); ?>
                                                                         id="sync_customers_client">Đẩy dữ liệu Khách
                                    hàng lên CRM</label></p>
                            <p><label for="sync_orders_client"><input type="checkbox"
                                                                      name="sync[sync_orders_client]" <?php checked('on', $sync_options['sync_orders_client']); ?>
                                                                      id="sync_orders_client">Đẩy dữ liệu Đơn hàng lên
                                    CRM</label></p>
                            <h4>Thời gian đồng bộ</h4>
                            <p>Dữ liệu sẽ được đồng bộ vào lúc <input type="text" name="sync[sync_time]"
                                                                      value="<?php echo substr($sync_options['sync_time'], -8); ?>"
                                                                      id="sync_time" placeholder="hh:mm:ss" required="">
                                hàng ngày!</p>
                            <p><i>Trong thời gian này, hệ thống có thể bị quá tải. Tránh tất cả các hoạt động trên hệ
                                    thống để đảm bảo quá trình đồng bộ diễn ra suôn sẻ!</i></p>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <p><strong>Thông tin được lưu lúc <?php echo $sync_options['save_time']; ?></strong></p>
                    <p><?php submit_button('Save'); ?></p>
                </form>
            </div>
        </div>
    <?php else:
        wp_redirect(admin_url('/'));
        exit;
    endif;
}

// add_action('sync_daily_process_event', 'sync_daily_process_callback');
function sync_daily_process_callback()
{
    update_option('test_sync', time());
    // include_once( home_url( '/sync/sync_process.php' ) );
}