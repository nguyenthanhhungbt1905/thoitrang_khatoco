<?php session_start();


/* CONFIGS */
define('KHATOCO_VERIFY_SALT', 'ZK08da8K4b');
$aj_config['sercure_key'] = 'AqcnQRcGKPhA';

include(get_template_directory() . '/includes/api.php');
/* Objects */
include(get_template_directory() . '/includes/func.php');
include(get_template_directory() . '/includes/social-func.php');
include(get_template_directory() . '/includes/sync.php');
include(get_template_directory() . '/includes/egif.php');
include(get_template_directory() . '/includes/roles.php');
include(get_template_directory() . '/includes/options.php');
include(get_template_directory() . '/includes/otp.php');

/* EXTEND CONFIGS */
$aj_config['wp_content_path'] = aj_explode('/themes',get_template_directory(),0,'');

// 
remove_action('wp_head', 'wp_generator'); 
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'rsd_link');
add_action( 'wp_before_admin_bar_render', 'remove_edit_comments_admin_bar' );
function remove_edit_comments_admin_bar() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu( 'comments' );
}

// Remove comments page
add_action( 'admin_menu', 'remove_admin_comments_page' );
function remove_admin_comments_page(){
    remove_menu_page( 'edit-comments.php' );
}

// Removes comments from post and pages
add_action('init', 'remove_comment_support', 100);
function remove_comment_support() {
    remove_post_type_support( 'post', 'comments' );
    remove_post_type_support( 'page', 'comments' );
    remove_post_type_support( 'attachment', 'comments' );
}

/* Admin Bar */
add_action('after_setup_theme', 'remove_admin_bar');
function remove_admin_bar() {
    //if (!current_user_can('administrator') && !is_admin()) {
        show_admin_bar(false);
    //}
}

/* Main Menu */
class Main_Nav_Menu extends Walker_Nav_Menu {
    public $index_sub_menu_col = 1;
    function start_lvl(&$output, $depth = 0, $args = array()) {
        $indent = ($depth > 0  ? str_repeat("\t", $depth) : ''); // code indent
        $display_depth = ($depth + 1);

        if($display_depth == 1) {
            $this->index_sub_menu_col = 1;
            $output .= "\n". $indent .'<div class="sub-menu">'. "\n\t";
            $output .= "\n". $indent .'<div class="sub-menu-wrapper">'. "\n\t";
        }
        elseif($display_depth == 2)
            $output .= "\n". $indent .'<ul class="col-links list-style-none">'. "\n";
    }

    function end_lvl(&$output,$depth = 0, $args = array()) {
        $display_depth = ($depth + 1);

        if($display_depth == 1)
        {
            $output .= "\n</div>\n";
            $output .= "\n</div>\n";
        }
        elseif($display_depth == 2)
            $output .= "\n</ul>\n";
    }

    function start_el(&$output, $item, $depth = 0, $args=array(), $id=0) {
        global $wp_query;
        $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
        $display_depth = ($depth + 1);
        
        $class_names = '';

        $classes = empty( $item->classes ) ? array() : (array) $item->classes;
        $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
        if($display_depth != 2) {
            $class_names = ' class="' . esc_attr( $class_names ) . '"';
            $output .= $indent . '<li id="menu-item-'. $item->ID;
            if($display_depth == 3) $output .= '">';
            else $output .= '"'. $class_names .'>';
        }
        else {
            $class_names = ' class="col-title condensed"';
            // if($this->index_sub_menu_col != 2) // && $this->index_sub_menu_col != 3
                $output .= $indent . '<div class="sub-menu-list fl"><div id="menu-item-'. $item->ID . '"' . $class_names .'>';
            /*else
                $output .= $indent . '<div id="menu-item-'. $item->ID . '"' . $class_names .'>';*/
        }

        $attributes = ! empty( $item->attr_title ) ? ' title="' . esc_attr( $item->attr_title ) .'"' : '';
        $attributes .= ! empty( $item->target ) ? ' target="' . esc_attr( $item->target ) .'"' : '';
        $attributes .= ! empty( $item->xfn ) ? ' rel="' . esc_attr( $item->xfn ) .'"' : '';
        $attributes .= ! empty( $item->url ) ? ' href="' . esc_attr( $item->url ) .'"' : '';

        $item_output = $args->before;
        $item_output .= '<a'. $attributes .'>';
        $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
        $item_output .= '</a>';
        if($display_depth == 2/* && ($this->index_sub_menu_col != 1)*//* && $this->index_sub_menu_col != 2)*/) 
            $item_output .= ' </div>';
        $item_output .= $args->after;

        $this->index_sub_menu_col++;

        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }

    function end_el(&$output, $item, $depth = 0, $args=array()) {
        $display_depth = ($depth + 1);
        if($display_depth == 2)
            $output .= "</div>";
    }
}
class Mobile_Nav_Menu extends Walker_Nav_Menu {
    public $index_sub_menu_col = 1;
    function start_lvl(&$output, $depth = 0, $args = array() ) {
        $indent = ($depth > 0  ? str_repeat("\t", $depth) : ''); // code indent

        $display_depth = ($depth + 1);

        if($display_depth == 1) {
            $this->index_sub_menu_col = 1;
            $output .= "\n". $indent .'<ul class="sub-menu-mobile">'. "\n\t";
            $output .= "\n". $indent .'<ul class="sub-menu-wrapper-mobile">'. "\n\t";
        }
        elseif($display_depth == 2)
            $output .= "\n". $indent .'<ul class="col-links list-style-none">'. "\n";
        
    }

    function end_lvl(&$output,$depth = 0, $args = array()) {
        $display_depth = ($depth + 1);

        if($display_depth == 1)
        {
            $output .= "\n</ul>\n";
            $output .= "\n</ul>\n";
        }
        elseif($display_depth == 2)
            $output .= "\n</ul>\n";
    }

    function start_el(&$output, $item, $depth = 0, $args=array(), $id=0) {
        global $wp_query;
        $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
        $display_depth = ($depth + 1);
        
        $class_names = '';

        $classes = empty( $item->classes ) ? array() : (array) $item->classes;
        $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
        if($display_depth != 2) {
            $class_names = ' class="' . esc_attr( $class_names ) . '"';
            $output .= $indent . '<li id="menu-item-'. $item->ID;
            if($display_depth == 3) $output .= '">';
            else $output .= '"'. $class_names .'>';
        }
        else {
            $class_names = ' class="col-title condensed"';
            // if($this->index_sub_menu_col != 2) // && $this->index_sub_menu_col != 3
                $output .= $indent . '<li class="sub-menu-list fl"><div id="menu-item-'. $item->ID . '"' . $class_names .'>';
            /*else
                $output .= $indent . '<div id="menu-item-'. $item->ID . '"' . $class_names .'>';*/
        }

        $attributes = ! empty( $item->attr_title ) ? ' title="' . esc_attr( $item->attr_title ) .'"' : '';
        $attributes .= ! empty( $item->target ) ? ' target="' . esc_attr( $item->target ) .'"' : '';
        $attributes .= ! empty( $item->xfn ) ? ' rel="' . esc_attr( $item->xfn ) .'"' : '';
            //$attributes .= ! empty( $item->url ) ? ' href="' . esc_attr( $item->url ) .'"' : '';
        $attributes .= ! empty( $item->url ) ? ' href="' . esc_attr( $item->url ) .'"' : '';
        $item_output = $args->before;
        $item_output .= '<a'. $attributes .'>';
        $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
        $item_output .= '</a>';
        if($display_depth == 2/* && ($this->index_sub_menu_col != 1)*//* && $this->index_sub_menu_col != 2)*/) 
            $item_output .= ' </div>';
        $item_output .= $args->after;

        $this->index_sub_menu_col++;

        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }

    function end_el(&$output, $item, $depth = 0, $args=array()) {
        $display_depth = ($depth + 1);
        if($display_depth == 2)
            $output .= "</li>";
    }
}

class Footer_Bot_Menu extends Walker_Nav_Menu {
    function start_lvl(&$output, $depth = 0, $args = array()) {
        $indent = ($depth > 0  ? str_repeat("\t", $depth) : ''); // code indent
        $display_depth = ($depth + 1);

        if($display_depth == 1)
            $output .= "\n". $indent .'<ul class="col-links list-style-none">'. "\n";
    }

    function end_lvl(&$output, $depth = 0, $args = array()) {
        $display_depth = ($depth + 1);

        if($display_depth == 1)
            $output .= "\n</ul>\n";
    }

    function start_el(&$output, $item, $depth = 0, $args=array(),$id=0) {
        global $wp_query;
        $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
        $display_depth = ($depth + 1);
        
        $class_names = '';

        $classes = empty( $item->classes ) ? array() : (array) $item->classes;
        $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
        if($display_depth != 1) {
            $class_names = ' class="' . esc_attr( $class_names ) . '"';
            $output .= $indent . '<li id="menu-item-'. $item->ID;
            if($display_depth == 2) $output .= '">';
            else $output .= '"'. $class_names .'>';
        }
        else {
            $class_names = ' class="col-title"';
            $output .= $indent . '<div id="menu-item-'. $item->ID . '"' . $class_names .'>';
        }

        $attributes = ! empty( $item->attr_title ) ? ' title="' . esc_attr( $item->attr_title ) .'"' : '';
        $attributes .= ! empty( $item->target ) ? ' target="' . esc_attr( $item->target ) .'"' : '';
        $attributes .= ! empty( $item->xfn ) ? ' rel="' . esc_attr( $item->xfn ) .'"' : '';
        $attributes .= ! empty( $item->url ) ? ' href="' . esc_attr( $item->url ) .'"' : '';

        $item_output = $args->before;
        $item_output .= '<a'. $attributes .'>';
        $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
        $item_output .= '</a>';
        if($display_depth == 1) $item_output .= '</div>';
        $item_output .= $args->after;

        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }
}

/* Menus */
register_nav_menus(array(
    'left-main-menu' => 'Left Main Menu',
    'right-main-menu' => 'Right Main Menu',
    'sidebar-category-menu' => 'Sidebar Category Menu',
    'sidebar-jobs-menu' => 'Sidebar Job Menu',
    'sidebar-styleguide-menu' => 'Sidebar Style Guide Menu',
    'top-left-menu' => 'Top Left Menu',
    'footer-top-menu' => 'Footer Top Menu',
    'footer-bot-menu-col-1' => 'Footer Bot Menu - Col 1',
    'footer-bot-menu-col-2' => 'Footer Bot Menu - Col 2',
    'footer-bot-menu-col-3' => 'Footer Bot Menu - Col 3',
    'footer-bot-menu-col-4' => 'Footer Bot Menu - Col 4',
    'footer-bot-menu-col-5' => 'Footer Bot Menu - Col 5',

    'main-menu-mobile' => 'Main Menu Mobile',
    'main-menu-mobile-2' => 'Main Menu Mobile 2',
    'main-menu-mobile-3' => 'Main Menu Mobile 3'
));

/* Adding Enqueue scripts */
add_action('wp_enqueue_scripts', 'enqueue_scripts_styles');
function enqueue_scripts_styles() {
    $useragent=$_SERVER['HTTP_USER_AGENT'];
    if (!isset($_SERVER['HTTP_USER_AGENT']) || stripos($_SERVER['HTTP_USER_AGENT'], 'Chrome-Lighthouse') === false){
        wp_enqueue_style('addons', get_template_directory_uri() . '/assets/css/addons.css', array(), '1.0');
        wp_enqueue_style('iris');
        wp_enqueue_style('jquery-ui-css', get_template_directory_uri() .'/assets/css/jquery-ui.min.css', array(), '1.11.2');

        wp_enqueue_style('fontawesome', get_template_directory_uri() .'/assets/css/font-awesome.min.css', array(), '4.6.3');
        wp_enqueue_style('owl-carousel-style', get_template_directory_uri() .'/assets/css/owl.carousel.css', array(), '1.3.3');

        wp_enqueue_style('styles', get_stylesheet_uri(), array(), time());
        wp_enqueue_style('responsive', get_template_directory_uri() .'/assets/css/responsive.css', array('styles'), time());


        wp_enqueue_script('jquery');
        wp_enqueue_script('jquery-ui-core');
        wp_enqueue_script('jquery-ui-datepicker');
        wp_enqueue_script('addons', get_template_directory_uri() . '/assets/js/addons.js', array(), '1.0');
        wp_enqueue_script('angular', get_template_directory_uri() . '/assets/js/angular.min.js', array(), '1.5');
        wp_enqueue_script('main', get_template_directory_uri() . '/assets/js/main.js', array(), '1.0');
        wp_enqueue_script('google-map', 'https://maps.google.com/maps/api/js', array(), '3.0', false);
        wp_enqueue_script('bootstrap', get_template_directory_uri() . '/assets/js/bootstrap.min.js');
        wp_enqueue_script('owl-carousel-js', get_template_directory_uri() .'/assets/js/owl.carousel.min.js', array(), '1.3.3', false);
    }
    wp_enqueue_style('bootstrap', get_template_directory_uri() .'/assets/css/bootstrap.min.css', array(), time());
    

    if(is_page(167)){
        wp_enqueue_script('cart', get_template_directory_uri() . '/assets/js/cart.js', array(), '1.1');
    }

    if(is_single()){
        wp_enqueue_script('suggestjs', get_template_directory_uri() . '/assets/js/suggest-size.js', array(), '1.0');
        wp_enqueue_script('productjs', get_template_directory_uri() . '/assets/js/product.js', array(), '1.0');
    }

    wp_localize_script('main', 'wp_vars', array( 
        'ajaxurl' => admin_url('admin-ajax.php'),
        'current' => home_url( $_SERVER['REQUEST_URI'] ),
    ));

    wp_localize_script('cart', 'wp_vars', array( 
        'ajaxurl' => admin_url('admin-ajax.php')
    ));

    wp_localize_script('addons', 'wp_vars', array( 
        'ajaxurl' => admin_url('admin-ajax.php')
    ));
}

add_action('admin_enqueue_scripts', 'admin_enqueue_scripts_styles');
function admin_enqueue_scripts_styles($hook) {
    wp_enqueue_script( 'wp-color-picker' );
    wp_enqueue_style( 'wp-color-picker' );
    wp_enqueue_style( 'datetimepicker-style', get_template_directory_uri() .'/assets/css/jquery.datetimepicker.css', array(), '2.4.1');
    wp_enqueue_style( 'select2-style', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css', array(), '4.0.0');
    
    wp_enqueue_script('addons', get_template_directory_uri() . '/assets/js/functions.js', array(), '1.0');
    wp_enqueue_script('angularjs-lib', get_template_directory_uri() . '/assets/js/angular.min.js', array('jquery'), '1.2.27',true);
    wp_enqueue_script('angularjs-sync', get_template_directory_uri() . '/assets/js/admin-sync.js', array('angularjs-lib'), '1.0.0',true);
    wp_enqueue_script('datetimepicker', get_template_directory_uri() .'/assets/js/jquery.datetimepicker.js', array('jquery'), '2.4.1', true);
    wp_enqueue_script('select2', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js', array('jquery'), '4.0.0', true);
    wp_enqueue_script('chartjs', get_template_directory_uri() .'/assets/js/chart.js', array(), '1.0.2', false);
    
    wp_enqueue_script('cryptoJs','https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.9-1/crypto-js.min.js',array(),'3.1.9-1',true);
    wp_localize_script('angularjs-sync', 'wp_js_vars', array( 
        'ajaxurl' => admin_url('admin-ajax.php')
    ));

    wp_enqueue_media();
}

/* Thumbnails - Images */
if ( function_exists( 'add_theme_support' ) ) {
    add_theme_support('post-thumbnails');
    add_image_size('thumbnail', 200, 200, true);
    add_image_size('thumbnail-post', 275, 300, true);
    add_image_size('thumbnail-news', 275, 250, true);
    add_image_size('thumbnail-collection', 363, 470, true);
    add_image_size('thumbnail-video', 363, 222, true);
    add_image_size('medium', 768, 0);
    add_image_size('large', 1024, 0);
    add_image_size('detail-post', 456, 583, true);
}

function is_admin_editor() {
    if(!current_user_can('contributor') && !current_user_can('author')) return true;
    return false;
}

function is_admin_author() {
    if(!current_user_can('author')) return true;
    return false;
}

function is_admin_author_contributor() {
    if(!current_user_can('editor')) return true;
    return false;
}

function is_author_m() {
    if(current_user_can('administrator') )
        return false;
    if(!current_user_can('editor') && !current_user_can('contributor') ) return true;
    return false;
}

function is_contributor_m() {
    if(current_user_can('administrator'))
        return false;
    if(!current_user_can('editor') && !current_user_can('author')) return true;
    return false;
}

function is_subscriber(){
    if(current_user_can('subscriber'))
        return true;
    return false;
}

// function is_author(){
//     if(current_user_can(''))
//         return false;
//     return true;

// }