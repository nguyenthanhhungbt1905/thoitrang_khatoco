<?php 
// http://cloudsms.vietguys.biz:8088/api/?u=KHATOCO&pwd=b3p4m&from=M.KHATOCO&phone=841675251288&sms=test

$sms_host = 'http://sms.vietguys.biz/api/?';

$_settings_sms = array(
	'u' => 'KHATOCO',
	'pwd' => 'b3p4m',
	'from' => 'M.KHATOCO',
	'phone' => '',
	'sms' => 'Ma xac minh cua ban la %s'
);

$salt_sms = 'vU2TLcJg2aWx5KNRUgC7';

function generate_hash_code(){
	if(!isset($_SESSION))
		session_start();

	global $salt_sms;
	$pre_hash = $salt_sms . time();
	$hash = substr(md5(md5($pre_hash)), 0, 6);

	$_SESSION['otp_hash_code_checkvip'] = $hash;
	return $hash;
}

// var_dump(send_hash_code('http://cloudsms.vietguys.biz:8088/api/?u=KHATOCO&pwd=b3p4m&from=M.KHATOCO&phone=841675251288', 'abc'));
// var_dump(send_hash_code('http://sms.vietguys.biz/api/?u=KHATOCO&pwd=b3p4m&from=M.KHATOCO&phone=841675251288&sms=test', 'abc'));

// Core OTP
function send_hash_code($url, $hash){
	if(!$url) return false;

	$return = array(
		'result' => 1,
		'message' => 'Gửi thành công'
	);

	// $result = file_get_contents($url);
	$result = wp_remote_get($url);

	if(is_wp_error( $result )){
		return false;
	}
	else{
		$result = $result['body'];
	}
	
	switch($result){
		case '-1':
			$return['result'] = 0;
			$return['message'] = 'Thiếu thông số';
			break;
		case '-2':
			$return['result'] = 0;
			$return['message'] = 'Không thể kết nối đến máy chủ';
			break;
		case '-3':
			$return['result'] = 0;
			$return['message'] = 'Thông tin tài khoản không chính xác';
			break;
		case '-4':
			$return['result'] = 0;
			$return['message'] = 'Tài khoản bị khoá';
			break;
		case '-5':
			$return['result'] = 0;
			$return['message'] = 'Thông tin xác nhận tài khoản không chính xác';
			break;
		case '-6':
			$return['result'] = 0;
			$return['message'] = 'API chưa được kích hoạt';
			break;
		case '-7':
			$return['result'] = 0;
			$return['message'] = 'IP này bị giới hạn truy cập';
			break;
		case '-8':
			$return['result'] = 0;
			$return['message'] = 'Sai thuộc tính Đơn vị gửi';
			break;
		case '-9':
			$return['result'] = 0;
			$return['message'] = 'Tài khoản hết credits';
			break;
		case '-10':
			$return['result'] = 0;
			$return['message'] = 'Số điện thoại chưa chính xác. Đăng nhập để chỉnh sửa lại!';
			break;
		case '-11':
			$return['result'] = 0;
			$return['message'] = 'Số điện thoại nằm trong blacklist';
			break;
		case '-12':
			$return['result'] = 0;
			$return['message'] = 'Tài khoản không đủ credits để thực hiện gửi tin nhắn';
			break;
	}

	return $return;
}

function compare_hash_code($text_entered){
	if(!isset($_SESSION))
		session_start();
	
	if(!isset($_SESSION['otp_hash_code_checkvip'])) return false;
	if(!$text_entered) return false;

	$text_entered = sanitize_text_field( $text_entered );

	if($text_entered == $_SESSION['otp_hash_code_checkvip']){
		unset($_SESSION['otp_hash_code_checkvip']);
		return true;
	}
	return false;
}

function vip_checker_pre_send_hash_code($phone){
	global $_settings_sms, $sms_host;
	$settings = $_settings_sms;
	$url = $sms_host;
	$hash = generate_hash_code();

	$settings['phone'] = '84'. substr($phone, 1);
	$settings['sms'] = sprintf($settings['sms'], $hash);

	foreach ($settings as $_key => $_v) {
		$url .= $_key .'='. urlencode($_v) .'&';
	}

	$url = substr($url, 0, strlen($url)-1);
	return send_hash_code($url, $hash);
}

