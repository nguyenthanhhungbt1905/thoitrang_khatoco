<?php
/* Campaign admin Service */
add_action('wp_ajax_camp_admin_service_ajax', 'camp_admin_service_ajax');
function camp_admin_service_ajax() {

    $do_ajax = isset($_GET['do_ajax']) ? esc_js(sanitize_text_field($_GET['do_ajax'])) : '';

    if ($do_ajax == 'get_camp_info') {
        $post_id = $_GET['postID'];
        get_camp_info_ajax($post_id);
    }
    else if ($do_ajax == 'validate_camp_submit'){
        parse_str($_GET['data'],$data);
        validate_submit_camp_ajax($data);
    }
    exit;
}

function get_camp_info_ajax($post_id) {
    
    $promotion_campaign = get_post_meta($post_id, 'campaign', true);
    $post = get_post($post_id);

    $promotion_campaign_default = new stdClass();
    $promotion_campaign_default->name = 'Tên chương trình khuyến mãi';
    $promotion_campaign_default->description = '';
    $promotion_campaign_default->dateAfter = '2016/10/20 00:00';
    $promotion_campaign_default->dateBefore = '2016/12/20 00:00';
    $promotion_campaign_default->saleType = 'card';
    $promotion_campaign_default->total = 0;
    $promotion_campaign_default->title = 'Title';


    if ($promotion_campaign == '') {
        $promotion_campaign = $promotion_campaign_default;
    }

    if (!isset($promotion_campaign->id)) {
        $promotion_campaign->id = $post_id;
    }

    if (empty($promotion_campaign->title))
        $promotion_campaign->title = $post->post_title;
    if (!empty($promotion_campaign->priority))
        $promotion_campaign->priority = intval($promotion_campaign->priority);

    echo json_encode($promotion_campaign);
    exit;
}

function validate_submit_camp_ajax($dataSubmit){
    $camp = $dataSubmit['promotion_campaign'];
    $saleType = $camp['sale']['type'];

    if ($saleType === 'card' && empty($camp['data_sale_card'])){
        echo 'Lỗi chưa nhập thông tin vào chiến dịch [ Mục tiêu : Thẻ thành viên ]';
        exit;
    }
    else if ( $saleType === 'order' && empty($camp['data_sale_order']) ) {
        echo 'Lỗi chưa nhập thông tin vào chiến dịch [ Mục tiêu : Đơn hàng ]';
        exit;
    }
    else if ( $saleType === 'product' && empty($camp['data_sale_prod']) ) {
        echo 'Lỗi chưa nhập thông tin vào chiến dịch [ Mục tiêu : Sản phẩm độc lập ]';
        exit;
    }
    else if ( $saleType === 'multi_product' && empty($camp['data_sale']) ) {
        echo 'Lỗi chưa nhập thông tin vào chiến dịch [ Mục tiêu : Mua N sản phẩm ]';
        exit;
    }
    else if ( $saleType === 'buy_money_product' && empty($camp['data_sale_money_prod']) ) {
        echo 'Lỗi chưa nhập thông tin vào chiến dịch [ Mục tiêu : Mua X đồng trên sản phẩm ]';
        exit;
    }
    else if ( ( $saleType === 'multi_product' || $saleType === 'buy_money_product' )  && !empty($camp['data_sale']) ) {
        if ( $camp['form'] === 'bonus' && empty($camp['data_sale_bonus']) ) {
            echo 'Lỗi chưa nhập thông tin vào hình thức khuyến mãi [ Tặng sản phẩm ]';
            exit;
        }
        else if ( $camp['form'] === 'special-price' && empty($camp['data_sale_price']) ) {
            echo 'Lỗi chưa nhập thông tin vào hình thức khuyến mãi [ Bán với giá đặc biệt ]';
            exit;
        }
    }

    echo '';
    exit;
}