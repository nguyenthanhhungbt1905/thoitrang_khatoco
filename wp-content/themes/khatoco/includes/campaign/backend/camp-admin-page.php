<?php
$post_id = $post->ID;
$products = get_posts(array('post_type' => 'post', 'posts_per_page' => -1));

$promotion_campaign = get_post_meta($post_id, 'campaign', true);

$promotion_campaign_default = new stdClass();
$promotion_campaign_default->name = 'Tên chương trình khuyến mãi';
$promotion_campaign_default->description = '';
$promotion_campaign_default->dateAfter = '2016/10/20 00:00';
$promotion_campaign_default->dateBefore = '2016/12/20 00:00';
$promotion_campaign_default->saleType = 'card';
$promotion_campaign_default->total = 0;


if ($promotion_campaign == '') {
    $promotion_campaign = $promotion_campaign_default;
}

if (!isset($promotion_campaign->id)) {
    $promotion_campaign->id = $post_id;
}
?>
    <div>
        <div>
            <div class="loading" ng-show="isLoading"> <!--loading page screen-->
                <div class="windows8">
                    <div class="wBall" id="wBall_1">
                        <div class="wInnerBall"></div>
                    </div>
                    <div class="wBall" id="wBall_2">
                        <div class="wInnerBall"></div>
                    </div>
                    <div class="wBall" id="wBall_3">
                        <div class="wInnerBall"></div>
                    </div>
                    <div class="wBall" id="wBall_4">
                        <div class="wInnerBall"></div>
                    </div>
                    <div class="wBall" id="wBall_5">
                        <div class="wInnerBall"></div>
                    </div>
                </div>
                <span class="descr">Đang tải dữ liệu ...</span>
            </div>

            <input type="hidden" name="promotion_campaign[id]" id="promotion_campaign_id"
                   ng-model="camp.id">

            <!-- Begin field standard -->
            <table class="form-table">
                <tr>
                    <th><label for="promotion_campaign_name">Tên chương trình *</label></th>
                    <td><input type="text" class="regular-text" name="promotion_campaign[name]"
                               id="promotion_campaign_name" ng-model="camp.name" required></td>
                </tr>
                <tr>
                    <th><label for="promotion_campaign_description">Mô tả chương trình</label></th>
                    <td><textarea name="promotion_campaign[description]" id="promotion_campaign_description"
                                  cols="45"
                                  rows="5">{{ camp.description}}</textarea></td>
                </tr>
                <tr>
                    <th><label for="promotion_campaign_date_start">Thời gian chương trình *</label></th>
                    <td>
                        <input type="text" class="datetime_picker" name="promotion_campaign[date][after]"
                               id="promotion_campaign_date_start"
                               ng-model="camp.dateAfter"
                               required>
                        <span>&nbsp;đến&nbsp;</span>
                        <input type="text" class="datetime_picker" name="promotion_campaign[date][before]"
                               id="promotion_campaign_date_end"
                               ng-model="camp.dateBefore"
                               required>
                        <span id="date-message"></span>
                    </td>
                </tr>

                <!-- Option for campaign -->
                <tr>
                    <th><label for="">Mục tiêu áp dụng</label></th>
                    <td>
                        <p style="margin: 0;">
                            <label for="promotion_campaign_type_card">
                                <input type="radio" ng-model="camp.saleType" name="promotion_campaign[sale][type]"
                                       ng-checked="camp.saleType==='card'" value="card"
                                       id="promotion_campaign_type_card" class="promotion_campaign_type">
                                &nbsp;Theo thẻ thành viên
                            </label>
                            <br/>
                            <label for="promotion_campaign_type_orders">
                                <input type="radio" ng-model="camp.saleType" name="promotion_campaign[sale][type]"
                                       ng-checked="camp.saleType==='order'" value="order"
                                       id="promotion_campaign_type_orders" class="promotion_campaign_type">
                                &nbsp;Theo đơn hàng
                            </label>
                            <br/>
                            <label for="promotion_campaign_type_product">
                                <input type="radio" ng-model="camp.saleType" name="promotion_campaign[sale][type]"
                                       ng-checked="camp.saleType==='product'" value="product"
                                       id="promotion_campaign_type_product" class="promotion_campaign_type">
                                &nbsp;Theo sản phẩm độc lập
                            </label>
                            <br/>
                            <label for="promotion_campaign_type_multi_product">
                                <input type="radio" ng-model="camp.saleType" name="promotion_campaign[sale][type]"
                                       ng-checked="camp.saleType==='multi_product'" value="multi_product"
                                       id="promotion_campaign_type_multi_product" class="promotion_campaign_type">
                                &nbsp;Mua N sản phẩm
                            </label>
                            <br/>
                            <label for="promotion_campaign_type_buy_money_product">
                                <input type="radio" ng-model="camp.saleType" name="promotion_campaign[sale][type]"
                                       ng-checked="camp.saleType==='buy_money_product'" value="buy_money_product"
                                       id="promotion_campaign_type_buy_money_product"
                                       class="promotion_campaign_type">
                                &nbsp;Mua X đồng trên sản phẩm
                            </label>
                        </p>
                        <table class="form-table" id="buy_money_product_table" style="display:none">
                            <tr>
                                <td>
                                    <label for="promotion_campaign_type_buy_money_product_price">
                                        <input type="radio" name="buy_money_product" value="buy_money_product_price"
                                               class="buy_money_product_option">
                                        &nbsp;Mua sản phẩm với giá đặc biệt
                                    </label>
                                </td>
                                <td>
                                    <label for="promotion_campaign_type_buy_money_product_gift">
                                        <input type="radio" name="buy_money_product" value="buy_money_product_gift"
                                               class="buy_money_product_option">
                                        &nbsp;Tặng N sản phẩm
                                    </label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="promotion_campaign_multiple_event">Không áp dụng cho các chương trình
                            khác</label>
                    </th>
                    <td>
                        <input type="checkbox" name="promotion_campaign[multiple_event]"
                               id="promotion_campaign_multiple_event" ng-checked="camp.multiEvent == 'on'"
                               ng-model="camp.multiEvent">
                    </td>
                </tr>
                <tr>
                    <th><label for="promotion_campaign_sort_order_event">Thứ tự ưu tiên</label></th>
                    <td>
                        <input type="number" name="promotion_campaign[sort_order_event]" min="0"
                               id="promotion_campaign_sort_order_event" ng-model="camp.priority"
                               ng-disabled="!camp.multiEvent">
                    </td>

                </tr>

                <tr ng-show="camp.saleType != 'card' && camp.saleType!='product'">
                    <th><label>Áp dụng theo thẻ thành viên</label></th>
                    <td>
                        <div class="form-inline loop-block">
                            <input type="checkbox" id="no-card" class="form-control"
                                   name="promotion_campaign[apply_by_card][]" value="0" ng-model="applyCard.noCard">
                            <label for="no-card">Không có thẻ</label>
                        </div>

                        <div class="form-inline loop-block">
                            <input type="checkbox" id="silver-card" class="form-control"
                                   name="promotion_campaign[apply_by_card][]" value="1"
                                   ng-model="applyCard.silverCard">
                            <label for="silver-card">Thẻ bạc</label>
                        </div>

                        <div class="form-inline loop-block">
                            <input type="checkbox" id="gold-card" class="form-control"
                                   name="promotion_campaign[apply_by_card][]" value="2"
                                   ng-model="applyCard.goldCard">
                            <label for="gold-card">Thẻ vàng</label>
                        </div>

                        <div class="form-inline loop-block">
                            <input type="checkbox" id="diamond-card" class="form-control"
                                   name="promotion_campaign[apply_by_card][]" value="3"
                                   ng-model="applyCard.diamondCard">
                            <label for="diamond-card">Thẻ bạch kim</label>
                        </div>
                    </td>
                </tr>

                <tr ng-show="camp.saleType!='product'">
                    <th><label>Áp dụng theo thanh toán online</label></th>
                    <td>
                        <div class="form-inline loop-block">
                            <input type="checkbox" id="internal" class="form-control"
                                   name="promotion_campaign[apply_by_payment][]" value="2"
                                   ng-model="applyPayment.internal">
                            <label for="internal">Thanh toán nội địa</label>
                        </div>

                        <div class="form-inline loop-block">
                            <input type="checkbox" id="internation" class="form-control"
                                   name="promotion_campaign[apply_by_payment][]" value="3"
                                   ng-model="applyPayment.international">
                            <label for="internation">Thanh toán quốc tế</label>
                        </div>
                    </td>
                </tr>
            </table>


            <h4 class='target-config header-target'>Thiết lập mục tiêu và hình thức khuyến mãi</h4>
            <div class='form-sale-config' ng-show="camp.saleType === 'multi_product' || camp.saleType === 'buy_money_product'">
                <h4 class='form-sale header-target' ng-show="camp.saleType === 'multi_product' || camp.saleType === 'buy_money_product'">Hình thức khuyến mãi</h4>
                <div class='select-form'>
                    <label><input type="radio" name='promotion_campaign[form]' class='form-control' value='bonus'
                                  checked="checked" ng-model="camp.form"> Tặng sản phẩm</label>
                    <label><input type="radio" name='promotion_campaign[form]' class='form-control'
                                  value='special-price' ng-model="camp.form"> Bán với giá đặc biệt</label>
                </div>
                <div class="form-inline" style="text-align: center">
                    <label class="total-campaign">Tổng số lượng</label>
                    <input type="number" min="0" max="9999" name='promotion_campaign[total]'
                           class='form-control flat-control' ng-model="camp.total">

                </div>
            </div>
            <?php
            $dataCampTableCard = new DataCampTableCard();
            $dataCampTableOrder = new DataCampTableOrder();
            $dataCampTableProduct = new DataCampTableProduct();
            $dataCampTableMultiProduct = new DataCampTableMultiProduct();
            $dataCampTableMoneyProduct = new DataCampTableMoneyProduct();
            $dataCampTableBonus = new DataCampTableBonus();
            $dataCampTablePrice = new DataCampTableSpecialPrice();


            // This function from abstract class data-table-module.php
            $dataCampTableCard->generate_camp_table();
            $dataCampTableOrder->generate_camp_table();
            $dataCampTableProduct->generate_camp_table();
            $dataCampTableMultiProduct->generate_camp_table();
            $dataCampTableMoneyProduct->generate_camp_table();
            $dataCampTableBonus->generate_camp_table();
            $dataCampTablePrice->generate_camp_table();
            ?>

            <br><br><br><br>
        <pre>
            <?php print_r($promotion_campaign); ?>
        </pre>
        </div>
    </div>
    <script>
        var postIDCampaign = <?php echo $post_id; ?>; // use in admin-campaign.js
        jQuery(document).ready(function ($) {
            // Prepare data for angularjs

            $('#publish').attr('submit-event','');
            $('html').attr('ng-app','campaignAdmin');
            $('body').attr('ng-controller','campaignAdminController');
            $('input[name=\'post_title\']').attr('ng-model','camp.title');
            $('label#title-prompt-text').hide();

            $('.datetime_picker').datetimepicker({
                defaultTime: '00:00',
                lang: 'vi'
            });
            $('.select-two').select2({theme: "classic"});

            function orderElement() { // sắp xếp lại các element của các table campaign
                $('.form-sale.header-target').insertBefore($('#wrap-table-camp-campaign_bonus_tb'));
                $('.form-sale-config').insertAfter($('.form-sale.header-target'));
            }

            orderElement();
        });
    </script>


<?php
$dataCampTableMultiProduct->generate_script_camp_table();

$dataCampTableCard->generate_logic_script_table();
$dataCampTableOrder->generate_logic_script_table();
$dataCampTableProduct->generate_logic_script_table();
$dataCampTableMultiProduct->generate_logic_script_table();
$dataCampTableMoneyProduct->generate_logic_script_table();
$dataCampTableBonus->generate_logic_script_table();
$dataCampTablePrice->generate_logic_script_table();