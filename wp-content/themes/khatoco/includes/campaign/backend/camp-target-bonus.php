<?php

class DataCampTableBonus extends DataCampTable {

    private $list_product_camp = array();
    private $data_camp_db = null;
    private $config = array(
        'btn' => 'campaign_bonus_btn',
        'btn_text' => 'mục tiêu',
        'type_offer' => 'bonus',
        'camp_tb' => 'campaign_bonus_tb'
    ); // Shouldn't change value , it effect to jQuery script variable
    private $headers = array('DANH MỤC SẢN PHẨM TẶNG');
    private $data = array();

    public function __construct() {
        global $post;
        $this->data_camp_db = get_post_meta($post->ID, 'campaign', true);
        $this->list_product_camp = get_all_product_min();
        $this->data = empty($this->data_camp_db->saleData) ? null : $this->data_camp_db->saleData;
        parent::__constructor($this->config, $this->headers, $this->data);
    }

    protected function create_popup_camp($is_edit_popup = false) {
        $id = 'bonus_field';
        if ($is_edit_popup) {
            $text = 'edit';
        } else {
            $text = 'add';
        }

        ob_start();
        ?>

        <div class='form-group clearfix'>
            <label class='col-md-3'>Sản phẩm</label>
            <span class='col-md-9'>
                <select autocomplete="off" class='form-control' style="width: 100%" name='<?php echo $id . '_product_' . $text; ?>' id='<?php echo $id . '_product_' . $text; ?>'>
                    <?php foreach ($this->list_product_camp as $product) : ?>
                        <option value='<?php echo $product->ID; ?>'><?php echo $product->post_title; ?></option>
                    <?php endforeach; ?>
                </select>
            </span>
        </div>

        <?php
        return ob_get_clean();
    }

    protected function convertDataList() { // Convert data of CampSaleData -> JSon can map into table
        if (!empty($this->data_camp_db->bonus)) {
            $result = array();
            foreach ($this->data_camp_db->bonus as $d) :
                $temp = new stdClass();
                $temp->key = $d;
                $temp->value = get_post($d)->post_title;
                $result[] = $temp;
            endforeach;
            return $result;
        } else
            return null;
    }

    public function generate_logic_script_table() {
        ?>
        <!--LOGIC -->
        <script>
            var table_campaign_bonus_tb = null;
            jQuery(document).ready(function () {
                data_campaign_bonus_tb = <?php echo json_encode($this->convertDataList()); ?>;
                table_campaign_bonus_tb = jQuery('#<?php echo $this->config['camp_tb']; ?>').DataTable({
                    bFilter: false,
                    bInfo: false,
                    paging: false,
                    scrollY: '50vh',
                    select: 'single',
                    scrollCollapse: true,
                    data: data_campaign_bonus_tb,
                    columns: [
                        {title: "Danh sách sản phẩm tặng", defaultContent: "<i>Not set</i>",data:null, render: function (data, type, full, meta) {
                                return render_hidden(data.key, data.value, 'promotion_campaign[data_sale_bonus][' + meta.row + ']');
                            }
                        }
                    ]
                });
            });
            jQuery('#subm_add_campaign_bonus_btn').on('click', function (e) {
                subm_camp_bonus();
            });

            function subm_camp_bonus() {
                var _popup = jQuery('#campaign_bonus_tb_add_popup');
                var _prod_select = jQuery('#bonus_field_product_add');

                if (_prod_select.val() === undefined || _prod_select.val() === null || _prod_select.val().length == 0)
                {
                    bs_error.set_error('* Bạn phải chọn dữ liệu nhập vào danh sách sản phẩm khuyến mãi');
                    return false;
                }
                var array_commit = null;
                    array_commit = parse_element_to_obj(_prod_select, 'select');

                table_campaign_bonus_tb.row.add(array_commit).draw();
                refreshDataTable(table_campaign_bonus_tb);
                _popup.modal('toggle');
                return true;
            }
        </script>
        <?php
    }

}

