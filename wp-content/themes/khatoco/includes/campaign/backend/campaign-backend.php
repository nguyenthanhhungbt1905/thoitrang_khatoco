<?php
/**
 *  Begin form create campain
 */
add_action('admin_init', 'add_metabox_campaign', 9);

function add_metabox_campaign()
{
    add_meta_box('promotion-campaign', 'Chi tiết chương trình khuyến mại', 'display_metabox_campaign_general', 'campaign', 'normal', 'high');

    function display_metabox_campaign_general($post)
    {
        require_once (get_template_directory() . '/includes/campaign/backend/camp-admin-page.php');
    }

}

/* Register Rate Type */
add_action('init', 'reg_post_type_campaign');

function reg_post_type_campaign()
{
    $labels = array(
        'name' => 'Chương trình khuyến mại',
        'singular_name' => 'Chương trình khuyến mại',
        'menu_name' => 'Chương trình khuyến mại',
        'all_items' => 'Tất cả chương trình khuyến mại',
        'add_new' => 'Thêm mới',
        'add_new_item' => 'Thêm mới chương trình khuyến mại',
        'edit_item' => 'Xem chương trình khuyến mại',
        'new_item' => 'chương trình khuyến mại mới',
        'view_item' => 'Xem chi tiết',
        'search_items' => 'Tìm kiếm',
        'not_found' => 'Không tìm thấy bài chương trình khuyến mại nào',
        'not_found_in_trash' => 'Không có bài chương trình khuyến mại nào trong thùng rác',
        'view' => 'Xem chương trình khuyến mại'
    );

    $args = array(
        'labels' => $labels,
        'description' => 'Chương trình khuyến mại của Khatoco',
        'public' => false,
        'publicly_queryable' => true,
        'exclude_from_search' => true,
        'show_in_nav_menus' => false,
        'show_ui' => true,
        'rewrite' => false,
        'supports' => array('title'),
        'menu_position' => 62,
        'menu_icon' => 'dashicons-awards',
        
    );
    register_post_type('campaign', $args);
}

add_filter('manage_edit-campaign_columns', 'campaign_columns_head');

function campaign_columns_head($defaults)
{
    $defaults = array_slice($defaults, 0, 2, true) + array('status' => 'Trạng thái', 'target' => 'Mục tiêu áp dụng', 'date_active' => 'Thời gian áp dụng') + array_slice($defaults, 2, count($defaults) - 1, true);
    return $defaults;
}

add_action('manage_campaign_posts_custom_column', 'campaign_custom_column_content', 10, 2);

function campaign_custom_column_content($column, $post_id)
{
    if (get_post_type($post_id) == 'campaign') {
        $campaign_details = get_post_meta($post_id, 'campaign', true);

        switch ($column) {
            case 'status':
                $status = "Hết hạn";
                $validateDate = CampUtils::validateDateIsStillRunning($campaign_details);

                if (empty($validateDate['error']))
                    $status = empty($validateDate['success']) ? $validateDate['future'] : $validateDate['success'];
                echo $status;
                break;
            case 'target':
                $type = "Theo thành viên";

                if ($campaign_details->saleType == "order") {
                    $type = "Theo đơn hàng";
                } elseif ($campaign_details->saleType == "product") {
                    $type = "Sản phẩm độc lập";
                } elseif ($campaign_details->saleType == "multi_product") {
                    $type = "Mua N sản phẩm";
                } elseif ($campaign_details->saleType == "buy_money_product") {
                    $type = " Mua X đồng trên sản phẩm ";
                }
                echo $type;
                break;
            case 'date_active':

                echo 'Từ ' . date('d/m/Y', strtotime($campaign_details->dateAfter)) . ' đến ' . date('d/m/Y', strtotime($campaign_details->dateBefore));
                break;
        }
    }
}

function get_genreal_info_camp($post_data)
{
    $data_post = empty($_POST['promotion_campaign']) ? null : $_POST['promotion_campaign'];
    $camp = new Camp();
    $camp->name = $data_post['name'];
    $camp->description = $data_post['description'];
    $camp->dateBefore = $data_post['date']['before'];
    $camp->dateAfter = $data_post['date']['after'];
    $camp->saleType = $data_post['sale']['type'];
    $camp->multiEvent = empty($data_post['multiple_event']) ? '' : $data_post['multiple_event'];
    $camp->priority = empty($data_post['sort_order_event']) ? 0 : $data_post['sort_order_event'];
    $camp->applyPayment = empty($data_post['apply_by_payment']) ? '' : $data_post['apply_by_payment'];
    $camp->applyCard = empty($data_post['apply_by_card']) ? '' : $data_post['apply_by_card'];
    $camp->total = $data_post['total'];
    return $camp;
}

require_once(get_template_directory() . '/includes/campaign/Camp.php');
require_once(get_template_directory() . '/includes/campaign/CampUtils.php');
require_once(get_template_directory() . '/includes/data_table_module.php');

//<!--CAMPAIGN TARGET-->
require_once(get_template_directory() . '/includes/campaign/backend/camp-target-card.php');
require_once(get_template_directory() . '/includes/campaign/backend/camp-target-order.php');
require_once(get_template_directory() . '/includes/campaign/backend/camp-target-product.php');
require_once(get_template_directory() . '/includes/campaign/backend/camp-target-multi-product.php');
require_once(get_template_directory() . '/includes/campaign/backend/camp-target-money-product.php');
require_once(get_template_directory() . '/includes/campaign/backend/camp-target-bonus.php');
require_once(get_template_directory() . '/includes/campaign/backend/camp-target-special-price.php');
require_once(get_template_directory() . '/includes/campaign/backend/camp-service.php');
?>