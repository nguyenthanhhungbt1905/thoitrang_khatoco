<?php

class DataCampTableSpecialPrice extends DataCampTable {

    private $list_product_camp = array();
    private $data_camp_db = null;
    private $config = array(
        'btn' => 'campaign_special_price_btn',
        'btn_text' => 'mục tiêu',
        'type_offer' => 'special-price',
        'camp_tb' => 'campaign_special_price_tb'
    ); // Shouldn't change value , it effect to jQuery script variable
    private $headers = array('DANH MỤC', 'GIÁ ĐẶC BIỆT');
    private $data = array();

    public function __construct() {
        global $post;
        $this->data_camp_db = get_post_meta($post->ID, 'campaign', true);
        $this->list_product_camp = get_all_product_min();
        $this->data = empty($this->data_camp_db->saleData) ? null : $this->data_camp_db->saleData;
        parent::__constructor($this->config, $this->headers, $this->data);
    }

    protected function create_popup_camp($is_edit_popup = false) {
        $id = 'special_price_field';
        if ($is_edit_popup) {
            $text = 'edit';
        } else {
            $text = 'add';
        }

        ob_start();
        ?>
        <div class='form-group clearfix'>
            <label class='col-md-3'>Sản phẩm</label>
            <span class='col-md-9'>
                <select autocomplete="off" class='form-control select-two' multiple="multiple" style="width: 100%" name='<?php echo $id . '_product_' . $text; ?>' id='<?php echo $id . '_product_' . $text; ?>'>
                    <?php foreach ($this->list_product_camp as $product) : ?>
                        <option value='<?php echo $product->ID; ?>'><?php echo $product->post_title; ?></option>
                    <?php endforeach; ?>
                </select>
            </span>
        </div>

        <div class='form-group clearfix'>
            <label class='col-md-3'>Giá bán đặc biệt</label>
            <span class='col-md-8'><input type='number' min="0" name='<?php echo $id . '_price_' . $text ?>' id='<?php echo $id . '_price_' . $text ?>'></span>
        </div>
        <?php
        return ob_get_clean();
    }

    protected function convertDataList() { // Convert data of CampSaleData -> JSon can map into table
        
        if (!empty($this->data_camp_db->specialPrice)) {
            $result = array();
            foreach ($this->data_camp_db->specialPrice as $d) :
                $temp = new stdClass();
                $temp->listProds = array();
                foreach ($d->listProds as $item){
                    $itemProd = new stdClass();
                    $itemProd->key = $item;
                    $itemProd->value = get_post($itemProd->key)->post_title;
                    $temp->listProds[] = $itemProd;
                }        
                $temp->price = $d->price;
                $result[] = $temp;
            endforeach;
            return $result;
        } else
            return null;
    }

    public function generate_logic_script_table() {
        ?>
        <!--LOGIC -->
        <script>
            var table_campaign_special_price_tb = null;
            jQuery(document).ready(function () {
                data_campaign_special_price_tb = <?php echo json_encode($this->convertDataList()); ?>;
                table_campaign_special_price_tb = jQuery('#<?php echo $this->config['camp_tb']; ?>').DataTable({
                    bFilter: false,
                    bInfo: false,
                    paging: false,
                    scrollY: '50vh',
                    select: 'single',
                    scrollCollapse: true,
                    data: data_campaign_special_price_tb,
                    columns: [
                        {title: "Sản phẩm", defaultContent: "<i>Not set</i>", data: 'listProds', render: function (data, type, full, meta) {
                                if (type === 'display') {
                                    var result = '';
                                    for (var i = 0; i < data.length; i++) {
                                        result += data[i].value + '<input type="hidden" name="promotion_campaign[data_sale_price][' + meta.row + '][data_list][]" value="' + data[i].key + '"><br>';
                                    }
                                    return result;
                                }
                            }
                        },
                        {title: "Giá bán đặc biệt", defaultContent: "<i>Not set</i>", data: 'price', render: function (data, type, full, meta) {
                                return render_hidden(data, data, 'promotion_campaign[data_sale_price][' + meta.row + '][price]');
                            }
                        }
                    ]
                });
            });
            jQuery('#subm_add_campaign_special_price_btn').on('click', function (e) {
                subm_camp_special_price();
            });

            function subm_camp_special_price() {
                var _popup = jQuery('#campaign_special_price_tb_add_popup');
                var _list_prods_select = jQuery('#special_price_field_product_add');
                var _price = jQuery('#special_price_field_price_add');

                if (_list_prods_select.val() === undefined || _list_prods_select.val() === null || _list_prods_select.val().length == 0)
                {
                    bs_error.set_error('* Bạn phải chọn dữ liệu nhập vào danh sách sản phẩm khuyến mãi');
                    return false;
                }
                if (!jQuery.isNumeric(_price.val()))
                {
                    bs_error.set_error('* Giá bán đặc biệt phải là số');
                    return false;
                }

                if (parseInt(_price.val()) < 1 )
                {
                    bs_error.set_error('* Giá đặc biệt phải > 0');
                    return false;
                }

                var array_commit = null;
                    var list_prods = parse_element_to_obj(_list_prods_select, 'select');
                    var list_prods_add = []
                    if (!jQuery.isArray(list_prods))
                        list_prods_add.push(list_prods);
                    else
                        list_prods_add = list_prods;
                    array_commit = {
                        listProds: list_prods_add,
                        price: _price.val()
                    };
                table_campaign_special_price_tb.row.add(array_commit).draw();
                refreshDataTable(table_campaign_special_price_tb);
                _popup.modal('toggle');
                return true;
            }
        </script>
        <?php
    }

}