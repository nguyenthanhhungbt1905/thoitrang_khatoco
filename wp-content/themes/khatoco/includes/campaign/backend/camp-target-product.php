<?php

class DataCampTableProduct extends DataCampTable {

    private $list_product_camp = array();
    private $config = array(
        'btn' => 'campaign_product_btn',
        'btn_text' => 'mục tiêu',
        'sale_type' => 'product',
        'camp_tb' => 'campaign_product_tb'
    ); // Shouldn't change value , it effect to jQuery script variable
    private $headers = array('SẢN PHẨM', 'PHẦN TRĂM GIẢM');
    private $data = array();
    private $data_camp_db = null;

    public function __construct() {
        global $post;
        $this->data_camp_db = get_post_meta($post->ID, 'campaign', true);
        $this->list_product_camp = get_all_product_min();
        $this->list_cat_camp = get_all_cat_min();
        $this->data = empty($this->data_camp_db->saleData) ? null : $this->data_camp_db->saleData;

        parent::__constructor($this->config, $this->headers, $this->data);
    }

    protected function convertDataList() {
        if (!empty($this->data_camp_db->saleType) && $this->data_camp_db->saleType === 'product') {
            $result = array();
            foreach ($this->data as $d) :
                $temp = new stdClass();
//            convert form sale
                $temp->formSale = new stdClass();
                $temp->formSale->key = $d->formSale;
                if ($d->formSale === 'prod')
                    $temp->formSale->value = 'Sản phẩm';
                else
                    $temp->formSale->value = 'Chuyên mục';

//            Convert data list
                
                foreach ($d->data as $list):
                    $dataListItem = new stdClass();
                    $dataListItem->key = $list;
                    $dataListItem->value = ($d->formSale === 'prod') ? get_post($dataListItem->key)->post_title : get_category($dataListItem->key)->name;
                    $temp->data[] = $dataListItem;
                endforeach;

                $temp->percent = $d->percent;
                $result[] = $temp;
            endforeach;
            return $result;
        } else
            return null;
    }

    protected function create_popup_camp($is_edit_popup = false) {
        $id = 'product_field';
        if ($is_edit_popup) {
            $text = 'edit';
        } else {
            $text = 'add';
        }

        ob_start();
        ?>

        <div class='form-group clearfix'>
            <label class='col-md-3'>Chọn loại</label>
            <span class='col-md-8'>
                <label><input type='radio' class="form-control" data-label='Chuyên mục' name='<?php echo $id . '_form_sale_' . $text ?>' value='cat' checked> 
                    Chuyên mục sản phẩm
                </label>
                <label><input type='radio' class="form-control" data-label='Sản phẩm' name='<?php echo $id . '_form_sale_' . $text ?>' value='prod'>
                    Danh sách sản phẩm
                </label>
            </span>
        </div>

        <div class='form-group clearfix' style="display:none" id='camp-prod-product-type-dropdown'>
            <label class='col-md-3'>Sản phẩm</label>
            <span class='col-md-9'>
                <select autocomplete="off" class='form-control select-two' multiple="multiple" style="width: 100%" name='<?php echo $id . '_product_' . $text; ?>' id='<?php echo $id . '_product_' . $text; ?>'>
                    <?php foreach ($this->list_product_camp as $product) : ?>
                        <option value='<?php echo $product->ID; ?>'><?php echo $product->post_title; ?></option>
                    <?php endforeach; ?>
                </select>
            </span>
        </div>

        <div class='form-group clearfix' id='camp-prod-cat-type-dropdown'>
            <label class='col-md-3'>Chuyên mục</label>
            <span class='col-md-9'>
                <select autocomplete="off" multiple="multiple" class='form-control select-two' style="width: 100%" name='<?php echo $id . '_cat_' . $text; ?>' id='<?php echo $id . '_cat_' . $text; ?>'>
                    <?php foreach ($this->list_cat_camp as $camp) : ?>
                        <option value='<?php echo $camp->term_id; ?>'><?php echo $camp->name; ?></option>
                    <?php endforeach; ?>
                </select>
            </span>
        </div>

        <div class='form-group clearfix'>
            <label class='col-md-3'>Số (%) sẽ giảm</label>
            <span class='col-md-8'><input type='number' min="1" max="100" name='<?php echo $id . '_percent_decrease_' . $text ?>' id='<?php echo $id . '_percent_decrease_' . $text ?>'></span>
        </div>
        <?php
        return ob_get_clean();
    }

    public function generate_logic_script_table() {
        ?>
        <!--LOGIC -->
        <script>

            var table_campaign_product_tb = null;
            jQuery(document).ready(function () {
                data_campaign_product_tb = <?php echo json_encode($this->convertDataList()); ?>;
                table_campaign_product_tb = jQuery('#<?php echo $this->config['camp_tb']; ?>').DataTable({
                    bFilter: false,
                    bInfo: false,
                    paging: false,
                    scrollY: '50vh',
                    select: 'single',
                    scrollCollapse: true,
                    data: data_campaign_product_tb,
                    columns: [
                        {title: "Loại", defaultContent: "<i>Not set</i>", data: "formSale", render: function (data, type, full, meta) {
                                return render_hidden(data.key, data.value, 'promotion_campaign[data_sale_prod][' + meta.row + '][formSale]');
                            }
                        },
                        {title: "List sản phẩm", defaultContent: "<i>Not set</i>", data: "data", render: function (data, type, full, meta) {
                                if (type === 'display') {
                                    var result = '';
                                    for (var i = 0; i < data.length; i++) {
                                        result += data[i].value + '<input type="hidden" name="promotion_campaign[data_sale_prod][' + meta.row + '][data][]" value="' + data[i].key + '"><br>';
                                    }
                                    return result;
                                }
                            }
                        },
                        {title: "Phần trăm giảm (%)", defaultContent: "<i>Not set</i>", data: 'percent', render: function (data, type, full, meta) {
                                return render_hidden(data, data, 'promotion_campaign[data_sale_prod][' + meta.row + '][percent]');
                            }
                        }
                    ]
                });
            });

            jQuery('input[name="product_field_form_sale_add"]').on('change', function () {
                if (jQuery(this).val() === 'cat') {
                    jQuery('#camp-prod-product-type-dropdown').hide();
                    jQuery('#camp-prod-cat-type-dropdown').show();
                } else {
                    jQuery('#camp-prod-product-type-dropdown').show();
                    jQuery('#camp-prod-cat-type-dropdown').hide();
                }
            });

            jQuery('#campaign_product_btn_rm').on('click', function (e) {
                refreshDataTable(table_campaign_product_tb);
            });

            jQuery('#subm_add_campaign_product_btn').on('click', function (e) {
                subm_camp_product();
            });

            function subm_camp_product() {
                var _popup = jQuery('#campaign_product_tb_add_popup');
                var _product_type_select = jQuery('#product_field_product_add');
                var _cat_type_select = jQuery('#product_field_cat_add');
                var _form = jQuery('input[name="product_field_form_sale_add"]:checked');
                var _percent = jQuery('#product_field_percent_decrease_add');

                if (
                        (_form.val() === 'cat' && _cat_type_select.val() === null) ||
                        (_form.val() === 'prod' && _product_type_select.val() === null)
                        )
                {
                    bs_error.set_error('* Bạn phải chọn dữ liệu nhập vào danh sách khuyến mãi');
                    return false;
                }
                if (!jQuery.isNumeric(_percent.val()))
                {
                    bs_error.set_error('* Phần trăm phải là số');
                    return false;
                }

                if (parseInt(_percent.val()) < 1 || parseInt(_percent.val()) > 100 )
                {
                    bs_error.set_error('* Phần trăm giảm phải nằm trong khoảng 1 - 100%');
                    return false;
                }

                var array_commit = null;
                var list_prods = null
                if (_form.val() === 'cat')
                    list_prods = parse_element_to_obj(_cat_type_select, 'select');
                else
                    list_prods = parse_element_to_obj(_product_type_select, 'select');

                var list_prods_add = []

                if (!jQuery.isArray(list_prods))
                    list_prods_add.push(list_prods);
                else
                    list_prods_add = list_prods;

                array_commit = {
                    data: list_prods_add,
                    percent: _percent.val(),
                    formSale: parse_element_to_obj(_form, 'radio')
                }

                table_campaign_product_tb.row.add(array_commit).draw();
                refreshDataTable(table_campaign_product_tb);
                _popup.modal('toggle');
                return true;
            }
        </script>
        <?php
    }

}

add_action('save_post', 'save_camp_target_product', 14, 1);

function save_camp_target_product($post_id) {
    if (get_post_type($post_id) == 'campaign' && !empty($_POST['promotion_campaign'])) {
        $data_post = $_POST['promotion_campaign'];
        $camp = get_genreal_info_camp($data_post);

        if (!empty($camp->saleType) && $camp->saleType === 'product') {
            if (!empty($data_post['data_sale_prod'])) {
// Camp is multi_product && have data was seted before
                foreach ($data_post['data_sale_prod'] as $row) {
                    $campSaleData = new CampSaleData();
                    $campSaleData->data = $row['data'];
                    $campSaleData->percent = $row['percent'];
                    $campSaleData->formSale = $row['formSale'];
                    $camp->saleData[] = $campSaleData;
                }

                update_post_meta($post_id, 'campaign', $camp);
            }
        }
    }
}
