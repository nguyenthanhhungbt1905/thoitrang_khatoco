<?php

class DataCampTableCard extends DataCampTable {

    private $list_card_camp = array();
    private $data_camp_db = null;
    private $config = array(
        'btn' => 'campaign_card_btn',
        'btn_text' => 'mục tiêu',
        'sale_type' => 'card',
        'camp_tb' => 'campaign_card_tb'
    ); // Shouldn't change value , it effect to jQuery script variable
    private $headers = array('LOẠI THẺ', 'TỔNG GIÁ TRỊ ĐƠN HÀNG >=', 'PHẦN TRĂM GIẢM');
    private $data = array();

    public function __construct() {
        global $post;
        $this->data_camp_db = get_post_meta($post->ID, 'campaign', true);
        $this->list_card_camp = get_all_card();
        $this->data = empty($this->data_camp_db->saleData) ? null : $this->data_camp_db->saleData;
        parent::__constructor($this->config, $this->headers, $this->data);
    }

    protected function create_popup_camp($is_edit_popup = false) {
        $id = 'card_field';
        if ($is_edit_popup) {
            $text = 'edit';
        } else {
            $text = 'add';
        }

        ob_start();
        ?>
        <div class='form-group clearfix'>
            <label class='col-md-3'>Loại thẻ</label>
            <span class='col-md-9'>
                <select autocomplete="off" class='form-control' name='<?php echo $id . '_type_' . $text; ?>' id='<?php echo $id . '_type_' . $text; ?>'>
                    <?php foreach ($this->list_card_camp as $cardKey => $cardVal) : ?>
                        <option value="<?php echo $cardKey; ?>"><?php echo $cardVal; ?></option>
                    <?php endforeach; ?>
                </select>
            </span>
        </div>

        <div class='form-group clearfix'>
            <label class='col-md-3'>Tổng giá trị đơn hàng >=</label>
            <span class='col-md-9'>
                <input type='number' id="<?php echo $id . '_min_buy_' . $text; ?>" min="10000">
            </span>
        </div>

        <div class='form-group clearfix'>
            <label class='col-md-3'>(%) Phần trăm giảm giá</label>
            <span class='col-md-8'><input type='number' min="1" max="100" name='<?php echo $id . '_percent_' . $text ?>' id='<?php echo $id . '_percent_' . $text ?>'></span>
        </div>
        <?php
        return ob_get_clean();
    }

    protected function convertDataList() { // Convert data of CampSaleData -> JSon can map into table
        if (!empty($this->data_camp_db->saleType) && $this->data_camp_db->saleType === 'card') {
            $result = array();
            foreach ($this->data as $d) :
                $temp = new stdClass();
                $temp->card = new stdClass();
                $temp->card->key = $d->card;
                $temp->card->value = get_card($temp->card->key);

                $temp->minBuy = $d->minBuy;
                $temp->percent = $d->percent;

                $result[] = $temp;
            endforeach;
            return $result;
        } else
            return null;
    }

    public function generate_logic_script_table() {
        ?>
        <!--LOGIC -->
        <script>
            var table_campaign_card_tb = null;
            jQuery(document).ready(function () {
                data_campaign_card_tb = <?php echo json_encode($this->convertDataList()); ?>;
                table_campaign_card_tb = jQuery('#campaign_card_tb').DataTable({
                    bFilter: false,
                    bInfo: false,
                    paging: false,
                    scrollY: '50vh',
                    select: 'single',
                    scrollCollapse: true,
                    data: data_campaign_card_tb,
                    columns: [
                        {title: "Loại thẻ", defaultContent: "<i>Not set</i>", data: "card", render: function (data, type, full, meta) {
                                return render_hidden(data.key, data.value, 'promotion_campaign[data_sale_card][' + meta.row + '][card]');
                            }
                        },
                        {title: "Tổng giá trị đơn hàng >=", defaultContent: "<i>Not set</i>", data: 'minBuy', render: function (data, type, full, meta) {
                                if (type === 'display') {
                                    return render_hidden(data, data, 'promotion_campaign[data_sale_card][' + meta.row + '][min_buy]');
                                }
                            }
                        },
                        {title: "Phần trăm giảm (%)", defaultContent: "<i>Not set</i>", data: 'percent', render: function (data, type, full, meta) {
                                return render_hidden(data, data, 'promotion_campaign[data_sale_card][' + meta.row + '][percent]');
                            }
                        }
                    ]
                });
            });


            jQuery('#subm_add_campaign_card_btn').on('click', function (e) {
                subm_camp_card();
            });

            jQuery('#campaign_card_btn_rm').on('click', function (e) {
                refreshDataTable(table_campaign_card_tb);
            });

            function subm_camp_card() {
                var _popup = jQuery('#campaign_card_tb_add_popup');
                var _card = jQuery('#card_field_type_add');
                var _percent = jQuery('#card_field_percent_add');
                var _min_buy = jQuery('#card_field_min_buy_add');

                if (!jQuery.isNumeric(_min_buy.val()) || !jQuery.isNumeric(_percent.val()))
                {
                    bs_error.set_error('* Giá trị đơn hàng & phần trăm giảm phải là số');
                    return false;
                }

                if (parseInt(_min_buy.val()) < 10000 || parseInt(_percent.val()) < 1 || parseInt(_percent.val()) > 100 )
                {
                    bs_error.set_error('* Giá trị đơn hàng >= 10.000 & Phần trăm giảm phải nằm trong khoảng 1 - 100%');
                    return false;
                }

                var array_commit = null;
                var card = parse_element_to_obj(_card, 'select');
                array_commit = {
                    card: card,
                    percent: _percent.val(),
                    minBuy: _min_buy.val()
                };

                table_campaign_card_tb.row.add(array_commit).draw();
                refreshDataTable(table_campaign_card_tb);
                _popup.modal('toggle');
                return true;
            }
        </script>
        <?php
    }

}

add_action('save_post', 'save_camp_target_card', 14, 1);
function save_camp_target_card($post_id) {
    if (get_post_type($post_id) == 'campaign' && !empty($_POST['promotion_campaign'])) {
        $data_post = $_POST['promotion_campaign'];
        $camp = get_genreal_info_camp($data_post);
        if (!empty($camp->saleType) && $camp->saleType === 'card') {
            if (!empty($data_post['data_sale_card'])) {
                // Camp is card && have data was seted before
                foreach ($data_post['data_sale_card'] as $row) {
                    $campSaleData = new CampSaleData();
                    $campSaleData->card = $row['card'];
                    $campSaleData->percent = $row['percent'];
                    $campSaleData->minBuy = $row['min_buy'];
                    $camp->saleData[] = $campSaleData;
                }
                update_post_meta($post_id, 'campaign', $camp);
            }
        }
    }
}
