<?php

class DataCampTableMoneyProduct extends DataCampTable {

    private $list_product_camp = array();
    private $list_cat_product_camp = array();
    private $data_camp_db = null;
    private $config = array(
        'btn' => 'campaign_money_product_btn',
        'btn_text' => 'mục tiêu',
        'sale_type' => 'buy_money_product',
        'camp_tb' => 'campaign_money_product_tb'
    ); // Shouldn't change value , it effect to jQuery script variable
    private $headers = array('HÌNH THỨC ÁP DỤNG', 'DANH MỤC', 'SỐ TIỀN TỐI THIỂU MUA');
    private $data = array();

    public function __construct() {
        global $post;
        $this->data_camp_db = get_post_meta($post->ID, 'campaign', true);
        $this->list_product_camp = get_all_product_min();
        $this->list_cat_product_camp = get_all_cat_min();
        $this->data = empty($this->data_camp_db->saleData) ? null : $this->data_camp_db->saleData;
        parent::__constructor($this->config, $this->headers, $this->data);
    }

    protected function create_popup_camp($is_edit_popup = false) {
        $id = 'money_product_field';
        if ($is_edit_popup) {
            $text = 'edit';
        } else {
            $text = 'add';
        }

        ob_start();
        ?>
        <div class='form-group clearfix'>
            <label class='col-md-3'>Hình thức</label>
            <span class='col-md-9'>
                <select autocomplete="off" class='form-control' name='<?php echo $id . '_type_' . $text; ?>' id='<?php echo $id . '_type_' . $text; ?>'>
                    <option value="list_product">List sản phẩm</option>
                    <option value="cat_product">Chuyên mục sản phẩm</option>
                </select>
            </span>
        </div>

        <div class='form-group clearfix' id='money-product-product-type-dropdown'>
            <label class='col-md-3'>Sản phẩm</label>
            <span class='col-md-9'>
                <select autocomplete="off" class='form-control select-two' multiple="multiple" style="width: 100%" name='<?php echo $id . '_product_' . $text; ?>' id='<?php echo $id . '_product_' . $text; ?>'>
                    <?php foreach ($this->list_product_camp as $product) : ?>
                        <option value='<?php echo $product->ID; ?>'><?php echo $product->post_title; ?></option>
                    <?php endforeach; ?>
                </select>
            </span>
        </div>

        <div class='form-group clearfix' id='money-product-cat-type-dropdown'>
            <label class='col-md-3'>Chuyên mục</label>
            <span class='col-md-9'>
                <select autocomplete="on" class='form-control select-two' multiple="multiple" style="width: 100%" name='<?php echo $id . '_cat_prod_' . $text; ?>' id='<?php echo $id . '_cat_prod_' . $text; ?>'>
                    <?php foreach ($this->list_cat_product_camp as $cat) : ?>
                        <option value='<?php echo $cat->term_id; ?>'><?php echo $cat->name; ?></option>
                    <?php endforeach; ?>
                </select>
            </span>
        </div>

        <div class='form-group clearfix'>
            <label class='col-md-3'>Số tiền mua tối thiểu</label>
            <span class='col-md-8'><input type='number' min="1"  name='<?php echo $id . '_min_buy_' . $text ?>' id='<?php echo $id . '_min_buy_' . $text ?>'></span>
        </div>
        <?php
        return ob_get_clean();
    }

    protected function convertDataList() { // Convert data of CampSaleData -> JSon can map into table
        if (!empty($this->data_camp_db->saleType) && $this->data_camp_db->saleType === 'buy_money_product') {
            $result = array();
            foreach ($this->data as $d) :
                $temp = new stdClass();
//            convert form sale
                $temp->formSale = new stdClass();
                $temp->formSale->key = $d->formSale;
                if ($d->formSale === 'list_product')
                    $temp->formSale->value = 'List sản phẩm';
                else
                    $temp->formSale->value = 'Chuyên mục sản phẩm';

//            Convert data list
                foreach ($d->dataList as $list):
                    $dataListItem = new stdClass();
                    $dataListItem->key = $list;
                    $dataListItem->value = ($d->formSale === 'list_product') ? get_post($dataListItem->key)->post_title : get_category($dataListItem->key)->name;
                    $temp->dataList[] = $dataListItem;
                endforeach;

//            Convert min buy
                $temp->minBuy = $d->minBuy;

                $result[] = $temp;
            endforeach;
            return $result;
        } else
            return null;
    }

    public function generate_logic_script_table() {
        ?>
        <!--LOGIC -->
        <script>
            var table_campaign_money_product_tb = null;
            jQuery(document).ready(function () {
                data_campaign_money_product_tb = <?php echo json_encode($this->convertDataList()); ?>;
                table_campaign_money_product_tb = jQuery('#<?php echo $this->config['camp_tb']; ?>').DataTable({
                    bFilter: false,
                    bInfo: false,
                    paging: false,
                    scrollY: '50vh',
                    select: 'single',
                    scrollCollapse: true,
                    data: data_campaign_money_product_tb,
                    columns: [
                        {title: "Hình thức", defaultContent: "<i>Not set</i>", data: "formSale", render: function (data, type, full, meta) {
                                return render_hidden(data.key, data.value, 'promotion_campaign[data_sale_money_prod][' + meta.row + '][form_sale]');
                            }
                        },
                        {title: "Danh mục", defaultContent: "<i>Not set</i>", data: 'dataList', render: function (data, type, full, meta) {
                                if (type === 'display') {
                                    var result = '';
                                    for (var i = 0; i < data.length; i++) {
                                        result += data[i].value + '<input type="hidden" name="promotion_campaign[data_sale_money_prod][' + meta.row + '][data_list][]" value="' + data[i].key + '"><br>';
                                    }
                                    return result;
                                }
                            }
                        },
                        {title: "Số tiền mua tối thiểu", defaultContent: "<i>Not set</i>", data: 'minBuy', render: function (data, type, full, meta) {
                                return render_hidden(data, data, 'promotion_campaign[data_sale_money_prod][' + meta.row + '][min_buy]');
                            }
                        }
                    ]
                });
            });
            function switch_type_money_product(type) {
                if (type === 'list_product') {
                    jQuery('#money-product-cat-type-dropdown').hide();
                    jQuery('#money-product-product-type-dropdown').show();
                } else {
                    jQuery('#money-product-cat-type-dropdown').show();
                    jQuery('#money-product-product-type-dropdown').hide();
                }
            }
            jQuery('#money_product_field_type_add').on('change', function () {
                switch_type_money_product(jQuery(this).children(':selected').val());
            });
            jQuery('#subm_add_campaign_money_product_btn').on('click', function (e) {
                subm_camp_money_product();
            });

            jQuery('#campaign_money_product_btn_add').on('click', function () {
                switch_type_money_product('list_product');
            });
            jQuery('#campaign_money_product_btn_rm').on('click', function (e) {
                refreshDataTable(table_campaign_money_product_tb);
            });

            function subm_camp_money_product() {
                var _popup = jQuery('#campaign_money_product_tb_add_popup');
                var _drop_down_type_select = jQuery('#money_product_field_type_add');
                var _cat_type_select = jQuery('#money_product_field_cat_prod_add');
                var _product_type_select = jQuery('#money_product_field_product_add');
                var _min_buy = jQuery('#money_product_field_min_buy_add');

                if (
                        _drop_down_type_select.children(':selected').val() === 'list_product' &&
                        (_product_type_select.val() === undefined || _product_type_select.val() === null || _product_type_select.val().length == 0)
                        )
                {
                    bs_error.set_error('* Bạn phải chọn dữ liệu nhập vào danh sách sản phẩm khuyến mãi');
                    return false;
                }
                if (
                        _drop_down_type_select.children(':selected').val() === 'cat_product' &&
                        (_cat_type_select.val() === undefined || _cat_type_select.val() === null || _cat_type_select.val().length == 0)
                        )
                {
                    bs_error.set_error('* Bạn phải chọn dữ liệu nhập vào chuyên mục sản phẩm sẽ được khuyến mãi');
                    return false;
                }
                if (!jQuery.isNumeric(_min_buy.val()))
                {
                    bs_error.set_error('* Số tiền mua tối thiểu phải là số nguyên');
                    return false;
                }

                if (parseInt(_min_buy.val()) < 1 )
                {
                    bs_error.set_error('* Số tiền không được nhỏ hơn 1');
                    return false;
                }

                var array_commit = null;
                if (_drop_down_type_select.children(':selected').val() === 'list_product') {
                    var list_prods = parse_element_to_obj(_product_type_select, 'select');
                    var list_prods_add = []
                    if (!jQuery.isArray(list_prods))
                        list_prods_add.push(list_prods);
                    else
                        list_prods_add = list_prods;

                    var formSale = parse_element_to_obj(_drop_down_type_select, 'select');
                    array_commit = {
                        formSale: formSale,
                        dataList: list_prods_add,
                        minBuy: _min_buy.val()
                    };
                } else {
                    var list_cats = parse_element_to_obj(_cat_type_select, 'select');
                    var list_cats_add = []
                    if (!jQuery.isArray(list_cats))
                        list_cats_add.push(list_cats);
                    else
                        list_cats_add = list_cats;

                    var formSale = parse_element_to_obj(_drop_down_type_select, 'select');
                    array_commit = {
                        formSale: formSale,
                        dataList: list_cats_add,
                        minBuy: _min_buy.val()
                    };
                }

                table_campaign_money_product_tb.row.add(array_commit).draw();
                refreshDataTable(table_campaign_money_product_tb);
                _popup.modal('toggle');
                return true;
            }
        </script>
        <?php
    }

}

add_action('save_post', 'save_camp_target_money_product', 14, 1);

function save_camp_target_money_product($post_id) {
    if (get_post_type($post_id) == 'campaign' && !empty($_POST['promotion_campaign'])) {
        $data_post = $_POST['promotion_campaign'];
        $camp = get_genreal_info_camp($data_post);

        if (!empty($camp->saleType) && $camp->saleType === 'buy_money_product') {
            if (!empty($data_post['data_sale_money_prod']) && !empty($data_post['form'])) {
                // Camp is money_product && have data was seted before
                $camp->form = $data_post['form'];
                foreach ($data_post['data_sale_money_prod'] as $row) {
                    $campSaleData = new CampSaleData();
                    $campSaleData->formSale = $row['form_sale'];
                    $campSaleData->dataList = $row['data_list'];
                    $campSaleData->minBuy = $row['min_buy'];
                    $camp->saleData[] = $campSaleData;
                }
                foreach ($data_post['data_sale_bonus'] as $row) {
                    $camp->bonus[] = $row;
                }
                foreach ($data_post['data_sale_price'] as $row) {
                    $salePrice = new stdClass();
                    $salePrice->listProds = $row['data_list'];
                    $salePrice->price = $row['price'];
//                    $salePrice->total = $row['total'];
                    $camp->specialPrice[] = $salePrice;
                }
                update_post_meta($post_id, 'campaign', $camp);
            }
        }
    }
}
