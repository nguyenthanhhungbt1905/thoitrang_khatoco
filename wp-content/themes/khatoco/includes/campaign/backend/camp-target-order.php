<?php

class DataCampTableOrder extends DataCampTable {

    private $config = array(
        'btn' => 'campaign_order_btn',
        'btn_text' => 'mục tiêu',
        'sale_type' => 'order',
        'camp_tb' => 'campaign_order_tb'
    ); // Shouldn't change value , it effect to jQuery script variable
    private $headers = array('TỔNG GIÁ TRỊ MUA >= ', 'PHẦN TRĂM GIẢM');
    private $data = array();
    private $data_camp_db = null;

    public function __construct() {
        global $post;
        $this->data_camp_db = get_post_meta($post->ID, 'campaign', true);
        $this->data = empty($this->data_camp_db->saleData) ? null : $this->data_camp_db->saleData;

        parent::__constructor($this->config, $this->headers, $this->data);
    }

    protected function convertDataList() {
        if (!empty($this->data_camp_db->saleType) && $this->data_camp_db->saleType === 'order') {
            $result = array();
            foreach ($this->data as $d) :
                $temp = new stdClass();
                $temp->minBuy = $d->minBuy;
                $temp->percent = $d->percent;
                $result[] = $temp;
            endforeach;
            return $result;
        } else
            return null;
    }

    protected function create_popup_camp($is_edit_popup = false) {
        $id = 'order_field';
        if ($is_edit_popup) {
            $text = 'edit';
        } else {
            $text = 'add';
        }

        ob_start();
        ?>

        <div class='form-group clearfix' id='order-min'>
            <label class='col-md-3'>Tổng giá trị đơn hàng >= </label>
            <span class='col-md-9'>
                <input type='number' min="10000" name='<?php echo $id . '_min_buy_' . $text ?>' id='<?php echo $id . '_min_buy_' . $text ?>'>
            </span>
        </div>

        <div class='form-group clearfix'>
            <label class='col-md-3'>Số (%) sẽ giảm</label>
            <span class='col-md-8'><input type='number' min="1" max="100" name='<?php echo $id . '_percent_' . $text ?>' id='<?php echo $id . '_percent_' . $text ?>'></span>
        </div>
        <?php
        return ob_get_clean();
    }

    public function generate_logic_script_table() {
        ?>
        <!--LOGIC -->
        <script>

            var table_campaign_order_tb = null;
            jQuery(document).ready(function () {
                data_campaign_order_tb = <?php echo json_encode($this->convertDataList()); ?>;
                table_campaign_order_tb = jQuery('#<?php echo $this->config['camp_tb']; ?>').DataTable({
                    bFilter: false,
                    bInfo: false,
                    paging: false,
                    scrollY: '50vh',
                    select: 'single',
                    scrollCollapse: true,
                    data: data_campaign_order_tb,
                    columns: [
                        {title: "Tổng giá trị sản phẩm >= ", defaultContent: "<i>Not set</i>", data: "minBuy",render: function (data, type, full, meta) {
                                return render_hidden(data, data, 'promotion_campaign[data_sale_order][' + meta.row + '][min_buy]');
                            }
                        },
                        {title: "Phần trăm giảm (%)", defaultContent: "<i>Not set</i>", data: 'percent', render: function (data, type, full, meta) {
                                return render_hidden(data, data, 'promotion_campaign[data_sale_order][' + meta.row + '][percent]');
                            }
                        }
                    ]
                });
            });

            jQuery('#campaign_order_btn_rm').on('click', function (e) {
                refreshDataTable(table_campaign_order_tb);
            });

            jQuery('#subm_add_campaign_order_btn').on('click', function (e) {
                subm_camp_order();
            });

            function subm_camp_order() {
                var _popup = jQuery('#campaign_order_tb_add_popup');
                var _min_buy = jQuery('#order_field_min_buy_add');
                var _percent = jQuery('#order_field_percent_add');

                if (!jQuery.isNumeric(_percent.val()) || !jQuery.isNumeric(_min_buy.val()))
                {
                    bs_error.set_error('* Tất cả phải nhập số');
                    return false;
                }
                
                if (parseInt(_min_buy.val()) < 10000 || parseInt(_percent.val()) < 1 || parseInt(_percent.val()) > 100 )
                {
                    bs_error.set_error('* Giá trị đơn hàng >= 10.000 & Phần trăm giảm phải nằm trong khoảng 1 - 100%');
                    return false;
                }

                var array_commit = null;
                array_commit = {
                    minBuy  : _min_buy.val(),
                    percent : _percent.val()
                }

                table_campaign_order_tb.row.add(array_commit).draw();
                refreshDataTable(table_campaign_order_tb);
                _popup.modal('toggle');
                return true;
            }
        </script>
        <?php
    }

}

add_action('save_post', 'save_camp_target_order', 14, 1);

function save_camp_target_order($post_id) {
    if (get_post_type($post_id) == 'campaign' && !empty($_POST['promotion_campaign'])) {
        $data_post = $_POST['promotion_campaign'];
        $camp = get_genreal_info_camp($data_post);

        if (!empty($camp->saleType) && $camp->saleType === 'order') {
            if (!empty($data_post['data_sale_order'])) {
                foreach ($data_post['data_sale_order'] as $row) {
                    $campSaleData = new CampSaleData();
                    $campSaleData->minBuy = $row['min_buy'];
                    $campSaleData->percent = $row['percent'];
                    $camp->saleData[] = $campSaleData;
                }
                update_post_meta($post_id, 'campaign', $camp);
            }
        }
    }
}
