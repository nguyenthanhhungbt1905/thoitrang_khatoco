<?php
require_once(get_template_directory() . '/includes/product/ProductUtils.php');
class CampUtils
{

    public function __construct()
    {

    }

    /*logic : Tìm những campaign hợp lệ theo thời gian [ Thời gian bắt đầu campaign <= Now <= Thời gian kết thúc campaign ]
    Tách ra những campaign chạy chung và những campaign chạy độc lập (independ campaign)
    So sánh trong giỏ hàng vs các campaign chạy chung xét xem có hợp điều kiện hay không ?
    => Nếu có thì áp dụng campagin chung vào
    => Nếu không có thì so sánh tiếp những campaign chạy độc lập với giỏ hàng (chỉ lấy campaign thứ tự ưu tiên cao nhất) - để áp dụng vô*/

    public static function getCampInformation($cartRequest)
    { // Return campaign from cart information request
        $campaign = new stdClass();
        $campaign->decrease = 0;
        $campaign->priced = 0;
        $campaign->listBonus = [];
        $campaign->listSpecialPrice = [];
        $campaign->detail = [];

        $campGeneral = self::getCampRunning($cartRequest); // Tìm những campaign chung (không có campaign chạy độc lập)

        $campIndepend = self::getCampRunning($cartRequest,false); // Tìm những campaign chạy độc lập

        if (empty($campGeneral) && empty($campIndepend)) {
            return $campaign;
        } elseif (!empty($campGeneral)) {

            $appliedCamp = self::applyCampToCart($campGeneral, $cartRequest);

            if ($appliedCamp->decrease == 0 && empty($appliedCamp->listBonus) && empty($appliedCamp->listSpecialPrice)) { // Campaign khuyến mãi chung không áp dụng , chuyển sang áp dụng campaign chạy độc lập
                $appliedCamp = self::applyCampToCart($campIndepend, $cartRequest);

            }
        } elseif (empty($campGeneral) && !empty($campIndepend)){
            $appliedCamp = self::applyCampToCart($campIndepend, $cartRequest);
        }

        return $appliedCamp;
    }

    public static function validateDateIsStillRunning($camp)
    {
        $dateBefore = strtotime($camp->dateBefore);
        $dateAfter = strtotime($camp->dateAfter);
        $now = strtotime(date('Y-m-d h:i:sa'));
        if (($dateBefore - $now) < 0) {
            return array('error' => 'Khuyến mãi đã quá hạn');
        }
        if (($dateAfter - $now) > 0) {
            $availableDay = ($dateAfter - $now) / (60 * 60 * 24);
            return array('future' => 'Khuyến mãi còn ' . round($availableDay, 0) . ' ngày sẽ bắt đầu chạy', 'number_of_day' => $availableDay);
        }

        $availableDay = ($dateBefore - $now) / (60 * 60 * 24);
        return array('success' => 'Khuyến mãi còn ' . round($availableDay, 0) . ' ngày sẽ hết hạn', 'number_of_day' => $availableDay);
    }

    private static function getPriceDecreaseByCard($camp, $cart)
    {
        $data = $camp->saleData;
        $currentUserCard = get_user_meta(get_current_user_id(), 'user_card', true);
        /*System hiện tại nhiều dev code nhưng k có thiết kế, meta 'user_card' có người lưu value, người lưu key, nên phải cover hết tất cả trường họp*/
        /*Để tránh trường hợp này , các dev có tâm vui lòng thống nhất lưu 1 kiểu integer : 0 - Không thẻ, 1 - Bạc, 2 - Vàng, 3 - Kim cương (lưu số vào DB nhé)*/
        if (empty($currentUserCard)){
            $currentUserCard = 0; // Không có card
        } elseif ($currentUserCard === 'SILVER'){
            $currentUserCard = 1; // Thành viên thẻ bạc
        } elseif ($currentUserCard === 'GOLD'){
            $currentUserCard = 2; // Thành viên thẻ vàng
        } elseif ($currentUserCard === 'DIAMOND'){
            $currentUserCard = 3; // Thành viên thẻ kim cương
        }
        $currentUserCard = intval($currentUserCard);
        $orgPrice = $cart['total_sale_off'];
        $decrease = 0;
        foreach ($data as $d) :
            if ($currentUserCard == intval($d->card) && intval($orgPrice) >= intval($d->minBuy)) {
                $decrease += $orgPrice * intval($d->percent) / 100;
            }
        endforeach;

        return $decrease;
    }

    private static function getPriceDecreaseByOrder($camp, $cart)
    {
        $data = $camp->saleData;
        $orgPrice = $cart['total_sale_off'];
        $decrease = 0;
        usort($data, function ($a, $b) {
            return intval($a->minBuy) < intval($b->minBuy);
        });
        foreach ($data as $d) :
            if (intval($orgPrice) >= intval($d->minBuy)) {
                $decrease += $orgPrice * (intval($d->percent) / 100);
                break;
            }
        endforeach;
        return $decrease;
    }

    public static function getPriceDecreaseByProduct($camp, $cart)
    {
        
    	$decrease = 0;
    	if($camp && $camp->saleType=='product') {
        $data = $camp->saleData;        
        $cartProds = $cart['products'];
        foreach ($data as $d) :
            foreach ($cartProds as $prod) :
                $price = $prod['price'];
                if ( ($d->formSale === 'prod' && in_array($prod['id'], $d->data)) || // sản phẩm thuộc list sản phẩm
                     ($d->formSale === 'cat' && in_category($d->data, $prod['id'])) // Hoặc sản phẩm thuộc category
                    )
                {
                    $decrease += ($price * intval($prod['quantity'])) * (intval($d->percent) / 100);
                }

            endforeach;
        endforeach;
    	}
        return $decrease;
    }

    private static function verifyMultiProduct($camp, $cart)
    { // Kiểm tra xem điều kiện trong [Mục tiêu áp dụng : Mua N sản phẩm] có đúng với giỏ hàng hiện tại không ?
        $cartProds = $cart['products'];

        foreach ($cartProds as $prod): // Kiểm tra từng sản phẩm trong giỏ hàng có điều kiện 'list_product' match với điều kiện trong campaign không ?
            foreach ($camp->saleData as $data):
                if ($data->formSale === 'list_product' && intval($prod['quantity']) >= intval($data->minBuy) && in_array($prod['id'], $data->dataList)) {
                    return true;
                }
            endforeach;
        endforeach;

        foreach ($camp->saleData as $data): // Kiểm tra từng data của campaign điều kiện 'cat_product' match với giỏ hàng không ?
            if ($data->formSale === 'cat_product' && self::countCartProdByCategory($cart, $data->dataList) >= intval($data->minBuy)) {
                return true;
            }
        endforeach;

        return false;
    }

    private static function verifyBuyProduct($camp, $cart)
    { // Kiểm tra xem điều kiện trong [Mục tiêu áp dụng : Mua X đồng sản phẩm] có đúng với giỏ hàng hiện tại không ?
        $cartProds = $cart['products'];

        foreach ($cartProds as $prod): // Kiểm tra từng sản phẩm trong giỏ hàng có điều kiện 'list_product' match với điều kiện trong campaign không ?

            foreach ($camp->saleData as $data):

                if ($data->formSale === 'list_product' && intval($cart['total']) >= intval($data->minBuy) && in_array($prod['id'], $data->dataList)) {
                    return true;
                }
            endforeach;
        endforeach;

        foreach ($camp->saleData as $data): // Kiểm tra từng data của campaign điều kiện 'cat_product' match với giỏ hàng không ?  
            
            if ($data->formSale === 'cat_product' && self::countCartPriceProdByCategory($cart, $data->dataList) >= intval($data->minBuy)) {
                return true;
            }
        endforeach;

        return false;
    }

    private static function getBonus($camp)
    { // Lấy danh sách tặng phẩm trong campaign : return array list bonus
        $listProd = new stdClass();
        $listProd->listProdBonus = [];
        foreach ($camp->bonus as $prod):
            $product = ProductUtils::getProductInfo($prod);
            $listProd->listProdBonus[] = $product;
        endforeach;
        $listProd->total = intval($camp->total);
        return $listProd;
    }

    private static function getSpecialPrice($camp)
    { // Lấy danh sách sản phẩm bán với giá ưu đãi : return array
        $listProd = new stdClass();
        $listProd->listProdBonus = [];

        foreach ($camp->specialPrice as $row):
            foreach ($row->listProds as $prod) :
                $product = ProductUtils::getProductInfo($prod);
                $product->quantity_default = 0;
                $product->price = intval($row->price);
                $listProd->listProdBonus[] = $product;
            endforeach;
        endforeach;
        $listProd->total = intval($camp->total);
        return $listProd;
    }

    private static function countCartProdByCategory($cart, $catID)
    { // Đếm số sản phẩm trong giỏ hàng theo điều kiện là category
        $cartProds = $cart['products'];
        $count = 0;
        foreach ($cartProds as $prod):
            $cats = get_the_category($prod['id']);
            $catsIDOfProduct = [];
            for ($i =0; $i< sizeof($cats); $i++) :
                $catsIDOfProduct []= $cats[$i]->term_id;
            endfor;
            $compare = array_diff($catsIDOfProduct,$catID ); // Nếu $catsIDOfProduct chứa $catID => sizeOf($compare) < sizeOf($catsIDOfProduct)
            if (sizeOf($compare) < sizeOf($catsIDOfProduct)) {
                $count+=$prod['quantity'];
            }
        endforeach;

        return $count;
    }
    public static function convertCategoriesToListIDCategories($cat){ // Convert list categories to list id categories
        return $cat->term_id;
    }
    private static function countCartPriceProdByCategory($cart, $catsID)
    { // Đếm tổng số tiền của sản phẩm trong giỏ hàng theo điều kiện là category (list categories id)
        $cartProds = $cart['products'];

        $count = 0;
        // Convert catsID to array (int)
        foreach ($catsID as $index => $catID):
            $catsID[$index] = (int) $catID;
        endforeach;

        foreach ($cartProds as $prod):
            // var_dump($prod);
            $cats = array_map('self::convertCategoriesToListIDCategories', get_the_category($prod['id']));

            // var_dump($catID->dataList);
            // foreach ($catID->dataList as $key => $value) {
            $productCategoryInCamp = false;
            foreach($cats as $id){
                if (in_array($id, $catsID)){
                    $productCategoryInCamp = true;
                    break;
                }
            }


            if ($productCategoryInCamp) {

                    $count += (int)$prod['price_saleoff']['value'];
            }
                // if (in_array($value, get_the_category($prod['id']))) {

                //         $count += $prod['sale_off'];

                // }
            // }
           
        endforeach;

        return $count;
    }

    public static function getCampRunning($cart,$isGeneral = true)
    { // Tìm những campaign đang chạy (isGeneral = true : loại trừ những campaign chạy độc lập, isGeneral = false : chỉ lấy campaign độc lập) - return array

        $listCampPost = get_posts(array('post_type' => 'campaign', 'post_status' => 'publish','posts_per_page' => -1)); // posts_per_page = -1 get unlimited post
        $listCamp = array();
        $listCampRunning = array();
        foreach ($listCampPost as $campPost):
            $listCamp[] = get_post_meta($campPost->ID, 'campaign', true);
        endforeach;

        foreach ($listCamp as $camp): // Filter by date
            if (!empty($camp) && empty(self::validateDateIsStillRunning($camp)['error']) && empty(self::validateDateIsStillRunning($camp)['future'])) {
                /*Campaign phải hợp lệ (không có error - đã quá hạn) && (không có future - campaign sẽ chạy trong tương lai)*/
                $listCampRunning[] = $camp;
            }
        endforeach;

        $result = array();
        foreach ($listCampRunning as $campRunning):
            if (empty($campRunning->multiEvent) && $isGeneral) { // Lọc ra campaign chạy chung
                $result[] = $campRunning;
            } elseif (!empty($campRunning->multiEvent) && !$isGeneral) { // Lọc ra campaign chạy độc lập
                $result[] = $campRunning;
            }
        endforeach;

        for ($i = 0; $i < sizeof($result); $i++): // Filter by payment
            $camp = $result[$i];
            $cart['payment'] = empty($cart['payment']) ? null: $cart['payment'];

            if (!empty($camp->applyPayment) && !in_array($cart['payment'], $camp->applyPayment)) {
                unset($result[$i]);
            }
        endfor;
        $result = array_values($result); // Reindex array after unset

        $cardID = get_user_meta(get_current_user_id(), 'user_card', true);
        $cardID = empty($cardID) ? 0 : intval($cardID);

        for ($i = 0; $i < sizeof($result); $i++): // Filter by member card
            $camp = $result[$i];
            if ($camp->saleType != 'card' && !empty($camp->applyCard) && !in_array($cardID, $camp->applyCard)) {
                unset($result[$i]);
            }
        endfor;
        $result = array_values($result); // Reindex array after unset

        if (!$isGeneral && sizeof($result) > 1) { // Chỉ lấy 1 phần tử của campaign chạy độc lập [ Phần tử có giá trị thứ tự ưu tiên cao nhất ]
            usort($result, function ($a, $b) {
                return $a->priority > $b->priority;
            });

            $result = array($result[0]);
        }

        return $result;
    }

    private static function applyCampToCart($listCamp, $cart)
    {
        $decrease = 0;
        $listBonus = array();
        $listSpecialPrice = array();

        $detail = []; // Thông tin mà campagin khuyến mãi được áp dụng

        foreach ($listCamp as $camp) {
            $value = 0;

            switch ($camp->saleType) {
                case 'card' :
                    $value = self::getPriceDecreaseByCard($camp, $cart);
                    break;
                case 'order' :
                    $value = self::getPriceDecreaseByOrder($camp, $cart);
                    break;
                case 'product' :
                    $value = self::getPriceDecreaseByProduct($camp, $cart);
                    break;
                case 'multi_product' :
                    if (self::verifyMultiProduct($camp, $cart) && $camp->form === 'bonus') { // Lấy danh sách sản phẩm tặng
                        $listBonus = self::getBonus($camp);
                    } elseif (self::verifyMultiProduct($camp, $cart) && $camp->form === 'special-price') { // Lấy danh sách sản phẩm bán với giá ưu đãi
                        $listSpecialPrice = self::getSpecialPrice($camp);
                    }
                    break;
                case 'buy_money_product' :

                    if (self::verifyBuyProduct($camp, $cart) && $camp->form === 'bonus') { // Lấy danh sách sản phẩm tặng
                        $listBonus = self::getBonus($camp);
                    } elseif (self::verifyBuyProduct($camp, $cart) && $camp->form === 'special-price') { // Lấy danh sách sản phẩm bán với giá ưu đãi
                        $listSpecialPrice = self::getSpecialPrice($camp);
                    }
                    break;
            }

            if ($value > 0){
                $detail []= [
                    'title' => $camp->name,
                    'type' => $camp->saleType,
                    'description' => $camp->description,
                    'dateBefore' => $camp->dateBefore,
                    'dateAfter' => $camp->dateAfter,
                    'value' => $value
                ];
            } // Thêm thông tin chi tiết chương trình khuyến mãi được áp dụng vào

            if (!empty($listBonus) || !empty($listSpecialPrice)){
                $detail []= [
                    'title' => $camp->name,
                	'type' => $camp->saleType,
                    'description' => $camp->description,
                    'dateBefore' => $camp->dateBefore,
                    'dateAfter' => $camp->dateAfter,
                    'value' => 0
                ];
            } // Thêm thông tin chi tiết chương trình khuyến mãi được áp dụng vào

            $decrease += $value;
        }
        $campInfoResponse = new stdClass();
        $campInfoResponse->decrease = $decrease;
        $campInfoResponse->listBonus = $listBonus;
        $campInfoResponse->listSpecialPrice = $listSpecialPrice;
        $campInfoResponse->detail = $detail;

        return $campInfoResponse;
    }

}

require_once(get_template_directory() . '/post-types/order.php');
