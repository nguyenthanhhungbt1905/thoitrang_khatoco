<?php
require_once(get_template_directory() . '/includes/campaign/CampUtils.php');

class ProductUtils
{

    public function __construct()
    {

    }

    public static function getAllSaleOffProd($paged = 1,$posts_per_page = 24){

        $listCampPost = get_posts(array('post_type' => 'campaign', 'post_status' => 'publish','posts_per_page' => -1)); // posts_per_page = -1 get unlimited post
        $listCamp = array();
        $listCampRunning = array();
        foreach ($listCampPost as $campPost):
            $listCamp[] = get_post_meta($campPost->ID, 'campaign', true);
        endforeach;

        foreach ($listCamp as $camp): // Filter by date
            if (empty(CampUtils::validateDateIsStillRunning($camp)['error']) && empty(CampUtils::validateDateIsStillRunning($camp)['future'])) {
                /*Campaign phải hợp lệ (không có error - đã quá hạn) && (không có future - campaign sẽ chạy trong tương lai)*/
                $listCampRunning[] = $camp;
            }
        endforeach;

        foreach($listCampRunning as $key => $c): // Filter by sản phẩm độc lập
            if ( !($c->saleType === 'product') ) {
                unset($listCampRunning[$key]);
            }
        endforeach;
        $listCampRunning = array_values($listCampRunning); // Reindex array after unset

        $resultGeneral = array();
        $resultIndepend = array();
        foreach ($listCampRunning as $campRunning):
            if (empty($campRunning->multiEvent)) { // Lọc ra campaign chạy chung
                $resultGeneral[] = $campRunning;
            } elseif (!empty($campRunning->multiEvent)) { // Lọc ra campaign chạy độc lập
                $resultIndepend[] = $campRunning;
            }
        endforeach;

        if (sizeof($resultIndepend) > 1) { // Chỉ lấy 1 phần tử của campaign chạy độc lập [ Phần tử có giá trị thứ tự ưu tiên cao nhất ]
            usort($resultIndepend, function ($a, $b) {
                return $a->priority > $b->priority;
            });

            $resultIndepend = array($resultIndepend[0]);
        }

        $finalCamp = []; // Các chương trình khuyến mãi sẽ được sử dụng
        $prodByCamp = []; // Sản phẩm khuyến mãi bởi chương trình khuyến mãi
        if(!empty($resultGeneral)){
            $finalCamp = $resultGeneral;
        } elseif (!empty($resultIndepend)) { // Trường hợp này thì $resultGeneral đã empty rồi
            $finalCamp = $resultIndepend;
        }

        foreach ($finalCamp as $c):
            foreach ($c->saleData as $data):
                if ($data->formSale === 'prod'){
                    $prodByCamp = array_merge($prodByCamp, $data->data); // add thêm vào array list id product
                } else {
                    $allProduct = get_posts( [
                        'post_type' => 'post',
                        'post_status' => 'publish',
                        'posts_per_page' => -1,
                        'category__in' => $data->data
                    ]);
                    foreach ($allProduct as $p):
                        $prodByCamp []= $p->ID;
                    endforeach;
                }
            endforeach;
        endforeach;
        /*End - Get danh sách sản phẩm sale off từ chương trình khuyến mãi*/
        $args = array('posts_per_page' => $posts_per_page, 'orderby' => 'date', 'order' => 'DESC', 'post_status' => 'publish','paged' => $paged);
        $args['meta_query'][] = array(
            'key' => 'sale_off',
            'compare' => '>',
            'value' => 0
        );
        $products = get_posts($args);

        $result = $prodByCamp;
        foreach ($products as $p):
            if (!in_array($p->ID, $prodByCamp))
                $result []= $p->ID;
        endforeach;

        return array_map('intval', $result); // List ID product saleoff - convert all string to int
    }

    public static function getProductSaleOffPrice($productID){
        /* return array[0] : decrease value , array[1] : percent decrease */

        $price = intval(get_post_meta($productID, 'post_price', true));

        $saleOffPercent = get_post_meta($productID, 'sale_off', true);

        $saleOffPercent = intval($saleOffPercent);

        $saleOff = (100-$saleOffPercent)/100 * $price;// Giá sau khi giảm



        $cart = [
            'products' => [
                ['id' => $productID, 'price' => $price, 'quantity' => 1]
            ]
        ]; // Tạo 1 cart ảo, chứa product với ID là product cần lấy giá sale_off để lấy thông tin sale_off của sản phẩm trong chương trình khuyến mãi

        $campGeneral = CampUtils::getCampRunning($cart); // Tìm những campaign chung (không có campaign chạy độc lập)

        $campIndepend = CampUtils::getCampRunning($cart,false); // Tìm những campaign chạy độc lập


        $decreaseByCamp = 0;
        if (empty($campGeneral) && empty($campIndepend)) {
            return [ 'value' => $saleOff, 'percent' => $saleOffPercent ];
        } elseif (!empty($campGeneral)) {
            foreach ($campGeneral as $camp):

                $decreaseByCamp += CampUtils::getPriceDecreaseByProduct($camp, $cart);

            endforeach;
            if ($decreaseByCamp == 0) { // Campaign khuyến mãi chung không áp dụng , chuyển sang áp dụng campaign chạy độc lập
                foreach ($campIndepend as $camp):
                    $decreaseByCamp += CampUtils::getPriceDecreaseByProduct($camp, $cart);
                endforeach;
            }
        } elseif (empty($campGeneral) && !empty($campIndepend)){
            foreach ($campIndepend as $camp):
                
                $decreaseByCamp += CampUtils::getPriceDecreaseByProduct($camp, $cart);
                
            endforeach;
        }

        if ($decreaseByCamp > 0)
            return [ 'value' => $price - $decreaseByCamp, 'percent' => $decreaseByCamp/$price * 100];
        else
            return [ 'value' => $saleOff, 'percent' => $saleOffPercent ];
    }

    public static function getProductInfo($id)
    {
        $product = get_post($id);
        $product->thumb = get_default_thumb_link($product->ID);
        $product->is_new = !empty(get_post_meta( $product->ID, 'post_is_new', true ));
        $product->is_hot = !empty(get_post_meta( $product->ID, 'post_is_hot', true ));
        $product->all_size = [];
        $product->all_size_label = [];

        $sizes = get_post_meta($product->ID, 'post_sizes', true); // Size trong hệ thống
        $onhands = get_post_meta($product->ID, 'post_onhand', true);
        foreach ($sizes as $size):
            $quantityOfSize = isset($onhands[$size]) ? intval($onhands[$size]) : 0 ;
            $quantityOfSize = empty($quantityOfSize) ? 0 : intval($quantityOfSize);
            $product->all_size []= $size;
            $product->all_size_label []= $size.' - ( còn '.$quantityOfSize.' )';
        endforeach;
        $product->size_default = $product->all_size[0];
        $product->price = intval(get_post_meta($product->ID, 'post_price', true));
        $product->sale_off = self::getProductSaleOffPrice($product->ID);
        $catName = isset(wp_get_post_categories($product->ID,array('fields'=> 'names'))[1]) ? wp_get_post_categories($product->ID,array('fields'=> 'names'))[1] : wp_get_post_categories($product->ID,array('fields'=> 'names'))[0];
        $product->cat_name = $catName;
        return $product;
    }

}