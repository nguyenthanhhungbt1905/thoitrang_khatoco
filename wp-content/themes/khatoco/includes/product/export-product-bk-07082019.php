<?php 
define('WP_USE_THEMES', false);
require_once($_SERVER['DOCUMENT_ROOT'] . '/wp-load.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/wp-content/themes/khatoco/includes/phpexcel/PHPExcel.php');


// Orders export fields
$export_orders = array(
	'product_name' => array(
		'required' => 1,
		'label' =>'Tên sản phẩm'
	),
	'sku' => array(
		'required' => 1,
		'label' =>'Mã hàng'
	),
	'color' => array(
		'required' => 1,
		'label' =>'Mã màu'
	),
	'status' =>array(
	    'required' => 1,
        'status'    => 'Status'
    ),
	'price' => array(
		'required' => 1,
		'label' =>'Giá bán'
	),
	'sale' => array(
		'required' => 1,
		'label' =>'Khuyến mãi'
	),
    'post_date' => array(
        'required' => 1,
        'label' =>'Ngày đăng'
    ),
	'onhand' => array(
		'required' => 1,
		'label' =>'Số lượng tồn kho'
	),
    'product_type' => array(
        'required' => 1,
        'label' =>'Loại sản phẩm'
    ),
    'yoast_description' => array(
        'required' => 1,
        'label' =>'Yoast Description'
    ),
    'yoast_FC_keyword' => array(
        'required' => 1,
        'label' =>'Yoast FC keyword'
    ),
    'product_link' => array(
        'required' => 1,
        'label' =>'Link sản phẩm'
    ),
    'product_image_1' => array(
        'required' => 1,
        'label' =>'Link hình ảnh 1'
    ),
    'product_image_2' => array(
        'required' => 1,
        'label' =>'Link hình ảnh 2'
    ),
    'product_image_3' => array(
        'required' => 1,
        'label' =>'Link hình ảnh 3'
    ),
    'product_image_4' => array(
        'required' => 1,
        'label' =>'Link hình ảnh 4'
    ),
    'product_image_5' => array(
        'required' => 1,
        'label' =>'Link hình ảnh 5'
    ),
    'product_infor' => array(
        'required' => 1,
        'label' =>'Thông tin sản phẩm'
    ),
    'post_preservation' => array(
        'required' => 1,
        'label' =>'Hướng dẫn bảo quản'
    ),
    'accessories' => array(
        'required' => 1,
        'label' =>'Phụ kiện đi kèm'
    )

);

$filename = 'Danh sách sản phẩm-'.date("Y-m-d");
$redirect = $_POST['_wp_http_referer'];
global $wpdb;
$table_name = $wpdb->prefix . 'customer_register_cards';
$table_user_register = $wpdb->prefix . 'customer_register';

$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Thời trang Khatoco")->setLastModifiedBy("Thời trang Khatoco");
$row = 1;
$col = ord('A');
$style = array(
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	),
	'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
		'color' => array('rgb' => '666666')
	),
	'font'  => array(
	'color' => array('rgb' => 'FFFFFF'),
	'size'  => 13,
	)
);

foreach ($export_orders as $eokey => $eo) {
	$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $eo['label']);
	$objPHPExcel->getActiveSheet()->getColumnDimension(chr($col-1))->setAutoSize(true);
}
$objPHPExcel->getActiveSheet()->getStyle('A1:T1')->applyFromArray($style);


$args = array('post_type' => 'post',
    			'posts_per_page' => -1,
				'orderby' => 'date',
                'post_status' => array('publish', 'pending', 'draft', 'future', 'private', 'inherit', 'trash'),
				'order' => 'ASC');
$posts = get_posts($args);
$datas = array();
foreach ($posts as $key => $p) {
	$temp =array();
	$temp['product_name'] = $p->post_title;
	$temp['status'] = $p->post_status;
	$temp['post_date'] = date_format(date_create($p->post_date),'d-m-y');
	$temp['sku'] = get_post_meta($p->ID,'post_sku',true);
    $temp['color'] = get_post_meta($p->ID, 'post_colors', true)[0]['color_id'];
    $sale = get_post_meta($p->ID, 'sale_off', true);
    $price = get_post_meta($p->ID, 'post_price', true);
    if ($price == '')
        $temp['price'] = 0;
    $temp['price'] = number_format($price, 0, ',', '.').' đ';
   
    if ($sale != '' && $sale != '0')
        $temp['sale'] = number_format($price * (100 - $sale) / 100, 0, ',', '.').' đ';
    $temp['sale'] = '';
    $temp['like_count'] = get_post_meta($p->ID, 'like_count', true) == '' ? '0' : get_post_meta($p->ID, 'like_count', true);
    $temp['onhand'] = array_sum(get_post_meta($p->ID, 'post_onhand',true));



    $productCates = get_post_meta($p->ID, 'post_category_ids', true);
    $productTypess = [];
    if($productCates){
        foreach( $productCates as $prKey => $prCate){
            $cate = get_the_category_by_ID($prCate);
            if(!$cate->errors && $cate)
                array_push($productTypess, $cate);
        }
    }

    $temp['product_type'] = count($productTypess)>0 ? join(", ", $productTypess) : $productTypess;

    $temp['product_link'] = get_permalink($p->ID);


    $product_colors = get_post_meta($p->ID, 'post_colors', true);

    $productImgs = [];
    if($product_colors && $product_colors[0]['images_ids'])
        $productImgs = explode(',', $product_colors[0]['images_ids']);

    $temp['product_image_1'] = isset($productImgs[0]) ? wp_get_attachment_url($productImgs[0]) : null;
    $temp['product_image_2'] = isset($productImgs[1]) ? wp_get_attachment_url($productImgs[1]) : null;
    $temp['product_image_3'] = isset($productImgs[2]) ? wp_get_attachment_url($productImgs[2]) : null;
    $temp['product_image_4'] = isset($productImgs[3]) ? wp_get_attachment_url($productImgs[3]) : null;
    $temp['product_image_5'] = isset($productImgs[4]) ? wp_get_attachment_url($productImgs[4]) : null;


    $temp['yoast_description'] = get_post_meta($p->ID, '_yoast_wpseo_metadesc', true);
    $temp['yoast_FC_keyword'] = get_post_meta($p->ID, '_yoast_wpseo_focuskw', true);

    $temp['product_infor'] = get_post_meta($p->ID, 'post_info', true);
    $temp['post_preservation'] = get_post_meta($p->ID, 'post_preservation', true);

    $acs = get_post_meta($p->ID, 'post_accessories', true);
    $productAccess = [];
    if($acs){
        foreach ($acs as $pacs => $ac){
            $postProduct = get_post($ac);
            if($postProduct)
                array_push($productAccess, $postProduct->post_title);
        }
    }
    $temp['accessories'] = count($productAccess) > 0 ? join(", ", $productAccess) : $productAccess;


	array_push($datas, $temp);
}


foreach ($datas as $dkey => $data) {
	$row++;
	$col = ord('A');
	$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $data['product_name']);
	$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $data['sku']);
	$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $data['color']);
    $objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $data['status']);
	$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $data['price']);
	$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $data['sale']);
    $objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $data['post_date']);
	$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $data['onhand']);
    $objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $data['product_type']);
    $objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $data['yoast_description']);
    $objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $data['yoast_FC_keyword']);
    $objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $data['product_link']);

    $objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $data['product_image_1']);
    $objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $data['product_image_2']);
    $objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $data['product_image_3']);
    $objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $data['product_image_4']);
    $objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $data['product_image_5']);

    $objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $data['product_infor']);
    $objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $data['post_preservation']);
    $objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $data['accessories']);

	$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(-1); 
	$objPHPExcel->getActiveSheet()->getStyle(chr($col++) . $row)->getAlignment()->setWrapText(true);
}

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment; filename="'. $filename .'.xlsx"');
header('Cache-Control: max-age=0');
header('Cache-Control: max-age=1');
header ('Last-Modified: '. gmdate('D, d M Y H:i:s') .' GMT');
header ('Cache-Control: cache, must-revalidate');
header ('Pragma: public');

PHPExcel_Calculation::getInstance()->writeDebugLog = true;
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
try {
    ob_end_clean();
    $objWriter->save('php://output');
}catch (Exception $e){
    echo 'Caught exception: ',  $e->getMessage(), "\n";
}
wp_redirect($redirect); 
exit;
?>