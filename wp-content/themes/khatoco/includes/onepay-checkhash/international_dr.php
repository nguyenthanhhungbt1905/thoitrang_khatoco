<?php if(!isset($_SESSION)) session_start();
define('WP_USE_THEMES', false);
require_once($_SERVER['DOCUMENT_ROOT'] . '/wp-load.php');

global $_onepay_settings;
$cancel_url = get_permalink(get_page_by_path('khatoco-loi')).'?error='.urlencode('Thanh toán không thành công').'&descr='.urlencode('Lỗi thanh toán không thành công Onepay');

if(!isset($_GET['vpc_SecureHash'])) {
    wp_redirect(add_query_arg(array('paid' => '0', 'statusPayment' => $status), get_page_link(2505)));
    exit();
}

$SECURE_SECRET = $_onepay_settings['international']['sercure_secret'];

$vpc_Txn_Secure_Hash = isset($_GET["vpc_SecureHash"]) ? $_GET["vpc_SecureHash"]:'';
$vpc_MerchTxnRef = isset($_GET["vpc_MerchTxnRef"]) ? $_GET["vpc_MerchTxnRef"]:'';
$vpc_AcqResponseCode = isset($_GET["vpc_AcqResponseCode"]) ? $_GET["vpc_AcqResponseCode"]:'';
unset($_GET["vpc_SecureHash"]);

$errorExists = false;

function null2unknown($data) {
    if($data == '')
        return 'No Value Returned';
    return $data;
}

if (strlen($SECURE_SECRET) > 0 && $_GET["vpc_TxnResponseCode"] != "7" && $_GET["vpc_TxnResponseCode"] != "No Value Returned") {
    ksort($_GET);
    $md5HashData = "";

    foreach ($_GET as $key => $value) {
        if ($key != "vpc_SecureHash" && (strlen($value) > 0) && ((substr($key, 0,4)=="vpc_") || (substr($key,0,5) =="user_"))) {
		    $md5HashData .= $key . "=" . $value . "&";
		}
    }
    $md5HashData = rtrim($md5HashData, "&");

	if (strtoupper ( $vpc_Txn_Secure_Hash ) == strtoupper(hash_hmac('SHA256', $md5HashData, pack('H*',$SECURE_SECRET)))) {
        $hashValidated = "CORRECT";
    }
    else {
        $hashValidated = "INVALID HASH";
    }
}
else {
    $hashValidated = "INVALID HASH";
}

/*
$batchNo = null2unknown($_GET["vpc_BatchNo"]);
$message = null2unknown($_GET["vpc_Message"]);
$cardType = null2unknown($_GET["vpc_Card"]);
$receiptNo = null2unknown($_GET["vpc_ReceiptNo"]);
$acqResponseCode = null2unknown($_GET["vpc_AcqResponseCode"]);*/
$version = null2unknown($_GET["vpc_Version"]);
$command = null2unknown($_GET["vpc_Command"]);
$locale = null2unknown($_GET["vpc_Locale"]);
$amount = null2unknown($_GET["vpc_Amount"]);
$orderInfo = null2unknown($_GET["vpc_OrderInfo"]);
$merchTxnRef = null2unknown($_GET["vpc_MerchTxnRef"]);
$merchantID = null2unknown($_GET["vpc_Merchant"]);
$transactionNo = null2unknown($_GET["vpc_TransactionNo"]);
$txnResponseCode = null2unknown($_GET["vpc_TxnResponseCode"]);
// 3-D Secure Data
/*$verType = array_key_exists("vpc_VerType", $_GET) ? $_GET["vpc_VerType"] : "No Value Returned";
$verStatus = array_key_exists("vpc_VerStatus", $_GET) ? $_GET["vpc_VerStatus"] : "No Value Returned";
$token = array_key_exists("vpc_VerToken", $_GET) ? $_GET["vpc_VerToken"] : "No Value Returned";
$verSecurLevel = array_key_exists("vpc_VerSecurityLevel", $_GET) ? $_GET["vpc_VerSecurityLevel"] : "No Value Returned";
$enrolled = array_key_exists("vpc_3DSenrolled", $_GET) ? $_GET["vpc_3DSenrolled"] : "No Value Returned";
$xid = array_key_exists("vpc_3DSXID", $_GET) ? $_GET["vpc_3DSXID"] : "No Value Returned";
$acqECI = array_key_exists("vpc_3DSECI", $_GET) ? $_GET["vpc_3DSECI"] : "No Value Returned";
$authStatus = array_key_exists("vpc_3DSstatus", $_GET) ? $_GET["vpc_3DSstatus"] : "No Value Returned";*/

$status = '';
if($hashValidated == 'CORRECT' && $txnResponseCode == '0') {
    $status = '0';
}
elseif($hashValidated == 'INVALID HASH' && $txnResponseCode == '0') {
    $status = '1';
}


$orders = get_posts(array(
    'meta_key' => 'order_code',
    'meta_value' => $orderInfo,
    'post_status' => 'draft',
    'post_type' => 'orders'
));

if( !empty($orders) ){
    $order_id = $orders[0]->ID;
    if( $status != '' ){
        wp_update_post( array(
            'ID' => $order_id,
            'post_status' => 'publish'
        ) );
        update_post_meta($order_id, 'order_payment_status', true);
        update_post_meta($order_id, 'order_payment_transaction', $transactionNo);
        $message = get_response_description($txnResponseCode, $orders[0]->ID);

        $order_user_id = get_post_meta( $order_id, 'order_user_id', true);

        if(($order_user_id > 0 || $order_user_id != '0') && ($order_user_id != 443 || $order_user_id != '443')){
            $order_user_data = get_userdata($order_user_id);
            $customer = array(
                'email' => $order_user_data->user_email,
            );
        }
        else {
            $customer = get_post_meta( $order_id, 'customer', true );
        }

        $nonce = wp_create_nonce( 'hash_dr' );
        wp_redirect(
            add_query_arg(array(
                'order_code' => $orderInfo,
                'paid' => '1',
                'statusPayment' => $status,
                'transaction' => $transactionNo,
                'responseCode' => $txnResponseCode,
                'message' => $message,
                '_nonce' => wp_create_nonce( 'hash_dr' )
            ), get_page_link(2505)) 
        );

        // Delete session
        unset($_SESSION['cart']);
        unset($_SESSION['cart_total']);
        
        // Send mail
        mail__announce_new_orders( $customer['email'], get_post( $order_id ));
    }
    else{
        wp_redirect( $cancel_url );
    }
}
else{
    wp_redirect( $cancel_url );
}

exit();
