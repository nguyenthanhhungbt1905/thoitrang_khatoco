<?php
// ini_set("soap.wsdl_cache_enabled", "0");
// libxml_disable_entity_loader(true);
libxml_use_internal_errors(false);

/**
 * Class SoapClientNG extends SoapClient
 * @function __doRequest
 */

class SoapClientNG extends SoapClient{
	function __doRequest($req, $location, $action, $version){
		$xml = parent::__doRequest($req, $location, $action, $version, $one_way = 0);
		$xml = explode("\r\n", $xml);
		$response = preg_replace( '/^(\x00\x00\xFE\xFF|\xFF\xFE\x00\x00|\xFE\xFF|\xFF\xFE|\xEF\xBB\xBF)/', '', $xml[6] );
        return $response;
    }
}

/**
 * Class SyncData
 * @version 1.1.1
 */

// if(current_user_can('manage_options')):
	class SyncData {
		private $key = '';
		// private $wsdl = 'http://khatoco.timevn.com/CRMService.svc?wsdl';
		// private $wsdl = 'http://accnetsvr.lacviet.vn/CRM/CRMService.svc?wsdl';
		private $wsdl = 'http://113.160.249.112:9898/CRMService.svc?wsdl';
		// private $wsdl = 'CRMService.svc.xml';

		private $sync;

		function __construct() {
			try{
				$opt = array(
					'trace' => 1,
	                'encoding' => 'UTF-8',
	                'exceptions' => 1,
					// 'cache_wsdl' => WSDL_CACHE_NONE,
				);
				$this->sync = new SoapClient($this->wsdl, $opt);
			}
			catch(SoapFault $e){
				echo '<pre>';
				var_dump(libxml_get_last_error());
    			var_dump($e);
				echo '</pre>';
			}
		}

		function __destruct() {
			unset($this->sync);
			return true;
		}

		// Customer Services ------------------------------------------
		function getCustomerInfo( $c ) {
			if( is_null($c) )
				return false;

			return array(
				'id' => $c->CustID, // id
				'name' => $c->CustName, // ten
				'email' => $c->Email, // email
				'repemail' => $c->RepEmail, // rep email
				'isb2b' => $c->IsB2B, //
				'aliasname' => $c->CustName, // ten hien thi 1
				'aliasname2' => $c->CustName2, // ten hien thi 2
				'fullname' => $c->CustFullName, // ten day du
				'address' => $c->Address, // dia chi
				'phone' => $c->PhoneNo1, // sdt
				'cellphone' => $c->CellPhone, // sdt
				'phone2' => $c->PhoneNo2, // sdt2
				'vatid' => $c->RegVATID, // ma so thue VAT
				'birthday' => $c->Birthday, // ngay sinh
				'skypeid' => $c->SkypeID, // skype
				'twitter' => $c->Twitter, // twitter
				'facebook' => $c->Facebook, // facebook
				'yahoo' => $c->YahooID, // yahoo
				'establishday' => $c->EstablishDay, // ngay tao tk ?!?
				'annual_revenue' => $c->AnnualRevenue,
			);
		}

		function buildCustomerObj( $cid ) {
			$cid = intval( $cid );
			$user = get_userdata( $cid );
			if(!$user) return false;

			$c = new stdClass();
			$c->CustID = get_user_meta( $cid, 'code_crm', true );
			$c->CustName = ($user->display_name) ? $user->display_name : get_user_meta( $cid, 'name', true );
			$c->Email = $user->user_email;
			$c->RepEmail = get_user_meta( $cid, 'repemail', true );
			$c->IsB2B = get_user_meta( $cid, 'isb2b', true );
			$c->CustName = get_user_meta( $cid, 'aliasname', true );
			$c->CustName2 = get_user_meta( $cid, 'aliasname2', true );
			$c->CustFullName = $user->display_name;
			$c->Address = get_user_meta( $cid, 'address', true );
			$c->PhoneNo1 = get_user_meta( $cid, 'phone', true );
			$c->CellPhone = get_user_meta( $cid, 'cellphone', true );
			$c->PhoneNo2 = get_user_meta( $cid, 'phone2', true );
			$c->RegVATID = get_user_meta( $cid, 'vatid', true );
			$c->Birthday = get_user_meta( $cid, 'birthday', true );
			$c->SkypeID = get_user_meta( $cid, 'skypeid', true );
			$c->Twitter = get_user_meta( $cid, 'twitter', true );
			$c->Facebook = get_user_meta( $cid, 'facebook', true );
			$c->YahooID = get_user_meta( $cid, 'yahoo', true );
			$c->EstablishDay = get_user_meta( $cid, 'establishday', true );
			$c->AnnualRevenue = get_user_meta( $cid, 'annual_revenue', true );
			$c->NoOfEmployees = 0;

			return $c;
		}

		function getCustomers($condition) {
			// @service LoadCustomer
			$r = array();
			$obj = (object) array('Key' => $this->key, 'Condition' => $condition);
			$res = (array) $this->sync->LoadCustomer($obj);
			// return $res;

			if(!empty($res['LoadCustomerResult']->CustomerList)) {
				foreach ($res['LoadCustomerResult']->CustomerList as $ckey => $c) {
					if(isset($c->CustID) && $c->CustID != '') {
						$data = $this->getCustomerInfo( $c );
						$r[] = $data;
					}
				}
				if(!empty($r))
					return array('datas' => $r, 'result' => 'data-available');
			}

			return array('message' => 'No sync data', 'result' => 'no-data');
		}

		function getCustomer($cid) {
			// @service GetCustomer
			$obj = (object) array('Key' => $this->key, 'CusID' => $cid);
			$res = (array)$this->sync->GetCustomer($obj);
			if( !empty($res) ){
				$c = $res['GetCustomerResult'];
				return $this->getCustomerInfo( $c );
			}
			else return false;
		}

		function addCustomer($cid) {
			// @service AddCustomer
			$obj = $this->buildCustomerObj( $cid );
			$req = (object) array('Key' => $this->key, 'CusID' => $obj);
			$res = (array) $this->sync->AddCustomer($req);

			// return $res;

			if( isset( $res['AddCustomerResult'] ) ){
				if( $res['AddCustomerResult'] == '' ){
					return true;
				}
				else return $res['AddCustomerResult'];
			}
		}

		function updateCustomer($cid) {
			// @service UpdateCustomer
			$obj = $this->buildCustomerObj( $cid );
			$req = (object) array('Key' => $this->key, 'CusID' => $obj);
			$res = (array) $this->sync->UpdateCustomer($req);
			return $res['UpdateCustomerResult'];
		}

		function deleteCustomer($cid) {
			// @service DeleteCustomer
			$cid = intval( $cid );
			$user = get_userdata( $cid );
			if(!$user) return false;

			$code = get_post_meta( $cid, 'code_crm', true );
			$req = (object) array('Key' => $this->key, 'CusID' => $code);
			$res = (array) $this->sync->UpdateCustomer($req);
			return $res['DeleteCustomerResult'];
		}

		// Product Items Services -------------------------------------
		function getProductItem($pid) {
			// @service GetItemInfo
			$obj = (object) array('Key' => $this->key, 'ItemID' => $pid);
			$res = (array) $this->sync->GetItemInfo($obj);
			// return $res;
			if(isset($res['GetItemInfoResult']->ItemList)){
				$p = $res['GetItemInfoResult']->ItemList;
				if(isset($p->ItemID))
					return array(
						'id' => $p->ItemID,
						'fullname' => $p->ItemFullName,
						'name' => $p->ItemName,
						'name2' => $p->ItemName2,
						'invMearsure' => $p->InvMearsure,
						'sellCnvFactor' => $p->SellCnvFactor,
						'sellMearsure' => $p->SellMearsure,
					);
				else return false;
			}
			else return false;
		}

		// All already sizes
		function getProductSizes($pid){
			// @service  GetSize
			$obj = (object) array('Key' => $this->key, 'SizeID' => $pid);
			$res = (array) $this->sync->GetSize($obj);
			if(isset($res['GetSizeResult']->SizeInfo)){
				return $res['GetSizeResult']->SizeInfo;
			}
			else return false;
		}

		// All already colors
		function getProductColors($pid){
			// @service GetColor
			$obj = (object) array('Key' => $this->key, 'ColorID' => $pid);
			$res = (array) $this->sync->GetColor($obj);
			if(isset($res['GetColorResult']->ColorInfo)){
				return $res['GetColorResult']->ColorInfo;
			}
			else return false;
		}

		function getBUInfo($pid){
			// @service GetBU
			$obj = (object) array('Key' => $this->key, 'BUID' => $pid);
			$res = (array) $this->sync->GetBU($obj);
			if(isset($res['GetBUResult']->BUInfo)){
				return $res['GetBUResult']->BUInfo;
			}
			else return false;
		}

		function getProductOnHand($pid){
			// @service  GetItemOnHand
			$obj = (object) array('Key' => $this->key, 'ItemID' => $pid);
			$res = (array) $this->sync->GetItemOnHand($obj);
			return $res;
			if(isset($res['GetItemOnHandResult'])){
				$datas = array();
				foreach ($res['GetItemOnHandResult']->ItemOnhand as $ikey => $item) {
					if(isset($item->OnHand) && (int) $item->OnHand > 0 ) {
						$sku = $item->ItemID;
						$color_id = $item->ColorID;
						$size_id = $item->SizeID;
						$shop_id = $item->StoreHouseID;
						$buid = $item->BUID;

						$item_info = $this->getProductItem( $sku );
						$color = ($color_id) ? $this->getProductColors( $color_id ) : $color_id;
						$size = ($size_id) ? $this->getProductSizes( $size_id ) : $size_id;
						$bu = ($buid) ? $this->getBUInfo( $buid ) : $buid;
						if($item_info){
							$item_info['sku'] = $sku;
							$item_info['color'] = $color;
							$item_info['size'] = $size;
							$item_info['shop'] = $shop_id;
							$item_info['buid'] = $bu;
						}

						$item_info['onhand'] = (int) $item->OnHand;

						$datas[$sku] = $item_info;
					}
				}
				return $datas;
			}
			else return false;
		}

		// Orders Services --------------------------------------------
		function getOrdersList($condition) {
			// @service LoadOrder
			$r = array();
			$obj = (object) array('Key' => $this->key, 'Condition' => $condition);
			$res = (array)$this->sync->LoadOrder($obj);
			$res = $res['LoadOrderResult'];

			if(!empty($res->OrderList)) {
				foreach ($res->OrderList as $okey => $o) {
					if(isset($o->OrderNo)) {
						$data = array(
							'id' => $o->OrderNo,
							'order_date' => $o->OrderDate,
							'order_user_id' => $o->CustID,
							'order_user_name' => $o->BuyerName,
							'order_user_addr' => $o->BuyerAddr,
							'order_user_tel' => $o->BuyerTel,

							'delivery_addr' => $o->DeliveryAddr,
							'delivery_meth' => $o->DeliveryMeth,

							'order_receiver' => $o->Receiver,
							'order_receiver_tel' => $o->ReceiverTel,
							'order_request_date' => $o->RequestDate,
							'order_payment_method' => $o->PayMethodID,

							'order_status' => $o->Status,
						);

						$orderDetails = $o->OrderDetail->OrderDetail;
						$dataDetails = array();

						if($orderDetails) {
							foreach ($orderDetails as $odkey => $od) {
								if(isset($od->ItemID)){
									$cart_id = $od->ItemID .'__'. $od->ColorID .'__'. $od->SizeID;

									$p = get_product_by_sku($od->ItemID);
									$meta_p = array(
										'id' => 0,
										'name' => '<no title>',
										'permalink' => get_template_directory_uri() .'/images/default-shirt.jpg',
										'thumb' => get_template_directory_uri() .'/images/default-shirt.jpg',
										'weight' => 0,
									);

									if($p) {
										$meta_p['id'] = $p->ID;
										$meta_p['name'] = get_the_title( $p->ID );
										$meta_p['permalink'] = get_permalink( $p->ID );
										$meta_p['weight'] = get_post_meta($p->ID, 'post_weight', true);

										$product_colors = get_post_meta($p->ID, 'post_colors', true);
										$images_ids = explode(',', $product_colors[0]['images_ids']);
										$images_ids = $images_ids[0];
										$images_ids = wp_get_attachment_image_src($images_ids, 'medium');
										$meta_p['thumb'] = $images_ids[0];
									}

									$dataDetails[$cart_id] = array(
										'sku' => $od->ItemID,
										'size' => $od->SizeID,
										'all_size' => array(),
										'color' => $od->ColorID,
										'color_name' => NULL,
										'color_code' => NULL,
										'quantity' => $od->Quantity,
										'uomid' => $od->UOMID,

										'id' => $meta_p['id'],
										'name' => $meta_p['name'],
										'permalink' => $meta_p['permalink'],
										'thumb' => $meta_p['thumb'],

										'price' => $od->UnitPrice,
										'price_saleoff' => '',
										'price_unsaleoff_formated' => aj_format_number((int)$od->UnitPrice * (int)$od->Quantity),
										'price_formated' => aj_format_number($od->UnitPrice),
										'price_total' => ((int)$od->UnitPrice * (int)$od->Quantity),

										'discPercent' => $od->DiscPercent,
										'notes' => $od->Notes,
										'vatid' => $od->VATID,
										'vatamount' => $od->VATAmount,
										'AmountBefVAT' => $od->AmountBefVAT,
										'cart_id' => $cart_id,
										'weight' => $meta_p['weight'],
										'gift' => false
									);
								}
							}

							$data['order_info']['products'] = $dataDetails;
							$data['order_info']['checkoutNote'] = $o->Memo;
						}

						$r[] = $data;
					}
				}
				if(!empty($r)){
					// $r = array_slice($r, 10);
					return array('datas' => $r, 'result' => 'data-available');
				}
			}

			return array('message' => 'No sync data', 'result' => 'no-data');
		}

		function getOrders($oid) {
			// @service GetOrder
			$obj = (object) array('Key' => $this->key, 'OrderNo' => $oid);
			$res = (array) $this->sync->GetOrder($obj);
			if(!empty($res))
				return $res['GetOrderResult'];
			else return false;
		}

		// Return obj for add or update orders
		function buildOrdersObj($oid){
			if(get_post_type( $oid ) != 'orders')
				return false;

			// $orders = get_post( $oid, ARRAY_A );
			$order_info = get_post_meta( $oid, 'order_info', true );
			$order_user_id = get_post_meta( $oid, 'order_user_id', true );

			$obj = new stdClass();
			$obj->BuyerAddr = NULL;
			$obj->BuyerName = NULL;
			$obj->BuyerTel = NULL;
			$obj->CustID = 'CUST00000014';//strval( $order_user_id );
			$obj->DeliveryAddr = ( isset( $order_info['address'] ) ) ? $order_info['address']:NULL;
			$obj->DeliveryMeth = ( isset( $order_info['shippingMethod'] ) ) ? strval( $order_info['shippingMethod'] ):NULL;
			$obj->Memo = ( isset( $order_info['checkoutNote'] ) ) ? $order_info['checkoutNote']:'';
			$obj->OrderDate = date( "Y-m-d\TH:i:s", strtotime( get_the_time( 'Y-m-d H:i:s', $oid ) ) );

			if($order_user_id != 0 || $order_user_id != '0'){
				$userdata = get_userdata( intval( $order_user_id ) );
				$obj->BuyerAddr = get_user_meta( $order_user_id, 'user_address', true );
				$obj->BuyerName = isset($userdata->display_name) ? $userdata->display_name : '';
				$obj->BuyerTel = get_user_meta( $order_user_id, 'user_phone', true );
			}


			if(isset($order_info['products'])){
				if(count($order_info['products']) > 1)
					$temp_details = new ArrayObject();
				else
					$temp_details = new stdClass();

				foreach ($order_info['products'] as $key => $product) {
					$temp = new stdClass();
					$temp->AmountBefVAT = strval( $product['quantity'] * (int) $product['price'] );
					$temp->ColorID = $product['color_name'];
					$temp->DiscPercent = '';
					$temp->ItemID = get_post_meta( (int) $product['id'], 'post_sku', true );
					$temp->Notes = '';
					$temp->OrderNo = get_post_meta( $oid, 'order_code', true );
					$temp->Quantity = strval( $product['quantity'] );
					$temp->SizeID = $product['size'];
					$temp->UOMID = 'KG';
					$temp->UnitPrice = $product['price'];
					$temp->VATAmount = '';
					$temp->VATID = '';

					if(count($order_info['products']) > 1)
						$temp_details->append( $temp );
					else
						$temp_details = $temp;
				}

				$obj->OrderDetail = $temp_details;
			}

			$obj->OrderNo = get_post_meta( $oid, 'order_code', true );
			$obj->PayMethodID = get_post_meta( $oid, 'order_payment_method', true ); // 1, 2, 3
			$obj->Receiver = ( isset($order_info['email']) ) ? $order_info['email']:NULL;
			$obj->ReceiverTel = ( isset( $order_info['phone'] ) ) ? $order_info['phone']:NULL;
			$obj->RequestDate = date( "Y-m-d\TH:i:s", strtotime( get_the_time( 'Y-m-d H:i:s', $oid ) ) );
			$obj->Status = get_post_meta( $oid, 'order_status', true );
			$obj->POSID = get_post_meta( $oid, 'order_pos', true ); // POS

			return $obj;
		}

		function addOrders($oid) {
			// @service AddOrder
			$obj = $this->buildOrdersObj( $oid );
			$req = (object) array('Key' => $this->key, 'Order' => $obj);
			$res = (array) $this->sync->AddOrder($req);
			if(isset($res['AddOrderResult']) && $res['AddOrderResult'] == '')
				return true;

			return $res['AddOrderResult'];
		}

		function updateOrders($oid) {
			// @service UpdateOrder
			$obj = $this->buildOrdersObj( $oid );
			$req = (object) array('Key' => $this->key, 'Order' => $obj);
			$res = (array) $this->sync->UpdateOrder($req);
			if(isset($res['UpdateOrderResult']) && $res['UpdateOrderResult'] == '')
				return true;

			return $res['UpdateOrderResult'];
		}

		function deleteOrders( $oid ){
			// @service DeleteOrder
			if(get_post_type( $oid ) != 'orders')
				return false;

			$code = get_post_meta( $oid, 'order_code', true );
			$req = (object) array('Key' => $this->key, 'OrderNo' => $code);
			$res = (array) $this->sync->DeleteOrder($req);
			if(isset($res['DeleteOrderResult']) && $res['DeleteOrderResult'] == '')
				return true;
			
			return $res['DeleteOrderResult'];
		}

		function getPOS( $condition ){
			// @service GetPOS
			$req = (object) array('Key' => $this->key, 'POSID' => $condition);
			$res = (array) $this->sync->GetPOS($req);
			// return $res;

			if(isset( $res['GetPOSResult']->POSInfo )){
				$pos_a = array();

				foreach($res['GetPOSResult']->POSInfo as $poskey => $pos) {
					$temp = array(
						'address' => $pos->Address,
						'buid' => $pos->BUID,
						'email' => $pos->Email,
						'fax' => $pos->Fax,
						'posid' => $pos->POSID,
						'name' => $pos->POSName,
						'tel' => $pos->Tel,
					);

					$pos_a[$pos->POSID] = $temp;
				}
				return update_option( 'pos_options', $pos_a );
				// return $pos_a;
			}
			return 0;
		}
	}
// endif;


/*----------------------------------------------------------------------------------------------------------------*/
/* User SOAP Web services support */
// Insert new user if user not exists, or update that exists user
function sync_insert_user($data) {
	if(!isset($data['email'])) return false;

	if(email_exists( $data['email'] )){
		$new_user_id = email_exists( $data['email'] );
	}
	else{
		$new_user_id = wp_insert_user(array(
			'user_login' => $data['email'],
			'user_email' => $data['email'],
			'user_pass' => wp_generate_password(12),
		));
	}

	if( is_wp_error($new_user_id) || !$new_user_id ) {
		$r = $new_user_id->get_error_message();

		if($r == 'Sorry, that username already exists!'){
			return false;
		}
	}
	else {
		wp_update_user(array(
			'ID' => $new_user_id,
			'display_name' => $data['fullname'],
			'user_registered' => $data['establishday'],
			'show_admin_bar_front' => false,
			'show_admin_bar_admin' => false,
		));

		update_user_meta($new_user_id, 'code_crm', $data['id']);

		update_user_meta($new_user_id, 'name', $data['name']);
		update_user_meta($new_user_id, 'aliasname', $data['aliasname']);
		update_user_meta($new_user_id, 'aliasname2', $data['aliasname2']);
		update_user_meta($new_user_id, 'fullname', $data['fullname']);

		if(isset($data['birthday'])){
			update_user_meta($new_user_id, 'birthday', date('d/m/Y', strtotime($data['birthday'])));
		}

		update_user_meta($new_user_id, 'repemail', $data['repemail']);
		update_user_meta($new_user_id, 'phone', $data['phone']);
		update_user_meta($new_user_id, 'phone2', $data['phone2']);
		update_user_meta($new_user_id, 'cellphone', $data['cellphone']);
		update_user_meta($new_user_id, 'address', $data['address']);

		update_user_meta($new_user_id, 'isb2b', $data['isb2b']);
		update_user_meta($new_user_id, 'vatid', $data['vatid']);
		update_user_meta($new_user_id, 'skypeid', $data['skypeid']);
		update_user_meta($new_user_id, 'twitter', $data['twitter']);
		update_user_meta($new_user_id, 'facebook', $data['facebook']);
		update_user_meta($new_user_id, 'yahoo', $data['yahoo']);

		return true;
	}
}

function sync_insert_orders($data) {
	if(!isset($data['id'])) return false;
	return false; // not to get orders data from crm to web

	$o_id = wp_insert_post(array(
		'post_type' => 'orders',
		'post_status' => 'pending',
		'post_title' => $data['order_date'] .' - '. $data['id'],
		'post_date' => $data['order_date']
	));

	if( is_wp_error($o_id) ) {
		return false;
	}
	else {
		update_post_meta($o_id, 'order_code', $data['id']);

		update_post_meta($o_id, 'order_user_id', $data['order_user_id']);
		update_post_meta($o_id, 'order_shipping_method', $data['delivery_meth']);
		update_post_meta($o_id, 'order_payment_method', $data['order_payment_method']);

		update_post_meta($o_id, 'order_user_name', $data['order_user_name']);
		update_post_meta($o_id, 'order_user_addr', $data['order_user_addr']);
		update_post_meta($o_id, 'order_user_tel', $data['order_user_tel']);

		update_post_meta($o_id, 'delivery_addr', $data['delivery_addr']);
		update_post_meta($o_id, 'delivery_meth', $data['delivery_meth']);

		update_post_meta($o_id, 'order_receiver', $data['order_receiver']);
		update_post_meta($o_id, 'order_receiver_tel', $data['order_receiver_tel']);
		update_post_meta($o_id, 'order_request_date', $data['order_request_date']);

		update_post_meta($o_id, 'order_info', $data['order_info']);

		// Update for full postmeta
		update_post_meta($o_id, 'hasCoupon', '');
		update_post_meta($o_id, 'couponName', '');
		update_post_meta($o_id, 'couponSaleoffValue', '');
		update_post_meta($o_id, 'isVip', '');
		update_post_meta($o_id, 'vipAfter', '');

		$order_status = 'done';
		switch ($data['order_status']) {
			case '': $order_status = 'neworder'; break;
			case 'neworder': $order_status = 'neworder'; break;
		}

		update_post_meta($o_id, 'order_status', $order_status);

		return true;
	}
}

function sync_insert_products($data) {
	if(!isset($data['sku'])) return false;

	$posts = get_posts( array(
		'post_type' => 'post',
		'posts_per_page' => -1,
		'post_status' => 'publish',
		'meta_key' => 'post_sku',
		'meta_value' => $data['sku'],
		'meta_compare' => '=',
	) );

	if( !$posts ){
		$o_id = wp_insert_post(array(
			'post_type' => 'post',
			'post_title' => $data['name'],
			'post_status' => 'pending',
			// 'post_author' => 2
		));
		if( is_wp_error($o_id) ) {
			return false;
		}
		update_post_meta($o_id, 'post_sku', $data['sku']);
		update_post_meta($o_id, 'fullname', $data['fullname']);
		update_post_meta($o_id, 'name', $data['name']);
		// update_post_meta($o_id, 'name2', $data['name2']);

		update_post_meta($o_id, 'onhand', $data['onhand']);
		update_post_meta($o_id, 'invMearsure', $data['invMearsure']);
		update_post_meta($o_id, 'sellCnvFactor', $data['sellCnvFactor']);
		update_post_meta($o_id, 'sellMearsure', $data['sellMearsure']);
		update_post_meta($o_id, 'colors_crm', $data['color']);
		$color = array();
		if(isset($data['color']) && isset($data['color']->ColorID)){
			$color = array(
				0 => array(
					'name' => isset($data['color']->ColorName) ? $data['color']->ColorName:'',
					'id' => isset($data['color']->ColorID) ? $data['color']->ColorID:'',
					'thumb' => '',
					'images_ids' => '',
				)
			);
		}
		update_post_meta($o_id, 'post_colors', $color);
		update_post_meta($o_id, 'size_crm', $data['size']);
		update_post_meta($o_id, 'buid_crm', $data['buid']);
		update_post_meta($o_id, 'shop', $data['shop']);

		return true;
	}
	else {
		$post = $posts[0];
		$o_id = $post->ID;
		wp_update_post(array(
			'id' => $o_id,
			'post_title' => $data['name'],
			'post_status' => $post->post_status
		));

		update_post_meta($o_id, 'fullname', $data['fullname']);
		update_post_meta($o_id, 'name', $data['name']);
		// update_post_meta($o_id, 'name2', $data['name2']);

		update_post_meta($o_id, 'onhand', $data['onhand']);
		update_post_meta($o_id, 'invMearsure', $data['invMearsure']);
		update_post_meta($o_id, 'sellCnvFactor', $data['sellCnvFactor']);
		update_post_meta($o_id, 'sellMearsure', $data['sellMearsure']);

		/* // Update colors of products
		$color = get_post_meta( $o_id, 'post_colors', true );
		if(isset($data['color'])){
			$color = array(
				0 => array(
					'name' => $data['color']->ColorName,
					'id' => $data['color']->ColorID
				)
			);
		}
		update_post_meta($o_id, 'post_colors', $color);*/

		update_post_meta($o_id, 'size_crm', $data['size']);
		update_post_meta($o_id, 'buid_crm', $data['buid']);
		update_post_meta($o_id, 'shop', $data['shop']);

		return true;
	}
}

function sync_push_customer($data){
	$data = intval( $data );
	$sync = new SyncData();
	$res = $sync->addCustomer($data);
	if(isset($res) && $res == '')
		return true;
	return false;
}

function sync_push_orders($data){
	$data = intval( $data );
	$sync = new SyncData();
	$res = $sync->addOrders($data);
	// return $res;
	if(isset($res) && $res == '')
		return true;
	return false;
}

if(is_admin() && get_current_user_id() == 2 && isset($_GET['test']) && $_GET['test'] = '1'){
	// wp_redirect( 'http://tungvn.info/' ); exit;
	$sync = new SyncData();
	// $a = file_get_contents( 'http://113.160.249.112:9898/CRMService.svc?wsdl' );
	// $a = file_get_contents( 'CRMService.svc.xml' );
	echo '<pre>';
	// echo $a;
	// var_dump( $sync->addOrders( 9000 ) );
	// var_dump($sync->getProductItem('10-00011CIS'));
	// var_dump($sync->getProductOnHand(''));
	var_dump($sync->getCustomers(''));
	// var_dump($sync->getPOS(''));
	echo '</pre>';
}

