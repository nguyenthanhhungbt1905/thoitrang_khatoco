<?php 
define('WP_USE_THEMES', false);
require_once($_SERVER['DOCUMENT_ROOT'] . '/wp-load.php');

if( current_user_can( 'edit_posts' ) ){
	if( isset( $_GET['orderid'] ) && $_GET['orderid'] ){
		$post_id = $_GET['orderid'];
		$order_info = get_post_meta( $post_id, 'order_info', true);
		if( !isset($order_info['products']) || empty($order_info['products']) || empty($order_info)) {
            echo '<h2>Đơn hàng bị huỷ do bị xoá hết sản phẩm!</h2>';
            exit;
        }

        ?>
        <div class="container clearfix">
            <table>
                <tr>
                    <th width="80px">Kho hàng</th>
                    <th width="30px">STT</th>
                    <th>Sản phẩm</th>
                    <th width="40px">Size</th>
                    <th width="60px">SL Mua</th>
                    <th width="50px">SL Tồn</th>
                </tr>
                <?php
                $number = 0;
                $stores = get_option( 'storehouses' );
                $stores_txt = implode( ',', array_keys( $stores ) );
                $datas = array();

                foreach($order_info['products'] as $pkey => $p){
                    $item_id = get_post_meta( $p['id'], 'post_sku', true );
                    $item_color = $p['color_code'];
                    $item_size = $p['size'];
                    $item_quantity = $p['quantity'];

                    $results = sync_getProductOnHand($item_id, $stores_txt, 0, 800);

                    if( !empty( $results ) ){
                        $onhands = 0;
                        foreach ($results as $rkey => $r) {
                            $_item_id = $r['ItemID'];
                            $_color_id = $r['ColorID'];
                            $_on_hand = $r['OnHand'];
                            $_size_id = $r['SizeID'];
                            $_store_id = $r['StoreHouseID'];
                            $_sku = $_item_id .'-'. $_color_id;

                            if( $item_id == $_item_id && $item_color == $_color_id && $item_size == $_size_id ){
                                if( !isset( $datas[$_store_id] ) )
                                    $datas[$_store_id] = array();

                                $onhands += $_on_hand;

                                $datas[$_store_id][] = array(
                                    'item_id' => $item_id,
                                    'item_color' => $item_color,
                                    'item_size' => $item_size,
                                    'item_quantity' => $item_quantity,
                                    'onhands' => $onhands,
                                );
                            }
                        }
                    }
                }

                if( !empty( $datas ) ){
                    foreach ($datas as $dkey => $data):
                        $new_row = true;
                        foreach ($data as $ddkey => $d): ?>
                        <tr><?php if( $new_row ): ?>
                            <td rowspan="<?php echo count( $data ); ?>">
                                <?php echo $stores[$dkey]; ?>
                            </td><?php $new_row = false;
                            endif; ?>
                            
                            <td><?php echo $ddkey+1; ?></td>
                            <td><?php echo $d['item_id'] .'-'. $item_color; ?></td>
                            <td><?php echo $d['item_size']; ?></td>
                            <td><?php echo $d['item_quantity']; ?></td>
                            <td><?php echo $d['onhands']; ?></td>
                        </tr>
                        <?php endforeach;
                    endforeach;
                }


                /*foreach($order_info['products'] as $pkey => $p){
                    $number++;
                    $onhands = get_post_meta( $p['id'], 'post_onhands', true );
                    $store_with_onhand = get_post_meta( $p['id'], 'stores_with_onhand', true ); ?>
                    <tr class="each-product" data-product-id="<?php echo $p['id']; ?>">
                        <td><?php echo $number; ?> <a href="javascript:;" class="drop-product-item">Xoá</a></td>
                        <td><a href="<?php echo get_permalink($p['id']); ?>" target="_blank"><?php echo $p['name']; ?></a></td>
                        <td><?php echo $p['color_code']; ?><br /><?php echo $p['color_name']; ?></td>
                        <td><?php $sizes = $p['all_sizes']; ?>
                        <select name="products[<?php echo $pkey; ?>][size]" class="can-changed size"><?php foreach ($sizes as $skey => $s) {
                            echo '<option value="'. $s .'" '. selected($p['size'], $s, false) .'>'. $s .'</option>';
                        } ?>
                        </select></td>
                        <td><input type="number" min="1" name="products[<?php echo $pkey; ?>][quantity]" class="can-changed quantity" value="<?php echo $p['quantity']; ?>" style="width:50px"></td>
                        <td><?php echo str_replace(array(',', '|'), array('<br>', ': '), $store_with_onhand[$p['size']]); ?></td>
                        <td><input type="hidden" name="products[<?php echo $pkey; ?>][price]" class="can-changed price" value="<?php echo $p['price_saleoff']; ?>"><?php echo $p['price_formated']; ?></td>
                    </tr>
                <?php }*/ ?>
            </table>
        </div>
	<?php }
}
else{
	echo '<h1>404 Not Found!</h1>';
}

?>

<link rel="stylesheet" type="text/css" href="../style.css">
<style type="text/css">
    table, tr, th, td{
        border-collapse: collapse;
    }
    table{
        width: 100%;
        margin: 20px 0 0;
    }
    th, td{
        padding: 5px;
        border: 1px solid #655;
        color: #616161;
        font-size: 14px;
    }
</style>