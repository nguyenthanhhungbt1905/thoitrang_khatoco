<?php

//$sync_url = 'http://khatoco.timevn.com/crm';
$sync_url = 'http://crm.khatocofashion.net/api/crm';
//$sync_url = 'http://localhost/api/public';
//$sync_url = 'http://api.thoitrang.demo.vn/api/crm';


function call_service_egift_ajax($url,$data = array()){
       
        $options = array(
        'http' => array(
        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
        'method'  => 'POST',
        'content' => http_build_query($data),
            )
        );

        $context  = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        return $result; 
                

 }



// JSON Client Call
function client_call_service($part_url, $args = array(), $method = 'GET')
{
    global $sync_url;
    $url = $sync_url . '/' . $part_url;
    $ch = curl_init();
    switch ($method) {
        case 'POST':
            curl_setopt($ch, CURLOPT_POST, true);
            if (!empty($args))
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($args));
            break;

        default: // GET
            if (!empty($args))
                $url = sprintf("%s?%s", $url, http_build_query($args));
            break;
    }
//    curl_setopt($ch, CURLOPT_COOKIE, "XDEBUG_SESSION=PHPSTORM");
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json'
    ));

    $res = curl_exec($ch);
    curl_close($ch);
    if ($res)
        return json_decode($res, true);
    else
        return false;
}

function client_call_url($url, $method = 'GET')
{
    $ch = curl_init();

    switch ($method) {
        case 'POST':
            curl_setopt($ch, CURLOPT_POST, true);
            if (!empty($args))
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($args));
            break;

        default: // GET
            if (!empty($args))
                $url = sprintf("%s?%s", $url, http_build_query($args));
            break;
    }

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json'
    ));

    $res = curl_exec($ch);
    curl_close($ch);
    if ($res)
        return json_decode($res, true);
    else
        return false;
}

// Customer Services ------------------------------------------
function sync_getCustomerInfo($c)
{
    if (is_null($c))
        return false;

    return array(
        'id' => $c['CustID'], // id
        'name' => $c['CustName'], // ten
        'email' => $c['Email'], // email
        'repemail' => $c['RepEmail'], // rep email
        'isb2b' => $c['IsB2B'], //
        'aliasname' => $c['CustName'], // ten hien thi 1
        'aliasname2' => $c['CustName2'], // ten hien thi 2
        'fullname' => $c['CustFullName'], // ten day du
        'address' => $c['Address'], // dia chi
        'phone' => $c['PhoneNo1'], // sdt
        'cellphone' => $c['CellPhone'], // sdt
        'phone2' => $c['PhoneNo2'], // sdt2
        'vatid' => $c['RegVATID'], // ma so thue VAT
        'birthday' => $c['Birthday'], // ngay sinh
        'skypeid' => $c['SkypeID'], // skype
        'twitter' => $c['Twitter'], // twitter
        'facebook' => $c['Facebook'], // facebook
        'yahoo' => $c['YahooID'], // yahoo
        'establishday' => $c['EstablishDay'], // ngay tao tk ?!?
        'annual_revenue' => $c['AnnualRevenue'],
        'order_revenue' => $c['OrderRevenue'],
        'card_id' => $c['TypeCardID'],
        'card_number' => $c['CardID'],
        'gender' => $c['Sex'],
    );
}

function sync_buildCustomerObj($cid)
{
    $cid = intval($cid);
    $user = get_userdata($cid);
    if (!$user) return null;

    $code_crm = get_user_meta($cid, 'code_crm', true);

    $c = new stdClass();
    $c->CustID = ($code_crm != '') ? $code_crm : 'WEB' . str_pad($cid, 9, '0', STR_PAD_LEFT);
    $c->CustName = isset($user->display_name) ? $user->display_name : get_user_meta($cid, 'name', true);
    $c->Email = $user->user_email; // .'|'. get_user_meta( $cid, 'user_phone', true );
    $c->CardID = get_user_meta($cid, 'user_card_id', true);
    $c->RepEmail = get_user_meta($cid, 'repemail', true);
    $c->IsB2B = get_user_meta($cid, 'isb2b', true);
    $c->CustName = get_user_meta($cid, 'aliasname', true);
    $c->CustName2 = get_user_meta($cid, 'aliasname2', true);
    $c->CustFullName = $user->display_name;

    $temp = $addr_temp = '';
    $fulladdress = get_user_meta($cid, 'user_address', true);
    $user_provinceid = get_user_meta($cid, 'user_provinceid', true);
    if (isset($user_provinceid)) {
        $temp = aj_get_province($user_provinceid);
        $addr_temp = $temp['name'];

        $user_districtid = get_user_meta($cid, 'user_districtid', true);
        if (isset($user_districtid)) {
            $temp = aj_get_district($user_districtid);
            $addr_temp = $temp['name'] . ', ' . $addr_temp;

            $user_wardid = get_user_meta($cid, 'user_wardid', true);
            if (isset($user_wardid)) {
                $temp = aj_get_ward($user_wardid);
                $addr_temp = $temp['name'] . ', ' . $addr_temp;
            }
        }
    }

    if ($addr_temp) {
        $fulladdress .= ', ' . $addr_temp;
    }
    $c->Address = $fulladdress;

    $c->PhoneNo1 = get_user_meta($cid, 'user_phone', true);
    $c->CellPhone = get_user_meta($cid, 'user_phone', true);
    $c->PhoneNo2 = get_user_meta($cid, 'user_phone', true);
    $c->RegVATID = get_user_meta($cid, 'vatid', true);
    $birthday = get_user_meta($cid, 'user_birthday', true);
    if ($birthday) {
        $birthday = $birthday['year'] . '/' . $birthday['month'] . '/' . $birthday['day'];
    }
    $c->Birthday = date("Y-m-d\TH:i:s", strtotime($birthday));
    $gender = get_user_meta($cid, 'user_gender', true);
    if (isset($gender['id'])) {
        if ($gender['id'] == 'male')
            $c->Sex = '1';
        elseif ($gender['id'] == 'female')
            $c->Sex = '2';
    }

    return $c;
}

function sync_addCustomerRegister($data = array())
{
    if ($data) {
        $required = array('CustID', 'PhoneNo1', 'CellPhone', 'Email');
        foreach ($required as $r) {
            if (!array_key_exists($r, $data))
                return false;
        }

        $obj = new stdClass();
        foreach ($data as $key => $v) {
            $obj->$key = $v;
        }

        $args = array(
            'Key' => '',
            'Customer' => $obj,
        );

        $res = client_call_service('AddCustomer', $args, 'POST');
        if ($res['code'] === 1)
            return true;
        else
            return $res['data'];
    }
    return false;
}

function sync_getCustomers($condition = '', $paged = 1, $limit = 2000)
{
    // @service LoadCustomer
    $args = array(
        'Key' => '',
        'Condition' => $condition,
        'pageCurrent' => $paged,
        'RecordOfPage' => $limit,
    );
    $res = client_call_service('LoadCustomer', $args);
    return $res;
}

function sync_getCustomer($cid)
{
    // @service GetCustomer
    $res = client_call_service('GetCustomer/' . $cid,null);
    return $res['ret'];
}

function sync_getCustomerCard($cid)
{
    $res = client_call_service('GetCardInfo/' . $cid, null);
    return $res['ret'];
}

function sync_addCustomer($cid)
{
    $obj = sync_buildCustomerObj($cid);
    $obj->CheckEmail = true;
    $obj->CheckSMS = true;
    $args = array(
        'Key' => '',
        'Customer' => $obj,
    );
    $res = client_call_service('AddCustomer', $args, 'POST');

    return $res['code'] ==1;
}

function sync_updateCustomer($cid)
{
    $obj = sync_buildCustomerObj($cid);

    if (get_current_user_id() == 2) {
        exit;
    }
    $args = array(
        'Key' => '',
        'Customer' => $obj,
    );

    $res = client_call_service('UpdateCustomer', $args, 'POST');

    return $res['code'] ==1;
}

function sync_deleteCustomer($cid)
{
    // @service DeleteCustomer
    $args = array(
        'Key' => '',
        'custid' => $cid,
    );
    $res = client_call_service('DeleteCustomer/' . $cid, $args);

    return $res['code'] ==1;
}

// Product Items Services -------------------------------------

function sync_getProductItem($pid)
{
    // @service GetItemInfo
    $args = array(
        'Key' => '',
        'itemid' => $pid,
    );
    $res = client_call_service('GetItemInfo/'. $pid);

    return $res['ret'];
}

function sync_getProductSizes($sizeid = '')
{
    // @service GetSize
    $args = array(
        'Key' => '',
        'sizeid' => $sizeid,
    );
    $res = client_call_service('GetSize', $args);

    return $res['ret'];
}

function sync_getProductColors($colorid = '')
{
    // @service GetColor
    $args = array(
        'Key' => '',
        'colorid' => $colorid,
    );
    $res = client_call_service('GetColor', $args);

    return $res['ret'];
}

function sync_getBUInfo($buid = '')
{
    // @service GetBU
    $args = array(
        'Key' => '',
        'buid' => $buid,
    );
    $res = client_call_service('GetBU', $args);

    return $res['ret'];
}

function sync_getProductOnHand($itemid = '', $storeid = '', $paged = 0, $limit = 1000)
{
    // @service GetItemOnHandPaging
    $args = array(
        'Key' => '',
        'ItemId' => $itemid,
        'StoreHouseID' => $storeid,
        'pageCurrent' => $paged,
        'RecordOfPage' => $limit,
    );
    $res = client_call_service('GetItemOnHandPaging', $args);
    return $res['ret'];
}

// Orders Services --------------------------------------------

function sync_buildOrdersObj($oid)
{
    if (get_post_type($oid) != 'orders')
        return false;

    $oid = intval($oid);

    $order_info = get_post_meta($oid, 'order_info', true);
    if (!isset($order_info['products']) || empty($order_info['products']))
        return false;

    $order_code = get_post_meta($oid, 'order_code', true);
    $order_total = get_post_meta($oid, 'order_total', true);
    $order_user_id = get_post_meta($oid, 'order_user_id', true);
    $customer = array();
    if ($order_user_id > 0 || $order_user_id != '0') {
        $order_user_data = get_userdata($order_user_id);
        $customer = array(
            'email' => $order_info['email'],
            'phone' => $order_info['phone'],
            'address' => $order_info['address'],
        );
    } else {
        $customer = get_post_meta($oid, 'customer', true);
    }
    $receiver = get_post_meta($oid, 'receiver', true);
    $code_crm = get_user_meta($order_user_id, 'code_crm', true);

    $obj = new stdClass();
    $obj->BuyerAddr = $customer['address'];
    $obj->BuyerName = (isset($order_user_data) && $order_user_data != '') ? $order_user_data->data->display_name : 'Khách hàng vãng lai';;
    $obj->BuyerTel = $customer['phone'];

    $obj->CustID = ($code_crm != '') ? $code_crm : 'WEB' . str_pad($order_user_id, 9, '0', STR_PAD_LEFT);
    $obj->DeliveryAddr = $receiver != '' ? $receiver['address'] : $order_info['address'];
    $obj->DeliveryMeth = isset($order_info['shippingMethod']) ? strval($order_info['shippingMethod']) : NULL;
    $obj->OrderDate = date("Y-m-d\TH:i:s", strtotime(get_the_time('Y-m-d H:i:s', $oid)));

    if (isset($order_info['products'])) {
        $temp_details = array();
        foreach ($order_info['products'] as $key => $product) {
            $temp = new stdClass();
            $temp->AmountBefVAT = strval($product['price_saleoff']['value'] * $product['quantity']);
            $temp->ColorID = $product['color_code'];
            $temp->DiscPercent = 0;
            $temp->ItemID = get_post_meta((int)$product['id'], 'post_sku', true);
            $temp->Notes = '';
            $temp->OrderNo = $order_code;
            $temp->Quantity = strval($product['quantity']);
            $temp->SizeID = $product['size'];
            $temp->UOMID = get_post_meta((int)$product['id'], 'post_unit', true);
            $temp->UnitPrice = $product['price_saleoff']['value'];
            $temp->VATAmount = 0;
            $temp->VATID = '';
            $temp->SOID = aj_random_string(5);

            $temp_details[] = $temp;
        }

        // Add sản phẩm được tặng kèm
        if (!empty($order_info['cart']['campaignInfo']['listBonus']['listProdBonus'])) :
        foreach ($order_info['cart']['campaignInfo']['listBonus']['listProdBonus'] as $key => $product) {
            if ($product['quantity'] > 0) {
                $temp = new stdClass();
                $temp->AmountBefVAT = '0';
                $temp->ColorID = get_post_meta($product['ID'], 'post_color_id', true);
                $temp->DiscPercent = 0;
                $temp->ItemID = get_post_meta((int)$product['ID'], 'post_sku', true);
                $temp->Notes = '';
                $temp->OrderNo = $order_code;
                $temp->Quantity = strval($product['quantity']);
                $temp->SizeID = $product['size'];
                $temp->UOMID = get_post_meta((int)$product['ID'], 'post_unit', true);
                $temp->UnitPrice = $product['sale_off']['value'];
                $temp->VATAmount = 0;
                $temp->VATID = '';
                $temp->SOID = aj_random_string(5);

                $temp_details[] = $temp;
            }
        }
        endif;

        // Add sản phẩm được bán với giá đặc biệt
        if (!empty($order_info['cart']['campaignInfo']['listSpecialPrice']['listProdBonus'])) :
            foreach ($order_info['cart']['campaignInfo']['listSpecialPrice']['listProdBonus'] as $key => $product) {
                if ($product['quantity'] > 0) {
                    $temp = new stdClass();
                    $temp->AmountBefVAT = strval($product['price'] * $product['quantity']);
                    $temp->ColorID = get_post_meta($product['ID'], 'post_color_id', true);
                    $temp->DiscPercent = 0;
                    $temp->ItemID = get_post_meta((int)$product['ID'], 'post_sku', true);
                    $temp->Notes = '';
                    $temp->OrderNo = $order_code;
                    $temp->Quantity = strval($product['quantity']);
                    $temp->SizeID = $product['size'];
                    $temp->UOMID = get_post_meta((int)$product['ID'], 'post_unit', true);
                    $temp->UnitPrice = $product['price'];
                    $temp->VATAmount = 0;
                    $temp->VATID = '';
                    $temp->SOID = aj_random_string(5);

                    $temp_details[] = $temp;
                }
            }
        endif;

        $obj->OrderDetail = $temp_details;
    }

    $obj->OrderNo = $order_code;
    $obj->BaseSellingAmount = floatval($order_total);
    // $obj->DiscAmount = 

    $order_payment_method = get_post_meta($oid, 'order_payment_method', true); // 1, 2, 3
    if ($order_payment_method == 2) {
        $obj->PayMethodID = 'ATM ngân hàng nội địa';
        $obj->Memo = 'Thanh toán ATM ngân hàng nội địa (transationNo: ' . get_post_meta($oid, 'order_payment_transaction', true) . ')';
    } elseif ($order_payment_method == 3) {
        $obj->PayMethodID = 'Visa/Master Card';
        $obj->Memo = 'Thanh toán Visa/Master Card (transationNo: ' . get_post_meta($oid, 'order_payment_transaction', true) . ')';
    } else {
        $obj->PayMethodID = 'Tiền mặt';
        $obj->Memo = 'Thanh toán Tiền mặt';
    }

    //$order_payment_status = get_post_meta($oid, 'order_payment_status', true);
    //$obj->Memo .= ' -- ' . ($order_payment_status == true ? ' Đã thanh toán' : ' Chưa thanh toán');

    /*if( isset( $order_info['giftProducts'] ) && !empty( $order_info['giftProducts'] ) ){
        $obj->Memo .= ' -- Quà tặng: ';
        $gift = '';
        $i = 1;
        foreach( $order_info['giftProducts'] as $gp ){
            $gift .= $i++ .'. '. $gp['name'] .'('. $gp['desc'] .')';
            if( $gp['message'] != '' )
                $gift .= 'với lời nhắn "'. $gp['message'] .'"';

            $gift .= ': '. aj_format_number( $gp['giftTotal'] ) .'VND';
            if( $i-1 < count($order_info['giftProducts']) )
                $gift .=  ' - ';
        }
        $obj->Memo .= $gift;
    }*/

    //$obj->Memo .= (isset($order_info['checkoutNote']) && $order_info['checkoutNote'] != '') ? ' -- ' . $order_info['checkoutNote'] : '';

    $obj->Memo .= ' | ' . floatval($order_total);

    if (!empty($receiver)) {
        $obj->Receiver = $receiver['fullname'];
        $obj->ReceiverTel = $receiver['phone'];
    } else {
        $obj->Receiver = $obj->BuyerName;
        $obj->ReceiverTel = $obj->BuyerTel;
    }

    $obj->RequestDate = date("Y-m-d\TH:i:s", strtotime(get_the_time('Y-m-d H:i:s', $oid)));
    $obj->Status = get_post_meta($oid, 'order_status', true);
    $obj->POSID = get_post_meta($oid, 'order_pos', true); // POS

    return $obj;
}

function sync_addOrder($oid)
{
    // @service AddOrder
    $order = sync_buildOrdersObj($oid);
    $args = array(
        'Key' => '',
        'Order' => $order,
    );
    $res = client_call_service('AddOrder', $args, 'POST');
    return $res['code'] ==1;
}

function sync_updateOrder($oid)
{
    // @service UpdateOrder
    $order = sync_buildOrdersObj($oid);
    $args = array(
        'Key' => '',
        'order' => $order,
    );
    $res = client_call_service('UpdateOrder/' . $oid, $args, 'POST');
    return $res['code'] ==1;
}

function sync_deleteOrder($oid)
{
    // @service DeleteOrder
    $args = array(
        'Key' => '',
        'OrderNo' => $oid,
    );
    $res = client_call_service('DeleteOrder/' . $oid, $args);

    return $res['code'] ==1;
}

// Get POS infos
function sync_getPOS($condition = '')
{
    $args = array(
        'Key' => '',
        'condition' => $condition,
    );
    $res = client_call_service('GetPOS', $args);

    $pos_a = array();

    foreach ($res as $pos) {
        $temp = array(
            'address' => $pos['Address'],
            'buid' => $pos['BUID'],
            'district_id' => $pos['DistrictID'],
            'email' => $pos['Email'],
            'fax' => $pos['Fax'],
            'memo' => $pos['Memo'],
            'posid' => $pos['POSID'],
            'name' => $pos['POSName'],
            'tel' => $pos['Tel'],
        );

        $pos_a[$pos['POSID']] = $temp;
    }
    // return $pos_a;
    update_option('pos_options', $pos_a);

    return true;
}

// Get POS infos
function sync_getStore($store = '')
{
    $args = array(
        'Key' => '',
        'StoreHouseID' => $store,
    );
    $res = client_call_service('GetStoreHouse', $args);

    $stores = array();
    foreach ($res as $s) {
        $stores[$s['StoreHouseID']] = $s['StoreHouseName'];
    }

    update_option('storehouses', $stores);

    return true;
}


/* Web services support */
// Insert new user if user not exists, or update that exists user
function sync_update_user($data, $syncing = false)
{
    if (!isset($data['cellphone']) || $data['cellphone'] == '') {
        return false;
    }

    $new_user = true;
    if (!isset($data['email']) && $data['email'] == '') {
        $data['email'] = 'ktccrm.' . $data['cellphone'] . '@thoitrangkhatoco.vn';
    }

    global $wpdb;
    $new_user_id_by_username = username_exists($data['cellphone']);
    $new_user_id_by_email = email_exists($data['email']);
    $new_user_id_by_phone = $wpdb->get_var("SELECT DISTINCT user_id FROM $wpdb->usermeta WHERE meta_key='user_phone' AND meta_value='" . $data['cellphone'] . "'");

    if ($new_user_id_by_email && !$syncing) {
        return false;
    }

    if (!$new_user_id_by_username && !$new_user_id_by_phone) {
        $new_user_id = wp_insert_user(array(
            'user_login' => $data['cellphone'],
            'user_email' => $data['email'],
            'user_pass' => wp_generate_password(12),
        ));
    } else {
        $new_user = false;
        if ($syncing) {
            if ($new_user_id_by_username) $new_user_id = intval($new_user_id_by_username);
            if ($new_user_id_by_phone) $new_user_id = intval($new_user_id_by_phone);
            if ($new_user_id_by_email) $new_user_id = intval($new_user_id_by_email);
        } else {
            return 0; // khi da co CRM va co Web thi ko cap nhat, thong bao nguoi dung da co
        }
    }

    // Dang tu dong lay ID da co va cap nhat, can co buoc xac thuc

    if (!is_wp_error($new_user_id) && $new_user_id) {
        $fullname = $data['fullname'] ? $data['fullname'] : $data['name'];

        if (!$new_user) {
            if (isset($data['establishday'])) {
                $establishday = php_convert_json_date($data['establishday']);
                update_user_meta($new_user_id, 'establishday', $establishday);
            }

            $args = array(
                'ID' => $new_user_id,
                'display_name' => $fullname,
                'user_registered' => $establishday,
                'show_admin_bar_front' => false,
                'show_admin_bar_admin' => false,
            );

            if (strpos($data['id'], 'CRM') !== false && strpos($data['email'], 'ktccrm.') !== false) {
                #if crm user and no email, do nothing
            } else {
                $args['user_email'] = $data['email'];
            }

            wp_update_user($args);
        }

        update_user_meta($new_user_id, 'code_crm', $data['id']);
		// làm cho card nội bộ đồng nhất với card ví dụ GOLD = GOLDNB
        switch(strtoupper($data['card_id'])){
        	case 'SLIVERNB':	//sai chính tả sẵn
        		$data['card_id'] = 'SILVER'; break;
        	case 'GOLDNB':
        		$data['card_id'] = 'GOLD'; break;
        	case 'DIAMONDNB':
        		$data['card_id'] = 'DIAMOND'; break;
        }
        update_user_meta($new_user_id, 'user_card', $data['card_id']);      
        update_user_meta($new_user_id, 'user_card_id', $data['card_number']);

        update_user_meta($new_user_id, 'name', $fullname);
        update_user_meta($new_user_id, 'aliasname', $data['aliasname']);
        update_user_meta($new_user_id, 'aliasname2', $data['aliasname2']);
        update_user_meta($new_user_id, 'fullname', $fullname);

        if (isset($data['birthday'])) {
            $date = php_convert_json_date($data['birthday']);
            list($__y, $__m, $__d) = explode('/', $date);
            $birthday = array(
                'year' => $__y,
                'month' => $__m,
                'day' => $__d,
            );
            update_user_meta($new_user_id, 'user_birthday', $birthday);
            update_user_meta($new_user_id, 'user_birthday_txt', $date);
        }

        $gender = array('id' => 'male', 'name' => 'Nam');
        if ($data['gender'] == '2')
            $gender = array('id' => 'female', 'name' => 'Nữ');
        update_user_meta($new_user_id, 'user_gender', $gender);

        update_user_meta($new_user_id, 'repemail', $data['repemail']);
        update_user_meta($new_user_id, 'user_phone', $data['cellphone']);
        // update_user_meta($new_user_id, 'address', $data['address']);

        update_user_meta($new_user_id, 'order_revenue', $data['order_revenue']);

        update_user_meta($new_user_id, 'isb2b', $data['isb2b']);
        update_user_meta($new_user_id, 'vatid', $data['vatid']);
        /*update_user_meta($new_user_id, 'skypeid', $data['skypeid']);
        update_user_meta($new_user_id, 'twitter', $data['twitter']);
        update_user_meta($new_user_id, 'facebook', $data['facebook']);
        update_user_meta($new_user_id, 'yahoo', $data['yahoo']);*/

        return $new_user_id;
    }

    if ($syncing) {
        return false;
    } else {
        $message = $new_user_id->get_error_message();

        if ($message == 'Sorry, that username already exists!') {
            return 'Tên người dùng đã có người sử dụng.';
        } elseif ($message == 'Sorry, that email address is already used!') {
            return 'Email đã có người sử dụng.';
        }

        return false;
    }
}

function sync_update_products($pid)
{
    $p_item_id = get_post_meta($pid, 'post_sku', true);
    $p_store_id = implode(',', array_keys(get_option('storehouses', array())));
    $colors = get_post_meta($pid, 'post_colors', true);

    $p_color_id = '';
    if (!isset($colors[0]['color_id'])) {
        return false;
    }
    $p_color_id = $colors[0]['color_id'];
    $p_sku = $p_item_id . '-' . $p_color_id;

    $returns = array();
    $new_onhands = array();
    $new_store_with_onhand = array();

    $results = sync_getProductOnHand($p_item_id, $p_store_id, 0, 800);

    if (!empty($results)) {
        foreach ($results as $rkey => $r) {
            $item_id = $r['ItemID'];
            $color_id = $r['ColorID'];
            $buid = $r['BUID'];
            $on_hand = $r['OnHand'];
            $size_id = $r['SizeID'];
            $store_id = $r['StoreHouseID'];
            $sku = $item_id . '-' . $color_id;

            if ($color_id == $p_color_id) {
                if (!isset($returns[$sku])) {
                    $returns[$sku] = array(
                        'ItemID' => $item_id,
                        'ColorID' => $color_id,
                        'Sizes' => array(),
                    );
                }

                if (intval($on_hand) > 0) {
                    if (!isset($returns[$sku]['Sizes'][$size_id])) {
                        $returns[$sku]['Sizes'][$size_id] = array();
                        $returns[$sku]['Sizes'][$size_id]['total'] = 0;
                    }

                    if (!isset($returns[$sku]['Sizes'][$size_id][$store_id]))
                        $returns[$sku]['Sizes'][$size_id][$store_id] = array();

                    $returns[$sku]['Sizes'][$size_id]['total'] += $on_hand;
                    $returns[$sku]['Sizes'][$size_id][$store_id] = array(
                        'BUID' => $buid,
                        'OnHand' => $on_hand,
                    );
                }
            }
        }
    }

    if (!empty($returns) && isset($returns[$p_sku]['Sizes'])) {
        foreach ($returns[$p_sku]['Sizes'] as $skey => $sdetails) {
            $new_onhands[$skey] = $sdetails['total'];

            $index = 1;
            foreach ($sdetails as $store => $infos) {
                if ($store != 'total') {
                    if (!isset($new_store_with_onhand[$skey]))
                        $new_store_with_onhand[$skey] = '';
                    $new_store_with_onhand[$skey] .= $store . '|' . $infos['OnHand'] . ',';
                }

                if ($index++ == count($sdetails)) {
                    $new_store_with_onhand[$skey] = substr($new_store_with_onhand[$skey], 0, -1);
                }
            }

        }

        update_post_meta($pid, 'post_onhand', $new_onhands);
        update_post_meta($pid, 'stores_with_onhand', $new_store_with_onhand);
        return true;
    }

    return false;
}

function sync_push_customer($data)
{
    $data = intval($data);
    $res = sync_addCustomer($data);
    return $res;
}

function sync_push_orders($data)
{
    $data = intval($data);
    $res = sync_addOrder($data);
    return $res;
}

function php_convert_json_date($date)
{
    if (strpos($date, 'Date') !== false)
        $date = substr($date, 6, -2);
    //  $date = strval( abs( intval( $date ) ) );
    //  $date = $date / 1000;
    $split_date = explode('T',$date);
//    $date_format = date_format($split_date[0].' '.$split_date[1],"Y-m-d H:i:s");
    $date = new DateTime( $split_date[0].' '.$split_date[1],new DateTimeZone('Asia/Ho_Chi_Minh'));
    return $date->format('Y/m/d');
}

if (is_admin() && get_current_user_id() == 2 && isset($_GET['test']) && $_GET['test'] = '1') {
    echo '<pre>';
    // update_option( 'storehouses', array( 'KHBSMVD' => 'Kho Công ty - SR Viễn Đông Size màu' ) );
    // $stores = get_option( 'storehouses' );
    // print_r( $stores );
    echo '</pre>';
}
