<?php 
define('WP_USE_THEMES', false);
require_once($_SERVER['DOCUMENT_ROOT'] . '/wp-load.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/wp-content/themes/khatoco/includes/phpexcel/PHPExcel.php');


// Orders export fields
$export_orders = array(
	'product_name' => array(
		'required' => 1,
		'label' =>'Tên sản phẩm'
	),
	'sku' => array(
		'required' => 1,
		'label' =>'SKU'
	),
	'color' => array(
		'required' => 1,
		'label' =>'Color'
	),
	'price' => array(
		'required' => 1,
		'label' =>'Giá sản phẩm'
	),
	'sale' => array(
		'required' => 1,
		'label' =>'Giá khuyến mãi'
	),
	'like' => array(
		'required' => 1,
		'label' =>'Like'
	),
	'onhand' => array(
		'required' => 1,
		'label' =>'Số lượng tồn'
	),
);

$filename = '-Danh sách sản phẩm-'.date("Y-m-d");
$redirect = $_POST['_wp_http_referer'];
global $wpdb;
$table_name = $wpdb->prefix . 'customer_register_cards';
$table_user_register = $wpdb->prefix . 'customer_register';

$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Thời trang Khatoco")->setLastModifiedBy("Thời trang Khatoco");
$row = 1;
$col = ord('A');
$style = array(
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	),
	'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
		'color' => array('rgb' => '666666')
	),
	'font'  => array(
	'color' => array('rgb' => 'FFFFFF'),
	'size'  => 13,
	)
);

foreach ($export_orders as $eokey => $eo) {
	$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $eo['label']);
	$objPHPExcel->getActiveSheet()->getColumnDimension(chr($col-1))->setAutoSize(true);
}
$objPHPExcel->getActiveSheet()->getStyle('A1:G1')->applyFromArray($style);


$args = array('post_type' => 'post',
    			'posts_per_page' => -1,
				'orderby' => 'date',
				'order' => 'ASC');
$posts = get_posts($args);
$datas = array();
foreach ($posts as $key => $p) {
	$temp =array();
	$temp['product_name'] = $p->post_title;
	$temp['sku'] = get_post_meta($p->ID,'post_sku',true);
    $temp['color'] = get_post_meta($p->ID, 'post_colors', true)[0]['color_id'];
    $sale = get_post_meta($p->ID, 'sale_off', true);
    $price = get_post_meta($p->ID, 'post_price', true);
    if ($price == '')
        $temp['price'] = 0;
    $temp['price'] = number_format($price, 0, ',', '.').' đ';
   
    if ($sale != '' && $sale != '0')
        $temp['sale'] = number_format($price * (100 - $sale) / 100, 0, ',', '.').' đ';
    $temp['sale'] = '';
    $temp['like_count'] = get_post_meta($p->ID, 'like_count', true) == '' ? '0' : get_post_meta($p->ID, 'like_count', true);
    $temp['onhand'] = array_sum(get_post_meta($p->ID, 'post_onhand',true));
	array_push($datas, $temp);
}

foreach ($datas as $dkey => $data) {
	$row++;
	$col = ord('A');
	$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $data['product_name']);
	$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $data['sku']);
	$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $data['color']);
	$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $data['price']);
	$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $data['sale']);
	$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $data['like_count']);
	$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $data['onhand']);
	$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(-1); 
	$objPHPExcel->getActiveSheet()->getStyle(chr($col++) . $row)->getAlignment()->setWrapText(true);
}

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment; filename="'. $filename .'.xlsx"');
header('Cache-Control: max-age=0');
header('Cache-Control: max-age=1');
header ('Last-Modified: '. gmdate('D, d M Y H:i:s') .' GMT');
header ('Cache-Control: cache, must-revalidate');
header ('Pragma: public');

PHPExcel_Calculation::getInstance()->writeDebugLog = true;
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
try {
    ob_end_clean();
    $objWriter->save('php://output');
}catch (Exception $e){
    echo 'Caught exception: ',  $e->getMessage(), "\n";
}
wp_redirect($redirect); 
exit;
?>