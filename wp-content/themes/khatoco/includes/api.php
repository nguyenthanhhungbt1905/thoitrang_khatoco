<?php 
require 'Slim/Slim.php';
\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();

$app->group('/api', function () use ($app) {
	$app->get(
		'/getNews',
		function () use ($app) {
			header("Content-Type: application/json");
			// $app->response->headers->set('Content-Type', 'application/json');
			$args = array('post_type' => 'news', 'posts_per_page' => 5);
			echo get_posts_by($args);
			exit();
		}
	);

	$app->get(
		'/getNews/:paged',
		function ($paged) use ($app) {
			header("Content-Type: application/json");
			$args = array('post_type' => 'news', 'posts_per_page' => 5, 'offset' => $paged*5);
			echo get_posts_by($args);
			exit();
		}
	);

	$app->get(
		'/getNews/:number/:offset',
		function ($number, $offset) use ($app) {
			header("Content-Type: application/json");
			$args = array('post_type' => 'news', 'posts_per_page' => $number, 'offset' => $offset);
			echo get_posts_by($args);
			exit();
		}
	);

	$app->get(
		'/getNews/:number/:offset/:paged',
		function ($number, $offset, $paged) use ($app) {
			header("Content-Type: application/json");
			$args = array('post_type' => 'news', 'posts_per_page' => $number, 'offset' => $offset, 'paged' => $paged);
			echo get_posts_by($args);
			exit();
		}
	);

	$app->get(
		'/getThumbnail/:post_id',
		function ($post_id) use ($app) {
			header("Content-Type: application/json");
			if(has_post_thumbnail( $post_id )) {
				$img = wp_get_attachment_url( get_post_thumbnail_id($post_id) );
				echo json_encode(array(
					'status' => 'OK',
					'results' => $img
				));
			}
			else {
				echo json_encode(array(
					'status' => 'Fail',
					'results' => 'No thumbnail found!'
				));
			}
			exit();
		}
	);

	$app->get(
		'/getPost/:post_id',
		function ($post_id) use ($app) {
			header("Content-Type: application/json");
			$post = get_post( $post_id );
			$post_content = apply_filters('the_content', $post->post_content);
			echo json_encode(array(
				'title' => apply_filters('the_title', $post->post_title),
				'date' => $post->post_date,
				'content' => $post_content
			));
			exit;
		}
	);

	$app->get(
		'/getPostsRelated/:post_id',
		function ($post_id) use ($app) {
			header("Content-Type: application/json");
			$args = array('posts_per_page' => 4, 'post_type' => 'news', 'post__not_in' => array($post_id));
			echo get_posts_by($args);
			exit;
		}
	);

	$app->notFound(function () use ($app){
		$app->stop();
		exit;
	});

	$app->error(function (\Exception $e) use ($app){
		$app->stop();
		exit;
	});
});

function get_posts_by( $args ) {
	$news = get_posts( $args );
	if($news)
		return json_encode(array(
			'status' => 'OK',
			'results' => $news
		));
	else
		return json_encode(array(
			'status' => 'Empty',
			'results' => 'No posts found!'
		));
}

$app->run();