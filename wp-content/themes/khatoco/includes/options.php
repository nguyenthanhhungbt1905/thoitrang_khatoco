<?php
// THEME OPTIONS
add_action( 'admin_menu', 'hl_theme_option_page' );
function hl_theme_option_page() {
	add_options_page('Khatoco Options', 'Khatoco Options', 'manage_options', 'ns-theme-options', 'theme_options');
}

add_action( 'admin_init', 'hl_register_setting' );
function hl_register_setting() {
	// Social and hotline
	register_setting('ns-theme-option', 'facebook_link');
	register_setting('ns-theme-option', 'google_plus_link');
	register_setting('ns-theme-option', 'linkedin_link');
	register_setting('ns-theme-option', 'pinterest_link');
	register_setting('ns-theme-option', 'issuu_link');
	register_setting('ns-theme-option', 'hotline');
	// Catalogue
	register_setting('ns-theme-option', 'catalogue');
	// Fee
	register_setting('ns-theme-option', 'wrap_fee');
	register_setting('ns-theme-option', 'card_fee');
	// Backlink list
	register_setting('ns-theme-option', 'backlink');
	// Home Popup
	register_setting( 'ns-theme-option', 'home-popup' );
    register_setting( 'ns-theme-option', 'home-popup-link' );
    register_setting('ns-theme-option', 'banner-event-news');
    register_setting('ns-theme-option', 'banner-event-event-activities');
    for ($i=1; $i < 6; $i++) {
    	register_setting('ns-theme-option', 'banner'.$i);
    	register_setting('ns-theme-option', 'banner-link'.$i);
    	register_setting('ns-theme-option', 'intro-text'.$i);
    	register_setting('ns-theme-option', 'intro-link'.$i);
    	register_setting('ns-theme-option', 'intro-bg-color'.$i);
    	register_setting('ns-theme-option', 'intro-text-color'.$i);
    }

}

/* Get attachment image source by ID */
function timevn_attachment_src( $attachmentID, $size = 'full' ) {
    $imageSrc = get_template_directory_uri() . '/images/thumbnail.png';

    if( wp_attachment_is_image( $attachmentID ) ) {
        $imageInfo = wp_get_attachment_image_src( $attachmentID, $size);
        $imageSrc = $imageInfo[0];
    }

    return $imageSrc;
}


add_action('admin_enqueue_scripts', 'cm_enqueue_scripts_styles');
function cm_enqueue_scripts_styles($hook) {
	wp_register_script('my-upload', get_bloginfo( 'stylesheet_directory' ) . '/assets/js/admin/uploader.js');
    wp_enqueue_script('my-upload');
    wp_enqueue_style('thickbox');
    wp_enqueue_script( 'thickbox' );
}

function theme_options() {
	add_action('admin_head', 'options_enqueue_media_lib_func');
	function options_enqueue_media_lib_func() {
		wp_enqueue_media();
	} ?>
	<div class="wrap">
            <h2>Khatoco Options</h2>

            

		<form id="theme-options-form" method="POST" action="options.php">
			<?php settings_fields( 'ns-theme-option' ); ?>
			<table class="theme-options form-table">
<tr>
	<th colspan="2"><h3>Intro Banner</h3><hr></th>
</tr>
<?php
	for ($i=1; $i < 6; $i++) { 
		$banner = get_option('banner'.$i);
?>
<tr>
	<th>Link <?php echo $i; ?></th>
	<td><input type="text" class="regular-text" name="banner-link<?php echo $i; ?>" id="banner-link<?php echo $i; ?>" value="<?php echo get_option('banner-link'.$i); ?>"></td>
</tr>
<tr>
	<th>Banner <?php echo $i; ?></th>
	<td>
		<div class="controls">
			<input size="40" class="upload-url" type="text" name="banner<?php echo $i; ?>" value="<?php echo $banner; ?>" />
		    <input class="st_upload_button button media_upload_button" type="button" name="upload_button" value="Upload" />
	    </div>
	    <?php if(!empty($banner)){ ?>
	    <div class="screenshot">
            <img src="<?php echo home_url() . '/wp-content/uploads/' . $banner; ?>" class="of-option-image" />
        </div>
		<?php } ?>
	</td>
</tr>
<?php } ?>
                <tr>

                    <?php $banner = get_option('banner-event-news'); ?>
                    <th style="padding: 20px 0">
                        Banner "Tin tức"</th>
                    <td style="padding: 20px 0">
                        <div class="controls">

                            <input size="40" class="upload-url" type="text" name="banner-event-news" value="<?php echo $banner; ?>" />
                            <input class="st_upload_button button media_upload_button" type="button" name="upload_button" value="Upload" />
                        </div>
                        <?php if(!empty($banner)){ ?>
                            <div class="screenshot">
                                <img src="<?php echo home_url() . '/wp-content/uploads/' . $banner; ?>" class="of-option-image" />
                            </div>
                        <?php } ?>
                    </td>
                </tr>
                <tr>

                    <?php $banner = get_option('banner-event-event-activities'); ?>
                    <th style="padding: 20px 0">
                        Banner "Sự kiện - hoạt động"</th>
                    <td style="padding: 20px 0">
                        <div class="controls">

                            <input size="40" class="upload-url" type="text" name="banner-event-event-activities" value="<?php echo $banner; ?>" />
                            <input class="st_upload_button button media_upload_button" type="button" name="upload_button" value="Upload" />
                        </div>
                        <?php if(!empty($banner)){ ?>
                            <div class="screenshot">
                                <img src="<?php echo home_url() . '/wp-content/uploads/' . $banner; ?>" class="of-option-image" />
                            </div>
                        <?php } ?>
                    </td>
                </tr>

<?php
	for ($i=1; $i < 6; $i++) { 
		$intro = get_option('intro'.$i);
?>
<tr>
	<th>Intro Text <?php echo $i; ?></th>
	<td><input type="text" class="regular-text" name="intro-text<?php echo $i; ?>" id="intro-text<?php echo $i; ?>" value="<?php echo get_option('intro-text'.$i); ?>"></td>
</tr>
<tr>
	<th>Intro Link <?php echo $i; ?></th>
	<td><input type="text" class="regular-text" name="intro-link<?php echo $i; ?>" id="intro-link<?php echo $i; ?>" value="<?php echo get_option('intro-link'.$i); ?>"></td>
</tr>
<tr>
	<th>Intro Background Color <?php echo $i; ?></th>
	<td><input type="text" class="regular-text" name="intro-bg-color<?php echo $i; ?>" id="intro-bg-color<?php echo $i; ?>" value="<?php echo get_option('intro-bg-color'.$i); ?>"></td>
</tr>
<tr>
	<th>Intro Text Color <?php echo $i; ?></th>
	<td><input type="text" class="regular-text" name="intro-text-color<?php echo $i; ?>" id="intro-text-color<?php echo $i; ?>" value="<?php echo get_option('intro-text-color'.$i); ?>"></td>
</tr>
<?php } ?>


				<tr>
					<th colspan="2"><h3>Mạng xã hội và kết nối</h3><hr></th>
				</tr>
				<tr>
					<th>Facebook link</th>
					<td><input type="text" class="regular-text" name="facebook_link" id="facebook_link" value="<?php echo get_option('facebook_link'); ?>" placeholder="Facebook Link"></td>
				</tr>
				</tr>
				<tr>
					<th>Google Plus link</th>
					<td><input type="text" class="regular-text" name="google_plus_link" id="google_plus_link" value="<?php echo get_option('google_plus_link'); ?>" placeholder="Google Plus Link"></td>
				</tr>
				<tr>
					<th>Linkedin link</th>
					<td><input type="text" class="regular-text" name="linkedin_link" id="linkedin_link" value="<?php echo get_option('linkedin_link'); ?>" placeholder="Linkedin Link"></td>
				</tr>
				<tr>
					<th>Pinterest link</th>
					<td><input type="text" class="regular-text" name="pinterest_link" id="pinterest_link" value="<?php echo get_option('pinterest_link'); ?>" placeholder="Pinterest Link"></td>
				</tr>
				<tr>
					<th>Issuu link</th>
					<td><input type="text" class="regular-text" name="issuu_link" id="issuu_link" value="<?php echo get_option('issuu_link'); ?>" placeholder="Issuu Link"></td>
				</tr>
				<tr>
					<th>Hotline</th>
					<td><input type="text" class="regular-text" name="hotline" id="hotline" value="<?php echo get_option('hotline'); ?>" placeholder="Hotline"></td>
				</tr>

				<tr>
					<th colspan="2"><h3>Catalogue</h3><hr></th>
				</tr>
				<tr>
					<th>Catalogue</th>
					<td>
						<?php $catalogue = get_option('catalogue');
						$txt = 'Sửa catalogue';
						if(!$catalogue) {
							$txt = 'Tạo mới catalogue';
						}
						$catalogue_arr = explode(',', $catalogue); ?>

						<input type="hidden" name="catalogue" id="catalogue" value="<?php echo $catalogue; ?>">
						<input type="button" value="<?php echo $txt; ?>" class="button" id="ctlg_btn">
						<?php if(!empty($catalogue_arr) && $catalogue_arr[0] != ''): echo '<ul style="width:100%">'; foreach ($catalogue_arr as $ckey => $c) {
							$img = wp_get_attachment_image_src( $c, 'medium' );
							echo '<li style="width:200px;max-height:200px;margin:5px;display:inline-block"><img style="width:100%;height:auto" src="'. $img[0] .'" alt=""></li>';
						} endif; ?>

					</td>
				</tr>

				<tr>
					<th colspan="2"><h3>Phí dịch vụ gói quà/thiệp</h3><hr></th>
				</tr>
				<tr>
					<th>Phí gói quà (number)</th>
					<td><input type="text" class="regular-text" name="wrap_fee" id="wrap_fee" value="<?php echo get_option('wrap_fee'); ?>" placeholder="Phí gói quà"> (đồng)</td>
				</tr>
				<tr>
					<th>Phí thiệp (number)</th>
					<td><input type="text" class="regular-text" name="card_fee" id="card_fee" value="<?php echo get_option('card_fee'); ?>" placeholder="Phí thiệp"> (đồng)</td>
				</tr>

				<!-- <tr>
					<th colspan="2"><h3>Backlink</h3><hr></th>
				</tr>
				<tr>
					<th>Backlink <button type="button" class="button" id="add_backlink">+</button></th>
					<td><?php /*$backlinks = get_option( 'backlink', array() );
					echo '<ul id="backlinks" data-row="'. count($backlinks) .'">';
					if($backlinks){
						foreach ($backlinks as $key => $value) {
							echo '<li><input type="text" class="regular-text" name="backlink['. $key .']" id="backlink_'. $key .'" value="'. $value .'" placeholder="Link"><button type="button" class="button delete-backlink">x</button></li>';
						}
					}
					echo '</ul>';*/ ?>
					</td>
				</tr> -->

				<tr class="form-field">
                    <th>Home Popup</th>
                    <td>
                        <img id="banner_image_display" src="<?php echo timevn_attachment_src( get_option('home-popup') ); ?>" alt="" title="Change image" style="display:block; max-width:100%; max-height:483px; margin:0 auto; cursor:pointer;">
                        <input type="hidden" name="home-popup" id="banner_image_value" value="<?php echo get_option('home-popup'); ?>">
                        <input type="text" name="home-popup-link" value="<?php echo get_option('home-popup-link'); ?>" placehoder="Home Popup Link">
                    </td>
                </tr>
			</table>
			<?php submit_button(); ?>
			<link rel="stylesheet" media="screen" type="text/css" href="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/assets/admin/css/colorpicker.css" />
			<script type="text/javascript" src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/assets/admin/js/colorpicker.js"></script>
			<script type="text/javascript">
				jQuery(document).ready(function() {
					// Home Popup
					jQuery('#banner_image_display').click(function(event) {
	                    var thisImage = jQuery(this);

	                    var fileFrame = wp.media.frames.fileFrame = wp.media({
	                        title: 'Select image',
	                        library: {type: 'image'},
	                        button: {text: 'Select'},   
	                        multiple: false
	                    });

	                    var thisImageID = jQuery('#banner_image_value').val();

	                    if( thisImageID ) {
	                        fileFrame.on('open', function() {
	                            var selection = fileFrame.state().get('selection');
	                            var attachment = wp.media.attachment( thisImageID );
	                            attachment.fetch();
	                            selection.add( attachment ? [ attachment ] : [] );
	                        });
	                    }

	                    fileFrame.on( 'select', function() {
	                        attachment = fileFrame.state().get('selection').first().toJSON();

	                        jQuery(thisImage).attr('src', attachment.url);
	                        jQuery('#banner_image_value').val( attachment.id );
	                    });

	                    fileFrame.open();
	                });
					// Add backlink
					jQuery('#add_backlink').click(function(event) {
						event.preventDefault();
						var key = parseInt(jQuery('#backlinks').attr('data-row')) + 1;
						jQuery('#backlinks').attr('data-row', (key+1));
						jQuery('#backlinks').append('<li><input type="text" class="regular-text" name="backlink['+ key +']" id="backlink_'+ key +'" value="" placeholder="Link"><button type="button" class="button delete-backlink">x</button></li>');
					});
					// Remove backlink
					jQuery('#backlinks').delegate('.delete-backlink', 'click', function(event){
						event.preventDefault();
						jQuery(this).parents('li').remove();
						return false;
					});

					jQuery('#ctlg_btn').click(function(event) {
						event.preventDefault();

						var _t = jQuery(this);
						var _p = _t.parents('td');
						var html = '';

						var file_frame = wp.media.frames.file_frame = wp.media({
							title: 'Select images',
							library: {},
							button: {text: 'Select'},
							multiple: true
						});

						/*file_frame.on('open', function() {
							var selection = file_frame.state().get('selection');
							var already_val = jQuery('#catalogue').val();
							var attachment = wp.media.attachment(already_val.);
							attachment.fetch();
							
							selection.add( attachment ? [ attachment ] : [] );
						});*/

						file_frame.on('select', function() {
							var attachment = file_frame.state().get('selection').toJSON();
							var attachment_ids = [];
							html += '<ul>';

							jQuery.each(attachment, function(index, item){
								attachment_ids.push(item.id);
								html += '<li style="width:200px;height:200px;margin:5px;display:inline-block"><img style="width:100%;height:auto" src="'+ item.url +'" alt=""></li>';
							});

							html += '</ul>';
							jQuery('#catalogue').val(attachment_ids.join(','));
							_p.find('ul').remove();
							_p.append(html);
							_t.val('Sửa catalogue');
						});

						file_frame.open();

						return false;
					});

					jQuery('#intro-bg-color1, #intro-text-color1, #intro-bg-color2, #intro-text-color2, #intro-bg-color3, #intro-text-color3, #intro-bg-color4, #intro-text-color4, #intro-bg-color5, #intro-text-color5').ColorPicker({
						onSubmit: function(hsb, hex, rgb, el) {
							jQuery(el).val(hex);
							jQuery(el).ColorPickerHide();
						},
						onBeforeShow: function () {
							jQuery(this).ColorPickerSetColor(this.value);
						}
					})
					.bind('keyup', function(){
						jQuery(this).ColorPickerSetColor(this.value);
					});

					jQuery(".import-submit").on('click', function(event) {
						event.preventDefault();

						var data = new FormData();
						data.append("action", "import_sp");
				        jQuery.each(jQuery("#file-7"), function(i, tag) {
				            jQuery.each(jQuery(tag)[0].files, function(i, file) {
				                data.append('quote_send', file);
				            });
				        });

				        jQuery.ajax({
				            url: "<?php echo home_url(); ?>/wp-admin/admin-ajax.php",
				            type: 'POST',
				            data: data,
				            cache: false,
				            dataType: 'json',
				            processData: false, // Don't process the files
				            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
				            success: function(data, textStatus, jqXHR) {
				                console.log(data);
				                
				                // if(!data.status && data.error != ''){
				                //     $('html, body').animate({
				                //         scrollTop: $("#quote-top").offset().top
				                //     }, 500);
				                //     var error = '<ul class="error-box">'+data.error+'</ul>';
				                //     contactEl.find('.error-noti').html(error);
				                // }
				                // if(data.status && data.error == ''){
				                //     contactEl.find('.success-noti').show();
				                //     $('#quoteForm input[type=text], #quoteForm textarea, #quoteForm select').val('');
				                // }

				                // el.css('display', 'block');
				                // el.siblings('.button-wait').css('display', 'none');
				            }
				        });

				        return false;
				    });

				});
			</script>
			<style>
				.screenshot img{max-width: 100px; max-height: 100px;}
				#theme-options-form th, #theme-options-form td{padding: 0 0 5px}
				#theme-options-form hr{margin-top: 0}
				#theme-options-form h3{margin-top: 15px;}
			</style>
		</form>
	</div>
<?php }