<?php 
define('WP_USE_THEMES', false);
require_once($_SERVER['DOCUMENT_ROOT'] . '/wp-load.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/wp-content/themes/khatoco/includes/phpexcel/PHPExcel.php');

// Orders export fields
$export_orders = array(
	'order_code' => array(
		'required' => 1,
		'label' => 'Mã đơn hàng'
	),
	/*'order_info' => array(
		'required' => 0,
		'label' => 'Chi tiết đơn hàng'
	),*/
	'order_status' => array(
		'required' => 1,
		'label' => 'Trạng thái đơn hàng'
	),
	'order_price' => array(
		'required' => 1,
		'label' => 'Giá trị đơn hàng'
	),
	'order_price_all' => array(
		'required' => 1,
		'label' => 'Tổng giá trị đơn hàng'
	),
	/*'order_shipping_transport_method' => array(
		'required' => 0,
		'label' => 'Phương thức giao hàng'
	),*/
	'order_payment_method' => array(
		'required' => 0,
		'label' => 'Phương thức thanh toán'
	),
	'order_date' => array(
		'required' => 0,
		'label' => 'Ngày đặt hàng'
	),
	'order_customer_code' => array(
		'required' => 0,
		'label' => 'Mã Khách hàng'
	),
	'order_user_id' => array(
		'required' => 0,
		'label' => 'Khách hàng'
	),
	'order_customer_email' => array(
		'required' => 0,
		'label' => 'Email khách hàng'
	),
	'order_customer_phone' => array(
		'required' => 0,
		'label' => 'SĐT khách hàng'
	),
	'order_customer_address' => array(
		'required' => 0,
		'label' => 'Tỉnh/Thành Phố'
	),
	'order_campaign' => array(
		'required' => 0,
		'label' => 'Chương trình khuyến mại'
	),
	'order_coupon' => array(
		'required' => 0,
		'label' => 'Mã giảm giá'
	),

);

// Products export fields
$export_products = array(
	'post_orders_code' => array(
		'required' => 1,
		'label' => 'Mã đơn hàng'
	),
	'post_sku' => array(
		'required' => 1,
		'label' => 'SKU'
	),
	'post_title' => array(
		'required' => 0,
		'label' => 'Tên sản phẩm'
	),
	'post_color' => array(
		'required' => 0,
		'label' => 'Màu sản phẩm'
	),
	'post_size' => array(
		'required' => 0,
		'label' => 'Size'
	),
	'post_price' => array(
		'required' => 0,
		'label' => 'Đơn giá'
	),
	'be_bought' => array(
		'required' => 1,
		'label' => 'Số lượng đã bán (thực tế)'
	),
	'price_total' => array(
		'required' => 0,
		'label' => 'Tổng doanh thu thực tế'
	),
);

// Campaigns
$export_campaigns = array(
	'name' => array(
		'required' => 1,
		'label' => 'Tên chương trình'
	),
	'time' => array(
		'required' => 1,
		'label' => 'Thời gian thực hiện'
	),
	'status' => array(
		'required' => 1,
		'label' => 'Trạng thái'
	),
	'deals' => array(
		'required' => 1,
		'label' => 'Ưu đãi khác (thẻ thành viên, coupon hay hình thức thanh toán)'
	),
	/*'view_counter' => array(
		'required' => 0,
		'label' => 'Lượt truy cập'
	),*/
	'orders_created' => array(
		'required' => 0,
		'label' => 'Số đơn hàng phát sinh'
	),
	'orders_completed' => array(
		'required' => 0,
		'label' => 'Số đơn hàng thành công'
	),
	'users_register' => array(
		'required' => 0,
		'label' => 'Thành viên đăng ký mới'
	),
	'coupon_counter' => array(
		'required' => 0,
		'label' => 'Số coupon đã sử dụng'
	),
);

if(current_user_can('manage_options')) {
	$datas = array();
	$order_check;
	$filename = 'BaoCaoChiTiet';
	$time_exports = '';
	// $redirect = '';
	$redirect = $_POST['_wp_http_referer'];

	if(isset($_POST) && !empty($_POST)){
		// Export normal
		if($_POST['export_time'] != 'campaign') {
			$args = array(
				'post_type' => 'orders',
				'posts_per_page' => -1,
				'orderby' => 'date',
				'order' => 'DESC',
			);
			
			// Month of Quarter of Year

			if($_POST['export_time'] == 'date'){
				$time_exports = '-Ngay-'. $_POST['export_date_from'] .'-'. $_POST['export_date_to'];
				$args['date_query'] = array(array('after' => $_POST['export_date_from'], 'before' => $_POST['export_date_to']));
			}
			elseif($_POST['export_time'] == 'month'){
				// print_r($_POST['export_month_year']); exit;
				list($y, $m) = explode('-', $_POST['export_month_year']);
				$time_exports = '-'. $m .'-'. $y;
				$y = (int) $y;
				$m = (int) $m;
				$args['date_query'] = array(array('year' => $y, 'month' => $m));

			}
			else{
				list($y, $q) = explode('-', $_POST['export_quarter_month']);
				
				$time_exports = '-Quy'. $q .'-'. $y;
				$y = (int) $y;
				$q = (int) $q;

				$start_date = ''; $end_date = '';

				if($q == 1){
					$start_date = $y .'-01-01 00:00:00';
					$end_date = $y .'-03-31 23:59:59';
				}
				else if($q == 2){
					$start_date = $y .'-04-01 00:00:00';
					$end_date = $y .'-06-31 23:59:59';
				}
				else if($q == 3){
					$start_date = $y .'-07-01 00:00:00';
					$end_date = $y .'-09-31 23:59:59';
				}
				else if($q == 4){
					$start_date = $y .'-10-01 00:00:00';
					$end_date = $y .'-12-31 23:59:59';
				}

				$args['date_query'] = array(array('after' => $start_date, 'before' => $end_date));
			}

			if($_POST['export_what'] == 'orders' && isset($_POST['export_orders_status']) && !empty($_POST['export_orders_status'])){
				$args['meta_query'] = array(
					'relation' => 'OR'
				);
				foreach ($_POST['export_orders_status'] as $status) {
					$args['meta_query'][] = array(
						'key' => 'order_status',
						'value' => $status,
					);
				}
			}

			$orders = get_posts($args);

			// Data empty
			if(!$orders || empty($orders)) {
				wp_redirect(add_query_arg(array('response' => 'empty'), $redirect));
				exit();
			}

			// Orders or Products report
			if($_POST['export_what'] == 'orders'){
				// File name
				$filename .= '-DonHang-';

				foreach ($orders as $okey => $o) {
					$post_id = $o->ID;
					$order_info = get_post_meta($post_id, 'order_info', true);
					$order_status = get_post_meta($post_id, 'order_status', true);
					$order_code = get_post_meta($post_id, 'order_code', true);
					$order_date = get_the_time( 'd-m-Y', $o );

					// print_r($order_date);exit();
					 $payment_status = get_post_meta($post_id, 'order_payment_status', true);
					// $payment_code = get_post_meta($post_id, 'order_payment_code', true);
				// $payment_status_text = ($payment_status == true && $payment_code) ? ' <b>(Đã thanh toán)</b>' : ' <b>(Chưa thanh toán)</b>';
					$payment_status_text = ($payment_status == true) ? ' <b>(Đã thanh toán)</b>' : ' <b>(Chưa thanh toán)</b>';

					// $shipping_method = get_post_meta($post_id, 'order_shipping_method', true);
					// $order_transport_method = (int) get_post_meta($post_id, 'order_transport_method', true);
					$payment_method = get_post_meta($post_id, 'order_payment_method', true);

					$user_id = get_post_meta( $post_id, 'order_user_id', true);
					$code =  get_user_meta($user_id, 'code_crm', true);

					
					// foreach ($order_info as $key => $value) {
								// echo json_encode($order_info['customer']['email']);
							// }
					if(isset($order_info['customer'])){
						$mail = $order_info['customer']['email'];
						$phone =  $order_info['customer']['phone'];
						$address =  $order_info['customer']['address'];
					}
					$customer = array();
					if($user_id > 0 || $user_id != '0'){
						$user_data = get_userdata($user_id);



							
							
							$customer = array(
								
								'email' => $mail,
								'phone' => $phone,
								'address' => $address
								
							);

							// exit();
							


						

					}
					else {
						$customer = get_post_meta( $post_id, 'customer', true );
					
					}

					// exit();
					$receiver = get_post_meta( $post_id, 'receiver', true );

					// Orders exports info
					$temp = array();
					$temp['order_code'] = $order_code;
					$ori_total = 0;

					if(empty($order_info['products'])){
						$temp['order_info'] = array();
					}
					else {
						foreach ($order_info['products'] as $opkey => $op) {
							$ori_total += intval($op['price_saleoff']['value']) * intval($op['quantity']);

							$temp['order_info'][] = array(
								'sku' => $op['sku'],
								'color_name' => $op['color_name'],
								'size' => $op['size'],
								'quantity' => $op['quantity']
							);
						}
					}

					switch($order_status){
						case 'neworder': $temp['order_status'] = 'Mới đặt hàng'; break;
						case 'pending': $temp['order_status'] = 'Đang xử lý tại kho'; break;
						case 'shipping': $temp['order_status'] = 'Đang giao hàng'; break;
						case 'not_available': $temp['order_status'] = 'Hết hàng'; break;
						case 'not_receiving': $temp['order_status'] = 'Khách hàng không nhận'; break;
						case 'return': $temp['order_status'] = 'Hàng trả về kho'; break;
						case 'cancel': $temp['order_status'] = 'Hủy đơn hàng'; break;
						case 'done': $temp['order_status'] = 'Đã hoàn thành'; break;
					}

					$temp['order_price'] = $ori_total;
					$temp['order_price_all'] = isset($order_info['total']) ? $order_info['total']:0;

					/*if($shipping_method == 2)
						$temp['order_shipping_transport_method'] = 'Đến cửa hàng lấy ('.$order_info['selectedStore']['name'].')';
					else {
						$temp['order_shipping_transport_method'] = 'Giao hàng tận nơi';
						if($order_transport_method > 0){
							$transport_methods = get_transport_methods();
							$order_current_transport_method = $transport_methods[$order_transport_method];
							$temp['order_shipping_transport_method'] .= ' ('. $order_current_transport_method['method_name'] .')';
						}
					}*/

					if($payment_method == 2){
						$temp['order_payment_method'] = 'ATM ngân hàng nội địa';
					}elseif($payment_method == 3){
						$temp['order_payment_method'] = 'Visa/Master Card';
					}else{
						$temp['order_payment_method'] = 'Tiền mặt';
					}

					$temp['order_date'] = $order_date;
					$temp['order_user_id'] = ($user_id) ? $user_data->display_name : 'Khách hàng vãng lai';
					$temp['order_customer_email'] = $customer['email'];
					$temp['order_customer_phone'] = $customer['phone'];
					$temp['order_customer_address'] = $customer['address'];
					$temp['order_customer_code'] = $code;

					$order_campaign = get_post_meta( $post_id, 'campaignInfo', true );
					$order_coupon = get_post_meta( $post_id, 'couponName', true );

					$titleCamp = '';
					if (!empty($order_campaign['detail'])){
                        foreach ($order_campaign['detail'] as $detailCamp) {
                            $titleCamp.=$detailCamp['title'].' - ';
                        }
                    }
					/*$temp['order_campaign'] = ($order_campaign) ? $order_campaign['detail'][0]['title'] : '';*/
                    $temp['order_campaign'] = $titleCamp;
					$temp['order_coupon'] = ($order_coupon) ? $order_coupon : '';
                    $temp['order_price_all'] = get_post_meta( $post_id, 'order_total', true );


					array_push($datas, $temp);
				}
				// print_r($filename);exit();
			}
			elseif($_POST['export_what'] == 'products'){
				// File name
				$filename .= '-SanPham-';

				foreach ($orders as $okey => $o) {
					$post_id = $o->ID;
					$info = get_post_meta($post_id, 'order_info', true);
					$status = get_post_meta($post_id, 'order_status', true);
					$order_code = get_post_meta($post_id, 'order_code', true);
					$payment_status = get_post_meta($post_id, 'order_payment_status', true);
					// $payment_code = get_post_meta($post_id, 'order_payment_code', true);
					$user_id = get_post_meta($post_id, 'order_user_id', true);

					if(!$info || !isset($info['products'])) continue; // Skip if no products in orders

					foreach ($info['products'] as $ipkey => $ip) {
						$temp = array(
							'id' => $ip['id'],
							'order_code' => $order_code,
							'sku' => $ip['sku'],
							'price' => intval( $ip['price'] ),
                            'price_saleoff' => intval( $ip['price_saleoff'] ),
							/*'price_total' => intval( $ip['price_total'] ),*/
                            'price_total' => intval( $ip['price_saleoff']['value']) * intval($ip['quantity']),
							'name' => $ip['name'],
							'color_name' => $ip['color_name'],
							'quantity' => intval( $ip['quantity'] ),
							'size' => $ip['size']
						);

						if( !array_key_exists($ipkey, $datas) ){
							$datas[$ipkey] = $temp;
						}
						else{
							if($payment_status == false /*|| !$payment_code*/){
								$datas[$ipkey]['price_total'] += intval( $temp['price_saleoff']['value']) * intval($temp['quantity']);
								$datas[$ipkey]['quantity'] += $temp['quantity'];
							}
						}
					}

				}
			}
			else{
				// Failed
				wp_redirect($redirect);
				exit;
			}
		}
		elseif($_POST['export_time'] == 'campaign'){
			$time_exports = '-All_campaign';
			$campaigns = get_all_campaigns();

			foreach ($campaigns as $ckey => $campaign) {
				$temp = array();
				$temp['name'] = $campaign['name']; // Name
				$temp['time'] = 'Từ '. $campaign['date']['after'] .' đến '. $campaign['date']['before']; // Time range

				// Status
				$status = '';
				$pc_actived = check_campaign_is_running();
				$start_date = $campaign['date']['after'];
				$end_date = $campaign['date']['before'];
				if(!isset($campaign['is_running']) || $campaign['is_running'] == '' || $campaign['is_running'] == '0')
					$status = 'Đã tắt';
				else{
					if(!check_current_date_in_range($start_date, $end_date)){
						if(strtotime($pc_actived['date']['before']) - strtotime($start_date) >= 0)
							$status = 'Đã hoàn thành';
						elseif(strtotime($start_date) - strtotime($pc_actived['date']['before']) >= 0)
							$status = 'Sắp chạy trong khoảng '. round((strtotime($start_date) - time()) / (60*60*24), 1) . ' ngày';
					}
					else
						$status = 'Đang chạy';
				}
				$temp['status'] = $status;

				$temp['deals'] = $campaign['incentives']; // Deals

				// Orders created and completed
				$args = array(
					'post_type' => 'orders',
					'posts_per_page' => -1,
					'date_query' => array(
						array(
							'after' => $start_date,
							'before' => $end_date
						)
					)
				);
				$orders_list = get_posts( $args );
				$orders_created = 0;
				$orders_completed = 0;
				$coupon_counter = 0;
				foreach ($orders_list as $okey => $o) {
					$order_info = get_post_meta( $o->ID, 'order_info', true );
					if( !$order_info )
						continue;

					$campaign_influence = get_post_meta( $o->ID, 'campaign_influence', true );
					if($campaign_influence == '1' || $campaign_influence == 1){
						$orders_created++;

						$order_payment_status = get_post_meta( $o->ID, 'order_payment_status', true );
						// $order_payment_code = get_post_meta( $o->ID, 'order_payment_code', true );
						// if( $order_payment_status == true && $order_payment_code )
						if( $order_payment_status == true )
							$orders_completed++;
					}

					$hasCoupon = get_post_meta($post_id, 'hasCoupon', true);
					if($hasCoupon){
						$coupon_counter++;
					}
				}
				$temp['orders_created'] = $orders_created;
				$temp['orders_completed'] = $orders_completed;
				$temp['coupon_counter'] = $coupon_counter;

				// Get users registed in time range
				global $wpdb;
				$query = $wpdb->prepare("SELECT `khatoco_users`.`id` FROM `khatoco_users` WHERE CAST(`khatoco_users`.`user_registered` AS DATE) BETWEEN %s AND %s ORDER BY `khatoco_users`.`id` ASC", $start_date, $end_date);
				$users = $wpdb->get_results($query);
				$temp['users_register'] = count($users); // Number of users registered in campaign

				// Push data to datas
				$datas[$ckey] = $temp;
			}
		}
		else{
			wp_redirect($redirect);
			exit;
		}
	}

	$filename .= $time_exports . '.xls';

	// Exports
	if(!empty($datas)){
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setCreator("Thời trang Khatoco")->setLastModifiedBy("Thời trang Khatoco");

		$style = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			),
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => '666666')
			),
			'font'  => array(
			'color' => array('rgb' => 'FFFFFF'),
			'size'  => 13,
			)
		);

		if($_POST['export_time'] != 'campaign'){

			if($_POST['export_what'] == 'orders'){
				$row = 1;
				$col = ord('A');
				// print_r($export_orders);exit();
				foreach ($export_orders as $eokey => $eo) {
					$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $eo['label']);
					$objPHPExcel->getActiveSheet()->getColumnDimension(chr($col-1))->setAutoSize(true);
				}
				$objPHPExcel->getActiveSheet()->getStyle('A1:L1')->applyFromArray($style);

				foreach ($datas as $dkey => $data) {
					$row++;
					$col = ord('A');

					$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $data['order_code']);

					$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $data['order_status']);
					$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $data['order_price']);
					$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $data['order_price_all']);
					/*$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $data['order_shipping_transport_method']);*/
                    /*$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, 'Ship ');*/
                    
					$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $data['order_payment_method']);
					$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $data['order_date']);
					$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $data['order_customer_code']);
					$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $data['order_user_id']);
					$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $data['order_customer_email']);
					$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $data['order_customer_phone']);
					$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $data['order_customer_address']);
					$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $data['order_campaign']);
					$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $data['order_coupon']);
				}
			}
			elseif($_POST['export_what'] == 'products'){
				$row = 1;
				$col = ord('A');

				foreach ($export_products as $epkey => $ep) {
					$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $ep['label']);
					$objPHPExcel->getActiveSheet()->getColumnDimension(chr($col-1))->setAutoSize(true);
				}
				$objPHPExcel->getActiveSheet()->getStyle('A1:G1')->applyFromArray($style);

				foreach ($datas as $dkey => $data) {
					$row++;
					$col = ord('A');
					$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $data['order_code']);
					$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $data['sku']);
					$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $data['name']);
					$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $data['color_name']);
					$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $data['size']);
					$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $data['price']);
					$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $data['quantity']);
					$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $data['price_total']);
				}
			}
			else{
				// Failed
				wp_redirect($redirect);
				exit;
			}
		}
		else{
			$row = 1;
			$col = ord('A');

			foreach ($export_campaigns as $eckey => $ec) {
				if($eckey != 'deals'){
					$objPHPExcel->getActiveSheet()->mergeCells(chr($col) .'1:'. chr($col) .'2');
					$objPHPExcel->getActiveSheet()->setCellValue(chr($col) . $row, $ec['label']);
					$col++;
					$objPHPExcel->getActiveSheet()->getColumnDimension(chr($col-1))->setAutoSize(true);
				}
				else{
					$objPHPExcel->getActiveSheet()->mergeCells(chr($col) .'1:'. chr($col+5) .'1');
					$objPHPExcel->getActiveSheet()->setCellValue(chr($col) . $row, $ec['label']);
					$objPHPExcel->getActiveSheet()->getColumnDimension(chr($col))->setAutoSize(true);

					$sub_deals = array(
						'Thẻ Bạc',
						'Thẻ Vàng',
						'Thẻ Kim Cương',
						'Coupon',
						'Thanh toán online nội địa',
						'Thanh toán online quốc tế',
					);

					foreach ($sub_deals as $sub) {
						$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) .'2', $sub);
						$objPHPExcel->getActiveSheet()->getColumnDimension(chr($col-1))->setAutoSize(true);
					}
				}
			}
			$objPHPExcel->getActiveSheet()->getStyle('A1:M1')->applyFromArray($style);
			$objPHPExcel->getActiveSheet()->getStyle('A2:C2')->applyFromArray($style);
			$objPHPExcel->getActiveSheet()->getStyle('J2:M2')->applyFromArray($style);

			$row++;
			foreach ($datas as $dkey => $data) {
				$row++;
				$col = ord('A');
				$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $data['name']);
				$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $data['time']);
				$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $data['status']);

				foreach($data['deals']['vip_cards'] as $vip){
					$text = 'Không';
					if( isset($vip['id']) && $vip['id'] != '0' ) $text = $vip['value'] .'%';
					$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $text);
				}

				$coupons = '';
				if( isset($data['deals']['coupon']['id']) && ($data['deals']['coupon']['id'] == 1 || $data['deals']['coupon']['id'] == '1') ){
					foreach ($data['deals']['coupon']['list'] as $couponkey => $couponid) {
						$coupons .= get_post_meta( (int) $couponid, 'coupon_code', true );
						if($couponkey < count($data['deals']['coupon']['list']) - 1) $coupons .= ', ';
					}
				}
				$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $coupons);

				foreach($data['deals']['pay_online'] as $pay){
					if( isset($pay['id']) && $pay['id'] != '' ) $text = $pay['value'] .'%';
					else $text = 'Không';
					$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $text);
				}

				$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $data['orders_created']);
				$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $data['orders_completed']);
				$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $data['users_register']);
				$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $data['coupon_counter']);
			}
		}

		$objPHPExcel->setActiveSheetIndex(0);

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment; filename="'. $filename .'"');
		header('Cache-Control: max-age=0');
		header('Cache-Control: max-age=1');

		header ('Last-Modified: '. gmdate('D, d M Y H:i:s') .' GMT');
		header ('Cache-Control: cache, must-revalidate');
		header ('Pragma: public');

        PHPExcel_Calculation::getInstance()->writeDebugLog = true;
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
//        $objWriter = PHPExcel_Writer_Excel5($objPHPExcel);
	    try {
            ob_end_clean();
            $objWriter->save('php://output');
        }catch (Exception $e){
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
		exit;
	}

	wp_redirect($redirect); 
	exit;
}
else {
	wp_redirect(home_url('/'));
	exit;
}