<?php

$_social_settings = array();

// Facebook
$_social_settings['fb_pageId'] = '713519438667916';
$_social_settings['fb_appId'] = '906193312777258';
$_social_settings['fb_secret'] = 'f51f6cd03c178176729beeca538598cb';

function get_fb_initialize() {
	global $_social_settings;
	require 'facebook-sdk/facebook.php';

	$facebook = new Facebook(array('appId'  => $_social_settings['fb_appId'], 'secret' => $_social_settings['fb_secret']));

	return $facebook;
}

function get_fb_feeds() {
	global $_social_settings;
	$facebook = get_fb_initialize();
	$pageInfo = $facebook->api('/'. $_social_settings['fb_pageId'], 'GET');
	$pageFeed = $facebook->api('/'. $_social_settings['fb_pageId'] .'/posts', 'GET', array('like_limit' => 250, 'limit' => 20));
	$pageAccess = $facebook->getAccessToken();

	return array(
		'page_info' => $pageInfo,
		'access_token' => $pageAccess,
		'posts' => $pageFeed
	);
}

function get_fb_likes_count($pid) {
	$likesCount = file_get_contents('http://graph.facebook.com/'. $pid. '/likes?summary=true');
	$likesCount = json_decode($likesCount, true);
	return $likesCount; //['summary']['total_count']
}

function get_fb_comments_count($pid) {
	$commentsCount = file_get_contents('http://graph.facebook.com/'. $pid. '/comments?summary=true');
	$commentsCount = json_decode($commentsCount, true);
	return $commentsCount; //['summary']['total_count']
}


// Youtube
$_social_settings['yt_channel'] =  'UCipoobgi12E4u51tM2QWsVA'; // UCyC_4jvPzLiSkJkLIkA7B8g/UCipoobgi12E4u51tM2QWsVA

function get_yt_initialize() {
	global $_social_settings;
	require_once 'Google/Client.php';
	require_once 'Google/Service/YouTube.php'; //Google_Service_YouTube

	$client = new Google_Client();
	$client->setDeveloperKey('AIzaSyBUC-3dlGdPjXTocWI1A-cMpm9b4taYuPw');
	$plus = new Google_Service_YouTube($client);
	return $plus;
}

function get_yt_feeds() {
	global $_social_settings;
	$plus = get_yt_initialize();
	$optParams = array('channelId' => $_social_settings['yt_channel'], 'maxResults' => 50, 'fields' => 'items/contentDetails');
	$results = $plus->activities->listActivities('contentDetails', $optParams);
	return $results;
}

function get_video_details($videoId) {
	global $_social_settings;
	$plus = get_yt_initialize();
	$optParams = array('id' => $videoId, 'fields' => 'items/statistics');
	$results = $plus->videos->listVideos('statistics', $optParams);
	return $results;
}

// Google Plus
$_social_settings['gp_pageId'] = '101732736942318540751';

function get_gp_initialize() {
	global $_social_settings;
	require_once 'Google/Client.php';
	require_once 'Google/Service/Plus.php';

	$client = new Google_Client();
	$client->setApplicationName("Client_Library_Examples");
	// $client->setDeveloperKey('AIzaSyA56R0C2n_rs_oJajhK1s_iGffr3zPjjo8');
	$client->setDeveloperKey('AIzaSyD5zkYneNP5vHND2L8VGqLZhKEMoqtSUV8');
	// $client->setDeveloperKey('AIzaSyA3SEsR-oweYJwY0EZLUqP7tul91udFzSI');
	$plus = new Google_Service_Plus($client);
	return $plus;
}

function get_gp_feeds() {
	global $_social_settings;
	$plus = get_gp_initialize();
	$optParams = array('maxResults' => 5);
	$results = $plus->activities->listActivities($_social_settings['gp_pageId'], 'public', $optParams);
	return $results;
}

// Pinterest
function curl_get_content($url = 'https://api.pinterest.com/v3/pidgets/users/khatoco/pins/?access_token=1440306') {
	$pin = curl_init();
	curl_setopt($pin, CURLOPT_URL, $url);
	curl_setopt($pin, CURLOPT_RETURNTRANSFER, 1);
	// curl_setopt($pin,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
	$pin_lst = curl_exec($pin);
	curl_close($pin);
	return $pin_lst;
}


?>