<?php
require_once(get_template_directory() . '/includes/product/ProductUtils.php');
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class CheckOutUtil
{
    public function __construct()
    {

    }

    public static function getOrderInfo($cart)
    {
        // Trả về thông tin của cart được tính toàn trên hệ thống backend ( thông tin này được dùng để lưu vào CSDL.
        // Tránh trường hợp hacker tấn công frontend thay đổi thông tin, nên cần tính toán lại thông tin của cart ở backend.
        // Lưu ý : dùng hàm verifyCart trước khi vào hàm này , để tránh thông tin cart có dấu hiệu bị sai lệch

        $total_org = self::getTotalOrgCart($cart); // Tổng tiền mua các sản phẩm (chưa tính sản phẩm mua thêm - đây là tổng tiền với giá gốc)
        $total_saleOff = self::getTotalSaleOffCart($cart); // Tổng tiền mua các sản phẩm (với giá được giảm)
        $total_plus = self::getTotalPlusCart($cart); // Tổng tiền các sản phẩm mua thêm (trong trường hợp rơi vào chương trình khuyến mãi bán với giá ưu đãi)
        $total_bill = $total_saleOff + $total_plus; // Tổng thành tiền (chưa trừ giảm giá coupon, phí vận chuyển)
        $couponCode = empty($cart['coupon']['code']) ? null : $cart['coupon']['code'];
        $decrease_coupon = self::getDecreaseCoupon($couponCode, $total_bill); // Giảm giá của mã giảm giá
        $camp = CampUtils::getCampInformation($cart); // Giảm giá của chiến dịch khuyến mãi đang áp dụng
        $transport_fee = 0; // Phí vận chuyển - code hiện tại đang free phí vận chuyển
        $total_order = $total_bill - $decrease_coupon + $transport_fee; // Tổng tiền cuối cùng khách phải trả

        if (!empty($camp->detail)){
            for ($i =0; $i < sizeof($camp->detail); $i++):
                $type = $camp->detail[$i]['type'];
                if ($type !== 'product'){
                    $total_order -= intval($camp->detail[$i]['value']);
                }
            endfor;
        }

        return array(
            'totalOrg' => $total_org,
            'totalPlus' => $total_plus,
            'totalBill' => $total_bill,
            'decreaseCoupon' => $decrease_coupon,
            'decreaseCampaign' => $camp->decrease,
            'transportFee' => $transport_fee,
            'totalOrder' => $total_order
        );

    }

    private static function getDecreaseCoupon($couponCode, $totalBill)
    {
        $total = 0;
        if (!empty($couponCode)) {
            $args = array('posts_per_page' => -1, 'post_type' => 'coupon', 'meta_key' => 'coupon_code', 'meta_value' => $couponCode);
            $res = get_posts($args);
            if (!count($res))
                return 0;
            else {
                $coupon = $coupon_kind = get_post_meta($res[0]->ID, 'coupon_kind', true);
            }

            $couponType = intval($coupon[0]);
            $couponValue = intval($coupon[1]);
            $couponLimit = get_post_meta($res[0]->ID, 'limit_decrease', true);
            $couponLimit = empty($couponLimit) ? 0 : intval($couponLimit);

            if ($couponType == 0) { // Giảm theo %
                if ((($couponValue / 100) * $totalBill) > $couponLimit && $couponLimit > 0)
                    return $couponLimit;
                else
                    return ($couponValue / 100) * $totalBill;
            } else {
                return $couponValue;
            }

        }
        return $total;
    }

    private static function getTotalOrgCart($cart)
    { // Tổng tiền mua các sản phẩm (chưa tính sản phẩm mua thêm khi rơi vào chương trình khuyến mãi bán với giá ưu đãi)
        $total = 0;
        foreach ($cart['products'] as $product):
            $quantity = intval($product['quantity']);
            $id = intval($product['id']);
            $price = get_post_meta($id, 'post_price', true);
            $price = intval($price);
            $total += $price * $quantity;
        endforeach;
        return $total;
    }

    private static function getTotalSaleOffCart($cart)
    { // Tổng tiền mua các sản phẩm (Nếu có giảm theo sale off)
        $total = 0;
        foreach ($cart['products'] as $product):
            $quantity = intval($product['quantity']);
            $id = intval($product['id']);
            $saleOff = ProductUtils::getProductSaleOffPrice($id)['value'];
            $total += $saleOff * $quantity;
        endforeach;
        return $total;
    }

    private static function getTotalPlusCart($cart)
    {
        // Tổng tiền các sản phẩm mua thêm (trong trường hợp rơi vào chương trình khuyến mãi bán với giá ưu đãi)
        $total = 0;
        if (!empty($cart['campaignInfo']['listSpecialPrice']) && !empty($cart['campaignInfo']['listSpecialPrice']['listProdBonus'])) {
            $campaign = CampUtils::getCampInformation($cart);
            $buySpecials = $cart['campaignInfo']['listSpecialPrice']['listProdBonus'];

            if (!empty($campaign->listSpecialPrice->listProdBonus) && sizeof($campaign->listSpecialPrice->listProdBonus) == sizeof($buySpecials)) {
                foreach ($buySpecials as $p):
                    $total += intval($p['price']) * intval($p['quantity']);
                endforeach;
            } else return 0;
        }
        return $total;
    }

    private static function verifyCart($cart, $cartFull)
    {
        /*Kiểm tra số lượng  && size còn trong kho*/
        $errorSizeOrQuantity = self::verifySizeAndQuantity($cart);
        if (!empty($errorSizeOrQuantity))
            return $errorSizeOrQuantity;

        /*Kiểm tra mã coupon có hợp lệ hay không*/
        $errorCoupon = self::verifyCoupon($cart);
        if (!empty($errorCoupon))
            return $errorCoupon;

        /*Kiểm tra thông tin người dùng nhập đầy đủ hay không ?*/
        $errorVerifyCustomer = self::verifyCustomerInfo($cartFull);
        if (!empty($errorVerifyCustomer))
            return $errorVerifyCustomer;

        /*Kiểm tra sản phẩm mua thêm (nếu rơi vào chương trình khuyến mãi mua thêm với giá ưu đãi) có đúng với chương trình km đưa ra (đúng giá, sản phẩm, số lượng) ?*/
        $errorPlusBuy = self::verifyPlusBuy($cart);
        if (!empty($errorPlusBuy))
            return $errorPlusBuy;

        /*Kiểm tra tặng phẩm người dùng chọn (nếu rơi vào chương trình khuyến mãi được tặng sản phẩm) có đúng với chương trình km đưa ra (đúng sản phẩm, số lượng) ?*/
        $errorBonusGet = self::verifyBonusGet($cart);
        if (!empty($errorBonusGet))
            return $errorBonusGet;

        return '';
    }

    private static function verifyPlusBuy($cart)
    {
        if (!empty($cart['campaignInfo']['listSpecialPrice']) && !empty($cart['campaignInfo']['listSpecialPrice']['listProdBonus'])) { // Kiểm tra sp mua giá đặc biệt
            $plusBuys = $cart['campaignInfo']['listSpecialPrice']['listProdBonus'];
            $campaign = CampUtils::getCampInformation($cart);
            $plusBuysBackEnd = (empty($campaign->listSpecialPrice->listProdBonus)) ? null : $campaign->listSpecialPrice->listProdBonus;

            if (empty($plusBuysBackEnd) || (sizeof($plusBuysBackEnd) != sizeof($plusBuys)))
                return 'Bạn không được mua sản phẩm với giá ưu đãi vì không rơi vào chương trình khuyến mãi của chúng tôi.';


            /*lấy danh sách các sản phẩm bán với giá ưu đãi của backend (ID && Price)*/
            $plusProducts = [];
            foreach ($plusBuysBackEnd as $plus):
                $p = ['ID' => $plus->ID, 'price' => $plus->price];
                $plusProducts[] = $p;
            endforeach;

            $totalPlusBuy = 0;
            foreach ($plusBuys as $product):
                $p = ['ID' => $product['ID'], 'price' => $product['price']]; // Lưu ý, chỗ này product là array không phải object
                if (!in_array($p, $plusProducts)) {
                    return 'Bạn không được mua sản phẩm với giá đặc biệt khi sản phẩm hoặc giá của sản phẩm không nằm trong chương trình khuyến mãi mua với giá ưu đãi';
                }
                $totalPlusBuy += intval($product['quantity']);
            endforeach;

            if ($totalPlusBuy > $campaign->listSpecialPrice->total)
                return 'Tổng số sản phẩm bạn chọn mua với giá ưu đãi vượt quá số lượng cho phép.';
        }

        return '';
    }

    private static function verifyBonusGet($cart)
    { // Kiểm tra người dùng có hack tặng vật phẩm khác so với campaign đưa ra hay không
        if (!empty($cart['campaignInfo']['listBonus']) && !empty($cart['campaignInfo']['listBonus']['listProdBonus'])) { // Kiểm tra sp mua giá đặc biệt
            $bonusGet = $cart['campaignInfo']['listBonus']['listProdBonus'];
            $campaign = CampUtils::getCampInformation($cart);
            $bonusBackEnd = (empty($campaign->listBonus->listProdBonus)) ? null : $campaign->listBonus->listProdBonus;

            if (empty($bonusBackEnd) || (sizeof($bonusGet) != sizeof($bonusBackEnd)))
                return 'Bạn không được tặng sản phẩm do không rơi vào chương trình khuyến mãi.';

            /*lấy danh sách các sản phẩm bán với giá ưu đãi của backend (ID)*/
            $bonusProducts = []; // List ID của sản phẩm được bonus
            foreach ($bonusBackEnd as $p):
                $bonusProducts[] = intval($p->ID);
            endforeach;

            $totalBonusGet = 0;
            foreach ($bonusGet as $product):
                if (!in_array(intval($product['ID']), $bonusProducts)) {
                    return 'Bạn đã chọn tặng phẩm không có trong chương trình khuyến mãi của chúng tôi.';
                }
                $totalBonusGet += intval($product['quantity']);
            endforeach;

            if ($totalBonusGet > $campaign->listBonus->total)
                return 'Tổng số tặng phẩm bạn chọn vượt quá số lượng cho phép.';

            if ($totalBonusGet < $campaign->listBonus->total)
                return 'Bạn chưa chọn số lượng tặng phẩm.';
        }

        return '';
    }

    private static function verifySizeAndQuantity($cart)
    {

        // print_r($cart);
        $verifySizeAndQuantity = CartSession::updateCartSession($cart); // Kiểm tra size và số lượng sản phẩm trong kho còn đủ hay không ?
        // print_r( $verifySizeAndQuantity);
        if (!empty($verifySizeAndQuantity['msg']['error'])) {
            return 'Bạn không thể đặt hàng với số lượng vượt quá số lượng trong kho.';
        }

        /*kiểm tra từng sản phẩm tặng kèm + sản phẩm khuyến mãi mua theo giá đặc biệt còn đủ số lượng trong kho hay không ?*/
        if (!empty($cart['campaignInfo']['listBonus']) && !empty($cart['campaignInfo']['listBonus']['listProdBonus'])) { // Kiểm tra sp tặng
            foreach ($cart['campaignInfo']['listBonus']['listProdBonus'] as $product):
                $quantity = empty($product['quantity']) ? 0 : intval($product['quantity']);
                $size = empty($product['size']) ? "S" : $product['size'];
                $id = $product['ID'];

                $sizes = get_post_meta($id, 'post_sizes', true);
                $onhands = get_post_meta($id, 'post_onhand', true);

                if (!empty($onhands[$size]) && $quantity > $onhands[$size]) { // sản phẩm chọn trong danh sách tặng vượt quá số sản phẩm trong kho
                    return 'Bạn không thể chọn tặng phẩm với số lượng vượt quá số lượng sản phẩm hiện có trong kho.';
                }

                if (!in_array($size, $sizes)) { // Size không tồn tại trong kho
                    return 'Bạn không thể chọn size trong tặng phẩm không tồn tại trong kho.';
                }
            endforeach;
        }

        if (!empty($cart['campaignInfo']['listSpecialPrice']) && !empty($cart['campaignInfo']['listSpecialPrice']['listProdBonus'])) { // Kiểm tra sp mua giá đặc biệt
            foreach ($cart['campaignInfo']['listSpecialPrice']['listProdBonus'] as $product):
                $quantity = empty($product['quantity']) ? 0 : intval($product['quantity']);
                $size = empty($product['size']) ? "S" : $product['size'];
                $id = $product['ID'];

                $sizes = get_post_meta($id, 'post_sizes', true);
                $onhands = get_post_meta($id, 'post_onhand', true);

                if (!empty($onhands[$size]) && $quantity > $onhands[$size]) { // sản phẩm chọn trong danh sách tặng vượt quá số sản phẩm trong kho
                    return 'Bạn không thể chọn sản phẩm mua với giá ưu đãi với số lượng vượt quá số lượng sản phẩm hiện có trong kho.';
                }

                if (!in_array($size, $sizes)) { // Size không tồn tại trong kho
                    return 'Bạn không thể chọn sản phẩm mua với giá ưu đãi với size không tồn tại trong kho.';
                }
            endforeach;
        }

        return '';
    }

    private static function verifyCoupon($cart)
    {
        if (!empty($cart['coupon']['code'])) {

            $couponCode = sanitize_text_field($cart['coupon']['code']);
            $uid = get_current_user_id() == 0 ? 443 : get_current_user_id();
            $couponVerify = check_invalid_coupon($couponCode, $uid);

            if ($couponVerify['result']==0)
                return $couponVerify['message'];
            // Everything OK
        }
        return '';
    }

    private static function verifyCustomerInfo($cartFull)
    {
        if (
            empty($cartFull['customer']) || empty($cartFull['customer']['fullname']) || empty($cartFull['customer']['email']) || empty($cartFull['customer']['phone']) ||
            empty($cartFull['customer']['province']) || empty($cartFull['customer']['province']['name']) || empty($cartFull['customer']['address'])
        ) {

            return 'Bạn nhập thiếu thông tin khách hàng.';
        }

        if (
            (!empty($cartFull['otherReceiver']) && $cartFull['otherReceiver']) &&
            (
                empty($cartFull['receiver']) || empty($cartFull['receiver']['fullname']) || empty($cartFull['receiver']['email']) || empty($cartFull['receiver']['phone']) ||
                empty($cartFull['receiver']['province']) || empty($cartFull['receiver']['province']['name']) || empty($cartFull['receiver']['address'])
            )
        )
            return 'Bạn nhập thiếu thông tin người nhận.';

        return '';
    }

    public static function checkOut($cartInfo)
        // $firstCheckOut : lần checkOut lần đầu (sau khi click nút thanh toán, user sẽ được system tính toán lại thông tin, hiển thị lần cuối trước khi lưu vào CSDL
        // Return : [ result : 0 là lỗi , result : 1 : là thông tin cho lần đầu checkOut (user sẽ được xem lại thông tin tính toán bởi hệ thống ]
        // Return : [ result : 2 : Hoàn thành thủ tục checkOut ]
        // Return : [ result : 3 : Có lỗi không mong muốn trong quá trình xử lý đơn hàng ]
    {
        $user_id = get_current_user_id();
        $cart = $cartInfo['cart'];
        // print_r($cart);
        if ($user_id <= 0) {
            $user_id = 443; // User dành cho khách hàng không/chưa đăng nhập
        }

        // Login to pay
        if ($user_id > 0) {
            $user_info = wp_get_current_user();
            $errorCart = self::verifyCart($cart, $cartInfo);
            if ($errorCart) {
                return array('result' => 0, 'error' => $errorCart);
            }

            $orderInfo = self::getOrderInfo($cart);
            if (!$cartInfo['firstCheckOut']) { // Chưa checkout lần đầu
                $cartInfo['orderInfo'] = $orderInfo;
                return array('result' => 1, 'data' => $cartInfo);
            }

            /*Lưu các thông tin vào database*/
            $inserted = self::insertNewOrder($cartInfo);
            if ($inserted['result']){ // result true - đã insert thành công giỏ hàng
                CartSession::detroyCartSession();

                $message = "Cảm ơn bạn đã mua hàng, đơn hàng đã được tiếp nhận và chúng tôi sẽ gọi lại cho bạn để xác nhận đơn hàng.";
                $nextUrl = get_permalink(get_page_by_path('thanh-toan-thanh-cong')->ID);
                // print_r($nextUrl);
                $nextUrl .= '?paid=0&statusPayment=0&_nonce=wp34oeisdo349qjsaervxero30&order_code='.$inserted['order']['code'].'&orderID='.$inserted['order']['orderID'];

                if ($cartInfo['paymentMethod'] == 2 || $cartInfo['paymentMethod'] == 3) {
                    $message = "Đơn hàng đã được tiếp nhận, bạn đang được chuyển hướng sang Onepay để tiến hành thanh toán.";
                    $nextUrl = redirect_onepay_url(array(
                        'user_id' => $inserted['order']['userID'],
                        'user_email' => $inserted['order']['email'],
                        'amount' => $inserted['order']['orderTotal'],
                        'order_code' => $inserted['order']['code'],
                        'paymentMethod' => $inserted['order']['paymentMethod']
                    ));
                } else {
                    if ($inserted['order']['email'])
                        mail__announce_new_orders($inserted['order']['email'], get_post($inserted['order']['orderID']));
                }

                return array('result' => 2, 'nextUrl' => $nextUrl, 'message' =>$message );
            } else {
                return array('result' => 3, 'nextUrl' => get_home_url(), 'message' => 'Đã có lỗi trong quá trình xử lý đơn hàng, bạn hãy quay ra đặt lại. Rất chân thành xin lỗi quý khách vì sự cố không mong muốn.' );
            }

        }
    }

    private static function insertNewOrder($cart)
    { // Insert cart vào database

        $order_code = 'WEB'.aj_random_string(8);
        $date_create = current_time('mysql', 0);
        $post_status = 'draft'; // Luôn insert dạng draft , khi nào thanh toán xong thì chuyển sang publish
        $user_id = get_current_user_id();
        if ($user_id <= 0) {
            $user_id = 443; // User dành cho khách hàng không/chưa đăng nhập
        }

        $new_order_info = array(
            'post_title' => $date_create . ' - ' . $order_code,
            'post_type' => 'orders',
            'post_status' => $post_status,
            'post_date' => $date_create,
            'post_author' => $user_id,
        );

        if ($cart['paymentMethod']==1){ // Thanh toán COD - Khi nhận được hàng, sẽ đẩy vào backend đơn hàng publish, không phải dạng draft như thanh toán online
            $new_order_info['post_status'] = 'publish';
        }

        $new_order_id = wp_insert_post($new_order_info);

        if (!is_wp_error($new_order_id)) {
            $campaign = CampUtils::getCampInformation($cart['cart']);
            $campaign_influence = false; // Sự ảnh hưởng của chiến dịch khuyến mãi
            if ( $campaign->decrease > 0 || !empty($campaign->listBonus->listProdBonus) || !empty($campaign->listSpecialPrice->listProdBonus) )
                $campaign_influence = true;

            update_post_meta($new_order_id, 'campaign_influence', $campaign_influence);

            if (isset($cartInfo['transportFeeTotal']))
                update_post_meta($new_order_id, 'transport_fee_total', $cart['transportFeeTotal']);

            $cart['products'] = $cart['cart']['products']; // Gán biến products ra ngoài cho giống code cũ
            $cart['orderInfo'] = self::getOrderInfo($cart['cart']);

            $cart['fullname'] = $cart['customer']['fullname']; // Bà nội đứa nào viết code cũ, bây giờ phải gán biến vậy nè mới show chính xác bên kia, viết tè le hầm bà lằng ra
            $cart['email'] = $cart['customer']['email'];
            $cart['phone'] = $cart['customer']['phone'];
            $cart['address'] = $cart['customer']['address'] .' - '. $cart['customer']['province']['name'];
            if (!empty($cart['receiver']))
                $cart['receiver']['address'] = $cart['receiver']['address'] .' - '. $cart['receiver']['province']['name'];

            update_post_meta($new_order_id, 'order_info', $cart);
            update_post_meta($new_order_id, 'order_code', $order_code);
            update_post_meta($new_order_id, 'order_payment_method', $cart['paymentMethod']);
            update_post_meta($new_order_id, 'order_status', 'neworder'); // Đơn hàng mới
            update_post_meta($new_order_id, 'order_payment_status', false); // Đơn hàng chưa được thanh toán
            update_post_meta($new_order_id, 'order_user_id', $user_id); // Người đặt hàng
            update_post_meta($new_order_id, 'receiver', $cart['receiver']); // Người nhận hàng
            update_post_meta($new_order_id, 'customer', $cart['customer']); // Lưu customer


            update_post_meta($new_order_id, 'order_total', $cart['orderInfo']['totalOrder']);
            update_post_meta($new_order_id, 'campaignInfo', $cart['cart']['campaignInfo']);
            update_post_meta($new_order_id, 'hasCoupon', !empty($cart['cart']['coupon']['code']));
            $cart['cart']['coupon']['code'] = empty($cart['cart']['coupon']['code']) ? null : $cart['cart']['coupon']['code'];
            update_post_meta($new_order_id, 'couponName',  $cart['cart']['coupon']['code']);
            update_post_meta($new_order_id, 'couponSaleoffValue', $cart['orderInfo']['decreaseCoupon']);
            // print_r($cart['cart']['coupon']['isVNECODE']);
            if(!empty($cart['cart']['coupon']['isVNECODE'])){

                update_post_meta($new_order_id, 'couponIsVneCode', $cart['cart']['coupon']['isVNECODE']);
            }
            if(!empty($cart['cart']['coupon']['updated'])){
                update_post_meta($new_order_id, 'updated', $cart['cart']['coupon']['updated']);
            }
         
            update_post_meta($new_order_id, 'note', sanitize_text_field($cart['checkoutNote']));

            $orderResult = [];
            $orderResult['code'] = $order_code;
            $orderResult['email'] = $cart['email'];
            $orderResult['orderTotal'] = $cart['orderInfo']['totalOrder'];
            $orderResult['paymentMethod'] = $cart['paymentMethod'];
            $orderResult['userID'] = $user_id;
            $orderResult['orderID'] = $new_order_id;


            if (!empty($cart['cart']['coupon']['code'])) {
                $couponCode = $cart['cart']['coupon']['code'];
                $couponID = check_coupon_available($couponCode);//Lấy ID bài post coupon

                // Update coupon mà user này đã dùng
                $coupon_used = get_user_meta($user_id, 'coupon_used', true); // List coupon mà user đã dùng lưu ở dạng array format như sau
                /*$coupon_used = [
                    'id bài post của coupon' => [
                            'name' => 'Mã coupon',
                            'date' => 'Ngày sử dụng'
                        ]
                */
                if (empty($coupon_used)) $coupon_used = array();
                if (empty($coupon_used[$couponID]))
                    $coupon_used[$couponID] = array();
                $coupon_used[$couponID][] = array(
                    'name' => $couponCode,
                    'date' => current_time('mysql', 0),
                );
                update_user_meta($user_id, 'coupon_used', $coupon_used);


                // Update số lần coupon được sử dụng
                $quota = intval( get_post_meta( $couponID, 'coupon_quota', true ) );
                $time_used = intval( get_post_meta( $couponID, 'coupon_used_time', true ) );
                if( $time_used + 1 > $quota )
                    $time_used = $quota;
                else
                    $time_used += 1;

                update_post_meta( $couponID, 'coupon_used_time', $time_used);
            }


            return ['result' => true, 'order' => $orderResult];

//            $coupon_used = get_user_meta($user_id, 'coupon_used', true);
//            if ($coupon_used == '') $coupon_used = array();
//            if (!isset($coupon_used[$coupon_id]))
//                $coupon_used[$coupon_id] = array();
//            $coupon_used[$coupon_id][] = array(
//                'name' => $coupon,
//                'date' => current_time('mysql', 0),
//            );
//            update_user_meta($user_id, 'coupon_used', $coupon_used);
        }

        return false;

    }

    private function oldCode()
    {
        /*
            $nonce = $dataArray['_checkout_nonce'];

    //        if ( ! wp_verify_nonce( $nonce, '_checkout_cart_ktc_nonce' ) ) {
    //            echo json_encode(array(
    //                'success' => 0,
    //                'message' => 'Phiên làm việc quá hạn. Vui lòng F5 để thử lại!',
    //            ));
    //        }

            $user_id = get_current_user_id();
            // Checking logined users
            if ($user_id <= 0) {
                $user_id = 443; // User dành cho khách hàng không/chưa đăng nhập
            }


            $wrap_fee = (int)get_option('wrap_fee');
            $card_fee = (int)get_option('card_fee');
    //        $payment_incentive = check_incentives_if_pay_online();

            // Login to pay
            if ($user_id > 0) {
                $user_info = wp_get_current_user();
                $cartInfo = get_cart_info_from_session();

                if ($cartInfo['total'] > 0) {
                    $cartInfo['giftTotal'] = 0;
                    /*if(isset($dataArray['giftProducts']) && !empty($dataArray['giftProducts'])){
                        $cartInfo['giftProducts'] = $dataArray['giftProducts'];
                        foreach($dataArray['giftProducts'] as $gProduct){
                            if($gProduct['giftbox'] == 1){
                                $cartInfo['total'] += $wrap_fee;
                                $cartInfo['giftTotal'] += $wrap_fee;
                            }
                            if($gProduct['giftcard'] == 1){
                                $cartInfo['total'] += $card_fee;
                                $cartInfo['giftTotal'] += $card_fee;
                            }
                        }
                    }*/

        /*if(isset($dataArray['selectedStore']) && $dataArray['selectedStore']){
            $cartInfo['selectedStore'] = $dataArray['selectedStore'];
        }
        if($dataArray['shippingMethod'] == 1){
           $cartInfo['transportMethod'] = $dataArray['transportMethod'] == 2 ? $dataArray['transportMethod'] : 1;
           $cartInfo['transportTotal'] = $cartInfo['transport_fee'][$cartInfo['transportMethod']];
           $cartInfo['total'] += $cartInfo['transportTotal'];
        }*/
        // $cartInfo['giftProducts'] = $dataArray['giftProducts'];
        /*}*/


        /* $cartInfo['total_formated'] = aj_format_number($cartInfo['total']) . ' VNĐ';
         $cartInfo['paymentMethod'] = $dataArray['paymentMethod'];
         $cartInfo['checkoutNote'] = sanitize_text_field($dataArray['checkoutNote']);

         $cartInfo['fullname'] = sanitize_text_field($dataArray['customer']['fullname']);
         $cartInfo['email'] = sanitize_email($dataArray['customer']['email']);
         $cartInfo['phone'] = sanitize_text_field($dataArray['customer']['phone']);
         if (isset($dataArray['customer'])) {
             $addr = $dataArray['customer'];
             $address = '';
             if (isset($addr['address'])) $address .= $addr['address'] . ', ';
             if (isset($addr['ward'])) $address .= $addr['ward']['name'] . ', ';
             if (isset($addr['district'])) $address .= $addr['district']['name'] . ', ';
             if (isset($addr['province'])) $address .= $addr['province']['name'];
         }
         $cartInfo['address'] = sanitize_text_field($address);
         // $cartInfo['shippingMethod'] = $dataArray['shippingMethod'];
         // $cartInfo['transportMethod'] = isset($dataArray['transportMethod']) ? $dataArray['transportMethod'] : '';

         $order_code = aj_random_string(8);
         $date_create = current_time('mysql', 0);
         $post_status = 'publish';
         $incentive = 1;*/
//            if($cartInfo['paymentMethod'] == 2 || $cartInfo['paymentMethod'] == 3){
//                $post_status = 'draft';
//
//                if( $payment_incentive ){
//                    if( $cartInfo['paymentMethod'] == 2 ){
//                        $incentive = 1 - $payment_incentive['domestic']['value']/100;
//                    }
//                    elseif( $cartInfo['paymentMethod'] == 3 ){
//                        $incentive = 1 - $payment_incentive['international']['value']/100;
//                    }
//                }
//            }
        /*
                    $new_order_info = array(
                        'post_title' => $date_create . ' - ' . $order_code,
                        'post_type' => 'orders',
                        'post_status' => $post_status,
                        'post_date' => $date_create,
                        'post_author' => $user_id,
                    );

                    $new_order_id = wp_insert_post($new_order_info);

                    if (!is_wp_error($new_order_id)) {
                        update_post_meta($new_order_id, 'order_info', $cartInfo);

                        $total_calc = 0;

                        // Re-calculated total price
                        foreach ($cartInfo['products'] as $pkey => $details) {
                            $id = intval($details['id']);
                            $quantity = intval($details['quantity']);
                            $price = intval($details['price_saleoff']);

                            $total_calc += $price * $quantity;
                        }

                        update_post_meta($new_order_id, 'order_code', $order_code);
                        update_post_meta($new_order_id, 'order_payment_method', $cartInfo['paymentMethod']);
                        update_post_meta($new_order_id, 'order_status', 'neworder');
                        if (isset($cartInfo['transportFeeTotal']))
                            update_post_meta($new_order_id, 'transport_fee_total', $cartInfo['transportFeeTotal']);

                        // update_post_meta($new_order_id, 'order_shipping_method', $cartInfo['shippingMethod']);
                        // update_post_meta($new_order_id, 'order_shipping_method', '2'); // mac dinh la chuyen phat nhanh

                        $dataArray['campaign_influence'] = '1'; // Nho fix
                        if ($dataArray['campaign_influence'])
                            $dataArray['campaign_influence'] = '1';
                        else
                            $dataArray['campaign_influence'] = '0';

                        // Calculate order price total
                        // Vip
                        if ($dataArray['isVip']) {
                            $vipAfterMoney = $total_calc * (1 - floatval($dataArray['vipAfter']));
                            $total_calc -= $vipAfterMoney;
                        }

                        $total_calc *= $incentive;
                        // Coupon
                        if ($dataArray['hasCoupon']) {
                            $coupon = sanitize_text_field($dataArray['couponName']);
                            $coupon_id = check_coupon_available($coupon);

                            if ($coupon_id) {
                                $total_calc -= intval($dataArray['couponSaleoffValue']);

                                $quota = intval(get_post_meta($coupon_id, 'coupon_quota', true));
                                $time_used = intval(get_post_meta($coupon_id, 'coupon_used_time', true));
                                if ($time_used + 1 > $quota)
                                    $time_used = $quota;
                                else
                                    $time_used += 1;

                                update_post_meta($coupon_id, 'coupon_used_time', $time_used);
                                $coupon_used = get_user_meta($user_id, 'coupon_used', true);
                                if ($coupon_used == '') $coupon_used = array();
                                if (!isset($coupon_used[$coupon_id]))
                                    $coupon_used[$coupon_id] = array();
                                $coupon_used[$coupon_id][] = array(
                                    'name' => $coupon,
                                    'date' => current_time('mysql', 0),
                                );
                                update_user_meta($user_id, 'coupon_used', $coupon_used);
                            }
                        }
                        $total_calc = CheckOutUtil::getCheckOutInfo($dataArray)->total_order;
                        update_post_meta($new_order_id, 'order_total', $total_calc);

                        update_post_meta($new_order_id, 'campaign_influence', $dataArray['campaign_influence']);
                        update_post_meta($new_order_id, 'campaignInfo', $dataArray['campaignInfo']);
        //                update_post_meta($new_order_id, 'payment_incentive', $payment_incentive);
                        update_post_meta($new_order_id, 'hasCoupon', $dataArray['hasCoupon']);
                        update_post_meta($new_order_id, 'couponName', $dataArray['couponName']);
                        update_post_meta($new_order_id, 'couponSaleoffValue', $dataArray['couponSaleoffValue']);
                        update_post_meta($new_order_id, 'isVip', $dataArray['isVip']);
                        update_post_meta($new_order_id, 'vipAfter', $dataArray['vipAfter']);

                        update_post_meta($new_order_id, 'note', sanitize_text_field($dataArray['checkoutNote']));

                        update_post_meta($new_order_id, 'order_user_id', $user_id);

                        $customer = array(
                            'fullname' => $cartInfo['fullname'],
                            'email' => $cartInfo['email'],
                            'phone' => $cartInfo['phone'],
                            'address' => $cartInfo['address'],
                        );
                        update_post_meta($new_order_id, 'customer', $customer);

                        if (isset($dataArray['receiver'])) {
                            $receiverObj = $dataArray['receiver'];
                            $receiver = array(
                                'email' => $receiverObj['email'],
                                'phone' => $receiverObj['phone'],
                                'fullname' => '',
                            );
                            $address = '';
                            if (isset($receiverObj['address_only'])) $address .= $receiverObj['address_only'] . ', ';
                            if (isset($receiverObj['ward'])) $address .= $receiverObj['ward']['name'] . ', ';
                            if (isset($receiverObj['district'])) $address .= $receiverObj['district']['name'] . ', ';
                            if (isset($receiverObj['province'])) $address .= $receiverObj['province']['name'];
                            $receiver['address'] = $address;

                            if (isset($receiverObj['fullname']))
                                $receiver['fullname'] = $receiverObj['fullname'];

                            update_post_meta($new_order_id, 'receiver', $receiver);
                        }*/

        /*if($cartInfo['shippingMethod'] == 1){
           update_post_meta($new_order_id, 'order_transport_method', $cartInfo['transportMethod']);
        }*/
        /*}*/
        /*
                    if ($cartInfo['paymentMethod'] == 2 || $cartInfo['paymentMethod'] == 3) {
                        echo json_encode(array(
                            'success' => 2,
                            'message' => "Chuyển hướng sang Onepay",
                            'next_url' => redirect_onepay_url(array(
                                'user_id' => $user_id,
                                'user_email' => ($user_id != 0) ? $user_info->user_email : $cartInfo['email'],
                                'amount' => $total_calc,
                                'order_code' => $order_code,
                                'paymentMethod' => $cartInfo['paymentMethod']
                            ))
                        ));
                    } else {

                        echo json_encode(array(
                            'success' => 1,
                            'message' => "Đơn hàng đã được ghi nhận. Cám ơn Quý khách đã chọn lựa mua hàng tại Thời trang Khatoco. Chúng tôi sẽ liên hệ với Quý khách để giao hàng trong thời gian sớm nhất!",
                            'next_url' => add_query_arg(array(
                                'order_code' => $order_code,
                                'paid' => '1',
                                'statusPayment' => '0',
                                '_nonce' => wp_create_nonce('hash_dr')
                            ), get_page_link(2505)) // trang don hang ca nhan
                        ));

                        // Mail to user
                        if ($cartInfo['email'])
                            mail__announce_new_orders($cartInfo['email'], get_post($new_order_id));
                    }
        */

        // Delete session
    }
}