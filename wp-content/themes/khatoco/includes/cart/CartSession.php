<?php
require_once(get_template_directory() . '/includes/product/ProductUtils.php');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class CartSession
{

    public function __construct()
    {
    }

    /* Nguyên tắc update cart
    Chỉ update số lượng sản phẩm mua vào trong SESSION, mọi thông tin còn lại điều được tính khi response (có thể lưu lại ở SESSION nhưng sẽ tính lại sau đó) và ở frontend
    Khi khách hàng click thanh toán, tiến hành tính lại tất cả thông số ở backend */

    public static function getCartSession()
    {
        $cart_info = array(
            'products' => array(),
            'total' => 0,
            'total_weight' => 0,
            'payment' => 1, //default payment is "Trả tiền mặt"
            'transport_fee' => array(0, 0, 0),
            'campaignInfo' => null,
            'user_info' => self::getUserInfoFromDBFillToCart()
        );
        
        if (!empty($_SESSION['cart'])) {

            foreach ($_SESSION['cart']['products'] as $cart_key => $cart_row) {

                $tmp_product_id = $cart_row['id'];

                $psizes = get_post_meta($tmp_product_id, 'post_sizes', true);

                $psizes_lbl = [];
                $onhands = get_post_meta($tmp_product_id, 'post_onhand', true);

                foreach ($psizes as $size) {

                    $quantityOfSize = empty($onhands[$size]) ? 0 : $onhands[$size];

                    $quantityOfSize = empty($quantityOfSize) ? 0 : intval($quantityOfSize);

                    $psizes_lbl []= $size.' - (còn '.$quantityOfSize.')';

                }

                $tmp_price = intval(get_post_meta($tmp_product_id, 'post_price', true));

                $after_saleoff = ProductUtils::getProductSaleOffPrice($tmp_product_id);
                // điểm chết
                $cart_info['products'][$cart_key] = array(
                    'id' => $tmp_product_id,
                    'sku' => get_post_meta($tmp_product_id, 'post_sku', true),
                    'price' => $tmp_price,
                    'price_saleoff' => $after_saleoff,
                    'is_new' => !empty(get_post_meta( $tmp_product_id, 'post_is_new', true )),
                    'is_hot' => !empty(get_post_meta( $tmp_product_id, 'post_is_hot', true )),
                    'permalink' => get_permalink($tmp_product_id),
                    'name' => get_the_title($tmp_product_id),
                    'color_name' => $cart_row['color_name'],
                    'color_id' => $cart_row['color_id'],
                    'color_code' => $cart_row['color_code'],
                    'thumb' => $cart_row['color_thumb'],
                    'color_thumb' => $cart_row['color_thumb'],
                    'quantity' => $cart_row['quantity'],
                    'weight' => $cart_row['weight'],
                    'size' => $cart_row['size'],
                    'all_sizes' => $psizes,
                    'all_sizes_lbl' => $psizes_lbl,
                    'weight' => $cart_row['weight'],
                    'color_number' => $cart_row['color_number'],
                    'cart_id' => implode('__', array($tmp_product_id, $cart_row['color_number'], $cart_row['size'])),
                    'gift' => false
                );
            }


            $cart_info = self::calFeeOfCart($cart_info); // Tính các phí của cart (phí vận chuyển, tổng trọng lượng, tổng hóa đơn ...)

            $cart_info['payment'] = empty($_SESSION['cart']['payment']) ? 1 : $_SESSION['cart']['payment'];

            // Load campaign info
         

            if (!empty($_SESSION['cart']['campaignInfo']))
                $cart_info['campaignInfo'] = $_SESSION['cart']['campaignInfo'];

            $_SESSION['cart'] = $cart_info;

        }
        return $cart_info;
    }

    private static function calFeeOfCart($cart_info)
    { // Tính các chi phí của $cart nhập vào (bao gồm phí vận chuyển, tổng tiền trong cart...
        $total = 0;
        $total_saleOff = 0; // tổng tiền có giảm giá (VD admin không ban với giá gốc mà bán < giá gốc - giá saleOff)
        $total_weight = 0;
        foreach ($cart_info['products'] as $productKey => $product):

            $price = intval(get_post_meta($product['id'], 'post_price', true)); // return original price (200000, 300000, ...)
            $weight = intval(get_post_meta($product['id'], 'post_weight', true));
            $afterSaleOff = ProductUtils::getProductSaleOffPrice($product['id']);

            $total_weight += $weight * intval($product['quantity']);
            $total += $price * intval($product['quantity']);
            $total_saleOff += $afterSaleOff['value'] * intval($product['quantity']);

        endforeach;
        $cart_info['total_weight'] = $total_weight;
        $cart_info['total_sale_off'] = $total_saleOff;
        $cart_info['total'] = $total;
        $cart_info['transport_fee'][1] = get_transport_fee(1, $cart_info['total_weight']);
        $cart_info['transport_fee'][2] = get_transport_fee(2, $cart_info['total_weight']);
        return $cart_info;

    }

    public static function updateCartSession($cartRequest)
    {
        $products = $cartRequest['products'];
        if ( intval($cartRequest['payment']) != 1 && intval($cartRequest['payment']) != 2 && intval($cartRequest['payment']) != 3 ){ // Payment là 1 trong 3 giá trị này mới hợp lệ
            $cartRequest['payment'] = 1;
        }

        $_SESSION['cart']['payment'] = $cartRequest['payment'];

        $message = array('success' => 'Cập nhật giỏ hàng thành công!');

        if (empty($products)) {
            unset($_SESSION['cart']);
            return null;
        }

        foreach ($products as $key => $product) { // Kiểm tra số lượng và size còn trong kho
            $sizes = get_post_meta(intval($_SESSION['cart']['products'][$key]['id']), 'post_sizes', true);
            $onhands = get_post_meta(intval($_SESSION['cart']['products'][$key]['id']), 'post_onhand', true);

            if (isset($onhands[$product['size']]) && $product['quantity'] <= $onhands[$product['size']]) {
                $_SESSION['cart']['products'][$key]['quantity'] = $product['quantity'];
            } else {
                $message = array('error' => 'Số lượng sản phẩm ' . $product['name'] . ' vượt quá số sản phẩm có trong kho!');
                $cartRequest['products'][$key]['quantity'] = intval($_SESSION['cart']['products'][$key]['quantity']); // Trả lại số lượng cũ
            }

            if (in_array($product['size'], $sizes))
                $_SESSION['cart']['products'][$key]['size'] = $product['size'];
            else {
                $message = array('error' => 'Size ' . $product['size'] . 'này không tồn tại hoặc đã hết hàng');
                $cartRequest['products'][$key]['size'] = $_SESSION['cart']['products'][$key]['size']; //  Trả lại size cũ
            }
        }
        $cartRequest = self::calFeeOfCart($cartRequest);
        $cartRequest['campaignInfo'] = self::updateCampInCart($cartRequest);
        $cartRequest['msg'] = $message;
        return $cartRequest;
    }

    public static function initCart($cartRequest)
    {
        if (empty($cartRequest['products'])) {
            unset($_SESSION['cart']);
            return null;
        }
        $camp = CampUtils::getCampInformation($cartRequest);
        // print_r($camp);
        $cartRequest['campaignInfo'] = $camp;

        return $cartRequest;
    }

    public static function updateCampInCart($cartRequest)
    {
        $_SESSION['cart']['campaignInfo'] = CampUtils::getCampInformation($cartRequest); // Tính lại giỏ hàng có áp dụng được vào các campaign khuyến mãi hay không ?
        return $_SESSION['cart']['campaignInfo'];
    }

    public static function getUserInfoFromDBFillToCart()
    {
        $user_info = wp_get_current_user();
        $checkout_info = array(
            'fullname' => '',
            'email' => '',
            'phone' => '',
            'card' => '0',
            'address' => '',
        );

        if ($user_info->ID > 0) {
            $address = get_user_meta($user_info->ID, 'user_address', true);
            $fulladdress = get_user_meta($user_info->ID, 'user_address', true) . ', ';

            $temp = aj_get_ward(get_user_meta($user_info->ID, 'user_wardid', true));
            $fulladdress .= $temp['type'] . ' ' . $temp['name'] . ', ';
            $temp = aj_get_district(get_user_meta($user_info->ID, 'user_districtid', true));
            $fulladdress .= $temp['type'] . ' ' . $temp['name'] . ', ';
            $temp = aj_get_province(get_user_meta($user_info->ID, 'user_provinceid', true));
            $fulladdress .= $temp['type'] . ' ' . $temp['name'];

            $card = get_user_meta($user_info->ID, 'user_card', true);
            if ($card == '') $card = '0';

            $fullname = $user_info->display_name;
            $name = get_user_meta($user_info->ID, 'name', true);
            if ($name) $fullname = $name;
            $name = get_user_meta($user_info->ID, 'fullname', true);
            if ($name) $fullname = $name;

            $checkout_info = array(
                'fullname' => $fullname,
                'email' => $user_info->data->user_email,
                'phone' => get_user_meta($user_info->ID, 'user_phone', true),
                'card' => $card,
                'address' => $fulladdress,
                'address_only' => $address,
            );
        }
        return $checkout_info;
    }

    public static function verifyListBonusAndSpecial($cart)
    {
        $totalGetBonus = 0;
        $totalGetSpecial = 0;
        $message = array();
        if (!empty($cart['campaignInfo']['listBonus']['listProdBonus'])) {
            $totalCanGet = $cart['campaignInfo']['listBonus']['total'];
            $i=0;
            foreach($cart['campaignInfo']['listBonus']['listProdBonus'] as $bonus):
                if ($bonus['quantity'] > $totalCanGet){
                    $message []='Sản phẩm tặng '.$bonus['post_title'].' vượt quá số lượng cho phép';
                    $cart['campaignInfo']['listBonus']['listProdBonus'][$i]['quantity'] = 0;
                } else {
                    $sizes = get_post_meta($bonus['ID'], 'post_sizes', true); // Size trong hệ thống
                    $onhands = get_post_meta($bonus['ID'], 'post_onhand', true);

                    if ( (empty($onhands[$bonus['size']]) && $bonus['quantity'] > 0 ) || $bonus['quantity'] > $onhands[$bonus['size']]) {
                        $message [] = 'Số lượng sản phẩm ' . $bonus['post_title'] . ' vượt quá số sản phẩm có trong kho!';
                        $cart['campaignInfo']['listBonus']['listProdBonus'][$i]['quantity'] = 0;
                    }

                    if (!in_array($bonus['size'], $sizes)) {
                        $message [] = 'Size ' . $bonus['size'] . ' của sản phẩm ' . $bonus['post_title'] . ' không tồn tại hoặc đã hết hàng';
                        $cart['campaignInfo']['listBonus']['listProdBonus'][$i]['quantity'] = 0;
                    }
                    $totalGetBonus += $bonus['quantity'];
                }
            $i++;
            endforeach;

            if ($totalGetBonus > $totalCanGet)
                $message []='Tổng sản phẩm tặng có thể lấy vượt quá số lượng cho phép';
        }

        if (!empty($cart['campaignInfo']['listSpecialPrice']['listProdBonus'])) {
            $totalCanGet = $cart['campaignInfo']['listSpecialPrice']['total'];
            $i=0;
            foreach($cart['campaignInfo']['listSpecialPrice']['listProdBonus'] as $bonus):
                if ($bonus['quantity'] > $totalCanGet){
                    $message []='Sản phẩm giá đặc biệt '.$bonus['post_title'].' vượt quá số lượng cho phép';
                    $cart['campaignInfo']['listSpecialPrice']['listProdBonus'][$i]['quantity'] = 0;
                } else {
                    $sizes = get_post_meta($bonus['ID'], 'post_sizes', true); // Size trong hệ thống
                    $onhands = get_post_meta($bonus['ID'], 'post_onhand', true);

                    if (( empty($onhands[$bonus['size']]) && $bonus['quantity'] > 0 )|| $bonus['quantity'] > $onhands[$bonus['size']]) {
                        $message [] = 'Số lượng sản phẩm ' . $bonus['post_title'] . ' vượt quá số sản phẩm có trong kho!';
                        $cart['campaignInfo']['listSpecialPrice']['listProdBonus'][$i]['quantity'] = 0;
                    }

                    if (!in_array($bonus['size'], $sizes)) {
                        $message [] = 'Size ' . $bonus['size'] . ' của sản phẩm ' . $bonus['post_title'] . ' không tồn tại hoặc đã hết hàng';
                        $cart['campaignInfo']['listSpecialPrice']['listProdBonus'][$i]['quantity'] = 0;
                    }
                    $totalGetSpecial += $bonus['quantity'];
                }
            $i++;
            endforeach;

            if ($totalGetSpecial > $totalCanGet)
                $message []='Tổng sản phẩm tặng có thể lấy vượt quá số lượng cho phép';
        }
        $error = '';
        foreach ($message as $m):
            $error.=$m;
        endforeach;
        $result = [
            'cart' => $cart,
            'error' => $error
        ];
        return $result;
    }

    public static function viewCartSession()
    {
        echo '<pre>';
        print_r($_SESSION['cart']);
        exit();
    }

    public static function detroyCartSession()
    {
        unset($_SESSION['cart']);
    }

    public static function checkOutCart($cart)
    {

    }

    public static function calBillPrice($cart)
    { // Phương thức này giúp tính thành tiền của hóa đơn ( tổng thành tiền chưa trừ khuyến mãi )
        $total = $cart['total_sale_off'];
        if (!empty($cart->campaignInfo->listSpecialPrice) && !empty($cart->campaignInfo->listSpecialPrice->listProdBonus)) { // Kiểm tra sp mua giá đặc biệt
            foreach ($cart->campaignInfo->listSpecialPrice->listProdBonus as $product):
                $quantity = empty($product['quantity']) ? 0 : intval($product['quantity']);
                $id = $product['ID'];

                $price = get_post_meta($id, 'post_price', true);
                $sale_off = get_post_meta($id, 'sale_off', true);
                $price = (empty($sale_off) || intval($sale_off) == 0) ? $price : $price * (1 - $sale_off / 100);

                $total += $price * $quantity; // Tổng + thêm các sản phẩm mua thêm với giá ưu đãi
            endforeach;
        }
        return $total;
    }

}

require_once(get_template_directory() . '/includes/campaign/CampUtils.php');
