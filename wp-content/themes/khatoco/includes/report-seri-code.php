<?php 

define('WP_USE_THEMES', false);
require_once($_SERVER['DOCUMENT_ROOT'] . '/wp-load.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/wp-content/themes/khatoco/includes/phpexcel/PHPExcel.php');
set_time_limit(3000);
$cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_to_phpTemp;
$cacheSettings = array( 'memoryCacheSize' => '1024MB');

PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);
$objPHPExcel = new PHPExcel();

function calculatorTopupValue($topup){
	switch ($topup) {
		case 10000:
			return 9.091;
		case 20000:
			return 18.182;
		case 50000:
			return 45.455;
		case 100000:
			return 90.909;
		default:
			return null;
	}
}

function cellColor($cells,$color){
    global $objPHPExcel;

    $objPHPExcel->getActiveSheet()->getStyle($cells)->getFill()->applyFromArray(array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'startcolor' => array(
             'rgb' => $color
        )
    ));
}


// Orders export fields
$export_orders = array(
	'seri_code' => array(
		'required' => 1,
		'label' =>'Seri Code'
	),
	'pin' => array(
		'required' => 1,
		'label' =>'Pin'
	),
	'last_status' => array(
		'required' => 1,
		'label' =>'Last Status'
	),
	'admin_created' => array(
		'required' => 1,
		'label' =>'Admin tạo'
	),
	'created_at' => array(
		'required' => 1,
		'label' =>'Ngày tạo'
	),
	'id_product' => array(
		'required' => 1,
		'label' =>'ID Sản phẩm'
	),
	'scanned_at' => array(
		'required' => 1,
		'label' =>'Ngày scan'
	),
	'ware_house' => array(
		'required' => 1,
		'label' =>'Kho xuất'
	),
	'market_place' => array(
		'required' => 1,
		'label' =>'Thị trường'
	),
	'agency' => array(
		'required' => 1,
		'label' =>'Đại lý'
	),
	'admin_active' => array(
		'required' => 1,
		'label' =>'Admin active'
	),
	'actived_at' => array(
		'required' => 1,
		'label' =>'Ngày active'
	),
	'client_name' => array(
		'required' => 1,
		'label' =>'Họ Tên Khách Hàng'
	),
	'client_phone' => [
		'required'	=> 1,
		'label'		=> 'Số Điện Thoại Khách Hàng'
	],
	'client_birth_day' => array(
		'required' => 1,
		'label' =>'Ngày sinh'
	),
	'client_gender' => array(
		'required' => 1,
		'label' =>'Giới tính'
	),
	'client_city' => array(
		'required' => 1,
		'label' =>'Tỉnh/Thành Phố'
	),
    'client_province' => array(
        'required' => 1,
        'label' =>'Quận/Huyện'
    ),
	'top_up_value' => array(
		'required' => 1,
		'label' =>'Giá trị top-up đã nạp'
	),
	'used_at' => array(
		'required' => 1,
		'label' =>'Ngày sử dụng'
	),
	'admin_deleted' => array(
		'required' => 1,
		'label' =>'Admin xóa'
	),
	'deleted_at' => array(
		'required' => 1,
		'label' =>'Ngày xóa'
	),
	'questions_1' => array(
		'required' => 1,
		'label' =>'Thương hiệu thời trang công sở nào bạn thường mua hoặc sử dụng'
	),
	'questions_2' => array(
		'required' => 1,
		'label' =>'Sản phẩm nào của Thời trang Khatoco bạn từng mua hoặc sử dụng'
	),
);

$filename = 'Report-'.date("Y-m-d");
$redirect = $_POST['_wp_http_referer'];

global $wpdb;
$table_name = $wpdb->prefix . 'customer_register_cards';
$table_user_register = $wpdb->prefix . 'customer_register';

$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Thời trang Khatoco")->setLastModifiedBy("Thời trang Khatoco");
$row = 1;
$col = ord('A');
$style = array(
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	),
	'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_SOLID,
		'color' => array('rgb' => '666666')
	),
	'font'  => array(
	'color' => array('rgb' => 'FFFFFF'),
	'size'  => 13,
	)
);

foreach ($export_orders as $eokey => $eo) {
	$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $eo['label']);
	$objPHPExcel->getActiveSheet()->getColumnDimension(chr($col-1))->setAutoSize(true);
}
$objPHPExcel->getActiveSheet()->getStyle('A1:X1')->applyFromArray($style);


$args = array('post_type' => 'cards',
    			'posts_per_page' => -1,
				'orderby' => 'date',
				'meta_query'=>[
					[
						'key'	=> 'status',
						'value'	=> 'deleted',
						'compare' => 'NOT LIKE'
					]
				],
				'order' => 'ASC');
$posts = get_posts($args);

$datas = array();

for($i = 0; $i < count($posts);$i++) {
    // ob_start();
    $temp = array();
    $dateActive = get_post_meta($posts[$i]->ID, 'dateActive', true);
    $dataTmp = $wpdb->get_results('select * from ' . $table_name . ' left join ' . $table_user_register . ' on ' . $table_name . '.customer_register_id = ' . $table_user_register . '.id' . ' where post_id=' . $posts[$i]->ID);
    $temp['serial_code'] = get_post_meta($posts[$i]->ID, 'serial', true);
    $temp['pin'] = get_post_meta($posts[$i]->ID, 'pin', true);
    $temp['last_status'] = get_post_meta($posts[$i]->ID, 'status', true);
    $temp['admin_created'] = get_post_meta($posts[$i]->ID, 'userId', true);
    $temp['created_at'] = get_post_meta($posts[$i]->ID, 'createdAt', true);
    $temp['product_id'] = get_post_meta($posts[$i]->ID, 'product_id', true);
    $temp['scanned_at'] = $dateActive ? date_format(date_create($dateActive), 'd-m-y H:i') : null;
    $temp['ware_house'] = get_post_meta($posts[$i]->ID, 'ware_house', true);
    $temp['market_place'] = get_post_meta($posts[$i]->ID, 'market_place', true);
    $temp['agency'] = get_post_meta($posts[$i]->ID, 'agency', true);
    $temp['admin_active'] = get_post_meta($posts[$i]->ID, 'userUpdate', true);
    $temp['actived_at'] = $dateActive;
    $temp['client_name'] = !empty($dataTmp) ? $dataTmp[0]->name : null;
    $temp['client_phone'] = !empty($dataTmp) ? $dataTmp[0]->phone : null;
    $temp['client_birth_day'] = !empty($dataTmp) ? $dataTmp[0]->date_of_birth : null;
    $temp['client_gender'] = !empty($dataTmp) ? $dataTmp[0]->gender : null;

    $temp['client_city'] = !empty($dataTmp) ? explode(':',explode(',', $dataTmp[0]->address)[0])[1] : null;
    $temp['client_province'] = !empty($dataTmp) ? explode(':',explode(',', $dataTmp[0]->address)[1])[1] : null;

    //$temp['client_address'] = !empty($dataTmp) ? explode(',', $dataTmp[0]->address)[0] . " \n" . explode(',', $dataTmp[0]->address)[1] : null;

    $temp['top_up_value'] = !empty($dataTmp) ? calculatorTopupValue(floatval($dataTmp[0]->top_up_value)) : null;

    $temp['used_at'] = !empty($dataTmp) ? $dataTmp[0]->created_at : null;
    $temp['admin_deleted'] = get_post_meta($posts[$i]->ID, 'admin_deleted', true);
    $temp['deleted_at'] = get_post_meta($posts[$i]->ID, 'deleted_at', true);
    $temp['questions_1'] = "";
    $temp['questions_2'] = "";
    if (!empty($dataTmp)) {

        $temp['questions_1'] .= join(unserialize($dataTmp[0]->questions)['answer1'], ', ');

        $temp['questions_2'] .= join(unserialize($dataTmp[0]->questions)['answer2'], ', ');
    }

    array_push($datas, $temp);
}


for($j = 0; $j < count($datas); $j++){

	$row++;
	$col = ord('A');
	$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $datas[$j]['serial_code']);
	$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $datas[$j]['pin']);
	$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $datas[$j]['last_status']);
	$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $datas[$j]['admin_created']);
	$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $datas[$j]['created_at']);
	$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $datas[$j]['product_id'], PHPExcel_Style_NumberFormat::FORMAT_TEXT);
	$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $datas[$j]['scanned_at']);
	$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $datas[$j]['ware_house']);
	$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $datas[$j]['market_place']);
	$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $datas[$j]['agency']);
	$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $datas[$j]['admin_active']);
	$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $datas[$j]['actived_at']);
	$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $datas[$j]['client_name']);
	$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $datas[$j]['client_phone']);
	$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $datas[$j]['client_birth_day']);
	$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $datas[$j]['client_gender']);

    $objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $datas[$j]['client_city']);
    $objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $datas[$j]['client_province']);

	//$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $datas[$j]['client_address']);
	$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $datas[$j]['top_up_value']);
	$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $datas[$j]['used_at']);
	$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $datas[$j]['admin_deleted']);
	$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $datas[$j]['deleted_at']);
	$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $datas[$j]['questions_1']);
	$objPHPExcel->getActiveSheet()->setCellValue(chr($col++) . $row, $datas[$j]['questions_2']);
	$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(-1); 
	$objPHPExcel->getActiveSheet()->getStyle(chr($col++) . $row)->getAlignment()->setWrapText(true);

	switch ($datas[$j]['last_status']) {
		case 'active':
			cellColor('A'.$row.':X'.$row, 'C55912');
			break;
		case 'used':
			cellColor('A'.$row.':X'.$row, '538134');
			break;
		default:
			cellColor('A'.$row.':X'.$row, 'FFF2CC');
			break;
	}


}
	// ob_end_clean();

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment; filename="'. $filename .'.xlsx"');
header('Cache-Control: max-age=0');
header('Cache-Control: max-age=1');
header ('Last-Modified: '. gmdate('D, d M Y H:i:s') .' GMT');
header ('Cache-Control: cache, must-revalidate');
header ('Pragma: public');

PHPExcel_Calculation::getInstance()->writeDebugLog = true;

ob_clean();
flush();

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
try {
    ob_end_clean();
    $objWriter->save('php://output');
}catch (Exception $e){
    echo 'Caught exception: ',  $e->getMessage(), "\n";
}

wp_redirect($redirect); 
exit;
?>