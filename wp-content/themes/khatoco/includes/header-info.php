<?php

/* INIT HEAD INFORMATION */
$GLOBALS['head_info'] = array(
    'title' => get_bloginfo('name'),
    'description' => is_single() ? wp_title('', false).', '.get_bloginfo('description') : get_bloginfo('description'),
    'keywords' => 'nhà sách cá chép, đọc sách online, mua sách trực tuyến',
    'url' => get_home_url('/').'/'.$wp->request,
    'image' => get_template_directory_uri().'/assets/images/thumnail.png',
    'facebookId' => '1418634538394476'
);

/* INIT BREADCRUMB */
$GLOBALS['breadcrumb'] = '<ul class="breadcrumb no-style" xmlns:v="http://rdf.data-vocabulary.org/#"><li typeof="v:Breadcrumb"><a rel="v:url" property="v:title" href="'.get_home_url('/').'" rel="nofollow">Trang chủ</a> <span class="divider">/</span></li>';

/* PRE RENDER */
if(is_single()){
    if($post->ID > 0){
        $GLOBALS['head_info']['title'] = $post->post_title;
        $GLOBALS['head_info']['url'] = get_permalink($post->ID);
        if($post->post_excerpt){
            $GLOBALS['head_info']['description'] = $post->post_excerpt.', '.$post->post_title;
        }else{
            $GLOBALS['head_info']['description'] = $post->post_title.', '.$GLOBALS['head_info']['description'];
        }
        if(has_post_thumbnail($post->ID )){
            $tmp = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium');//'share-thumbnail');
            $GLOBALS['head_info']['image'] = $tmp[0];
            unset($tmp);
        }
        if($post_tags = aj_wp_get_post_terms($post->ID, 'post_tag', true)){
            $GLOBALS['head_info']['keywords'] = strip_tags($post_tags);
        }
        
        function aj_build_breadcrumb($cat, $level = 1, $breadcrum_cats = array()){
            $breadcrum_cats[$level][$cat->term_id] = '<a rel="v:url" property="v:title" href="'.get_term_link($cat->term_id, $cat->taxonomy).'" rel="nofollow">'.$cat->name.'</a>';
            if($cat->parent > 0){
                $breadcrum_cats += aj_build_breadcrumb(get_term($cat->parent, $cat->taxonomy), $level+1, $breadcrum_cats);
            }
            return $breadcrum_cats;
        }

        $breadcrum_levels = array();
        $level = 1;

        if($post->post_type == 'post'){
            $post_cats = wp_get_post_terms($post->ID, 'category');
        }elseif($post->post_type == 'news'){
            $post_cats = wp_get_post_terms($post->ID, 'news_category');
        }

        if(!empty($post_cats)){
            foreach($post_cats as $cat){
                $breadcrum_levels += aj_build_breadcrumb($cat, $level, $breadcrum_levels);
            }

            krsort($breadcrum_levels);

            foreach($breadcrum_levels as $blevel){
                $GLOBALS['breadcrumb'] .= '<li typeof="v:Breadcrumb">'.implode(',&nbsp', $blevel).'<span class="divider">/</span></li>';
            }
        }
        

        $GLOBALS['breadcrumb'] .= '<li typeof="v:Breadcrumb" class="active"><em property="v:title">'.$post->post_title.'</em></li>';
    }else{
        $GLOBALS['breadcrumb'] .= '<li typeof="v:Breadcrumb" class="active">404</li>';
    }
}elseif(is_page()){
    $GLOBALS['head_info']['title'] = $post->post_title.' - '.$GLOBALS['head_info']['title'];
    $GLOBALS['head_info']['url'] = get_permalink($post->ID);
    $GLOBALS['head_info']['description'] = $post->post_title.', '.$GLOBALS['head_info']['description'];
    $GLOBALS['head_info']['keywords'] = $post->post_title.', '.$GLOBALS['head_info']['keywords'];

    if($post->ID == 2 || $post->ID ==  183){
    	$GLOBALS['breadcrumb'] .= '<li typeof="v:Breadcrumb" class="active"><em property="v:title">Về chúng tôi</em></li>';
    }

    if($post->ID == 218){
        if(isset($_GET['q']) && $search_keywords = $_GET['q']){
            $GLOBALS['head_info']['title'] = $search_keywords.' - '.$GLOBALS['head_info']['title'];
            $GLOBALS['head_info']['description'] = $search_keywords.', '.$GLOBALS['head_info']['description'];
            $GLOBALS['breadcrumb'] .= '<li typeof="v:Breadcrumb" class="active"><em>Tìm kiếm</em></li>';
            //$GLOBALS['breadcrumb'] .= '<li typeof="v:Breadcrumb" class="active">Kết quả tìm kiếm cho từ khóa "<strong>'.  $search_keywords.'</strong>"</li>';
            $GLOBALS['head_info']['keywords'] = $search_keywords.', '.$GLOBALS['head_info']['keywords'];
        }
    }
}
if(is_post_type_archive()){
    $post_type = get_post_type();
    $current_post_type = get_post_type_object($post_type);
    //print_r($current_post_type);
    $GLOBALS['head_info']['title'] = $current_post_type->labels->name.' - '.$GLOBALS['head_info']['title'];
    $GLOBALS['head_info']['url'] = get_post_type_archive_link($post_type);
    $GLOBALS['head_info']['description'] = $current_post_type->description.', '.$GLOBALS['head_info']['description'];
    $GLOBALS['head_info']['keywords'] = $current_post_type->labels->name.', '.$GLOBALS['head_info']['keywords'];

    if($post_type == 'job'){
    	$GLOBALS['breadcrumb'] .= '<li typeof="v:Breadcrumb" class="active"><em property="v:title">Về chúng tôi</em></li>';
    }else{
    	$GLOBALS['breadcrumb'] .= '<li typeof="v:Breadcrumb" class="active"><strong property="v:title">'.$current_post_type->labels->name.'</strong></li>';
    }
    
}elseif(is_archive()){
    $current_post_type = get_post_type();
    $current_term = get_queried_object();

    if($current_post_type == 'news'){
        $current_post_type_object = get_post_type_object($current_post_type);
        $GLOBALS['breadcrumb'] .= '<li typeof="v:Breadcrumb"><a rel="v:url" property="v:title" href="'.get_post_type_archive_link($current_post_type).'" rel="nofollow">'.$current_post_type_object->labels->name.'</a> <span class="divider">/</span></li>';
    }

    if($current_term->parent > 0){
        $parent_terms = aj_get_term($current_term->parent, $current_term->taxonomy);
        if(!empty($parent_terms)){
            foreach($parent_terms as $t){
                $GLOBALS['breadcrumb'] .= '<li typeof="v:Breadcrumb"><a rel="v:url" property="v:title" href="'.get_term_link($t->term_id, $t->taxonomy).'" rel="nofollow">'.$t->name.'</a> <span class="divider">/</span></li>';
            }
        }
    }
    
    if(isset($_GET['s'])){
        $current_term_name = $_GET['s'].' in '.$current_term->name;
        $GLOBALS['breadcrumb'] .= '<li typeof="v:Breadcrumb"><a rel="v:url" property="v:title" href="'.get_term_link($current_term->term_id, $current_term->taxonomy).'" rel="nofollow">'.$current_term->name.'</a> <span class="divider">/</span></li>';
        $GLOBALS['breadcrumb'] .= '<li typeof="v:Breadcrumb" class="active"><em property="v:title">Search result for "<strong>'.$_GET['s'].'</strong>"</em></li>';
        $GLOBALS['head_info']['keywords'] = $_GET['s'].', '.$GLOBALS['head_info']['keywords'];
    }else{
        $GLOBALS['breadcrumb'] .= '<li typeof="v:Breadcrumb" class="active"><em property="v:title">'.$current_term->name.'</em></li>';
        $current_term_name = $current_term->name;
    }
    
    $GLOBALS['head_info']['title'] = $current_term_name.' - '.$GLOBALS['head_info']['title'];
    $GLOBALS['head_info']['url'] = get_term_link($current_term->term_id, $current_term->taxonomy);
    $GLOBALS['head_info']['description'] = $current_term_name.', '.$GLOBALS['head_info']['description'];
    $GLOBALS['head_info']['keywords'] = $current_term->name.', '.$GLOBALS['head_info']['keywords'];
}elseif(is_home()){
    $GLOBALS['breadcrumb'] .= '<li typeof="v:Breadcrumb" class="active">All</li>';
}elseif(is_search()){
    if($search_keywords = get_query_var('s')){
        $GLOBALS['head_info']['title'] = $search_keywords.' - '.$GLOBALS['head_info']['title'];
        $GLOBALS['head_info']['description'] = $search_keywords.', '.$GLOBALS['head_info']['description'];
        $GLOBALS['breadcrumb'] .= '<li typeof="v:Breadcrumb" class="active">Search result for "<strong>'.  $search_keywords.'</strong>"</li>';
        $GLOBALS['head_info']['keywords'] = $search_keywords.', '.$GLOBALS['head_info']['keywords'];
    }
}

if($paged > 1){
    $GLOBALS['head_info']['title'] .= ' - Trang '.$paged;
    $GLOBALS['head_info']['description'] .= ' - Trang '.$paged;
}

/* CLOSE BREADCRUMB */
$GLOBALS['breadcrumb'] .= '</ul>';