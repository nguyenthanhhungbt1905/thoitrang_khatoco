<?php

abstract class DataCampTable
{

    abstract protected function create_popup_camp($is_edit_popup = false);

    abstract protected function generate_logic_script_table();

    abstract protected function convertDataList();


    private $config;
    private $headers;
    private $data;
    private $popup;

    public function __constructor($config, $headers, $data)
    {
        $this->config = $config;
        $this->headers = $headers;
        $this->data = $data;
    }

    public function generate_camp_table()
    {

        $popup['add'] = $this->create_popup_camp();
        $popup['edit'] = $this->create_popup_camp(true);
        ?>

        <div class="wrap-table-camp" id="wrap-table-camp-<?php echo $this->config['camp_tb']; ?>"
             ng-show="<?php if (empty($this->config['sale_type'])) {
                 echo 'camp.form === \'' . $this->config['type_offer'] . '\' && (camp.saleType === \'multi_product\' || camp.saleType === \'buy_money_product\')';
             } else
                 echo 'camp.saleType===\'' . $this->config['sale_type'] . '\'';
             ?>">
            <!--BUTTON ADD / EDIT / DELETE-->
            <button type='button' id='<?php echo $this->config['btn'] . '_add'; ?>'
                    class='<?php echo $this->config['btn'] . '_add'; ?> btn' data-toggle="modal"
                    data-target="#<?php echo $this->config['camp_tb'] . '_add_popup'; ?>"><i class="fa fa-plus"
                                                                                             aria-hidden="true"></i>
                Thêm
            </button>

            <button type='button' id='<?php echo $this->config['btn'] . '_rm'; ?>'
                    class='<?php echo $this->config['btn'] . '_rm'; ?> btn'><i class="fa fa-times"
                                                                                          aria-hidden="true"></i> Xóa
            </button>

            <div class='table_camp_error'></div>
            <!--TABLE DATA-->
            <table id='<?php echo $this->config['camp_tb']; ?>' class="<?php echo $this->config['camp_tb']; ?> display"
                   cellspacing="0" width='100%'>
            </table>

            <!--POP UP ADD-->
            <div class="modal fade" tabindex="-1" id='<?php echo $this->config['camp_tb'] . '_add_popup'; ?>'
                 role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Thêm <?php echo $this->config['btn_text']; ?></h4>
                        </div>
                        <div class="modal-body">
                            <?php echo $popup['add']; ?>
                            <div class='bs-error'></div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="'subm_add_<?php echo $this->config['btn']; ?> btn btn-success"
                                    id='subm_add_<?php echo $this->config['btn']; ?>'><i class="fa fa-plus"
                                                                                         aria-hidden="true"></i> Thêm
                            </button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"
                                                                                                 aria-hidden="true"></i>
                                Đóng
                            </button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

        </div> <!--end wrap table camp -->
        <script>
            var table_<?php echo $this->config['camp_tb']; ?> = null;
            jQuery(document).ready(function ($) {
                jQuery('#<?php echo $this->config['btn'] . '_add'; ?>').on('click', function (e) {
                    bs_error.clear_error();
                });

                jQuery('#<?php echo $this->config['btn'] . '_edit'; ?>').on('click', function (e) {
                    var row_selected = table_<?php echo $this->config['camp_tb']; ?>.rows({selected: true});
                    if (row_selected.count() > 1 || row_selected.count() === 0) {
                        e.stopPropagation();
                        camp_tb_error.set_error('* Bạn phải chọn 1 dòng để thay đổi dữ liệu');
                        return;
                    }
                    camp_tb_error.clear_error();
                    bs_error.clear_error();
                });

                jQuery('#<?php echo $this->config['btn'] . '_rm'; ?>').on('click', function (e) {
                    var row_selected = table_<?php echo $this->config['camp_tb']; ?>.rows({selected: true});
                    if (row_selected.count() === 0) {
                        camp_tb_error.set_error('* Bạn chưa chọn dữ liệu để xóa');
                        return;
                    }
                    camp_tb_error.clear_error();
                    table_<?php echo $this->config['camp_tb']; ?>.rows({'selected': true}).remove().draw();
                    table_<?php echo $this->config['camp_tb']; ?>.rows(table_<?php echo $this->config['camp_tb']; ?>.rows().data()).invalidate().data();
                });
            });

            var list_products = <?php echo json_encode(get_all_product_min()); ?>;
            var list_cats = <?php echo json_encode(get_all_cat_min()); ?>;
        </script>
        <?php
    }

    public function generate_script_camp_table()
    {
        ?>
        <script src="<?php echo get_template_directory_uri() . '/assets/js/data_table_module.js'; ?>"
                type="text/javascript"></script>
        <?php
    }

}
