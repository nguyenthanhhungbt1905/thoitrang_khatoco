<?php get_header();

$post_cats = array();
// $post_cats_priority = array();

$all_cats = get_categories(array(
	'orderby' => 'id',
	'hide_empty' => 0
));
foreach($all_cats as $cat_obj) {
	$obj_id = $cat_obj->term_id;
	$post_cats[] = $obj_id;
}
print_r($post_cats);

$cat = array();
$cat['name'] = single_cat_title('', false);
$cat['id'] = get_cat_id($cat['name']);

global $post;
$post_type = ($post) ? $post->post_type: '';

$tag_name = single_tag_title('', false);
$tag = get_term_by('name', $tag_name, 'post_tag'); ?>
<div class="tax-slide-holder">
	<div class="tax-slide">
		<ul class="slides clearfix">
			<li>
				<img src="<?php echo get_template_directory_uri(); ?>/images/tax-slider-1.png" alt="">
				<div class="intro">
					<h2 class="condensed">Áo sơ mi tay ngắn</h2>
					<p>Phong cách khỏe khoắn mùa he 2014</p>
				</div>
			</li>
			<li>
				<img src="<?php echo get_template_directory_uri(); ?>/images/tax-slider-1.png" alt="">
				<div class="intro">
					<h2 class="condensed">Áo sơ mi tay ngắn</h2>
					<p>Phong cách khỏe khoắn mùa he 2014</p>
				</div>
			</li>
			<li>
				<img src="<?php echo get_template_directory_uri(); ?>/images/tax-slider-1.png" alt="">
				<div class="intro">
					<h2 class="condensed">Áo sơ mi tay ngắn</h2>
					<p>Phong cách khỏe khoắn mùa he 2014</p>
				</div>
			</li>
		</ul>
	</div>
</div>
<div class="block-outer block-tax">
	<div class="block-inner container clearfix fluid">
		<div class="grid2-5 fl clearfix left-col">
			<h2 class="condensed">Sản phẩm</h2>
			<ul class="tax-left-menu clearfix">
				<?php aj_nav_menu('sidebar-category-menu'); ?>
			</ul>
			<div class="block-advertise">
				<a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/ad.png" alt=""></a>
			</div>
			<div class="block-tag"><?php wp_tag_cloud(array(
				'smallest' => 13,
				'largest' => 18,
				'unit' => 'px',
				'number' => 20,
			)); ?>
			</div>
		</div>
		<div class="grid9-5 fl block-main">
			<div class="top-block clearfix">
				<ul class="breadcrumb list-style-none fl">
					<?php blix_breadcrumbs(); ?>
				</ul>
				<span class="fr">
					Hiển thị&nbsp;&nbsp;
					<span class="sort sort-list <?php if(isset($_COOKIE['view-style']) && $_COOKIE['view-style'] == 'list') echo 'active'; ?> fr"></span>&nbsp;&nbsp;
					<span class="sort sort-grid <?php if(!isset($_COOKIE['view-style']) ||(isset($_COOKIE['view-style']) && $_COOKIE['view-style'] == 'grid')) echo 'active'; ?> fr"></span>
				</span>
			</div>
			<div class="block-content">
				<div class="block-filter-product clearfix">
					<form action="" method="GET" id="form-filter">
						<div class="categories-filter">
							<select class="fl form-record root-record" name="form[]" id="dong-san-pham" data-order="1">
								<option value="">Loại sản phẩm</option><?php $args = array('hide_empty' => 0, 'parent' => 0, 'orderby' => 'id');
								$cats_p = get_categories($args);
								foreach($cats_p as $cat_p) {
									echo '<option value="'. $cat_p->slug .'" data-id="'. $cat_p->term_id .'" '. (isset($_GET['form'][0]) ? selected($_GET['form'][0], $cat_p->slug, false):'') .'>'. $cat_p->name .'</option>';
								} ?>
							</select><?php if(isset($_GET['form'])) {
								foreach ($_GET['form'] as $key => $record_get) {
									if($key != 0) {
										if($record_get != '')
											generate_filter_record($record_get, $key);
										else generate_filter_record($_GET['form'][$key-1], $key, false);
									}
								}
							} ?>
						</div>
						<select class="fl" name="price" id="gia-tien">
							<option value="">Giá tiền</option><?php $price_a = array(
							'0-500' => 'Nhỏ hơn 500k',
							'500-1000' => '500k-1000k',
							'1000-1500' => '1000k-1500k',
							'1500-*' => '1500k trở lên',
						);
						foreach ($price_a as $key => $pr) {
							$slt = isset($_GET['price']) ? selected($key, $_GET['price'], false):'';
							echo '<option value="'. $key .'" '. $slt .'>'. $pr .'</option>';
						} ?>
						</select>
						<button type="submit" id="filter-submit" class="submit-btn fl condensed">Liệt kê</button>
						<script>
						jQuery(document).ready(function() {
							jQuery(document).delegate('#form-filter .form-record', 'change', function() {
								var order = parseInt(jQuery(this).attr('data-order'));
								for(var i = (order+1); i <= 4; i++) {
									jQuery('#form-filter .form-record[data-order="'+ i +'"]').remove();
								}
								var parent = jQuery(this).find('option:selected').attr('data-id');
								if(parent != '') {
									jQuery.ajax({
										url: wp_vars.ajaxurl,
										type: 'POST',
										dataType: 'json',
										data: {
											action: 'filter__get_sub_categories',
											cat_p: parent
										}
									})
									.done(function(res) {
										if(res.length == 0)
											return false;
										var form_name = 'form-', i = 1, max_order = 1;;
										while(true) {
											if(jQuery(document).find('#form-'+ i).length <= 0) {
												form_name += i;
												break;
											}
											else i++;
										}
										jQuery('body').find('.form-record').each(function() {
											var temp_order = parseInt(jQuery(this).attr('data-order'));
											if(temp_order >= max_order)
												max_order = temp_order + 1;
										});
										var html = '<select class="fl form-record" name="form[]" id="'+ form_name +'" data-order="'+ max_order +'"><option value="">--Lựa chọn--</option>';
										for(var i in res) {
											html += '<option value="'+ res[i]['slug'] +'" data-id="'+ res[i]['id'] +'">'+ res[i]['name'] +'</option>';
										}
										html += '</select>';
										jQuery('#form-filter .categories-filter').append(html);
									});
								}
							});
						});
						</script>
					</form>
				</div>
				<div class="block-list-products"><?php 
				$paged = (get_query_var('page')) ? get_query_var('page') : 1;
				$posts_per_page = 36;
				$args = array('posts_per_page' => $posts_per_page, 'tag_id' => $tag->term_id, 'orderby' => 'date', 'order' => 'DESC', 'paged' => $paged);
				// Price filter
				if(isset($_GET['price']) && $_GET['price'] != '') {
					$args['meta_query'] = array(
						array(
							'key' => 'post_price',
							// 'type' => 'numeric'
						)
					);

					$pp = explode('-', $_GET['price']);
					if($pp[0] == '0') {
						$args['meta_query'][0]['value'] = $pp[1]*1000;
						$args['meta_query'][0]['compare'] = '<=';
					}
					else if($pp[1] == '*') {
						$args['meta_query'][0]['value'] = $pp[0]*1000;
						$args['meta_query'][0]['compare'] = '>=';
					}
					else {
						$args['meta_query'][0]['value'] = $pp[0]*1000;
						$args['meta_query'][0]['compare'] = '>=';

						$args['meta_query'][1]['value'] = $pp[1]*1000;
						$args['meta_query'][1]['compare'] = '<=';
					}

				}
				// Category filter
				if(isset($_GET['form']) && !empty($_GET['form'])) {
					$filter_cat_name = '';
					if(isset($_GET['form'][0]) && $_GET['form'][0] != '') {
						foreach ($_GET['form'] as $f_key => $f) {
							if($f != '') {
								if($f_key != 0) $filter_cat_name .= '+';
								$filter_cat_name .= $f;
							}
							else continue;
						}
						$args['category_name'] = $filter_cat_name;
					}
				}
				else {
					$args['cat'] = $cat['id'];
				}
				if(isset($_GET['khuyen-mai'])) {
					$args['meta_query'][] = array(
						'key' => 'sale_off',
						'value' => $_GET['khuyen-mai']
					);
				}
				/*if(current_user_can('administrator')) {
					$args['posts_per_page'] = 1;
				}*/
				// print_r($args);
				$p_lists = new WP_Query($args);
				$p_lists_temp = array();
				if($p_lists->have_posts()): ?>
					<ul class="lists grid-style fluid clearfix <?php if(isset($_COOKIE['view-style']) && $_COOKIE['view-style'] == 'list') echo 'hidden'; ?>">
					<?php while($p_lists->have_posts()): $p_lists->the_post();
						$p_lists_temp[] = $post->ID; ?>
						<li class="grid4 fl single-item-product">
							<div class="item-outer clearfix">
								<div class="item-inner clearfix"><?php $product_colors = get_post_meta($post->ID, 'post_colors', true);
								foreach($product_colors as $c_key => $c): ?>
									<div class="item-thumb" data-color="<?php echo $c_key; ?>">
										<a href="<?php the_permalink(); ?>"><img src="<?php echo $product_colors[$c_key]['thumb']; ?>" /></a>
										<span class="add-to-wishlist"><i class="fa fa-heart"></i></span>
									</div><?php endforeach; ?>
									<h3 class="item-title condensed"><a href="<?php the_permalink(); ?>"><?php the_title(); ?> <span>(256)</span></a></h3>
									<?php $saleoff_info = get_saleoff_info($post->ID);

						            if($saleoff_info){
						                $sale_off = (int) $saleoff_info['sale'];
						                $sale_txt_class = 'sale';
						                echo '<p class="item-price">Giá '. $saleoff_info['new_price_formated'] .' Đ</p>';
						            }
						            else{
										$sale_txt_class = '';
						            } ?>
									<p class="item-price <?php echo $sale_txt_class; ?>">Giá <?php echo number_format(get_post_meta($post->ID, 'post_price', true ), 0, ',', '.'); ?> Đ</p>
									<p class="item-color clearfix"><?php foreach ($product_colors as $c_key => $c) { ?>
										<span class="fl color" style="background-color: <?php echo $c['code']; ?>" title="<?php echo $c['name']; ?>" data-color="<?php echo $c_key; ?>"></span>
									<?php } ?></p>
								</div>
							</div>
						</li><?php endwhile; ?>
					</ul>
					<ul class="lists list-style list-style-none fluid clearfix <?php if(!isset($_COOKIE['view-style']) ||(isset($_COOKIE['view-style']) && $_COOKIE['view-style'] == 'grid')) echo 'hidden'; ?>"><?php foreach ($p_lists_temp as $pkey => $p) { ?>
						<li class="row fl single-item-product">
							<div class="item-outer clearfix">
								<div class="item-inner fl clearfix"><?php $product_colors = get_post_meta($p, 'post_colors', true);
								foreach($product_colors as $c_key => $c): ?>
									<div class="item-thumb" data-color="<?php echo $c_key; ?>">
										<a href="<?php echo get_permalink($p); ?>"><img src="<?php echo $product_colors[$c_key]['thumb']; ?>" /></a>
										<span class="add-to-wishlist"><i class="fa fa-heart"></i></span>
									</div><?php endforeach; ?>
									<div class="item-infos fl">
										<h3 class="item-title condensed"><a href="<?php echo get_permalink($p); ?>"><?php echo get_the_title($p); ?> <span>(256)</span></a></h3>
										<?php $saleoff_info = get_saleoff_info($p);
							            	
							            if($saleoff_info){
							                $sale_off = (int) $saleoff_info['sale'];
							                $sale_txt_class = 'sale';
							                echo '<p class="item-price">Giá '. $saleoff_info['new_price_formated'] .' Đ</p>';
							            }
							            else{
											$sale_txt_class = '';
							            } ?>
										<p class="item-price <?php echo $sale_txt_class; ?>">Giá <?php echo number_format(get_post_meta($p, 'post_price', true ), 0, ',', '.'); ?> Đ</p>
										<p class="item-color clearfix"><?php foreach ($product_colors as $c_key => $c) { ?>
											<span class="fl color" style="background-color: <?php echo $c['code']; ?>" title="<?php echo $c['name']; ?>" data-color="<?php echo $c_key; ?>"></span>
										<?php } ?></p>
									</div>
								</div>
							</div>
						</li>
					<?php } ?>
					</ul>
					<?php $p_counts = $p_lists->found_posts;
					// if($p_counts > 12): ?>
					<div class="pagination clearfix">
						<div class="grid5 fl total"><?php $p_start = ($paged - 1)*$posts_per_page + 1;
						$p_end = $paged*$posts_per_page;
						$p_end = ($p_end > $p_counts) ? $p_counts : ($paged*$posts_per_page); ?>
							<p><?php echo 'Từ '. $p_start .' đến '. $p_end .' (trong '. $p_counts .' sản phẩm)'; ?></p>
						</div>
						<div class="grid7 fr paginate clearfix">
							<ul class="list-style-none fr">
								<div id="paginate-hidden" class="hidden"><?php //$big = 999999999;
								echo paginate_links( array(
									'base' => '%_%',
									'format' => '?page=%#%',
									'show_all' => true,
									'prev_text' => '',
									'next_text' => '',
									'current' => max( 1, get_query_var('page') ),
									'total' => $p_lists->max_num_pages
								));  wp_reset_postdata(); ?></div>
								<li class="fl"><a href="" id="page_n_prev" class="prev page-numbers"></a></li>
								<li class="fl select-holder" id="paginate-show">
									<p>
										Trang <select class="" name="paginate" id="paginate"></select> trong <?php echo $p_lists->max_num_pages; ?>
									</p>
								</li>
								<li class="fl"><a href="" id="page_n_next" class="next page-numbers"></a></li>
							</ul>
							<script type="text/javascript">
							jQuery(document).ready(function() {
								var page_n = [];
								var page_n_curt = '';
								var page_n_prev = '%_%';
								var page_n_next = '%_%';

								jQuery('#paginate-hidden').find('.page-numbers').each(function() {
									if(jQuery(this).hasClass('current'))
										page_n_curt = jQuery(this).text();
									else if(jQuery(this).hasClass('next'))
										page_n_next = jQuery(this).attr('href');
									else if(jQuery(this).hasClass('prev'))
										page_n_prev = jQuery(this).attr('href');
									else 
										page_n.push(jQuery(this).attr('href'));
								});
								page_n.splice(page_n_curt-1, 0, '');
								if(page_n[0] == '') page_n[0] = '?page=1';
								if(page_n[page_n.length-1] == '') page_n[page_n.length-1] = '?page='+ page_n.length;

								if(page_n_prev != '%_%')
									jQuery('#page_n_prev').attr('href', page_n_prev);
								else
									jQuery('#page_n_prev').addClass('disabled');

								if(page_n_next != '%_%')
									jQuery('#page_n_next').attr('href', page_n_next);
								else
									jQuery('#page_n_next').addClass('disabled');

								var html = '';
								for(var i in page_n) {
									var index = parseInt(i);
									
									if((index+1) == page_n_curt) {
										html += '<option selected="">'+ (index+1) +'</option>';
										continue;
									}
									var redirect_uri = page_n[i];
									if(redirect_uri != '')
										html += '<option onclick="window.location.replace(\''+ redirect_uri +'\')">'+ (index+1) +'</option>';
									else
										html += '<option onclick="window.location.replace(\'?page=1\')">'+ (index+1) +'</option>';
								}
								jQuery('#paginate').append(html);

								jQuery('.page-numbers.disabled').click(function(event) {
									event.preventDefault();
									return false;
								});
							});
							</script>
						</div>
					</div><?php  //endif;
					else:
						echo '<p style="text-align: center; padding: 30px 20px;">Hiện chưa có sản phẩm trong mục này!</p>';
					endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="block-outer block-catalogue">
	<div class="block-catalogue-inner container fluid">
		<div class="catalogue-holder">
			<ul class="slides list-style-none">
				<li class="fl"><a class="fl" href=""><img src="<?php echo get_template_directory_uri(); ?>/images/catalogue-1.png" alt=""><span><i class="fa fa-eye fa-2x"></i></span></a></li>
				<li class="fl"><a class="fl" href=""><img src="<?php echo get_template_directory_uri(); ?>/images/catalogue-1.png" alt=""><span><i class="fa fa-eye fa-2x"></i></span></a></li>
				<li class="fl"><a class="fl" href=""><img src="<?php echo get_template_directory_uri(); ?>/images/catalogue-1.png" alt=""><span><i class="fa fa-eye fa-2x"></i></span></a></li>
				<li class="fl"><a class="fl" href=""><img src="<?php echo get_template_directory_uri(); ?>/images/catalogue-1.png" alt=""><span><i class="fa fa-eye fa-2x"></i></span></a></li>
				<li class="fl"><a class="fl" href=""><img src="<?php echo get_template_directory_uri(); ?>/images/catalogue-1.png" alt=""><span><i class="fa fa-eye fa-2x"></i></span></a></li>
				<li class="fl"><a class="fl" href=""><img src="<?php echo get_template_directory_uri(); ?>/images/catalogue-1.png" alt=""><span><i class="fa fa-eye fa-2x"></i></span></a></li>
				<li class="fl"><a class="fl" href=""><img src="<?php echo get_template_directory_uri(); ?>/images/catalogue-1.png" alt=""><span><i class="fa fa-eye fa-2x"></i></span></a></li>
				<li class="fl"><a class="fl" href=""><img src="<?php echo get_template_directory_uri(); ?>/images/catalogue-1.png" alt=""><span><i class="fa fa-eye fa-2x"></i></span></a></li>
				<li class="fl"><a class="fl" href=""><img src="<?php echo get_template_directory_uri(); ?>/images/catalogue-1.png" alt=""><span><i class="fa fa-eye fa-2x"></i></span></a></li>
				<li class="fl"><a class="fl" href=""><img src="<?php echo get_template_directory_uri(); ?>/images/catalogue-1.png" alt=""><span><i class="fa fa-eye fa-2x"></i></span></a></li>
			</ul>
		</div>
	</div>
</div>