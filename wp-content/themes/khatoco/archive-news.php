<?php get_header(); ?>
    <script type="text/javascript" src="<?php echo get_template_directory_uri().'/assets/js/news.js' ?>"></script>
    <section id="event-page">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h3 class="masthead">Tin Tức</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="group-category break-line">
                                <div class="item active" category-type="tin-khuyen-mai" data-toggle="tab" href="#tin-khuyen-mai-tab"
                                     aria-expanded="true"><span class="name">Tin Khuyến Mãi</span></div>
                                <div class="item" category-type="su-kien" data-toggle="tab" href="#su-kien-tab"
                                     aria-expanded="false"><span class="name"> Sự Kiện - Hoạt Động</span></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 tab-content">
                            <div id="tin-khuyen-mai-tab" class="tab-pane fade active in">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <img class="event-banner img-fluid"
                                             src="<?php echo wp_upload_dir()['baseurl'] . "/" . get_settings('banner-event-news') ?>"
                                             alt="">
                                    </div>
                                    <div class="col-xs-12">
                                        <p class="masthead">TIN KHUYẾN MÃI</p>
                                        <p class="short-description">Áo sơ mi nam – Tỏa sáng phong thái – tự tin truyền
                                            đạt Ra…</p>
                                        <div class="break-line">
                                        </div>
                                        <div class="row" id="list-tin-khuyen-mai-render">
                                            <?php $eventList = display_post_with_filter('tin-khuyen-mai',0,'news') ?>
<!--                                            --><?php //$eventList = display_news_with_filter('su-kien') ?>
                                            <?php foreach($eventList as $event){ ?>
                                                <div class="col-xs-12 col-sm-6 col-lg-4">
                                                    <div class="group-event">
                                                        <div class="image-side">
                                                            <!--                                <img class="img-fluid" src="--><?php //echo $event['feature_image'] ?><!--" alt="--><?php //echo $event['title'] ?><!--">-->
                                                            <img class="img-fluid" src="<?php echo get_template_directory_uri()."/images/new/test-image.png" ?>" alt="<?php echo $event['title'] ?>">
                                                        </div>
                                                        <div class="content-side">
                                                            <a href="<?php echo $event['link'] ?>">
                                                                <p class="name"><?php echo $event['title'] ?></p>
                                                            </a>
                                                            <p class="description"><?php echo $event['short_description'] ?></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 text-center">
                                                <a href="javascript:void(0);" data-render="list-tin-khuyen-mai-render" class="all-event-cta" data-offset="9" data-type="tin-khuyen-mai" data-post="news">XEM THÊM</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="su-kien-tab" class="tab-pane fade">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <img class="event-banner img-fluid"
                                             src="<?php echo wp_upload_dir()['baseurl'] . "/" . get_settings('banner-event-event-activities') ?>"
                                             alt="">
                                        <p class="masthead">SỰ KIỆN - HOẠT ĐỘNG</p>
                                        <p class="short-description">Áo sơ mi nam – Tỏa sáng phong thái – tự tin truyền
                                            đạt Ra…</p>
                                        <div class="break-line"></div>
                                        <div class="row" id="list-su-kien-render"></div>
                                        <div class="row">
                                            <div class="col-xs-12 text-center">
                                                <a href="javascript:void(0);" class="all-event-cta" data-render="list-su-kien-render" data-offset="0" data-type="su-kien" data-post="news">XEM THÊM</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </section>
<?php echo get_template_part('partials/event-banner'); ?>

<?php get_footer(); ?>