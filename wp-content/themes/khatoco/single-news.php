<?php get_header();
$post = getSingleEvent();
$permalink = get_permalink($post['id']); ?>
<section id="event-detail">
    <div class="container">
        <?php
        $link = get_permalink(get_page_by_title( 'Tin Tức' ) );
        _get_template_part('partials/breadcrumb-render', null, ['data'=>[
                'category'=>['name'=>'Tin Tức','link'=>$link],
                'subCategory'=>['name'=>$post[0]['category_name'],'link'=>'#'],
                'currentPage'=>['name'=>$post[0]['title'],'link'=>$permalink]
        ]]);
        ?>
        <div class="row">
            <div class="col-xs-12 col-lg-8">
                <h1 class="masthead"><?php echo $post[0]['title'] ?></h1>
                <div class="row">
                    <div class="col-xs-12">
                    <div class="content">
                        <?php echo $post[0]['content'] ?>
                    </div>
                    <div class="break-line"></div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-lg-4">

                <?php
                $relatedNews = get_related_news($post[0]['slug']);
                _get_template_part('partials/related-post', null, ['post' => $relatedNews]);
                ?>
            </div>
        </div>
    </div>
</section>

<?php echo get_template_part('partials/event-banner'); ?>
<?php get_footer(); ?>