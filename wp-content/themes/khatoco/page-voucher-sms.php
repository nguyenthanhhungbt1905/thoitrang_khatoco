<?php
/*
Template Name: Voucher landing page
*/?>
<?php get_header() ?>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/dncss/vendors/bootstrap/bootstrap.min.css" crossorigin="anonymous">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/dncss/css/style.css" crossorigin="anonymous">
<!-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous"> -->
<style type="text/css">
    #footer{
        margin-top: 0!important;
    }
    .no-wrap {
      white-space: nowrap;
    }
    .card-gold > .img-product,
    .card-silver > .img-product,
    .card-dimond > .img-product {
      margin: 0 auto;
    }
    .tmg-ngay {
      display: block;
      clear: both;
      width: 100%;
      margin-bottom: 20px;
    }
    .promo .mt-0 {
      margin-top: 0;
    }
    .text-content-mid {
      font-size: 55px;
      font-family: sans-serif;
    }
    .text-content-campaign {
      font-size: 45px;
    }
    .content {
      margin-top: 110px;

    }
    .content-campaign {
        padding-left: 264px;
        margin: 10px;
    }
    .popup .popup-inner-custom {
        position: relative;
        z-index: 9999;
        padding: 3%;
        background-color: #fff;
        max-width: 400px;
        width: 80%;
        margin: 11% auto;
    }
    @media screen and (max-width: 768px) {
      .content-campaign {
        padding-left: 0;
      }
      .text-35 {
        max-width: 100%;
      }
      .promo {
        padding: 0 10px;
      }
      .input-number {
        padding-right: 0;
      }
      .padding-0 {
        padding: 0;
      }
      .content {
        margin-top: 0;
      }
      .ml-5 {
        margin-left: 0 !important;
      }
      .popup .popup-inner-custom {
          position: relative;
          z-index: 9999;
          padding: 9%;
          background-color: #fff;
          max-width: 400px;
          width: 90%;
          margin: 37% auto;
      }
      .popup .popup-inner-custom .popup-message p{
            margin: 2px 0;
            vertical-align: middle;
            padding: 5px;
        }
        .card-dimond > .img-product {
            height: 119px;
            /*width: auto;*/
        }
        .btn-custom-confirm-sms{
            width: 300px;
        }
        #title-text{
              font-size: 23px;
        }
    }
</style>
<body>
    <div class="header-content pb-5" style="background-color: white;">
        <div class="banner container">
            <div class="content row mt-5 promo">
                <div class="col-md-4">
                    <div class="row"><div class="col-md-6" style="border-top:2px solid white; margin-right: 50%;"></div></div>
                    <div class="row pt-3">
                        <h3>ĐĂNG KÝ LIỀN TAY</h3>
                    </div>
                    <div class="row pb-3">
                        <h3 style="text-align: right; display: block; width: 100%;">NHẬN NGAY ƯU ĐÃI</h3>
                    </div>
                    <div class="row"><div class="offset-6 col-md-6" style="border-top:2px solid white"></div></div>
                    <div class="row pt-5">
                        <p>
                            Nhanh tay Đăng ký Số Điện Thoại của Bạn để được nhận ngay tin nhắn chứa mã giảm giá 35% khi mua hàng trên website thoitrangkhatoco.vn hoặc Showroom KHATOCO
                        </p>
                        <p>
                          Nếu bạn đã là <span class="text-org">Khách Hàng Thân Thiết (Hạng Thẻ BẠC, VÀNG, KIM CƯƠNG)</span> khi đăng ký tham gia chương trình sẽ nhận được <span class="text-org">10 Voucher giảm 35% và Phiếu Quà tặng</span> tương ứng với từng Hạng thẻ Bên Dưới</span> được gửi qua đường bưu điện (EMS)
                        </p>
                        <p>
                            Thời hạn chương trình: đến hết 31/8/2018
                        </p>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="row content text-center">
                        <div class="col-xs-12 col-sm-6 col-lg-6" id="bound-title-text" style="padding-right: 5px;">
                            <span class="text-org text-uppercase float-right  p-1 padding-0" id="title-text">Nhập Số điện thoại:</span>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-lg-4 input-number" style="padding-left: 0;">
                            <div class="form-group">
                                <input type="number" name="cellphone" id="cellphone" class="form-control">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-lg-2 padding-0">
                            <div class="form-group">
                                <button type="button" id="get_voucher" class="btn btn-custom  text-uppercase  btn-custom-confirm-sms float-left">Tiếp tục</button>
                            </div>
                        </div>
                        <div class="text-35">
                          <div class="row content-campaign">
                              <div class="">
                                  <h3 class="text-uppercase text-black text-content-campaign"><b>Nhận mã giảm giá</b></h3>
                              </div>
                          </div>
                          <div class="row"><div class="offset-4 col-md-2" style="border-top:2px solid #cd7229;margin-left: 420px;"></div></div>
                          <div class="row content-campaign">
                              <div class="">
                                  <h3 class="text-uppercase text-org text-content-mid ml-4"><b>35% & Đổi Quà</b></h3>
                              </div>
                          </div>
                          <div class="row"><div class="offset-3 col-md-2" style="border-top:2px solid #cd7229;margin-left: 487px;"></div></div>

                          <!--<div class="row content-campaign ml-2">
                              <div class="col-md-12">
                                  <h3 class="text-uppercase text-black text-content-bottom  float-left"><b>&</b> <b class="text-uppercase text-org text-content-bottom-sub">Đổi quà</b></h3>
                              </div>
                          </div>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container mt-4">
        <div class="row mt-3">
            <div class="card-gold col-md-4 col-lg-4 card-gold-bound text-center">

                <img class="img-product mt-4"  src="<?php echo get_template_directory_uri(); ?>/assets/landing-page/balo.png">
                <p class="text-uppercase mt-3 text-black" >Hạng Thẻ Kim Cương<br>(Balo đựng laptop)</p>
            </div>
            <div class="card-dimond col-md-4 col-lg-4 card-dimond-bound text-center">

                <img class="img-product mt-5" src="<?php echo get_template_directory_uri(); ?>/assets/landing-page/tuixach.png">
                <p class="text-uppercase mt-5 text-black" >Hạng Thẻ Vàng<br>(Túi đựng thể thao)</p>
            </div>
            <div class="card-silver col-md-4 col-lg-4 card-silver-bound text-center">

                <img class="img-product mt-4" src="<?php echo get_template_directory_uri(); ?>/assets/landing-page/khan.png">
                <p class="text-uppercase mt-5 text-black" >Hạng Thẻ Bạc<br>(Khăn tắm)</p>
            </div>
        </div>
    </div>
    <div class="footer-content mt-4">
        <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="text-center">ĐỊA CHỈ ÁP DỤNG VOUCHER VÀ ĐỔI QUÀ</h3>
                        <!-- <p class="pt-3"> -->
                            <p class="text-org">HỒ CHÍ MINH</p>
                            <p class="ml-5">- 259A Hai Bà Trưng,Phường 6, Quận 3</p>
                            <p class="ml-5">- 282 Quang Trung,Phường 10, Quận Gò Vấp </p>
                            <p class="ml-5">- 969 Cách Mạng Tháng 8, Phường 7, Quận Tân Bình</p>
                            <p class="ml-5">- 319 Cách Mạng Tháng 8, Phường 12, Quận 10</p>
                            <p class="ml-5">- 6M Nguyễn Ảnh Thủ, P.Trung Mỹ Tây, Quận 12</p>
                        <!-- </p> -->
                        <p>
                            <span class="text-org">CẦN THƠ</span>
                            <p class="ml-5">- 3A Mậu Thân, P. Xuân Khánh, Q. Ninh Kiều </p>
                            <p class="ml-5">- 18 Nguyễn Trãi, P. An Hội, Q.Ninh Kiều</p>
                        </p>
                        <p>
                            <span class="text-org">KHÁNH HÒA</span>
                            <p class="ml-5">- Số B4 Chưng cư Vĩnh Phước, Đường 2/4,TP. Nha Trang</p>
                            <p class="ml-5">- 80C Quang Trung, P. Lộc Thọ, TP. Nha Trang</p>
                            <p class="ml-5">- 221 Thống Nhất, P. Phương Sài, TP. Nha Trang</p>
                            <p class="ml-5">- TTTM Nha Trang Center, 20 Trần Phú, TP. Nha Trang</p>
                        </p>
                        <p>
                            <span class="text-org">BÌNH ĐỊNH</span>
                            <p class="ml-5">- 164 Nguyễn Thái Học, P. Ngô Mây , TP. Quy Nhơn</p>
                            <p class="ml-5">- 231 Lê Hồng Phong, P. Lê Hồng Phong TP.Quy Nhơn</p>
                        </p>
                        <p>
                            <span class="text-org">ĐỒNG NAI</span>
                            <p class="ml-5">- 1137 Phạm Văn Thuận (Đối diện ngã ba Máy Cưa), Khu phố 1, P. Tân Tiến, TP. Biên Hòa</p>
                        </p>
                            <span class="text-org">MUA ONLINE:</span>
                            <p class="ml-5">- <a href="http://thoitrangkhatoco.vn">http://thoitrangkhatoco.vn</a></p>
                    </div>
                </div>
        </div>
    </div>
   <div id="popup" class="popup close-wrap hidden">
        <div class="close-bg"></div>
        <div class="popup-inner-custom">
            <div id="popup-message" class="message-wrap row clearfix">
            </div>
            <a href="#close" class="close">x</a>
            <div class="button-close row clearfix" id="popup_close">
            </div>
        </div>
    </div>
</body>
<script>
    jQuery(document).ready(function($) {
        $('#get_voucher').click(function(event) {
            event.preventDefault();
            $.ajax({
                url: '<?php echo admin_url("admin-ajax.php") ?>?action=user_service_ajax&do_ajax=get_user_for_sms',
                type: 'GET',
                dataType: 'json',
                data: {
                    cellphone:$('#cellphone').val()
                },
            })
            .done(function(response) {
                if(response.success){
                    window.location.href = "<?php echo home_url()?>/dang-ky-tham-gia/"+$('#cellphone').val();
                }else{
                     document.getElementById('popup-message').innerHTML = '<p style="text-align: justify;" class="mb-3">' + response.message + '</p>';
                      document.getElementById('popup_close').innerHTML = '<p style="margin:auto;"><a class="btn btn-custom btn-custom-confirm btn-lg" href="http://thoitrangkhatoco.vn/tri-an-khach-hang-than-thiet-2018" id="link" class="button">OK</a></p>';
                     document.getElementById('popup').className = document.getElementById('popup').className.replace(/hidden/i, '');
                }
            });
        });
        if(window.innerWidth <= 768){
            jQuery('#title-text').removeClass('float-right');
            jQuery('#bound-title-text').addClass('mb-2');
        }
    });
</script>
<?php get_footer() ?>
