<?php get_header(); ?>
<div class="banner">
	<?php echo get_the_post_thumbnail( 362, 'full', array( 'nopin' => 'nopin' ) ); ?>
</div>
<div class="block-outer block-introduce-subpage">
<div class="container fluid clearfix">
	<div class="block-inner">
		<div class="block-content">
			<div class="col-md-3 col-xs-12">
				<div class="menu-subpage-outer">
				<ul class="menu-subpage">
					<?php aj_nav_menu('sidebar-jobs-menu'); ?>
				</ul>
				<div class="ad-block">
					<a href=""><img nopin="nopin" src="" alt=""></a>
				</div>
				</div>
			</div>
			<div class="col-md-9 col-xs-12">
				<div class="top-block clearfix">
					<div class="intro-header-section">
						<div class="intro-header-title"><?php single_term_title('', true); ?></div>
					</div>
				</div>
				<div class="main-page">
					<div class="tablist"><?php $term = get_query_var('term');
					$args = array('posts_per_page' => -1, 'post_type' => 'job-hr', 'job-hr-type' => $term);
					$q = new WP_Query($args);
					$order = 1;
					$class = 'col-xs-12 col-md-6 mobile-nopadding';
					if ($q->post_count == 1)
						$class = 'col-xs-12 col-md-12 mobile-nopadding';
					while($q->have_posts()):
						$q->the_post(); 
						$wordcount = countWords();?>
						<div class="<?php echo $class;?>" <?php if($order % 2 == 0) echo 'style="float:right;"'; ?>>
							<div class="qa-outer">
								<div class="qa-inner">
									<div class="qa-header">
										<div class="circle"><span class="number"<?php if($order > 9) echo 'style="padding:0 0 0 6px;"'; ?>><?php echo $order++; ?></span></div>
										<div class="title"><?php the_title(); ?></div>
									</div>
									<div class="qa-body the-content">
										<?php the_content(); ?>
									</div>
								</div>
							</div>
						</div>
					<?php endwhile; wp_reset_postdata(); ?></div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<?php get_footer(); ?>