<?php
get_header();
$term_slug = get_query_var('term');
$tax = get_query_var('taxonomy');
$term = get_term_by('slug', $term_slug, $tax);
$terms = get_terms( $tax, array( 'hide_empty' => 0 ) ); ?>
<style type="text/css">
    .block-style-guide .item-lists-holder .pagination{
        display: block;
    }
    .block-inner .item-inner .item-thumb img{
    	    /*height: 385px !important;*/
		    object-fit: cover;
		    object-position: center;
    }
    @media (max-width: 768px) {
        .block-inner .item-inner .item-thumb img{
            height: auto !important;
        }
    }
</style>
<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $(document).on('change', 'select#paginate', function () {
            var page = $(this).val()
            $.ajax({
                url: "<?=admin_url('admin-ajax.php');?>",
                type:"post",
                data: {
                    action: 'ajax_load_more_style_guide',
                    ajax_paged : page,
                    term_slug: '<?= $term_slug ?>',
                    tax : '<?= $tax ?>'
                },
                beforeSend: function () {

                },
                success: function (response) {
                    if(response.success){
                        $('.item-lists').children().remove()
                        $('.item-lists').append(response.data)
                    }
                }
            })
        })
    })
</script>
<div class="block-outer block-style-guide">
	<div class="block-inner container fluid clearfix">
		<div class="col-xs-12 top-widget">
			<ul class="left-widget list-style-none clearfix">
				<?php foreach ($terms as $tkey => $t) {
                                    $splitName = explode(" - ", $t->name);
					echo '<li '. ( $t->term_id == $term->term_id ? 'class="current-menu-item"':'' ) .'><a href="'. get_term_link( $t, $tax ) .'" title="'. $t->name .'">'. $splitName[0];
					if (array_key_exists(1, $splitName))
						echo '<span>'.$splitName[1].'</span></a></li>';
					else 
						echo '</a></li>';
				} ?>
			</ul>
		</div>
		<div class="col-xs-12 mobile-nopadding">
			<div class="banner"><?php $img_src = wp_get_attachment_image_src(get_single_taxonomy_meta('cat-description', $term->term_id, 'cat-banner-img'), 'full'); ?>
				<img nopin="nopin" src="<?php echo $img_src[0]; ?>" />
			</div>
			<div class="item-lists-holder">
				<div class="top-block">
					<h3><?php echo $term->name; ?></h3>
				</div>
				<div class="item-lists clearfix"><?php $paged = (get_query_var('paged')) ? absint(get_query_var('paged')) : 1;
				$args = array('posts_per_page' => 6, 'post_type' => 'style-guide', $tax => $term_slug, 'paged' =>$paged, 'post_status' => 'publish', 'orderby' => 'date',
                    'order' => 'DESC');
				$pc = new WP_Query($args);
				$i = 1; $j = 0;
				$p_counts = $pc->found_posts;
				while($pc->have_posts()):
					$pc->the_post();
					$excerpt = wp_trim_words(apply_filters('the_content', $post->post_content), 15); ?>
					<div class="item col-lg-4 col-sm-6 col-xs-6 mobile-nopadding"> <!-- item-animate -->
						<div class="item-outer clearfix">
							<div class="item-inner">
								<a class="item-thumb" href="<?php echo the_permalink(); ?>">
									<?php if(has_post_thumbnail()) the_post_thumbnail('full'); ?>
								</a>
								<div class="item-info">
									<h2 class="item-title"><a href="<?php echo the_permalink(); ?>"><?php the_title(); ?></a></h2>
									<p class="item-excerpt"><?php echo $excerpt; ?></p>
								</div>
							</div>
						</div>
					</div>
				<?php endwhile; ?>
				</div>
				<div class="pagination clearfix" <?= $pc->max_num_pages <= 1 ? 'hidden':'' ?> >
					<div class="grid5 fl total"><?php $p_counts = $pc->found_posts;
					$p_start = ($paged - 1)*12 + 1;
					$p_end = $paged*12;
					$p_end = ($p_end > $p_counts) ? $p_counts : ($paged*12); ?>
						<p><?php echo 'Từ '. $p_start .' đến '. $p_end .' (trong '. $p_counts .')'; ?></p>
					</div>
					<div class="grid7 fr paginate clearfix">
						<ul class="list-style-none fr"><?php $big = 999999999;
							$paginate_array = paginate_links(array(
								'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
								'format' => '/paged/%#%',
								'current' => $paged,
								'show_all' => true,
								'total' => $pc->max_num_pages,
								'prev_text' => '',
								'next_text' => '',
								'type' => 'array'
							));
							$pgd_array = array(
								'prev' => '',
								'all' => array(),
								'next' => '',
								'current' => 1
							);
							if(!empty($paginate_array)) 
								foreach($paginate_array as $p_key => $pgd) {
									if(strpos($pgd, 'current') !== false)
										$pgd_array['current'] = $p_key;
									else if(strpos($pgd, 'next') !== false)
										$pgd_array['next'] = $p_key;
									else if(strpos($pgd, 'prev') !== false)
										$pgd_array['prev'] = $p_key;
									if(strpos($pgd, 'page-numbers') !== false && $p_key != 0)
										array_push($pgd_array['all'], $p_key);
							} ?>
							<li class="fl"><a href="" class="prev page-numbers"></a></li>
							<li class="fl select-holder">
								<p>
									Trang <select class="dropkick_targetasdjaksd" name="paginate" id="paginate"><?php foreach($pgd_array['all'] as $p_key => $pgd) {
										echo '<option value="'. ($p_key + 1) .'">'. $pgd .'</option>';
									} ?></select> trong <?php echo $pc->max_num_pages; ?>
								</p>
							</li>
							<li class="fl"><a href="" class="next page-numbers"></a></li>
						</ul>
					</div>
				</div><?php wp_reset_postdata(); ?>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>