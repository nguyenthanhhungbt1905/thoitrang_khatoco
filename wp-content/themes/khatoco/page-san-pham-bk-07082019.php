<?php 
/*
Template Name: Page Sản phẩm
*/
get_header();

?>
<style type="text/css">
	.block-home-collection .item-inner{
		height: 335px;
	}

	.block-home-collection .item-inner .item-thumb img{
		object-fit: cover;
		object-position: top left;
	}

</style>
<div class="home-slide-holder">
	<div class="home-slide">
		<?php get_banner_at('trang-san-pham'); ?>
	</div>
</div>

<div class="block-outer block-product-cat">
	<div class="block-inner">
		<div class="container clearfix fluid">
			<div class="block-content product-cat-slide"><?php
			$args = array('number' => 6, 'parent' => 0, 'hide_empty' => 0, 'orderby' => 'id');
			$terms = get_categories($args);
			$i = 0;
			foreach ($terms as $key => $term):
				if($i < 6) {?>
				<div class="item">
					<div class="item-outer">
						<div class="item-inner">
							<div class="col-md-6 col-xs-6">
								<div class="block-title-outer"><h2 class="block-title block-title-inner condensed"><?php echo $term->name; ?></h2></div>
								<span class="item-description"><?php echo $term->description; ?></span>
								<a class="item-button" href="<?php echo get_category_link($term); ?>" id="button_category_<?php echo $term->term_id; ?>">Mua Hàng</a>
							</div>
							<div class="col-md-6 col-xs-6 nopadding">
								<?php 
									$sub_categories = get_categories( array( 'parent' => $term->term_id, 'hide_empty' => 0, 'orderby' => 'id', 'hierarchical' => true ) );
									if(!empty($sub_categories)):?>
									<ul class="menu-subproduct">
										<?php foreach($sub_categories as $sub_category):?>
										<li><a href="<?php echo get_category_link($sub_category); ?>" id="category_<?php echo $sub_category->term_id; ?>"><?php echo $sub_category->name;?></a></li>
										<?php endforeach;?>
									</ul>
								<?php endif;?>
								<a href="<?php echo get_category_link($term); ?>" id="category_<?php echo $term->term_id; ?>"><?php $img_src = wp_get_attachment_image_src(get_single_taxonomy_meta('product-cat-description', $term->term_id, 'product-cat-img'), 'full'); ?>
									<span class="item-right-thumb">
										<img nopin="nopin" src="<?php echo $img_src[0]; ?>" alt="">
									</span>	
								</a>
							</div>
						</div>
					</div>
				</div>
				<?php 
				} 
				$i++;
			endforeach; ?>
			</div>
		</div>
	</div>
</div>


<div class="block-outer block-home-collection">
	<div class="block-inner">
		<div class="containerpd clearfix">
			<div class="block-title-outer"><h2 class="block-title block-title-inner condensed">Bộ sưu tập</h2></div>
		</div>
		<div class="block-content container clearfix fluid">
		<?php 
			$args = array(
			    'post_type' 		=> 'style-guide',
			    'orderby'   		=> 'post_date',
			    'order'     		=> 'DESC',
			    'posts_per_page'	=> 3,
			    'post_status'					=> 'publish'
			    ); 
			$the_query = new WP_Query( $args );
			//$terms = get_terms('style-guide-category', array('hide_empty' => 0, 'number' => 3, 'orderby'    => 'id', 'order'      => 'desc',));

			if($the_query->have_posts() ):
				while ( $the_query->have_posts() ):
					$the_query->the_post();
			?>
			
			
			<div class="row-fluid">
				<div class="col-xs-12 col-md-4 nopadding">
					<div class="item-outer">
						<div class="item-inner">
							<a href="<?php echo get_permalink($post->ID); ?>">
								<span class="item-thumb">
									<?php 
									//$img_src = wp_get_attachment_image_src(get_single_taxonomy_meta('cat-description', $term->term_id, 'cat-bg-img'), 'full'); 
									$image =get_the_post_thumbnail($post->ID, 'thumbnail' );
									echo $image;
									?>
									<!--img src="" nopin="nopin" /-->
								</span>
								<h3 class="item-title condensed"><?php echo the_title(); ?></h3>
							</a>
						</div>
					</div>
				</div>
			</div>
			<?php
					endwhile;
				endif;
			?>
		</div>
	</div>
</div>
<?php get_footer(); ?>