<?php 
/*
Template Name: Page Đồng phục
*/
get_header();
if(isset($_POST['uni_submit'])) {
	// Đặt hàng
	$new_post = wp_insert_post(array(
		'post_title' => wp_strip_all_tags($_POST['uni_fullname']),
		'post_content' => wp_strip_all_tags($_POST['uni_content']),
		'post_type' => 'uniform_orders'
	));

	if(!is_wp_error($new_post)) {
		update_post_meta($new_post, 'uni_email', $_POST['uni_email']);
		update_post_meta($new_post, 'uni_phone', $_POST['uni_phone']);
		update_post_meta($new_post, 'uni_company', $_POST['uni_company']);
	}
} ?>
<div class="banner">
	<?php echo get_the_post_thumbnail( $post->ID, 'full', array( 'nopin' => 'nopin' ) ); ?>
</div>
<div class="block-outer block-products dongphuc">
	<div class="block-inner">
		<div class="block-content container clearfix top-products fluid">
				<div class="col-xs-6 fl item-outer">
                                    <h3 class="condensed">Đồng phục công sở</h3>
					<div class="item item-dongphuc">
						<img nopin="nopin" src="<?php echo get_template_directory_uri(); ?>/images/dong-phuc_01.jpg" alt="">
				</div>
				
			</div>
					<div class="col-xs-6 fl item-outer">
	                                    <h3 class="condensed">Đồng phục bảo hộ lao động</h3>
						<div class="item item-dongphuc">
							<img nopin="nopin" src="<?php echo get_template_directory_uri(); ?>/images/dong-phuc_02.jpg" alt="">
				</div>
				
			</div>
		</div>
	</div>
</div>
<?php /* ?>
<div class="block-outer block-uni-partners">
	<div class="block-title-outer"><h2 class="block-title">Nhiều năm kinh nghiệm sản xuất cho các thương hiệu tên tuổi</h2></div>
	<div class="block-content">
		<div class="clearfix">
			<p class="intro">Các doanh nghiệp, các đơn vị sản xuất kinh doanh trong <strong>nhiều lĩnh vực</strong> như: đóng tàu, cơ khí, cầu đường, in ấn, bao bì, dệt may...<br>Các đơn vị dịch vụ : bưu chính, ngân hàng, khách sạn, nhà hàng, công ty tổ chức sự kiện…</p>
			<p class="condensed"><strong>Một số khách hàng tiêu biểu của Khatoco:</strong></p>
		</div>
		<div class="container fluid clearfix">
			<div class="uni-partners-holder">
				<ul class="slides list-style-none">
					<li class="fl"><a class="fl" href=""><img nopin="nopin" src="<?php echo get_template_directory_uri(); ?>/images/dong-phuc_12.png" alt=""></a></li>
					<li class="fl"><a class="fl" href=""><img nopin="nopin" src="<?php echo get_template_directory_uri(); ?>/images/dong-phuc_13.png" alt=""></a></li>
					<li class="fl"><a class="fl" href=""><img nopin="nopin" src="<?php echo get_template_directory_uri(); ?>/images/dong-phuc_14.png" alt=""></a></li>
					<li class="fl"><a class="fl" href=""><img nopin="nopin" src="<?php echo get_template_directory_uri(); ?>/images/dong-phuc_15.png" alt=""></a></li>
					<li class="fl"><a class="fl" href=""><img nopin="nopin" src="<?php echo get_template_directory_uri(); ?>/images/dong-phuc_16.png" alt=""></a></li>
					<li class="fl"><a class="fl" href=""><img nopin="nopin" src="<?php echo get_template_directory_uri(); ?>/images/dong-phuc_17.png" alt=""></a></li>
					<li class="fl"><a class="fl" href=""><img nopin="nopin" src="<?php echo get_template_directory_uri(); ?>/images/dong-phuc_19.png" alt=""></a></li>
					<li class="fl"><a class="fl" href=""><img nopin="nopin" src="<?php echo get_template_directory_uri(); ?>/images/dong-phuc_20.png" alt=""></a></li>
				</ul>
			</div>
		</div>
	</div>
</div>
<?php */ ?>

<div class="clearfix dongphuc-service">
	<img src="<?php echo get_template_directory_uri(); ?>/images/hang-dong-phuc-service.png" class="hang-dong-phuc">
</div>

<div class="block-outer block-intrust">
	<div class="container fluid clearfix">
		<div class="col-md-6 col-xs-12">
			<div class="item big-item right">
				<h2>Vì sao chọn chúng tôi?</h2>
				<ul class="clearfix">
					<li>Giải pháp tư vấn trọn gói</li>
					<li>Thiết kế ấn tượng, chuyên nghiệp, có tính ứng dụng cao</li>
					<li>Chất liệu bền đẹp, thích hợp từng điều kiện công việc</li>
					<li>Đường may chắc chắn tinh tế</li>
					<li>Đáp ứng các tiêu chuẩn kỹ thuật khắt khe</li>
					<li>Thời gian nhanh chóng</li>
					<li>Giá cả cạnh tranh</li>
					<li>Dịch vụ uy tín, tận tâm.</li>
				</ul>
			</div>
		</div>
		<div class="col-md-6 col-xs-12">
			<div class="item nor-item-1 right left">
				<p>Mục tiêu của Khatoco mang đến những sản phẩm đồng phục cao cấp, có tính ứng dụng cao, mang hơi thở hiện đại, thể hiện sự chuyên nghiệp, bản sắc riêng, duy nhất phù hợp với văn hóa, tầm vóc thương hiệu của mỗi doanh nghiệp. Qua đó, hình ảnh thương hiệu của khách hàng được nhận diện tốt và giúp gia tăng giá trị.</p>
			</div>
		</div>
	</div>
</div>

<div class="block-outer block-product-trainer">
	<div class="block-content container fluid clearfix">
		<div class="tablist-multilvl clearfix">
			<?php display_uniforms_tab(); ?>
			<a href="javascript:;" class="button-download-catalogue">Catalogue</a>
			<ul class="catalogue-preview hidden clearfix"><?php $catalogue = get_option('catalogue');
				$catalogue_arr = explode(',', $catalogue);
				if(!empty($catalogue_arr) && $catalogue_arr[0] != ''): foreach ($catalogue_arr as $ckey => $c):
					$img = wp_get_attachment_image_src( $c, 'large' ); ?>
					<li><a class="fancybox" rel="group" href="<?php echo $img[0]; ?>"><img src="<?php echo $img[0]; ?>" alt=""></a></li>
				<?php endforeach;
				endif; ?>
			</ul>
		</div>
	</div>
</div>

<div class="block-outer block-uni-orders">
	<div class="container fluid clearfix">
		<!-- <p class="intro">Bạn muốn đặt hàng liên hệ Phòng kinh doanh dệt may: <strong>(+84 58) 3889978</strong><br>Hoặc để lại thông tin của bạn cho chúng tôi chúng tôi sẽ liên hệ đến bạn sớm nhất!</p> -->
		<div class="col-md-4 col-xs-12 mobile-nopadding">
			<h2 class="block-title condensed">Khách hàng may đồng phục</h2>
			<ul class="lsn fluid clearfix">
				<li class="grid3"><img src="<?php echo get_template_directory_uri(); ?>/images/partner-1.png" alt=""></li>
				<li class="grid3"><img src="<?php echo get_template_directory_uri(); ?>/images/partner-2.png" alt=""></li>
				<li class="grid3"><img src="<?php echo get_template_directory_uri(); ?>/images/partner-3.png" alt=""></li>
				<li class="grid3"><img src="<?php echo get_template_directory_uri(); ?>/images/partner-4.png" alt=""></li>
				<li class="grid3"><img src="<?php echo get_template_directory_uri(); ?>/images/partner-5.png" alt=""></li>
				<li class="grid3"><img src="<?php echo get_template_directory_uri(); ?>/images/partner-6.png" alt=""></li>
				<li class="grid3"><img src="<?php echo get_template_directory_uri(); ?>/images/partner-7.png" alt=""></li>
				<li class="grid3"><img src="<?php echo get_template_directory_uri(); ?>/images/partner-8.png" alt=""></li>
				<li class="grid3"><img src="<?php echo get_template_directory_uri(); ?>/images/partner-9.png" alt=""></li>
				<li class="grid3"><img src="<?php echo get_template_directory_uri(); ?>/images/partner-10.png" alt=""></li>
				<li class="grid3"><img src="<?php echo get_template_directory_uri(); ?>/images/partner-11.png" alt=""></li>
				<p class="grid12">và với hơn <strong>26 đơn vị</strong> thành viên<br>thuộc TCT Khánh Việt</p>
			</ul>
		</div>
		<?php if(isset($_SESSION['uni-orders']) && $_SESSION['uni_orders']):
			echo '<div class="grid8 clearfix"><p style="text-align: center">Cám ơn bạn đã tin tưởng Khatoco. Chúng tôi sẽ xem xét và gửi câu trả lời cho bạn vào thời gian sớm nhất!</p></div>';
		else: ?>
		<form method="POST" class="uni-form col-md-8 col-xs-12 clearfix">
			<h2 class="block-title condensed">Đặt hàng</h2>
			<div class="inner clearfix">
				<div class="col-md-6 col-xs-12 clearfix">
					<p><input name="uni_fullname" type="text" placeholder="Họ và tên (*)" required></p>
					<p><input name="uni_email" type="email" placeholder="Email (*)" required></p>
					<p><input name="uni_phone" type="text" placeholder="Số điện thoại (*)" required></p>
				</div>
				<div class="col-md-6 col-xs-12 clearfix">
					<p><input name="uni_company" type="text" placeholder="Công ty (*)" required></p>
					<textarea name="uni_content" placeholder="Nội dung cần liên hệ"></textarea>
				</div>
				<div class="grid12">
					<p><span class="fl">(*) thông tin bắt buộc</span><input type="submit" class="submit_btns" name="uni_submit" value="Gửi"></p>
				</div>
			</div>
		</form><?php $_SESSION['uni_orders'] = false;
	endif; ?>
	</div>
</div>

<?php get_footer(); ?>