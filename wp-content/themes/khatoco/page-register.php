<?php
$user_info = wp_get_current_user();
if($user_info->ID > 0){
    wp_redirect(home_url('/'));
    exit;
}

/*
Template Name: Page Register
*/
get_header(); ?>

<div data-ng-app="khatocoUserProfile" class="block-outer block-account">
	<div class="block-inner">
    	<div data-ng-controller="UserController" class="container fluid">
	        <div class="col-xs-12">
	        	<div class="block-title-outer"><h2 class="block-title block-title-inner condensed">Đăng ký</h2></div>
	        	<p class="description">Thông tin cá nhân Quý khách nhập dưới đây có thể được sử dụng cho chương trình Thẻ thành viên Khatoco. 
	        	Để biết thêm thông tin về chương trình, vui lòng xem thêm <a href="<?php echo get_page_link(1057); ?>" target="blank">tại đây</a>.</p>
	        	<p class="clearfix">(<span class="required">*</span>) Thông tin bắt buộc</p>
	        </div>
            <form name="registerForm" method="POST" data-ng-submit="formSubmit()">
                <div class="col-md-6 col-xs-12">
	                <h2>Thông tin tài khoản</h2>
	                <p class="clearfix">
	                	<label for="phone">Số điện thoại <span class="required">*</span></label>	                    
	                    <input class="field" data-ng-model="datapost.phone" value="" type="text" pattern="\d*" placeholder="Điện thoại | (09xxxxxxxx) hoặc (01xxxxxxxxx)" required />
	                </p>	                
	                <p class="clearfix">
	                	<label for="fullname">Họ và tên <span class="required">*</span></label>
	                    <input class="field" data-ng-model="datapost.fullname" value="" type="text" placeholder="Họ và Tên"required />
	                </p>
	                <p class="clearfix">
	                	<label for="email">Email <span class="required">*</span></label>
	                    <input class="field" data-ng-model="datapost.email" value="" placeholder="Email"required />
	                </p>
	                <label for="birthday">Ngày sinh <span class="required">*</span></label>	
	                <p class="birthday">	                    
	                    <select name="" data-ng-model="datapost.birthday.year" required>
	                        <option value="">Năm</option>
	                        <option data-ng-repeat="year in years" data-ng-selected="datapost.birthday.year==year" value="{{year}}">{{year}}</option>
	                    </select>
                    </p>
					<p class="birthday">	                    
	                    <select name="" data-ng-model="datapost.birthday.month" required>
	                        <option value="">Tháng</option>
	                        <option data-ng-repeat="month in months" data-ng-selected="datapost.birthday.month==month" value="{{month}}">{{month}}</option>
	                    </select>
                    </p>
                    <p class="birthday last">
	                    <select name="" data-ng-model="datapost.birthday.day" required>
	                        <option value="">Ngày</option>
	                        <option data-ng-repeat="day in days" data-ng-selected="datapost.birthday.day==day" value="{{day}}">{{day}}</option>
	                    </select>
	                </p>
	                <p class="clearfix">
	                	<label for="gender">Giới tính <span class="required">*</span></label>
	                    <select data-ng-model="datapost.gender" data-ng-options="gen as gen.name for gen in genders" required>
	                        <option value="" selected>Giới tính</option>
	                    </select>
	                </p>
                </div>
                <div class="col-md-6 col-xs-12">
                	<h2>Thiết lập mật khẩu</h2>
	                <p class="clearfix">
	                	<label for="password">Mật khẩu <span class="required">*</span></label>
	                    <input class="field" data-ng-model="datapost.password" value="" type="password" placeholder="Mật khẩu" required/>
	                </p>
	                <p class="clearfix">	                    
	                	<label for="repassword">Nhập lại mật khẩu <span class="required">*</span></label>
	                    <input class="field" data-ng-model="datapost.repassword" value="" type="password" placeholder="Xác nhận mật khẩu" required/>
	                </p>                	
	                <h2 class="sub-title">Địa chỉ nhận hàng</h2>
	                <p class="birthday">
	                	<label for="province">Tỉnh/TP</label>
	                    <select data-ng-model="datapost.province" data-ng-options="province as province.name for province in provinces" data-ng-change="setCurrentProvince()" name="province" id="province" class="address-select">
	                        <option value="" selected>-- Lựa chọn --</option>
	                    </select>
	                </p>
	                <p class="birthday" data-ng-show="datapost.province.provinceid">	                	
	                	<label for="district" data-ng-show="datapost.province.provinceid">Quận/Huyện</label>
	                    <select data-ng-model="datapost.district" data-ng-options="district as district.name for district in districts" data-ng-change="setCurrentDistrict()" name="district" id="district" class="address-select">
	                        <option value="" selected>-- Lựa chọn --</option>
	                    </select>
	                </p>
	                <p class="birthday last" data-ng-show="datapost.district.districtid && datapost.province.provinceid">	                    
	                    <label for="ward" data-ng-show="datapost.district.districtid && datapost.province.provinceid">Phường/Xã</label>
	                    <select data-ng-model="datapost.ward" data-ng-options="ward as ward.name for ward in wards" data-ng-change="setCurrentWard()" name="ward" id="ward" class="address-select">
	                        <option value="" selected>-- Lựa chọn --</option>
	                    </select>
	                </p>
	                <p class="clearfix">
	                    <label for="addr">Tên đường, số nhà</label>
	                    <input class="field" data-ng-model="datapost.address" value="" type="text" placeholder="Tên đường, số nhà"/>
	                </p>
	                <p class="clearfix">
	                    <label for="checkbox_required">
	                        <input style="width: auto; float: none; vertical-align: middle;" type="checkbox" data-ng-model="datapost.required" name="checkbox_required" id="checkbox_required" value="on"> Tôi đồng ý với các điều khoản quy định của Thời trang Khatoco.
	                    </label>
	                </p>
	
	                <p class="clearfix">
                		<label class="col-md-8 col-xs-12 nopadding">
                			Mã kiểm tra
	                        <input class="field" data-ng-model="datapost.captcha"  placeholder="Mã kiểm tra ở hình bên" type="text" name="captcha_code" id="captcha_code" size="10" maxlength="4" />
	                        <a class="captcha" href="javascript:;" id="click-captcha" onclick="document.getElementById('captcha_code').value='';document.getElementById('captcha').src = '<?php echo esc_url( home_url( '/wp-includes/captcha/' ) );?>securimage_show.php?' + Math.random(); return false"><i class="fa fa-refresh"></i></a>
	                    </label>
						<span class="col-md-4 col-xs-12 mobile-nopadding">
							<img id="captcha" src="<?php echo esc_url( home_url( '/wp-includes/captcha/' ) );?>securimage_show.php" alt="CAPTCHA Image" />							
						</span>
	                </p>
	
	                <p class="clearfix">	                    
	                    <button class="button" type="submit">
	                        <span data-ng-hide="loading">Đăng ký</span>
	                        <span data-ng-show="loading">Đăng ký <i class="fa fa-refresh fa-spin"></i></span>
	                    </button>
	                </p>
                </div>
            </form>
        </div>
    </div>
</div>

<div id="popup" class="popup close-wrap hidden">
	<div class="close-bg"></div>
    <div class="popup-inner">
        <div id="popup-message" class="message-wrap row clearfix">
        </div>
        <a href="#close" class="close">x</a>
        <div class="button-close row clearfix">
			<p><a href="javascript:;" id="link" class="button">Đóng lại</a></p>
		</div>
    </div>
</div>

<script type="text/javascript">
    var apiUrl = "<?php echo admin_url('admin-ajax.php'); ?>";
    var app = angular.module("khatocoUserProfile", []);

    app.service('userService', ['$http', '$q', '$timeout', function($http, $q, $timeout){

        var getAddressInfo = function(action, id){
            return $http({
                method: 'GET',
                url: apiUrl,
                params: {action: 'user_service_ajax', do_ajax: action, id: id}
            });
        };

        var saveUserInfo = function(data){
            var d = $q.defer();

            $timeout(function () {
                $http({
                    method: 'POST',
                    url: apiUrl,
                    params: {action: 'user_service_ajax', do_ajax: 'register_user_info'},
                    data: data,
                    //headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'}
                }).success(function(data, status, headers){
                    d.resolve(data);
                }).error(function(data, status, headers){
                    d.reject(data);
                });
            }, 1000);

            return d.promise;
        };

        return {
            getAddressInfo: getAddressInfo,
            saveUserInfo: saveUserInfo
        };
    }]);

    app.controller('UserController', ['$scope', '$sce', 'userService', function($scope, $sce, userService){
        $scope.submitMessage = '';
        $scope.loading = false;

        $scope.years = [2010,2009,2008,2007,2006,2005,2004,2003,2002,2001,2000,1999,1998,1997,1996,1995,1994,1993,1992,1991,1990,1989,1988,1987,1986,1985,1984,1983,1982,1981,1980,1979,1978,1977,1976,1975,1974,1973,1972,1971,1970,1969,1968,1967,1966,1965,1964,1963,1962,1961,1960,1959,1958,1957,1956,1955,1954,1953,1952,1951,1950,1949,1948,1947,1946,1945,1944,1943,1942,1941,1940,1939,1938,1937,1936,1935,1934,1933,1932,1931,1930];
        $scope.months = [1,2,3,4,5,6,7,8,9,10,11,12];
        $scope.days = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31];

        $scope.wards = [];
        $scope.districts = [];
        $scope.provinces = [];
        $scope.datapost = {};

        $scope.genders = [{id: 'male', name: 'Nam'}, {id: 'female', name: 'Nữ'}];

        $scope.datapost.province = {};
        $scope.datapost.district = {};
        $scope.datapost.ward = {};
        $scope.datapost.gender = {};
        $scope.datapost.birthday = {};
        $scope.datapost.address = '';
        $scope.datapost.phone = '';
        $scope.datapost.nonce = "<?php echo wp_create_nonce('register_nonce_action'); ?>";
        $scope.datapost.redirect = "<?php echo home_url('/'); ?>";

        $scope.userInfo = {};
        $scope.phoneMessage = '';

        $scope.datapost.captcha = '';

        $scope.error = false;

        $scope.getWards = function(districtid){
            userService.getAddressInfo('get_wards', districtid)
                .success(function(data, status, headers){
                    if(data.success == 'true'){
                        $scope.wards = $scope.objectToArray(data.result);
                        if(data.current){
                            $scope.datapost.ward = data.result[data.current]
                        }
                    }
                })
                .error(function(data, status, headers){
                    $scope.error = true;
                });
        };

        $scope.getDistricts = function(provinceid){
            userService.getAddressInfo('get_districts', provinceid)
                .success(function(data, status, headers){ //console.log(data);
                    if(data.success == 'true'){
                        $scope.districts = $scope.objectToArray(data.result);
                        if(data.current){
                            $scope.datapost.district = data.result[data.current];
                            $scope.getWards(data.current);
                        }
                    }
                })
                .error(function(data, status, headers){
                    $scope.error = true;
                });
        };

        $scope.getProvinces = function(){
            userService.getAddressInfo('get_provinces', 0)
                .success(function(data, status, headers){
                    if(data.success == 'true'){
                        $scope.provinces = $scope.objectToArray(data.result);
                        if(data.current){
                            $scope.datapost.province = data.result[data.current];
                            $scope.getDistricts(data.current);
                        }
                    }
                })
                .error(function(data, status, headers){
                    $scope.error = true;
                });
        };

        $scope.getProvinces();

        if($scope.error == true){
            alert('Không thể kết nối đến server, vui lòng F5 và thử lại!'); return;
        }

        $scope.formSubmit = function(){
            if(!$scope.datapost.required){
                alert('Bạn phải đồng ý với các điều khoản quy định của Thời trang Khatoco để có thể đăng ký tài khoản!');
                return false;
            }

            $scope.loading = true;
            $scope.submitMessage = '';

            /*console.log( $scope.datapost );
             return false;*/

            userService.saveUserInfo($scope.datapost)
                .then(function(data){
                    if( data.message ){
                        // $scope.submitMessage = data.message;
                        document.getElementById('popup-message').innerHTML = '<p>'+ data.message +'</p>';
                        document.getElementById('popup').className = document.getElementById('popup').className.replace(/hidden/i, '');
                        document.getElementById('captcha').src = '<?php echo esc_url( home_url( '/wp-includes/captcha/' ) );?>securimage_show.php?' + Math.random();
                    }
                    if( data.result == 1  && data.redirect){
                        var link = document.getElementById('link');
                        link.setAttribute('href', data.redirect);
                        link.className = link.className.replace(/close/i, '');
                        link.innerHTML = 'Đăng nhập';
                        link.addEventListener( 'click', function(e){
                            document.location = data.redirect;
                        });
                    }
                    $scope.loading = false;
                    // document.location.href = '<?php echo home_url("/"); ?>';

                }, function(err){
                    $scope.submitMessage = 'Không thể kết nối đến máy chủ, F5 để thử lại.';
                    $scope.loading = false;
                });
        };

        $scope.setCurrentProvince = function(){
            $scope.getDistricts($scope.datapost.province.provinceid);
        };

        $scope.setCurrentDistrict = function(){
            $scope.getWards($scope.datapost.district.districtid);
        };

        $scope.objectToArray = function(object){
            var result = [];
            for(ob in object){
                result.push(object[ob]);
            }
            return result;
        };
    }]);
</script>

<?php get_footer(); ?>
