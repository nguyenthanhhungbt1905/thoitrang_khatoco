<?php get_header();
global $post;
$post_id = $post->ID;
$permalink = get_permalink($post_id); ?>
<div class="block-outer block-news">
	<div class="block-inner container fluid clearfix">
		<div class="col-md-8 col-xs-12 block-news-inner">
			<div class="top-block clearfix">
				<div class="breadcrumb fl">
					<?php blix_breadcrumbs(); ?>
				</div>
			</div>
			<div class="block-content">
				<h2><?php echo get_the_title($post_id); ?></h2>
				<p><span class="comments-preview"><i class="fa fa-comment"></i> <fb:comments-count href="<?php echo $permalink; ?>"></fb:comments-count></span> | <span class="date"><i class="fa fa-calendar"></i>  <?php echo get_the_date(); ?></span></p>
				<div class="the-content">
				<?php echo apply_filters('the_content', $post->post_content); ?>
				</div>
			</div>
			<div class="block-comments">
				<div class="fb-comments" data-href="<?php echo $permalink; ?>" data-width="100%" data-numposts="5" data-colorscheme="light"></div>
			</div>
			
		</div>

		<div class="col-md-4 block-news-relative">
			<div class="title-outer"><h2 class="title title-inner condensed">Tin tức khác</h2></div>
			<ul class="clearfix"><?php $term = get_the_terms($post_id, 'news_category');
			$args = array('posts_per_page' => 7, 'post_type' => $post->post_type, 'post__not_in' => array($post_id), 'orderby' => 'date', 'order' => 'DESC');
			if($term) {
				$term = array_values($term);
				$term = $term[0];
				$args['news_category'] = $term->slug;
			}
			$relative_posts = new WP_Query($args);
			while($relative_posts->have_posts()): $relative_posts->the_post(); ?>
				<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a> <span class="date">(<?php echo get_the_date(); ?>)</span></li><?php endwhile;
			wp_reset_postdata(); ?>
			</ul>
		</div>
	</div>
</div>

<script>
jQuery(document).ready(function(){
	var wrap = jQuery(".block-news .block-news-relative");
	jQuery(window).on("scroll", function(e) {
		if (jQuery(this).outerWidth() > 720)
			fixedScroll();
		else {
			wrap.removeClass("fix-top");
			wrap.attr('style','');
		}
	});

	function fixedScroll(){
		wrap.removeClass("fix-top");
		wrap.attr('style','');
		var left = wrap.position().left;
		var width = wrap.outerWidth();
		var height = wrap.outerHeight();
		var scrollTop = jQuery('body').scrollTop();

		if (scrollTop > wrap.position().top) {
			var wrapHeight = jQuery('.block-outer.block-news').outerHeight();
			if (scrollTop + height > wrapHeight){
				wrap.css({'position': 'fixed','top':-(scrollTop + height - wrapHeight),'left':left,'width': width});
			} else {
				wrap.css({'left':left,'width': width});
				wrap.addClass("fix-top");
			}
		} else {
			wrap.removeClass("fix-top");
		}
	}
});
</script>
<style>.block-news-relative.fix-top {top: 0;position: fixed;}</style>
<?php get_footer(); ?>