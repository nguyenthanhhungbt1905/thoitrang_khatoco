<?php 
/*
Template Name: Gửi hồ sơ
*/

if(isset($_POST['send-cv-sbmt'])) {
	require_once(ABSPATH . 'wp-admin/includes/admin.php'); 

	/* Setup new apply job post */
	$new_apply_job = array(
		'post_type' => 'job_apply',
		'post_title' => $_POST['cv_name'],
		'post_status' => 'publish'
	);
	$new_apply_id = wp_insert_post($new_apply_job);

	/* Update post meta */
	add_post_meta($new_apply_id, 'apply_position', $_POST['cv_position']);
	add_post_meta($new_apply_id, 'apply_name', $_POST['cv_name']);
	add_post_meta($new_apply_id, 'apply_phone', $_POST['cv_phone']);
	add_post_meta($new_apply_id, 'apply_note', $_POST['cv_note']);

	/* Upload CV */
	if($_FILES['cv_file']) {
		$cv_attach_id = media_handle_upload('cv_file', $new_apply_id);

		if(!is_wp_error($cv_attach_id)) {
			add_post_meta($new_apply_id, 'apply_cv', $cv_attach_id);
		}
	}

	$_SESSION['job_apply'] = 'ok';
	
	wp_redirect(get_permalink());
	exit();
}

get_header(); ?>
<div class="banner">
	<img src="<?php echo get_template_directory_uri(); ?>/images/banner-tuyendung.png" alt="">
</div>
<div class="block-outer block-applyjob">
	<div class="block-inner">
		<div class="block-content container clearfix top-products fluid">
			<div class="grid3">
				<ul class="left-widget list-style-none">
					<?php aj_nav_menu('sidebar-jobs-menu'); ?>
				</ul>
				<div class="ad-block">
					<a href=""><img src="" alt=""></a>
				</div>
			</div>
			<div class="grid9">
				<div class="top-block clearfix">
					<ul class="breadcrumb list-style-none fl">
						<li class="fl"><a href=""></a></li>
						<li class="fl"><a href="">Giới thiệu</a></li>
						<li class="fl"><a href="">Gửi hồ sơ</a></li>
					</ul>
				</div><?php if(isset($_SESSION['job_apply']) && $_SESSION['job_apply']): ?>
				<div class="main-page success">
					<p>Cám ơn bạn đã ứng tuyển tại Khatoco! Chúng tôi sẽ xem xét hồ sơ ứng tuyển và trả lời trong thời gian sớm nhất!</p>
				</div><?php else: ?>
				<div class="main-page">
					<h2>Hồ sơ của bạn</h2>
					<form method="post" class="clearfix" enctype="multipart/form-data">
						<div class="row"><label for="cv_name">Họ và tên</label><input type="text" id="cv_name" name="cv_name"></div>
						<div class="row"><label for="cv_phone">Số điện thoại</label><input type="text" id="cv_phone" name="cv_phone"></div>
						<div class="row"><label for="cv_position">Vị trí ứng tuyển</label><input type="text" id="cv_position" name="cv_position"></div>
						<div class="row"><label for="cv_file">Đính kèm CV</label>
							<div class="file_input_holder">
								<input type="file" id="cv_file" name="cv_file" for="doc,docx,pdf">
								<span class="file_input_holder_inner"></span>
							</div>
						</div>
						<div class="row"><label for="cv_note">Lời ngỏ</label><textarea name="cv_note" id="cv_note" rows="6"></textarea></div>
						<div class="row"><label for="">&nbsp;</label><input type="submit" class="btns" name="send-cv-sbmt" value="Gửi"></div>
					</form>
				</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>