<?php 
/*
Template Name: Vị trí tuyển dụng
*/
get_header(); ?>
<div class="banner">
	<img src="<?php echo get_template_directory_uri(); ?>/images/banner-tuyendung.png" alt="">
</div>
<div class="block-outer block-applyjob">
	<div class="block-inner">
		<div class="block-content container clearfix top-products fluid">
			<div class="grid3">
				<ul class="left-widget list-style-none">
					<?php aj_nav_menu('sidebar-jobs-menu'); ?>
				</ul>
				<div class="ad-block">
					<a href=""><img src="" alt=""></a>
				</div>
			</div>
			<div class="grid9">
				<div class="top-block clearfix">
					<ul class="breadcrumb list-style-none fl">
						<li class="fl"><a href=""></a></li>
						<li class="fl"><a href="">Giới thiệu</a></li>
						<li class="fl"><a href="">Vị trí tuyển dụng</a></li>
					</ul>
				</div>
				<div class="main-page">
					<h2 class="main-title">Web Designer</h2>
					<div class="main-content the-content">
						<h2>Job Description</h2>
						<ul>
							<li>- Over 3 years experience at website designer.</li>
							<li>- Strong product knowledge about HTML, CSS.</li>
							<li>- Be able to create HTML websites from available designs</li>
						</ul>
						<h2>Job Requirements</h2>
						<ul>
							<li>- Graduated from College/ Universities (Web graphics-design faculty).</li>
							<li>- English communication.</li>
							<li>- Strong product knowledge and business flair</li>
							<li>- Good negotiation and communication skills</li>
							<li>- Good solve problem and make plan</li>
							<li>- Work high pressure, team work, careful</li>
						</ul>
						<h2>Job Requirements</h2>
						<ul>
							<li>- Graduated from College/ Universities (Web graphics-design faculty).</li>
							<li>- English communication.</li>
							<li>- Strong product knowledge and business flair</li>
							<li>- Good negotiation and communication skills</li>
							<li>- Good solve problem and make plan</li>
							<li>- Work high pressure, team work, careful</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>