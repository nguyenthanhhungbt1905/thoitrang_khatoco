<?php 
/*
Template Name: Page Style Guid
*/
get_header(); ?>
<div class="home-slide-holder">
    <div class="home-slide">
        <?php get_banner_at('trang-style-guide'); ?>
    </div>
</div>
<div class="block-outer block-style-guid">
	<div class="block-inner">
		<div class="block-content fluid"><?php $tax = 'style-guide-category';
		$terms = get_terms($tax, array('orderby' => 'id', 'order' => 'desc', 'hide_empty' => false, 'parent' => 0));
		if(!is_null($terms) || !is_wp_error($terms)):
			foreach($terms as $term):
			$term_url = get_term_link($term, $tax); 
			$term_name_parts = explode("-", $term->name);?>
			<div class="col-md-12 fl" id="<?php echo 'term-'. $term->term_id; ?>">
				<div class="item-outer">
					<div class="item-inner clearfix">
						<a href="<?php echo $term_url; ?>">
							<span class="item-thumb"><?php $img_src = wp_get_attachment_image_src(get_single_taxonomy_meta('cat-description', $term->term_id, 'cat-bg-img'), array(562, 310)); ?>
								<img nopin="nopin" src="<?php echo $img_src[0]; ?>" />
							</span>
						</a>
                    <div class="info">
						<h3 class="item-title condensed">
							<?php echo $term_name_parts[0]; ?>
							<?php if (array_key_exists(1, $term_name_parts)) echo '<span>'.$term_name_parts[1].'</span>'; ?>
						</h3>
	
						<p><?php echo $term->description; ?></p>
						<a class="item-button" href="<?php echo $term_url; ?>" id="button_term_<?php echo $term->term_id; ?>">Xem Thêm</a>
                    </div>
					</div>
				</div>
			</div><?php endforeach;
		endif; ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>