<?php get_header(); ?>
<div class="block-outer block-news">
	<div class="container clearfix fluid">
		<div class="block-inner clearfix">
		<?php $args = array('posts_per_page' => 4, 'post_type' => 'news');
		$hot_news = new WP_Query($args);
		$priority = 1;
		while($hot_news->have_posts()):
			$hot_news->the_post();
			$permalink = get_the_permalink();
			if($priority == 1):
				$priority = 0;?>
				<div class="col-md-12 col-xs-6 hot-news mobile-nopadding">
				<div class="hvitem-outer">
					<div class="hvitem-inner clearfix">
						<div class="hvitem-thumb">
		                	<a class="hvitem-thumb-inner" rel="nofollow" href="<?php echo $permalink; ?>">
		                		<?php if(has_post_thumbnail()) 
		                				echo get_the_post_thumbnail(get_the_id(), 'thumbnail-news');
	                				else 
	                					echo '<img src="'. get_template_directory_uri() .'/images/front_news.png'.'" class="attachment-thumbnail-news wp-post-image" alt="front-news">';?>
	                		</a>
		            	</div>
						<div class="hvitem-info">
							<h2 class="hvitem-title condensed"><a href="<?php echo $permalink; ?>" title="<?php echo get_the_title(); ?>"><?php echo wp_trim_words(get_the_title(), 20, '...'); ?></a></h2>
							<p class="hvitem-excerpt"><?php echo wp_trim_words(get_the_content(), 50, '...'); ?></p>
						</div>
					</div>
				</div>
			</div>
			<?php else:
			 ?>
			<div class="col-md-4 col-xs-6 hot-news mobile-nopadding">
				<div class="item-outer">
					<div class="item-inner clearfix">
						<div class="item-thumb">
		                	<a class="item-thumb-inner" rel="nofollow" href="<?php echo $permalink; ?>">
		                		<?php if(has_post_thumbnail()) 
		                				echo get_the_post_thumbnail(get_the_id(), 'thumbnail-news');
	                				else 
	                					echo '<img src="'. get_template_directory_uri() .'/images/front_news.png'.'" class="attachment-thumbnail-news wp-post-image" alt="front-news">';?>
	                		</a>
		            	</div>
						<div class="item-info">
							<h2 class="item-title condensed"><a href="<?php echo $permalink; ?>" title="<?php echo get_the_title(); ?>"><?php echo wp_trim_words(get_the_title(), 20, '...'); ?></a></h2>
							<p class="item-excerpt"><?php echo wp_trim_words(get_the_excerpt(), 15, '...'); ?></p>
						</div>
					</div>
				</div>
			</div>

		<?php endif; endwhile;
		wp_reset_postdata(); ?>
		</div>
	</div>
</div>
<div class="block-outer block-news">
	<div class="container clearfix fluid">
		<div class="title-outer"><h2 class="title title-inner condensed">Tin khuyến mãi</h2></div>
		<div class="block-inner clearfix"><?php $args = array('posts_per_page' => 6, 'post_type' => 'news', 'news_category' => 'tin-khuyen-mai', 'orderby' => 'date', 'order' => 'DESC');
		$hot_news = new WP_Query($args);
		while($hot_news->have_posts()):
			$hot_news->the_post(); 
			display_each_news($post->ID);
		endwhile;
		wp_reset_postdata(); ?>
		
		</div>
		<div class="block-inner">
			<a class="view-more-button" href="<?php echo get_term_link( 'tin-khuyen-mai', 'news_category' ); ?>">Các tin cũ hơn >></a>
		</div>
	</div>

</div>
<div class="block-outer block-news">
	<div class="container clearfix fluid">
		<div class="title-outer"><h2 class="title title-inner condensed">Sự kiện & Hoạt động</h2></div>
		<div class="block-inner clearfix"><?php $args = array('posts_per_page' => 6, 'post_type' => 'news', 'news_category' => 'su-kien', 'orderby' => 'date', 'order' => 'DESC');
		$hot_news = new WP_Query($args);
		while($hot_news->have_posts()):
			$hot_news->the_post(); 
			display_each_news($post->ID);
		endwhile;
		wp_reset_postdata(); ?>
		</div>
		<div class="block-inner">
			<a class="view-more-button" href="<?php echo get_term_link( 'su-kien', 'news_category' ); ?>">Các tin cũ hơn >></a>
		</div>
	</div>
</div>
<?php get_footer(); ?>