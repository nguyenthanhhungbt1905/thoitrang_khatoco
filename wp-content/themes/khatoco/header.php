<?php $metaProperty = array(
    'app_id' => '1447368075554272',
    'type' => 'article',
    'title' => get_option('blogname') .(get_option('blogdescription') ? ' | '. get_option('blogdescription') : ''),
    'url' => home_url() . $_SERVER['REQUEST_URI'],
    'image' => get_template_directory_uri() . '/screenshot.png',
    'description' => get_option('blogdescription'),
);
if( is_tax() || is_category() || is_tag() ) {
    $metaProperty['title'] = single_term_title( '', false ) .' | '. get_option('blogname');
    $metaProperty['description'] = term_description() ? term_description() : $metaProperty['description'];
}
if( is_search() ) {
    $metaProperty['title'] = 'Tìm kiếm: '. get_query_var('s') .' | '. get_option('blogname');
    $metaProperty['description'] = 'Kết quả tìm kiếm "'. get_query_var('s') .'" from '. get_option('blogname');
}
if( is_author() ) {
    $authorID = get_query_var('author');
    $authorData = get_userdata( $authorID );
    $metaProperty['title'] = $authorData->display_name .' @ '. get_option('blogname');
    $metaProperty['description'] = $authorData->descriptionn ? $authorData->description : $metaProperty['description'];
}
if( is_single() || is_page() ) {
    if(get_post_type() == 'post'){
        $product_colors = get_post_meta(get_the_id(), 'post_colors', true);
        foreach($product_colors as $c_key => $c){
            $images_ids = explode(',', $c['images_ids']);
            $images_ids = $images_ids[0];
            $imageSource = wp_get_attachment_image_src($images_ids, 'large');
        }
    }
    else{
        $imageSource = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );
    }
    $postDescription = $post->post_excerpt ? $post->post_excerpt : wp_trim_words( $post->post_content );
    $metaProperty['title'] = $post->post_title;
    if( get_post_type() == 'post' )
        $metaProperty['title'] = 'Sản phẩm '. $metaProperty['title'];
    $metaProperty['description'] = strip_tags( $postDescription );
    $metaProperty['image'] = $imageSource ? $imageSource[0] : $metaProperty['image'];
}
if( is_paged() ) {
    $metaProperty['title'] .= ' | Page '. get_query_var('paged');
    $metaProperty['description'] .= ' | Page '. get_query_var('paged');
}
?>



    <!DOCTYPE html>
    <html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
    <head>
        <title><?php
            // if(is_home()) bloginfo('name'); else wp_title();
            echo str_replace('"', '', $metaProperty['title']);
            ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <meta charset="<?php bloginfo('charset'); ?>" />
        <meta content="text/html; charset=utf-8" http-equiv="content-type">


        <meta property="fb:app_id" content="<?php echo $metaProperty['app_id'];?>" />
        <meta property="og:type" content='<?php echo $metaProperty['type'];?>' />
        <meta property="og:title" content="<?php echo str_replace('"', '', $metaProperty['title']);?>" />
        <meta property="og:url" content="<?php echo $metaProperty['url'];?>" />
        <meta property="og:image" content="<?php echo $metaProperty['image'];?>" />
        <meta property="og:description" content="<?php echo str_replace('"', '', $metaProperty['description']);?>" />
        <script src="<?php echo get_template_directory_uri().'/assets/js/jquery.js' ?>"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.9-1/crypto-js.min.js"></script>
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,700,300,600,400|Open+Sans+Condensed:700,300&subset=latin,vietnamese,latin-ext' rel='stylesheet' type='text/css'>
        <link type="image/x-icon" rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon.png" />
        <link rel="stylesheet" id="addons-css" href="http://<?php echo $_SERVER['HTTP_HOST'] ?>/wp-content/themes/khatoco/assets/css/convert/main.css" type="text/css" media="all">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/assets/css/convert/plugins/slick.css' ?>" type="text/css" media="all">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/assets/css/convert/plugins/slick-theme.css' ?>" type="text/css" media="all">

        <!--    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>-->

        <!--script>
            (function(w,d,u){
                    var s=d.createElement('script');s.async=true;s.src=u+'?'+(Date.now()/60000|0);
                    var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
            })(window,document,'https://cdn.bitrix24.com/b9198309/crm/site_button/loader_2_xrm4mx.js');
        </script-->
        <?php wp_head(); ?>

    </head>
<body <?php body_class(); ?> >
<script type="text/javascript" src="<?php echo get_template_directory_uri().'/assets/plugins/slick/slick.js' ?>"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri().'/assets/js/style.js' ?>"></script>
<?php if (!isset($_SERVER['HTTP_USER_AGENT']) || stripos($_SERVER['HTTP_USER_AGENT'], 'Chrome-Lighthouse') === false): ?>
    <script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>
    <div id="fb-root"></div>

    <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=1447368075554272&version=v2.3";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
        // GooglePlus
        (function() {
            var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
            po.src = 'https://apis.google.com/js/plusone.js?onload=onLoadCallback';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
        })();
    </script>
    <!-- Google Tag Manager -->
    <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-5CZCRW"
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-5CZCRW');</script>
    <!-- End Google Tag Manager -->
<?php endif; ?>


<div class="page-tools">
    <a id="order-check-cta-anchor" href="<?php echo get_permalink(167); ?>" title="">
        <span class="icon"><i class="fa fa-shopping-cart"></i></span>
        <span class="cart-num" id="load_cart_num_anchor"></span>
        <span class="note">Giỏ hàng</span>
    </a>
</div>

<div class="popup close-wrap hidden" id="order-check">
    <div class="close-bg"></div>
    <div class="popup-inner">
        <h2>Tra cứu đơn hàng</h2>
        <form action="<?php echo get_page_link( 2505 ); ?>" method="get" accept-charset="utf-8">
            <p class="popup-hint">Nhập mã đơn hàng (hệ thống đã gửi qua email cho bạn)</p>
            <input type="text" name="order_code" placeholder="Mã đơn hàng *">
            <!-- <input type="text" name="order_phone" placeholder="Nhập số điện thoại"> -->
            <input type="submit" value="Kiểm tra">
        </form>
        <a href="#close" class="close">x</a>
    </div>
</div>

<div class="popup close-wrap hidden" id="login-modal">
    <div class="close-bg"></div>
    <div class="popup-inner">
        <h2>ĐĂNG NHẬP</h2>
        <div class="mid clearfix">
            <form id="" class="account-form" action="" method="POST" autocompleted="off">
                <p class="clearfix">
                </p>
                <p class="clearfix">
                    <input class="field" type="text" name="email" id="lg_email" placeholder="Số điện thoại" autocompleted="off" required />
                </p>
                <p class="clearfix">
                    <input class="field" name="password" id="lg_password" placeholder="Mật khẩu" autocompleted="off" type="password" required />
                </p>
                <p class="clearfix"></p>
                <p class="clearfix">
                    <input type="checkbox" class="checkbox" name="lg_remember" id="lg_remember">
                    <label for="lg_remember"style="padding-left:30px">Ghi nhớ đăng nhập</label>
                    <a href="<?php echo get_page_link(10954); ?>" class="fr">Quên mật khẩu?</a>
                </p>
                <p class="clearfix"></p>
                <p class="clearfix">
                    <button type="submit">Đăng nhập</button>
                    <a href="#login-facebook" id="facebook-login"><i class="fa fa-facebook"></i></a>
                    <input type="hidden" name="redirect" value="<?php echo get_current_url(); ?>" />
                    <input type="hidden" name="action" value="form_login_ajax" />
                    <input type="hidden" id="login_nonce_field" name="login_nonce_field" value="<?php echo wp_create_nonce('login_nonce_action'); ?>" />
                    <input type="hidden" name="_wp_http_referer" value="/" />
                </p>
                <p></p>
                <p id="login_status" class="form-status"></p>
            </form>
        </div>
        <a href="#close" class="close">x</a>
    </div>
</div>

<div class="side-menu-mobile hidden">
    <ul class="list-style-none clearfix">
        <h3><?php if(is_user_logged_in()){
                $user_info = wp_get_current_user();
                echo $user_info->data->display_name ? $user_info->data->display_name : $user_info->data->user_email;
            }
            else echo 'Menu'; ?></h3><?php if(is_user_logged_in()){ ?>
            <li class="personal personal-mobile"><a href="#collapse-menu" data-toggle="collapse" aria-expanded="false" aria-controls="collapse-menu" style="border-bottom: 1px solid #2d2b2b;">Hồ sơ cá nhân&nbsp;&nbsp; <i class="fa fa-angle-right pull-right angle-rotate-menu" aria-hidden="true"></i></a>
                <ul id="collapse-menu" style="padding-left: 15px;">
                    <li class="personal"><a href="<?php echo get_page_link(2503); ?>"><i class="fa fa-user" aria-hidden="true"></i>&nbsp;&nbsp; Thông tin cá nhân</a></li>
                    <li class="personal"><a href="<?php echo get_page_link(2505); ?>"><i class="fa fa-shopping-bag" aria-hidden="true"></i>&nbsp;&nbsp; Đơn hàng</a></li>
                    <li class="personal"><a href="<?php echo get_page_link(2507); ?>"><i class="fa fa-heart" aria-hidden="true"></i>&nbsp;&nbsp; Sản phẩm yêu thích</a></li>
                    <li class="personal-last"><a href="<?php echo wp_logout_url(home_url('/')); ?>"><i class="fa fa-sign-out" aria-hidden="true"></i>&nbsp;&nbsp; Đăng xuất</a></li>
                </ul>
            </li>
        <?php }
        else{ ?>
            <li class="personal"><a href="<?php echo get_page_link(171); ?>">Đăng ký</a></li>
            <li class="personal last"><a href="<?php echo get_page_link(169); ?>">Đăng nhập</a></li>
        <?php } ?>
        <?php wp_nav_menu(array('theme_location' => 'main-menu-mobile', 'container' => false, 'items_wrap' => '%3$s', 'walker' => new Mobile_Nav_Menu())); ?>
    </ul>
    <div id="mobile_menu_close"></div>
</div>
<header id="page-header" class="header">
    <section id="desktop-menu">
        <section id="small-top-menu">
            <div class="container">
                <div class="menu-row">
                    <div class="menu-item">
                        <a href="/">HƯỚNG DẪN MUA HÀNG</a>
                    </div>
                    <div class="menu-item">
                        <a href="/">HỆ THỐNG CỬA HÀNG</a>
                    </div>
                    <div class="menu-item">
                        <a href="/">HỆ THỐNG SHOWROOM</a>
                    </div>
                    <div class="menu-item">
                        <a href="/">THẺ THÀNH VIÊN</a>
                    </div>
                    <div class="menu-item">
                        <a href="/">TRA CỨU ĐƠN HÀNG</a>
                    </div>
                    <div class="menu-item">
                        <a href="/">LIÊN HỆ</a>
                    </div>
                </div>
            </div>
        </section>
        <section id="main-menu">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-2 col-lg-3">
                        <img class="img-fluid logo" src="<?php echo get_template_directory_uri()."/images/new/logo.png" ?>" alt="logo">
                    </div>
                    <div class="col-xs-12 col-md-7 col-lg-5">
                        <div class="group-middle-menu">
                            <div class="mid-menu-item">
                                <img class="img-fluid" alt="san pham" src="<?php echo get_template_directory_uri()."/images/new/icon/tshirt.png" ?>" >
                                <span class="name">SẢN PHẨM</span>
                            </div>
                            <div class="mid-menu-item">
                                <img class="img-fluid" alt="san pham" src="<?php echo get_template_directory_uri()."/images/new/icon/tag.png" ?>" >
                                <span class="name">KHUYẾN MÃI</span>
                            </div>
                            <div class="mid-menu-item">
                                <img class="img-fluid" alt="san pham" src="<?php echo get_template_directory_uri()."/images/new/icon/watch.png" ?>" >
                                <span class="name">GU ĐÀN ÔNG</span>
                            </div>
                            <div class="mid-menu-item">
                                <img class="img-fluid" alt="san pham" src="<?php echo get_template_directory_uri()."/images/new/icon/search.png" ?>" >
                                <span class="name">TÌM KIẾM</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-3 col-lg-4">
                        <div class="group-last-menu">
                            <div class="last-menu-item">
                                <img class="img-fluid" alt="san pham" src="<?php echo get_template_directory_uri()."/images/new/icon/shopping-cart.png" ?>" >
                                <span class="name">GIỎ HÀNG</span>
                            </div>
                            <div class="last-menu-item">
                                <img class="img-fluid" alt="san pham" src="<?php echo get_template_directory_uri()."/images/new/icon/user-tie.png" ?>" >
                                <span class="name">ĐĂNG NHẬP</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>
    <div class="wrapped-mobile">
        <section id="mobile-menu">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="menu-group-menu">
                            <div class="menu-item">
                                <img class="img-fluid logo" src="<?php echo get_template_directory_uri()."/images/new/logo.png" ?>" alt="logo">
                            </div>
                            <div class="menu-item">
                                <a href="javascript:void(0);" class="clickable-item" data-target="product-dropdown">
                                    <img class="img-fluid" alt="san pham" src="<?php echo get_template_directory_uri()."/images/new/icon/tshirt.png" ?>" >
                                    <span class="name">SẢN PHẨM</span>
                                </a>
                            </div>
                            <div class="menu-item">
                                <img class="img-fluid" alt="san pham" src="<?php echo get_template_directory_uri()."/images/new/icon/watch.png" ?>" >
                                <span class="name">GU ĐÀN ÔNG</span>
                            </div>
                            <div class="menu-item">
                                <img class="img-fluid" alt="san pham" src="<?php echo get_template_directory_uri()."/images/new/icon/shopping-cart.png" ?>" >
                                <span class="name">GIỎ HÀNG</span>
                            </div>
                            <div class="menu-item" data-toggle="list-dropdown">
                                <a href="javascript:void(0);" class="clickable-item" data-target="list-dropdown">
                                    <i class="fa fa-bars" aria-hidden="true"></i>
                                    <span class="name">DANH MỤC</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div id="toggle-menu-mobile" class="close">
            <div class="row d-none style-dropdown" id="list-dropdown" >
                <div class="col-xs-12">
                    <div class="item">
                        <img class="img-fluid" alt="san pham" src="<?php echo get_template_directory_uri()."/images/new/icon/tshirt.png" ?>" >
                        <span class="name">SẢN PHẨM</span>
                    </div>
                    <div class="item">
                        <img class="img-fluid" alt="san pham" src="<?php echo get_template_directory_uri()."/images/new/icon/tag.png" ?>" >
                        <span class="name">KHUYẾN MÃI</span>
                    </div>
                    <div class="item">
                        <img class="img-fluid" alt="san pham" src="<?php echo get_template_directory_uri()."/images/new/icon/watch.png" ?>" >
                        <span class="name">GU ĐÀN ÔNG</span>
                    </div>
                    <div class="item">
                        <img class="img-fluid" alt="san pham" src="<?php echo get_template_directory_uri()."/images/new/icon/user-tie.png" ?>" >
                        <span class="name">ĐỒNG PHỤC</span>
                    </div>
                    <div class="item">
                        <img class="img-fluid" alt="san pham" src="<?php echo get_template_directory_uri()."/images/new/icon/star.png" ?>" >
                        <span class="name">TIN TỨC</span>
                    </div>
                    <div class="break-line"></div>
                    <div class="item">
                        <img class="img-fluid" alt="san pham" src="<?php echo get_template_directory_uri()."/images/new/icon/sign-in.png" ?>" >
                        <span class="name">ĐĂNG NHẬP</span>
                    </div>
                    <div class="item">
                        <img class="img-fluid" alt="san pham" src="<?php echo get_template_directory_uri()."/images/new/icon/shopping-cart.png" ?>" >
                        <span class="name">XEM GIỎ HÀNG</span>
                    </div>
                    <div class="break-line"></div>
                    <div class="item">
                        <img class="img-fluid" alt="san pham" src="<?php echo get_template_directory_uri()."/images/new/icon/phone-alt.png" ?>" >
                        <span class="name">LIÊN HỆ</span>
                    </div>
                    <div class="item">
                        <img class="img-fluid" alt="san pham" src="<?php echo get_template_directory_uri()."/images/new/icon/store-alt.png" ?>" >
                        <span class="name">HỆ THỐNG CỬA HÀNG</span>
                    </div>
                    <div class="item">
                        <img class="img-fluid" alt="san pham" src="<?php echo get_template_directory_uri()."/images/new/icon/list-alt.png" ?>" >
                        <span class="name">HƯỚNG DẪN MUA HÀNG</span>
                    </div>
                    <div class="item">
                        <img class="img-fluid" alt="san pham" src="<?php echo get_template_directory_uri()."/images/new/icon/swatchbook.png" ?>" >
                        <span class="name">HƯỚNG DẪN CHỌN SIZE</span>
                    </div>
                    <div class="break-line"></div>
                    <div class="item">
                        <img class="img-fluid" alt="san pham" src="<?php echo get_template_directory_uri()."/images/new/icon/search.png" ?>" >
                        <span class="name">TÌM KIẾM</span>
                    </div>
                    <div class="item">
                        <img class="img-fluid" alt="san pham" src="<?php echo get_template_directory_uri()."/images/new/icon/shopping-bag.png" ?>" >
                        <span class="name">TRA CỨU ĐƠN HÀNG</span>
                    </div>
                </div>
            </div>
            <div class="row d-none style-dropdown" id="product-dropdown">
                <div class="col-xs-12">
                    <?php
                    $menus = wp_get_nav_menu_items('product-menu');
                    $beautifyMenu = [];
                    foreach($menus as $menuItem) {
                        if ($menuItem->post_parent == 0) {
                            $beautifyMenu[$menuItem->ID]['parent'] = $menuItem;
                        } else {
                            $beautifyMenu[$menuItem->menu_item_parent]['childs'][] = $menuItem;
                        }
                    }

                    ?>
                    <?php foreach($beautifyMenu as $key => $item){ ?>
                        <div id="group-<?php echo $key ?>" class="collapse-group">
                            <div class="item item-header" data-toggle="collapse"
                                 data-target="#<?php echo "collapse" . $item['parent']->ID ?>" data-parent="#group-<?php echo $key ?>">
                            <span class="name">
                            <?php echo $item['parent']->title ?>
                                </span>
                                <i class="fa fa-angle-right" aria-hidden="true"></i>
                            </div>
                            <div id="<?php echo "collapse" . $item['parent']->ID ?>"
                                 class="item-collapse collapse">
                                <?php if (!isset($item['childs'])) {
                                    $item['childs'][] = $item['parent'];
                                }
                                foreach ($item['childs'] as $childItem) { ?>
                                    <div class="item">
                                        <span class="name"><?php echo $childItem->title ?></span>
                                    </div>
                                    <?php
                                } ?>
                            </div>
                        </div>
                    <?php } ?>
                </div>

            </div>
        </div>
    </div>
</header>
<?php
//var_dump($menus);die;
?>