<?php 
/*
Template Name: Thẻ khách hàng thân thiết
*/
get_header();
global $post; ?>
<div class="banner">
	<?php echo get_the_post_thumbnail( get_the_id(), 'full', array( 'nopin' => 'nopin' ) ); ?>
</div>
<div class="block-outer block-introduce-subpage">
<div class="container fluid clearfix">
	<div class="block-inner">
		<div class="block-content">
			<div class="col-md-3 col-xs-12">
				<div class="menu-subpage-outer">
				<ul class="menu-subpage">
					<?php aj_nav_menu('sidebar-jobs-menu'); ?>
				</ul>
				<div class="ad-block">
					<a href=""><img nopin="nopin" src="" alt=""></a>
				</div>
				</div>
			</div>
			<div class="col-md-9 col-xs-12">
				<div class="top-block clearfix">
					<div class="intro-header-section">
						<div class="intro-header-title"><?php echo wp_trim_words(get_the_title( $post ), 7); ?></div>
					</div>
				</div>
				<div class="main-page">
					<div class="the-content clearfix">
						<?php echo apply_filters('the_content', $post->post_content); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>