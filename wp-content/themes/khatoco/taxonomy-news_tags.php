<?php get_header();
$tag_slug = get_query_var('term');
$tax = get_query_var('taxonomy');
$tag = get_term_by('slug', $tag_slug, $tax); ?>
<div class="block-outer block-news">
	<div class="container clearfix fluid">
		<div class="block-title-outer"><h1 class="block-title block-title-inner condensed"><span>Từ khoá "<?php single_tag_title(); ?>"</span></h1></div>
		<div class="block-inner clearfix"><?php $args = array('posts_per_page' => 6, 'post_type' => 'news', 'news_category' => 'tin-khuyen-mai', 'news_tags' => $tag->term_id, 'orderby' => 'date', 'order' => 'DESC');
		$hot_news = new WP_Query($args);
		if($hot_news->have_posts())
			while($hot_news->have_posts()):
				$hot_news->the_post(); 
				display_each_news();
			endwhile;
		else
			echo '<p style="text-align:center;padding:50px 0 100px">Không tìm thấy bài đăng phù hợp</p>';
		wp_reset_postdata(); ?>
		</div>
	</div>

</div>
<?php get_footer(); ?>