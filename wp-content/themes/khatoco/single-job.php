<?php get_header(); ?>
<div class="banner">
	<img src="<?php echo get_template_directory_uri(); ?>/images/banner-tuyendung.png" alt="">
</div>
<div class="block-outer block-applyjob">
	<div class="block-inner">
		<div class="block-content container clearfix top-products fluid">
			<div class="grid3">
				<ul class="left-widget list-style-none">
					<?php aj_nav_menu('sidebar-jobs-menu'); ?>
				</ul>
				<div class="ad-block">
					<a href=""><img src="" alt=""></a>
				</div>
			</div>
			<div class="grid9">
				<div class="top-block clearfix">
					<div class="breadcrumb fl">
						<a href=""></a>
						<span class="sub"></span>
						<a href="">Giới thiệu</a>
						<span class="sub"></span>
						<a href="">Vị trí tuyển dụng</a>
					</div>
				</div>
				<div class="main-page"><?php global $post; ?>
					<h2 class="main-title"><?php echo $post->post_title; ?></h2>
					<div class="main-content the-content">
						<?php echo apply_filters('the_content', $post->post_content); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>